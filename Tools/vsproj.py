import os, sys, re, pprint
from xml.dom.minidom import parseString
from os.path import normpath, abspath, relpath, isfile, dirname
from os.path import join as dirjoin

def parse_pxf_project(path):
    """ Extracts information from build-files. The build files must
        use the Builder.lua API.
    """
    with open(path) as f:
        contents = f.read()
        config = {}
        def get_string_arg_for(thing, line):
            if thing in line and not line.startswith("--"):
                match = re.search('{0}\("(.*?)"'.format(thing), line, re.DOTALL)
                if match:
                    return match.group(1)
            return None
        def get_build_target(line):
            if "BUILD_TARGET" in line:
                match = re.findall('"(.*?)"', line, re.DOTALL)
                if match:
                    match[0] = match[0].replace(" ", "_")
                    return tuple(match)
            return None

        includedirs = []
        for line in contents.split("\n"):
            i = get_string_arg_for("Import", line)
            if i: config.setdefault("imports", []).append(i)
            i = get_string_arg_for("NewProject", line)
            if i: config.setdefault("projects", []).append(i)
            i = get_string_arg_for("KjellProject", line) # hack
            if i: config.setdefault("projects", []).append(i)
            i = get_string_arg_for("RequireLibrary", line)
            if i: config.setdefault("libraries", []).append(i)
            i = get_string_arg_for("AddIncludeDirectory", line)
            if i:includedirs.append(i)
            i = get_string_arg_for("RequireModule", line)
            if i: config.setdefault("modules", []).append(i)
            t = get_build_target(line)
            if t: config.setdefault("targets", []).append(t)

        config["relpath"] = "../.."
        for imp in config["imports"]:
            if "Builder.lua" in imp:
                config["relpath"] = dirname(imp)
                break
        # fix includedirs
        config["includedirs"] = []
        for inc in includedirs:
            root = abspath(config["relpath"])
            confdir = dirname(abspath(path))
            incdir = dirjoin(confdir, inc)
            relinc = normpath(incdir.replace(root, config["relpath"]))
            config["includedirs"].append(relinc)

        return config
    return None

text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
def toxml(dom):
    ugly = dom.toprettyxml(indent='  ')
    pretty = text_re.sub('>\g<1></', ugly)
    return pretty

def create_uuid():
    import random
    import hashlib
    uuid = hashlib.md5(str(random.getrandbits(32))).hexdigest().upper()
    def chunks(l, n):
        return [l[i:i+n] for i in range(0, len(l), n)]
    return "{{{0}}}".format('-'.join(chunks(uuid, 5)))

def fmt(path):
    return path.replace("/", "\\")

def create_filter(dom, path):
    f = dom.createElement("Filter")
    f.attributes["Include"] = path
    i = dom.createElement("UniqueIdentifier")
    f.appendChild(i)
    t = dom.createTextNode(create_uuid())
    i.appendChild(t)
    return f

def create_filter_ref(dom, itemtype, filepath, filtername):
    f = dom.createElement(itemtype)
    f.attributes["Include"] = filepath
    i = dom.createElement("Filter")
    f.appendChild(i)
    t = dom.createTextNode(filtername)
    i.appendChild(t)
    return f

def create_file_ref(dom, itemtype, filepath):
    f = dom.createElement(itemtype)
    f.attributes["Include"] = filepath
    return f

def create_sln(options):
    targets = options["config"]["targets"]
    stuff = options
    stuff["STUFF1"] = ""
    stuff["STUFF2"] = ""
    for target in targets:
        name = target[0]
        stuff["STUFF1"] += "\t\t\t{0}|Win32 = {0}|Win32\n".format(name)
        stuff["STUFF2"] += "\t\t\t{1}.{0}|Win32.ActiveCfg = {0}|Win32\n".format(name, options["project_uuid"])
        stuff["STUFF2"] += "\t\t\t{1}.{0}|Win32.Build.0 = {0}|Win32\n".format(name, options["project_uuid"])
    output = SLN_TEMPLATE.format(**stuff)
    return output

def create_vcxproj(options):
    dom = parseString(VCXPROJ_TEMPLATE.format(**options))

    def create_global(dom, options):
        node = parseString("""
          <PropertyGroup Label="Globals">
            <ProjectName>Framework</ProjectName>
            <ProjectGuid>{project_uuid}</ProjectGuid>
            <RootNamespace>Framework</RootNamespace>
            <Keyword>MakeFileProj</Keyword>
          </PropertyGroup>""".format(**options)).childNodes[0]
        return node

    def create_configurations(dom, targets):
        itemgroup = dom.createElement("ItemGroup")
        for target in targets:
            name, command = target[0], target[1]
            projconf = dom.createElement("ProjectConfiguration")
            configuration = dom.createElement("Configuration")
            platform = dom.createElement("Platform")
            itemgroup.appendChild(projconf)
            projconf.appendChild(configuration)
            projconf.appendChild(platform)
            itemgroup.attributes["Label"] = "ProjectConfigurations"
            projconf.attributes["Include"] = "{0}|Win32".format(name)
            configuration.appendChild(dom.createTextNode(name))
            platform.appendChild(dom.createTextNode("Win32"))
        return itemgroup

    def create_property_groups(dom, targets):
        propertygroups = []
        for target in targets:
            name, command = target[0], target[1]
            propertygroup = dom.createElement("PropertyGroup")
            conftype = dom.createElement("ConfigurationType")
            toolset = dom.createElement("PlatformToolset")
            propertygroup.attributes["Condition"] = "'$(Configuration)|$(Platform)'=='{0}|Win32'".format(name)
            propertygroup.attributes["Label"] = "Configuration"
            conftype.appendChild(dom.createTextNode("Makefile"))
            toolset.appendChild(dom.createTextNode("v110"))
            propertygroup.appendChild(conftype)
            propertygroup.appendChild(toolset)
            propertygroups.append(propertygroup)
        return propertygroups

    def create_import_groups(dom, targets):
        importgroups = []
        for target in targets:
            name, command = target[0], target[1]
            importgroup = dom.createElement("ImportGroup")
            importgroup.attributes["Condition"] = "'$(Configuration)|$(Platform)'=='{0}|Win32'".format(name)
            importgroup.attributes["Label"] = "PropertySheets"
            imp = dom.createElement("Import")
            imp.attributes["Project"] = "$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props"
            imp.attributes["Condition"] = "exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')"
            imp.attributes["Label"] = "LocalAppDataPlatform"
            importgroup.appendChild(imp)
            importgroups.append(importgroup)
        return importgroups

    def create_debugger_commands(dom, options):
        commands = []
        targets = options["config"]["targets"]
        for target in targets:
            if len(target) == 4:
                name, command, filetarget = target[0], target[1], target[2]
                debugcmd = target[3]
                pgroup = dom.createElement("PropertyGroup")
                pgroup.attributes["Condition"] = "'$(Configuration)|$(Platform)'=='{0}|Win32'".format(name)
                cmd = dom.createElement("LocalDebuggerCommand")
                cmd.appendChild(dom.createTextNode(debugcmd))
                cmdarg = dom.createElement("LocalDebuggerCommandArguments")
                cmdarg.appendChild(dom.createTextNode(filetarget.format(**options)))
                cmdflv = dom.createElement("DebuggerFlavor")
                cmdflv.appendChild(dom.createTextNode("WindowsLocalDebugger"))
                pgroup.appendChild(cmd)
                pgroup.appendChild(cmdarg)
                pgroup.appendChild(cmdflv)
                commands.append(pgroup)
        return commands


    def create_build_commands(dom, options):
        def create_node(name, conf, content):
            node = dom.createElement(name)
            node.attributes["Condition"] = confstr
            node.appendChild(dom.createTextNode(content))
            return node
        configs = options["config"]["targets"]
        propertygroup = dom.createElement("PropertyGroup")
        projectversion = dom.createElement("_ProjectFileVersion")
        projectversion.appendChild(dom.createTextNode("10.0.30319.1"))
        propertygroup.appendChild(projectversion)

        dirs = options["config"]["includedirs"]

        includesearch = list(dirs) +["..\\..\\Include", "$(NMakeIncludeSearchPath)"]
        for target in configs:
            name, command, destfile = target[0], target[1], target[2]
            defs = ["WIN32", "$(NMakePreprocessorDefinitions)"]
            if "debug" in name.lower() or "debug" in command.lower():
                defs.append("_DEBUG")
            confstr = "'$(Configuration)|$(Platform)'=='{0}|Win32'".format(name)
            propertygroup.appendChild(create_node("NMakeOutput", confstr, destfile.format(**options)))    # todo dll support
            propertygroup.appendChild(create_node("NMakeBuildCommandLine", confstr, "{0}".format(command)))    
            propertygroup.appendChild(create_node("NMakeReBuildCommandLine", confstr, "{0}".format(command)))    
            propertygroup.appendChild(create_node("NMakeCleanCommandLine", confstr, "{0} -c".format(command)))    
            propertygroup.appendChild(create_node("NMakePreprocessorDefinitions", confstr, ";".join(defs)))    
            propertygroup.appendChild(create_node("NMakeForcedIncludes", confstr, "$(NMakeForcedIncludes)"))    
            propertygroup.appendChild(create_node("NMakeIncludeSearchPath", confstr, ";".join(includesearch)))    
            propertygroup.appendChild(create_node("NMakeForcedUsingAssemblies", confstr, "$(NMakeForcedUsingAssemblies)"))    
            propertygroup.appendChild(create_node("NMakeAssemblySearchPath", confstr, "$(NMakeAssemblySearchPath)"))    
            propertygroup.appendChild(create_node("OutDir", confstr, "$(SolutionDir)$(Configuration)\\"))    
            propertygroup.appendChild(create_node("IntDir", confstr, "$(Configuration)\\"))    
        return propertygroup

    def generate_project_configurations(dom):
        config = options["config"]
        nodes = []
        nodes += [create_configurations(dom, config["targets"])]
        nodes += [create_global(dom, options)]
        nodes += [parseString('<Import Project="$(VCTargetsPath)\\Microsoft.Cpp.Default.props" />').childNodes[0]]
        nodes += create_property_groups(dom, config["targets"])
        nodes += [parseString('<Import Project="$(VCTargetsPath)\\Microsoft.Cpp.props" />').childNodes[0]]
        nodes += [parseString('<ImportGroup Label="ExtensionSettings"></ImportGroup>').childNodes[0]]
        nodes += [parseString('<ItemDefinitionGroup></ItemDefinitionGroup>').childNodes[0]]
        nodes += create_import_groups(dom, config["targets"])
        nodes += [parseString('<PropertyGroup Label="UserMacros" />').childNodes[0]]
        nodes += [create_build_commands(dom, options)]
        nodes += create_debugger_commands(dom, options)
        nodes += [parseString('<Import Project="$(VCTargetsPath)\\Microsoft.Cpp.targets" />').childNodes[0]]
        nodes += [parseString('<ImportGroup Label="ExtensionTargets"></ImportGroup>').childNodes[0]]
        return nodes

    for node in generate_project_configurations(dom):
        dom.childNodes[0].appendChild(node)

    for t, node in nodetypes:
        gsource = dom.createElement("ItemGroup")
        for cat, files in options["files"].items():
            for fn in files.get(t, []):
                gsource.appendChild(create_file_ref(dom, node, fn))
        dom.childNodes[0].appendChild(gsource)
    return toxml(dom)

def create_vcxproj_filters(options):
    def all_paths(path):
        paths = set()
        parts = path.split("\\")
        sub = ""
        for part in parts:
            sub += part
            paths.add(sub)
            sub += "\\"
        return paths

    def make_filter(group, filetype, fn):
        filtername = group
        subcats = dict(source="Source", include="Headers", text=None, other=None)
        fn = abspath(fn)
        subcat = subcats[filetype]
        if group == "Library":
            path = fn.split("\\")
            idx = path.index("Libraries")
            comp = path[idx:idx+2]
        elif group == "Module":
            path = fn.split("\\")
            idx = path.index("Modules")
            comp = path[idx:idx+2]
        elif group == "Framework":
            path = fn.split("\\")
            if "Source" in path:
                idx = path.index("Source")
                comp = ["Framework"] + path[idx+1:-1]
            else:
                idx = path.index("Include")
                path.remove("Pxf")
                if path[-2] == "Include":
                    subcat = None # Top level files
                comp = ["Framework"] + path[idx+1:-1]
        elif group == "Project":
            subcat = None
            path = fn.split("\\")
            idx = path.index("Projects")
            comp = path[idx:-1]
            if "main.c" in fn.lower():
                comp = path[idx:idx+2]
        if subcat:
            comp += [subcat]
        filtername = "\\".join(comp)
        return filtername

    all_filters = {}
    for filetype, node in nodetypes:
        for group in options["files"].keys():
            for fn in options["files"].get(group,{}).get(filetype, []):
                flt = make_filter(group, filetype, fn)
                nodetype = dict(nodetypes)[filetype]
                all_filters.setdefault(flt, []).append((fn, nodetype))

    filters = set()
    for flt in all_filters.keys():
        filters.update(all_paths(flt))

    dom = parseString(VCXPROJ_FILTER_TEMPLATE)
    gfilter = dom.createElement("ItemGroup")

    for flt in filters:
        gfilter.appendChild(create_filter(dom, fmt(flt)))
    dom.childNodes[0].appendChild(gfilter)

    groups = {}
    for flt, files in all_filters.items():
        for fn, nodetype in files:
            groups.setdefault(nodetype, []).append((fn, flt))
    for nodetype, files in groups.items():
        gsource = dom.createElement("ItemGroup")
        for fn, flt in files:
            gsource.appendChild(create_filter_ref(dom, nodetype, fn, flt))
        dom.childNodes[0].appendChild(gsource)

    return toxml(dom)

nodetypes = [("source", "ClCompile")
            ,("include", "ClInclude")
            ,("text", "Text")
            ,("other", "None")]

def scan_for_files(options):
    projbase = options["relative_path"]
    config = options["config"]
    projpath = os.getcwd()
    framework_path = abspath(dirjoin(projpath, projbase))
    sourcedirs = [("Include", "Framework"),
                  ("Source", "Framework"),
                  ("Modules", "Module"),
                  ("Projects\\Lapp", "Project"),  #TODO: only include relevant projects
                  ("Projects\\Tetos", "Project"),
                  ("Projects\\Saint", "Project"),
                  ("Projects\\Mad", "Project"),
                  ("Projects\\Kjell", "Project")]
    sourcedirs += [("Libraries\\"+x, "Library") for x in config["libraries"]]
    def find_files(d):
        headers = []
        sources = []
        texts = []
        others = []
        for root, dirs, files in os.walk(d):
            if "Libraries" in d:
                blacklist = ["Documentation", "html", "test", "samples",
                            "bin", "contents", "doc", "projects",
                            "data", "cmake",  "backup"]
                if any(b.lower() in root.lower() for b in blacklist):
                    continue
            for f in files:
                projbase = relpath(dirjoin(root, f))
                projbase = fmt(projbase)
                if any(projbase.endswith(e) for e in [".h", ".hxx"]):
                    headers.append(projbase)
                elif any(projbase.endswith(e) for e in [".c", ".cpp", ".cxx"]):
                    sources.append(projbase)
                elif any(projbase.endswith(e) for e in [".txt"]):
                    texts.append(projbase)
                elif any(projbase.endswith(e) for e in [".def"]):
                    others.append(projbase)
        return headers, sources, texts, others
    cats = {}
    for d, cat in sourcedirs:
        searchpath = dirjoin(framework_path, d)
        print "Scanning for files in {0}".format(searchpath)
        hs, ss, ts, oths = find_files(searchpath)
        if not cat in cats:
            cats[cat] = dict(source=set(), include=set(), text=set(), other=set())
        cats[cat]["source"].update(ss)
        cats[cat]["include"].update(hs)
        cats[cat]["text"].update(ts)
        cats[cat]["other"].update(oths)
    return cats

def main(argv):
    import pprint
    if len(argv) == 0:
        print("usage: vsproj <path-to-buildfile>")
        return
    buildfile = argv[0]
    if not isfile(buildfile) and not buildfile.endswith(".lua"):
        print("Invalid build file.")
        return

    print "Scanning build file..."
    config = parse_pxf_project(buildfile)
    msg = None
    if len(config.get("imports", [])) == 0:
        msg = "Missing import(s)"
    elif len(config.get("projects", [])) == 0:
        msg = "Missing project definition(s)"
    elif len(config.get("projects", [])) > 1:
        msg = "Too many project definitions."
    elif len(config.get("targets", [])) == 0:
        msg = "Missing build target(s)"
    if msg:
        print "Could not parse build file: {0}".format(msg)
        return
    #includes = config["includedirs"]

    # Figure out the relative path to the the framework basedir
    relpath = config["relpath"]

    # Scan for libraries used by modules
    print "Scanning modules..."
    for mod in config["modules"]:
        path = dirjoin(relpath, "Modules", mod, "{0}.lua".format(mod))
        proj = parse_pxf_project(path)
        config["libraries"] += proj["libraries"]
        config["includedirs"] += proj["includedirs"]

    # Scan for libraries used by modules
    print "Scanning libraries..."
    for lib in config["libraries"]:
        path = dirjoin(relpath, "Libraries", lib, "{0}.lua".format(lib))
        proj = parse_pxf_project(path)
        config["includedirs"] += proj["includedirs"]

    print "Creating project files..."

    options = {}
    options["version"] = "12.00"
    options["project_name"] = config["projects"][0]
    options["solution_uuid"] = create_uuid()
    options["project_uuid"] = create_uuid()
    options["config"] = config
    options["relative_path"] = relpath
    options["files"] = scan_for_files(options)

    with open("options.txt", "w") as f:
        f.write(pprint.pformat(options))

    sln = create_sln(options)
    vcx = create_vcxproj(options)
    vcxf = create_vcxproj_filters(options)

    targets = [("sln", sln), ("vcxproj", vcx), ("vcxproj.filters", vcxf)]
    for targetext, contents in targets:
        targetfile = "vs_{0}.{1}".format(options["project_name"], targetext)
        with open(targetfile, "w") as f:
            print "Writing file: {0}".format(targetfile)
            for line in contents.split("\n"):
                if line.strip() == "":
                    continue
                f.write(line + "\n")

#
# Templates
#

SLN_TEMPLATE = """Microsoft Visual Studio Solution File, Format Version {version}
Project("{solution_uuid}") = "Framework", "vs_{project_name}.vcxproj", "{project_uuid}"
EndProject
Global
    GlobalSection(SolutionConfigurationPlatforms) = preSolution
{STUFF1}
    EndGlobalSection
    GlobalSection(ProjectConfigurationPlatforms) = postSolution
{STUFF2}
    EndGlobalSection
    GlobalSection(SolutionProperties) = preSolution
        HideSolutionNode = FALSE
    EndGlobalSection
    GlobalSection(Performance) = preSolution
        HasPerformanceSessions = true
    EndGlobalSection
EndGlobal
"""


VCXPROJ_TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003"></Project>"""


VCXPROJ_FILTER_TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003"></Project>"""

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])