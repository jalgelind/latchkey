import os, difflib, copy

d = difflib.Differ()

allguards = {}

import sys
REWRITE = "rewrite" in sys.argv

linecount = {}
linecount["total"] = 0

headercount = {}

def do_dir(targetdir):
    for root, dirs, files in os.walk(targetdir):

        for file in files:
            filepath = os.path.join(root, file)
            filext = os.path.splitext(filepath)[1]
            if filext in (".h", ".hpp", ".hxx", ".c", ".cpp", ".def"):

                content = ""
                with open(filepath, "r") as f:
                    content = f.readlines()
                    fullfile = "".join(content)

                linecount.setdefault(filext, 0)
                linecount[filext] += len(content)
                linecount["total"] += len(content)

                if filext in (".h", ".hpp", ".hxx"):
                    count = fullfile.count("#include")
                    headercount.setdefault(count, [])
                    headercount[count].append(os.path.basename(filepath))

                if len(content) == 0:
                    continue

                original = copy.copy(content)

                # Fix trailing whitespace issues
                for i, line in enumerate(content):
                    content[i] = line.rstrip() + "\n"

                # Add newline if missing
                if content[-1].strip() != "":
                    content.append("\n")


                if not "Libraries" in root:

                    for i, line in enumerate(content):
                        line = line.strip()
                        linel = line.lower()
                        annotations = ["fixme", "todo", "xxx", "bug:"]
                        if any(a in linel for a in annotations):
                            print line, "(" + os.path.basename(filepath) + ":" + str(i) + ")"

                # Find header guards for our code 
                def find_guards(content):
                    guards = []
                    for line in content:
                        line = line.strip()
                        keywords = ["#define", "#ifndef", "#endif"]
                        for keyword in keywords:
                            if keyword in line and "PXF_" in line and \
                                    (line.endswith("H_") \
                                     or line.endswith("_H__")
                                     or line.endswith("_H")):
                                s = line.split()
                                constant = s[-1]
                                guards.append(constant.replace("//", ""))
                    return guards

                def is_guard_ok(guards):
                    guard = guards[0]
                    if guard.endswith("_H") \
                        or guard.startswith("__") \
                        or guard.endswith("H__"):
                        return False
                    validity = all(x == guard for x in guards)
                    return validity

                if "Modules" in filepath or "Include" in filepath:
                    guards = find_guards(content)
                    if len(guards) > 0 and not is_guard_ok(guards):
                        print "Header guard for '{0}' is broken.".format(filepath)
                        print guards
                        print
                    else:
                        if len(guards) > 0 and filepath.endswith(".h"):
                            if guards[0] not in allguards:
                                allguards[guards[0]] = filepath
                            elif file[:-file.rfind(".")-1].lower() not in guards[0].lower():
                                print "Possibly broken header guard name:"
                                print "-->", filepath
                                print "-->", guards[0]
                            else:
                                print "Include guard conflicts:"
                                print "-->", filepath
                                print "-->", allguards[guards[0]]
                                print
                        else:
                            if filepath.endswith(".h"):
                                print "Headerfile is missing include guards."
                                print "--> ", filepath
                                print

                if REWRITE:
                    with open(filepath, "wb") as f:
                        f.writelines(content)

do_dir("../Include")
do_dir("../Source")
do_dir("../Projects")
do_dir("../Modules")
#do_dir("../Tools")

import pprint
print
print "Headercount:"
pprint.pprint(headercount)
#for c, hs in sorted(headercount.items(), key=lambda x: x[0]):
#    print c, hs
print
print "Linecount: "
pprint.pprint(linecount)
