import os, sys, glob, json, shutil, pprint

def format_api(objects):
    lines = []
    f = None

    def get_description(comments):
        descr = ""
        for c in comments:
            descr += c.replace("//)", "") + "\n"
        return descr

    def get_collapsed(args, kwargs):
        margs = {}
        for i, arg in enumerate(args):
            margs[arg[1]] = (i, arg[0], arg[2], arg[3])

        collapsed = []
        for ck, v in kwargs.items():
            if "," in ck:
                cnames = []
                ctype = "<UNKNOWN>"
                cdescr = ""
                ks = map(unicode.strip, ck.split(","))
                if not all(k in margs for k in ks):
                    continue
                for k in ks:
                    cnames.append(k)
                    ctype = margs[k][1]
                    cdescr = v[0]
                collapsed.append((ctype, cnames, cdescr))
        return collapsed

    def out(f, txt):
        lines.append(txt)
    
    for obj in objects:
        out(f, "## " + obj["system"])

        out(f, "@text")
        out(f, "\n".join(obj["comments"]))
        
        out(f, "@api")
        for method in obj["used_methods"] + obj["used_operators"]:
            objinfo = obj["methods"][method]

            #pprint.pprint(objinfo)

            def format_ret(ret):
                ret = list(set(ret))
                if len(ret) > 0:
                    return ": " + ' | '.join(ret)
                else:
                    return ""

            def format_arg(arg):
                dv = ("" if not arg[2] else " = {0}".format(arg[2]))
                name = arg[1].strip()
                return name + dv

            ret = ""
            if objinfo["numreturn"] > 0:
                ret = format_ret(objinfo["return"])
            description = get_description(objinfo["comments"])
            details = get_description(objinfo["detail_comments"])
            

            arglists = objinfo["args"]
            for arglist in arglists:
                args = ", ".join(format_arg(x) for x in arglist)

                optbl = {"__add": "+,-", "__eq": "==", "__mul": "*", "__div": "/"}
                if method in optbl:
                    out(f, "operator " + optbl[method] + " ({0}) ".format(args) + str(ret))
                else:
                    out(f, method + "({0}) ".format(args) + str(ret))
                out(f, description)

                collapsed = get_collapsed(arglist, objinfo["kwargs"])
                iscollapsed = []
                for ct, cn, cdesc in collapsed:
                    iscollapsed += cn
                    out(f, "\t:" + ", ".join(cn) + " : " + ct)
                    out(f, "\t\t" + cdesc)

                for at, an, ad, desc in arglist:
                    if an in iscollapsed:
                        continue
                    out(f, "\t:" + an + " : " + at)
                    kwa = objinfo["kwargs"]
                    if an in kwa:
                        out(f, "\t\t" + "\n\t\t".join(kwa[an]))
                    elif desc:
                        out(f, "\t\t" + desc)
                
                out(f, details)

        for method in obj["used_properties"]:
            out(f, "parameter: " + method)

        out(f, "@text")
        out(f, "")
    return "\n".join(lines)

def get_api(fp):
    singletons = []
    objects = []
    with open(fp, "r") as f:
        content = json.loads(f.read())
        for ns, objs in content.items():
            for n, obj in objs.items():
                obj = obj[0]
                if "options" in obj and "singleton" in obj["options"]:
                    singletons.append(obj)
                else:
                    objects.append(obj)
    ret = {}
    ret[ns.upper() + "_SINGLETONS"] = format_api(singletons)
    ret[ns.upper() + "_OBJECTS"] = format_api(objects)
    return ret


def main(args):
    if len(args) != 2:
        print "usage: gen.py output/ docsource/"
        return
    outdir = args[0]
    docdir = args[1]
    if not os.path.exists(outdir):
        print "Output directory must exist and be populated with meta data"
        return
    if not os.path.exists(docdir):
        print "Invalid source documentation dir..."
        return

    if docdir == outdir:
        print "Output directory and documentation source can not be the same"
        return

    # Copy files to a separate working directory
    join = os.path.join
    for fp in glob.glob(join(docdir, "*.rb")):
        shutil.copy(fp, outdir)
    for fp in glob.glob(join(docdir, "*.bsdoc")):
        shutil.copy(fp, outdir)
    shutil.copy(join(docdir, "template.html"), outdir)

    stuff = {}
    for fp in glob.glob(join(outdir, "*.json")):
        for k, v in get_api(fp).items():
            stuff[k] = v
    #print stuff
    #stuff["LAPP_SINGLETONS"] = "test..."
    #stuff["LAPP_OBJECTS"] = "test..."
    #stuff["MATH_SINGLETONS"] = "test..."
    #stuff["MATH_OBJECTS"] = "test..."
    #stuff["SAINT_SINGLETONS"] = "test..."
    #stuff["SAINT_OBJECTS"] = "test..."

    for fp in glob.glob(join(outdir, "*.bsdoc")):
        content = None
        with open(fp, "r") as f:
            content = f.read()
        content = content.format(**stuff)
        with open(fp, "w") as f:
            f.write(content)

    os.chdir(outdir)
    os.system("ruby _make.rb")


if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) == 1 and args[0] == '':
        args = []
    main(args)
