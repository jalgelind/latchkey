import os, re
from jinja2 import Template

SCANDIRS = [
    "../Include",
    "../Source",
    "../Projects",
    "../Modules"
]

SKIPFILES = ["PreloadedResources.h"]

TODOS = ["todo", "xxx", "fixme", "bug:", "hack"]

FINDEXTS = [".h", ".hxx", ".hpp", ".c", ".cpp", ".lua", ".def"]

def analyze_file(path):
    linecount = 0
    blanklines = 0
    includes = []
    todos = []
    with open(path) as f:
        for line in f:
            s = re.search("#include <(.*)>", line)
            if s: includes.append(os.path.normpath(s.group(1)))
            linecount += 1
            if len(line.strip()) == 0:
                blanklines += 1
            for todo in TODOS:
                if todo.lower() in line.lower():
                    todos.append("{0}: {1}".format(linecount, line))

    ret = {"path": path, 
           "linecount": linecount,
           "blanklines": blanklines,
           "includes": includes,
           "todos": todos}
    return ret

def fext(path):
    return os.path.splitext(path)[1]

def find_source_files(scandirs, exts):
    found = {}
    for targetdir in scandirs:
        for root, dirs, files in os.walk(targetdir):
            for filename in files:
                filepath = os.path.normpath(os.path.join(root, filename))
                filext = fext(filepath)
                if filext in exts:
                    for skip in SKIPFILES:
                        if skip not in filepath:
                            found.setdefault(filext, []).append(analyze_file(filepath))
    return found

def find_files_using(allfiles, targetfile, targext):
    deps = []
    for ext, files in allfiles.items():
        for finfo in files:
            for inc in finfo["includes"]:
                if inc in targetfile and fext(finfo["path"]).startswith(targext):
                    deps.append(finfo["path"])
    return deps

def get_general_info(allfiles):
    ret = {"linecount": 0}
    for ext, files in allfiles.items():
        for finfo in files:
            ret["linecount"] += finfo["linecount"]
    return ret


def main():
    allfiles = find_source_files(SCANDIRS, FINDEXTS)
    headerdeps = {}
    sourcedeps = {}
    for ext, files in allfiles.items():
        for f in files:
            if ext.startswith(".h"):
                headerdeps[f["path"]] = find_files_using(allfiles, f["path"], ".h")
                sourcedeps[f["path"]] = find_files_using(allfiles, f["path"], ".c")
    general = get_general_info(allfiles)
    template = Template(REPORT_TEMPLATE)
    with open("report.html", "w") as f:
        f.write(template.render(allfiles=allfiles, 
                                headerdeps=headerdeps, 
                                sourcedeps=sourcedeps,
                                general=general))

REPORT_TEMPLATE = """
<html>
    <head><title>pxf-base report</title></head>
    <body>
        <h1>general statistics</h1>
        <table border="1">
            <tbody>
                {% for key, value in general.items() %}
                <tr>
                    <td>{{ key }}</td>
                    <td>{{ value }}</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>

        <h1>header dependencies</h1>
        <table border="1">
            <thead>
                <tr>
                    <td>header</td>
                    <td>used in header</td>
                </tr>
            </thead>
            <tbody>
                {% for header, used_by in headerdeps.items() %}
                <tr>
                    <td>{{ header }}</td>
                    <td>
                        <table>
                        {% for header_user in used_by %}
                        <tr><td> {{ header_user }}</td></tr>
                        {% endfor %}
                        </table>
                    </td>
                </tr>
                {% endfor %}
            </tbody>
        </table>

        <h1>source file dependencies</h1>
        <table border="1">
            <thead>
                <tr>
                    <td>header</td>
                    <td>used in source file</td>
                </tr>
            </thead>
            <tbody>
                {% for source, used_by in sourcedeps.items() %}
                <tr>
                    <td>{{ source }}</td>
                    <td>
                        <table>
                        {% for header_user in used_by %}
                        <tr><td> {{ header_user }}</td></tr>
                        {% endfor %}
                        </table>
                    </td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
    
        <h1>files scanned</h1>
        <table border="1">
            <thead>
                <tr>
                    <td>path</td>
                    <td>total lines</td>
                    <td>blank lines</td>
                    <td>todo / notes</td>
                    <td>includes</td>
                </tr>
            </thead>
            <tbody>
                {% for ext, fs in allfiles.items() %}
                {% for finfo in fs %}
                <tr>
                    <td>{{ finfo.path }}</td>
                    <td>{{ finfo.linecount }}</td>
                    <td>{{ finfo.blanklines }}</td>
                    <td>{{ finfo.todos|length }}</td>
                    <td>{{ finfo.includes|length }}</td>
                </tr>
                {% endfor %}
                {% endfor %}
            </tbody>
        </table>


    </body>
</html>
"""

if __name__ == '__main__':
    main()