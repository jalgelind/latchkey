#
# Extract our Lua API from our source files
#

"""
    Goal:
        Extract as much documentation as possible from the source code, so documentation 
        and API/implementation stay in sync with minimal need for maintenance.

    Notes:
        This is not a C or C++ parser. We only look for macros, calls and variables used by Lua/Luna/Lunar.
        We try to extract return values and arguments by inspecting the Lua calls in the function body.

    Use special comments to indicate options and documentation.

    //) like this

    Specify if object is singleton or not

    //) option: singleton

    Specify if source file should be ignored

    //) option: ignore

    Files that inherits from a common base (for grabbing more implementation details)

    //) inherit: Math_VecT.h

    When the parser gets confused, specify arguments and return types like this:

    //) def: arg1 : type, arg2 = defaultvalue : integer, arg3 : table -> returntype

"""

# TODO: multiline comments, /*)   */

import sys, re, os, pprint

API_PATHS = ["../Projects/Lapp"
            ,"../Projects/Saint"]


#
# (nasty) Source file parser
#

def classify(r, selftype):
    if 'number' in r:
        return 'number'
    if 'integer' in r:
        return 'integer'
    elif 'string' in r:
        return 'string'
    elif 'boolean' in r:
        return 'boolean'
    elif 'pushnil' in r:
        return 'nil'
    elif '<' in r and '>' in r:
            cls = re.search("<(.*)>", r).group(1).replace("_", ".")
            if cls == "Class":
                cls = selftype
            return "%r " + cls.lower()+"()"
    return None

def sanitize(name):
    return name.replace("(", "").replace(")", "").strip()

def get_return_type(r, selftype):
    return classify(r, selftype)

class ArgConflictException(Exception):
    pass

class ArgErrorException(Exception):
    pass

class ArgTypeException(Exception):
    pass

def sortargs(args):
    items = sorted(args.items(), key = lambda x: x[0])
    return [item for idx, item in items]

def get_arguments(method, arglines, selftype):
    arglist = {}
    for arg in arglines:
        errmsg = "{0} {1}: {2}".format(selftype, method, arg)
        if not "check" in arg and not "toboolean" in arg:
            continue
        arg = arg.strip()
        simple = re.search("\s*(.*)=(.*)\(.*,.*(\d+)\)", arg)
        if simple:
            varname, funname, argnum = simple.groups()
            if ' ' in varname:
                varname = varname.split()[-1]
                # dotted members
                if "." in varname:
                    if "[" in varname:
                        raise ArgErrorException("won't extract variable from array")
                    varname = varname.rsplit(".")[-1]
            vartype = classify(funname, selftype)
            if vartype == None:
                raise ArgTypeException(errmsg)
            if argnum in arglist:
                raise ArgConflictException(errmsg)
            
            descr = None
            if "//)" in arg:
                descr = arg.rsplit("//)", 1)[1].strip()

            defaultvalue = None # Hm, get from comment on the same line

            arglist[argnum] = (vartype.strip(), sanitize(varname), defaultvalue, descr)
        else:
            print "ARGERROR:", errmsg
            #raise ArgErrorException(errmsg)
    return sortargs(arglist)

def get_explicit_arguments(method, margs, kwargs, selftype):
    if not "def" in kwargs:
        raise ArgConflictException("{0}: {1}".format(method, kwargs))
    rets = []
    for args in kwargs["def"]:
        ret = {}
        returntype = None
        if "->" in args:
            args, returntype = args.split("->", 1)
            if "," in returntype:
                returntype = returntype.split(",")
            else:
                returntype = [returntype]
        if "," in args:
            args = args.split(",")
        else:
            args = [args]

        for i, arg in enumerate(args):
            if arg == '': continue
            name, typename = arg.split(":")
            dv = None
            typename = typename.strip()
            if "|" in typename:
                types = typename.split("|")
                types = [("%r " + t.strip()) if t.strip().endswith("()") else t for t in types]
                typename = " | ".join(types)
            else:
                if typename.endswith("()"):
                    typename = "%r " + typename
            if "=" in name:
                name, dv = name.split("=")
                dv = dv.strip()
            name = name.strip()
            typename = typename.strip()
            descr = None

            # TODO: refactor this part, it's copy pasted from above...
            for arg in margs:
                arg = arg.strip()
                simple = re.search("\s*(.*)=(.*)\(.*,.*(\d+)\)", arg)
                if simple:
                    varname, funname, argnum = simple.groups()
                    if ' ' in varname:
                        varname = varname.split()[-1]
                    if name == varname and "//)" in arg:
                        descr = arg.rsplit("//)", 1)[1].strip()
            ret[i] = (typename, name, dv, descr)
        rets.append(sortargs(ret))
    return rets, returntype


#
# Process and parse file
#

def process_file(path, headers):
    kvline = "//\)\s*([a-zA-Z0-9_,\s*]+):(.*)"
    methodline = "LUNA.*_DECLARE_METHOD\(.*, (.*)\)"
    propertyline = "LUNA_DECLARE_PROPERTY\(.*, (.*)\)"
    classnameline = "::className\[\] = \"(.*)\""
    namespaceline = "::classNamespace\[\] = \"(.*)\""
    functiondef = "int\s*.*::(.*)\(lua_State\s*\*\s*L\)"
    functiondef2 = "int\s*(.*)\(lua_State\s*\*\s*L\)"
    retval = "return\s+(\d+);"

    NONLOCALARGH = {} # since I use python 2.7, I can't use nonlocal.
    NONLOCALARGH["ignore_file"] = False
    NONLOCALARGH["classname"] = "?"
    NONLOCALARGH["namespace"] = "?"

    related = [] # related files
    options = [] # all enabled options
    comments = []
    used_methods = []
    used_properties = []
    used_operators = []
    methods = {}

    system = os.path.basename(os.path.splitext(path)[0])
    system = system.lower().replace("_", ".")

    def process_kv(match):
        k, v = match.groups()
        k = k.strip()
        if k == "inherit":
            related.append(v.strip())
        else:
            options.append(v.strip())

    def process_method(match):
        method = match.group(1)
        if method.startswith("__"):
            used_operators.append(method)
        else:
            used_methods.append(method)

    def is_argument(line):
        return re.search("\(L,\s*\d+\)", line) or \
            re.search("check.*\(L,.*\)", line) or \
            re.search("tobool.*\(L,.*\)", line)

    def extract_argument(line):
        return line

    def is_return(line):
        if "lua_pushlightuserdata" in line:
            return False
        return "::push" in line or "lua_push" in line

    def extract_return(line):
        return get_return_type(line, system)

    def process_methodparse(state, line):
        def add_comment(line):
            if "@details" in line:
                state["_details?"] = True
            else:
                if state["_details?"]:
                    state["detail_comments"].append(line.strip())
                else:
                    state["comments"].append(line.strip())
        m = re.search(kvline, line)
        rm = re.search(retval, line)
        if is_argument(line):
            state["args"].append(extract_argument(line))
        elif is_return(line):
            state["return"].append(extract_return(line))
        elif m:
            k, v = line.replace("//)", "").split(':', 1)
            k = k.strip() # might be csv
            v = v.strip()
            if v == "" and k not in state["kwargs"]:
                add_comment(line)
            else:
                state["kwargs"].setdefault(k, []).append(v)
        elif line.strip().startswith("//)"):
            add_comment(line)
        elif rm:
            state["numreturn"] = int(rm.group(1))

    def do_file(fn):
        with open(fn, "r") as f:
            scope = 0
            mscope = 0
            parsemethod = False
            state = {}
            for line in f:
                line = line.strip()

                # Ignore file
                if 'ignore' in options:
                    NONLOCALARGH["ignore_file"] = True
                    break

                # Ignore regular comments.
                if not line.startswith("//)") and line.startswith("//"):
                    continue

                # Track current scope
                obrack = line.count("{")
                cbrack = line.count("}")
                scope += obrack - cbrack

                # Single line data
                m = re.search(kvline, line)
                if m and not parsemethod: process_kv(m)
                elif not parsemethod and line.startswith("//)"):
                    comments.append(line.replace("//)", "").strip())
                m = re.search(methodline, line)
                if m: process_method(m)
                m = re.search(propertyline, line)
                if m: used_properties.append(m.group(1))
                m = re.search(classnameline, line)
                if m: NONLOCALARGH["classname"] = m.group(1)
                m = re.search(namespaceline, line)
                if m: NONLOCALARGH["namespace"] = m.group(1)

    
                # Multiline data (function bodies)
                m = re.search(functiondef, line)
                if not m and fn.endswith(".h"):
                    m = re.search(functiondef2, line)
                if m and not ';' in line:
                    mscope = scope
                    parsemethod = True
                    state = {}
                    state["method"] = m.group(1)
                    state["return"] = []
                    state["comments"] = []
                    state["detail_comments"] = []
                    state["_details?"] = False
                    state["kwargs"] = {}
                    state["args"] = []
                    state["numreturn"] = 0
                    continue
                if parsemethod:
                    if scope == mscope:
                        parsemethod = False

                        # Format arguments
                        method, args, kwargs = state["method"], state["args"], state["kwargs"]
                        try:
                            state["args"], ret = get_explicit_arguments(method, args, kwargs, system)
                            if ret:
                                state["return"] = ret
                        except ArgConflictException:
                            state["args"] = [get_arguments(method, args, system)]
                        methods[state["method"]] = state
                    else:
                        process_methodparse(state, line)

    # Process source file
    do_file(path)

    # Process related file, grab methods and whatnot.
    for rel in related:
        for h in headers:
            if rel in h:
                do_file(h)

    if NONLOCALARGH["ignore_file"] or NONLOCALARGH["classname"] == "?":
        return system, None

    classinfo = {}
    classinfo['options'] = options
    classinfo['related'] = related
    classinfo['comments'] = comments
    classinfo['detail_comments'] = comments
    classinfo['used_methods'] = used_methods
    classinfo['used_operators'] = used_operators
    classinfo['used_properties'] = used_properties
    classinfo['methods'] = methods
    classinfo['classname'] = NONLOCALARGH["classname"]
    classinfo['namespace'] = NONLOCALARGH["namespace"]
    return system, classinfo

def process_project(project, source, headers):
    objinfo = {}
    for f in source:
        #if not "_QuadB" in f: continue
        #if not "_Color" in f: continue
        system, p = process_file(f, headers)
        if p: objinfo[system] = p
    return objinfo

#
# Output
#

def output_metadata(outdir, project, objinfo):
    import json
    namespaces = {}
    for cls, obj in objinfo.items():
        ns, name = cls.split(".")
        if obj["namespace"] != "?":
            ns = obj["namespace"] 
        obj["system"] = cls
        namespaces.setdefault(ns, {}).setdefault(name, []).append(obj)

    for ns, objs in namespaces.items():
        with open(os.path.join(outdir, ns + ".meta.json"), "w") as f:
            f.write(json.dumps({ns: objs}, indent=4, separators=(',', ': ')))

#
# Source file finder
#

def find_source_files(path):
    rets = []
    reth = []
    for root, dirs, files in os.walk(path):
        for fn in files:
            fp = os.path.join(root, fn)
            ext = os.path.splitext(fn)[1]
            if ext in (".cpp", ".cxx"):
                rets.append(fp)
            elif ext in (".h"):
                reth.append(fp)
    return rets, reth


def main(args):
    import shutil
    outdir = "output"
    if len(args) > 0:
        outdir = args[0]

    #if os.path.exists(outdir):
    #    shutil.rmtree(outdir)
    
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    
    for path in API_PATHS:
        name = os.path.basename(path)
        files, headers = find_source_files(path)
        objinfo = process_project(name, files, headers)
        #output_project(outdir, name, objinfo)
        output_metadata(outdir, name, objinfo)

if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) == 1 and args[0] == '':
        args = []
    main(args)
