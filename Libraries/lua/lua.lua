Import("../../Builder.lua")

library = NewLibrary("lua")
--library:AddDefine("LUA_COMPAT_MODULE")

library:AddIncludeDirectory(".")

-- use luajit if it exists
-- (configure to compile as static library, move to project directory)
if file_exists("libluajit.a") or file_exists("luajit.lib") then
	library:AddSystemLibrary("luajit")
	library:AddIncludeDirectory("luajit/src")
	library:AddSourceDirectory("compat_luajit.c")
	useluajit = true
else
	-- otherwise use standard lua
	library:AddIncludeDirectory("sdk/include")
	library:AddIncludeDirectory("sdk/source")
	library:AddSourceDirectory("sdk/src/*.c")
end

library:AddIncludeDirectory("luasocket/include")
library:AddSourceDirectory("luasocket/source/*.c")
if family == "windows" then
	library:AddDefine("LUASOCKET_API=__declspec(dllexport)")
	library:AddSystemLibrary("wsock32")
	library:AddSystemLibrary("ws2_32")
	library:AddSourceDirectory("luasocket/source/windows/*.c")
else
    library:AddDefine("LUASOCKET_API=")
    library:AddSourceDirectory("luasocket/source/unix/*.c")
end
library:AddDefine("LUA_USE_APICHECK")
