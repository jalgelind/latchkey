
#pragma once
#include "lua.h"
#if LUA_VERSION_NUM == 501

void *luaL_testudata (lua_State *L, int ud, const char *tname);

LUA_API lua_Number lua_tonumberx (lua_State *L, int idx, int *isnum);
LUA_API lua_Integer lua_tointegerx (lua_State *L, int idx, int *isnum) ;


# define lua_rawlen lua_objlen
#define LUA_OK          0

LUA_API void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup);

LUA_API lua_Integer lua_tointegerx (lua_State *L, int idx, int *isnum);

#endif