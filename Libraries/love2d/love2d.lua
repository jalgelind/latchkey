Import("../../Builder.lua")

library = NewLibrary("love2d")

--library:RequireLibrary("lua")
--library:RequireLibrary("box2d")
if file_exists("libluajit.a") or file_exists("luajit.lib") then
	library:AddIncludeDirectory("../lua/luajit/src")
else
	library:AddIncludeDirectory("../lua/sdk/include")
end
library:AddIncludeDirectory("../box2d/Box2D/")

library:AddIncludeDirectory("src/")
library:AddSourceDirectoryR("src/*.cpp")
