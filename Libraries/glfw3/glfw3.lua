Import("../../Builder.lua")


if family == "unix" then
    if platform == "macosx" then
        prefix = "cocoa_"
    else
        prefix = "x11_"
    end
elseif family == "windows" then
    prefix = "win32_"
end

library = NewLibrary("glfw3")

--library:AddDefine("GLFW_VERSION_MAJOR", "3")
--library:AddDefine("GLFW_VERSION_MINOR", "0")
library:AddDefine("GLFW_VERSION_PATCH", "0")
library:AddDefine("GLFW_VERSION_EXTRA", "")
library:AddDefine("GLFW_VERSION", "3.0")
library:AddDefine("_GLFW_VERSION_FULL", "3.0.0")

library:AddDefine("_GLFW_USE_OPENGL")

if family == "unix" then
    if platform == "macosx" then
        library:AddDefine("_GLFW_USE_CHDIR")
        library:AddDefine("_GLFW_USE_MENUBAR")
        library:AddDefine("_GLFW_COCOA")
        library:AddDefine("_GLFW_NSGL")
        library:AddSystemFramework("IOKit")
        library:AddSystemFramework("OpenGL")
        library:AddSystemFramework("Cocoa")
        library:AddSystemFramework("CoreFoundation")
        --library:AddSourceDirectory("src/nsgl_*.c")
    else
        library:AddDefine("_GLFW_HAS_GLXGETPROCADDRESS")
        library:AddDefine("_GLFW_HAS_XRANDR")
        library:AddDefine("_GLFW_X11")
        library:AddDefine("_GLFW_GLX")
        library:AddSystemLibrary("X11")
        library:AddSystemLibrary("Xxf86vm")
        library:AddSystemLibrary("Xi")
        library:AddSystemLibrary("Xrandr")
        library:AddSystemLibrary("GLU")
        library:AddSystemLibrary("GL")
        library:AddSourceDirectory("src/glx_*.c")
    end
elseif family == "windows" then
        library:AddDefine("_GLFW_WIN32", 1)
        library:AddDefine("_GLFW_WGL", 1)
        library:AddDefine("_GLFW_NO_DLOAD_WINMM")
        library:AddSystemLibrary("opengl32")
        library:AddSystemLibrary("glu32")
        library:AddSystemLibrary("gdi32")
        library:AddSystemLibrary("user32")
        library:AddSystemLibrary("ole32")
        library:AddSystemLibrary("Shell32")
        library:AddSystemLibrary("winmm")
        library:AddSourceDirectory("src/wgl_*.c")
end

library:AddIncludeDirectory("include/")
library:AddIncludeDirectory("include/GL")
library:AddIncludeDirectory("src/")

library:AddSourceDirectory("src/" .. prefix .."*.c")
library:AddSourceDirectory("src/clipboard.c")
library:AddSourceDirectory("src/context.c")
library:AddSourceDirectory("src/gamma.c")
library:AddSourceDirectory("src/init.c")
library:AddSourceDirectory("src/input.c")
library:AddSourceDirectory("src/joystick.c")
library:AddSourceDirectory("src/monitor.c")
library:AddSourceDirectory("src/time.c")
library:AddSourceDirectory("src/window.c")
