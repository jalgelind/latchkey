Download the vstsdk and put the following folders in this directory:

public.sdk/
pluginterfaces/


If you get errors about multiple symbols for _DllMain, then
open 'vst2sdk\public.sdk\source\vst2.x\vstplugmain.cpp'
and delete or put a multi-line comment around the following at the end of the file:

#if WIN32
#include <windows.h>
void* hInstance;

extern "C" {
BOOL WINAPI DllMain (HINSTANCE hInst, DWORD dwReason, LPVOID lpvReserved)
{
	hInstance = hInst;
	return 1;
}
} // extern "C"
#endif