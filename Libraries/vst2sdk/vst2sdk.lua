Import("../../Builder.lua")

library = NewLibrary("vst2sdk")

library:AddIncludeDirectory("public.sdk/source/vst2.x")
library:AddIncludeDirectory("pluginterfaces/vst2.x")
library:AddIncludeDirectory("./")

library:AddSourceDirectory("public.sdk/source/vst2.x/audioeffect.cpp")
library:AddSourceDirectory("public.sdk/source/vst2.x/audioeffectx.cpp")
library:AddSourceDirectory("public.sdk/source/vst2.x/vstplugmain.cpp")