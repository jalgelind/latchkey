Import("../../Builder.lua")

library = NewLibrary("box2d")
library:AddIncludeDirectory("Box2D/")
library:AddIncludeDirectory("./")
library:AddSourceDirectoryR("Box2D/*.cpp")
