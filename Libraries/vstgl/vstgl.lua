Import("../../Builder.lua")

library = NewLibrary("vstgl")

if family == "unix" then
    if platform == "macosx" then
        library:AddSystemFramework("AGL")
        library:AddSystemFramework("OpenGL")
        library:AddSystemFramework("Carbon")
        library:AddIncludeDirectory("source/macosx")
        library:AddSourceDirectory("source/macosx/*.cpp")
    else
        library:AddSystemLibrary("X11")
        library:AddSystemLibrary("GLU")
        library:AddSystemLibrary("GL")
        library:AddSystemLibrary("rt")
    end
elseif family == "windows" then
        library:AddSystemLibrary("opengl32")
        library:AddSystemLibrary("glu32")
        library:AddSystemLibrary("gdi32")
        library:AddSystemLibrary("user32")
        library:AddSystemLibrary("ole32")
        library:AddIncludeDirectory("source/win32")
        library:AddSourceDirectory("source/win32/*.cpp")
end

library:AddIncludeDirectory("source/")