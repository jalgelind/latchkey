//	VSTGLTimer.cpp - Simple message-based timer class.
//	---------------------------------------------------------------------------
//	Copyright (c) 2005-2006 Niall Moody
//	
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the "Software"),
//	to deal in the Software without restriction, including without limitation
//	the rights to use, copy, modify, merge, publish, distribute, sublicense,
//	and/or sell copies of the Software, and to permit persons to whom the
//	Software is furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//	DEALINGS IN THE SOFTWARE.
//	---------------------------------------------------------------------------

#include "VSTGLTimer.h"

Timer::Timer(int interval):
timerInterval(interval),
running(false)
{
	
}

//-----------------------------------------------------------------------------
Timer::~Timer()
{
	if(running)
		stop();
}

//-----------------------------------------------------------------------------
bool Timer::start()
{
	bool retval = false;

	if(running)
		return false;

	OSStatus err;

	err = InstallEventLoopTimer(GetCurrentEventLoop(),
								1,
								(static_cast<double>(timerInterval)/1000.0),
								NewEventLoopTimerUPP(timerProc),
								this,
								&timer);
	if(err == noErr)
		retval = true;

	if(retval)
		running = true;

	return retval;
}

//-----------------------------------------------------------------------------
void Timer::stop()
{
	if(running)
	{
		RemoveEventLoopTimer(timer);
		running = false;
	}
}

//-----------------------------------------------------------------------------
pascal void Timer::timerProc(EventLoopTimerRef theTimer, void *userData)
{
	Timer *tim = static_cast<Timer *>(userData);

	if(tim)
		tim->timerCallback();
}