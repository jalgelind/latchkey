//	VSTGLEditor.cpp - Editor window for a VST plugin using OpenGL to handle
//					  all the drawing.
//	---------------------------------------------------------------------------
//	Copyright (c) 2005-2006 Niall Moody
//
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the "Software"),
//	to deal in the Software without restriction, including without limitation
//	the rights to use, copy, modify, merge, publish, distribute, sublicense,
//	and/or sell copies of the Software, and to permit persons to whom the
//	Software is furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//	DEALINGS IN THE SOFTWARE.
//	---------------------------------------------------------------------------

#include <GL/glew.h>
#include <gl/wglew.h>
#include "VSTGLEditor.h"
#include "audioeffect.h"

#include <cstdio>

//-----------------------------------------------------------------------------
VSTGLEditor::VSTGLEditor(AudioEffect *effect, WindowFlags flags):
AEffEditor(effect),
useVSync(true),
antialiasing(0)
{
	char tempstr[32];

	effect->setEditor(this);

	_rect.left = 0;
	_rect.top = 0;
	_rect.right = 0;
	_rect.bottom = 0;

	if((flags&WaitForVerticalSync) == WaitForVerticalSync)
		useVSync = true;
	if((flags&Antialias6x) == Antialias6x)
		antialiasing = 2;
	else if((flags&Antialias4x) == Antialias4x)
		antialiasing = 4;
	else if((flags&Antialias2x) == Antialias2x)
		antialiasing = 6;

	context = NULL;
	pixels = NULL;
	boundsX = 0;
	boundsY = 0;

	//Register our dummy HIView object.
	//Not sure that all of these are needed, but I don't want to risk breaking
	//anything.
	EventTypeSpec eventList[] = {{kEventClassControl, kEventControlBoundsChanged},
								 {kEventClassControl, kEventControlHitTest},
								 {kEventClassControl, kEventControlDraw},
								 {kEventClassMouse, kEventMouseWheelMoved},
								 {kEventClassControl, kEventControlClick},
								 {kEventClassControl, kEventControlTrack},
								 {kEventClassControl, kEventControlContextualMenuClick},
								 {kEventClassKeyboard, kEventRawKeyRepeat},
								 {kEventClassControl, kEventControlDragEnter},
								 {kEventClassControl, kEventControlDragWithin},
								 {kEventClassControl, kEventControlDragLeave},
								 {kEventClassControl, kEventControlDragReceive},
								 {kEventClassControl, kEventControlGetClickActivation},
								 {kEventClassControl, kEventControlGetOptimalBounds},
								 {kEventClassScrollable, kEventScrollableGetInfo},
								 {kEventClassScrollable, kEventScrollableScrollTo},
								 {kEventClassControl, kEventControlSetFocusPart},
								 {kEventClassControl, kEventControlGetFocusPart}};
	sprintf(tempstr, "com.vstgl.%08x", reinterpret_cast<int>(this));
	classId = CFStringCreateWithCString(NULL, tempstr, 0);
	controlSpec.defType = kControlDefObjectClass;
	controlSpec.u.classRef = NULL;
	ToolboxObjectClassRef controlClass = NULL;

	OSStatus status = RegisterToolboxObjectClass(classId,
												 NULL,
												 GetEventTypeCount (eventList),
												 eventList,
												 macEventHandler,
												 this,
												 &controlClass);
	if (status == noErr)
		controlSpec.u.classRef = controlClass;
}

//-----------------------------------------------------------------------------
VSTGLEditor::~VSTGLEditor()
{
	//Unregister our HIView object.
	UnregisterToolboxObjectClass((ToolboxObjectClassRef)controlSpec.u.classRef);
}

//-----------------------------------------------------------------------------
bool VSTGLEditor::open(void *ptr)
{
	AEffEditor::open(ptr);
	createWindow();

	WindowAttributes attr;
	HIRect bounds;
	window = static_cast<WindowRef>(ptr);

	//If the host's provided us with a composited window, add an HIView to
	//the window, since this is what the latest vstsdk requires.
	GetWindowAttributes((WindowRef)window, &attr);
	if(attr & kWindowCompositingAttribute)
	{
		HIViewRef contentView;
		if(HIViewFindByID(HIViewGetRoot((WindowRef)window),
						  kHIViewWindowContentID,
						  &contentView) == noErr)
		{
			Rect r = {0,
					  0,
					  (_rect.right-_rect.left),
					  (_rect.bottom-_rect.top)};

			//Create our control/HIView
			CreateCustomControl(NULL, &r, &controlSpec, NULL, &controlRef);

			//Are these necessary?
			SetControlDragTrackingEnabled(controlRef, true);
			SetAutomaticControlDragTrackingEnabledForWindow(window, true);

			//Search for parent HIView.
			if(HIViewFindByID(HIViewGetRoot(window),
							  kHIViewWindowContentID,
							  &contentView) == noErr)
			{
				//Add our HIView to the parent.
				HIViewAddSubview (contentView, controlRef);

				//Set our HIView's size.
				//Is all of this necessary?
				HIViewGetFrame(controlRef, &bounds);
				HIViewConvertPoint(&(bounds.origin), contentView, controlRef);
				bounds.size.width = _rect.right-_rect.left;
				bounds.size.height = _rect.bottom-_rect.top;
				HIViewSetFrame(controlRef, &bounds);

				//Install our extra event handler on the window.
				//(because we never seem to receive these events if we try to
				//install them on the control)
				EventHandlerUPP eventHandler;
				EventTypeSpec eventList[] = {{kEventClassMouse, kEventMouseUp},
											 {kEventClassMouse, kEventMouseMoved},
											 {kEventClassMouse, kEventMouseDragged},
											 {kEventClassKeyboard, kEventRawKeyDown},
											 {kEventClassKeyboard, kEventRawKeyUp}};

				eventHandler = NewEventHandlerUPP(macEventHandler);
				InstallWindowEventHandler(window,
										  eventHandler,
										  GetEventTypeCount(eventList),
										  eventList,
										  this,
										  &eventHandlerRef);
			}
		}
	}
	else
	{
		//If it's not a compositing window, we don't need to add a dummy
		//HIView, so we install our event handler on the window itself.
		EventHandlerUPP eventHandler;
		EventTypeSpec eventList[] = {{kEventClassMouse, kEventMouseDown},
									 {kEventClassMouse, kEventMouseUp},
									 {kEventClassMouse, kEventMouseMoved},
									 {kEventClassMouse, kEventMouseDragged},
									 {kEventClassMouse, kEventMouseWheelMoved},
									 {kEventClassKeyboard, kEventRawKeyDown},
									 {kEventClassKeyboard, kEventRawKeyUp}};

		eventHandler = NewEventHandlerUPP(macEventHandler);
		InstallWindowEventHandler(window,
								  eventHandler,
								  GetEventTypeCount(eventList),
								  eventList,
								  this,
								  &eventHandlerRef);
	}

	//Set up the AGL control.
	if(!antialiasing)
	{
		GLint attributes[] = {AGL_RGBA,
							  AGL_ACCELERATED,
							  AGL_DOUBLEBUFFER,
							  AGL_DEPTH_SIZE, 32,
							  AGL_NONE};

		pixels = aglChoosePixelFormat(NULL, 0, attributes);
	}
	else //If we want antialiasing.
	{
		GLint attributes[] = {AGL_RGBA,
							  AGL_ACCELERATED,
							  AGL_DOUBLEBUFFER,
							  AGL_DEPTH_SIZE, 32,
							  AGL_SAMPLE_BUFFERS_ARB, 1, 
							  AGL_SAMPLES_ARB, antialiasing, 
							  AGL_NO_RECOVERY,
							  AGL_NONE};

		pixels = aglChoosePixelFormat(NULL, 0, attributes);

		//Check it worked, get a pixel format without antialiasing if it
		//didn't.
		if(!pixels)
		{
			GLint attributes[] = {AGL_RGBA,
								  AGL_ACCELERATED,
								  AGL_DOUBLEBUFFER,
								  AGL_DEPTH_SIZE, 32,
								  AGL_NONE};

			pixels = aglChoosePixelFormat(NULL, 0, attributes);
		}
	}
	if(pixels)
		context = aglCreateContext(pixels, NULL);
	else
		return false;
	if(context)
	{
		if(!aglSetDrawable(context, GetWindowPort(window)))
			return false;
		if(!aglSetCurrentContext(context))
			return false;
	}
	else
		return false;

	//If one of the antialiasing options was specified in the constructor,
	//try to switch it on.


	if(antialiasing)
		setupAntialiasing();

	//If WaitForVerticalSync was specified in the constructor, try to switch
	//it on.


	if(useVSync)
		setupVSync();

	guiOpen();

	return true;
}

//-----------------------------------------------------------------------------
void VSTGLEditor::close()
{
	setupContext();
	guiClose();
	swapBuffers();

	aglSetCurrentContext(NULL);
	aglSetDrawable(context, NULL);
	if(context)
	{
		aglDestroyContext(context);
		context = NULL;
	}
	if(pixels)
	{
		aglDestroyPixelFormat(pixels);
		pixels = NULL;
	}

	HIViewRemoveFromSuperview(controlRef);

	if(controlRef)
		DisposeControl(controlRef);
}

//-----------------------------------------------------------------------------
void VSTGLEditor::refreshGraphics()
{
	// My gosh, setting up the context every frame? really?
	// FIXME: http://www.kvraudio.com/forum/printview.php?t=346284&start=0
	setupContext();
	draw();
	swapBuffers();
}

//-----------------------------------------------------------------------------
void VSTGLEditor::updateBounds(int x, int y)
{
	int tempHeight;
	Rect tempRect2;
	HIRect tempRect;

	//Get the new bounds of our dummy HIView.
	HIViewGetFrame(controlRef, &tempRect);

	//Invert tempRect's y-axis (honestly, who designed this API!).
	GetWindowBounds(window, kWindowContentRgn, &tempRect2);
	tempHeight = (tempRect2.bottom-tempRect2.top);
	tempRect.origin.y = tempHeight - tempRect.origin.y - tempRect.size.height;

	boundsX = (int)tempRect.origin.x;
	boundsY = (int)tempRect.origin.y;
}

//-----------------------------------------------------------------------------
//This is only necessary in Windows (and only really for Tracktion) - the
//window will have already been constructed for us on OS X.
void VSTGLEditor::createWindow()
{
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setupVSync()
{
	GLint swap = 1;
	//Some things are so much easier on OSX...
	aglSetInteger(context, AGL_SWAP_INTERVAL, &swap);
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setupAntialiasing()
{
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setupContext()
{
	GLint tempBounds[4];

    tempBounds[0] = boundsX;
    tempBounds[1] = boundsY;
	tempBounds[2] = (_rect.right-_rect.left);
    tempBounds[3] = (_rect.bottom-_rect.top);

	//Move the port to the correct offset.
	aglSetInteger(context, AGL_BUFFER_RECT, tempBounds);
    aglEnable(context, AGL_BUFFER_RECT);

	//Also necessary for Bidule.
	aglSetCurrentContext(context);
}

//-----------------------------------------------------------------------------
void VSTGLEditor::swapBuffers()
{
	aglSwapBuffers(context);
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setRect(int x, int y, int width, int height)
{
	_rect.left = x;
	_rect.top = y;
	_rect.right = x+width;
	_rect.bottom = y+height;
}

//-----------------------------------------------------------------------------
pascal OSStatus VSTGLEditor::macEventHandler(EventHandlerCallRef handler,
											 EventRef event,
											 void *userData)
{
	int width;
	int height;
	int tempHeight;
	Rect tempRect;
	int actualButton;
	EventMouseButton button;
	HIPoint location;
	EventMouseWheelAxis wheelAxis;
	SInt32 wheelDelta;
	UInt32 key;
	VstKeyCode tempKey;
	WindowRef win;
	Rect titleBounds;

	OSStatus result = eventNotHandledErr;
	VSTGLEditor *ed = static_cast<VSTGLEditor *>(userData);
	UInt32 eventClass = GetEventClass(event);
	UInt32 eventType = GetEventKind(event);

	if(ed)
	{
		if(eventClass == kEventClassMouse)
		{
			switch(eventType)
			{
				case kEventMouseDown:
					GetEventParameter(event,
									 kEventParamMouseButton,
									 typeMouseButton,
									 NULL,
									 sizeof(EventMouseButton),
									 NULL,
									 &button);
					GetEventParameter(event,
									 kEventParamWindowMouseLocation,
									 typeHIPoint,
									 NULL,
									 sizeof(HIPoint),
									 NULL,
									 &location);
					switch(button)
					{
						case kEventMouseButtonPrimary:
							actualButton = 1;
							break;
						case kEventMouseButtonSecondary:
							actualButton = 2;
							break;
						case kEventMouseButtonTertiary:
							actualButton = 3;
							break;
						default:
							actualButton = -1;
					}

					//Make sure we're not picking up a mouse click on the
					//title bar.
					win = ed->getWindowRef();
					GetWindowBounds(win, kWindowTitleBarRgn, &titleBounds);
					location.y -= (titleBounds.bottom - titleBounds.top);
					if(location.y > -1)
					{
						if(actualButton != -1)
							ed->onMouseDown(actualButton,
											static_cast<int>(location.x),
											static_cast<int>(location.y));
					}
					else
					{
						//So we can still click on the title bar.
						CallNextEventHandler(handler, event);
					}
					result = noErr;
					break;
				case kEventMouseUp:
					GetEventParameter(event,
									  kEventParamMouseButton,
									  typeMouseButton,
									  NULL,
									  sizeof(EventMouseButton),
									  NULL,
									  &button);
					GetEventParameter(event,
									  kEventParamWindowMouseLocation,
									  typeHIPoint,
									  NULL,
									  sizeof(HIPoint),
									  NULL,
									  &location);
					switch(button)
					{
						case kEventMouseButtonPrimary:
							actualButton = 1;
							break;
						case kEventMouseButtonSecondary:
							actualButton = 2;
							break;
						case kEventMouseButtonTertiary:
							actualButton = 3;
							break;
						default:
							actualButton = -1;
					}

					//Make sure we're not picking up a mouse click on the
					//title bar.
					win = ed->getWindowRef();
					GetWindowBounds(win, kWindowTitleBarRgn, &titleBounds);
					location.y -= (titleBounds.bottom - titleBounds.top);
					if(location.y > -1)
					{
						
						if(actualButton != -1)
						{
							ed->onMouseUp(actualButton,
										  static_cast<int>(location.x),
										  static_cast<int>(location.y));
						}
					}
					else
					{
						//So we can still click on the title bar.
						CallNextEventHandler(handler, event);
					}
					result = noErr;
					break;
				case kEventMouseMoved:
					width = ed->getWidth();
					height = ed->getHeight();

					GetEventParameter(event,
									  kEventParamWindowMouseLocation,
									  typeHIPoint,
									  NULL,
									  sizeof(HIPoint),
									  NULL,
									  &location);

					//Make sure we're not picking up a mouse click on the
					//title bar.
					win = ed->getWindowRef();
					GetWindowBounds(win, kWindowContentRgn, &tempRect);
					GetWindowBounds(win, kWindowTitleBarRgn, &titleBounds);
					location.y -= (titleBounds.bottom - titleBounds.top);
					if(location.y > -1)
					{
						tempHeight = (tempRect.bottom-tempRect.top);
						location.y -= tempHeight - ed->getBoundsY() - ed->getHeight();
						if(location.y < height)
						{
							location.x -= ed->getBoundsX();
							if((location.x > -1) && (location.x < width))
							{
								ed->onMouseMove(static_cast<int>(location.x),
												static_cast<int>(location.y));
							}
						}
					}
					result = noErr;
					break;
				//We handle dragging as well as moving events, because OS X
				//makes a specific distinction between them, whereas I
				//prefer to use the mouseUp and mouseDown events to tell
				//whether I'm supposed to be dragging...
				case kEventMouseDragged:
					width = ed->getWidth();
					height = ed->getHeight();

					GetEventParameter(event,
									  kEventParamWindowMouseLocation,
									  typeHIPoint,
									  NULL,
									  sizeof(HIPoint),
									  NULL,
									  &location);

					//Make sure we're not picking up a mouse click on the
					//title bar.
					win = ed->getWindowRef();
					GetWindowBounds(win, kWindowContentRgn, &tempRect);
					GetWindowBounds(win, kWindowTitleBarRgn, &titleBounds);
					location.y -= (titleBounds.bottom - titleBounds.top);
					if(location.y > -1)
					{
						tempHeight = (tempRect.bottom-tempRect.top);
						location.y -= tempHeight - ed->getBoundsY() - ed->getHeight();
						if(location.y < height)
						{
							location.x -= ed->getBoundsX();
							if((location.x > -1) && (location.x < width))
							{
								ed->onMouseMove(static_cast<int>(location.x),
												static_cast<int>(location.y));
							}
						}
					}
					result = noErr;
					break;
				case kEventMouseWheelMoved:
					GetEventParameter(event,
									  kEventParamMouseWheelAxis,
									  typeMouseWheelAxis,
									  NULL,
									  sizeof(EventMouseWheelAxis),
									  NULL,
									  &wheelAxis);
					GetEventParameter(event,
									  kEventParamMouseWheelDelta,
									  typeLongInteger,
									  NULL,
									  sizeof(SInt32),
									  NULL,
									  &wheelDelta);
					GetEventParameter(event,
									  kEventParamWindowMouseLocation,
									  typeHIPoint,
									  NULL,
									  sizeof(HIPoint),
									  NULL,
									  &location);

					//Make sure we're not picking up a mouse click on the
					//title bar.
					win = ed->getWindowRef();
					GetWindowBounds(win, kWindowTitleBarRgn, &titleBounds);
					location.y -= (titleBounds.bottom - titleBounds.top);
					if(location.y > -1)
					{
						if(wheelAxis == kEventMouseWheelAxisY)
						{
							ed->onMouseWheel(static_cast<int>(wheelDelta),
											 static_cast<int>(location.x),
											 static_cast<int>(location.y));
						}
					}
					result = noErr;
					break;
			}
		}
		else if(eventClass == kEventClassKeyboard)
		{
			switch(eventType)
			{
				case kEventRawKeyDown:
					GetEventParameter(event,
									  kEventParamKeyCode,
									  typeUInt32,
									  NULL,
									  sizeof(UInt32),
									  NULL,
									  &key);
					tempKey.character = key;
					ed->onGLKeyDown(tempKey);
					result = noErr;
					break;
				case kEventRawKeyUp:
					GetEventParameter(event,
									  kEventParamKeyCode,
									  typeUInt32,
									  NULL,
									  sizeof(UInt32),
									  NULL,
									  &key);
					tempKey.character = key;
					ed->onGLKeyUp(tempKey);
					result = noErr;
					break;
			}
		}
		else if((eventClass == kEventClassControl)||(eventClass == kEventClassWindow))
		{
			switch(eventType)
			{
				case kEventWindowBoundsChanged:
				case kEventControlBoundsChanged:
					Rect tempRect;
					OSStatus err;
					err = GetEventParameter(event,
									  kEventParamCurrentBounds, 
									  typeQDRectangle,
									  NULL,
									  sizeof(Rect),
									  NULL,
									  &tempRect);
					if(err == noErr)
						ed->updateBounds(tempRect.left, tempRect.top);
					else
						ed->updateBounds(0, 0);
					result = noErr;
					break;
				case kEventControlHitTest:
					{
						ControlPartCode part = 127;
						SetEventParameter(event, kEventParamControlPart, typeControlPartCode, sizeof(part), &part);
					}
					result = noErr;
					break;
				case kEventControlClick:
					GetEventParameter(event,
									 kEventParamMouseButton,
									 typeMouseButton,
									 NULL,
									 sizeof(EventMouseButton),
									 NULL,
									 &button);
					GetEventParameter(event,
									 kEventParamWindowMouseLocation,
									 typeHIPoint,
									 NULL,
									 sizeof(HIPoint),
									 NULL,
									 &location);
					switch(button)
					{
						case kEventMouseButtonPrimary:
							actualButton = 1;
							break;
						case kEventMouseButtonSecondary:
							actualButton = 2;
							break;
						case kEventMouseButtonTertiary:
							actualButton = 3;
							break;
						default:
							actualButton = -1;
					}

					//Make sure we're not picking up a mouse click on the
					//title bar.
					if(actualButton != -1)
					{
						ed->onMouseDown(actualButton,
										static_cast<int>(location.x),
										static_cast<int>(location.y));
					}
					result = noErr;
					break;
				case kEventControlContextualMenuClick:
					GetEventParameter(event,
									 kEventParamMouseButton,
									 typeMouseButton,
									 NULL,
									 sizeof(EventMouseButton),
									 NULL,
									 &button);
					GetEventParameter(event,
									 kEventParamWindowMouseLocation,
									 typeHIPoint,
									 NULL,
									 sizeof(HIPoint),
									 NULL,
									 &location);
					switch(button)
					{
						case kEventMouseButtonPrimary:
							actualButton = 1;
							break;
						case kEventMouseButtonSecondary:
							actualButton = 2;
							break;
						case kEventMouseButtonTertiary:
							actualButton = 3;
							break;
						default:
							actualButton = -1;
					}

					//Make sure we're not picking up a mouse click on the
					//title bar.
					if(actualButton != -1)
					{
						ed->onMouseDown(actualButton,
										static_cast<int>(location.x),
										static_cast<int>(location.y));
					}
					result = noErr;
					break;
				case kEventControlTrack:
					result = noErr;
					break;
				case kEventControlHiliteChanged:
					result = noErr;
					break;
				case kEventControlDraw:
					result = noErr;
					break;
				default:
					CallNextEventHandler(handler, event);
			}
		}
	}

	return result;
}