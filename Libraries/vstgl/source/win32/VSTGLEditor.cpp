//	VSTGLEditor.cpp - Editor window for a VST plugin using OpenGL to handle
//					  all the drawing.
//	---------------------------------------------------------------------------
//	Copyright (c) 2005-2006 Niall Moody
//
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the "Software"),
//	to deal in the Software without restriction, including without limitation
//	the rights to use, copy, modify, merge, publish, distribute, sublicense,
//	and/or sell copies of the Software, and to permit persons to whom the
//	Software is furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//	DEALINGS IN THE SOFTWARE.
//	---------------------------------------------------------------------------

#include <GL/glew.h>
#include <gl/wglew.h>
#include "VSTGLEditor.h"
#include "audioeffect.h"

#include <cstdio>

//This is the instance of the application, set in the main source file.
extern void* hInstance;

//Used to check/setup Vertical Sync.
typedef void (APIENTRY *PFNWGLEXTSWAPCONTROLPROC) (int);
typedef int (*PFNWGLEXTGETSWAPINTERVALPROC) (void);

//Used to check/setup Antialiasing.
typedef BOOL (WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats);

// Mouse hook fixing for windows
HHOOK MouseHook = 0L;
HHOOK KbdHook = 0L;
HWND g_Hwnd = 0L;

/*
struct MSLLHOOKSTRUCT 
{
	POINT     pt;
	DWORD     mouseData;
	DWORD     flags;
	DWORD     time;
	ULONG_PTR dwExtraInfo;
};
*/
LRESULT CALLBACK MouseProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx (MouseHook, nCode, wParam, lParam);

	if (wParam == 0x020a) // WM_MOUSEWHEEL
	{
		MSLLHOOKSTRUCT *hs = (MSLLHOOKSTRUCT*) lParam; 

		// get window under cursor 
		HWND hwnd = WindowFromPoint(hs->pt);

		if (hwnd!=0)
		{         
			HINSTANCE inst1 = (HINSTANCE)hInstance;
			HINSTANCE inst2 = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			if(inst1 == inst2)
			{
				return PostMessage(hwnd,  0x020a, hs->mouseData &0xffff0000, // low-word is undefined 
					(hs->pt.x&0xffff) | (hs->pt.y<<16)); 
			}
		}
	}
	return CallNextHookEx (MouseHook, nCode, wParam, lParam);
}

/*
struct KBDLLHOOKSTRUCT
{
	DWORD     vkCode;
	DWORD     scanCode;
	DWORD     flags;
	DWORD     time;
	ULONG_PTR dwExtraInfo;
};
*/
LRESULT CALLBACK KbdProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx (KbdHook, nCode, wParam, lParam);

	if (wParam == WM_KEYDOWN || wParam == WM_KEYUP)
	{
		
		KBDLLHOOKSTRUCT  *hs = (KBDLLHOOKSTRUCT *) lParam; 
		HWND hwnd = g_Hwnd;
		if (hwnd!=0)
		{         
			HINSTANCE inst1 = (HINSTANCE)hInstance;
			HINSTANCE inst2 = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			if(inst1 == inst2)
			{
				return PostMessage(hwnd,  wParam, hs->vkCode, 0); 
			}
		}
		
	}
	return CallNextHookEx (KbdHook, nCode, wParam, lParam);
}


VSTGLEditor::VSTGLEditor(AudioEffect *effect, WindowFlags flags)
	: AEffEditor(effect)
	, useVSync(true)
	, antialiasing(0)
{
	char tempstr[32];

	effect->setEditor(this);

	_rect.left = 0;
	_rect.top = 0;
	_rect.right = 0;
	_rect.bottom = 0;

	if((flags&WaitForVerticalSync) == WaitForVerticalSync)
		useVSync = true;
	if((flags&Antialias6x) == Antialias6x)
		antialiasing = 2;
	else if((flags&Antialias4x) == Antialias4x)
		antialiasing = 4;
	else if((flags&Antialias2x) == Antialias2x)
		antialiasing = 6;

	dc = 0;
	glRenderingContext = 0;

	//Register window class.
	WNDCLASSEX winClass;

	//most of this stuff is copied from VSTGUI
	winClass.cbSize = sizeof(WNDCLASSEX);
	winClass.style = CS_HREDRAW|CS_VREDRAW | CS_OWNDC;
	winClass.lpfnWndProc = GLWndProc;
	winClass.cbClsExtra = 0;
	winClass.cbWndExtra = 0;
	winClass.hInstance = static_cast<HINSTANCE>(hInstance);
	winClass.hIcon = 0;
	winClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	winClass.lpszMenuName = 0;
	//sprintf(tempstr, "VSTGLWindow%08x", static_cast<HINSTANCE>(hInstance));
	sprintf(tempstr, "VSTGLWindow%08x", reinterpret_cast<int>(this));
	winClass.lpszClassName = tempstr;
	winClass.hIconSm = NULL;

	//Register the window class (this is unregistered in the VSTGLEditor
	//destructor).
	RegisterClassEx(&winClass);
}

//-----------------------------------------------------------------------------
VSTGLEditor::~VSTGLEditor()
{
	char tempstr[32];

	//sprintf(tempstr, "VSTGLWindow%08x", static_cast<HINSTANCE>(hInstance));
	sprintf(tempstr, "VSTGLWindow%08x", reinterpret_cast<int>(this));
	//unregisters the window class
	UnregisterClass(tempstr, static_cast<HINSTANCE>(hInstance));
}

//-----------------------------------------------------------------------------
bool VSTGLEditor::open(void *ptr)
{
	AEffEditor::open(ptr);
	createWindow();
	int tempint;
	dc = GetDC(tempHWnd);

	//Have to set the pixel format first...
	pixelformat.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelformat.nVersion = 1;
    pixelformat.dwFlags = PFD_DRAW_TO_WINDOW |
						  PFD_SUPPORT_OPENGL |
						  PFD_DOUBLEBUFFER;
    pixelformat.iPixelType = PFD_TYPE_RGBA;
    pixelformat.cColorBits = 32;
    pixelformat.cRedBits = 0;
    pixelformat.cRedShift = 0;
    pixelformat.cGreenBits = 0;
    pixelformat.cGreenShift = 0;
    pixelformat.cBlueBits = 0;
    pixelformat.cBlueShift = 0;
    pixelformat.cAlphaBits = 0;
    pixelformat.cAlphaShift = 0;
    pixelformat.cAccumBits = 0;
    pixelformat.cAccumRedBits = 0;
    pixelformat.cAccumGreenBits = 0;
    pixelformat.cAccumBlueBits = 0;
    pixelformat.cAccumAlphaBits = 0;
    pixelformat.cDepthBits = 32;
    pixelformat.cStencilBits = 0;
    pixelformat.cAuxBuffers = 0;
    pixelformat.iLayerType = PFD_MAIN_PLANE;
    pixelformat.bReserved = 0;
    pixelformat.dwLayerMask = 0;
    pixelformat.dwVisibleMask = 0;
    pixelformat.dwDamageMask = 0;
	tempint = ChoosePixelFormat(dc, &pixelformat);
	SetPixelFormat(dc, tempint, &pixelformat);

	glRenderingContext = wglCreateContext(dc);
	wglMakeCurrent(dc, glRenderingContext);

	if(antialiasing)
		setupAntialiasing();

	if(useVSync)
		setupVSync();

	if (!MouseHook)
		MouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseProc, (HINSTANCE)hInstance, 0);
	//if (!KbdHook)
	//	KbdHook = SetWindowsHookEx(WH_KEYBOARD_LL, KbdProc, (HINSTANCE)hInstance, 0);

	guiOpen();

	return true;
}

//-----------------------------------------------------------------------------
void VSTGLEditor::close()
{
	setupContext();
	guiClose();
	swapBuffers();

	// Release mouse hook
	if (MouseHook)
	{
		UnhookWindowsHookEx(MouseHook);
		MouseHook = 0L;
	}

	if (KbdHook)
	{
		UnhookWindowsHookEx(KbdHook);
		KbdHook = 0L;
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(glRenderingContext);
	glRenderingContext = NULL;

	ReleaseDC(tempHWnd, dc);
	DestroyWindow(tempHWnd);
	tempHWnd = 0L;
	g_Hwnd = 0;
}

//-----------------------------------------------------------------------------
void VSTGLEditor::refreshGraphics()
{
	// FIXME: http://www.kvraudio.com/forum/printview.php?t=346284&start=0
	// Look at this too: https://github.com/olilarkin/wdl-ol/blob/master/WDL/IPlug/IGraphicsWin.cpp
	setupContext();
	draw();
	swapBuffers();
}

//-----------------------------------------------------------------------------
void VSTGLEditor::createWindow()
{
	char tempstr[32];
	HWND parentHWnd = static_cast<HWND>(systemWindow);

	//sprintf(tempstr, "VSTGLWindow%08x", static_cast<HINSTANCE>(hInstance));
	sprintf(tempstr, "VSTGLWindow%08x", reinterpret_cast<int>(this));
	tempHWnd = CreateWindowEx(0,					//extended window style
							  tempstr,				//pointer to registered class name
							  "VSTGLEditor",		//pointer to window name
							  WS_CHILD|
							  WS_VISIBLE,			//window style
							  0,					//horizontal position of window
							  0,					//vertical position of window
							  (_rect.right-_rect.left),//window width
							  (_rect.bottom-_rect.top),//window height
							  parentHWnd,			//handle to parent or owner window
							  NULL,					//handle to menu, or child-window identifier
							  (HINSTANCE)hInstance,	//handle to application instance
							  NULL);				//pointer to window-creation data
							  
	//This is so we can send messages to this object from the message loop.
	SetWindowLong(tempHWnd, GWL_USERDATA, (long)this);
	SetWindowLong(tempHWnd, GWL_HINSTANCE, (LONG)hInstance);
	g_Hwnd = tempHWnd;
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setupVSync()
{
	unsigned char *extensions;
	PFNWGLEXTSWAPCONTROLPROC wglSwapIntervalEXT = NULL;

	//Check the graphics card's extensions.
	extensions = const_cast<unsigned char *>(glGetString(GL_EXTENSIONS));
  
	//Check if extensions contains the vertical sync string.
	if(strstr(reinterpret_cast<char *>(extensions),"WGL_EXT_swap_control"))
	{
		//Get the address of the relevant functions.
		wglSwapIntervalEXT = reinterpret_cast<PFNWGLEXTSWAPCONTROLPROC>(wglGetProcAddress("wglSwapIntervalEXT"));

		//Turn it on.
		if(wglSwapIntervalEXT)
			wglSwapIntervalEXT(1);
	}
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setupAntialiasing()
{
	unsigned char *extensions;
	PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
	int pixelFormat;
	UINT numFormats;
	float fAttributes[] = {0,0};

	//Check the graphics card's extensions.
	extensions = const_cast<unsigned char *>(glGetString(GL_EXTENSIONS));
  
	//Check if extensions contains the multisample string.
	if(strstr(reinterpret_cast<char *>(extensions),"GL_ARB_multisample"))
	{
		//Get the address of the relevant functions.
		wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");

		if(wglChoosePixelFormatARB)
		{
			int iAttributes[] = {
				WGL_DRAW_TO_WINDOW_ARB,GL_TRUE,
				WGL_SUPPORT_OPENGL_ARB,GL_TRUE,
				WGL_ACCELERATION_ARB,WGL_FULL_ACCELERATION_ARB,
				WGL_COLOR_BITS_ARB,24,
				WGL_ALPHA_BITS_ARB,8,
				WGL_DEPTH_BITS_ARB,16,
				WGL_STENCIL_BITS_ARB,0,
				WGL_DOUBLE_BUFFER_ARB,GL_TRUE,
				WGL_SAMPLE_BUFFERS_ARB,GL_TRUE,
				WGL_SAMPLES_ARB, antialiasing,
				0,0};

			//Try and get the correct pixel format.
			if(wglChoosePixelFormatARB(dc,
									   iAttributes,
									   fAttributes,
									   1,
									   &pixelFormat,
									   &numFormats))
			{
				if(numFormats > 0)
				{
					//Destroy the current window.
					wglMakeCurrent(NULL, NULL);
					wglDeleteContext(glRenderingContext);
					ReleaseDC(tempHWnd, dc);
					DestroyWindow(tempHWnd);

					//Create a new one with the correct pixel format.
					createWindow();
					dc = GetDC(tempHWnd);
					SetPixelFormat(dc, pixelFormat, &pixelformat);
					glRenderingContext = wglCreateContext(dc);
					wglMakeCurrent(dc, glRenderingContext);
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setupContext()
{
	wglMakeCurrent(dc, glRenderingContext);
}

//-----------------------------------------------------------------------------
void VSTGLEditor::swapBuffers()
{
	SwapBuffers(dc);
}

//-----------------------------------------------------------------------------
void VSTGLEditor::setRect(int x, int y, int width, int height)
{
	_rect.left = x;
	_rect.top = y;
	_rect.right = x+width;
	_rect.bottom = y+height;
}

//-----------------------------------------------------------------------------


static unsigned int keycode_to_vstkey(unsigned int key)
{
	switch(key)
	{
		case VK_BACK: return VKEY_BACK;
		case VK_TAB: return VKEY_TAB;
		case VK_CLEAR: return VKEY_CLEAR;
		case VK_RETURN: return VKEY_RETURN;
		case VK_PAUSE: return VKEY_PAUSE;
		case VK_ESCAPE: return VKEY_ESCAPE;
		case VK_SPACE: return VKEY_SPACE;
		case VK_END: return VKEY_END;
		case VK_HOME: return VKEY_HOME;
		case VK_LEFT: return VKEY_LEFT;
		case VK_UP: return VKEY_UP;
		case VK_RIGHT: return VKEY_RIGHT;
		case VK_DOWN: return VKEY_DOWN;
		case VK_PRIOR: return VKEY_PAGEUP;
		case VK_NEXT: return VKEY_PAGEDOWN;
		case VK_SELECT: return VKEY_SELECT;
		case VK_PRINT: return VKEY_PRINT;
		case VK_SNAPSHOT: return VKEY_SNAPSHOT;
		case VK_INSERT: return VKEY_INSERT;
		case VK_DELETE: return VKEY_DELETE;
		case VK_HELP: return VKEY_HELP;
		case VK_NUMPAD0: return VKEY_NUMPAD0;
		case VK_NUMPAD1: return VKEY_NUMPAD1;
		case VK_NUMPAD2: return VKEY_NUMPAD2;
		case VK_NUMPAD3: return VKEY_NUMPAD3;
		case VK_NUMPAD4: return VKEY_NUMPAD4;
		case VK_NUMPAD5: return VKEY_NUMPAD5;
		case VK_NUMPAD6: return VKEY_NUMPAD6;
		case VK_NUMPAD7: return VKEY_NUMPAD7;
		case VK_NUMPAD8: return VKEY_NUMPAD8;
		case VK_NUMPAD9: return VKEY_NUMPAD9;
		case VK_MULTIPLY: return VKEY_MULTIPLY;
		case VK_ADD: return VKEY_ADD;
		case VK_SEPARATOR: return VKEY_SEPARATOR;
		case VK_SUBTRACT: return VKEY_SUBTRACT;
		case VK_DECIMAL: return VKEY_DECIMAL;
		case VK_DIVIDE: return VKEY_DIVIDE;
		case VK_F1: return VKEY_F1;
		case VK_F2: return VKEY_F2;
		case VK_F3: return VKEY_F3;
		case VK_F4: return VKEY_F4;
		case VK_F5: return VKEY_F5;
		case VK_F6: return VKEY_F6;
		case VK_F7: return VKEY_F7;
		case VK_F8: return VKEY_F8;
		case VK_F9: return VKEY_F9;
		case VK_F10: return VKEY_F10;
		case VK_F11: return VKEY_F11;
		case VK_F12: return VKEY_F12;
		case VK_NUMLOCK: return VKEY_NUMLOCK;
		case VK_SCROLL: return VKEY_SCROLL;
		case VK_LSHIFT: return VKEY_SHIFT;
		case VK_RSHIFT: return VKEY_SHIFT;
		case VK_LCONTROL: return VKEY_CONTROL;
		case VK_RCONTROL: return VKEY_CONTROL;
		case VK_MENU: return VKEY_ALT;
	}
	//else if (key == VK_EQUALS) return VKEY_EQUALS; // http://www.gamedev.net/topic/293214-virtual-keycode-hell---or-where-is-vk_equals/
	return 0;
	
}

bool VSTGLEditor::onKeyDown(VstKeyCode& keyCode)
{
	onGLKeyDown(keyCode);
	return true;
}

bool VSTGLEditor::onKeyUp(VstKeyCode& keyCode)
{
	onGLKeyUp(keyCode);
	return true;
}

LONG WINAPI VSTGLEditor::GLWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	VstKeyCode tempkey;
	VSTGLEditor *ed = reinterpret_cast<VSTGLEditor *>(GetWindowLong(hwnd, GWL_USERDATA));
	wParam = keycode_to_vstkey(wParam);
	if (ed)
	{
		// TODO: Fix weird casts.
		switch(message)
		{
		case WM_CREATE:
			SetFocus(hwnd);
			break;
		case WM_LBUTTONDOWN:
				SetCapture(hwnd);
				ed->onMouseDown(1,
					(int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case WM_MBUTTONDOWN:
				SetCapture(hwnd);
				ed->onMouseDown(3,
					(int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case WM_RBUTTONDOWN:
				SetCapture(hwnd);
				ed->onMouseDown(2,
					(int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case WM_LBUTTONUP:
				ReleaseCapture();
				ed->onMouseUp(1,
					(int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case WM_MBUTTONUP:
				ReleaseCapture();
				ed->onMouseUp(3,
					(int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case WM_RBUTTONUP:
				ReleaseCapture();
				ed->onMouseUp(2,
					(int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case 0x020E:
		case WM_MOUSEMOVE:
				ed->onMouseMove((int)(short)LOWORD(lParam),
					(int)(short)HIWORD(lParam));
			break;
		case WM_MOUSEWHEEL:
				ed->onMouseWheel(GET_WHEEL_DELTA_WPARAM(wParam), (int)(short)LOWORD(lParam), (int)(short)HIWORD(lParam));
			break;
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
				//This could be improved
				tempkey.character = wParam;
				ed->onGLKeyDown(tempkey);
			break;
		case WM_CHAR:
		case WM_SYSKEYUP:
		case WM_KEYUP:
				tempkey.character = wParam;
				ed->onGLKeyUp(tempkey);
			break;
		case WM_PAINT:
			ed->idle();
			break;
		}
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}
