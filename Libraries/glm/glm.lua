Import("../../Builder.lua")

library = NewLibrary("glm")
library:AddIncludeDirectory("include/")
library:AddSourceDirectory("source/*.cpp")
