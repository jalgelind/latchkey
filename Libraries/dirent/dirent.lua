Import("../../Builder.lua")

library = NewLibrary("dirent")

if family == "windows" then
    library:AddIncludeDirectory("include")
end

library:AddSourceDirectory("source/*.cpp")