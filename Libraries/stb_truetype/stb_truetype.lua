Import("../../Builder.lua")

library = NewLibrary("stb_truetype")

library:AddIncludeDirectory("sdk/include")
library:AddSourceDirectory("sdk/source/*.c")
