#vertexshader

void main()
{
    gl_Position = ftransform();
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_FrontColor = gl_Color;
}

#fragmentshader

uniform sampler2D tex;
const float distancethreshold = 0.5;
const float gamma = 2.2;

float aastep(float threshold, float dist)
{
    float afwidth = 0.7 * length(vec2(dFdx(dist), dFdy(dist)));
    return smoothstep(threshold - afwidth, threshold + afwidth, dist);
}

void main()
{
    if (gl_Color.a <= 0.0001)
        discard;
    float dist = texture2D(tex, gl_TexCoord[0].xy).a;
    float alpha = aastep(distancethreshold, dist);
    alpha = pow(alpha, 1.0/gamma);
    gl_FragColor = vec4(gl_Color.rgb, alpha);
}

