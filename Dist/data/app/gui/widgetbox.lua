require 'ext/class'

-- Widget encapsulation for supporting margins and padding.

widgetbox = class(function(self, baserect)
  if baserect == nil then
    self.inner_rect = rect.new(0, 0, 0, 0)  -- widget rect
  else
    self.inner_rect = rect.dup(baserect)  -- widget rect
  end
  self.outer_rect = rect.dup(self.inner_rect) -- with margin
  self.padded_rect = rect.dup(self.inner_rect) -- with padding
  self.margin = {top=0,bottom=0,left=0,right=0}
  self.padding = {top=0,bottom=0,left=0,right=0}
end)

function widgetbox:set(r)
  self.inner_rect = rect.dup(r)
  self:_updatebounds()
end

function widgetbox:setsize(w, h)
  self.inner_rect.w = w
  self.inner_rect.h = h
  self:_updatebounds()
end

function widgetbox:setpos(x, y)
  self.inner_rect.x = x
  self.inner_rect.y = y
  self:_updatebounds()
end

function widgetbox:translate(x, y)
  self.inner_rect.x = self.inner_rect.x + x
  self.inner_rect.y = self.inner_rect.y + y
  self:_updatebounds()
end

function widgetbox:_updatebounds()
  self.padded_rect = rect.dup(self.inner_rect)
  self.padded_rect.w = self.padded_rect.w + self.padding.left + self.padding.right
  self.padded_rect.h = self.padded_rect.h + self.padding.top + self.padding.bottom
  self.padded_rect.x = self.padded_rect.x + self.margin.left + (self.padding.left + self.padding.right)/2
  self.padded_rect.y = self.padded_rect.y + self.margin.top + (self.padding.top + self.padding.bottom) / 2

  self.outer_rect = rect.dup(self.padded_rect)
  self.outer_rect.w = self.outer_rect.w + self.margin.left + self.margin.right
  self.outer_rect.h = self.outer_rect.h + self.margin.top + self.margin.bottom
  self.outer_rect.x = self.inner_rect.x
  self.outer_rect.y = self.inner_rect.y
end

function widgetbox:setmargin(margin)
  self.margin = margin
  self:_updatebounds()
end

function widgetbox:setpadding(padding)
  self.padding = padding
  self:_updatebounds()
end

-- Get full outer bounds (+margin +padding)
function widgetbox:outer()
  return self.outer_rect
end

-- Get inner bounds (+padding)
function widgetbox:inner()
  return self.padded_rect
end

-- Get actual widget bounds
function widgetbox:bounds()
  return self.inner_rect
end

function widgetbox:print()
  print("inner, outer, padded")
  rect.print(self.inner_rect)
  rect.print(self.outer_rect)
  rect.print(self.padded_rect)
end
