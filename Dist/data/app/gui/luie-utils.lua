require 'gui/rect'

function subtex(texture, r)
    local w, h = texture:size()
    local a = 1. / w
    local b = 1. / h
    return rect.new(a*r.x, b*r.y, a*(r.x + r.w), b*(r.y + r.h))
end

function rgb(r, g, b)
    return r, g, b, 1.0
end

function rgba(r, g, b, a)
    return r, g, b, a
end

function torgba(...)
    function onerror(color)
        local fmtstr = "'%s' is not a supported color format"
        print(string.format(fmtstr, color))
        return nil
    end
    local argc = select('#', ...)
    if argc == 1 then
        local c = select(1, ...)
        if type(c) == 'string' then
            if #c > 0 and string.sub(c, 1, 1) == "#" then
                local s = string.sub(c, 2)
                if #s == 6 then
                    local r = tonumber(string.sub(s, 1, 2), 16)
                    local g = tonumber(string.sub(s, 3, 4), 16)
                    local b = tonumber(string.sub(s, 5, 6), 16)
                    return {r / 255, g / 255, b / 255, 1}
                elseif #s == 3 then
                    local r = tonumber(string.sub(s, 1, 1), 16)
                    local g = tonumber(string.sub(s, 2, 2), 16)
                    local b = tonumber(string.sub(s, 3, 3), 16)
                    return {r / 15, g / 15, b / 15, 1}
                else
                    return onerror(c)
                end
            end
        else
            return onerror(c)
        end
    -- todo: validate
    elseif argc == 3 then
        local r, g, b = ...
        return {r, g, b, 1}
    elseif argc == 4 then
        local r, g, b, a = ...
        return {r, g, b, a}
    end
end