--
-- luie the gui: default theme
--

require 'gui/luie-utils'
require 'gfx'

local theme = {}

-- todo: invalid textures should return the size of the
-- internal "error" image.
local texw, texh = 10, 10
theme.texture_image = "data/images/luie-default-theme.png"
theme.texture = theme.texture or gfx.texture(theme.texture_image)
if theme.texture:valid() then
	theme.texture:set("min", "nearest")
	theme.texture:set("mag", "nearest")
	texw, texh = theme.texture:size()
end

theme.font = theme.font or gfx.font("data/fonts/cantarell/Cantarell-Regular.ttf", 16)
--theme.font = theme.font or gfx.font("data/fonts/DroidSerif-Regular.ttf", 16)


theme.texturemap = {}
theme.texturemap.empty       = rect.new(7, 0, 1, 1)
theme.texturemap.corner_tl   = rect.new(0, 0, 2, 2)
theme.texturemap.corner_bl   = rect.new(0, 5, 2, 2)
theme.texturemap.corner_tr   = rect.new(5, 0, 2, 2)
theme.texturemap.corner_br   = rect.new(5, 5, 2, 2)
theme.texturemap.edge_left   = rect.new(0, 2, 1, 3)
theme.texturemap.edge_right  = rect.new(6, 2, 1, 3)
theme.texturemap.edge_top    = rect.new(2, 0, 3, 1)
theme.texturemap.edge_bottom = rect.new(2, 6, 3, 1)
theme.texturemap.fade        = rect.new(texw - 16, 0, 8, texh)
theme.texturemap.fadeinv     = rect.new(texw - 8, 0, 8, texh)
theme.texturemap.cross = rect.new(0, 16, 7, 7)

theme.bgcolor = torgba("#4d4d4d")
theme.white = torgba("#fff")
theme.black = torgba("#000")

local function putcorner(r, x, y, sub)
	return { rect.new(r.x + x, r.y + y, sub.w, sub.h), theme.white, subtex(theme.texture, sub)}
end

local function putedge(r, x, y, w, h, sub)
	return { rect.new(r.x + x, r.y + y, w, h), theme.white, subtex(theme.texture, sub)}
end

theme.background = {
	draw = function(self, state)
			   local tm = theme.texturemap
			   return {{self.box:inner(), theme.bgcolor, subtex(theme.texture, tm.empty)}}
		   end
}

theme.button = {
	draw = function(self)
			   local r = self.box:inner()
			   local tm = theme.texturemap
			   local int = rect.new(self.box:inner().x + 1, self.box:inner().y + 1, self.box:inner().w - 2, self.box:inner(). h - 2)
			   
			   return {
		       	    -- bg
		           {int, theme.white, subtex(theme.texture, tm.fadeinv)},
		           -- corners
		           putcorner(r, 0, 0, tm.corner_tl),
		           putcorner(r, r.w - tm.corner_tr.w, 0, tm.corner_tr),
		           putcorner(r, 0, r.h - tm.corner_bl.h, tm.corner_bl),
		           putcorner(r, r.w - tm.corner_tr.w, r.h - tm.corner_bl.h, tm.corner_br),
		           -- borders
		           putedge(r, 0, 2, tm.edge_left.w, r.h - 4, tm.edge_left),
		           putedge(r, r.w - 1, 2, tm.edge_left.w , r.h - 4, tm.edge_right),
		           putedge(r, 2, 0, r.w - 4, tm.edge_top.h, tm.edge_top),
		           putedge(r, 2, r.h - tm.edge_bottom.h, r.w - 4, tm.edge_top.h, tm.edge_bottom),
			   }
		   end
}


theme.label = {
	draw = function(self)
			   local r = self.box:inner()
			   local tm = theme.texturemap
			   local rz = rect.new(self.box:inner().x, self.box:inner().y, self.box:inner().w , self.box:inner(). h)
			   local bg = tm.empty --tm.fadeinv
			   local color = theme.bgcolor -- theme.white
			   return {{rz, color, subtex(theme.texture, bg)}}
		   end
}

theme.checkbox = {
	draw = function(self)
			   local r = self.box:inner()
			   local tm = theme.texturemap
			   local int = rect.new(self.box:inner().x + 1, self.box:inner().y + 1, self.box:inner().w - 2, self.box:inner(). h - 2)
			   local chk = rect.new(self.box:inner().x + 5, self.box:inner().y + 5,  7, 7)
			   local qs = {
		       	    -- bg
		           {int, theme.white, subtex(theme.texture, tm.fadeinv)},
		           -- corners
		           putcorner(r, 0, 0, tm.corner_tl),
		           putcorner(r, r.w - tm.corner_tr.w, 0, tm.corner_tr),
		           putcorner(r, 0, r.h - tm.corner_bl.h, tm.corner_bl),
		           putcorner(r, r.w - tm.corner_tr.w, r.h - tm.corner_bl.h, tm.corner_br),
		           -- borders
		           putedge(r, 0, 2, tm.edge_left.w, r.h - 4, tm.edge_left),
		           putedge(r, r.w - 1, 2, tm.edge_left.w, r.h - 4, tm.edge_right),
		           putedge(r, 2, 0, r.w - 4, tm.edge_top.h, tm.edge_top),
		           putedge(r, 2, r.h - tm.edge_bottom.h, r.w - 4, tm.edge_top.h, tm.edge_bottom),
			   }
			   if self.value then
		       	table.insert(qs, {chk, theme.white, subtex(theme.texture, tm.cross)})
		       end
			   return qs
		   end
}

theme.split = {
	draw = function(self, state)
		   end
}

theme.stack = {
	draw = function(self, state)
		   end
}

luie.theme = theme
