--
-- luie the gui: default theme
--

require 'gui/luie-utils'
require 'gfx'

local theme = {}

local _dbgcolor = 0.98
local function getcolor()
  if (_dbgcolor <= 0.2) then
    _dbgcolor = 0.98
  end
  _dbgcolor = _dbgcolor - 0.05
  return _dbgcolor
end

theme.background = {
    draw = function(self)
        local color = getcolor()
        return {{ self.box:inner(), {color, color, 1-color, 1}, nil}}
    end
}

theme.button = {
    draw = function(self)
        local color = getcolor()
        return {{ self.box:inner(), {color, color, 1-color, 1}, nil}}
    end
}


theme.label = {
    draw = function(self)
        local color = getcolor()
        return {{ self.box:inner(), {color, color, 1-color, 1}, nil}}
    end
}

theme.checkbox = {
    draw = function(self)
        local color = getcolor()
        return {{ self.box:inner(), {color, color, 1-color, 1}, nil}}
    end
}

theme.split = {
    draw = function(self)
        local color = getcolor()
        return {{ self.box:inner(), {color, color, 1-color, 1}, nil}}
    end
}

theme.stack = {
    draw = function(self)
        local color = getcolor()
        return {{ self.box:inner(), {1-color, 1-color, 1-color, 1}, nil}}
    end
}

luie.theme = theme
