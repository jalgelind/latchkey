rect = {}
function rect.new(x, y, w, h)
    local r = {}
   -- setmetatable(r, r.mt)
    r.x = x
    r.y = y
    r.w = w
    r.h = h
    return r
end

--function rect.__add(a, b)
--    return rect.new(a.x+b.x, a.y+b.y, a.w+b.w, a.h+b.h)
--end

function rect.format(rect)
    return string.format("x: %d y: %d w: %d h: %d", rect.x, rect.y, rect.w, rect.h)
end

function rect.print(r)
    print(rect.format(r))
end

-- add rects
function rect.add(rect, other)
    local ret = {}
    ret.x = rect.x + other.x
    ret.y = rect.y + other.y
    ret.w = rect.w + other.w
    ret.h = rect.h + other.h
    return ret
end

-- duplicate
function rect.dup(other)
    return rect.new(other.x, other.y, other.w, other.h)
end

-- check if rect contains point
function rect.contains_point(r, p)
	local left = r.x
	local right = r.x + r.w
	local top = r.y
	local bottom = r.y + r.h
	return p.x >= left and p.x <= right and p.y >= top and p.y <= bottom
end

-- split from top
function rect.split_htop(rect, where)
    local top = {}
    top.x = rect.x
    top.y = rect.y
    top.w = rect.w
    top.h = where
    
    local bot = {}
    bot.x = rect.x
    bot.y = rect.y + where
    bot.w = rect.w
    bot.h = rect.h - where

    return top, bot
end

-- split equal, horizontal
function rect.split_hequal(rect, num_pieces)
    local pieces = {}
    local h = rect.h / num_pieces
    for i = 0, num_pieces-1 do
        local e = {}
        e.x = rect.x
        e.y = rect.y + i*h
        e.w = rect.w
        e.h = h
        table.insert(pieces, e)
    end
    return pieces
end

local function translate_inp(inp_rect, wherefmt)
    -- convert to 0..1
    if wherefmt:sub(#wherefmt, #wherefmt) == "%" then
      wherefmt = wherefmt:sub(1, #wherefmt-1)
      -- todo: assert in range 0..100
      return tonumber(wherefmt) / 100
    elseif wherefmt:sub(#wherefmt-1, #wherefmt) == "px" then
        wherefmt = wherefmt:sub(1, #wherefmt-2)
        return tonumber(wherefmt) / inp_rect.h
    end
    -- raise not reached
    return 0
end

function rect.split_horizontal(inp_rect, wherefmt, invert)
      local recta, rectb = nil, nil
      local where = translate_inp(inp_rect, wherefmt)
      local htop = inp_rect.h * where
      local hbot = inp_rect.h * (1 - where)
      if invert == true then
        htop, hbot = hbot, htop
      end
      recta = rect.new(inp_rect.x, inp_rect.y, inp_rect.w, htop)
      rectb = rect.new(inp_rect.x, inp_rect.y + htop, inp_rect.w, hbot)
      return recta, rectb
end

function rect.split_vertical(inp_rect, wherefmt, invert)
      local recta, rectb = nil, nil
      local where = translate_inp(inp_rect, wherefmt)
      local wleft  = inp_rect.w * where
      local wright = inp_rect.w * (1 - where)
      if invert == true then
        wleft, wright = wright, wleft
      end
      recta = rect.new(inp_rect.x, inp_rect.y, wleft, inp_rect.h)
      rectb = rect.new(inp_rect.x + wleft, inp_rect.y, wright, inp_rect.h)
      return recta, rectb
end