--
-- luie the gui
--

-- TODO
-- Hard error when widgets don't fit in layouts
-- Option to share borders.
-- 

-- onmouseover
-- active widget
-- onkey events

-- sdf scaling for batched fonts. something that doesn't require
--   creating a huge fonts and then manually scaling down when drawing.

--[[

GUI WIDGET ATTRIBUTES

General:
  onclick: callback
  draw
  align: left/center/right
  valign: top/center/bottom
  margin: {left=px,right=px,top=px,bottom=px}
  padding: {left=px,right=px,top=px,bottom=px}

Button
  text: button text

Checkbox
  value: boolean

Split2
  body = { children }
  from = left/right/top/bottom
  at = "50px", "50%", ...

Stack
  body = { children }
  direction = horizontal/vertical
]]

require 'gui/rect'
require 'gui/widgetbox'
require 'gui/luie-utils'
require 'ext/pprint'

luie = {}
luie.__index = luie

luie.widget = {}
luie.layout = {}
luie.theme = {}
luie.error_counter = 0

luie.debug = false

if luie.debug then
  require 'gui/luie-theme-debug'
else
  require 'gui/luie-core-theme'
end

require 'gui/luie-core'

if not luie.theme.font then
  luie.theme.font = gfx.font("data/fonts/cantarell/Cantarell-Regular.ttf", 16)
end

function luie.new()
  local gui = {}
  setmetatable(gui, luie)
  gui.active = nil
  return gui
end

function luie.error(fmt, ...)
  printf("LUIE ERROR: " .. fmt, ...)
  luie.error_counter = luie.error_counter + 1
end

function luie._build(gui, hood, parent, subtree)
  if parent.children == nil then
    parent.children = {}
  end

  -- connect adjacent siblings, might work (untested ;D)
  function _connect_siblings(children)
    local first = children[1]
    local last = children[#children]
    if first == last then
      first.next_sibling = nil
      first.prev_sibling = nil
    else
      first.prev_sibling = nil
      last.next_sibling = nil
      for i=1,#children-1 do
        local child = children[i]
        local nchild = children[i+1]
        local pchild = children[i-1]
        child.next_sibling = nchild
        child.prev_sibling = pchild
        if nchild == last then
          nchild.prev_sibling = child
        end
      end
    end
  end

  function process_one(data)
    -- construct widget tree
    local layout_t = luie.layout[data.type]
    local widget_t = luie.widget[data.type]
    local constructor = layout_t or widget_t
    if constructor == nil then
      printf("'%s' is neither widget or layout.", data.type)
      return
    elseif layout_t ~= nil and widget_t ~= nil then
      printf("'%s' is both widget and layout. huh?", data.type)
      return
    end
    local widget = constructor(data)
    widget.is_layout = constructor == layout_t
    widget.is_widget = constructor == widget_t
    widget.hood = hood
    widget.type = data.type
    widget.box = widgetbox(data.rect or parent.box:outer())
    if widget.is_layout then
      local children = gui:_build(hood, widget, data.body)
      _connect_siblings(children)
      widget.children = children
    end
    if not parent.is_root then
      widget.parent = parent
    else
      widget.is_root = true -- first user window
    end
    widget.data = data
    widget:init()
    return widget
  end

  local children = {}
  for i, data in ipairs(subtree) do
    local child = process_one(data)
    table.insert(children, child)
  end
  return children
end

function luie._dump_widgets(gui, widget, depth)
  if depth == nil then depth = 0 end
  local spaces = 2 + depth
  if tostring(widget):sub(1, 4) == "(E) " then
    spaces = spaces - 4
  end
  printf("%s%s", string.rep(" ", spaces), tostring(widget))
  if widget.children ~= nil then
    for i, child in ipairs(widget.children) do
      gui:_dump_widgets(child, depth + 1)
    end
  end
end

function luie._update_primitives(gui, prims, widget, depth)
  local ps = widget:draw()
  if ps ~= nil then
    for i, p in ipairs(ps) do
     table.insert(prims, p)
    end
  end
  if widget.children ~= nil then
    for i, child in ipairs(widget.children) do
      gui:_update_primitives(prims, child, depth + 1)
    end
  end
end

-- structure: unprocessed gui tree data
-- size: {width, height} or {w=width, h=height}

function luie:build(structure, size)
  --rootsize = app.get_root_rect()
  local root = {}
  root.type = "stack"
  local winw, winh = 0, 0
  if size.h and size.w then
    winw, winh = size.w, size.h
  else
    winw, winh = table.unpack(size)
  end
  root.rect = rect.new(20, 20, winw-40, winh-40)
  root.body = { structure }
  
  local window = {}
  window.rect = rect.dup(root.rect)
  window.is_root = true
 
  -- stuff used under the hood
  local hood = {}
  hood.textbatch = gfx.textbatch(luie.theme.font)
  hood.buffer = gfx.quadbuffer(1024)
  hood.buffer_active_quads = 0

  -- convert from data to objects
  self.error_counter = 0
  window = self:_build(hood, window, {root})
  window = window[1]
  window:updatebounds()

  if luie.debug then
    print("Window tree")
    if window.children ~= nil then
      self:_dump_widgets(window)
    end
    if self.error_counter > 0 then
      printf("Warning - layout contains %d error(s).", self.error_counter)
    end
  end
  window.dirty = true
  return window
end

luie.windows = {}
function luie:pushwindow(window)
  table.insert(luie.windows, window)
end

function luie:get_active_window()
  return self.windows[#self.windows]
end

function luie:popwindow(allowrootpop)
  print(#self.windows, allowrootpop)
  if #self.windows == 1 and allowrootpop ~= true then
    return
  end
  table.remove(self.windows, #self.windows)
end

function luie:update()
  local window = self:get_active_window()
  local events = inp.get_events()
  for i, ev in ipairs(events) do
    if ev.type == "button" and ev.pressed ~= nil then
      local point = ev -- x, y
      if window:covers(point) then
        -- set active if we hit a widget
        window:onclick(ev.pressed, point)
        print("active", self.active)
        window.dirty = true
      else -- close window if we click outside it's bounds... (temporary)
        self:popwindow()
      end
    elseif ev.type == "key" and ev.released == "esc" then
      self:popwindow()
    end
  end
end


function buildquadtable(prims)
  local quads = {}
  for i, e in ipairs(prims) do
    local quad = lapp.quad()
    local q, color, coords = table.unpack(e)
    quad.set_rect(q.x, q.y, q.w, q.h)
    quad.set_color(table.unpack(color))
    if coords then
      quad.set_texcoords(coords.x, coords.y, coords.w, coords.h)
    end
    quad.set_depth(-1)
    table.insert(quads, quad)
  end
  return quads
end

function luie:build_background()
  self.bgbuffer = gfx.quadbuffer(16)
  local bgquads = luie.theme.background.draw(self.windows[1])
  local quads = buildquadtable(bgquads)
  self.bgbuffer:update(0, quads)
end

function luie:draw_background()
  if not self.bgbuffer then
    self:build_background()
  end
  self.bgbuffer:draw()
end

function luie:draw()
  if luie.theme.texture then
    luie.theme.texture:bind()
  end
  luie:draw_background()
  -- refresh gui
  for i, window in ipairs(self.windows) do
    if window.dirty then
      self.prims = {}
      if luie.debug then
        local r = window.box:outer()
        local should_be_invisible = rect.new(r.x, r.y, r.w, r.h)
        local should_be_border = rect.new(r.x-1, r.y-1, r.w+2, r.h+2)
        table.insert(self.prims, { should_be_border, torgba("#00f"), nil})
        table.insert(self.prims, { should_be_invisible, torgba("#f00"), nil})
      end
      self:_update_primitives(self.prims, window, 0)

      -- update quadbuffer
      local quads = buildquadtable(self.prims)
      window.hood.buffer:update(0, quads)
      window.hood.buffer_active_quads = #quads
      window.dirty = false
    end
    window.hood.buffer:draw(0, window.hood.buffer_active_quads)
  window.hood.textbatch:draw()
  end
  if luie.theme.texture then
    luie.theme.texture:unbind()
  end
end
