--
-- luie the gui: core widgets and core layouts
--

require 'gui/rect'
require 'gui/widgetbox'
require 'gui/luie-utils'
require 'ext/class'

local theme = luie.theme

local fmt = function(obj, fmt, ...)
  local err = obj.broken or false
  local errstr = ""
  if err == true then
    errstr = "(E) "
  end
  return string.format(errstr .. fmt, ...)
end

--
-- widgets
--

-- Base widget with default method implementations
local basewidget = class(function(self)
end)

-- Default methods below

function basewidget:init()
  -- not implemented
  luie.error("missing init method")
end

function basewidget:draw()
    -- not implemented
end

function basewidget:onclick(button, point)
  if self.children then
    for i, child in ipairs(self.children) do
      --print(child)
      if not child.onclick then
        luie.error("<%s> is missing :onclick", child)
      elseif child:covers(point) then
        -- if we hit a child; mark it as active
        if child.is_widget then
          luie.active = child
        end
        return child:onclick(button, point)
      end
    end
  end
end

function basewidget:updatebounds()
  -- not implemented
end

function basewidget:validate()
  -- not implemented
end

function basewidget:updatealignment()
  -- not implemented
end

function basewidget:getbounds()
  -- not implemented
end

function basewidget:updateproperties(data)
  self.align = data.align or "left"  -- left/center/right
  self.valign = data.valign or "top" -- top/center/bottom

  -- TODO:
  self.min_width = data.min_width
  self.max_width = data.max_width
  self.min_height = data.min_height
  self.max_height = data.max_height

  -- don't apply margins and stuff to the root window
  -- skip it for layouts as well.
  if self.is_root or 
     self.is_layout then
    self.margin = {top=0,bottom=0,left=0,right=0}
    self.padding = {top=0,bottom=0,left=0,right=0}
  else
    self.margin = {top=data.margin_top       or 1
                  ,bottom=data.margin_bottom or 1 
                  ,right=data.margin_right   or 1
                  ,left=data.margin_left     or 1}
    self.padding = {top=0,bottom=0,left=0,right=0}
  end
  self.box:setmargin(self.margin)
  self.box:setpadding(self.padding)
end

function basewidget:gettrace()
  local widget = self
  local trace = {widget}
  while widget.parent ~= nil do
    widget = widget.parent
    table.insert(trace, widget)
  end
  return trace
end

function basewidget:showtrace()
  for i, t in ipairs(self:gettrace()) do
    print("- " .. tostring(t))
  end
end

-- Helper, check if a coordinate is contained within widget.
function basewidget:covers(point)
  return rect.contains_point(self.box:inner(), point)
end

-- Mark widget as dirty, traverse parent hierarchy to trigger ui update.
function basewidget:setdirty()
  self.dirty = true
  if self.parent and not self.parent.dirty then
    self.parent:setdirty()
  end
end


function basewidget:setrect(r)
  self.box:setpos(r.x, r.y)
  self.box:setsize(r.w, r.h)
end

function basewidget:setsize(w, h)
  self.box:setsize(w, h)
end

function basewidget:setpos(x, y)
  self.box:setpos(x, y)
end

function basewidget:translate(x, y)
  self.box:translate(x, y)
end

-- todo: shoulde be renamed updatebounds(bounds)
-- todo: and updatebounds() should be renamed update()?
function basewidget:setbounds(bounds)
  self.box:setsize(bounds.w, bounds.h)
end

-- Button widget
luie.widget.button =
  class(basewidget, function(self, data)
    self.mt = getmetatable(self)
    self.mt.__tostring = function (self)
     return fmt(self, "W.button - text: %s (%s)", tostring(self.text), rect.format(self.box:outer()))
    end

    self.init = function(self)
      self:updateproperties(self.data)
      self.text = data.text or "I'm a button, click me!"
      self.text_padding = data.text_padding or theme.label.text_padding or {x = 10, y = 4}
    end

    self.draw = function(self)
      self.textstring:set_position(self.box:inner().x + self.text_padding.x/2,
                                   self.box:inner().y + self.text_padding.y/2)
      self.textstring:set(self.text)
      return theme.button.draw(self)
    end

    self.onclick_default = function(self, button, point)
      print(string.format("%s - what am I supposed to do?", self.text))
    end
    self.onclick = data.onclick or self.onclick_default
    
    self.getbounds = function(self)
      return self.box:outer()
    end

    self.setbounds = function(self, bounds)
      -- decline.
    end

    self.updatebounds = function(self)
      if self.textstring == nil then
        self.textstring = self.hood.textbatch:create_string(0, 0, 32, self.text)
      end
      local textw, texth = self.textstring:size()
      self:setsize(textw + self.text_padding.x, texth + self.text_padding.y)
    end
end)

-- Label widget
luie.widget.label =
  class(basewidget, function(self, data)
    self.mt = getmetatable(self)
    self.mt.__tostring = function (self)
     return fmt(self, "W.label - text: %s (%s)", tostring(self.text), rect.format(self.box:outer()))
    end

    self.init = function(self)
      self:updateproperties(self.data)
      self.text = data.text or "I'm a label"
    end

    self.draw = function(self)
      self.textstring:set_position(self.box:inner().x, self.box:inner().y)
      self.textstring:set(self.text)
      return theme.label.draw(self)
    end
    
    self.getbounds = function(self)
      return self.box:outer()
    end

    self.setbounds = function(self, bounds)
      -- decline.
    end

    self.updatebounds = function(self)
      if self.textstring == nil then
        self.textstring = self.hood.textbatch:create_string(0, 0, 32, self.text)
      end
      local textw, texth = self.textstring:size()
      self:setsize(textw, texth)
    end
end)

-- Checkbox widget
luie.widget.checkbox =
  class(basewidget, function(self, data)
    self.mt = getmetatable(self)
    self.mt.__tostring = function (self)
     return fmt(self, "W.checkbox - value: %s (%s)", tostring(self.value), rect.format(self.box:outer()))
    end

    self.init = function(self)
      self:updateproperties(self.data)
      self.value = data.value or false

      if self.value == 0 then self.value = false
      else self.value = true end
    end
    
    self.draw = function(self)
      return theme.checkbox.draw(self)
    end

    self.onclick_default = function(self, button, point)
      self.value = not self.value
    end
    self.onclick = data.onclick or self.onclick_default

    self.getbounds = function(self)
      return self.box:outer()
    end

    self.setbounds = function(self, bounds)
      -- decline.
    end

    self.updatebounds = function(self)
      self:setsize(16, 16)
    end
end)

--
-- layouts
--

-- two-panel split
-- 
-- parameters
--  from: top | bottom | left | right
--  at: Npx, N%
luie.layout.split2 = 
  class(basewidget, function(self, data)
    self.mt = getmetatable(self)
    self.mt.__tostring = function (self)
      return fmt(self, "L.split2 - from: %s, at: %s, size: %s", tostring(self.from), tostring(self.at), rect.format(self.box:outer()))
    end

    self.init = function(self)
      self:updateproperties(self.data)
      self.from = data.from or "top"
      self.at = data.at or "50%"
      self.is_hsplit = self.from == "top" or self.from == "bottom"
      self.is_vsplit = self.from == "left" or self.from == "right"
    end

    self.draw = function(self)
      return theme.split.draw(self)
    end

    self.translate = function(self, x, y)
      self.box:translate(x, y)
      for i, ch in ipairs(self.children) do
        ch:translate(x, y)
      end
    end

    self.setpos = function(self, x, y)
      local dx, dy = x-self.box:bounds().x, y-self.box:bounds().y
      self.box:setpos(x, y)
      for i, ch in ipairs(self.children) do
        ch:translate(dx, dy)
      end
    end

    self.getbounds = function(self)
      return self.box:outer()
    end

    self.updatealignment = function(self)
      -- Splits are made as large as possible to fit the parent
      -- container, so there's no space left for alignment.
    end

    -- todo: diagnostic error messages
    self.validate = function(self)
      local dim_accum = {h=0,w=0}
      local outer = self.box:outer()
      for i, child in ipairs(self.children) do
        local cb = child.box:outer()
        if self.is_hsplit then
          dim_accum.h = dim_accum.h + cb.h
        else
          dim_accum.w = dim_accum.w + cb.w
        end
        if cb.w > outer.w then
          luie.error("Split: Child width exceeds layout boundary: (%d > %d).", cb.w, outer.w)
          self.broken = true
        end
        if cb.h > outer.h then
          luie.error("Split: Child height exceeds layout boundary: (%d > %d).", cb.h, outer.h)
          self.broken = true
        end
      end
      if dim_accum.w > outer.w then
        luie.error("Split: Total child width exceeds layout boundary: (%d > %d).", dim_accum.w, outer.w)
        self.broken = true
      end
      if dim_accum.h > outer.h then
        luie.error("Split: Total child height exceeds layout boundary: (%d > %d).", dim_accum.h, outer.h)
        --self:showtrace()
        self.broken = true
      end
    end

    local function calc_split(inp)
      -- we split using bounds because those are the actual dimensions
      -- of the widget.
      if self.is_hsplit then
        return rect.split_horizontal(self.box:bounds(), inp, self.from == "bottom")
      else
        return rect.split_vertical(self.box:bounds(), inp, self.from == "right")
      end
    end

    self.setbounds = function(self, bounds)
      -- accept new bounds
      self.box:setsize(bounds.w, bounds.h)
    end

    self.updatebounds = function(self)
      if (#self.children ~= 2) then
        luie.error("split2 can only have two elements, got %d", #self.children)
        self.broken = true
        return
      end

      local childrects = { calc_split(self.at) }
      for i, child in ipairs(self.children) do
        child:setrect(childrects[i])
        child:updatebounds()
        child:updatealignment()
        child:validate()
      end
    end
end)

-- horizontal/vertical widget stack
-- parameters
--  direction: vertical | horizontal
--  margin: space between widgets in pixels
luie.layout.stack = 
  class(basewidget, function(self, data)
    self.mt = getmetatable(self)
    self.mt.__tostring = function (self)
      return fmt(self, "L.stack_%s - size: %s - %s", 
          self.direction, rect.format(self.box:outer()), self.align)
    end

    self.init = function(self)
      self:updateproperties(self.data)
      self.direction = data.direction or "horizontal" -- or vertical (container flow direction)
      self.is_vstack = self.direction == "vertical"
      self.is_hstack = self.direction == "horizontal"
    end


    self.draw = function(self)
     return theme.stack.draw(self)
    end

    self.translate = function(self, x, y)
      self.box:translate(x, y)
      for i, ch in ipairs(self.children) do
        ch:translate(x, y)
      end
    end

    self.setpos = function(self, x, y)
      local dx, dy = x-self.box:bounds().x, y-self.box:bounds().y
      self.box:setpos(x, y)
      for i, ch in ipairs(self.children) do
        ch:translate(dx, dy)
      end
    end

    self.getbounds = function(self)
      local r = rect.new(0, 0, 0, 0)
      for i, child in ipairs(self.children) do
        local chrect = child:getbounds()
        if self.is_vstack then
          r.h = r.h + chrect.h
          r.w = math.max(chrect.w, r.w)
        else
          r.h = math.max(chrect.h, r.h)
          r.w = r.w + chrect.w
        end
      end
      local b = widgetbox(r)
      b:setmargin(self.margin)
      b:setpadding(self.padding)
      return b:outer()
    end

    -- TODO: diagnostic error messages
    -- TODO: check that child is within bounds
    self.validate = function(self)
      local dim_accum = {h=0,w=0}
      local outer = self.box:outer()
      for i, child in ipairs(self.children) do
        local cb = child.box:outer()
        if self.is_vstack then
          dim_accum.h = dim_accum.h + cb.h
        else
          dim_accum.w = dim_accum.w + cb.w
        end
        if cb.w > outer.w then
          luie.error("Stack: Child width exceeds layout boundary.")
          self.broken = true
        end
        if cb.h > outer.h then
          luie.error("Stack: Child height exceeds layout boundary.")
          self.broken = true
        end
      end
      if dim_accum.w > outer.w then
        luie.error("Stack: Total child width exceeds layout boundary.")
        self.broken = true
      end
      if dim_accum.h > outer.h then
        luie.error("Stack: Total child height exceeds layout boundary.")
        self.broken = true
      end
    end

    self.updatealignment = function(self)
      -- update widget alignment attributes so we can group alignment targets
      -- 
      -- this will change the alignment for the widgets so they don't
      -- change their logical position relative each other.
      -- e.g
      --   left (right) center right -> left (center) center right
      --   left center (left) right -> left center (center) right
      --
      -- FIXME: Needs a better implementation. This probably miss a lot of
      --        cases. Widgets might be swapped around a bit.
      local function realign_widgets(children)
        local left, center, right = "left", "center", "right"
        local target = "align"
        if self.is_vstack then
          left, center, right = "top", "center", "bottom" 
          target = "valign"
        end
        local resolvers = {
           {right, left, left,     "fst"}
          ,{right, center, center, "fst"}
          ,{center, left, center,  "snd"}
        }
        for i, child in ipairs(self.children) do
          if #self.children ~= i then
            local nchild = self.children[i+1]
            for o, res in ipairs(resolvers) do
              if child[target] == res[1] and nchild[target] == res[2] then
                if res[4] == "fst" then
                  child[target] = res[3]
                elseif res[4] == "snd" then
                  nchild[target] = res[3]
                end
              end
            end
          end
        end
      end

      realign_widgets(self.children)

      -- group alignment targets
      local align = {left={}, center={}, right={}}
      local width = {left=0, center=0, right=0}
      local valign = {top={}, center={}, bottom={}}
      local height = {top=0, center=0, bottom=0}

      for i, child in ipairs(self.children) do
        local ch = child.box:outer()
        if child.align then
          table.insert(align[child.align], child)
          width[child.align] = width[child.align] + ch.w
        end
        if child.valign then
          table.insert(valign[child.valign], child)
          height[child.valign] = height[child.valign] + ch.h
        end
      end

      local left_w, center_w, right_w = 0, 0, 0
      local top_h, center_h, bottom_h = 0, 0, 0

      for o, child in ipairs(self.children) do
        local ch = child.box:outer()
        local cb = child:getbounds()
        local px, py, tx, ty = child.box:outer().x, child.box:outer().y, 0, 0
        local self_rect = self.box:inner()
        -- do horizontal alignments
        if child.align == "left" then
          if self.is_hstack then
            px = self_rect.x
            tx = left_w
            left_w = left_w + ch.w
          else
            -- ?
          end
        elseif child.align == "center" then
          local freespace = self_rect.w - width.left - width.right - width.center
          if self.is_hstack then
            px = self_rect.x + width.left + freespace/2
            tx = center_w
            center_w = center_w + ch.w
          else
            px = self_rect.x + (self_rect.w - ch.w) / 2
            tx = 0
          end
        elseif child.align == "right" then
          if self.is_hstack then
            px = self_rect.x + self_rect.w - width.right
            tx = right_w
            right_w = right_w + ch.w
          else
            if self_rect.w > ch.w then
              px = self_rect.x + self_rect.w - ch.w
              tx = 0
            end
          end
        end
        -- do vertical alignments
        if child.valign == "top" then
            -- No need to implement - top by default?
          if self.is_vstack then
            py = self_rect.y
            ty = top_h
            top_h = top_h + ch.h
          else
            -- 
          end
        elseif child.valign == "center" then
          local freespace = self_rect.h - height.top - height.bottom - height.center
          if self.is_vstack then
            py = self_rect.y + height.top + freespace/2
            ty = center_h
            center_h = center_h + ch.h
          else
            py = self_rect.y + (self_rect.h - ch.h)/2
            ty = 0
          end
        elseif child.valign == "bottom" then
          if self.is_vstack then
            py = self_rect.y + self_rect.h - height.bottom
            ty = bottom_h
            bottom_h = bottom_h + ch.h
          else
            py = self_rect.y + self_rect.h - ch.h
          end
        end
        child:setpos(px, py)
        child:translate(tx, ty)
      end
    end

    self.setbounds = function(self, bounds)
      -- decline
    end
  
    self.updatebounds = function(self)
      local flow_dir = self.is_vstack and "h" or "w"
      local fixed_dim = self.is_vstack and "w" or "h"
      local axis = self.is_hstack and "x" or "y"
      local curr = rect.dup(self.box:inner())

      -- only splits can be expanders
      function is_expander(child)
        return child.is_vsplit or child.is_hsplit
      end

      -- mark children as expanders
      -- expanding child widgets share "leftover" space
      local expanders = 0
      local leftover = self.box:outer()[flow_dir]
      for i, child in ipairs(self.children) do
        child:updatebounds()
        local bounds = child:getbounds()
        leftover = leftover - bounds[flow_dir]
        if is_expander(child) then
          expanders = expanders + 1
          child.should_expand = true
        end
      end

      for i, child in ipairs(self.children) do
        local dim = {h = 0, w = 0}
        local cb = child:getbounds()
        dim.w = cb.w
        dim.h = cb.h

        -- share left-over space for expanders if possible
        -- otherwise expand the last widget.
        if expanders > 0 and child.should_expand then
          local even = leftover / expanders
          dim[flow_dir] = dim[flow_dir] + even
          child.should_expand = nil
        elseif expanders == 0 then
          if i == #self.children then
            dim[flow_dir] = curr[flow_dir]
          end
        end

        local newrect = rect.new(curr.x, curr.y, dim.w, dim.h)
        newrect[fixed_dim] = self.box:inner()[fixed_dim]
        child.box:set(newrect)

        -- refresh child tree
        child:updatebounds()

        -- adjust widget alignment
        child:updatealignment()

        -- check for errors
        child:validate()

        -- advance to next widget position
        curr[flow_dir] = curr[flow_dir] - dim[flow_dir]
        curr[axis] = curr[axis] + dim[flow_dir]
      end
    end
end)
