app = {}
app.__app = lapp.app()
app.__interpolator = lapp.interpolator()
app.__rndgen = lapp.random()

app.timer = lapp.timer
app.configuration = lapp.configuration

app.__mappings = {}

app.__imappings = {
    {"easereg", "reg"},
    {"easeget", "get"}}

app.__rmappings = {
    {"rand",     "get"},
    {"randseed", "seed"}}

util.register_methods(app, app.__mappings, lapp.app, app.__app)
util.register_methods(app, app.__imappings, lapp.interpolator, app.__interpolator)
util.register_methods(app, app.__rmappings, lapp.random, app.__rndgen)
