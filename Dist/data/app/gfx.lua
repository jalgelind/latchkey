require 'util'

gfx = {}

-- Graphics Core
gfx.__instance = lapp.graphics()

-- bind functions to gfx-namespace
gfx.__mappings = {}

util.register_methods(gfx, gfx.__mappings, lapp.graphics, gfx.__instance)

gfx.font = lapp.font
gfx.textbatch = lapp.textbatch
gfx.textstring = lapp.textstring
gfx.color = lapp.color
gfx.texture = lapp.texture
gfx.batch = lapp.pbatch
gfx.quadbuffer = lapp.quadbuffer
gfx.quad = lapp.quad
gfx.framebuffer = lapp.framebuffer
gfx.shader = lapp.shader
gfx.window = lapp.window
gfx.print = lapp.font.print
