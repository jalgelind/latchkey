-- Utility module

util = {}

function util.register_methods(ns, mappings, impl, instance)
  function reg(k, v)
     ns[k] =
      function(...)
        return impl[v](instance, ...)
      end   
  end
  -- register custom mappings
  for i, m in pairs(mappings) do
    local k, v = m[1], m[2]
    reg(k, v)
  end
  -- register others
  for n, m in pairs(impl) do
    if n ~= "new" then
      reg(n, n)
    end
  end
end

function util.locals()
  local variables = {}
  local idx = 1
  while true do
    local ln, lv = debug.getlocal(2, idx)
    if ln ~= nil then
      variables[ln] = lv
    else
      break
    end
    idx = 1 + idx
  end
  return variables
end

function util.split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
   table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function util.escape_backslashes(str)
  local res = string.gsub(str, "\\", "\\\\")
  return res
end

function util.wrap(s, w, i1, i2)
  w = w or 78
  i1 = i1 or 0
  i2 = i2 or 0
  s = string.rep(" ", i1) .. s
  local lstart, len = 1, #s
  while len - lstart > w do
    local i = lstart + w
    while i > lstart and string.sub(s, i, i) ~= " " do i = i - 1 end
    local j = i
    while j > lstart and string.sub(s, j, j) == " " do j = j - 1 end
    s = string.sub(s, 1, j) .. "\n" .. string.rep(" ", i2) ..
      string.sub(s, i + 1, -1)
    local change = i2 + 1 - (i - j)
    lstart = j + change
    len = len + change
  end
  return s
end
