require "ext/introspect"
require "ext/cron"
require "ext/middleclass"

require 'gfx'
require 'app'
require 'inp'
require 'util'


-- say hello
print("booting...")

font = gfx.font("data/fonts/debug.pfnt")
font:set_draw_offset(2, -4)

if not font:valid() then
  print("Could not load debug font.")
end

-- logging

backlog = {data = {}, max = 1024}
function backlog:print(...)
  table.insert(self.data, (...))
  if (#self.data > self.max) then
    table.remove(self.data, 1)
  end
end
function backlog:clear()
  self.data = {}
end

_print = print
function print(...)
  _print(...)
  backlog:print(...)
end

function printf(fmt, ...)
  print(string.format(fmt, ...))
end

local function _dispatch_call(fun, ...)
  if _G[fun] ~= nil then
    return _G[fun](...)
  end
end

function _on_log_message(type, target, identifier, msg)
  -- heads up:
  -- attempting to _print() here will recursivly call itself
  -- until the stack blows up.
  -- print_dbg(type, target, identifier, msg)
  backlog:print(msg)
  _dispatch_call("on_log_message", type, target, identifier, msg)
end

-- error handling
error_stop = false
function _on_error_signal()
  error_stop = true
  print(debug.traceback())
  _dispatch_call("on_error_signal")
end

-- When file `path` is reloaded
-- return true if the entire application should be rebooted, return
-- false just continue
function _on_file_reload(result, path)
  error_stop = false
  return _dispatch_call("on_file_reload", result, path)
end

function _on_init()
  _dispatch_call("on_init")
end

-- When application is terminated
function _on_shutdown()
  _dispatch_call("on_shutdown")
end

-- When the application is rebooted
function _on_reboot()
  local res = _dispatch_call("on_reboot")
  if res ~= nil then
    return res
  end
  return ""
end

-- When the application is restored from previous reboot
function _on_restore(state)
  _dispatch_call("on_restore", state)
end


-- pre update, pre draw
function _update()
  if (not error_stop) then
    update()
  else
    -- runtime error
  end
end

function _draw()
  if (not error_stop) then
    draw()
  else
    _draw_error()
  end
end


local win = gfx.get_active_window()
win_w, win_h = win:size()
--gfx.set_viewport(0, 0, win_w, win_h)
function _draw_error()
  --gfx.set_ortho_projection(math.ortho(0, win_w, win_h, 0, -10, 10))
  gfx.clear()
  -- runtime error
  text = ""
  for i, line in ipairs(backlog.data) do
    text = text .. tostring(line) .. "\n"
  end

  text = text.gsub(text, "-- runtime error --", "<red>%1</red>")
  text = text.gsub(text, "-- stack traceback --", "<red>%1</red>")
  gfx.print(font, 0, 0, text)
  --font:print(0, 0, text)
end
