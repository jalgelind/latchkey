#include <Pxf/Kernel.h>
#include <Pxf/System.h>
#include <Pxf/Module.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Modules/auxinput/auxinput.h>
#include <Pxf/Modules/auxinput/Devices.h>


static const unsigned Module_Kernel_Version = Pxf::Kernel::KERNEL_VERSION;
static const unsigned Module_Api_Version = Pxf::Module::MODULE_VERSION;

using Pxf::Modules::AuxiliaryInputController;
#ifndef CONF_MODULAR
REGISTER_MODULE(AuxiliaryInputController, "aux", Module_Kernel_Version, Module_Api_Version);
#endif

namespace AuxiliaryInputController_
{
	PXFEXPORT Pxf::Module* CreateInstance()
	{
		Pxf::Modules::AuxiliaryInputController *m = new Pxf::Modules::AuxiliaryInputController("aux", Module_Kernel_Version, Module_Api_Version);
		return m;
	}

	PXFEXPORT void DestroyInstance(Pxf::Module* _module)
	{
		if (_module)
		{
			delete _module;
		}
	}
}

bool Pxf::Modules::AuxiliaryInputController::RegisterSystem(Pxf::Kernel* _Kernel, unsigned _SystemType)
{
	if (_SystemType & Pxf::System::INPUT_CONTROLLER)
	{
		_Kernel->RegisterInputController(Pxf::make_unique<GamepadController>(_Kernel));
		_Kernel->RegisterInputController(Pxf::make_unique<MidiDeviceController>(_Kernel));
		return true;
	}

	return false;
}

