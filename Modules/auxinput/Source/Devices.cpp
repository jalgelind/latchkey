#include <Pxf/Modules/auxinput/Devices.h>
#include <cmath>
#include <cstdio>

extern "C" {
#include <gamepad/Gamepad.h>
}

extern "C" {
#include <portmidi.h>
}

using namespace Pxf;
using namespace Input;

class GamepadController;
static GamepadController* g_GlobalGamepadController = 0;

// Gamepad callbacks
//
//

bool on_button_down(void * sender, const char * eventID, void * eventData, void * context) {
	struct Gamepad_buttonEvent * event = (Gamepad_buttonEvent*)eventData;
	g_GlobalGamepadController->OnBtnEvent(event->device->deviceID, event->buttonID, Gamepad::EVENT_PRESS);
	//Message("unk", "Button %u down (%d) on device %u at %f\n", event->buttonID, (int) event->down, event->device->deviceID, event->timestamp);
	return true;
}

bool on_button_up(void * sender, const char * eventID, void * eventData, void * context) {
	struct Gamepad_buttonEvent * event = (Gamepad_buttonEvent*)eventData;
	g_GlobalGamepadController->OnBtnEvent(event->device->deviceID, event->buttonID, Gamepad::EVENT_RELEASE);
	//Message("unk", "Button %u up (%d) on device %u at %f\n", event->buttonID, (int) event->down, event->device->deviceID, event->timestamp);
	return true;
}

bool on_axis_moved(void * sender, const char * eventID, void * eventData, void * context) {
	struct Gamepad_axisEvent * event = (Gamepad_axisEvent*)eventData;
	g_GlobalGamepadController->OnAxisChange(event->device->deviceID, event->axisID, event->value);
	//Message("unk", "Axis %u moved to %f on device %u at %f\n", event->axisID, event->value, event->device->deviceID, event->timestamp);
	return true;
}

bool on_device_attached(void * sender, const char * eventID, void * eventData, void * context) {
	struct Gamepad_device * device = (Gamepad_device*)eventData;
	g_GlobalGamepadController->OnAttach(device->deviceID, device->description, device->vendorID, device->productID, eventData);
	//Message("unk", "Device ID %u attached (vendor = 0x%X; product = 0x%X)\n", device->deviceID, device->vendorID, device->productID);
	device->eventDispatcher->registerForEvent(device->eventDispatcher, GAMEPAD_EVENT_BUTTON_DOWN, on_button_down, device);
	device->eventDispatcher->registerForEvent(device->eventDispatcher, GAMEPAD_EVENT_BUTTON_UP, on_button_up, device);
	device->eventDispatcher->registerForEvent(device->eventDispatcher, GAMEPAD_EVENT_AXIS_MOVED, on_axis_moved, device);
	return true;
}

bool on_device_removed(void * sender, const char * eventID, void * eventData, void * context) {
	struct Gamepad_device * device = (Gamepad_device*)eventData;
	g_GlobalGamepadController->OnDetach(device->deviceID);
	//Message("unk", "Device ID %u removed\n", device->deviceID);
	return true;
}

//
// Gamepad Device
//

AuxGamepad::AuxGamepad(Pxf::Kernel* krn, Pxf::Input::InputController* inp, int ctrlidx)
	: Pxf::Input::Gamepad(krn, inp)
	, controller_index(ctrlidx)
	, devinfo(0)
{
}

void AuxGamepad::Activate()
{
	if (is_activated_)
		return;

	if (devinfo)
	{
		m_AxisMemory.clear();
		m_AxisMemoryPrev.clear();
		m_AxisStates.clear();

		button_count = devinfo->numButtons;
		axis_count = devinfo->numAxes;
		for(int i = 0; i < axis_count; i++)
		{
			m_AxisMemory.push_back(devinfo->axisStates[i]);
			m_AxisMemoryPrev.push_back(devinfo->axisStates[i]);
			m_AxisStates.push_back(Gamepad::EVENT_AXISDOWNRELEASE);
		}
		is_activated_ = true;
	}
}

void AuxGamepad::Deactivate()
{
	is_activated_ = false;
	m_AxisMemory.clear();
	m_AxisMemoryPrev.clear();
	m_AxisStates.clear();
}

AuxGamepad::~AuxGamepad()
{}

int AuxGamepad::ButtonCount()
{
	return button_count;
}

int AuxGamepad::AxisCount()
{
	return axis_count;
}

int AuxGamepad::GetButtonStatus(int idx)
{
	// Handle unique virtual button codes (used in events)
	int codeoffset = AxisCount() * 2;
	if (idx - codeoffset >= 0)
		idx -= codeoffset;

	if (controller_index < Gamepad_numDevices() && idx < ButtonCount())
	{
		Gamepad_device* devinfo = Gamepad_deviceAtIndex(controller_index);
		return devinfo->buttonStates[idx];
	}
	return 0;
}

// idx is not consistent with the id reported by axis events
float AuxGamepad::GetAxisValue(int idx)
{
	if (controller_index < Gamepad_numDevices())
	{
		Gamepad_device* devinfo = Gamepad_deviceAtIndex(controller_index);
		return devinfo->axisStates[idx];
	}
	return 0.f;
}

float AuxGamepad::GetPreviousAxisValue(int idx)
{
	if (controller_index < Gamepad_numDevices() && idx < m_AxisMemoryPrev.size())
	{
		return m_AxisMemoryPrev[idx];
	}
	return 0.f;
}

Gamepad::AxisStatus AuxGamepad::GetAxisStatus(int idx)
{
	if (controller_index < Gamepad_numDevices() && idx < m_AxisStates.size())
		return Gamepad::AxisStatus(idx, m_AxisStates[idx]);
	return Gamepad::AxisStatus();
}

void AuxGamepad::OnBtnEvent(int button, Pxf::Input::Gamepad::EventType type)
{
	Gamepad::Event ev;
	ev.code = button;
	ev.type = type;

	// We don't want button id:s and axis:ids to collide, you'd need two compares to use it otherwise...
	int codeoffset = AxisCount() * 2;
	ev.code += codeoffset;

	m_EventQ.Put(ev);
}

void AuxGamepad::OnAxisEvent(int axis, Pxf::Input::Gamepad::EventType type)
{
	Gamepad::Event ev;
	int offset = 0;
	if (type == Gamepad::EVENT_AXISUP|| type == Gamepad::EVENT_AXISUPRELEASE)
		offset = 1;
	int virtaxis = axis;
	if (axis > 0)
		virtaxis += axis*2;

	ev.axis = virtaxis + offset;
	ev.type = type;
	m_EventQ.Put(ev);
}

void AuxGamepad::OnAxisChange(int axis, float delta)
{
	if (devinfo)
	{
		for(int o = 0; o < AxisCount(); o++)
		{
			if (axis >= m_AxisMemory.size())
				break;
			if (axis >= m_AxisStates.size())
				break;
			float oldvalue = fabs(m_AxisMemory[o]);
			float newvalue_org = devinfo->axisStates[o];
			bool up = newvalue_org <= 0.f;
			float newvalue = fabs(newvalue_org);
			Gamepad::EventType type = Gamepad::EVENT_INVALID;
			if (oldvalue < axis_activation_threshold && newvalue >= axis_activation_threshold)
			{
				type = up ? Gamepad::EVENT_AXISUP : Gamepad::EVENT_AXISDOWN;
			}
			else if (oldvalue >= axis_activation_threshold && newvalue < axis_activation_threshold)
			{
				type = up ? Gamepad::EVENT_AXISUPRELEASE : Gamepad::EVENT_AXISDOWNRELEASE;
			}

			if (type != Gamepad::EVENT_INVALID)
			{
				OnAxisEvent(o, type);
				m_AxisStates[o] = type;
			}
			m_AxisMemoryPrev[o] = oldvalue;
			m_AxisMemory[o] = newvalue_org;
		}
	}
}


//
// Gamepad Controller
//

GamepadController::GamepadController(Pxf::Kernel* _Kernel)
	: Pxf::Input::InputController(_Kernel, "Gamepad Input Controller")
{
	logger_ = _Kernel->GetLogger("padctrl");
	g_GlobalGamepadController = this;
	m_HasKeyboard = false;
	m_HasMouse = false;
	m_HasMidiDevice = false;
	m_HasGamepad = false; // initial state, a gamepad will be available when detected
	canupdate = true;
}

GamepadController::~GamepadController()
{
	g_GlobalGamepadController = 0;
	Gamepad_shutdown();
}

void GamepadController::Update()
{
	Gamepad_processEvents();
}

void GamepadController::Discover()
{
		Gamepad_shutdown();
		Gamepad_eventDispatcher()->registerForEvent(Gamepad_eventDispatcher(), GAMEPAD_EVENT_DEVICE_ATTACHED, on_device_attached, NULL);
		Gamepad_eventDispatcher()->registerForEvent(Gamepad_eventDispatcher(), GAMEPAD_EVENT_DEVICE_REMOVED, on_device_removed, NULL);
		Gamepad_init();
		canupdate = true;
		Gamepad_detectDevices();
}

void GamepadController::OnAttach(int id, std::string description, unsigned int vendor_id, unsigned int product_id, void* data)
{
	Gamepad_device* devinfo = (Gamepad_device*)data;
	m_HasGamepad = true;
	if (id < auxgamepads_.size())
	{
		if (auxgamepads_[id]->vendor_id == vendor_id && auxgamepads_[id]->product_id == product_id)
		{
		//	logger_.Information("Re-discovered controller id %d: '%s'.", id, auxgamepads_[id]->GetDescription().c_str());
		}
		else
		{
			logger_.Information("Replaced controller id %d: '%s'.", id, auxgamepads_[id]->GetDescription().c_str());
		}
	}
	else
	{
		AuxGamepad* pad = new AuxGamepad(m_Kernel, this, id);
		auxgamepads_.push_back(pad);
		m_Gamepads.push_back(std::unique_ptr<Gamepad>(pad));
		if (devinfo)
			logger_.Information("Found controller '%s' (id: %d)", devinfo->description, id);
	}

	if (devinfo)
	{
		auxgamepads_[id]->SetDescription(description);
		auxgamepads_[id]->vendor_id = vendor_id;
		auxgamepads_[id]->product_id = product_id;
		auxgamepads_[id]->button_count = devinfo->numButtons;
		auxgamepads_[id]->axis_count = devinfo->numAxes;
		auxgamepads_[id]->devinfo = devinfo;
		auxgamepads_[id]->controller_index = id;
	}

}

void GamepadController::OnDetach(int id)
{
	//Discover();
	//if (id < auxgamepads_.size())
	//	auxgamepads_.erase(auxgamepads_.begin() + i);
}

void GamepadController::OnBtnEvent(int controller, int button, Pxf::Input::Gamepad::EventType type)
{
	auxgamepads_[controller]->OnBtnEvent(button, type);
}

void GamepadController::OnAxisEvent(int controller, int axis, Pxf::Input::Gamepad::EventType type)
{
	auxgamepads_[controller]->OnAxisEvent(axis, type);
}

void GamepadController::OnAxisChange(int controller, int axis, float delta)
{
	AuxGamepad* pad = auxgamepads_[controller];
	pad->OnAxisChange(axis, delta);
}

// 
// AuxMidiDevice
//

AuxMidiDevice::AuxMidiDevice(Pxf::Kernel* krn, Pxf::Input::InputController* inp, int index)
	: Pxf::Input::MidiDevice(krn, inp)
	, index(index)
	, inputstream(0)
{
	logger_ = m_Kernel->GetLogger("mididev");
	const PmDeviceInfo* devinfo = Pm_GetDeviceInfo(index);
	if (devinfo)
	{
		num_inputs = devinfo->input;
		num_outputs = devinfo->output;
		SetDescription(devinfo->name);
	}
}

void AuxMidiDevice::Activate()
{
	if (num_inputs <= 0 || is_activated_)
		return;

	if (inputstream)
		Deactivate();

	PmError err = Pm_OpenInput(&inputstream, index, NULL, 10, NULL, NULL);
	if (err)
	{
		char error_text[4096];
		Pm_GetHostErrorText(error_text, 4095);
		logger_.Debug("Attempted to open MIDI device id %d, '%s'", index, GetDescription().c_str());
		logger_.Debug("%s", Pm_GetErrorText(err));
		logger_.Debug("%s", error_text);
		inputstream = 0;
	}
	//Pm_SetFilter(inputstream, PM_FILT_SYSTEMCOMMON);
	is_activated_ = true;
}

void AuxMidiDevice::Deactivate()
{
	if (inputstream)
	{
		Pm_Abort(inputstream);
		inputstream = 0;
		is_activated_ = false;
	}
}

void AuxMidiDevice::Update()
{
	if (!inputstream)
		return;
	if (!is_activated_)
		return;
	if (Pm_CountDevices() <= 0)
	{
		is_activated_ = false;
		return;
	}

	PmEvent events[10];
	int num = Pm_Read(inputstream, events, 10);
	for(int i = 0; i < num; i++)
	{
		MidiDevice::MidiMessage msg;
		PmMessage message = events[i].message;
		PmTimestamp timestamp = events[i].timestamp;
		unsigned char status = Pm_MessageStatus(message);
		unsigned char data1 = Pm_MessageData1(message);
		unsigned char data2 = Pm_MessageData2(message);
		msg.time = timestamp;
		msg.status = status;
		msg.data1 = data1;
		msg.data2 = data2;
		msgs.Put(msg);
	}
}

AuxMidiDevice::~AuxMidiDevice()
{
	if (inputstream)
		Pm_Close(inputstream);
}


//
// MidiDeviceController
//

MidiDeviceController::MidiDeviceController(Pxf::Kernel* _Kernel)
	: Pxf::Input::InputController(_Kernel, "Midi Input Controller")
	, default_input(0)
	, default_output(0)
{
	logger_ = _Kernel->GetLogger("midictrl");
	m_HasKeyboard = false;
	m_HasMouse = false;
	m_HasGamepad = false;
	m_HasMidiDevice = false;
}

MidiDeviceController::~MidiDeviceController()
{
	Pm_Terminate();
}

void MidiDeviceController::Update()
{
	for(int i = 0; i < m_AuxMidiDevices.size(); i++)
	{
		AuxMidiDevice* dev = m_AuxMidiDevices[i];
		if (dev->IsActivated())
			dev->Update();
	}
}

void MidiDeviceController::Discover()
{
	for(int i = 0; i < m_AuxMidiDevices.size(); i++)
	{
		if (m_AuxMidiDevices[i]->IsActivated())
			m_AuxMidiDevices[i]->Deactivate();
	}

	Pm_Terminate();
	Pm_Initialize();

	default_input = Pm_GetDefaultInputDeviceID();
	default_output = Pm_GetDefaultOutputDeviceID();

	int num_devices = Pm_CountDevices();
	const PmDeviceInfo* devinfo = 0;

	if (num_devices <= 0)
	{
		logger_.Information("No MIDI controllers found.");
		return;
	}

	for(int i = 0; i < num_devices; i++)
	{
		devinfo = Pm_GetDeviceInfo(i);
		if (devinfo)
			OnAttach(i, devinfo->name);
	}
}

void MidiDeviceController::OnAttach(int id, std::string description)
{
	m_HasMidiDevice = true;
	if (id < m_AuxMidiDevices.size())
	{
		const PmDeviceInfo* devinfo = Pm_GetDeviceInfo(id);
		if (devinfo)
		{
			m_AuxMidiDevices[id]->num_inputs = devinfo->input;
			m_AuxMidiDevices[id]->num_outputs = devinfo->output;
		}
		else
			return;

		if (m_AuxMidiDevices[id]->GetDescription() == description)
		{
			//logger_.Information("Re-discovered midi controller id %d: '%s'.", id, description.c_str());
		}
		else
		{
			m_AuxMidiDevices[id]->SetDescription(description);
			logger_.Information("Replaced midi controller id %d: '%s'.", id, description.c_str());
		}
		m_AuxMidiDevices[id]->index = id;
	}
	else
	{
		AuxMidiDevice* midi = new AuxMidiDevice(m_Kernel, this, id);
		midi->SetDescription(description);
		m_AuxMidiDevices.push_back(midi);
		m_MidiDevices.push_back(std::unique_ptr<MidiDevice>(midi));
		const PmDeviceInfo* devinfo = Pm_GetDeviceInfo(id);
		if (devinfo->input > 0 && devinfo->output == 0)
			logger_.Information("%.2d. Found MIDI input/output device '%s'", id, devinfo->name);
		else if (devinfo->input > 0)
			logger_.Information("%.2d. Found MIDI input device '%s'", id, devinfo->name);
		else if (devinfo->output > 0)
			logger_.Information("%.2d. Found MIDI output device '%s'", id, devinfo->name);
	}
}

int MidiDeviceController::GetIntOption(const std::string& option)
{
	if (option == "default_output")
		return default_output;
	else if (option == "default_input")
		return default_input;
	return -1;
}
