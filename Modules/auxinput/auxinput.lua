Import("../../Builder.lua")


module = NewModule("auxinput")
module:RequireLibrary("gamepad")
module:RequireLibrary("portmidi")
module:AddIncludeDirectory("Include")
module:AddSourceDirectory("Source/*.cpp")
