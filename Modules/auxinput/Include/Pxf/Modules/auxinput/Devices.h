#ifndef _PXF_MODULE_AUXINPUT_DEVICES_H_
#define _PXF_MODULE_AUXINPUT_DEVICES_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Input/InputDevice.h>

//
// Aux Gamepad
//

class Gamepad_device;

class AuxGamepad : public Pxf::Input::Gamepad
{
public:
	int controller_index;
	int button_count;
	int axis_count;
	Gamepad_device* devinfo;
	// values of last update; axis event detection
	std::vector<float> m_AxisMemory;

	// previous value for outside usage
	std::vector<float> m_AxisMemoryPrev;

	std::vector<Gamepad::EventType> m_AxisStates;

	AuxGamepad(Pxf::Kernel* krn, Pxf::Input::InputController* inp, int ctrlidx);
	virtual ~AuxGamepad();

	virtual int ButtonCount();
	virtual int AxisCount();
	virtual int GetButtonStatus(int idx);
	virtual float GetAxisValue(int idx);
	virtual float GetPreviousAxisValue(int idx);
	virtual AxisStatus GetAxisStatus(int idx);
	virtual void Activate();
	virtual void Deactivate();

	void OnBtnEvent(int button, Pxf::Input::Gamepad::EventType ev);
	void OnAxisEvent(int axis, Pxf::Input::Gamepad::EventType type);
	void OnAxisChange(int axis, float delta);

};

//
// Aux Midi device
//
class AuxMidiDevice : public Pxf::Input::MidiDevice
{
public:
	Pxf::Logger logger_;
	int index;
	void* inputstream;
	AuxMidiDevice(Pxf::Kernel* krn, Pxf::Input::InputController* inp, int index);
	virtual ~AuxMidiDevice();

	virtual void Activate();
	virtual void Deactivate();
	void Update();
};

//
// Gamepad controller
//

class GamepadController : public Pxf::Input::InputController
{
private:
	Pxf::Logger logger_;
	std::vector<AuxGamepad*> auxgamepads_;
	bool canupdate;
	virtual bool Init()
	{   return true;	}
public:
	GamepadController(Pxf::Kernel* _Kernel);
	virtual ~GamepadController();
	virtual void Update();
	virtual void Discover();

	void OnAttach(int id, std::string description, unsigned int vendor_id, unsigned int product_id, void* data);
	void OnDetach(int id);

	void OnBtnEvent(int controller, int button, Pxf::Input::Gamepad::EventType ev);
	void OnAxisEvent(int controller, int axis, Pxf::Input::Gamepad::EventType type);
	void OnAxisChange(int controller, int axis, float delta);

	virtual int GetIntOption(const std::string& option) { return -1; }
	virtual float GetFloatOption(const std::string& option) { return -1; }
};

//
// MidiDevice controller
//

class MidiDeviceController : public Pxf::Input::InputController
{
private:
	Pxf::Logger logger_;
	std::vector<AuxMidiDevice*> m_AuxMidiDevices;
	int default_input;
	int default_output;

	virtual bool Init()
	{   return true;	}
public:
	MidiDeviceController(Pxf::Kernel* _Kernel);
	virtual ~MidiDeviceController();
	virtual void Update();
	virtual void Discover();
	virtual void OnAttach(int id, std::string description);

	virtual int GetIntOption(const std::string& option);
	virtual float GetFloatOption(const std::string& option) { return -1; }
};

#endif // _PXF_MODULE_AUXINPUT_DEVICES_H_
