#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Modules/mesh/MeshLoader.h>

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>

using namespace Pxf;
using namespace Modules;

OpenCTMMesh::OpenCTMMesh(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
	: Resource::Mesh(_Kernel, _Chunk,_Loader)
{
	m_Logger = Logger(_Kernel, "res");
}

OpenCTMMesh::~OpenCTMMesh()
{
}


bool OpenCTMMesh::Build()
{
	// TODO: This will cause resource reloading not to work for meshes. Uh-oh.
	return true;
}

/*
void OpenCTMMesh::SetData(unsigned int _VertCount, unsigned int _TriCount,const float* _Vertices,const unsigned int* _Indices,const float* _Normals)
{
	m_MeshData.vertex_count = _VertCount;
	m_MeshData.triangle_count = _TriCount;
	m_MeshData.vertices = _Vertices;
	m_MeshData.normals = _Normals;
	m_MeshData.indices= _Indices;

	if(_Normals)
		m_MeshData.has_normals = true;
}*/

CtmMeshLoader::CtmMeshLoader(Pxf::Kernel* _Kernel)
	: MeshLoader(_Kernel, "Ctm Mesh Loader")
{
	m_Logger = Logger(_Kernel, "res");
	Init();
}

CtmMeshLoader::~CtmMeshLoader()
{
	ctmFreeContext(m_Context);
}

bool CtmMeshLoader::Init()
{
	m_Context = ctmNewContext(CTM_IMPORT);

	if(ctmGetError(m_Context) == CTM_NONE)
	{
		return true;
	}
	else
	{
		m_Logger.Error("Unable to create OpenCTM Context");
		return false;
	}
}

Resource::Mesh* CtmMeshLoader::Load(const char* _FilePath)
{
	ctmLoad(m_Context, _FilePath);
	Resource::Chunk* _Chunk = Resource::LoadFile(_FilePath);

	// something failed during load
	if(!_Chunk)
	{
		m_Logger.Error("Error loading model %s",_FilePath);
		return 0;
	}

	// something is wrong with context
	if(ctmGetError(m_Context) == CTM_NONE)
	{
		// no errors found
		CTMuint _VertCount			= 0;
		CTMuint	_TriCount			= 0;
		int	_UVMapCount				= 0;
		const CTMuint*	_Indices	= 0;
		const CTMfloat* _Vertices	= 0;
		const CTMfloat* _Normals	= 0;
		const CTMfloat* _UVMap	= 0;
		bool _HasNormals			= false;
		bool _HasUVMap			= false;

		_VertCount = ctmGetInteger(m_Context, CTM_VERTEX_COUNT);
		_Vertices = ctmGetFloatArray(m_Context, CTM_VERTICES);
		_TriCount = ctmGetInteger(m_Context, CTM_TRIANGLE_COUNT);
		_Indices = ctmGetIntegerArray(m_Context, CTM_INDICES);
		_UVMapCount = ctmGetInteger(m_Context, CTM_UV_MAP_COUNT);

		if(ctmGetInteger(m_Context, CTM_HAS_NORMALS) == CTM_TRUE)
		{
			_HasNormals = true;
			_Normals = ctmGetFloatArray(m_Context,CTM_NORMALS);
		}

		if (_UVMapCount > 0)
		{
			_HasUVMap = true;
			_UVMap = ctmGetFloatArray(m_Context, CTM_UV_MAP_1);
		}

		OpenCTMMesh* _NewMesh = new OpenCTMMesh(m_Kernel, _Chunk,this);

		Resource::Mesh::mesh_descriptor _Data;
		_Data.has_normals = _HasNormals;
		_Data.vertex_count = _VertCount;
		_Data.triangle_count = _TriCount;
		_Data.has_uvmap = _HasUVMap;

		float *n_vertices, *n_normals, *n_uvmap;
		unsigned int* n_indices;

		size_t vertsize = 3 * sizeof(float)*_VertCount;
		size_t normsize = 3 * sizeof(float)*_VertCount;
		size_t uvsize = 2 * sizeof(float)*_VertCount;
		size_t indsize = 3 * sizeof(unsigned int) * _TriCount;
		n_vertices = new float[vertsize];
		std::copy(_Vertices, _Vertices+vertsize, n_vertices);
		if (_HasNormals)
		{
			n_normals = new float[vertsize];
			std::copy(_Normals, _Normals+vertsize, n_normals);
		}
		if (_HasUVMap)
		{
			n_uvmap = new float[uvsize];
			std::copy(_UVMap, _UVMap+uvsize, n_uvmap);
		}

		n_indices = new unsigned int[indsize];
		std::copy(_Indices, _Indices+indsize, n_indices);

		_Data.vertices = n_vertices;
		_Data.normals = n_normals;
		_Data.indices= n_indices;
		_Data.texcoords = n_uvmap;

		m_Logger.Information("Finished loading model %s", _FilePath);

		_NewMesh->SetData(_Data);

		return _NewMesh;
	}
	else
	{
		m_Logger.Error("Error loading model %s",_FilePath);
		delete _Chunk;
		return 0;
	}
}

Resource::Mesh* CtmMeshLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	// TODO, XXX: Move file loading logic here, and just do file reading in the method above
	return 0;
}

