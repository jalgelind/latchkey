#ifndef _PXF_RESOURCE_MODELLOADER_H_
#define _PXF_RESOURCE_MODELLOADER_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Resource/Chunk.h>
#include <stb_truetype.h>

namespace Pxf{
namespace Modules {
	class TruetypeFont : public Resource::Font
	{
	protected:
		Logger m_Logger;
		virtual void Cleanup();
		stbtt_fontinfo font;
		unsigned m_TextureSize;
		int m_FontSizeHint;
		std::shared_ptr<Pxf::Resource::Image> m_Image;
	public:
		TruetypeFont(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader);
		virtual bool Build();
		virtual std::shared_ptr<Pxf::Resource::Image> CreateImage(int _FontSize);
		virtual void Reload();
		virtual ~TruetypeFont();
		int64 GetMemoryUsage();
	};

	class TruetypeFontLoader : public Resource::FontLoader
	{
	private:
		Logger m_Logger;
		bool Init();
	public:
		TruetypeFontLoader(Pxf::Kernel* _Kernel);
		virtual ~TruetypeFontLoader();
		virtual Resource::Font* Load(const char* _FilePath);
		virtual Resource::Font* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
	};

} // Graphics
} // Pxf

#endif //_PXF_RESOURCE_MODELLOADER_H_

