Import("../../Builder.lua")

module = NewModule("ttf")
module:RequireLibrary("stb_truetype")
module:AddIncludeDirectory("Include")
module:AddSourceDirectory("Source/*.cpp")
