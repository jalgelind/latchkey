#include <Pxf/Kernel.h>
#include <Pxf/System.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Module.h>
#include <Pxf/Modules/ttf/ttf.h>
#include <Pxf/Modules/ttf/TruetypeFont.h>

static const unsigned Module_Kernel_Version = Pxf::Kernel::KERNEL_VERSION;
static const unsigned Module_Api_Version = Pxf::Module::MODULE_VERSION;

using Pxf::Modules::TruetypeFontImporter;
#ifndef CONF_MODULAR
REGISTER_MODULE(TruetypeFontImporter, "ttf", Module_Kernel_Version, Module_Api_Version);
#endif

namespace TTFLoader_
{
	PXFEXPORT Pxf::Module* CreateInstance()
	{
		Pxf::Modules::TruetypeFontImporter *m = new Pxf::Modules::TruetypeFontImporter("ttf", Module_Kernel_Version, Module_Api_Version);
		return m;
	}

	PXFEXPORT void DestroyInstance(Pxf::Module* _module)
	{
		if (_module)
		{
			delete _module;
		}
	}
}

bool Pxf::Modules::TruetypeFontImporter::RegisterSystem(Pxf::Kernel* _Kernel, unsigned _SystemType)
{
	if (_SystemType & Pxf::System::RESOURCE_LOADER)
	{
		Pxf::Modules::TruetypeFontLoader* loader = new Pxf::Modules::TruetypeFontLoader(_Kernel);
		_Kernel->RegisterResourceLoader("ttf", loader);
		return true;
	}

	return false;
}

