#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Modules/ttf/TruetypeFont.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/Chunk.h>

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>

using namespace Pxf;
using namespace Modules;

TruetypeFont::TruetypeFont(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
	: Resource::Font(_Kernel, _Chunk,_Loader)
	, m_TextureSize(512)
	, m_FontSizeHint(12)
	, m_Image(0)
{
	m_Logger = Logger(_Kernel, "res");
	m_FontInfo.size_restriction = false;
}

TruetypeFont::~TruetypeFont()
{
	Cleanup();
}

void TruetypeFont::Cleanup()
{
	SafeDelete(m_Chunk);
}

void TruetypeFont::Reload()
{
	Cleanup();
	Build();
}

bool TruetypeFont::Build()
{
	stbtt_bakedchar cdata[256]; // ASCII 32..126 is 95 glyphs
	if (m_FontSizeHint > 50)
		m_TextureSize = 1024;
	else
		m_TextureSize = 512;
	auto data = (const unsigned char*)m_Chunk->data;
	auto temp_bitmap = new unsigned char[m_TextureSize * m_TextureSize];
	int ascent, descent, linegap;
	stbtt_InitFont(&font, data, stbtt_GetFontOffsetForIndex(data, 0));
	stbtt_BakeFontBitmap(data, 0, m_FontSizeHint, temp_bitmap,m_TextureSize,m_TextureSize, 32,256-32, cdata); // no guarantee this fits!
	stbtt_GetFontVMetrics(&font, &ascent, &descent, &linegap);
	float scale = stbtt_ScaleForPixelHeight(&font, m_FontSizeHint);
	m_FontInfo.name = Pxf::Path::GetFileName(m_Chunk->source);
	m_FontInfo.size_px = m_FontSizeHint;
	m_FontInfo.size_restriction = false;
	m_FontInfo.line_height = m_FontSizeHint;
	m_FontInfo.newline_offset = (linegap+descent)*scale;
	
	// Character data
	int num_chars = 256;
	m_CharInfo.clear();
	m_CharInfo.resize(num_chars);
	std::memset(&m_CharInfo[0], 0, sizeof(m_CharInfo[0]) * m_CharInfo.size());
	for(int i = 0; i < 256-32; i++)
	{
		float ipw = 1.0f / m_TextureSize, iph = 1.0f / m_TextureSize;
		int id = i + 32;
		stbtt_bakedchar* bch = cdata + i;
		m_CharInfo[id].Width	= bch->x1 - bch->x0;
		m_CharInfo[id].Height   = bch->y1 - bch->y0;
		m_CharInfo[id].XOffset  = floor(bch->xoff + 0.5);
		m_CharInfo[id].YOffset  = floor(bch->yoff + m_FontInfo.size_px + (descent*scale) + 0.5);
		m_CharInfo[id].XAdvance = bch->xadvance;
		m_CharInfo[id].Tx1	  = bch->x0 * ipw;
		m_CharInfo[id].Ty1	  = bch->y0 * iph;
		m_CharInfo[id].Tx2	  = bch->x1 * ipw;
		m_CharInfo[id].Ty2	  = bch->y1 * iph;
	}

	// Get kernings (unsure how to query for all pairs, so...)
	for(int i = 0; i < num_chars; i++)
	{
		for(int o = 0; o < num_chars; o++)
		{
			KerningPair_t pair;
			pair.first = i;
			pair.second = o;
			pair.offset = stbtt_GetCodepointKernAdvance(&font, pair.first, pair.second);
			if (pair.offset != 0)
				m_KerningPairs.push_back(pair);
		}
	}

	// Create image based on the bitmap created by stb_truetype
	Resource::ResourceManager* mgr = m_Kernel->GetResourceManager();
	m_Image = mgr->CreateImageFromRaw(m_TextureSize, m_TextureSize, 1, temp_bitmap, "ttf-fontmap");
	m_Initialized = true;
	delete [] temp_bitmap;
	return true;
}

std::shared_ptr<Pxf::Resource::Image> TruetypeFont::CreateImage(int _FontSize)
{
	if (_FontSize != m_FontSizeHint || !m_Initialized)
	{
		m_FontSizeHint = _FontSize;
		Build();
	}
	return m_Image;
}


int64 TruetypeFont::GetMemoryUsage()
{
	int64 sum = 0;
	if (m_Chunk && !m_Chunk->is_static)
		sum += m_Chunk->size;
	if (m_Image)
		sum += m_Image->GetChunk()->size;
	return sizeof(TruetypeFont) + sizeof(Resource::Chunk) + sum;
}

TruetypeFontLoader::TruetypeFontLoader(Pxf::Kernel* _Kernel)
	: FontLoader(_Kernel, "TrueType Font Loader")
{
	m_Logger = Logger(_Kernel, "res");
	Init();
}

TruetypeFontLoader::~TruetypeFontLoader()
{
}

bool TruetypeFontLoader::Init()
{
	return true;
}

Resource::Font* TruetypeFontLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = Resource::LoadFile(_FilePath);
	if (!chunk)
		return NULL;
	return new TruetypeFont(m_Kernel, chunk, this);
}

Resource::Font* TruetypeFontLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	chunk->source = _Path ? _Path : "unknown";
	chunk->data = (void*) _DataPtr;
	chunk->size = _DataLen;
	chunk->is_static = true;
	return new TruetypeFont(m_Kernel, chunk, this);
}

