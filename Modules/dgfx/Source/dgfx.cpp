#include <Pxf/Kernel.h>
#include <Pxf/System.h>
#include <Pxf/Module.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Modules/dgfx/dgfx.h>
#include <Pxf/Modules/dgfx/Device.h>
#include <Pxf/Modules/dgfx/InputDevice.h>

static const unsigned Module_Kernel_Version = Pxf::Kernel::KERNEL_VERSION;
static const unsigned Module_Api_Version = Pxf::Module::MODULE_VERSION;

using Pxf::Modules::DesktopGraphics;
#ifndef CONF_MODULAR
REGISTER_MODULE(DesktopGraphics, "dgfx", Module_Kernel_Version, Module_Api_Version);
#endif

namespace PortableRenderInput_
{
	PXFEXPORT Pxf::Module* CreateInstance()
	{
		Pxf::Modules::DesktopGraphics *m = new Pxf::Modules::DesktopGraphics("dgfx", Module_Kernel_Version, Module_Api_Version);
		return m;
	}

	PXFEXPORT void DestroyInstance(Pxf::Module* _module)
	{
		if (_module)
		{
			delete _module;
		}
	}
}

bool Pxf::Modules::DesktopGraphics::RegisterSystem(Pxf::Kernel* _Kernel, unsigned _SystemType)
{
	bool retval = false;

	if (_SystemType & Pxf::System::GRAPHICS_DEVICE)
	{
		_Kernel->RegisterGraphicsDevice(Pxf::make_unique<Pxf::Modules::DeviceGL2>(_Kernel));
		retval = true;
	}

	if (_SystemType & Pxf::System::INPUT_CONTROLLER)
	{
		_Kernel->RegisterInputController(Pxf::make_unique<Pxf::Modules::InputControllerGLFW>(_Kernel));
		retval = true;
	}

	return retval;
}

