#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/String.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Modules/dgfx/InputDevice.h>

#include <algorithm>

#include <cstring>
#include <string>

#ifdef CONF_WITH_LIBRARY_GLFW3
#include <GLFW/glfw3.h>
#else
#include <GL/glfw.h>
#endif

using namespace Pxf::Modules;

using namespace Pxf;
using namespace Pxf::Input;
using namespace Pxf::Graphics;
using std::string;

// -- GLFW callbacks
void InputControllerGLFW::char_callback(GLFWwindow* window, unsigned int _Char)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(window);
	GLFWKeyboard* kbd = g_GLFWInputController->m_GLFWKeyboard;
	kbd->GetCharQueue().Put(Keyboard::Event(_Char, Keyboard::Event::KEY_PRESS, wnd));
}

void InputControllerGLFW::key_callback(GLFWwindow* window, int _Key, int _Scancode, int _Action, int _Modifiers)
{
	GLFWKeyboard* kbd = g_GLFWInputController->m_GLFWKeyboard;
	if (_Action == GLFW_REPEAT && kbd->m_Keyrepeat == false)
		return;
	Window* wnd = (Window*)glfwGetWindowUserPointer(window);
	Keyboard::Event::Type action = _Action == GLFW_PRESS ? Keyboard::Event::KEY_PRESS : Keyboard::Event::KEY_RELEASE;
	kbd->GetKeyQueue().Put(Keyboard::Event(_Key, action, wnd));
}
void InputControllerGLFW::mouse_callback(GLFWwindow* window, int _Button, int _Action, int _Modifiers)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(window);
	Mouse::Event last_ev = g_GLFWInputController->m_Mouse->m_LastEvent;
	Mouse::Event ev;
	ev.window = wnd;
	ev.type = Mouse::Event::BUTTON;

	ev.x = last_ev.x;
	ev.y = last_ev.y;
	ev.dx = 0;
	ev.dy = 0;
	ev.scroll_x = last_ev.scroll_x;
	ev.scroll_y = last_ev.scroll_y;
	ev.scroll_dx = 0;
	ev.scroll_dy = 0;

	Input::MouseButton btn = (Input::MouseButton)(_Button + MOUSE_1);

	if (_Action == GLFW_PRESS)
	{
		g_GLFWInputController->m_GLFWMouse->last_button = btn;
		g_GLFWInputController->m_GLFWMouse->mouse_buttons |= (1<<_Button); // set bit = 1
		ev.mouse_pressed = btn;
	}
	else
	{
		g_GLFWInputController->m_GLFWMouse->last_button_released = btn;
		g_GLFWInputController->m_GLFWMouse->mouse_buttons &= ~(1<<_Button); // set bit = 0
		ev.mouse_released = btn;
	}
	g_GLFWInputController->m_Mouse->m_EventQueue.Put(ev);
	g_GLFWInputController->m_Mouse->m_LastEvent = ev;
}

void InputControllerGLFW::mousewheel_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(window);
	Mouse::Event last_ev = g_GLFWInputController->m_Mouse->m_LastEvent;
	Mouse::Event ev;
	ev.window = wnd;
	ev.type = Mouse::Event::SCROLL;
	ev.scroll_dx = xoffset;
	ev.scroll_dy = yoffset;
	ev.scroll_x = last_ev.scroll_x + xoffset;
	ev.scroll_y = last_ev.scroll_y + yoffset;

	ev.x = last_ev.x;
	ev.y = last_ev.y;
	ev.dx = 0;
	ev.dy = 0;

	// TODO: Remove legacy scroll counter
	int pos = (int)yoffset;
	if (pos > 0)
		g_GLFWInputController->m_GLFWMouse->mouse_wheel_delta++;
	else
		g_GLFWInputController->m_GLFWMouse->mouse_wheel_delta--;

	g_GLFWInputController->m_Mouse->m_EventQueue.Put(ev);
	g_GLFWInputController->m_Mouse->m_LastEvent = ev;
}

void InputControllerGLFW::mousepos_callback(GLFWwindow* window, double x, double y)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(window);
	Mouse::Event last_ev = g_GLFWInputController->m_Mouse->m_LastEvent;
	Mouse::Event ev;
	ev.type = Mouse::Event::POSITION;
	ev.window = wnd;
	ev.x = x;
	ev.y = y;
	ev.dx = x - last_ev.x;
	ev.dy = y - last_ev.y;

	ev.scroll_x = last_ev.scroll_x;
	ev.scroll_y = last_ev.scroll_y;
	ev.scroll_dx = 0;
	ev.scroll_dy = 0;

	g_GLFWInputController->m_Mouse->m_EventQueue.Put(ev);
	g_GLFWInputController->m_Mouse->m_LastEvent = ev;
}

// END -- GLFW callbacks

InputControllerGLFW::InputControllerGLFW(Pxf::Kernel* _Kernel)
			: Pxf::Input::InputController(_Kernel, "GLFW Input Device")
			, m_GLFWKeyboard(0)
			, m_GLFWMouse(0)
{
	// Singleton hack for our GLFW callbacks to work
	g_GLFWInputController = this;

	m_GLFWKeyboard = new GLFWKeyboard(_Kernel, this);
	m_Keyboard = std::unique_ptr<Keyboard>(m_GLFWKeyboard);
	m_HasKeyboard = true;
	m_GLFWMouse = new GLFWMouse(_Kernel, this);
	m_Mouse = std::unique_ptr<Mouse>(m_GLFWMouse);
	m_HasMouse = true;

	// Reset state vars
	m_GLFWMouse->last_button = 0;
	m_GLFWMouse->last_button_released = 0;
	m_GLFWMouse->mouse_x = 0;
	m_GLFWMouse->mouse_y = 0;
	m_GLFWMouse->mouse_mode = MODE_ABSOLUTE;
	m_GLFWMouse->mouse_buttons = 0;

	m_Logger = Logger(GetKernel(), "inp");
}

InputControllerGLFW::~InputControllerGLFW()
{
	g_GLFWInputController = NULL;
	
	for(auto handle: m_WindowHandles)
	{
		glfwSetCharCallback(handle, NULL);
		glfwSetKeyCallback(handle, NULL);
		glfwSetMouseButtonCallback(handle, NULL);
		glfwSetScrollCallback(handle, NULL);
		glfwSetCursorPosCallback(handle, NULL);
	}
	m_WindowHandles.clear();
}

InputControllerGLFW* InputControllerGLFW::GetInstance()
{
	return g_GLFWInputController;
}

void InputControllerGLFW::RegisterWindowHandle(GLFWwindow* handle)
{
	m_WindowHandles.push_back(handle);
	glfwSetCharCallback(handle, char_callback);
	glfwSetKeyCallback(handle, key_callback);
	glfwSetMouseButtonCallback(handle, mouse_callback);
	glfwSetScrollCallback(handle, mousewheel_callback);
	glfwSetCursorPosCallback(handle, mousepos_callback);
}

void InputControllerGLFW::UnRegisterWindowHandle(GLFWwindow* handle)
{
	glfwSetCharCallback(handle, NULL);
	glfwSetKeyCallback(handle, NULL);
	glfwSetMouseButtonCallback(handle, NULL);
	glfwSetScrollCallback(handle, NULL);
	glfwSetCursorPosCallback(handle, NULL);
	m_WindowHandles.erase(std::remove(m_WindowHandles.begin(), m_WindowHandles.end(), handle), m_WindowHandles.end());
}

void InputControllerGLFW::Update()
{
	glfwPollEvents();
}

void InputControllerGLFW::Discover()
{

}


bool GLFWKeyboard::IsKeyDown(int _key)
{
	//GLFWTODO
	GLFWwindow* handle = g_GLFWInputController->GetInstance()->m_WindowHandles[0];
	return glfwGetKey(handle, _key) == GLFW_PRESS;
}

bool GLFWMouse::IsButtonDown(int _button)
{
	//GLFWTODO
	GLFWwindow* handle = g_GLFWInputController->GetInstance()->m_WindowHandles[0];
	return glfwGetMouseButton(handle, _button - MOUSE_1) == GLFW_PRESS;
}

// XXX: Skall den nollst�lla h�r, eller borde det g�ras i Update()?
int GLFWMouse::GetMouseWheelDelta()
{
	int delta = mouse_wheel_delta;
	mouse_wheel_delta = 0;
	return delta;
}

void GLFWMouse::GetMousePos(int *x, int *y)
{
	GLFWwindow* handle = g_GLFWInputController->GetInstance()->m_WindowHandles[0];
	if (mouse_mode == MODE_RELATIVE)
	{
		double new_x = 0, new_y = 0;
		//GLFWTODO
		glfwGetCursorPos(handle, &new_x, &new_y);
		//GLFWTODO: why doubles?
		*x = int(new_x) - mouse_x;
		*y = int(new_y) - mouse_y;
		mouse_x = new_x;
		mouse_y = new_y;
	}
	else
	{
		//GLFWTODO
		double new_x = 0, new_y = 0;
		glfwGetCursorPos(handle, &new_x, &new_y);
		*x = int(new_x);
		*y = int(new_y);
	}
}

void GLFWMouse::SetMouseMode(MouseMode _Mode)
{
	GLFWwindow* handle = g_GLFWInputController->GetInstance()->m_WindowHandles[0];
	if (_Mode == MODE_RELATIVE)
	{
		//glfwDisable(GLFW_MOUSE_CURSOR);
		//GLFWTODO
		glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		double new_x = 0, new_y = 0;
		glfwGetCursorPos(handle, &new_x, &new_y);
		mouse_x = int(new_x);
		mouse_y = int(new_y);
	}
	else
	{
		//GLFWTODO
		glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		//glfwEnable(GLFW_MOUSE_CURSOR);
	}

	mouse_mode = _Mode;
}

MouseMode GLFWMouse::GetMouseMode()
{
	return mouse_mode;
}

void GLFWMouse::SetMousePos(int x, int y)
{
	//GLFWTODO
	GLFWwindow* handle = g_GLFWInputController->GetInstance()->m_WindowHandles[0];
	glfwSetCursorPos(handle, x, y);
}

void GLFWMouse::ShowCursor(bool _show)
{
	GLFWwindow* handle = g_GLFWInputController->GetInstance()->m_WindowHandles[0];
	//GLFWTODO
	if (_show)
		//glfwEnable(GLFW_MOUSE_CURSOR);
		glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	else
		//glfwDisable(GLFW_MOUSE_CURSOR);
		glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

int GLFWMouse::GetLastButton()
{
	return last_button;
}

int GLFWMouse::GetLastReleasedButton()
{
	return last_button_released;
}

unsigned GLFWMouse::GetButtonStates()
{
	return mouse_buttons;
}

void GLFWMouse::ClearLastButton()
{
	last_button = 0;
	last_button_released = 0;

}

void GLFWKeyboard::SetKeyRepeat(bool _On)
{
	//GLFWTODO: GLFW_REPEAT is reported to the key callback.
	m_Keyrepeat = _On;
/*
	if (_On)
		glfwEnable(GLFW_KEY_REPEAT);
	else
		glfwDisable(GLFW_KEY_REPEAT);
*/
}

void GLFWKeyboard::SetSystemKeys(bool _On)
{
	//GLFWTODO: not supported anymore...
/*
	if (_On)
		glfwEnable(GLFW_SYSTEM_KEYS);
	else
		glfwDisable(GLFW_SYSTEM_KEYS);
*/
}
