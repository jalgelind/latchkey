#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Platform.h>
#include <Pxf/Modules/dgfx/OpenGL.h>
#include <Pxf/Modules/dgfx/Window.h>
#include <Pxf/Modules/dgfx/InputDevice.h>

#include <string>

#ifdef CONF_PLATFORM_MACOSX
#include <Carbon/Carbon.h>
#endif

using namespace Pxf;
using namespace Pxf::Graphics;
using std::string;

int WindowGL2::GetWidth() { return m_width; }
int WindowGL2::GetHeight() {return m_height; }
float WindowGL2::GetAspectRatio() { return ((float)m_width / (float)m_height); }

void on_window_close(GLFWwindow* w)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(w);
	Window::Event ev;
	ev.type = Window::Event::CLOSE;
	wnd->GetQueue().Put(ev);
}

void on_window_resize(GLFWwindow* w, int x, int y)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(w);
	Window::Event ev;
	ev.type = Window::Event::RESIZE;
	ev.width = x;
	ev.height = y;
	if (wnd->GetQueue().Peek().type == Window::Event::RESIZE)
		wnd->GetQueue().Pop();
	wnd->GetQueue().Put(ev);
}

void on_window_move(GLFWwindow* w, int x, int y)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(w);
	Window::Event ev;
	ev.type = Window::Event::MOVE;
	ev.x = x;
	ev.y = y;
	
	// If an unfocused window is moved, a focus event is first put on the event queue.
	// It's in the way if we only want to report the most recent move event. So we remove it temporarily...
	bool focus = false;
	Window::Event fev;
	if (wnd->GetQueue().Peek().type == Window::Event::FOCUS)
	{
		focus = true;
		wnd->GetQueue().PopTo(fev);
	}

	// ... before we check for any old window events to remove ...
	if (wnd->GetQueue().Peek().type == Window::Event::MOVE)
		wnd->GetQueue().Pop();
	wnd->GetQueue().Put(ev);

	// ... and then we put the focus event back.
	if (focus)
		wnd->GetQueue().Put(fev);
}

void on_window_focus(GLFWwindow* w, int focus)
{
	Window* wnd = (Window*)glfwGetWindowUserPointer(w);
	Window::Event ev;
	ev.type = Window::Event::FOCUS;
	ev.focus = focus == GL_TRUE;
	wnd->GetQueue().Put(ev);
}

WindowGL2::WindowGL2(GraphicsDevice* _Device, WindowDetails *_window_spec)
	: Window(_Device)
{
	m_Logger = Logger(_Device->GetKernel(), "gfx");

	// Window settings
	m_width = _window_spec->Width;
	m_height = _window_spec->Height;
	m_xpos = _window_spec->XPos;
	m_ypos = _window_spec->YPos;
	m_fullscreen = _window_spec->Fullscreen;
	m_resizeable = _window_spec->Resizeable;
	m_vsync = _window_spec->VerticalSync;
	m_fsaa_samples = _window_spec->FSAASamples;
	m_handle = NULL;

	// Colorbits
	switch (_window_spec->ColorBits)
	{
	case 16:
		m_bits_r = 5;
		m_bits_g = 6;
		m_bits_b = 5;
		break;
	case 24:
		m_bits_r = 8;
		m_bits_g = 8;
		m_bits_b = 8;
		break;
	default:
		m_Logger.Error("Could not initiate widow, invalid number of colorbits (%d)", _window_spec->ColorBits);
	}

	// Other buffer bits settings
	m_bits_alpha = _window_spec->AlphaBits;
	m_bits_depth = _window_spec->DepthBits;
	m_bits_stencil = _window_spec->StencilBits;

	// FPS
	m_fps = 0;
	m_fps_count = 0;
	m_fps_laststamp = Platform::GetTime();
}

WindowGL2::~WindowGL2()
{
	Close();
}

bool WindowGL2::Open()
{
	int t_params = 0;

	if (IsOpen())
		return false; // can't open an already open window


	// Enable vertical sync
	if (m_vsync)
		glfwSwapInterval(1);


	// Set number of FSAA samples
	if (m_fsaa_samples > 0)
		glfwWindowHint(GLFW_SAMPLES, m_fsaa_samples);

	glfwWindowHint(GLFW_RESIZABLE, m_resizeable);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	//glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	
	int count;
	GLFWmonitor** monitors = glfwGetMonitors(&count);
	GLFWmonitor* monitor = NULL; // NULL = windowed mode
	if (m_fullscreen)
	{
		//monitor = monitors[0];
		monitor = glfwGetPrimaryMonitor();
	}
	
	GLFWwindow* window = NULL; // share stuff? null = nope.

	glfwWindowHint(GLFW_RED_BITS, m_bits_r);
	glfwWindowHint(GLFW_GREEN_BITS, m_bits_g);
	glfwWindowHint(GLFW_BLUE_BITS, m_bits_b);
	glfwWindowHint(GLFW_ALPHA_BITS, m_bits_alpha);
	glfwWindowHint(GLFW_DEPTH_BITS, m_bits_depth);
	glfwWindowHint(GLFW_STENCIL_BITS, m_bits_stencil);
	
	window = glfwCreateWindow(m_width, m_height, "window title", monitor, window);
	//	if (GL_TRUE == glfwOpenWindow(m_width, m_height, m_bits_r, m_bits_g, m_bits_b, m_bits_alpha, m_bits_depth, m_bits_stencil, t_params))
	if (window != NULL)
	{
		Pxf::Modules::InputControllerGLFW::GetInstance()->RegisterWindowHandle(window);
		glfwSetWindowUserPointer(window, this);
		monitor = monitors[0];
		m_handle = window;
		glfwMakeContextCurrent(window);

		// If we are a window, set title
		if (!m_fullscreen)
		{
			SetTitle("OpenGL");

			const GLFWvidmode* m = glfwGetVideoMode(monitor);
			if (!m)
			{
				m_Logger.Error("Failed to get video mode for monitor.");
				return false;
			}

			int nx = -1, ny = -1;
			nx = (m->width / 2) - (m_width / 2);
			ny = (m->height / 2) - (m_height / 2);

			if (m_xpos > 0)
				nx = m_xpos;

			if (m_ypos > 0)
				ny = m_ypos;

			if (ny >= m->height || ny == -1)
				ny = (m->height / 2) - (m_height / 2);

			if (nx >= m->width || nx == -1)
				nx = (m->width / 2) - (m_width / 2);

			// TODO: Add support for manually set position
			glfwSetWindowPos((GLFWwindow*)m_handle, nx, ny);
		}

#ifdef CONF_PLATFORM_MACOSX
		/* HACK - Get events without bundle */
		ProcessSerialNumber psn;
		GetCurrentProcess(&psn);
		TransformProcessType(&psn,kProcessTransformToForegroundApplication);
		SetFrontProcess(&psn);
#endif

		GLenum err = glewInit();
		glewExperimental = true;
		if (err != GLEW_OK)
		{
			m_Logger.Error("Could not initiate glew.");
		}

		// Map gl-functionality
		Pxf::Graphics::GL::SetupExtensions();

		// Setup callbacks for window eventsw
		glfwSetWindowCloseCallback((GLFWwindow*)m_handle, on_window_close);
		glfwSetWindowPosCallback((GLFWwindow*)m_handle, on_window_move);
		glfwSetWindowSizeCallback((GLFWwindow*)m_handle, on_window_resize);
		glfwSetWindowFocusCallback((GLFWwindow*)m_handle, on_window_focus);

		m_Logger.Information("Opened window of %dx%d@%d (r: %d g: %d b: %d a: %d d: %d s: %d)", m_width, m_height, m_bits_r+m_bits_g+m_bits_b+m_bits_alpha, m_bits_r, m_bits_g, m_bits_b, m_bits_alpha, m_bits_depth, m_bits_stencil);
		m_Logger.Information("OpenGL Vendor  : %s", (const char*)glGetString(GL_VENDOR));
		m_Logger.Information("OpenGL Renderer: %s", (const char*)glGetString(GL_RENDERER));
		m_Logger.Information("OpenGL Version : %s", (const char*)glGetString(GL_VERSION));
		m_Logger.Information("GLSL Version   : %s", (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

		return true;
	}
	else
	{
		m_Logger.Information("Faild to open window with properties %dx%d@%d (r: %d g: %d b: %d a: %d d: %d s: %d)", m_width, m_height, m_bits_r+m_bits_g+m_bits_b+m_bits_alpha, m_bits_r, m_bits_g, m_bits_b, m_bits_alpha, m_bits_depth, m_bits_stencil);
		return false;
	}
}

void WindowGL2::Activate()
{
	glfwMakeContextCurrent((GLFWwindow*)m_handle);
}

bool WindowGL2::Close()
{
	if(!IsOpen())
		return false;
	Pxf::Modules::InputControllerGLFW::GetInstance()->UnRegisterWindowHandle((GLFWwindow*)m_handle);
	glfwSetWindowCloseCallback((GLFWwindow*)m_handle, 0);
	glfwDestroyWindow((GLFWwindow*)m_handle);
	m_handle = NULL;
	return true;
}

void WindowGL2::Swap()
{
	if (IsOpen())
	{
		int64 t_current_time = Platform::GetTime();
		int64 diff = t_current_time - m_fps_laststamp;
		if (diff >= 1000)
		{
			m_fps = m_fps_count;
			m_fps_count = 0;
			m_fps_laststamp = t_current_time;
		}

		glfwSwapBuffers((GLFWwindow*)m_handle);
		m_fps_count += 1;
	}
}

bool WindowGL2::IsOpen()
{
	if (m_handle)
		return true;
	else
		return false;
}

bool WindowGL2::IsActive()
{
	if (IsOpen())
	{
		if (GL_TRUE == glfwGetWindowAttrib((GLFWwindow*)m_handle, GLFW_FOCUSED))
			return true;
	}
	return false;
}

bool WindowGL2::IsMinimized()
{
	if (IsOpen())
	{
		if (GL_TRUE == glfwGetWindowAttrib((GLFWwindow*)m_handle, GLFW_ICONIFIED))
			return true;
	}

	return false;
}

void WindowGL2::SetWindowSize(int _Width, int _Height)
{
	m_width = _Width;
	m_height = _Height;
	glfwSetWindowSize((GLFWwindow*)m_handle, _Width, _Height);
}

void WindowGL2::SetTitle(const char *_title)
{
	if (IsOpen())
	{
		glfwSetWindowTitle((GLFWwindow*)m_handle, _title);
	}
}

int WindowGL2::GetFPS()
{
	return m_fps;
}

const char* WindowGL2::GetContextTypeName()
{
	return "OpenGL";
}

