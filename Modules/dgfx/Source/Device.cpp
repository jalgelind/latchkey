#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/PrimitiveBatch.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Raw.h>
#include <Pxf/Resource/GLSL.h>
#include <Pxf/Util/Profiler.h>

#include <Pxf/Modules/dgfx/Device.h>
#include <Pxf/Modules/dgfx/FrameBuffer.h>
#include <Pxf/Modules/dgfx/OpenGL.h>
#include <Pxf/Modules/dgfx/RenderBuffer.h>
#include <Pxf/Modules/dgfx/Shader.h>
#include <Pxf/Modules/dgfx/Texture.h>
#include <Pxf/Modules/dgfx/VertexBuffer.h>

#include <string>
#include <string.h>
#include <sys/stat.h>

using namespace Pxf;
using namespace Pxf::Graphics;
using namespace Pxf::Modules;
using std::string;

void APIENTRY DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char* message, void* _userParam)
{
	// Skip error about shader recompilation.
	if (id == 131218)
		return;

	char debSource[16], debType[20], debSev[5];

	if(source == GL_DEBUG_SOURCE_API_ARB)
		strcpy(debSource, "OpenGL");
	else if(source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB)
		strcpy(debSource, "Windows");
	else if(source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB)
		strcpy(debSource, "Shader Compiler");
	else if(source == GL_DEBUG_SOURCE_THIRD_PARTY_ARB)
		strcpy(debSource, "Third Party");
	else if(source == GL_DEBUG_SOURCE_APPLICATION_ARB)
		strcpy(debSource, "Application");
	else if(source == GL_DEBUG_SOURCE_OTHER_ARB)
		strcpy(debSource, "Other");

	if(type == GL_DEBUG_TYPE_ERROR_ARB)
		strcpy(debType, "Error");
	else if(type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB)
		strcpy(debType, "Deprecated behavior");
	else if(type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB)
		strcpy(debType, "Undefined behavior");
	else if(type == GL_DEBUG_TYPE_PORTABILITY_ARB)
		strcpy(debType, "Portability");
	else if(type == GL_DEBUG_TYPE_PERFORMANCE_ARB)
		strcpy(debType, "Performance");
	else if(type == GL_DEBUG_TYPE_OTHER_ARB)
		strcpy(debType, "Other");



	if(severity == GL_DEBUG_SEVERITY_HIGH_ARB)
		strcpy(debSev, "High");
	else if(severity == GL_DEBUG_SEVERITY_MEDIUM_ARB)
		strcpy(debSev, "Medium");
	else if(severity == GL_DEBUG_SEVERITY_LOW_ARB)
		strcpy(debSev, "Low");

	Kernel* k = Kernel::GetInstance();
	Logger l = k->GetLogger("GLDBG");
	
	l.Debug("%s | %s| ID:%d | %s: %s\n",debSource,debType,id,debSev,message);

	// Raise breakpoints on errors and undefined behavior.
	if (type == GL_DEBUG_TYPE_ERROR_ARB || type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB )
	{
		#if defined(CONF_COMPILER_MSVC)
			__asm {int 3};
		#elif defined(CONF_COMPILER_GCC)
			asm("int $0x3\n");
		#endif
	}
}

void APIENTRY DebugCallbackAMD(GLuint _id, GLenum _category, GLenum _severity, GLsizei _length, const GLchar* _message, GLvoid* _userParam)
{
	// TODO: Implement this if needed...
	printf("[GLDBG] %s\n",_message);
}


// static helper functions for skipping redundant state changes
static GLenum mode = GL_CULL_FACE;
static void matrixmode(GLenum m)
{
	if (mode != m)
	{
		glMatrixMode(m);
		mode = m;
	}
}

// debug texture
static const char* debug_broken_texture_png_path = "data/images/debug-broken-texture.png";
static const unsigned int debug_broken_texture_png_size = 839;
const unsigned char debug_broken_texture_png[] = {0x89
	,0x50,0x4e,0x47,0xd,0xa,0x1a,0xa,0x0,0x0,0x0,0xd,0x49,0x48,0x44,0x52,0x0,0x0,0x0,0x40,0x0
	,0x0,0x0,0x40,0x8,0x2,0x0,0x0,0x0,0x25,0xb,0xe6,0x89,0x0,0x0,0x0,0x9,0x70,0x48,0x59,0x73
	,0x0,0x0,0xb,0x13,0x0,0x0,0xb,0x13,0x1,0x0,0x9a,0x9c,0x18,0x0,0x0,0x0,0x20,0x63,0x48,0x52
	,0x4d,0x0,0x0,0x7a,0x25,0x0,0x0,0x80,0x83,0x0,0x0,0xf9,0xff,0x0,0x0,0x80,0xe9,0x0,0x0,0x75
	,0x30,0x0,0x0,0xea,0x60,0x0,0x0,0x3a,0x98,0x0,0x0,0x17,0x6f,0x92,0x5f,0xc5,0x46,0x0,0x0,0x2
	,0x59,0x49,0x44,0x41,0x54,0x78,0xda,0xd4,0x9a,0x61,0x67,0x3,0x31,0x18,0xc7,0xff,0xe9,0x47,0x38,0x4a
	,0x29,0x63,0x1f,0x6a,0xdf,0x6e,0x8c,0x31,0x46,0x29,0xa3,0x94,0x51,0x3b,0xae,0xca,0x28,0x63,0x8c,0x31
	,0xf6,0x7a,0xaf,0xf6,0x76,0x2f,0x9f,0xbd,0x48,0x7b,0xae,0x77,0xc9,0x25,0x79,0xf2,0xdc,0x5d,0x12,0xa1
	,0xbd,0x24,0x4f,0xf2,0xfb,0xdd,0x25,0xee,0xae,0xd,0x8,0x5,0x61,0x47,0xa0,0xc,0xf3,0x8e,0x50,0x40
	,0x7f,0x64,0xe8,0x70,0xc2,0x46,0xe3,0xa0,0xcc,0x8d,0xbe,0x24,0x10,0xba,0x45,0xc9,0xe7,0xe7,0x26,0x2a
	,0x8c,0x5a,0xb9,0xd0,0xb7,0x4,0xd2,0x77,0xd0,0xf4,0x55,0xb3,0x10,0x3d,0xd3,0x2b,0x7d,0x7a,0xa3,0x40
	,0x9a,0xe,0x5b,0x23,0xbd,0x4d,0x20,0x35,0x7,0x2b,0x7d,0x8f,0x40,0x3a,0xe,0x5b,0xc2,0xdc,0x46,0xdf
	,0x2f,0x90,0x82,0x83,0x83,0xde,0x29,0x30,0xad,0x83,0x9b,0xde,0x47,0x60,0x2a,0x7,0x2f,0x7a,0x4f,0x81
	,0xf1,0x1d,0xf4,0xaa,0x3d,0xf8,0x34,0x6,0xe3,0xf1,0x23,0x1d,0xfa,0x20,0x81,0x71,0x1c,0xc2,0xe8,0x43
	,0x5,0x86,0x76,0xd0,0xf3,0xfe,0x10,0x14,0x85,0xf0,0x61,0xca,0x61,0x1c,0x36,0xc,0x7a,0x9e,0xc0,0x10
	,0xe,0x4c,0x7a,0xb6,0x80,0xac,0x3,0x9f,0x3e,0x46,0x40,0xca,0x21,0x8a,0x3e,0x52,0xa0,0x76,0x78,0xe1
	,0x86,0x3f,0x11,0xe6,0x84,0xd7,0x18,0x6,0x44,0x4f,0x0,0xf6,0x75,0x10,0xa0,0x17,0x11,0xe0,0x39,0xc8
	,0xd0,0x4b,0x9,0x84,0x3a,0x68,0xfa,0xa3,0xc8,0xd0,0x6e,0x1,0x74,0x92,0xbd,0xfc,0xfc,0x53,0x87,0x25
	,0x44,0xd3,0xdb,0xaa,0x7a,0xa3,0xb8,0x2,0xdd,0x5e,0x6c,0xbd,0xd7,0xe,0xf6,0x2a,0x22,0xac,0x1,0x34
	,0xcf,0x7d,0x5d,0xd5,0x1b,0x15,0x2d,0x0,0x53,0x6a,0x95,0x5f,0xba,0x95,0xf6,0xaa,0xa3,0xbd,0xca,0xdc
	,0xa1,0xbc,0x80,0xb1,0xfc,0x12,0xa5,0x8,0xa5,0x64,0xb,0xcc,0xe0,0x91,0x5a,0x4a,0xa,0xaa,0x5b,0xae
	,0xb,0xcf,0xa3,0xae,0x9b,0xaf,0xe1,0xe7,0xaa,0x63,0xd0,0x40,0x97,0x1d,0x5a,0x93,0xea,0x6f,0xa1,0x3a
	,0xd,0x9a,0xf4,0xf5,0xf7,0xd6,0x61,0xa7,0x6a,0xa1,0xf0,0x13,0x1e,0xe5,0xe5,0xe0,0x75,0x5,0x14,0x54
	,0x9d,0x7d,0xce,0x4a,0x27,0x6d,0x30,0x58,0x9a,0x61,0x8c,0xf4,0x37,0x5c,0xd7,0xe3,0x4c,0xa1,0x42,0xe1
	,0xd7,0xb8,0x58,0x6d,0x3d,0xf8,0x4f,0x21,0xb7,0x80,0xe0,0xd9,0x6a,0x21,0xfa,0x47,0xf1,0xa7,0x10,0xeb
	,0x26,0xb0,0xb2,0xb7,0xdc,0x7b,0xde,0xda,0x3,0x96,0x99,0xf4,0x9b,0xe1,0x8a,0xb0,0x20,0xbc,0xd9,0x9f
	,0x97,0x2a,0xd9,0x11,0x31,0x16,0xbd,0xce,0x95,0xb8,0x3,0x46,0xa4,0x1f,0xc4,0x41,0x4a,0xe0,0xd1,0x8f
	,0x5e,0xde,0x1,0x72,0xf4,0xef,0x21,0x21,0x62,0xe,0x98,0x82,0x5e,0xd2,0x1,0x13,0xd1,0x8b,0x39,0xc4
	,0x8,0x3c,0x10,0x16,0x84,0x8f,0xb8,0x53,0x10,0xeb,0x80,0x49,0xe9,0x5,0x1c,0x30,0x35,0x7d,0xac,0x3
	,0x58,0xf4,0x4b,0x51,0xfa,0x28,0x7,0xa4,0x41,0xcf,0x77,0x40,0x32,0xf4,0x4c,0x7,0x7f,0x81,0xfb,0xe1
	,0xe9,0x39,0xe,0x48,0x8c,0x3e,0xd8,0x1,0xde,0xf4,0x9f,0xe3,0xfe,0xcd,0xea,0xeb,0x80,0x24,0xe9,0x3
	,0x1c,0xfa,0x5,0xee,0xa6,0xa3,0xf7,0x75,0x40,0x2f,0xfd,0xd5,0xa4,0xf4,0xb5,0x3,0x67,0xb3,0x47,0x22
	,0xf4,0x6e,0x7,0x24,0x4f,0xef,0x70,0x40,0xe,0xf4,0xb5,0xc3,0xc2,0xb9,0xe5,0x4c,0xaf,0xda,0xaf,0x54
	,0x37,0xfd,0x19,0x1c,0x90,0xf,0xbd,0xd9,0x1,0x59,0xd1,0x1b,0x1c,0x90,0x1b,0x7d,0xdb,0x1,0x84,0x5b
	,0xc2,0x92,0xf0,0x9d,0xdb,0xe6,0xef,0x93,0x83,0x22,0x2c,0x81,0xa,0xb8,0x46,0x7e,0x69,0xf,0xdc,0xfc
	,0xf,0x0,0x2e,0x94,0xdb,0xed,0xa8,0xa3,0x5b,0xa7,0x0,0x0,0x0,0x0,0x49,0x45,0x4e,0x44,0xae,0x42
	,0x60,0x82};

DeviceGL2::DeviceGL2(Pxf::Kernel* _Kernel)
	: GraphicsDevice(_Kernel, "OpenGL2 Graphics Device")
{
	m_Logger = _Kernel->GetLogger("gfx");
	m_Res = _Kernel->GetResourceManager();

	// Clear BindTexture history
	for(int i = 0; i < 16; ++i)
		m_BindHistory[i] = NULL;
}

DeviceGL2::~DeviceGL2()
{
	m_DebugTexture.reset(); // will call Device::_RemoveTexture
	glfwTerminate();
}

static uint64 get_nvidia_memory_info(const int param)
{
	GLint tmp;
	uint64 mem_kb = 0;
	if (glewIsSupported("GL_NVX_gpu_memory_info"))
		glGetIntegerv(param, &tmp);
	mem_kb = (uint64)tmp * 1024;
	return mem_kb;
}

uint64 DeviceGL2::GetMemorySize()
{
	static const int GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX  = 0x9048;
	return get_nvidia_memory_info(GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX);
}

uint64 DeviceGL2::GetMemoryUsage()
{

	static const int GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX = 0x9049;
	uint64 cur_avail_mem_kb = get_nvidia_memory_info(GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX);
	return GetMemorySize() - cur_avail_mem_kb;
}

uint64 DeviceGL2::GetDedicatedMemory()
{
	static const int GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX = 0x9047;
	return get_nvidia_memory_info(GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX);
}

uint64 DeviceGL2::GetEvictedMemory()
{
	static const int GPU_MEMORY_INFO_EVICTED_MEMORY_NVX = 0x904B;
	return get_nvidia_memory_info(GPU_MEMORY_INFO_EVICTED_MEMORY_NVX);
}

unsigned DeviceGL2::GetEvictionCount()
{
	static const int GPU_MEMORY_INFO_EVICTION_COUNT_NVX  = 0x904A;
	return get_nvidia_memory_info(GPU_MEMORY_INFO_EVICTION_COUNT_NVX);
}

const char* DeviceGL2::GetVendor()
{
	return (const char*)glGetString(GL_VENDOR);
}

const char* DeviceGL2::GetRenderer()
{
	return (const char*)glGetString(GL_RENDERER);
}

const char* DeviceGL2::GetVersion()
{
	return (const char*)glGetString(GL_VERSION);
}

const char* DeviceGL2::GetShaderLangVersion()
{
	return (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
}

std::shared_ptr<Window> DeviceGL2::OpenWindow(WindowDetails& _pWindowSpecs)
{
	if (glfwInit() != GL_TRUE)
	{
		m_Logger.Error("Failed to initialize glfw.");
		return nullptr;
	}

	auto window = std::make_shared<WindowGL2>(this, &_pWindowSpecs);

	window->SetID(m_Windows.size());
	m_Windows.push_back(window);

	if (window->Open())
	{
		
#ifdef CONF_DEBUG
		m_Logger.Debug("* Enabling OpenGL debug message callback.");
		bool has_GLDBG = glewGetExtension("GL_EXT_direct_state_access");
		if (has_GLDBG)
		{
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
			if (glDebugMessageCallbackARB != 0)
				glDebugMessageCallbackARB(DebugCallback, NULL);
			else if (glDebugMessageCallbackAMD != 0)
				glDebugMessageCallbackAMD(DebugCallbackAMD, NULL);	
		}
#endif
		// TODO: use this extension through out the implementation.
		bool has_DSA = glewGetExtension("GL_EXT_direct_state_access");
		if (!has_DSA)
		{
			m_Logger.Warning("* OpenGL driver does not support EXT_direct_state_access.");
		}

		glEnable(GL_POINT_SMOOTH);
		glEnable(GL_LINE_SMOOTH);

		// Upload debug texture

		m_Window = window;
		auto img = m_Res->CreateImageFromData(debug_broken_texture_png, debug_broken_texture_png_size, debug_broken_texture_png_path);
		m_DebugTexture = CreateTexture(img);
		return m_Window;
	}
	else
		return NULL;
}

void DeviceGL2::SetActiveWindow(const std::shared_ptr<Window>& _Window)
{
	m_Window = _Window;
	_Window->Activate();
}

const std::shared_ptr<Window>& DeviceGL2::GetActiveWindow()
{
	return m_Window;
}

const std::shared_ptr<Graphics::Window> DeviceGL2::GetWindow(int id)
{
	if (id >= 0 && id < m_Windows.size())
		return m_Windows[id];
	return nullptr;
}

void DeviceGL2::SetProjectionMatrix(glm::mat4& _matrix)
{
	matrixmode (GL_PROJECTION);
	glLoadMatrixf(&_matrix[0][0]);
	matrixmode(GL_MODELVIEW);
	m_ProjectionMatrix = _matrix;
}

glm::mat4 DeviceGL2::GetProjectionMatrix()
{
	return m_ProjectionMatrix;
}

void DeviceGL2::PushProjectionMatrix()
{
	matrixmode(GL_PROJECTION);
	glPushMatrix();
}

void DeviceGL2::PopProjectionMatrix()
{
	matrixmode(GL_PROJECTION);
	glPopMatrix();
}

void DeviceGL2::SetModelViewMatrix(glm::mat4& _matrix)
{
	matrixmode(GL_MODELVIEW);
	glLoadMatrixf(&_matrix[0][0]);
	m_ModelViewMatrix = _matrix;
}


glm::mat4 DeviceGL2::GetModelViewMatrix()
{
	return m_ModelViewMatrix;
}


void DeviceGL2::PushModelMatrix()
{
	matrixmode(GL_MODELVIEW);
	glPushMatrix();
}

void DeviceGL2::PopModelMatrix()
{
	matrixmode(GL_MODELVIEW);
	glPopMatrix();
}


void DeviceGL2::SetViewport(int _x, int _y, int _w, int _h)
{
	Rect viewport(_x, _y, _w, _h);
	SetViewport(viewport);
}

void DeviceGL2::SetViewport(Rect viewport)
{
	if (viewport_ != viewport)
	{
		glViewport(viewport.x, viewport.y, viewport.w, viewport.h);
		viewport_ = viewport;
	}
}

Rect DeviceGL2::GetViewport()
{
	return viewport_;
}

void DeviceGL2::PushViewportAttribute()
{
	glPushAttrib(GL_VIEWPORT_BIT);
}

void DeviceGL2::PopAttribute()
{
	glPopAttrib();
}

void DeviceGL2::SwapBuffers()
{
	if (m_Window)
	{
		m_Window->Swap();
	}
}

void DeviceGL2::Translate(glm::vec3 _translate)
{
	glTranslatef(_translate.x, _translate.y, _translate.z);
}

void DeviceGL2::Scale(glm::vec3 _scale)
{
	glScalef(_scale.x, _scale.y, _scale.z);
}

void DeviceGL2::SetDepthFunction(DepthFuncType _DepthType)
{
	int _DepthFunc;

	switch(_DepthType)
	{
		case DF_NEVER:		_DepthFunc = GL_NEVER; break;
		case DF_ALWAYS:		_DepthFunc = GL_ALWAYS; break;
		case DF_LESS:		_DepthFunc = GL_LESS; break;
		case DF_EQUAL:		_DepthFunc = GL_EQUAL; break;
		case DF_LEQUAL:		_DepthFunc = GL_LEQUAL; break;
		case DF_GREATER:	_DepthFunc = GL_GREATER; break;
		case DF_NOTEQUAL:	_DepthFunc = GL_NOTEQUAL; break;
		case DF_GEQUAL:		_DepthFunc = GL_GEQUAL; break;
		default:			_DepthFunc = GL_LESS; break;
	}

	glDepthFunc(_DepthFunc);
}

bool DeviceGL2::SetDepthTest(bool _On)
{
	GLboolean ret;
	glGetBooleanv(GL_DEPTH_TEST, &ret);

	if(_On)
		glEnable(GL_DEPTH_TEST);
	else
		glDisable(GL_DEPTH_TEST);
	return ret;
}

void DeviceGL2::SetCullFace(bool _On)
{
	if (_On)
		glEnable(GL_CULL_FACE);
	else
		glDisable(GL_CULL_FACE);
}

void DeviceGL2::Clear(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void DeviceGL2::SetBlendNone()
{
	if (blend_mode_ != BLEND_NONE)
	{
		glDisable(GL_BLEND);
		blend_mode_ = BLEND_NONE;
	}
}

void DeviceGL2::SetBlendAdditive()
{
	if (blend_mode_ != BLEND_ADDITIVE)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		blend_mode_ = BLEND_ADDITIVE;
	}
}

void DeviceGL2::SetBlendNormal()
{
	if (blend_mode_ != BLEND_NORMAL)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		blend_mode_ = BLEND_NORMAL;
	}
}

void DeviceGL2::SetClip(int x, int y, int w, int h)
{
	if (x < 0) w += x;
	if (y < 0) h += y;
	x = Clamp(x, 0, m_Window->GetWidth());
	y = Clamp(y, 0, m_Window->GetHeight());
	w = Clamp(w, 0, m_Window->GetWidth() - x);
	h = Clamp(h, 0, m_Window->GetHeight() - y);
	glScissor(x, y, w, h);
	glEnable(GL_SCISSOR_TEST);
}

void DeviceGL2::SetNoClip()
{
	glDisable(GL_SCISSOR_TEST);
}

std::shared_ptr<Graphics::Texture> DeviceGL2::CreateEmptyTexture(int _Width,int _Height, TextureFormat _Format)
{
	TextureGL2* _Tex = new TextureGL2(this);
	_Tex->CreateEmpty(_Width, _Height, _Format);
	return std::shared_ptr<Texture>(_Tex);
}

std::shared_ptr<Graphics::Texture> DeviceGL2::CreateTexture(const char* _filepath, bool force_pot)
{
	auto _tex = std::make_shared<TextureGL2>(this);
	auto img = m_Res->Acquire<Resource::Image>(_filepath);
	_tex->SetPOTHint(force_pot);
	_tex->LoadData(img);
	img->AddDependency(_tex);
	return _tex;
}

std::shared_ptr<Graphics::Texture> DeviceGL2::CreateTexture(const std::shared_ptr<Resource::Image>& _Image, bool force_pot)
{
	_Image->_AddRef();
	auto tex = std::make_shared<TextureGL2>(this);
	tex->SetPOTHint(force_pot);
	tex->LoadData(_Image);
	_Image->AddDependency(tex);
	return tex;
}

std::shared_ptr<Graphics::Texture> DeviceGL2::CreateTextureFromData(unsigned char* _datachunk, int _width, int _height, int _channels, bool force_pot)
{
	TextureGL2* _tex = new TextureGL2(this);
	auto img = m_Res->CreateImageFromRaw(_width, _height, _channels, _datachunk);
	_tex->SetPOTHint(force_pot);
	_tex->LoadData(img);
	return std::shared_ptr<Texture>(_tex);
}

std::shared_ptr<Graphics::Texture> DeviceGL2::BindTexture(const std::shared_ptr<Graphics::Texture>& _texture, unsigned int _texture_unit)
{
	auto ret = m_BindHistory[_texture_unit]; // this can't be const-ref
	m_BindHistory[_texture_unit] = _texture; // because it would refer to the bound texture because of this
	if (_texture == NULL)
	{
		TextureGL2::Unbind(_texture_unit);
	} else {
		if (_texture->IsValid())
			_texture->Bind(_texture_unit);
		else
			m_DebugTexture->Bind(_texture_unit);
	}
	return ret;
}

static void flip_image(int x, int y, int w, int h, int c, unsigned char* pixeldata)
{
	/* invert image */
	for(int j = 0; j*2 < h; ++j)
	{
		int index1 = j * w * c;
		int index2 = (h - 1 - j) * w * c;
		for(int i = w * c; i > 0; --i)
		{
			unsigned char temp = pixeldata[index1];
			pixeldata[index1] = pixeldata[index2];
			pixeldata[index2] = temp;
			++index1;
			++index2;
		}
	}
}

std::shared_ptr<Graphics::Texture> DeviceGL2::CreateTextureFromFramebuffer()
{
	int x = 0;
	int y = 0;
	int w = m_Window->GetWidth();
	int h = m_Window->GetHeight();
	int c = 3;

	glBindBuffer(GL_PIXEL_PACK_BUFFER, 0); 
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	auto pixeldata = new unsigned char[w * h * 3];
	glReadPixels (x, y, w, h, GL_RGB, GL_UNSIGNED_BYTE, pixeldata);
	flip_image(x, y, w, h, c, pixeldata);
	auto tex = CreateTextureFromData(pixeldata, w, h, c);
	tex->SetClampMethod(TEX_CLAMP_TO_EDGE);
	tex->SetMinFilter(TEX_FILTER_LINEAR);
	tex->SetMagFilter(TEX_FILTER_LINEAR);
	delete [] pixeldata;
	return tex;
}


std::shared_ptr<Resource::Image> DeviceGL2::CreateImageFromTexture(const std::shared_ptr<Graphics::Texture>& _texture)
{
	if (_texture)
		return _texture->ToImage();
	return nullptr;
}

std::shared_ptr<VertexBuffer> DeviceGL2::CreateVertexBuffer(VertexBufferLocation _VertexBufferLocation, VertexBufferUsageFlag _VertexBufferUsageFlag)
{
	return std::make_shared<VertexBufferGL2>(this, _VertexBufferLocation, _VertexBufferUsageFlag);
}

static unsigned LookupPrimitiveType(VertexBufferPrimitiveType _PrimitiveType)
{
	switch(_PrimitiveType)
	{
	case VB_PRIMITIVE_POINTS:		return GL_POINTS;
	case VB_PRIMITIVE_LINES:		return GL_LINES;
	case VB_PRIMITIVE_LINE_LOOP:	return GL_LINE_LOOP;
	case VB_PRIMITIVE_LINE_STRIP:	return GL_LINE_STRIP;
	case VB_PRIMITIVE_TRIANGLES:	return GL_TRIANGLES;
	case VB_PRIMITIVE_TRIANGLE_STRIP:	return GL_TRIANGLE_STRIP;
	case VB_PRIMITIVE_TRIANGLE_FAN:	return GL_TRIANGLE_FAN;
	case VB_PRIMITIVE_QUADS:		return GL_QUADS;
	case VB_PRIMITIVE_QUAD_STRIP:	return GL_QUAD_STRIP;
	}
	PXF_ASSERT(0, "Unknown primitive type.");
	return 0;
}

void DeviceGL2::DrawBuffer(const std::shared_ptr<VertexBuffer>& _pVertexBuffer, unsigned _VertexOffset, unsigned _VertexCount)
{
	PXF_PROFILE("device.drawbuffer");
	PXF_ASSERT(_VertexCount <= _pVertexBuffer->GetVertexCount(), "Attempting to draw too many vertices");
	_pVertexBuffer->_PreDraw();
	GLuint primitive = LookupPrimitiveType(_pVertexBuffer->GetPrimitive());
	unsigned vertex_count = _pVertexBuffer->GetVertexCount();
	if (_VertexCount > 0)
		vertex_count = _VertexCount;
	glDrawArrays(primitive, _VertexOffset, vertex_count);
	_pVertexBuffer->_PostDraw();
}

std::shared_ptr<RenderBuffer> DeviceGL2::CreateRenderBuffer(unsigned _Format, unsigned _Width, unsigned _Height)
{
	return std::make_shared<RenderBufferGL2>(this,_Format,_Width,_Height);
}

void DeviceGL2::BindRenderBuffer(const std::shared_ptr<RenderBuffer>& _pRenderBuffer)
{
	if (_pRenderBuffer)
		_pRenderBuffer->Bind();
	else
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
}

std::shared_ptr<FrameBuffer> DeviceGL2::CreateFrameBuffer()
{
	return std::make_shared<FrameBufferGL2>(this);
}

std::shared_ptr<FrameBuffer> DeviceGL2::BindFrameBuffer(const std::shared_ptr<FrameBuffer>& _pFrameBuffer)
{
	if(_pFrameBuffer)
	{
		auto oldfbo = m_CurrentFrameBuffer;
		m_CurrentFrameBuffer = _pFrameBuffer;
		_pFrameBuffer->Bind();
		return oldfbo;
	}
	else
	{
		const auto& oldfbo = m_CurrentFrameBuffer;
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		m_CurrentFrameBuffer = nullptr;
		glDrawBuffer(GL_BACK);
		return oldfbo;
	}
}

float DeviceGL2::SetLineWidth(float v)
{
	float p;
	glGetFloatv(GL_LINE_WIDTH, &p);
	glLineWidth(Pxf::Max(0.1f, v));
	return p;
}

float DeviceGL2::SetPointSize(float v)
{
	float p;
	glGetFloatv(GL_POINT_SIZE, &p);
	glPointSize(Pxf::Max(0.1f, v));
	return p;
}

std::shared_ptr<Shader> DeviceGL2::CreateShader(const char* _Ident, const char* _Shader)
{
	if (_Ident && _Shader)
	{
		auto shdr = m_Res->CreateShaderFromData(_Shader, strlen(_Shader));
		return std::make_shared<ShaderGLSL>(this, _Ident, shdr);
	}
	return nullptr;
}

std::shared_ptr<Shader> DeviceGL2::CreateShader(const char* _Ident, const std::shared_ptr<Resource::GLSL>& _Shader)
{
	if (_Ident && _Shader)
	{
		auto shader = std::make_shared<ShaderGLSL>(this, _Ident, _Shader);
		_Shader->AddDependency(shader);
		return shader;
	}
	return nullptr;
}

std::shared_ptr<Shader> DeviceGL2::CreateShaderFromPath(const char* _Ident, const char* _ShaderPath)
{
	if(!_Ident || !_ShaderPath)
		return nullptr;

	Resource::ResourceManager* res = GetKernel()->GetResourceManager();
	auto shdr = res->Acquire<Resource::GLSL>(_ShaderPath);

	if(!shdr)
		return nullptr;

	auto _Shader = std::make_shared<ShaderGLSL>(this, _Ident, shdr);
	shdr->AddDependency(_Shader); // TODO no raw pointer
	return _Shader;
}

std::shared_ptr<Shader> DeviceGL2::BindShader(const std::shared_ptr<Shader>& _Shader)
{
	auto previous = m_CurrentShader;
	if (_Shader)
	{
		m_CurrentShader = _Shader;
		_Shader->Bind();
	}
	else
	{
		m_CurrentShader = NULL;
		ShaderGLSL::Unbind();
	}

	return previous;
}

