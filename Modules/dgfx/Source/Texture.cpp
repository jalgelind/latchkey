#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Graphics/GraphicsStateSaver.h>

#include <Pxf/Modules/dgfx/Texture.h>
#include <Pxf/Modules/dgfx/OpenGL.h>

#include <Pxf/Base/Utils.h>

#include <string>
#include <SOIL.h>

// TODO: Support glTexSubImage2D in some way?
// TODO: look over mipmapping

using namespace Pxf;
using namespace Pxf::Graphics;
using std::string;

TextureGL2::TextureGL2(GraphicsDevice* _pDevice)
	: Texture(_pDevice)
	, m_TextureID(SOIL_CREATE_NEW_ID)
	, m_Image(0)
	, m_MinFilter(TEX_FILTER_LINEAR_MIPMAP_LINEAR)
	, m_MagFilter(TEX_FILTER_LINEAR)
	, m_ClampMethod(TEX_REPEAT)
	, m_HasMipmaps(true)
	, m_MipmapHint(true)
	, m_ForcePOT(false)
	, m_Format(TEX_FORMAT_RGBA)
{
	m_Logger = Logger(m_pDevice->GetKernel(), "gfx");
}

TextureGL2::~TextureGL2()
{
	Unload();
}

void TextureGL2::SetMipmapHint(bool on)
{
	m_MipmapHint = on;
}

void TextureGL2::SetPOTHint(bool on)
{
	m_ForcePOT = on;
}

glm::vec4 TextureGL2::CreateTextureSubset(float _x1, float _y1, float _x2, float _y2)
{
	float xdelta, ydelta;
	xdelta = 1.0f / (float)m_Width;
	ydelta = 1.0f / (float)m_Height;

	glm::vec4 coords;
	coords.x = _x1 * xdelta;
	coords.y = _y1 * ydelta;
	coords.z = coords.x + _x2 * xdelta;
	coords.w = coords.y + _y2 * ydelta;
	return coords;
}

void TextureGL2::CreateEmpty(int _width, int _height, TextureFormat _format)
{
	GLenum type = GL_UNSIGNED_BYTE;
	GLenum internal_fmt = GL_RGBA;
	GLenum fmt = GL_RGBA;
	if (_format == TEX_FORMAT_RGB)
	{
		internal_fmt = GL_RGBA;
		fmt = GL_RGB;
	}
	else if (_format == TEX_FORMAT_R)
	{
		internal_fmt = GL_RED;
		fmt = GL_RED;
	}
	else if (_format == TEX_FORMAT_DEPTH)
	{
		internal_fmt = GL_DEPTH_COMPONENT24;
		fmt = GL_DEPTH_COMPONENT;
	}
	else if (_format == TEX_FORMAT_DEPTH_STENCIL)
	{
		internal_fmt =  GL_DEPTH_STENCIL_EXT; // GL_DEPTH24_STENCIL8_EXT
		fmt = GL_DEPTH_STENCIL_EXT;
		type = GL_UNSIGNED_INT_24_8_EXT;
	}

	if (m_TextureID)
		Unload();

	BoundTextureStateSaver statesaver(*m_pDevice);

	glGenTextures(1, &m_TextureID);
	Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, internal_fmt, _width, _height, 0, fmt, type, NULL);
	glGenerateMipmapEXT(GL_TEXTURE_2D);
	int c;
	switch(_format)
	{
	case TEX_FORMAT_RGBA:
		c = 4; break;
	case TEX_FORMAT_RGB:
		c = 3; break;
	default:
		c = 1;
	}
	m_pDevice->_UpdateTextureMemoryUsage(_width*_height*c);
}

void TextureGL2::LoadData(const std::shared_ptr<Resource::Image>& _img)
{
	if(!_img || !_img->IsReady())
	{
		m_Logger.Error("Invalid image object supplied.");
		return;
	}
	
	m_Image = _img;

	if (m_TextureID)
		Unload();

	Reload();
}

void TextureGL2::Unload()
{
	glDeleteTextures( 1, &m_TextureID );
	m_pDevice->_UpdateTextureMemoryUsage(-m_Width*m_Height*m_Channels);
	m_TextureID = SOIL_CREATE_NEW_ID;
}

void TextureGL2::Reload()
{
	if (!m_Image)
	{
		m_Logger.Error("I don't have a valid image to work with!");
		return;
	}

	unsigned flags = SOIL_FLAG_MULTIPLY_ALPHA;

	if (m_ForcePOT)
		flags |= SOIL_FLAG_POWER_OF_TWO;

	// TODO: is non-pot mipmapping supported these days...?
	if (m_ForcePOT || (IsPow2(m_Image->Width()) &&  IsPow2(m_Image->Height())))
	{
		if (m_MipmapHint)
		{
			flags = flags | SOIL_FLAG_MIPMAPS;
			m_HasMipmaps = true;
		}
	}
	else
	{
		// FIXME/disabled: I don't think this will work with gl*(GL_TEXTURE_2D...
		//if (glewIsSupported("ARB_texture_rectangle"))
		//	flags = flags | SOIL_FLAG_TEXTURE_RECTANGLE;
	}

	m_TextureID = SOIL_create_OGL_texture(
		m_Image->Ptr(),
		m_Image->Width(), m_Image->Height(), m_Image->Channels(),
		m_TextureID,
		flags);

	SetMagFilter(m_MagFilter);
	SetMinFilter(m_MinFilter);
	SetClampMethod(m_ClampMethod);

	m_Width = m_Image->Width();
	m_Height = m_Image->Height();
	m_Channels = m_Image->Channels();
	m_Format = (m_Channels == 4) ? TEX_FORMAT_RGBA : ((m_Channels == 1) ? TEX_FORMAT_R : TEX_FORMAT_RGB);

	if (m_TextureID == 0)
		m_Logger.Error("Failed to create texture for '%s'", m_Image->GetSource());
	else
		m_pDevice->_UpdateTextureMemoryUsage(m_Width*m_Height*m_Channels*1.33); // 1.33 is for the extra mipmapdata
}

static GLuint _texture_units_array[16] = {GL_TEXTURE0_ARB, GL_TEXTURE1_ARB, GL_TEXTURE2_ARB, GL_TEXTURE3_ARB, GL_TEXTURE4_ARB,
	GL_TEXTURE5_ARB, GL_TEXTURE6_ARB, GL_TEXTURE7_ARB, GL_TEXTURE8_ARB, GL_TEXTURE9_ARB,
	GL_TEXTURE10_ARB, GL_TEXTURE11_ARB, GL_TEXTURE12_ARB, GL_TEXTURE13_ARB, GL_TEXTURE14_ARB,
	GL_TEXTURE15_ARB};

void TextureGL2::Bind(unsigned texture_unit)
{
	glActiveTextureARB(_texture_units_array[texture_unit]);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, GetTextureID());
}

void TextureGL2::Unbind(unsigned texture_unit)
{
	glActiveTextureARB(_texture_units_array[texture_unit]);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

int TextureGL2::GetWidth()
{
	return m_Width;
}

int TextureGL2::GetHeight()
{
	return m_Height;
}

int TextureGL2::GetChannels()
{
	return m_Channels;
}

unsigned int TextureGL2::GetTextureID()
{
	return (unsigned int)m_TextureID;
}


void TextureGL2::SetMagFilter(TextureFilter _Filter)
{
	GLint param = GL_NEAREST;

	// use a lut
	if (_Filter == TEX_FILTER_NEAREST)
		param = GL_NEAREST;
	else if (_Filter == TEX_FILTER_LINEAR)
		param = GL_LINEAR;
	else
		m_Logger.Warning("invalid mag filter, using 'nearest'");

	m_MagFilter = _Filter;

	BoundTextureStateSaver statesaver(*m_pDevice);
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, param);
}

void TextureGL2::SetMinFilter(TextureFilter _Filter)
{
	GLint param = GL_NEAREST;
	// use a lut
	if	  (_Filter == TEX_FILTER_NEAREST) param = GL_NEAREST;
	else if (_Filter == TEX_FILTER_LINEAR)  param = GL_LINEAR;
	else
	{
		if (HasMipmaps())
		{
			if (_Filter == TEX_FILTER_LINEAR_MIPMAP_LINEAR)  param = GL_LINEAR_MIPMAP_LINEAR;
			else if (_Filter == TEX_FILTER_LINEAR_MIPMAP_NEAREST)  param = GL_LINEAR_MIPMAP_NEAREST;
			else if (_Filter == TEX_FILTER_NEAREST_MIPMAP_LINEAR)  param = GL_NEAREST_MIPMAP_LINEAR;
			else if (_Filter == TEX_FILTER_NEAREST_MIPMAP_NEAREST)  param = GL_NEAREST_MIPMAP_NEAREST;
			else	m_Logger.Warning("invalid min filter, using GL_NEAREST");
		}
		else
			m_Logger.Warning("invalid min filter, texture has no mipmap data");
	}

	m_MinFilter = _Filter;

	BoundTextureStateSaver statesaver(*m_pDevice);
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, param);
}

void TextureGL2::SetClampMethod(TextureClampMethod _Method)
{
	GLint m = 0;
	switch(_Method)
	{
	case TEX_CLAMP: m = GL_CLAMP; break;
	case TEX_CLAMP_TO_EDGE: m = GL_CLAMP_TO_EDGE; break;
	case TEX_REPEAT: m = GL_REPEAT; break;
	default:
		PXF_ASSERT(false, "No such clamp method");
	}

	m_ClampMethod = _Method;

	BoundTextureStateSaver statesaver(*m_pDevice);
	Bind();
	glBindTexture(GL_TEXTURE_2D, m_TextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m);
}

std::shared_ptr<Resource::Image> TextureGL2::ToImage()
{
	int width = GetWidth();
	int height = GetHeight();
	int channels = 4;

	glBindBuffer(GL_PIXEL_PACK_BUFFER, 0); 
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	auto old_tex = m_pDevice->GetBoundTexture();
	Bind();

	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);

	m_Logger.Debug("Creating image from %dx%d texture.", width, height);

	unsigned char* pixels = new unsigned char[width*height*channels];

	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	if (old_tex) old_tex->Bind();

	auto img = m_pDevice->GetKernel()->GetResourceManager()->CreateImageFromRaw(width, height, channels, pixels);
	delete [] pixels;
	return img;
}