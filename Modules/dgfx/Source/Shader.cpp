#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Resource/GLSL.h>

#include <Pxf/Modules/dgfx/Shader.h>
#include <Pxf/Modules/dgfx/OpenGL.h>

using namespace Pxf;
using namespace Pxf::Graphics;

ShaderGLSL::ShaderGLSL(GraphicsDevice* _pDevice, const char* _Identifier, const std::shared_ptr<Resource::GLSL>& _Shader)
	: Shader(_pDevice)
	, m_Shader(0)
{
	m_Logger = Logger(m_pDevice->GetKernel(), "gfx");
	m_Shader = _Shader;

	Load();

	if (m_Valid)
		m_Logger.Information("Shader '%s' was compiled and linked successfully", _Identifier);
}

ShaderGLSL::~ShaderGLSL()
{
	Unload();
}

bool ShaderGLSL::Load()
{
	Unload();

	if (m_Shader)
	{
		m_Valid = true;
		m_ProgramHandle = GL::CreateProgram();

		const char* vshdr =  m_Shader->GetVertexShader();
		const char* fshdr =  m_Shader->GetFragmentShader();

		// Create vertex shader
		m_VertexShaderHandle = GL::CreateShader(GL_VERTEX_SHADER);
		GL::ShaderSource(m_VertexShaderHandle, 1, &vshdr, NULL);
		GL::CompileShader(m_VertexShaderHandle);
		CheckForCompilationErrors(m_VertexShaderHandle);

		// Create fragment shader
		m_FragmentShaderHandle = GL::CreateShader(GL_FRAGMENT_SHADER);
		GL::ShaderSource(m_FragmentShaderHandle, 1, &fshdr, NULL);
		GL::CompileShader(m_FragmentShaderHandle);
		if (!CheckForCompilationErrors(m_FragmentShaderHandle))
			m_Valid = false;

		// Attach vertex and fragment shader
		GL::AttachShader(m_ProgramHandle, m_VertexShaderHandle);
		GL::AttachShader(m_ProgramHandle, m_FragmentShaderHandle);

		GL::LinkProgram(m_ProgramHandle);
		if (!CheckForLinkerErrors(m_ProgramHandle))
			m_Valid = false;
	}
	return true;
}

bool ShaderGLSL::Unload()
{
	if(!m_Valid)
		return true;

	GL::UseProgram(0);

	// Detach shaders from program
	GL::DetachShader(m_ProgramHandle, m_VertexShaderHandle);
	GL::DetachShader(m_ProgramHandle, m_FragmentShaderHandle);

	// Delete shaders
	GL::DeleteShader(m_VertexShaderHandle);
	GL::DeleteShader(m_FragmentShaderHandle);

	// Delete program
	GL::DeleteProgram(m_ProgramHandle);

	m_Valid = false;
	return true;
}

void ShaderGLSL::Reload()
{
	Unload();
	Load();
}

bool ShaderGLSL::CheckForCompilationErrors(unsigned _ShaderHandle)
{
	GLint res;
	GL::GetShaderiv(_ShaderHandle, GL::COMPILE_STATUS, &res);
	if (res == GL_FALSE)
	{
		char buff[4096];
		GLsizei len = 0;
		GL::GetShaderInfoLog(_ShaderHandle, 4095, &len, buff);
		buff[len] = 0;

		m_Logger.Error("Failed to compile shader.");
		m_Logger.Error(buff);
		m_Logger.Error("Shader compilation error", buff);

		return false;
	}
	return true;
}

bool ShaderGLSL::CheckForLinkerErrors(unsigned _ProgramHandle)
{
	GLint res;
	GL::GetProgramiv(_ProgramHandle, GL::LINK_STATUS, &res);
	if (res == GL_FALSE)
	{
		char buff[4096];
		GLsizei len = 0;
		GL::GetProgramInfoLog(_ProgramHandle, 4095, &len, buff);
		buff[len] = 0;

		m_Logger.Error("Failed to link shader.");
		m_Logger.Error(buff);
		if (*buff != 0x0)
			m_Logger.Error("Shader link error", buff);
		return false;
	}
	return true;
}

void ShaderGLSL::Bind()
{
	GL::UseProgram(m_ProgramHandle);
}

void ShaderGLSL::Unbind()
{
	GL::UseProgram(0);
}

void ShaderGLSL::BindAttributeLocation(unsigned _Index, const char* _Name)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	glBindAttribLocation(m_ProgramHandle, _Index, _Name);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformi(const char* _name, int _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform1i(loc, _value);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformf(const char* _name, float _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform1f(loc, _value);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformVec2(const char* _name, const glm::vec2& _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform2fv(loc, 1, &_value[0]);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformVec3(const char* _name, const glm::vec3& _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform3fv(loc, 1, &_value[0]);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformVec3v(const char* _name, unsigned count, const glm::vec3& _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform3fv(loc, count, &_value[0]);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformVec4(const char* _name, const glm::vec4& _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform4fv(loc, 1, &_value[0]);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformVec4v(const char* _name, unsigned count, const glm::vec4& _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::Uniform4fv(loc, 1, &_value[0]);
	if (old_shader) old_shader->Bind();
}

void ShaderGLSL::SetUniformMat4(const char* _name, const glm::mat4& _value)
{
	auto old_shader = m_pDevice->GetActiveShader();
	Bind();
	int loc = GL::GetUniformLocation(m_ProgramHandle, _name);
	GL::UniformMatrix4fv(loc, 1, 0,&_value[0][0]);
	if (old_shader) old_shader->Bind();
}
