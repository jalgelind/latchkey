#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Utils.h>

#include <Pxf/Graphics/GraphicsStateSaver.h>

#include <Pxf/Modules/dgfx/FrameBuffer.h>
#include <Pxf/Modules/dgfx/RenderBuffer.h>
#include <Pxf/Modules/dgfx/Texture.h>
#include <Pxf/Modules/dgfx/OpenGL.h>

#include <stdio.h>

using namespace Pxf;
using namespace Pxf::Graphics;
using namespace Pxf::Modules;

// XXX, FIXME: Add more diagnostics to the error messages (framebuffer id, texture ids etc.)

unsigned TranslateAttachment(unsigned _ID);
bool CheckFBO(GLenum _status);

FrameBufferGL2::FrameBufferGL2(Graphics::GraphicsDevice* _Device)
	: FrameBuffer(_Device)
{
	m_Logger = Logger(m_pDevice->GetKernel(), "gfx");
	_Configure();
}

FrameBufferGL2::~FrameBufferGL2()
{
	// FIXME/THINKABOUTIT: Detach them automatically, or just inform the user?
	if (m_NumColorAttachment != 0)
		m_Logger.Error("Deleting framebuffer with undetached color attachments!");
	if (m_UseStencilAttachment)
		m_Logger.Error("Deleting framebuffer with undetached stencil attachment!");
	if (m_UseDepthAttachment)
		m_Logger.Error("Deleting framebuffer with undetached depth attachment!");

	glDeleteFramebuffersEXT(1, &m_Handle);
}

void FrameBufferGL2::_Configure()
{
	GLint _Attachments = 0;
	glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &_Attachments);

	if(_Attachments == GL_INVALID_ENUM)
	{
		m_Logger.Warning("Framebuffer could not get the number of supported attachments.");
		_Attachments = 0;
	}

	m_MaxColorAttachments = _Attachments;
	glGenFramebuffersEXT(1, &m_Handle);

	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	m_Complete = CheckFBO(status);
}

bool FrameBufferGL2::CheckFBO(unsigned _status)
{
	switch(_status)
	{
	case GL_FRAMEBUFFER_COMPLETE_EXT:
		return true;
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT");
		break;
	case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
		m_Logger.Error("Framebuffer::Attach -> GL_FRAMEBUFFER_UNSUPPORTED_EXT");
		break;
	default:
		m_Logger.Error("Framebuffer::Attach ->  Unknown error. (0x%X)", _status);
		break;
	}
	return false;
}

unsigned int FrameBufferGL2::Translate(Graphics::FrameBufferAttachmentType _type)
{
	switch(_type)
	{
	case FB_COLOR0: return GL_COLOR_ATTACHMENT0_EXT;
	case FB_COLOR1: return GL_COLOR_ATTACHMENT1_EXT;
	case FB_COLOR2: return GL_COLOR_ATTACHMENT2_EXT;
	case FB_COLOR3: return GL_COLOR_ATTACHMENT3_EXT;
	case FB_COLOR4: return GL_COLOR_ATTACHMENT4_EXT;
	case FB_COLOR5: return GL_COLOR_ATTACHMENT5_EXT;
	case FB_COLOR6: return GL_COLOR_ATTACHMENT6_EXT;
	case FB_COLOR7: return GL_COLOR_ATTACHMENT7_EXT;
	case FB_COLOR8: return GL_COLOR_ATTACHMENT8_EXT;
	case FB_COLOR9: return GL_COLOR_ATTACHMENT9_EXT;
	case FB_COLOR10: return GL_COLOR_ATTACHMENT10_EXT;
	case FB_COLOR11: return GL_COLOR_ATTACHMENT11_EXT;
	case FB_COLOR12: return GL_COLOR_ATTACHMENT12_EXT;
	case FB_COLOR13: return GL_COLOR_ATTACHMENT13_EXT;
	case FB_COLOR14: return GL_COLOR_ATTACHMENT14_EXT;
	case FB_COLOR15: return GL_COLOR_ATTACHMENT15_EXT;
	case FB_STENCIL: return GL_STENCIL_ATTACHMENT_EXT;
	case FB_DEPTH_STENCIL: return GL_DEPTH_STENCIL_ATTACHMENT;
	case FB_DEPTH: return GL_DEPTH_ATTACHMENT_EXT;
	}
	m_Logger.Error("Invalid framebuffer attachement type.");
	return 0;
}

void FrameBufferGL2::Detach(const FrameBufferAttachmentType _Attachment)
{
	unsigned int attachment = Translate(_Attachment);
	if(attachment == GL_DEPTH_ATTACHMENT_EXT)
	{
		if(!m_UseDepthAttachment)
		{
			m_Logger.Warning("Framebuffer::Detach -> Unable to detach depth buffer, no such buffer attached");
			return;
		}

		m_UseDepthAttachment = false;
	}
	else if(attachment == GL_STENCIL_ATTACHMENT_EXT)
	{
		if(!m_UseStencilAttachment)
		{
			m_Logger.Warning("Framebuffer::Detach -> Unable to detach stencil buffer, no such buffer attached");
			return;
		}

		m_UseStencilAttachment = false;
	}
	else if(IsWithinOrEqual(attachment, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT15))
	{
		unsigned short is_attached = m_AttachmentMask & (1 << (_Attachment));
		if(!is_attached)
		{
			m_Logger.Warning("Framebuffer::Detach -> Unable to detach unattached attachment");
			return;
		}

		m_NumColorAttachment--;
		m_AttachmentMask &= ~(1 << (_Attachment));
	}
	else
	{
		m_Logger.Warning("Framebuffer::Detach -> Unable to detach an invalid attachment");
		return;
	}

	BoundFrameBufferStateSaver statesaver(*m_pDevice);
	Bind();
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, _Attachment, GL_TEXTURE_2D, 0, 0);
}

void FrameBufferGL2::Attach(const std::shared_ptr<Texture>& _Texture, const FrameBufferAttachmentType _Attachment, bool _GenMipmaps)
{
	unsigned int attachment = Translate(_Attachment);
	if(!_Texture)
	{
		m_Logger.Warning("Framebuffer::Attach -> Unable to attach with invalid texture");
		return;
	}

	if(attachment == GL_DEPTH_ATTACHMENT_EXT)
	{
		if(m_UseDepthAttachment)
			Detach(_Attachment);
		_GenMipmaps = false;
		m_UseDepthAttachment = true;
	}

	else  if(attachment == GL_DEPTH_STENCIL_ATTACHMENT)
	{
		if(m_UseDepthAttachment || m_UseStencilAttachment)
			Detach(_Attachment);
		_GenMipmaps = false;
		m_UseDepthAttachment = true;
		m_UseStencilAttachment = true;
		if (!glewIsSupported("GL_EXT_packed_depth_stencil"))
		{
			m_Logger.Error("GL_EXT_packed_depth_stencil is not supported on this machine.");
		}
	}

	else if(attachment == GL_STENCIL_ATTACHMENT_EXT)
	{
		if(m_UseStencilAttachment)
			Detach(_Attachment);

		m_UseStencilAttachment = true;
	}

	else if(IsWithinOrEqual(attachment, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT15))
	{
		short unsigned is_attached = (m_AttachmentMask & (1 << (_Attachment)));
		if(is_attached)
		{
			Detach(_Attachment);
			m_Logger.Warning("Framebuffer::Attach -> Already attached, reattaching with new ID");
		}

		m_NumColorAttachment++;
		m_AttachmentMask |= (1 << (_Attachment));
	}
	else
	{
		m_Logger.Warning("Framebuffer::Attach -> Unable to attach with invalid ID");
		return;
	}

	BoundFrameBufferStateSaver fbo_statesaver(*m_pDevice);
	BoundTextureStateSaver tex_statesaver(*m_pDevice);
	
	Bind();

	if (attachment == GL_DEPTH_STENCIL_ATTACHMENT)
	{
		GLuint texid = ((TextureGL2*) _Texture.get())->GetTextureID();
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, texid, 0);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_TEXTURE_2D, texid, 0);
	}
	else
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, attachment, GL_TEXTURE_2D, ((TextureGL2*) _Texture.get())->GetTextureID(), 0);

	if (_GenMipmaps)
		glGenerateMipmapEXT(GL_TEXTURE_2D);

	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	m_Complete = CheckFBO(status);
}

void FrameBufferGL2::Attach(const std::shared_ptr<RenderBuffer>& _Buffer, const FrameBufferAttachmentType _Attachment)
{
	unsigned int attachment = Translate(_Attachment);
	if(!_Buffer)
	{
		m_Logger.Warning("Framebuffer::Attach -> Invalid renderbuffer passed to attach function");
		return;
	}

	if(!_Buffer->Ready())
	{
		m_Logger.Warning("Framebuffer::Attach -> Unable to add attachment, attachment is not ready");
		return;
	}

	if(attachment == GL_DEPTH_ATTACHMENT_EXT)
	{
		if(m_UseDepthAttachment)
		{
			Detach(_Attachment);
			m_Logger.Warning("Framebuffer::Attach -> Depth buffer already attached, reattaching with new ID");
		}

		m_UseDepthAttachment = true;
	}

	else if(attachment == GL_STENCIL_ATTACHMENT_EXT)
	{
		if(m_UseStencilAttachment)
		{
			Detach(_Attachment);
			m_Logger.Warning("Framebuffer::Attach -> Stencil buffer already attached, reattaching with new ID");
		}

		m_UseStencilAttachment = true;
	}

	else if(IsWithinOrEqual(attachment, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT15))
	{
		short unsigned is_attached = (m_AttachmentMask & (1 << (_Attachment)));
		if(is_attached)
		{
			Detach(_Attachment);
			m_Logger.Warning("Framebuffer::Attach -> Already attached, reattaching with new ID");
		}

		m_NumColorAttachment++;
		m_AttachmentMask |= 1 << (_Attachment);
	}
	else
	{
		m_Logger.Warning("Framebuffer::Attach -> Unable to attach with invalid ID");
		return;
	}

	BoundFrameBufferStateSaver statesaver(*m_pDevice);
	Bind();
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, _Attachment,
							 GL_RENDERBUFFER_EXT, ((RenderBufferGL2*) _Buffer.get())->GetHandle());
	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	m_Complete = CheckFBO(status);
}

void FrameBufferGL2::Bind()
{
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, GetHandle());
	int _buffercount = 0;
	GLenum _attachment_lut[] = {GL_COLOR_ATTACHMENT0_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_COLOR_ATTACHMENT2_EXT,
		            GL_COLOR_ATTACHMENT3_EXT, GL_COLOR_ATTACHMENT4_EXT, GL_COLOR_ATTACHMENT5_EXT,
		            GL_COLOR_ATTACHMENT6_EXT, GL_COLOR_ATTACHMENT7_EXT, GL_COLOR_ATTACHMENT8_EXT};

	_buffercount = GetNumAttached();
	if (_buffercount > 1)
		glDrawBuffers(_buffercount, _attachment_lut);
	else
		glDrawBuffer(_attachment_lut[0]);
}

int FrameBufferGL2::GetNumAttached()
{
	return m_NumColorAttachment;
}

