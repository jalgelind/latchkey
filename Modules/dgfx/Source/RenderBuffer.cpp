#include <Pxf/Kernel.h>

#include <Pxf/Modules/dgfx/RenderBuffer.h>
#include <Pxf/Modules/dgfx/OpenGL.h>

using namespace Pxf;
using namespace Pxf::Graphics;
using namespace Pxf::Modules;

RenderBufferGL2::RenderBufferGL2(Graphics::GraphicsDevice* _Device, unsigned _Format, unsigned _Width, unsigned _Height)
	: RenderBuffer(_Device,_Width,_Height)
	, m_Format(_Format)
{
	m_Logger = Logger(m_pDevice->GetKernel(), "gfx");
	_InitBuffer();
}

void RenderBufferGL2::_InitBuffer()
{
	glGenRenderbuffersEXT(1, &m_Handle);
	Bind();
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, m_Format, m_Width, m_Height);
	m_pDevice->BindRenderBuffer(nullptr);

	if(m_Handle)
	{
		m_Logger.Information("Created render buffer (%i,%i)",m_Width,m_Height);
		m_Ready = true;
	}
}

void RenderBufferGL2::Bind()
{
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, GetHandle());
}

RenderBufferGL2::~RenderBufferGL2()
{
	if (m_Handle)
		ReleaseBuffer();
}

void RenderBufferGL2::ReleaseBuffer()
{
	if(m_Handle)
	{
		glDeleteRenderbuffersEXT(1, &m_Handle);
		m_Handle = 0;
	}
}

