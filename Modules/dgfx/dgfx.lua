Import("../../Builder.lua")


module = NewModule("dgfx")
module:RequireLibrary("glfw3")
module:RequireLibrary("glew")
module:RequireLibrary("soil")
module:AddIncludeDirectory("Include")
module:AddSourceDirectory("Source/*.cpp")
--module:AddSourceDirectory("Source/Shader/*.cpp")
