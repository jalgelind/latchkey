#ifndef _PXF_MODULE_DGFX_OPENGL_H_
#define _PXF_MODULE_DGFX_OPENGL_H_

#include <Pxf/Base/Config.h>
#include <GL/glew.h>

#if defined(CONF_FAMILY_WINDOWS)
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#undef CreateFont // ...
#endif

#if defined(CONF_PLATFORM_MACOSX)
	#include <OpenGL/OpenGL.h>
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif
#ifdef CONF_WITH_LIBRARY_GLFW3
	#include <GLFW/glfw3.h>
#else
	#include <GL/glfw.h>
#endif

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Graphics/GraphicsDevice.h>

//////////////////////////////////////////////////////////////////////////
// OpenGL Utilities and helper functions
//////////////////////////////////////////////////////////////////////////
namespace Pxf
{
	namespace Graphics
	{
		inline GLenum LookupDDT(DeviceDataType type)
		{
			switch(type)
			{
			case DDT_BYTE: return GL_BYTE;
			case DDT_UNSIGNED_BYTE: return GL_UNSIGNED_BYTE;
			case DDT_SHORT: return GL_SHORT;
			case DDT_UNSIGNED_SHORT: return GL_UNSIGNED_SHORT;
			case DDT_INT: return GL_INT;
			case DDT_UNSIGNED_INT: return GL_UNSIGNED_INT;
			case DDT_FLOAT: return GL_FLOAT;
			case DDT_DOUBLE: return GL_DOUBLE;
			default: PXF_ASSERT(0, "No such DeviceDataType");
			}
			return 0;
		}

		namespace GL
		{
			// Generic helper functions
			inline GLvoid* BufferObjectPtr(unsigned int ptr)
			{
				return (GLvoid*)( ((char*)NULL) + ptr );
			};

			inline void CheckError(const char* _Source)
			{
				int error = GL_NO_ERROR;
				while((error = glGetError()) != GL_NO_ERROR)
				{
					Pxf::Kernel::GetInstance()->GetLogger("gl").Error("GL error %d => '%s'", error, gluErrorString(error));
					#if defined(CONF_FAMILY_WINDOWS) && defined(CONF_COMPILER_MSVC)
						__asm int 3;
					#endif
				}
			}

			//////////////////////////////////////////////////////////////////////////
			// Wrapped OpenGL functions
			//////////////////////////////////////////////////////////////////////////

			void SetupExtensions();

			// Buffer
			extern unsigned int ARRAY_BUFFER;
			extern PFNGLDELETEBUFFERSPROC DeleteBuffers;
			extern PFNGLBINDBUFFERPROC BindBuffer;
			extern PFNGLGENBUFFERSPROC GenBuffers;
			extern PFNGLBUFFERDATAPROC BufferData;
			extern PFNGLBUFFERSUBDATAPROC BufferSubData;
			extern PFNGLMAPBUFFERPROC MapBuffer;
			extern PFNGLUNMAPBUFFERPROC UnmapBuffer;

			extern PFNGLDRAWBUFFERSPROC DrawBuffers;

			// Shader
			extern unsigned int COMPILE_STATUS;
			extern unsigned int LINK_STATUS;
			extern unsigned int VERTEX_SHADER;
			extern unsigned int FRAGMENT_SHADER;

			extern PFNGLCREATEPROGRAMPROC CreateProgram;
			extern PFNGLCREATESHADERPROC CreateShader;
			extern PFNGLSHADERSOURCEPROC ShaderSource;
			extern PFNGLCOMPILESHADERPROC CompileShader;
			extern PFNGLATTACHSHADERPROC AttachShader;
			extern PFNGLLINKPROGRAMPROC LinkProgram;
			extern PFNGLUSEPROGRAMPROC UseProgram;
			extern PFNGLDETACHSHADERPROC DetachShader;
			extern PFNGLDELETESHADERPROC DeleteShader;
			extern PFNGLDELETEPROGRAMPROC DeleteProgram;
			extern PFNGLGETSHADERIVPROC GetShaderiv;
			extern PFNGLGETPROGRAMIVPROC GetProgramiv;
			extern PFNGLGETSHADERINFOLOGPROC GetShaderInfoLog;
			extern PFNGLGETPROGRAMINFOLOGPROC GetProgramInfoLog;
			extern PFNGLGETUNIFORMLOCATIONPROC GetUniformLocation;


			// Uniform (float, float vectors)
			extern PFNGLUNIFORM1FPROC Uniform1f;
			extern PFNGLUNIFORM2FPROC Uniform2f;
			extern PFNGLUNIFORM3FPROC Uniform3f;
			extern PFNGLUNIFORM4FPROC Uniform4f;
			extern PFNGLUNIFORM1FVPROC Uniform1fv;
			extern PFNGLUNIFORM2FVPROC Uniform2fv;
			extern PFNGLUNIFORM3FVPROC Uniform3fv;
			extern PFNGLUNIFORM4FVPROC Uniform4fv;

			// Uniform (integer, integer vectors)
			extern PFNGLUNIFORM1IPROC Uniform1i;
			extern PFNGLUNIFORM2IPROC Uniform2i;
			extern PFNGLUNIFORM3IPROC Uniform3i;
			extern PFNGLUNIFORM4IPROC Uniform4i;
			extern PFNGLUNIFORM1IVPROC Uniform1iv;
			extern PFNGLUNIFORM2IVPROC Uniform2iv;
			extern PFNGLUNIFORM3IVPROC Uniform3iv;
			extern PFNGLUNIFORM4IVPROC Uniform4iv;

			// Uniform (matrices)
			extern PFNGLUNIFORMMATRIX2FVPROC UniformMatrix2fv;
			extern PFNGLUNIFORMMATRIX3FVPROC UniformMatrix3fv;
			extern PFNGLUNIFORMMATRIX4FVPROC UniformMatrix4fv;

			// Other
			extern PFNGLCLIENTACTIVETEXTUREPROC ClientActiveTexture;


			/* Todo: OpenGL 3.0 */
			//void BindFragDataLocation(int _Index, const char* _Name);
			//void BindAttribLocation(int _Index, const char* _Name);
		} // GL
	}
}

#undef OPENGL_TRAIT
#undef OPENGL_TRAIT_T

#endif // _PXF_MODULE_DGFX_OPENGL_H_

