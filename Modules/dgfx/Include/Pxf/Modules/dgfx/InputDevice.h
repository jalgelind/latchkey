#ifndef _PXF_MODULE_DGFX_GLFWINPUTDEVICE_H_
#define _PXF_MODULE_DGFX_GLFWINPUTDEVICE_H_

#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Input/InputDefs.h>

#include <vector>
struct GLFWwindow;

namespace Pxf
{
	namespace Modules
	{
		class InputControllerGLFW;
		static InputControllerGLFW* g_GLFWInputController;

		class GLFWKeyboard : public Pxf::Input::Keyboard
		{
		public:
			bool m_Keyrepeat;

			GLFWKeyboard(Kernel* krn, Pxf::Input::InputController* inp)
				: Pxf::Input::Keyboard(krn, inp)
				, m_Keyrepeat(false)
			{}

			virtual bool IsKeyDown(int _key);
			virtual void SetKeyRepeat(bool _On);
			virtual void SetSystemKeys(bool _On);
		};

		//
		// GLFW Mouse
		//
		class GLFWMouse : public Pxf::Input::Mouse
		{
		public:

			int last_button; /* stores last mouse button pressed */
			int last_button_released; /* stores last mouse button released */

			Input::MouseMode mouse_mode;
			int mouse_x, mouse_y;
			int mouse_wheel_delta;
			unsigned int mouse_buttons; /* store status for mouse button, bit #0 = button 1. 1 = pressed */

			GLFWMouse(Kernel* krn, Pxf::Input::InputController* inp)
				: Pxf::Input::Mouse(krn, inp)
				, mouse_wheel_delta(0)
			{}

			virtual bool IsButtonDown(int _button);
			virtual unsigned GetButtonStates();
			virtual int GetLastButton();
			virtual int GetLastReleasedButton();
			virtual void ClearLastButton();
			virtual int GetMouseWheelDelta();
			virtual void GetMousePos(int *x, int *y);
			virtual void SetMousePos(int x, int y);
			virtual Pxf::Input::MouseMode GetMouseMode();
			virtual void SetMouseMode(Pxf::Input::MouseMode _Mode);
			virtual void ShowCursor(bool _show);
		};

		class InputControllerGLFW : public Pxf::Input::InputController
		{
		private:
			virtual bool Init()
			{   return true;	}
		public:

		GLFWKeyboard* m_GLFWKeyboard;
		GLFWMouse* m_GLFWMouse;

		static void char_callback(GLFWwindow* window, unsigned int _Char);
		static void key_callback(GLFWwindow* window, int _Key, int _Scancode, int _Action, int _Modifiers);
		static void mouse_callback(GLFWwindow* window, int _Button, int _Action, int _Modifiers);
		static void mousewheel_callback(GLFWwindow* window, double xoffset, double yoffset);
		static void mousepos_callback(GLFWwindow* window, double x, double y);

		InputControllerGLFW(Pxf::Kernel* _Kernel);
		virtual ~InputControllerGLFW();

		static InputControllerGLFW* GetInstance();
		std::vector<GLFWwindow*> m_WindowHandles;
		void RegisterWindowHandle(GLFWwindow* handle);
		void UnRegisterWindowHandle(GLFWwindow* handle);


		virtual void Update();
		virtual void Discover();

		virtual int GetIntOption(const std::string& option) { return -1; }
		virtual float GetFloatOption(const std::string& option) { return -1; }

		public:
		Logger m_Logger;
		};
	}
}

#endif // _PXF_MODULE_DGFX_GLFWINPUTDEVICE_H_

