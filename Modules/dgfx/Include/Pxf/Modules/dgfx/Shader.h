#ifndef _PXF_MODULE_DGFX_SHADER_H_
#define _PXF_MODULE_DGFX_SHADER_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/Shader.h>

#include <time.h>


namespace Pxf
{
	namespace Resource
	{
		class GLSL;
	}

	namespace Graphics
	{
		class ShaderGLSL : public Graphics::Shader
		{
		protected:
			Logger m_Logger;
			unsigned m_ProgramHandle;
			unsigned m_FragmentShaderHandle;
			unsigned m_VertexShaderHandle;

			bool CheckForCompilationErrors(unsigned _ShaderHandle);
			bool CheckForLinkerErrors(unsigned _ProgramHandle);

		public:
			ShaderGLSL(GraphicsDevice* _pDevice, const char* _Identifier, const std::shared_ptr<Resource::GLSL>& _Shader);
			virtual ~ShaderGLSL();

			void Bind();
			static void Unbind();

			void BindAttributeLocation(unsigned _Index, const char* _Name);
			void SetUniformi(const char* _name, int _value);
			void SetUniformf(const char* _name, float _value);
			void SetUniformVec2(const char* _name, const glm::vec2& _value);
			void SetUniformVec3(const char* _name, const glm::vec3& _value);
			void SetUniformVec3v(const char* _name, unsigned count, const glm::vec3& _value);
			void SetUniformVec4(const char* _name, const glm::vec4& _value);
			void SetUniformVec4v(const char* _name, unsigned count, const glm::vec4& _value);
			void SetUniformMat4(const char* _name, const glm::mat4& _value);

			bool Load();
			bool Unload();
			void Reload();

			unsigned GetProgramHandle() const
			{
				return m_ProgramHandle;
			}

			std::shared_ptr<Resource::GLSL> m_Shader;
		};
	}
}

#endif // _PXF_MODULE_DGFX_SHADER_H_

