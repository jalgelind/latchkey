#ifndef _PXF_MODULE_DGFX_GLFWWINDOW_H_
#define _PXF_MODULE_DGFX_GLFWWINDOW_H_

#include <Pxf/Base/Types.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Graphics/WindowDetails.h>

namespace Pxf{
	namespace Graphics {

		class WindowGL2 : public Window
		{
		public:
			WindowGL2(GraphicsDevice* _Device, WindowDetails *_window_spec);
			virtual ~WindowGL2();

			bool Open();
			void Activate();
			bool Close();
			void Swap();

			void SetTitle(const char *_title);
			void SetWindowSize(int _Width, int _Height);

			int GetFPS();
			int GetWidth();
			int GetHeight();
			float GetAspectRatio();
			const char* GetContextTypeName();

			bool IsOpen();
			bool IsActive();
			bool IsMinimized();
		private:
			void* m_handle;

			int m_width, m_height;
			int m_xpos, m_ypos;
			bool m_fullscreen, m_resizeable, m_vsync;

			// Bit settings
			int m_bits_r, m_bits_g, m_bits_b;
			int m_bits_alpha, m_bits_depth, m_bits_stencil;

			// FSAA
			int m_fsaa_samples;

			// FPS
			int64 m_fps_laststamp;
			int m_fps, m_fps_count;
			Logger m_Logger;
		};

	} // Graphics
} // Pxf

#endif // _PXF_MODULE_DGFX_GLFWWINDOW_H_

