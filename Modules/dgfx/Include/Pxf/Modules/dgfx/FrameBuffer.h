#ifndef _PXF_MODULE_DGFX_FRAMEBUFFEROBJECT_H_
#define _PXF_MODULE_DGFX_FRAMEBUFFEROBJECT_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/FrameBuffer.h>
#include <Pxf/Modules/dgfx/OpenGL.h>

namespace Pxf{
	namespace Graphics
	{
		class GraphicsDevice;
		class RenderBuffer;
	}

	namespace Modules {
		class FrameBufferGL2 : public Graphics::FrameBuffer
		{
		public:
			FrameBufferGL2(Graphics::GraphicsDevice* _Device);
			virtual ~FrameBufferGL2();

			void Attach(const std::shared_ptr<Graphics::Texture>& _Texture, const Graphics::FrameBufferAttachmentType _Attachment, bool _GenMipmaps);
			void Attach(const std::shared_ptr<Graphics::RenderBuffer>& _Buffer, const Graphics::FrameBufferAttachmentType _Attachment);
			void Detach(const Graphics::FrameBufferAttachmentType _Attachment);

			void Bind();

			int GetNumAttached();
			bool CheckFBO(unsigned _status);

			unsigned GetHandle() { return m_Handle; }
		private:
			unsigned int Translate(Graphics::FrameBufferAttachmentType _type);
			void _Configure();	// check max attachments
			unsigned m_Handle;
			Logger m_Logger;
			//GLenum m_Attachments[16];
		};
	} // Graphics
} // Pxf

#endif //_PXF_MODULE_DGFX_FRAMEBUFFEROBJECT_H_

