#ifndef _PXF_MODULE_DGFX_RENDERBUFFER_H_
#define _PXF_MODULE_DGFX_RENDERBUFFER_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/RenderBuffer.h>

namespace Pxf{
    namespace Graphics
    {
		class GraphicsDevice;
    }

	namespace Modules {
		class RenderBufferGL2 : public Graphics::RenderBuffer
		{
		public:
			RenderBufferGL2(Graphics::GraphicsDevice* _Device, unsigned _Format, unsigned _Width, unsigned _Height);

			virtual ~RenderBufferGL2();

			unsigned GetHandle() { return m_Handle; }
			unsigned GetFormat() { return m_Format; }

			void ReleaseBuffer();

		private:
			void _InitBuffer();

			void Bind();

			unsigned m_Handle;
			unsigned m_Format;
			Logger m_Logger;
		};

	} // Graphics
} // Pxf

#endif //_PXF_MODULE_DGFX_RENDERBUFFER_H_

