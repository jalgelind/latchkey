#ifndef _PXF_MODULE_DGFX_TEXTURE_H_
#define _PXF_MODULE_DGFX_TEXTURE_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Modules/dgfx/Device.h>
#include <Pxf/Graphics/Texture.h>

#include <string>

namespace Pxf
{
	namespace Graphics
	{

		//! Abstract texture class
		class TextureGL2 : public Texture
		{
		public:
			TextureGL2(GraphicsDevice* _pDevice);
			virtual ~TextureGL2();

			bool IsValid()
			{
				return m_TextureID != 0;
			}

			void SetMipmapHint(bool on);
			void SetPOTHint(bool on);

			void CreateEmpty(int _width, int _height, TextureFormat _format);
			void LoadData(const std::shared_ptr<Resource::Image>& _img);
			void Unload();
			void Reload();

			void Bind(unsigned texture_unit = 0);
			static void Unbind(unsigned texture_unit);

			int GetWidth();
			int GetHeight();
			int GetChannels();

			void SetMagFilter(TextureFilter _Filter);
			void SetMinFilter(TextureFilter _Filter);

			void SetClampMethod(TextureClampMethod _Method);

			glm::vec4 CreateTextureSubset(float _x1, float _y1, float _x2, float _y2);

			bool HasMipmaps()
			{
				return m_HasMipmaps;
			}

			std::shared_ptr<Resource::Image> ToImage();

			// OGL specific
			unsigned int GetTextureID();

		private:
			GLuint m_TextureID;
			TextureFilter m_MinFilter;
			TextureFilter m_MagFilter;
			TextureClampMethod m_ClampMethod;
			int m_Width, m_Height, m_Channels;
			TextureFormat m_Format;
			Logger m_Logger;
			std::shared_ptr<Resource::Image> m_Image;
			bool m_HasMipmaps;
			bool m_ForcePOT;
			bool m_MipmapHint;
		};
	} // Graphics
} // Pxf

#endif // _PXF_MODULE_DGFX_TEXTURE_H_

