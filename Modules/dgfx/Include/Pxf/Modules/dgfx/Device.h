#ifndef _PXF_MODULE_DGFX_DEVICE_H_
#define _PXF_MODULE_DGFX_DEVICE_H_

#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Modules/dgfx/OpenGL.h>
#include <Pxf/Modules/dgfx/Window.h>

namespace Pxf{
	class Logger;
	namespace Modules {
		class DeviceGL2 : public Graphics::GraphicsDevice
		{
		public:
			DeviceGL2(Pxf::Kernel* _Kernel);
			virtual ~DeviceGL2();

			uint64 GetMemorySize();
			uint64 GetMemoryUsage();
			uint64 GetDedicatedMemory();
			uint64 GetEvictedMemory();
			unsigned GetEvictionCount();

			uint64 GetTextureMemoryUsage();
			uint64 GetVBOMemoryUsage();

			const char* GetVendor();
			const char* GetRenderer();
			const char* GetVersion();
			const char* GetShaderLangVersion();


			std::shared_ptr<Graphics::Window> OpenWindow(Graphics::WindowDetails& _pWindowSpecs);
			void SetActiveWindow(const std::shared_ptr<Graphics::Window>& _Window);
			const std::shared_ptr<Graphics::Window>& GetActiveWindow();
			const std::shared_ptr<Graphics::Window> GetWindow(int id);

			Graphics::DeviceType GetDeviceType() { return Graphics::OPENGL2; }

			void SetProjectionMatrix(glm::mat4& _matrix);
			glm::mat4 GetProjectionMatrix();
			void PushProjectionMatrix();
			void PopProjectionMatrix();

			void SetModelViewMatrix(glm::mat4& _matrix);
			glm::mat4 GetModelViewMatrix();
			void PushModelMatrix();
			void PopModelMatrix();

			void SetViewport(int _x, int _y, int _w, int _h);
			void SetViewport(Graphics::Rect rect);
			Graphics::Rect GetViewport();

			void PushViewportAttribute();
			void PopAttribute();

			void SwapBuffers();
			void Translate(glm::vec3 _translate);
			void Scale(glm::vec3 _scale);
			void SetDepthFunction(Graphics::DepthFuncType _DepthType);
			bool SetDepthTest(bool _On); /* returns previous depth test value */
			void SetCullFace(bool _On);

			void Clear(float r, float g, float b, float a);
			void SetBlendNone();
			void SetBlendAdditive();
			void SetBlendNormal();

			void SetClip(int x, int y, int w, int h);
			void SetNoClip();

			std::shared_ptr<Graphics::Texture> CreateEmptyTexture(int _Width,int _Height, Graphics::TextureFormat _Format = Graphics::TEX_FORMAT_RGBA);
			std::shared_ptr<Graphics::Texture> CreateTexture(const char* _filepath, bool force_pot = false);
			std::shared_ptr<Graphics::Texture> CreateTexture(const std::shared_ptr<Resource::Image>& _Image, bool force_pot = false);
			std::shared_ptr<Graphics::Texture> CreateTextureFromData(unsigned char* _datachunk, int _width, int _height, int _channels, bool force_pot = false);
			std::shared_ptr<Graphics::Texture> BindTexture(const std::shared_ptr<Graphics::Texture>& _texture, unsigned int _texture_unit = 0);

			std::shared_ptr<Graphics::Texture> CreateTextureFromFramebuffer();
			std::shared_ptr<Resource::Image> CreateImageFromTexture(const std::shared_ptr<Graphics::Texture>& _texture);

			std::shared_ptr<Graphics::VertexBuffer> CreateVertexBuffer(Graphics::VertexBufferLocation _VertexBufferLocation, Graphics::VertexBufferUsageFlag _VertexBufferUsageFlag);
			void DrawBuffer(const std::shared_ptr<Graphics::VertexBuffer>& _pVertexBuffer, unsigned _VertexOffset = 0, unsigned _VertexCount = -1);

			std::shared_ptr<Graphics::RenderBuffer> CreateRenderBuffer(unsigned _Format, unsigned _Width, unsigned _Height);
			void BindRenderBuffer(const std::shared_ptr<Graphics::RenderBuffer>& _pRenderBuffer);

			std::shared_ptr<Graphics::FrameBuffer> CreateFrameBuffer();
			std::shared_ptr<Graphics::FrameBuffer> BindFrameBuffer(const std::shared_ptr<Graphics::FrameBuffer>& _pFrameBuffer);

			float SetLineWidth(float v);
			float SetPointSize(float v);

			std::shared_ptr<Graphics::Shader> CreateShader(const char* _Ident, const std::shared_ptr<Resource::GLSL>& _Shader);
			std::shared_ptr<Graphics::Shader> CreateShader(const char* _Ident, const char* _Shader);
			std::shared_ptr<Graphics::Shader> CreateShaderFromPath(const char* _Ident, const char* _ShaderPath);
			std::shared_ptr<Graphics::Shader> BindShader(const std::shared_ptr<Graphics::Shader>& _Shader);
		private:
			bool Init(){ return true; };
			Logger m_Logger;
			std::vector<std::shared_ptr<Graphics::Window>> m_Windows;
		};

	} // Graphics
} // Pxf

#endif //_PXF_MODULE_DGFX_DEVICE_H_

