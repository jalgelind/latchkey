#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Modules/img/ImageLoader.h>
#include <Pxf/Resource/Chunk.h>

#include <SOIL.h>

using namespace Pxf;

Modules::SOILImage::SOILImage(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
	: Resource::Image(_Kernel, _Chunk, _Loader)
	, m_IsRaw(false)
{
	m_Logger = Logger(_Kernel, "res");
	Build();
}

bool Modules::SOILImage::Build()
{
	if(m_ImageData && !m_IsRaw)
		SOIL_free_image_data(m_ImageData);

	// load raw data into memory
	m_ImageData = SOIL_load_image_from_memory(reinterpret_cast<const unsigned char*>(m_Chunk->data), m_Chunk->size, &m_Width, &m_Height, &m_Channels, 0);

	if (!m_ImageData)
	{
		const char* error = SOIL_last_result();
		m_Logger.Error("failed to create image from '%s'", GetSource() ? GetSource() : "unknown");
		m_Logger.Error(error);
		return false;
	}
	return true;
}
Modules::SOILImage::~SOILImage()
{
	if(m_ImageData && !m_IsRaw)
		SOIL_free_image_data(m_ImageData);
}

bool Modules::SOILImage::SaveAs(const char* _Filename)
{
	std::string ext = Path::GetExt(_Filename);
	int save_type = SOIL_SAVE_TYPE_PNG;
	if (ext == "png")
		save_type = SOIL_SAVE_TYPE_PNG;
	else
		m_Logger.Error("save images using .png. '%s' is not supported.", ext.c_str());
	return SOIL_save_image(_Filename, save_type, m_Width, m_Height, m_Channels, m_ImageData);
}

Modules::GenericImageLoader::GenericImageLoader(Pxf::Kernel* _Kernel)
	: ImageLoader(_Kernel, "Generic Image Loader")
{
	m_Logger = Logger(m_Kernel, "res");
}

Modules::GenericImageLoader::~GenericImageLoader()
{
}

Resource::Image* Modules::GenericImageLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = Resource::LoadFile(_FilePath);
	if (!chunk)
		return NULL;
	return new SOILImage(m_Kernel, chunk, this);
}

Resource::Image* Modules::GenericImageLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	if (_Path)
		chunk->source = _Path;
	else
		chunk->source = "unknown";
	chunk->data = (void*) _DataPtr;
	chunk->size = _DataLen;
	chunk->is_static = true;
	return new SOILImage(m_Kernel, chunk, this);
}

Resource::Image* Modules::GenericImageLoader::CreateFromRaw(int _Width, int _Height, int _Channels, unsigned char* _DataPtr, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	unsigned char* data = new unsigned char[_Width*_Height*_Channels];
	size_t size = _Width*_Height*_Channels;
	std::memcpy(data, _DataPtr, size);
	chunk->data = (void*) data;
	chunk->size = _Width * _Height * _Channels;
	chunk->is_static = false;
	return new SOILImage(m_Kernel, chunk, this, _Width, _Height, _Channels);
}

