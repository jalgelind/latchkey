#include <Pxf/Kernel.h>
#include <Pxf/System.h>
#include <Pxf/Module.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Modules/img/img.h>
#include <Pxf/Modules/img/ImageLoader.h>


static const unsigned Module_Kernel_Version = Pxf::Kernel::KERNEL_VERSION;
static const unsigned Module_Api_Version = Pxf::Module::MODULE_VERSION;

using Pxf::Modules::GenericImageImporter;
#ifndef CONF_MODULAR
REGISTER_MODULE(GenericImageImporter, "img", Module_Kernel_Version, Module_Api_Version);
#endif

namespace GenericImageLoader_
{
	PXFEXPORT Pxf::Module* CreateInstance()
	{
		Pxf::Modules::GenericImageImporter *m = new Pxf::Modules::GenericImageImporter("img", Module_Kernel_Version, Module_Api_Version);
		return m;
	}

	PXFEXPORT void DestroyInstance(Pxf::Module* _module)
	{
		if (_module)
		{
			delete _module;
		}
	}
}

bool Pxf::Modules::GenericImageImporter::RegisterSystem(Pxf::Kernel* _Kernel, unsigned _SystemType)
{
	if (_SystemType & Pxf::System::RESOURCE_LOADER)
	{
		// Multiple loaders, so they can be deleted by the kernel without trouble (sharing loader =>
		// multiple deletions / instance).
		Pxf::Modules::GenericImageLoader* pngloader = new Pxf::Modules::GenericImageLoader(_Kernel);
		Pxf::Modules::GenericImageLoader* jpgloader = new Pxf::Modules::GenericImageLoader(_Kernel);
		Pxf::Modules::GenericImageLoader* jpegloader = new Pxf::Modules::GenericImageLoader(_Kernel);
		Pxf::Modules::GenericImageLoader* gifloader = new Pxf::Modules::GenericImageLoader(_Kernel);
		_Kernel->RegisterResourceLoader("png", pngloader);
		_Kernel->RegisterResourceLoader("jpg", jpgloader);
		_Kernel->RegisterResourceLoader("jpeg", jpegloader);
		_Kernel->RegisterResourceLoader("gif", gifloader);
		return true;
	}

	return false;
}

