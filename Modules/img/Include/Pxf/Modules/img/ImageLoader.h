#ifndef _PXF_RESOURCE_IMAGELOADER_H_
#define _PXF_RESOURCE_IMAGELOADER_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/Image.h>


namespace Pxf{
namespace Resource
{
	class Chunk;
}

namespace Modules {

	class SOILImage : public Resource::Image
	{
	protected:
		Logger m_Logger;
		bool m_IsRaw;
		virtual bool Build();
	public:
		SOILImage(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader);

		SOILImage(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader, int _Width, int _Height, int _Channels)
			: Resource::Image(_Kernel, _Chunk, _Loader, _Width, _Height, _Channels)
			, m_IsRaw(true)
		{
			m_ImageData = (unsigned char*)_Chunk->data;
		}

		virtual ~SOILImage();

		virtual bool SaveAs(const char* _Filename);
	};

	class GenericImageLoader : public Resource::ImageLoader
	{
	private:
		Logger m_Logger;
		bool Init(){ return true; }
	public:
		GenericImageLoader(Pxf::Kernel* _Kernel);
		~GenericImageLoader();
		virtual Resource::Image* Load(const char* _FilePath);
		virtual Resource::Image* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
		virtual Resource::Image* CreateFromRaw(int _Width, int _Height, int _Channels, unsigned char* _DataPtr, const char* _Path = 0);
	};

} // Graphics
} // Pxf

#endif //_PXF_RESOURCE_IMAGELOADER_H_

