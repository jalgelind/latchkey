#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Resource/Chunk.h>
#include <Pxf/Modules/snd/WavAudioLoader.h>

#include <cstdlib>
#include <cstdio>

using namespace Pxf;

bool Modules::WavSound::FindSection(MemoryStream& ms, uint32 section)
{
	uint32 id = 0;
	while(true)
	{
		id = ms.ReadBE<uint32>();
		if (id == section)
			break;
		else
		{
			char* tmp = (char*)&id;
			//m_Logger.Error("Wav: skipping section '%c%c%c%c'", tmp[3], tmp[2], tmp[1], tmp[0]);
			int skipbytes = ms.ReadLE<uint32>();
			ms.Skip(skipbytes);
			if (ms.GetRemainingBytes() <= 0)
				return false;
		}
	}
	return true;
}

bool Modules::WavSound::Build()
{
	if (m_SoundData)
		delete [] m_SoundData;

	// https://ccrma.stanford.edu/courses/422/projects/WaveFormat/

	const uint16 WAVE_FORMAT_PCM = 1;
	const uint16 WAVE_FORMAT_IEEE_FLOAT = 3;

	MemoryStream ms((char*)m_Chunk->data, m_Chunk->size);

	uint32 ChunkID = ms.ReadBE<uint32>();
	uint32 ChunkSize = ms.ReadLE<uint32>();
	uint32 Format = ms.ReadBE<uint32>();
	if (ChunkID != 'RIFF' || Format != 'WAVE')
	{
		m_Logger.Error("Invalid wave header.");
		return false;
	}

	if (!FindSection(ms, 'fmt '))
	{
		m_Logger.Warning("No format section found.");
		return false;
	}

	uint32 Subchunk1Size = ms.ReadLE<uint32>();
	uint16 AudioFormat = ms.ReadLE<uint16>();

	if (AudioFormat != WAVE_FORMAT_PCM)
	{
		m_Logger.Error("Unsupported audio format 0x00%d - only supports PCM (0x00%d)", AudioFormat, WAVE_FORMAT_PCM);
		return false;
	}

	uint16 NumChannels = ms.ReadLE<uint16>();
	uint32 SampleRate = ms.ReadLE<uint32>();
	uint32 ByteRate = ms.ReadLE<uint32>();
	uint16 BlockAlign = ms.ReadLE<uint16>();
	uint16 BitsPerSample = ms.ReadLE<uint16>();

	int bytes_left = Subchunk1Size - 16;

	if (bytes_left != 0)
	{
		m_Logger.Warning("Found extra header data - skipping");
		ms.Skip(bytes_left);
	}

	if (!FindSection(ms, 'data'))
	{
		m_Logger.Warning("No data section found.");
		return false;
	}

	uint32 Subchunk2Size = ms.ReadLE<uint32>();
	uint32 left = ms.GetRemainingBytes();

	int bps = 0;
	if (AudioFormat == WAVE_FORMAT_PCM)
	{
		if (BitsPerSample == 16)
		{
			bps = 2;
			m_Format = FMT_PCM16;

			m_SoundDataLen = (Subchunk2Size / bps);
			m_SoundData = new char[Subchunk2Size];
			ms.Read(m_SoundData, Subchunk2Size);
		}
		else if (BitsPerSample == 24)
		{
			bps = 4;
			m_Format = FMT_PCM24;

			m_SoundDataLen = Subchunk2Size / 3;
			m_SoundData = new char[m_SoundDataLen * 4];
			int* sounddata = (int*)m_SoundData;
			for(int i = 0; i < m_SoundDataLen; i++)
			{
				char tmp[3];
				ms.Read(&tmp, 3);
				sounddata[i] = ((signed char)tmp[2] << 16)
				             | ((unsigned char)tmp[1] << 8)
				             | ((unsigned char)tmp[0]);
			}
		}
	}
	else if (AudioFormat == WAVE_FORMAT_IEEE_FLOAT)
	{
		bps = 4;
		m_Format = FMT_FLOAT;

		m_SoundDataLen = (Subchunk2Size / bps);
		m_SoundData = new char[Subchunk2Size];
		ms.Read(m_SoundData, Subchunk2Size);
	}

	m_SampleRate = SampleRate;
	m_Channels = NumChannels;
	return true;
}
Modules::WavSound::~WavSound()
{
	if (m_SoundData)
		free(m_SoundData);
}

Modules::WavAudioLoader::WavAudioLoader(Pxf::Kernel* _Kernel)
	: SoundLoader(_Kernel, "Wav Audio Loader")
{
	m_Logger = new Logger(_Kernel, "snd");
}

Modules::WavAudioLoader::~WavAudioLoader()
{
	if (m_Logger)
		delete m_Logger;
}

Resource::Sound* Modules::WavAudioLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = Resource::LoadFile(_FilePath);
	if (!chunk)
		return NULL;
	return new WavSound(m_Kernel, chunk, this);
}

Resource::Sound* Modules::WavAudioLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	if (_Path)
		chunk->source = _Path;
	else
		chunk->source = "unknown";
	chunk->data = (void*) _DataPtr;
	chunk->size = _DataLen;
	chunk->is_static = true;
	return new WavSound(m_Kernel, chunk, this);
}

