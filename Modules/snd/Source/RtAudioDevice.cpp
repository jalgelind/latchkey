#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Resource/Sound.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Modules/snd/RtAudioDevice.h>
#include <RtAudio.h>

#include <mutex>

using namespace Pxf;
using namespace Pxf::Modules;

#define MAX_REGISTERED_SOUNDS 128

using std::lock_guard;
using std::mutex;

mutex g_Lock;

class DefaultAudioCallback : public Audio::AudioCallback
{
	public:
	DefaultAudioCallback(Audio::AudioDevice* _device)
		: Audio::AudioCallback(_device)
	{};
	virtual ~DefaultAudioCallback(){};
	void Mix(float* _in, float* _out, unsigned _channels, unsigned _size)
	{
		float** fout = (float**)_out;
		RtAudioDevice* device = (RtAudioDevice*)m_AudioDevice;
		std::vector<RtAudioDevice::SoundEntry>* voices = device->GetVoices();
		unsigned int num_voices = voices->size();

		RtAudioDevice::SoundEntry* entry;

		// TODO: compare performance with copying _num_frames*2 at a time for each voice.
		// TODO: support _channels

		for(unsigned int i = 0; i < _size*2; i += 2)
		{
			if (m_BufferFormat == AudioCallback::EBuffersInterleaved)
			{
				_out[i+0] = 0.f;
				_out[i+1] = 0.f;
			}
			else
			{
				fout[0][i] = 0.f;
				fout[1][i] = 0.f;
			}
			for (int j = 0; j < num_voices; j++)
			{
				entry = &voices->at(j);
				if (entry->clip && entry->active)
				{
					if (entry->current_frame >= entry->clip->DataLen())
					{
						entry->current_frame = 0;
						if (!entry->loop)
						{
							entry->clip = 0;
							continue;
						}
					}
					float ch0, ch1;
					
					switch (entry->clip->Format())
					{
						case Resource::Sound::FMT_PCM16:
						{
							const float fscale = 1.f / (1 << 15);
							short* dataptr = (short*)entry->clip->DataPtr();
							ch0 = dataptr[entry->current_frame] * fscale;
							ch1 = dataptr[entry->current_frame + 1] * fscale;
							break;
						}
						case Resource::Sound::FMT_PCM24:
						{
							const float fscale = 1.f / (1 << 23);
							int* dataptr = (int*)entry->clip->DataPtr();
							ch0 = dataptr[entry->current_frame] * fscale;
							ch1 = dataptr[entry->current_frame + 1] * fscale;
							break;
						}
						case Resource::Sound::FMT_FLOAT:
						{
							float* dataptr = (float*)entry->clip->DataPtr();
							ch0 = dataptr[entry->current_frame];
							ch1 = dataptr[entry->current_frame + 1];
							break;
						}
					}

					if (m_BufferFormat == AudioCallback::EBuffersInterleaved)
					{
						_out[i+0] = ch0;
						_out[i+1] = ch1;
					}
					else
					{
						fout[0][i] = ch0;
						fout[1][i] = ch1;
					}
					entry->current_frame += 2;
				}
			}
		}
	}
};

int mix(void *_outbuff, void *_inbuff, unsigned int _num_frames,
		double _time, RtAudioStreamStatus _status, void *_device)
{
	RtAudioDevice* device = (RtAudioDevice*)_device;
	lock_guard<mutex> guard(g_Lock);

	if (device->_IsClosed())
	{
		return 2;
	}

	if (_status == RTAUDIO_OUTPUT_UNDERFLOW)
	{
		device->_ShowMixerWarning("Warning: underflow, stuttering might occur...");
	}

	Audio::AudioCallback* callback = device->GetAudioCallback();
	if(callback)
		callback->Mix(0, (float*)_outbuff, 2, _num_frames);

	// Continue if active, else abort stream.
	return device->IsActive() ? 0 : 2;
}

RtAudioDevice::RtAudioDevice(Pxf::Kernel* _Kernel)
	: Pxf::Audio::AudioDevice(_Kernel, "Rt Audio Device")
	, m_Active(false)
	, m_Channels(2)
	, m_Initialized(false)
	, m_Closed(false)
	, m_DAC(nullptr)
{
	m_Logger = Logger(_Kernel, "snd");
	SetAudioCallback(Pxf::make_unique<DefaultAudioCallback>(this));
}

bool RtAudioDevice::Initialize()
{
	if (m_Initialized)
	{
		CloseStream();
	}

	m_SoundBank.resize(MAX_REGISTERED_SOUNDS);
	m_ActiveVoices.resize(m_MaxActiveVoices);

	m_DAC = Pxf::make_unique<RtAudio>();
	m_DAC->showWarnings(true);

	unsigned num_devices = m_DAC->getDeviceCount();

	/* Initialize audio stream */
	if (num_devices == 0)
	{
		m_Logger.Information("No available output devices");
		return false;
	}

	// Just assume this for now, would be nice to override somehow
	RtAudio::StreamParameters params;
	params.deviceId = m_DAC->getDefaultOutputDevice();
	params.nChannels = 2;
	params.firstChannel = 0;

	unsigned int buffer_frames = m_PreferredBufferSize;
	RtAudio::StreamOptions options;
	options.flags = RTAUDIO_MINIMIZE_LATENCY;

	try
	{
		m_Active = true;
		m_Closed = false;
		m_DAC->openStream(&params, NULL, RTAUDIO_FLOAT32, m_SamplingRate, &buffer_frames, &mix, (void*)this);
		m_DAC->startStream();
		m_Initialized = true;
	}
	catch (RtError& e)
	{
		m_Logger.Error("Fatal error: %s", e.getMessage().c_str());
	}
	return true;
}

RtAudioDevice::~RtAudioDevice()
{
	if (m_Active)
		Shutdown();
}

void RtAudioDevice::Shutdown()
{
	m_Active = false;
	auto callbackinterval = m_SamplingRate / m_PreferredBufferSize;

	// Let the stream shut down by itself first, then force it. Then 
	// hope it actually shut down, so it doesnt attempt to use a bunch of deleted stuff elsewhere...
	// is it possible to join() with the audio thread?
	Platform::ThreadSleep(200 + 2 * callbackinterval);
	StopAll();
	CloseStream();
	Platform::ThreadSleep(200 + 2 * callbackinterval);
}

void RtAudioDevice::CloseStream()
{
	if (!m_Initialized)
		return;

	try
	{
		if (m_DAC->isStreamOpen())
		{
			m_DAC->closeStream();
			m_Closed = true;
			m_Active = false;
		}
	}
	catch (RtError& e)
	{
		m_Logger.Error("Fatal error trying to stop stream: %s", e.getMessage().c_str());
	}
}

int RtAudioDevice::RegisterSound(const char* _Filename)
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	auto snd = GetKernel()->GetResourceManager()->Acquire<Resource::Sound>(_Filename);
	for(int i = 0; i < MAX_REGISTERED_SOUNDS; i++)
	{
		if (m_SoundBank[i] == snd)
		{
			m_Logger.Information("Trying to register sound '%s' more than once.", _Filename);
			return i;
		}
	}

	for(int i = 0; i < MAX_REGISTERED_SOUNDS; i++)
	{
		if (m_SoundBank[i] == NULL)
		{
			m_SoundBank[i] = snd;
			return i;
		}
	}
	return -1;
}

int RtAudioDevice::RegisterSound(const std::shared_ptr<Resource::Sound>& _Sound)
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	for(int i = 0; i < MAX_REGISTERED_SOUNDS; i++)
	{
		if (m_SoundBank[i] == _Sound)
		{
			m_Logger.Information("Trying to register sound '%s' more than once.", _Sound->GetSource());
			return i;
		}
	}

	_Sound->_AddRef();
	for(int i = 0; i < MAX_REGISTERED_SOUNDS; i++)
	{
		if (m_SoundBank[i] == NULL)
		{
			m_SoundBank[i] = _Sound;
			return i;
		}
	}
	return -1;
}

int RtAudioDevice::GetSoundID(const std::shared_ptr<Resource::Sound>& _Sound)
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	for(int i = 0; i < MAX_REGISTERED_SOUNDS; i++)
	{
		if (m_SoundBank[i] == _Sound)
		{
			return i;
		}
	}
	return -1;
}

void RtAudioDevice::UnregisterSound(int _Id)
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	if(m_SoundBank[_Id])
	{
		for(unsigned i = 0; i < m_MaxActiveVoices; i++)
		{
			if (m_ActiveVoices[i].clip == m_SoundBank[_Id])
				m_ActiveVoices[i].clip = 0;
		}

		GetKernel()->GetResourceManager()->Release(m_SoundBank[_Id]);
		m_SoundBank[_Id] = 0;
	}
}

void RtAudioDevice::Play(int _SoundID, bool _Loop)
{
	// Invalid
	if (_SoundID == -1)
		return;

	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	if (m_SoundBank[_SoundID])
	{
		unsigned free_slot = -1;
		for(unsigned i = 0; i < m_MaxActiveVoices; i++)
		{
			// Restart playback or resume paused sound
			if (m_ActiveVoices[i].clip == m_SoundBank[_SoundID])
			{
				if (m_ActiveVoices[i].active)
					m_ActiveVoices[i].current_frame = 0;
				m_ActiveVoices[i].active = true;
				return;
			}
			// id of free slot
			else if (m_ActiveVoices[i].clip == 0)
				free_slot = i;
		}

		// No sound with it was paused, and there is a voice available
		if (free_slot != -1)
		{
			m_ActiveVoices[free_slot].clip = m_SoundBank[_SoundID];
			m_ActiveVoices[free_slot].current_frame = 0;
			m_ActiveVoices[free_slot].active = true;
			m_ActiveVoices[free_slot].loop = _Loop;
			return;
		}
	}
}

void RtAudioDevice::Stop(int _SoundID)
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	if (m_SoundBank[_SoundID])
	{
		for(unsigned i = 0; i < m_MaxActiveVoices; i++)
		{
			if (m_ActiveVoices[i].clip == m_SoundBank[_SoundID])
			{
				m_ActiveVoices[i].clip = 0;
				m_ActiveVoices[i].current_frame = 0;
				m_ActiveVoices[i].active = false;
				return;
			}
		}
	}
}

void RtAudioDevice::StopAll()
{
	if (!m_Initialized)
		return;

	lock_guard<mutex> guard(g_Lock);

	for(unsigned i = 0; i < m_MaxActiveVoices; i++)
	{
		if (m_ActiveVoices[i].clip)
		{
			m_ActiveVoices[i].clip = 0;
			m_ActiveVoices[i].current_frame = 0;
			m_ActiveVoices[i].active = false;
			return;
		}
	}
}

void RtAudioDevice::Pause(int _SoundID)
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	if (m_SoundBank[_SoundID])
	{
		for(unsigned i = 0; i < m_MaxActiveVoices; i++)
		{
			if (m_ActiveVoices[i].clip == m_SoundBank[_SoundID])
			{
				m_ActiveVoices[i].active = false;
				return;
			}
		}
	}
}

void RtAudioDevice::PauseAll()
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	for(unsigned i = 0; i < m_MaxActiveVoices; i++)
	{
		if (m_ActiveVoices[i].clip)
		{
			m_ActiveVoices[i].active = false;
			return;
		}
	}
}

void RtAudioDevice::DumpInfo()
{
	if (!m_Initialized)
		Initialize();

	lock_guard<mutex> guard(g_Lock);

	/* Enumerate audio devices */

	unsigned num_devices = m_DAC->getDeviceCount();
	m_Logger.Information("Available output devices: %d", num_devices);

	RtAudio::DeviceInfo info;
	for(int i = 0; i < num_devices; i++)
	{
		info = m_DAC->getDeviceInfo(i);
		if (info.probed)
		{
			m_Logger.Information(" | %d%s %s (cout: %d; cin: %d)", i, info.isDefaultOutput ? " >" : "."
						 ,info.name.c_str(), info.outputChannels, info.inputChannels);
		}
	}
}

void RtAudioDevice::_ShowMixerWarning(const char* _Msg)
{
	m_Logger.Warning(_Msg);
}

