#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Resource/Chunk.h>
#include <Pxf/Modules/snd/OggAudioLoader.h>

#include <stb_vorbis.h>

#include <cstdlib>
#include <cstdio>

using namespace Pxf;

bool Modules::OggSound::Build()
{
	if (m_SoundData)
		free(m_SoundData);
	short* data = 0;
	m_SoundDataLen = stb_vorbis_decode_memory((unsigned char*)m_Chunk->data, m_Chunk->size, &m_Channels, &data);
	m_SoundData = (char*)data;
	m_SoundDataLen *= m_Channels;
	m_Format = FMT_PCM16;

	int error;
	stb_vorbis *v = stb_vorbis_open_memory((unsigned char*)m_Chunk->data, m_Chunk->size, &error, NULL);
	if (v)
	{
		stb_vorbis_info info = stb_vorbis_get_info(v);
		m_SampleRate = info.sample_rate;
		stb_vorbis_close(v);
	}

	return true;
}
Modules::OggSound::~OggSound()
{
	if (m_SoundData)
		free(m_SoundData);
}

Modules::OggAudioLoader::OggAudioLoader(Pxf::Kernel* _Kernel)
	: SoundLoader(_Kernel, "Ogg Audio Loader")
{
	m_Logger = new Logger(_Kernel, "snd");
}

Modules::OggAudioLoader::~OggAudioLoader()
{
	if (m_Logger)
		delete m_Logger;
}

Resource::Sound* Modules::OggAudioLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = Resource::LoadFile(_FilePath);
	if (!chunk)
		return NULL;
	return new OggSound(m_Kernel, chunk, this);
}

Resource::Sound* Modules::OggAudioLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	if (_Path)
		chunk->source = _Path;
	else
		chunk->source = "unknown";
	chunk->data = (void*) _DataPtr;
	chunk->size = _DataLen;
	chunk->is_static = true;
	return new OggSound(m_Kernel, chunk, this);
}

