#ifndef _PXF_RESOURCE_WAVAUDIOLOADER_H_
#define _PXF_RESOURCE_WAVAUDIOLOADER_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/Sound.h>


namespace Pxf{
class MemoryStream;
namespace Resource
{
	class Chunk;
}

namespace Modules {

	class WavSound : public Resource::Sound
	{
	protected:
		virtual bool Build();
	public:
		WavSound(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
			: Resource::Sound(_Kernel, _Chunk, _Loader)
		{
			Build();
		}

		bool FindSection(MemoryStream& ms, uint32 section);

		virtual ~WavSound();
	};

	class WavAudioLoader : public Resource::SoundLoader
	{
	private:
		Logger* m_Logger;
		bool Init(){ return true; }
	public:
		WavAudioLoader(Pxf::Kernel* _Kernel);
		~WavAudioLoader();
		Resource::Sound* Load(const char* _FilePath);
		Resource::Sound* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
	};

}
}

#endif //_PXF_RESOURCE_WAVAUDIOLOADER_H_

