#ifndef _PXF_MODULES_RTAUDIODEVICE_H_
#define _PXF_MODULES_RTAUDIODEVICE_H_

#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Audio/AudioCallback.h>

#include <vector>
#include <memory>

class RtAudio;

namespace Pxf
{
	namespace Modules
	{
		class RtAudioDevice : public Pxf::Audio::AudioDevice
		{
		public:
			struct SoundEntry
			{
				std::shared_ptr<Resource::Sound> clip;
				unsigned int current_frame;
				bool active;
				bool loop;
				SoundEntry(const std::shared_ptr<Resource::Sound>& _clip, unsigned int _current_frame, bool _active, bool _loop)
				{
					clip = _clip;
					current_frame = _current_frame;
					active = _active;
					loop = _loop;
				}
				SoundEntry()
				{
					clip = 0;
					current_frame = 0;
					active = false;
					loop = false;
				}
			};

		protected:
			std::unique_ptr<RtAudio> m_DAC;
			std::vector<std::shared_ptr<Resource::Sound>> m_SoundBank;
			std::vector<SoundEntry> m_ActiveVoices;
			unsigned int m_Channels;
			Logger m_Logger;

			bool m_Active;
			bool m_Initialized;
			bool m_Closed;

			void CloseStream();

			virtual bool Init() { return true; }

		public:
		RtAudioDevice(Pxf::Kernel* _Kernel);
		virtual ~RtAudioDevice();

		virtual bool Initialize();
		virtual void Shutdown();

		virtual int RegisterSound(const char* _Filename);
		virtual int RegisterSound(const std::shared_ptr<Resource::Sound>& _Sound);
		virtual int GetSoundID(const std::shared_ptr<Resource::Sound>& _Sound);
		virtual void UnregisterSound(int _Id);
		virtual void Play(int _SoundID, bool _Loop);
		virtual void Stop(int _SoundID);
		virtual void StopAll();
		virtual void Pause(int _SoundID);
		virtual void PauseAll();
		virtual void DumpInfo();

		void _ShowMixerWarning(const char* _Msg);
		bool _IsClosed() {return m_Closed;}

		std::vector<std::shared_ptr<Resource::Sound>>* GetSoundBank()
		{
			return &m_SoundBank;
		}

		std::vector<SoundEntry>* GetVoices()
		{
			return &m_ActiveVoices;
		}

		bool IsActive() const
		{
			return m_Active;
		}

		};
	}
}

#endif // _PXF_MODULES_RTAUDIODEVICE_H_

