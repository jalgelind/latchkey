#ifndef _PXF_RESOURCE_OGGAUDIOLOADER_H_
#define _PXF_RESOURCE_OGGAUDIOLOADER_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/Sound.h>


namespace Pxf{
namespace Resource
{
	class Chunk;
}

namespace Modules {

	class OggSound : public Resource::Sound
	{
	protected:
		virtual bool Build();
	public:
		OggSound(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
			: Resource::Sound(_Kernel, _Chunk, _Loader)
		{
			Build();
		}

		virtual ~OggSound();
	};

	class OggAudioLoader : public Resource::SoundLoader
	{
	private:
		Logger* m_Logger;
		bool Init(){ return true; }
	public:
		OggAudioLoader(Pxf::Kernel* _Kernel);
		~OggAudioLoader();
		Resource::Sound* Load(const char* _FilePath);
		Resource::Sound* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
	};

}
}

#endif //_PXF_RESOURCE_OGGAUDIOLOADER_H_

