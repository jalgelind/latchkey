Import("../../Builder.lua")


module = NewModule("snd")
module:RequireLibrary("stb_vorbis")
module:RequireLibrary("rtaudio")
module:AddIncludeDirectory("Include")
module:AddSourceDirectory("Source/*.cpp")
