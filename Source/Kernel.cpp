#include <Pxf/Kernel.h>
#include <Pxf/Module.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/SharedLibrary.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/TaskPool.h>
#include <Pxf/Audio/NullAudioDevice.h>
#include <Pxf/Input/NullInputDevice.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Network/NetworkDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Util/Configuration.h>

#include <algorithm>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <mutex>

using namespace Pxf;

Kernel* Kernel::s_Kernel = 0;
const unsigned Kernel::KERNEL_VERSION = PXF_PACKSHORT2(1, 62);

#ifdef CONF_RELEASE
#include <Pxf/License.gen>
#else
license_information_t pxf_autogen_license_information = {
    "*** See license.txt ***", NULL, 0
};
#endif

static std::mutex g_KernelLoggerMutex;

Kernel::Kernel()
	: m_AudioDevice(nullptr)
	, m_InputDevice(nullptr)
	, m_GraphicsDevice(nullptr)
	, m_ResourceManager(nullptr)
	, m_NetworkDevice(nullptr)
	, m_FileLogger(nullptr)
	, m_StdLogger(nullptr)
	, m_ExecutableType(EXE_NATIVE)
	, m_Configuration(nullptr)
	, m_TaskPool(nullptr)
{
	s_Kernel = this;
	m_KernelLogger = Logger(this, "krn");

	AddResourcePath("");

	m_Configuration = make_unique<Configuration>(this);

	// We need to make sure that the resource manager is created in this address space.
	// (And we know that the kernel is created in the in the main application executable.)
	m_ResourceManager = make_unique<Resource::ResourceManager>(this);
	m_InputDevice = make_unique<Input::InputDevice>(this);
}

Kernel::~Kernel()
{
	m_Configuration.reset();

	m_GraphicsDevice.reset();
	m_AudioDevice.reset();
	m_ResourceManager->Shutdown();
	m_ResourceManager.reset();

	for (auto mod: m_AvailableModules)
		mod.reset();
}

//static
const license_information_t* Kernel::GetLicenseInformation()
{
	return &pxf_autogen_license_information;
}

const std::string& Kernel::GetPathOfExecutable()
{
	return m_PathOfExecutable;
}

const std::string& Kernel::GetCommandLineArgumentString()
{
	return m_CommandLine;
}

const std::vector<std::string>& Kernel::GetCommandLineArguments()
{
	return m_CommandLineArguments;
}

const std::unique_ptr<Configuration>& Kernel::GetConfiguration() const
{
	return m_Configuration;
}

void Kernel::AddResourcePath(const std::string& path, const std::string& ext)
{
	auto ps = m_ResourcePaths[ext];
	if (std::find(begin(ps), end(ps), path) == end(ps))
		m_ResourcePaths[ext].push_back(path);
}

std::vector<std::string> Kernel::GetResourcePaths(const std::string& ext)
{
	return m_ResourcePaths[ext];
}

bool Kernel::GetResourcePath(const std::string& filename, std::string& result)
{
	// scan global paths
	auto paths = GetResourcePaths();
	for(auto& path: paths)
	{
		std::string pathtest = path + filename;
		if (Path::Exist(pathtest) || m_ResourceManager->HasCachedFile(pathtest))
		{
			result = path + filename;
			return true;
		}
	}

	// scan extension path
	std::string ext = Path::GetExt(filename);
	paths = GetResourcePaths(ext);
	for(auto& path: paths)
	{
		if (Path::Exist(path + filename))
		{
			result = path + filename;
			return true;
		}
	}

	return false;
}

void Kernel::RegisterAudioDevice(std::unique_ptr<Audio::AudioDevice> _Device)
{
	if (_Device == nullptr)
		m_AudioDevice = make_unique<Audio::NullAudioDevice>(this);
	else
		m_AudioDevice = std::move(_Device);
	if (!m_AudioDevice->IsNull())
		m_KernelLogger.Information("Registered audio device '%s'", m_AudioDevice->GetIdentifier());
}

Audio::AudioDevice* Kernel::GetAudioDevice()
{
	if (!m_AudioDevice)
		RegisterAudioDevice(nullptr);
	return m_AudioDevice.get();
}

void Kernel::RegisterInputController(std::unique_ptr<Input::InputController> _Controller)
{
	if (_Controller == nullptr)
		return;
	m_InputDevice->RegisterInputController(std::move(_Controller));
}

Input::InputDevice* Kernel::GetInputDevice()
{
	return m_InputDevice.get();
}

void Kernel::RegisterGraphicsDevice(std::unique_ptr<Graphics::GraphicsDevice> _Device)
{
	m_KernelLogger.Information("Registered video device '%s'", _Device->GetIdentifier());
	m_GraphicsDevice = std::move(_Device);
}

Graphics::GraphicsDevice* Kernel::GetGraphicsDevice()
{
	return m_GraphicsDevice.get();
}

void Kernel::RegisterNetworkDevice(std::unique_ptr<Network::NetworkDevice> _Device)
{
	m_KernelLogger.Information("Registered network device '%s'", _Device->GetIdentifier());
	m_NetworkDevice = std::move(_Device);
}

Network::NetworkDevice* Kernel::GetNetworkDevice()
{
	return m_NetworkDevice.get();
}

void Kernel::RegisterResourceLoader(const char* _Ext, Resource::ResourceLoader* _ResourceLoader)
{
	Resource::ResourceManager* mgr = GetResourceManager();
	mgr->RegisterResourceLoader(_Ext, _ResourceLoader);
}

Resource::ResourceManager* Kernel::GetResourceManager()
{
	return m_ResourceManager.get();
}

Logger Kernel::GetLogger(const char* _Identifier)
{
	return Logger(this, _Identifier);
}

void Kernel::RegisterLoggingDevice(const std::shared_ptr<LoggingDevice>& _logger)
{
	g_KernelLoggerMutex.lock();
	m_LoggingDevices.push_back(_logger);
	g_KernelLoggerMutex.unlock();
}

void Kernel::UnregisterLoggingDevice(const std::shared_ptr<LoggingDevice>& _logger)
{
	g_KernelLoggerMutex.lock();
	m_LoggingDevices.erase(std::remove(m_LoggingDevices.begin(), m_LoggingDevices.end(), _logger)
						  ,m_LoggingDevices.end());
	g_KernelLoggerMutex.unlock();
}

void Kernel::LoggingDeviceBroadcast(LogMessage* _Message)
{
	g_KernelLoggerMutex.lock();
	auto iter = m_LoggingDevices.begin();
	for(; iter != m_LoggingDevices.end(); ++iter)
		(*iter)->Write(_Message);
	g_KernelLoggerMutex.unlock();
}

bool Kernel::RegisterModule(const char* _FilePath, unsigned _Filter, bool _OverrideBuiltin)
{
	// If the file is missing extention, add one that's appropriate for the platform.
	const char* suffix = CONF_MODULE_SUFFIX;
	std::string filepath;

#	if defined(CONF_PLATFORM_MACOSX)
		if (!IsPrefix(_FilePath, "./"))
			filepath += "./lib";
#	elif defined(CONF_FAMILY_UNIX)
		if (!IsPrefix(_FilePath, "./"))
			filepath += "./";
#	endif

	filepath += _FilePath;
	if (!IsSuffix(filepath, CONF_DYLIB_EXT))
		filepath += suffix;

	std::shared_ptr<SharedLibrary> lib = std::make_shared<SharedLibrary>();
	if (!Path::Exist(filepath))
	{
		// Only report error if this is a modular build.
		// Otherwise we will always fall back on internal modules.
		#ifdef CONF_MODULAR
		m_KernelLogger.Error("Module '%s' not found.", filepath.c_str());
		#endif
		return false;
	}
	if(!lib->Load(filepath))
	{
		const char* error = lib->GetError();
		if (error != 0x0)
		{
			std::string buff = error;
			std::remove(buff.begin(), buff.end(), '\r');
			std::replace(buff.begin(), buff.end(), '\n', ' ');
			m_KernelLogger.Error("Error loading '%s' => '%s'", filepath.c_str(), buff.c_str());
		}
		else
		{
			m_KernelLogger.Error("Error loading '%s' => 'unknown'", filepath.c_str());
		}
		return false;
	}

	typedef Module*(*CreateInstance_fun)(void);
	CreateInstance_fun CreateInstance = (CreateInstance_fun)lib->LookupName("CreateInstance");
	DestroyModuleInstance_fun DestroyInstance = (DestroyModuleInstance_fun)lib->LookupName("DestroyInstance");

	if(!CreateInstance || !DestroyInstance)
	{
		m_KernelLogger.Debug("Library initialization failed, missing exports ('%s').", filepath.c_str());
		return false;
	}

	std::shared_ptr<Module> module = std::shared_ptr<Module>(CreateInstance(), DestroyInstance);

	// Check that the module isn't already available, or override
	bool replaced = false;
	for(unsigned int i = 0; i < m_AvailableModules.size(); i++)
	{
		if (strcmp(m_AvailableModules[i]->module->GetIdentifier(), module->GetIdentifier()) == 0)
		{
			if (!_OverrideBuiltin)
			{
				m_KernelLogger.Debug("Module '%s' is already built-in. Updated filter.", module->GetIdentifier());
				m_AvailableModules[i]->filter = _Filter;
				return false;
			}
			else
			{
				m_KernelLogger.Debug("'%s' is overriding built-in '%s'.", filepath.c_str(), module->GetIdentifier());
				m_AvailableModules[i]->module = module;
				m_AvailableModules[i]->filter = _Filter;
				m_AvailableModules[i]->dynlib = lib;
				m_AvailableModules[i]->destroy = DestroyInstance;
				replaced = true;
				break;
			}
		}
	}

	unsigned short kmaj, kmin, mmaj, mmin, currkmaj, currkmin, currmmaj, currmmin;
	UnpackShort2(module->GetKernelVersion(), &kmaj, &kmin);
	UnpackShort2(module->GetApiVersion(), &mmaj, &mmin);
	UnpackShort2(Kernel::KERNEL_VERSION, &currkmaj, &currkmin);
	UnpackShort2(Module::MODULE_VERSION, &currmmaj, &currmmin);

	if (kmaj < currkmaj || (kmaj == currkmaj && kmin < currkmin))
		m_KernelLogger.Warning("Warning, kernel version mismatch (%d.%d is recommended)", currkmaj, currkmin);
	if (mmaj < currmmaj || (mmaj == currmmaj && mmin < currmmin))
		m_KernelLogger.Warning("Warning - Module API version mismatch (%d.%d is recommended)", currmmaj, currmmin);

	if (!replaced)
		m_AvailableModules.push_back(std::make_shared<ModuleEntry_t>(_Filter, lib, module, DestroyInstance));
	return true;
}

bool Kernel::SetModuleFilter(const char* _ModuleName, unsigned _Filter)
{
	for(unsigned int i = 0; i < m_AvailableModules.size(); i++)
	{
		if (strcmp(m_AvailableModules[i]->module->GetIdentifier(), _ModuleName) == 0)
		{
			m_AvailableModules[i]->filter = _Filter;
			return true;
		}
	}
	return false;
}

static void DestroyBuiltInInstance(Module* _Module)
{
	if (_Module)
		delete _Module;
}

bool Kernel::RegisterModule(Module* _Module)
{
	m_AvailableModules.push_back(std::make_shared<ModuleEntry_t>(0xFFFFFFFF, std::shared_ptr<Module>(_Module), DestroyBuiltInInstance));
	return true;
}

// Initialize kernel resource here for determenistic initialization.
// (the constructor is first run by a static initializer, when
// statically built).

void Kernel::Initialize(std::string exepath, std::vector<std::string> args)
{
	for(auto s: args)
		m_CommandLine += s + " ";
	m_CommandLineArguments = args;
	m_PathOfExecutable = exepath;
	m_FileLogger = std::make_shared<FileLogger>();
	m_StdLogger = std::make_shared<StdLogger>();
	RegisterLoggingDevice(m_FileLogger);
	RegisterLoggingDevice(m_StdLogger);
	m_KernelLogger.Information("Kernel initialized.");
	m_TaskPool = std::unique_ptr<TaskPool>(new TaskPool());
	m_ResourceManager->Initialize();
}

void Kernel::InitializeModules()
{
	auto iter = m_AvailableModules.begin();
	for(; iter != m_AvailableModules.end(); ++iter)
	{
		(*iter)->module->RegisterSystem(this, (*iter)->filter);
	}
	m_KernelLogger.Information("Modules initialized.");
}


void Kernel::DumpAvailableModules()
{
	m_KernelLogger.Information("Enumerating available modules...");
	for(unsigned int i = 0; i < m_AvailableModules.size(); i++)
	{
		const char* path = "built-in";
		if (m_AvailableModules[i]->dynlib)
			path = m_AvailableModules[i]->dynlib->GetFilePath().c_str();
		m_KernelLogger.Information("| %d. %s (%s)", i, m_AvailableModules[i]->module->GetIdentifier(), path);
	}
}

