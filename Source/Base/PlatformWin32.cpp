#include <Pxf/Base/Platform.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Random.h>
#include <Pxf/Base/String.h>
#ifdef CONF_FAMILY_WINDOWS

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h> // QueryPerformanceCounter et al
#include <DbgHelp.h> // SYMBOL_INFO...
#include <time.h>

using namespace Pxf;

bool Platform::s_TimeInitialized = false;
int64 Platform::m_Timestamp = 0;
int64 Platform::m_Frequency = -1;

class PlatformInitializer
{
public:
	PlatformInitializer()
	{
		Platform platform;
	}
};
static PlatformInitializer init;

void Platform::Initialize()
{
	int64 freq = 0;
	if (QueryPerformanceFrequency((LARGE_INTEGER*) &freq))
	{
		m_Frequency = freq;
		QueryPerformanceCounter((LARGE_INTEGER*) &m_Timestamp);
	}
	else
	{
		m_Frequency = -1;
		// TODO:  fallback on timeGetTime()?
	}
	RandSetSeed(m_Timestamp);
}

int64 Platform::GetTime()
{
	int64 newTimeStamp;
	QueryPerformanceCounter((LARGE_INTEGER*) &newTimeStamp);

	return ((newTimeStamp - m_Timestamp) * 1000) / m_Frequency;
}

double Platform::GetTimeF()
{
	int64 newTimeStamp;
	QueryPerformanceCounter((LARGE_INTEGER*) &newTimeStamp);

	return ((newTimeStamp - m_Timestamp) * 1000.0) / m_Frequency;
}

int32 Platform::GetNumberOfProcessors()
{
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	return si.dwNumberOfProcessors;
}

std::vector<std::string> Platform::GetStacktrace()
{
	std::vector<std::string> stacktrace;
#ifdef CONF_FAMILY_WINDOWS
		unsigned int   i;
		void         * stack[ 100 ];
		unsigned short frames;
		SYMBOL_INFO  * symbol;
		HANDLE         process;

		process = GetCurrentProcess();

		SymInitialize( process, NULL, TRUE );

		frames               = CaptureStackBackTrace( 0, 100, stack, NULL );
		symbol               = ( SYMBOL_INFO * )calloc( sizeof( SYMBOL_INFO ) + 256 * sizeof( char ), 1 );
		symbol->MaxNameLen   = 255;
		symbol->SizeOfStruct = sizeof( SYMBOL_INFO );

		for( i = 0; i < frames; i++ )
		{
			SymFromAddr( process, ( DWORD64 )( stack[ i ] ), 0, symbol );

			std::string frame = Pxf::Format("%i: %s - 0x%0X\n", frames - i - 1, symbol->Name, symbol->Address);
			stacktrace.push_back(frame);
		}

		free( symbol );
#endif
	return stacktrace;
}


void Platform::ThreadSleep(int32 _ms)
{
	Sleep(_ms);
}

#endif // CONF_FAMILY_WINDOWS

