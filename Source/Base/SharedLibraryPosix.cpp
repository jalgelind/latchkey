#include <Pxf/Base/Config.h>

#ifdef CONF_FAMILY_UNIX

#include <Pxf/Base/SharedLibrary.h>
#include <string.h>
#include <cstdio>
#include <dlfcn.h>

Pxf::SharedLibrary::SharedLibrary()
	: m_pHandle(0)
{
}

Pxf::SharedLibrary::~SharedLibrary()
{
	if (m_pHandle)
		Close();
}

bool Pxf::SharedLibrary::Load(const std::string& _File)
{
	m_Path = _File;
	m_pHandle = dlopen(m_Path.c_str(), RTLD_NOW);
	return m_pHandle != 0;
}

bool Pxf::SharedLibrary::Close()
{
	if (dlclose(m_pHandle) < 0)
		return false;
	else
		return true;
}

void* Pxf::SharedLibrary::LookupName(const std::string& _Name)
{
	return dlsym(m_pHandle, _Name.c_str());
}

char* Pxf::SharedLibrary::GetError()
{
	return dlerror();
}

#endif // CONF_FAMILY_UNIX

