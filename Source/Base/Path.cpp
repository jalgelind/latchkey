#include <Pxf/Base/Config.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Stream.h>

#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>
#include <Pxf/Resource/ResourceManager.h>

#include <cstring>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

#include <dirent.h>

#ifdef CONF_FAMILY_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <sys/stat.h>
#include <sys/types.h>
#include <cerrno>

using namespace Pxf;

Path::Path()
	: m_Valid(false)
{}

Path::Path(const std::string& _path)
	: m_Valid(false)
{
	Set(_path);
}

bool Path::_dostat(const char* path, struct stat* _sb)
{
	if (stat(path, _sb) == -1)
		return false;
	return true;
}

bool Path::_mkdir(const std::string& _path)
{
#ifdef CONF_FAMILY_WINDOWS
	CreateDirectory(_path.c_str(), NULL);
	DWORD lasterror = GetLastError();
	if (lasterror > 0 && lasterror != ERROR_ALREADY_EXISTS)
		return false;
#elif defined(CONF_FAMILY_UNIX)
	struct stat sb;
	int mode = 0777;
	if (_dostat(_path.c_str(), &sb))
		mode = sb.st_mode;
	if (!mkdir(_path.c_str(), mode))
		return false;
#endif
	return true;
}

void Path::Set(const std::string& _path)
{
	m_Valid = Exist(_path);
	m_Path = _path;
}

std::string Path::GetExt()
{
	return GetExt(m_Path);
}

std::string Path::GetFileName()
{
	return GetFileName(m_Path);
}

std::string Path::GetPath()
{
	return GetPath(m_Path);
}

std::string Path::Join(const std::string& path2) const
{
	return Join(m_Path, path2);;
}

void Path::Mkdir()
{
	return Mkdir(m_Path);
}

std::string Path::GetFullPath()
{
	return m_Path;
}

int64 Path::GetAccessTime()
{
	if (!m_Valid)
		return false;
	return GetAccessTime(m_Path);
}

int64 Path::GetModifiedTime()
{
	if (!m_Valid)
		return false;
	return GetModifiedTime(m_Path);
}

int64 Path::GetCreationTime()
{
	if (!m_Valid)
		return false;
	return GetCreationTime(m_Path);
}

bool Path::IsDirectory()
{
	if (!m_Valid)
		return false;
	return Path::IsDirectory(m_Path);
}

bool Path::IsFile()
{
	if (!m_Valid)
		return false;
	return Path::IsFile(m_Path);
}

bool Path::IsValid()
{
	return m_Valid;
}

bool Path::Exists()
{
	bool exist = Exist(m_Path);
	if (!exist)
		m_Valid = false;
	return exist;
}

std::vector<Path::FileEntry> Path::ListDirectory(const std::string& path, int levels)
{
	std::vector<Path::FileEntry> ret;

	if (levels <= 0)
		return ret;

	DIR* d = opendir(path.c_str());
	if(!d)
	{
		// TODO: report error
		return ret;
	}

	dirent* entry;
	while(1)
	{
		entry = readdir(d);
		if (!entry)
			break;
		if(entry->d_type & DT_DIR)
		{
			Path::FileEntry e = Path::FileEntry(Path::FileEntry::DIRECTORY, entry->d_name);
			if (e.name == "." || e.name == "..")
				continue;
			e.children = ListDirectory(path + "/" + e.name, levels - 1);
			ret.push_back(e);
		}
		else if(entry->d_type & DT_REG)
		{
			ret.push_back(Path::FileEntry(Path::FileEntry::FILE, entry->d_name));
		}
	}

	return ret;
}

std::string Path::GetExt(const std::string& _path)
{
	// Note: Valgrind does not like this function, "substr" is leaking. Not sure why.

	std::string::size_type idx;
	idx = _path.rfind('.');
	if(idx == std::string::npos)
		return ""; // No extension found
	std::string ext = _path.substr(idx+1);
	return ext;
}

std::string Path::GetFileName(const std::string& _path)
{
	std::string::size_type idx;
	idx = StringFindRev2(_path, '/', '\\');
	if (idx == std::string::npos)
		return _path;
	return _path.substr(idx + 1);
}

std::string Path::GetPath(const std::string& _path)
{
	std::string::size_type idx;
	idx = StringFindRev2(_path, '/', '\\');
	if (idx == std::string::npos)
		return "./";
	return _path.substr(0, idx + 1);
}

std::string Path::Join(const std::string& path1, const std::string& path2)
{
	std::string ret = path1;
	if (!ret.empty() && ret.back() != '/')
		ret += "/";
	ret += path2;
	return ret;
}

void Path::Mkdir(const std::string& _path)
{
	std::string partial;
	std::string::size_type idx;
	idx = StringFindRev2(_path, '/', '\\');
	if (idx != std::string::npos)
	{
		Logger logger = Kernel::GetInstance()->GetLogger("path");
		std::string sep;
		sep += _path[idx];
		std::vector<std::string> strs = split(_path, sep[0]);
		std::reverse(strs.begin(), strs.end());
		while(!strs.empty())
		{
			std::string& last = strs.back();
			if (last == sep || last == "")
				break;
			partial += strs.back() + sep;
			strs.pop_back();
			_mkdir(partial.c_str());
		}
	}
	else if (_path != "")
	{
		_mkdir(_path);
	}
}

int64 Path::GetAccessTime(const std::string& _path)
{
	struct stat sb;
	if (!_dostat(_path.c_str(), &sb))
		return 0;
	return sb.st_atime;
}

int64 Path::GetModifiedTime(const std::string& _path)
{
	struct stat sb;
	if (!_dostat(_path.c_str(), &sb))
		return 0;
	return sb.st_mtime;
}

int64 Path::GetCreationTime(const std::string& _path)
{
	struct stat sb;
	if (!_dostat(_path.c_str(), &sb))
		return 0;
	return sb.st_ctime;
}

bool Path::IsDirectory(const std::string& _path)
{
	struct stat sb;
	if (!_dostat(_path.c_str(), &sb))
		return false;
	if ((sb.st_mode&S_IFMT) != S_IFDIR)
		return false;
	return true;
}

bool Path::IsFile(const std::string& _path)
{
	struct stat sb;
	if (!_dostat(_path.c_str(), &sb))
		return false;
	if ((sb.st_mode&S_IFMT) != S_IFREG)
		return false;
	return true;
}

// TODO: Should check other paths registered with the resource manager.
bool Path::Exist(const std::string& _path)
{
	Pxf::Kernel* krn = Kernel::GetInstance();
	if (krn->GetResourceManager()->HasCachedFile(_path.c_str()))
		return true;

	// Needed?
	if (_path.size() > 256)
		return false;

#ifdef CONF_FAMILY_WINDOWS
	DWORD fileattr = GetFileAttributes(_path.c_str());
	return 0xFFFFFFFF != fileattr;
#else
	struct stat sb;
	if (stat(_path.c_str(), &sb) == -1)
		return false;
	return true;
#endif
}
