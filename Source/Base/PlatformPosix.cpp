#include <Pxf/Base/Platform.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Random.h>
#ifdef CONF_PLATFORM_LINUX
#include <execinfo.h>
#endif
#ifdef CONF_FAMILY_UNIX
#include <sys/time.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

using namespace Pxf;

bool Platform::s_TimeInitialized = false;
int64 Platform::m_Frequency = -1;
int64 Platform::m_Timestamp = 0;

class PlatformInitializer
{
public:
	PlatformInitializer()
	{
		Platform platform;
	}
};
static PlatformInitializer init;

void Platform::Initialize()
{
	struct timeval timeVal;
	gettimeofday(&timeVal, NULL);
	m_Timestamp = (int64)timeVal.tv_sec * (int64)1000000 + (int64)timeVal.tv_usec;
	m_Frequency = 1e-6;
	RandSetSeed(m_Timestamp);
}

int64 Platform::GetTime()
{
	struct timeval timeVal;
	gettimeofday(&timeVal, NULL);
	int64 newTimeStamp = (int64)timeVal.tv_sec * (int64)1000000 + (int64)timeVal.tv_usec;
	return (newTimeStamp - m_Timestamp) / (int64)1000;
}

double Platform::GetTimeF()
{
	struct timeval timeVal;
	gettimeofday(&timeVal, NULL);
	int64 newTimeStamp = (int64)timeVal.tv_sec * (int64)1000000 + (int64)timeVal.tv_usec;
	return (newTimeStamp - m_Timestamp) / 1000.0;
}

int32 Platform::GetNumberOfProcessors()
{
	int n = 1;
	n = (int)sysconf(_SC_NPROCESSORS_ONLN);
	return n;
}

std::vector<std::string> Platform::GetStacktrace()
{
	std::vector<std::string> stacktrace;
	#ifdef CONF_PLATFORM_LINUX
	size_t sz; 
    void *bt[20];
    char **strings;

    sz = backtrace(bt, 20);
    strings = backtrace_symbols(bt, sz);

    for(int i = 0; i < sz; ++i)
        stacktrace.push_back(strings[i]);
	#else
	stacktrace.push_back("*** stacktrace not available for this platform");
	#endif
	return stacktrace;
}

void Platform::ThreadSleep(int32 _ms)
{
	usleep(_ms*1000);
}

#endif // CONF_FAMILY_UNIX

