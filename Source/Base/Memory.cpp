#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Config.h>

#ifdef CONF_FAMILY_WINDOWS
#include <malloc.h>
#else
#include <cstdint>
#endif

#include <cstdlib>
#include <cstdio>
#include <cstring>