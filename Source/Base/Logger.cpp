#include <Pxf/Base/Config.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Kernel.h>
#include <cstdio>
#include <cstdarg>
#include <cstring>
#include <string>

using namespace Pxf;

Logger::Logger()
	: m_Kernel(0)
	, m_Target("default")
	, m_Identifier("unk")
{}

Logger::Logger(Kernel* _Kernel, const char* _Destination)
	: m_Kernel(_Kernel)
	, m_Target("default")
{
	std::vector<std::string> parts = split(_Destination, '.');
	if (parts.size() == 1)
		m_Identifier = _Destination;
	else if (parts.size() == 2)
	{
		m_Target = parts[0];
		m_Identifier = parts[1];
	}
	else
	{
		PXF_ASSERT(0, "Unexpected logger identifier string.");
	}
}

#define MESSAGE_FORMAT_IMPL(MSGTYPE)                               \
	char buffer[4092] = {0};                                       \
	va_list va;                                                    \
	va_start(va, _Format);                                         \
	size_t identifier_len = m_Identifier.size();                   \
	vsprintf(buffer, _Format, va);                                 \
	va_end(va);                                                    \
	LogMessage m;                                                  \
	m.message = buffer;                                            \
	m.source = this;                                               \
	m.type = MSGTYPE;                                              \
	if (m_Kernel)                                                  \
		m_Kernel->LoggingDeviceBroadcast(&m);                      \

void Logger::Warning(const char* _Format, ...)
{
	MESSAGE_FORMAT_IMPL('W');
}

void Logger::Error(const char* _Format, ...)
{
	MESSAGE_FORMAT_IMPL('E');
}

void Logger::Information(const char* _Format, ...)
{
	MESSAGE_FORMAT_IMPL('I');
}

void Logger::Debug(const char* _Format, ...)
{
	MESSAGE_FORMAT_IMPL('D');
}

#undef MESSAGE_FORMAT_IMPL

/*  Find a decent position where to split the input string `_txt`.
	Return offset adjustment with origin between back_limit and fwd_limit.
*/
static int adjust_offset(const char* _txt, int offset, int rest)
{
	const int back_limit = 30;
	const int fwd_limit = 10;
	const int len = back_limit + fwd_limit;
	const char* start = _txt + Max(0, offset - back_limit);
	const char* chars = " /'\"<>[]\0";
	int idx = 0;
	for(int ti = 0; ti < len; ++ti)
	{
		for(int i = 0; i < sizeof(chars)/sizeof(chars[0]); ++i)
		{
			if (start[ti] == chars[i])
			{
				idx = -back_limit + ti + 1;
				break;
			}
		}
	}
	return idx;
}

LoggingDevice::LoggingDevice()
	: force_flush(true)
	, linebreakhint(69)
	, enabled(true)
{
#if defined(CONF_FAMILY_WINDOWS)
	linebreakhint = 69; // preferred split length for a windows console window.
#else
	linebreakhint = 180;
#endif
}

void LoggingDevice::FormatPrefix(LogMessage* logmsg, std::string& prefix)
{
    prefix = "";
    prefix += "[";
    prefix += logmsg->type;
    prefix += "][";
    prefix += logmsg->source->GetIdentifier();
    prefix += "] ";
}

bool LoggingDevice::Write(LogMessage* _LogMessage)
{
	if (!enabled)
		return false;
	PreWrite(_LogMessage);
	std::string prefix;
	std::string composed;
	FormatPrefix(_LogMessage, prefix);
	composed = prefix + _LogMessage->message;
	const char* _Message = composed.c_str();
	size_t len = composed.length();
	int maxchars = linebreakhint;

	size_t prefixlen = prefix.length();
	int offset = 0;
	static const char dots[] = ".............................";
	static const int dotslen = sizeof(dots)/sizeof(dots[0]);
	prefixlen = Min(prefixlen, dotslen) - 1;
	if (maxchars != -1 && len > maxchars)
	{
		const char* msg = 0;
		while (offset+maxchars <= len)
		{
			if (offset > 0 && prefixlen > 0)
			{
				WriteImpl(dots, prefixlen);
				//WriteImpl(_Message, prefixlen);
				WriteImpl(" ", 1);
			}
			int offsetadj = adjust_offset(_Message, offset+maxchars, len - offset+maxchars);
			msg = _Message + offset;
			WriteLImpl(msg, maxchars + offsetadj);
			offset += maxchars + offsetadj;
		}
		int rest = len - offset;
		if (rest > 0)
		{
			WriteImpl(dots, prefixlen);
			WriteImpl(" ", 1);
			msg = _Message + offset;
			return WriteLImpl(msg, rest);
		}
		return true;
	}
	return WriteLImpl(_Message, len);
}

// Logger for stdout

bool StdLogger::WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength)
{
	int ret = 1;
	if ((void*)stdout != (void*)-1)
	{
		ret = fwrite(_SrcBuffer, 1, _SrcLength, stdout);
		if (force_flush)
			fflush(stdout);
	}
	return ret > 0;
}

bool StdLogger::WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength)
{
	int ret = WriteImpl(_SrcBuffer, _SrcLength);
	ret = WriteImpl("\n", 1);
	return ret > 0;
}


FileLogger::FileLogger(const char* filename)
{
	force_flush = true;
	_fs = NULL;
	_filename = filename;
	linebreakhint = -1;
}

FileLogger::~FileLogger()
{
	for (auto& kv: _fss)
	{
		kv.second->Close();
	}
	_fs = NULL;
}

void FileLogger::PreWrite(LogMessage* m)
{
	// Select output file...
	const std::string& target = m->source->GetTarget();
	if (_fss.find(target) == end(_fss))
	{
		std::string logfile = target + ".log";
		_fss[target] = std::move(make_unique<FileStream>());
		_fss[target]->OpenWriteBinary(logfile.c_str());
	}
	_fs = _fss[target].get();
}

bool FileLogger::WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength)
{
	if (_fs->IsValid())
	{
		_fs->Write(_SrcBuffer, _SrcLength);
		if (force_flush)
			return _fs->Flush();
		return true;
	}
	return false;
}

bool FileLogger::WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength)
{
	WriteImpl(_SrcBuffer, _SrcLength);
	return WriteImpl("\r\n", 2);
}
