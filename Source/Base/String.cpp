#include <Pxf/Base/String.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Utils.h>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cstdarg> /* strfmt_n*/
#include <algorithm>
#include <cctype>
#include <functional> 
#include <locale>

#include <string>
#include <vector>
#include <sstream>


std::vector<std::string> &Pxf::split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> Pxf::split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	return split(s, delim, elems);
}

std::string& Pxf::ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

std::string& Pxf::rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

std::string& Pxf::trim(std::string &s) {
	return ltrim(rtrim(s));
}

// http://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
std::string Pxf::Format(const char *fmt, va_list ap)
{
	// Allocate a buffer on the stack that's big enough for us almost
	// all the time.  Be prepared to allocate dynamically if it doesn't fit.
	size_t size = 1024;
	char stackbuf[1024];
	std::vector<char> dynamicbuf;
	char *buf = &stackbuf[0];

	while (1) {
		// Try to vsnprintf into our buffer.
		int needed = vsnprintf (buf, size, fmt, ap);
		// NB. C99 (which modern Linux and OS X follow) says vsnprintf
		// failure returns the length it would have needed.  But older
		// glibc and current Windows return -1 for failure, i.e., not
		// telling us how much was needed.

		if (needed <= (int)size && needed >= 0) {
			// It fit fine so we're done.
			return std::string (buf, (size_t) needed);
		}

		// vsnprintf reported that it wanted to write more characters
		// than we allotted.  So try again using a dynamic buffer.  This
		// doesn't happen very often if we chose our initial size well.
		size = (needed > 0) ? (needed+1) : (size*2);
		dynamicbuf.resize (size);
		buf = &dynamicbuf[0];
	}
}

std::string Pxf::Format(const char *format, ...)
{
	va_list ap;
	va_start (ap, format);
	std::string buf = Format(format, ap);
	va_end (ap);
	return buf;
}

/*
 * String manipulaton functions which modifies input string
 *****************************************************************************/

void Pxf::StringToLower(std::string& p)
{
	std::transform(p.begin(), p.end(), p.begin(), tolower);
}

void Pxf::StringToUpper(std::string& p)
{
	std::transform(p.begin(), p.end(), p.begin(), toupper);
}

/*
 * String find functions
 *****************************************************************************/

std::string::size_type Pxf::StringFind2(const std::string& _target, const char find, const char alt)
{
	std::string::size_type pos = std::string::npos;
	if ((pos = _target.find(find)) != std::string::npos)
		return pos;
	else if ((pos = _target.find(alt)) != std::string::npos)
		return pos;
	return pos;
}

std::string::size_type Pxf::StringFindRev2(const std::string& _target, const char find, const char alt)
{
	std::string::size_type pos = std::string::npos;
	if ((pos = _target.rfind(find)) != std::string::npos)
		return pos;
	else if ((pos = _target.rfind(alt)) != std::string::npos)
		return pos;
	return pos;
}

/*
 * String compare functions
 *****************************************************************************/

#ifdef _WIN32
	#define _stricmp(a,b) stricmp(a,b)
	#define _strnicmp(a,b,n) strnicmp(a,b,n)
#else
	#define _stricmp(a,b) strcasecmp(a,b)
	#define _strnicmp(a,b,n) strncasecmp(a,b,n)
#endif

bool Pxf::IsEqualI(const std::string& str1, const std::string& str2)
{
	return _stricmp(str1.c_str(), str2.c_str()) == 0;
}

bool Pxf::IsPrefix(const std::string& _s, const std::string& _p)
{
	if (_s.size() == 0 && _p.size() > 0) return false;
	if (_p.size() == 0 && _s.size() > 0) return false;
	const char* p = _s.c_str();
	const char* s = _p.c_str();
	for(;*p && *s;p++,s++)
		if(*p != *s)
			return false;
	return true;
}

bool Pxf::IsPrefixI(const std::string& _s, const std::string& _p)
{
	if (_s.size() == 0 && _p.size() > 0) return false;
	if (_p.size() == 0 && _s.size() > 0) return false;
	const char* p = _p.c_str();
	const char* s = _s.c_str();
	for(;*p && *s;p++,s++)
		if(tolower(*p) != tolower(*s))
			return false;
	return true;
}

bool Pxf::IsSuffix(const std::string& _s, const std::string& _p)
{
	if (_s.size() == 0 && _p.size() > 0) return false;
	if (_p.size() == 0 && _s.size() > 0) return false;
	const char* p = _p.c_str();
	const char* s = _s.c_str();
	p += _p.size() - 1;
	s += _s.size() - 1;
	for(;p >= _p.c_str() && *p && s >= _s.c_str() && *s;s--,p--)
		if(*p != *s)
			return false;
	return true;
}

bool Pxf::IsSuffixI(const std::string& _s, const std::string& _p)
{
	if (_s.size() == 0 && _p.size() > 0) return false;
	if (_p.size() == 0 && _s.size() > 0) return false;
	const char* p = _p.c_str();
	const char* s = _s.c_str();
	p += _p.size() - 1;
	s += _s.size() - 1;
	for(;p >= _p.c_str() && *p && s >= _s.c_str() && *s;s--,p--)
		if(tolower(*p) != tolower(*s))
			return false;
	return true;
}

bool Pxf::IsWhitespace(const char c)
{
	return (c == ' ' || c == '\n' || c == '\t' || c == '\r');
}

bool Pxf::IsAlpha(const char c)
{
	if ((c >= 'a') && (c <= 'z')) return true;
	if ((c >= 'A') && (c <= 'Z')) return true;
	return false;
}

bool Pxf::IsNumeric(const char c)
{
	return (c >= '0') && (c <= '9');
}

bool Pxf::IsAlphanumeric(const char c)
{
	return IsAlpha(c) || IsNumeric(c) || c == '_';
}

/*
 * String conversion functions
 *****************************************************************************/
int Pxf::StringToInteger(const std::string&  number)
{
	return atoi(number.c_str());
}

double Pxf::StringToDouble(const std::string&  number)
{
	return atof(number.c_str());
}


