#include <Pxf/Base/TaskPool.h>
#include <Pxf/Base/Task.h>
#include <Pxf/Base/Platform.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/String.h>
#include <Pxf/Kernel.h>

#include <thread>
#include <algorithm>
#include <memory>

using namespace Pxf;

TaskPool::TaskPool()
{
	logger_ = Kernel::GetInstance()->GetLogger("prc");
}

TaskPool::~TaskPool()
{
	mutex_.lock();
	for(auto ch: channels_)
	{
		ch->queue_->cancel();
		ch->running_ = false;
		ch->thread_->join();
	}

	for(auto chg: channel_groups_)
	{
		chg->cancel();
	}

	mutex_.unlock();
}

void TaskPool::channel_processor(TaskPool* pool, const std::shared_ptr<TaskQueue>& queue, const std::shared_ptr<Channel>& channel)
{
	try
	{
		while(true)
		{
			auto t = queue->pop();
			channel->processing_ = true;
			t->set_channel(channel);
			t->on_start();
			t->run();
			t->on_complete();
			channel->on_complete();
			channel->processing_ = false;
		}
	}
	catch(TaskQueue::CancellationException& e)
	{
		channel->running_ = false;
	}
}

TaskGroupID TaskPool::create_channel_group()
{
	unsigned id = channel_groups_.size();
	channel_groups_.push_back(std::make_shared<TaskQueue>());
	return id;
}

TaskChannelID TaskPool::create_channel(TaskGroupID gid)
{
	PXF_ASSERT(gid >= 0 || gid < channel_groups_.size(), "invalid group id");
	mutex_.lock();
	int idx = channels_.size();
	auto ch = std::make_shared<Channel>();
	ch->name_ = Format("gid%d-cid%d", gid, idx);
	ch->id_ = idx;
	ch->queue_ = channel_groups_[gid];
	ch->thread_ = std::unique_ptr<std::thread>(new std::thread(TaskPool::channel_processor, this, ch->queue_, ch));
	channels_.push_back(ch);
	mutex_.unlock();
	return idx;
}

void TaskPool::join(TaskGroupID gid)
{
	PXF_ASSERT(gid >= 0 || gid < channel_groups_.size(), "invalid group id");
	for(auto ch: channels_)
	{
		if (ch->queue_ == channel_groups_[gid])
			ch->join();
	}
}

void TaskPool::add(TaskGroupID gid, const std::shared_ptr<Task>& task)
{
	PXF_ASSERT(gid >= 0 || gid < channel_groups_.size(), "invalid group id");
	channel_groups_[gid]->push(task);
}