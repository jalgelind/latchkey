#include <Pxf/Util/Application.h>

#include <Pxf/Base/Base.h>
#include <Pxf/Base/Timer.h>
#include <Pxf/Base/TaskPool.h>
#include <Pxf/Audio/Audio.h>
#include <Pxf/Graphics/Graphics.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Input/Input.h>
#include <Pxf/Resource/Resource.h>
#include <cstdio>

using namespace Pxf;
using namespace Pxf::Graphics;


static Application* g_Application = NULL;

Application::Application(Pxf::Kernel* kernel, const char* loggertag)
	: fps_(0.f)
	, kernel_(*kernel)
	, snd_(*kernel_.GetAudioDevice())
	, gfx_(*kernel_.GetGraphicsDevice())
	, inp_(*kernel_.GetInputDevice())
	, kbd_(kernel_.GetInputDevice()->GetKeyboard())
	, mouse_(kernel_.GetInputDevice()->GetMouse())
	, pad_(kernel_.GetInputDevice()->GetGamepad())
	, res_(*kernel_.GetResourceManager())
	, scene_manager_(0)
	, cfg_(kernel)
	, running_time_(0.f)
	, frame_counter_(0)
	, window_width_(0)
	, window_height_(0)
	, refresh_rate_(30)
	, profiler_(Profiler::GetInstance())
	, interpolmgr_(nullptr)
	, vsync_(true)
{
	g_Application = this;
	logger_ = kernel->GetLogger(loggertag);
	scene_manager_ = SceneManager(this);
	start_.Start();
	interpolmgr_ = Pxf::make_unique<InterpolationManager>(this);

	// Default settings
	cfg_.Set("win.width", 550);
	cfg_.Set("win.height", 300);
	cfg_.Set("win.xpos", -1);
	cfg_.Set("win.ypos", -1);
	cfg_.Set("win.vsync", true);
	cfg_.Set("audio.buffersize", 512);
	cfg_.Set("audio.samplerate", 44100, "only 44100 is tested");
	cfg_.Set("app.firstlaunch", true);
	cfg_.Set("app.script", "data/app/default.lua");
};

Application::~Application()
{
	cfg_.Save("config.json");
	g_Application = NULL;
}

Application* Application::GetInstance()
{
	return g_Application;
}

bool Application::Init()
{
	return OnInit();
}

void Application::AddListner(std::shared_ptr<ApplicationListner> listner)
{
	RemoveListner(listner);
	listners_.push_back(listner);
}

void Application::RemoveListner(std::shared_ptr<ApplicationListner> listner)
{
	listners_.erase(std::remove(begin(listners_), end(listners_), listner), end(listners_));
}

void Application::_DoUpdate(unsigned int framenum)
{
	PXF_PROFILE("app.update");
	running_time_ = start_.Interval() / 1000.f;
	just_switched_scene_ = scene_manager_.Update();
	interpolmgr_->Update();
	DoUpdate(framenum);
	for(auto l: listners_)
		l->OnUpdate(framenum);
}


void Application::_DoRender(float interpolation)
{
	PXF_PROFILE("app.render");
	running_time_ = start_.Interval() / 1000.f;
	DoRender(interpolation);
	for(auto l: listners_)
		l->OnRender(interpolation);
}

void Application::CheckForReloadedResources()
{
	Pxf::Resource::ResourceManager* mgr = kernel_.GetResourceManager();

	std::vector<std::string> luafiles;
	bool reload = false;
	bool lappreload = false;
	bool reinit = false;
	Pxf::Logger log = logger_;

	mgr->CheckForUpdatedResources([&luafiles,&reload, &lappreload, &reinit, &log](Resource::ResourceBase& res)
	{
		const std::string& s = res.GetSource();
		if (IsSuffixI(s, ".lua"))
		{
			reload = true;
			lappreload = true;
			log.Debug("Updated lua source file: %s", s.c_str());
			luafiles.push_back(s);
		}
		else if (IsSuffixI(s, ".png"))
		{
			reload = true;
			log.Debug("Updated texture: %s", s.c_str());
		}
		else if (IsSuffixI(s, ".pfnt"))
		{
			reload = true;
			log.Debug("Updated font: %s", s.c_str());
		}
		else if (IsSuffixI(s, ".glsl"))
		{
			reload = true;
			log.Debug("Updated glsl: %s", s.c_str());
		}
		else if (IsSuffixI(s, "config.json"))
		{
			reload = true;
			reinit = true;
			lappreload = true;
			log.Debug("New configuration found.");
		}

		if (reload)
		{
			res.Unload();
			res.Load();
		}
	});

	if (reinit)
	{
		log.Debug("FIXME: Re-initialization");
		// FIXME: re-initializing should re-read the configuration and
		//        reconfigure the graphics & audio device, and reload all resources.
		//Init();
	}

	if (lappreload)
	{
		for(auto l: listners_)
			l->OnFileRefresh(luafiles);
	}
}

static float GetTimeF()
{
	return Platform::GetTime() / 1000.f;
}

void Application::ProcessWindowEvents()
{
	Window::Event ev;
	while(wnd_->GetQueue().PopTo(ev))
	{
		switch (ev.type)
		{
		case Window::Event::CLOSE:
			for(auto l: listners_)
				l->OnShutdown();
			wnd_->Close();
			is_running_ = false;
			break;
		case Window::Event::FOCUS:
			for(auto l: listners_)
				l->OnFocus(ev.focus);
			OnFocus(ev.focus);
			break;
		case Window::Event::RESIZE:
			for(auto l: listners_)
				l->OnResize(ev.width, ev.height);
			OnResize(ev.width, ev.height);
			break;
		case Window::Event::MOVE:
			cfg_.Set("win.xpos", ev.x);
			cfg_.Set("win.ypos", ev.y);
			for(auto l: listners_)
				l->OnMove(ev.x, ev.y);
			OnMove(ev.x, ev.y);
			break;
		}
	}
}

void Application::RunConstantFPS()
{
if (!wnd_)
	{
		logger_.Error("Unialized window.");
		return;
	}

	const static int SKIP_TICKS = 1000 / refresh_rate_;
	int next_tick = Platform::GetTime();

	Timer fps_timer;
	float frames = 0;

	start_.Start();
	is_running_ = true;
	while(is_running_)
	{
		running_time_ = start_.Interval() / 1000.f;
		_DoUpdate(frame_counter_ % refresh_rate_);
		_DoRender(0);
		wnd_->Swap();
		frame_counter_++;
		next_tick += SKIP_TICKS;
		int sleep = (next_tick - Platform::GetTime());
		if (sleep > 0)
			Platform::ThreadSleep(sleep);

		float interval = fps_timer.Interval() / 1000.f;
		if (interval > 1.0)
		{
			fps_ = frames / interval;
			fps_timer.Start();
			frames = 0;
		}

		ProcessWindowEvents();
		frames += 1;
		is_running_ = is_running_ && wnd_->IsOpen();
	}
	return;
}

void Application::RunVariableFPS()
{
	if (!wnd_)
	{
		logger_.Error("Uninitialized window.");
		return;
	}

	unsigned int TICKS_PER_SECOND = refresh_rate_;
	const static float SKIP_TICKS = 1./TICKS_PER_SECOND;
	const static float MAX_FRAMESKIP = 5;

	int loops;
	float next_game_tick = GetTimeF();
	float interpolation = 0.f;
	float frames = 0;
	float updates = 0;
	Timer fps_timer;
	Timer updates_timer;

	start_.Start();

	is_running_ = true;
	while(is_running_)
	{
		loops = 0;
		while(GetTimeF() > next_game_tick && loops < MAX_FRAMESKIP)
		{
			_DoUpdate(frame_counter_ % refresh_rate_);
			next_game_tick += SKIP_TICKS;
			++loops;
			++frame_counter_;
			++updates;
		}

		interpolation = (GetTimeF() + SKIP_TICKS - next_game_tick) / SKIP_TICKS;

		_DoRender(interpolation);
		wnd_->Swap();

		float interval = fps_timer.Interval() / 1000.f;
		if (interval > 1.0)
		{
			fps_ = frames / interval;
			//std::string t = Format("%f", fps_);
			//wnd_->SetTitle(t.c_str());
			fps_timer.Start();
			frames = 0;
		}

		interval = updates_timer.Interval() / 1000.f;
		if (interval > 1.0)
		{
			float upd = updates / interval;
			//std::string t = Format("%f", upd);
			//wnd_->SetTitle(t.c_str());
			updates_timer.Start();
			updates = 0;
		}
		ProcessWindowEvents();
		++frames;
		is_running_ = is_running_ && wnd_->IsOpen();
	}
}

void Application::Run()
{
	RunVariableFPS();
}

