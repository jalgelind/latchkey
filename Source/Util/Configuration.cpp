#include <Pxf/Util/Configuration.h>
#include <Pxf/Resource/ResourceManager.h>

#include <string>
#include <vector>
#include <sstream>

using namespace Pxf;

void Configuration::_Merge(Json::Value& dst, Json::Value& src)
{
	std::vector<std::string> members = src.getMemberNames();
	std::vector<std::string>::iterator srciter = members.begin();
	for(; srciter != members.end(); ++srciter)
	{
		std::string key = *srciter;
		Json::Value& srcvalue = src[key];
		Json::Value& dstvalue = dst[key];
		if (srcvalue.isObject() && dstvalue.isObject())
			_Merge(dstvalue, srcvalue);
		else
		{
			if (m_StrictMerge && srcvalue.type() != dstvalue.type())
			{
				if (dstvalue.type() != Json::nullValue)
					m_Logger.Error("Error merging settings for key = '%s'. Incompatible types.", key.c_str());
				continue;
			}
			
			if (dstvalue.hasComment(Json::commentAfterOnSameLine))
				srcvalue.setComment(dstvalue.getComment(Json::commentAfterOnSameLine), Json::commentAfterOnSameLine);
			dstvalue.setComment(srcvalue.getComment(Json::commentAfterOnSameLine).c_str(), Json::commentAfterOnSameLine);
			dstvalue = srcvalue;
		}
	}
}

bool Configuration::Load(const std::string& _path, bool _replace_existing /* overwrite shared keys with ones from the configuration file */)
{
	m_Path = _path;
	m_SettingsDocument = m_Kernel->GetResourceManager()->Acquire<Resource::Json>(_path.c_str());
	if (m_SettingsDocument)
	{
		Json::Value& settings = m_SettingsDocument->GetRoot();
		if (!_replace_existing)
			m_Settings = settings;
		else
		{
			Json::Value& lsettings = settings;
			_Merge(m_Settings, lsettings);
		}
		return true;
	}
	return false;
}

bool Configuration::Save(const std::string& _path)
{
	auto rm = m_Kernel->GetResourceManager();
	if (!m_SettingsDocument)
	{
		m_SettingsDocument = rm->CreateJsonDocument();
	}
	m_SettingsDocument->SetRoot(m_Settings);
	if(m_SettingsDocument->SaveToDisk(_path.c_str()))
		return true;
	return false;
}

Json::Value& Configuration::GetRoot()
{
	return m_Settings;
}

Json::Value& Configuration::Get(const std::string& k)
{
	return Get(m_Settings, k);
}

Json::Value& Configuration::Get(Json::Value& config, const std::string& k)
{
	std::vector<std::string> components = split(k, '.');
	std::vector<std::string>::iterator iter = components.begin();
	Json::Value* v = &config;
	for(; iter != components.end(); ++iter)
		v = &(*v)[*iter];

	// compare types of k with the one in fallback.
	if(m_Typechecking && &config == &m_Settings)
	{
		Json::Value& dv = Get(m_FallbackSettings, k);
		if (dv.type() != v->type() && !dv.isNull())
		{
			m_Logger.Error("Type mismatch on key '%s'. Using fallback value. Check your settings.", k.c_str());
			return dv;
		}
	}

	return *v;
}

void Configuration::Set(const std::string& k, Json::Value v, const std::string& description)
{
	Json::Value& key = Get(m_Settings, k);
	key = v;
	if (description != "")
	{
		std::string comment = std::string("// ") + description;
		key.setComment(comment, Json::commentAfterOnSameLine);
	}
}

void Configuration::SetString(const std::string& k, const std::string& v)
{
	Set(k, v);
}

void Configuration::SetFloat(const std::string& k, float v)
{
	Set(k, v);
}

void Configuration::SetInt(const std::string& k, int v)
{
	Set(k, v);
}

void Configuration::SetBool(const std::string& k, bool v)
{
	Set(k, v);
}

void Configuration::MarkAsFallback()
{
	m_FallbackSettings = m_Settings;
}


const std::string Configuration::GetString(const std::string& k, const std::string& _fallback)
{
	Json::Value& v = Get(m_Settings, k);
	std::string ret = _fallback;
	if (v.isString())
	{
		ret = v.asString();
	}
	else if (!v.isNull())
		m_Logger.Error("Configuration error. Key '%s' should be a string.", k.c_str());
	return ret;
}

float Configuration::GetFloat(const std::string& k, float _fallback)
{
	Json::Value& v = Get(m_Settings, k);
	if (v.isDouble())
		return (float)v.asDouble();
	if (v.isInt())
		return (float)v.asInt();
	if (!v.isNull())
		m_Logger.Error("Configuration error. Key '%s' should be a floating point number.", k.c_str());
	return _fallback;
}

int Configuration::GetInt(const std::string& k, int _fallback)
{
	Json::Value& v = Get(m_Settings, k);
	if (v.isInt())
		return v.asInt();
	if (!v.isNull())
		m_Logger.Error("Configuration error. Key '%s' should be an integer.", k.c_str());
	return _fallback;
}

bool Configuration::GetBool(const std::string& k, bool _fallback)
{
	Json::Value& v = Get(m_Settings, k);
	if (v.isBool())
		return v.asBool();
	if (!v.isNull())
		m_Logger.Error("Configuration error. Key '%s' should be a boolean.", k.c_str());
	return _fallback;
}

bool Configuration::IsString(const std::string& k)
{
	Json::Value& v = Get(m_Settings, k);
	return v.isString();
}

bool Configuration::IsFloat(const std::string& k)
{
	Json::Value& v = Get(m_Settings, k);
	return v.isDouble();
}

bool Configuration::IsInt(const std::string& k)
{
	Json::Value& v = Get(m_Settings, k);
	return v.isInt();
}

bool Configuration::IsBool(const std::string& k)
{
	Json::Value& v = Get(m_Settings, k);
	return v.isBool();
}
