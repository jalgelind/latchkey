#include <Pxf/Util/Application.h>
#include <Pxf/Util/InterpolationManager.h>
#include <Pxf/Util/Easing.h>
#include <Pxf/Util/Profiler.h>

using namespace Pxf;

InterpolationManager::InterpolationManager(Application* app)
	: app_(app)
{

}

InterpolationManager::~InterpolationManager()
{
}

void InterpolationManager::Register(Interpolation var, float* _Var, float _Start, float _Stop, float _From, float _To)
{
	var.ptr = _Var;
	var.start = app_->running_time_ + _Start;
	float from = _From;
	float to = _To;
	if (_From > _To)
	{
		from = _To;
		to = _From;
		var.inv = true;
	}
	*(var.ptr) = from;
	var.interpolator->from = from;
	var.interpolator->offset = to - from;
	var.interpolator->duration = _Stop - _Start;
	m_Array.push_back(std::move(var));
}

void InterpolationManager::UpdateVar(float* _Var, float _NewValue, float _Start, float _Stop, float _From, float _To)
{
	for(unsigned i = 0; i < m_Array.size(); i++)
	{
		const Interpolation& var = m_Array[i];
		if (var.ptr == _Var)
		{
			*(var.ptr) = _NewValue;
			var.interpolator->duration = _Stop - _Start;
			var.interpolator->from = _From;
			var.interpolator->offset = _To - _From;
			break;
		}
	}
}

void InterpolationManager::Update()
{
	PXF_PROFILE("app.interpolate");
	std::vector<Interpolation>::iterator iter = m_Array.begin();
	for(iter; iter != m_Array.end(); ++iter)
	{
		Interpolation& var = *iter;

		float t = app_->running_time_ - var.start;

		if (t < 0.f || t > var.interpolator->duration)
			continue;

		float to = var.interpolator->offset;
		float from = var.interpolator->from;

		float& val = *(var.ptr);

		switch(var.type)
		{
		case FADE_IN:
			val = var.interpolator->EaseIn(t);
			break;
		case FADE_OUT:
			val = var.interpolator->EaseOut(t);
			break;
		case FADE_INOUT:
			val = var.interpolator->EaseInOut(t);
			break;
		}

		if (var.inv)
		{
			val = to - val;
		}
	}
}