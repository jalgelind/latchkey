#include <Pxf/Util/DistanceField.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/ResourceManager.h>

using namespace Pxf;
using namespace Resource;

DistanceField::Point DistanceField::inside = DistanceField::Point(0, 0);
DistanceField::Point DistanceField::empty = DistanceField::Point(9999, 9999);

void DistanceField::GenerateSDF(Grid& grid)
{
    for (int y=0;y<grid.height;y++)
    {
        for (int x=0;x<grid.width;x++)
        {
            Point p = grid.Get(x, y );
            grid.Compare(p, x, y, -1,  0 );
            grid.Compare(p, x, y,  0, -1 );
            grid.Compare(p, x, y, -1, -1 );
            grid.Compare(p, x, y,  1, -1 );
            grid.Put(x, y, p );
        }

        for (int x=grid.width-1;x>=0;x--)
        {
            Point p = grid.Get(x, y );
            grid.Compare(p, x, y, 1, 0 );
            grid.Put(x, y, p );
        }
    }

    for (int y=grid.height-1;y>=0;y--)
    {
        for (int x=grid.width-1;x>=0;x--)
        {
            Point p = grid.Get(x, y );
            grid.Compare(p, x, y,  1,  0 );
            grid.Compare(p, x, y,  0,  1 );
            grid.Compare(p, x, y, -1,  1 );
            grid.Compare(p, x, y,  1,  1 );
            grid.Put(x, y, p );
        }

        for (int x=0;x<grid.width;x++)
        {
            Point p = grid.Get(x, y );
            grid.Compare(p, x, y, -1, 0 );
            grid.Put(x, y, p );
        }
    }
}

bool DistanceField::Build(const std::shared_ptr<Image>& img)
{
	int height = img->Height();
	int width = img->Width();
	int channels = img->Channels();
	unsigned char* data = (unsigned char*)img->GetChunk()->data;

	Grid grid1(width, height);
	Grid grid2(width, height);

	// Initialize grid
	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			int offset = (y*width + x) * channels;
			int v = 0;
			switch(channels)
			{
			case 1:
				v = data[offset];
				break;
			default:
				break;
				//TODO: support multi-channel images
			}
			
			if ( v < 128 )
			{
				grid1.Put(x, y, inside);
				grid2.Put(x, y, empty);
			}
			else
			{
				grid2.Put(x, y, inside);
				grid1.Put(x, y, empty);
			}
		}
	}

	GenerateSDF(grid1);
	GenerateSDF(grid2);

	unsigned int res_c = 1;
	unsigned char* result = new unsigned char[width * height * res_c];
	
	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			// Calculate the actual distance from the dx/dy
			int dist1 = (int)(sqrt((double)grid1.Get(x,y).dist_sqr()));
			int dist2 = (int)(sqrt((double)grid2.Get(x,y).dist_sqr()));
			int dist = dist1 - dist2;

			// Clamp and scale it, just for display purposes.
			int c = dist*3 + 128;
			if (c < 0) c = 0;
			if (c > 255) c = 255;

			int offset = (y*width + x) * res_c;
			for(int r = 0; r < res_c; r++)
			{
				result[offset+r] = c;
			}
		}
	}
	
	auto rm = Kernel::GetInstance()->GetResourceManager();
	m_Result = rm->CreateImageFromRaw(width, height, res_c, result, "generated-sdf");
	return true;
}

std::shared_ptr<Image> DistanceField::GetImage()
{
	return m_Result;
}