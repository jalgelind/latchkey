#include <Pxf/Util/Color.h>

using namespace Pxf;

void Color::update(float r, float g, float b, float a)
{
	this->r = TransformRange(r, 0.f, 1.f, range_min, range_max);
	this->g = TransformRange(g, 0.f, 1.f, range_min, range_max);
	this->b = TransformRange(b, 0.f, 1.f, range_min, range_max);
	this->a = TransformRange(a, 0.f, 1.f, range_min, range_max);
}

void Color::set_range(float min, float max)
{
	float old_min = range_min;
	float old_max = range_max;
	range_min = min;
	range_max = max;
	r = TransformRange(r, old_min, old_max, range_min, range_max);
	g = TransformRange(g, old_min, old_max, range_min, range_max);
	b = TransformRange(b, old_min, old_max, range_min, range_max);
	a = TransformRange(a, old_min, old_max, range_min, range_max);
}

void Color::set_float(float r, float g, float b, float a)
{
	update(r, g, b, a);
}

void Color::set_int8(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	update(r/255.f, g/255.f, b/255.f, a/255.f);
}

bool Color::set_hex(const std::string& hex_in)
{
	std::string hex = hex_in;
	if (hex.size() == 0)
		return false;
	if (hex[0] == '#')
		hex = hex.substr(1);
	/* TODO: validate hex */
	unsigned char hr = 0, hg = 0, hb = 0;
	switch(hex.size())
	{
	case 3:
		{
		hr = strtol(hex.substr(0, 1).c_str(), NULL, 16);
		hg = strtol(hex.substr(1, 1).c_str(), NULL, 16);
		hb = strtol(hex.substr(2, 1).c_str(), NULL, 16);
		break;
		}
	case 6:
		{
			hr = strtol(hex.substr(0, 2).c_str(), NULL, 16);
			hg = strtol(hex.substr(2, 2).c_str(), NULL, 16);
			hb = strtol(hex.substr(4, 2).c_str(), NULL, 16);
			break;
		}
	default:
		/* error */
		return false;
	}
	set_int8(hr, hg, hb);
	return true;
}

void Color::set_hsv(float h, float s, float v)
{
	hsv_t hsv;
	hsv.h = h; hsv.s = s; hsv.v = v;
	rgb_t rgb = hsv_to_rgb(hsv);
	update(rgb.r, rgb.g, rgb.b);
}

void Color::set_hsl(float h, float s, float l)
{
	Color::hsl_t hsl;
	hsl.h = h; hsl.s = s; hsl.l = l;
	rgb_t rgb = hsl_to_rgb(hsl);
	update(rgb.r, rgb.g, rgb.b);
}

Color::rgb_t Color::hsl_to_rgb(hsl_t in)
{
	rgb_t out;
	float t[3] = { 0.f };

	float tH = in.h;
	float tS = in.s;
	float tL = in.l;

	if (tS == 0.f)
	{
		t[0] = t[1] = t[2] = tL;
	}
	else
	{
		float q, p;

		q = tL < 0.5 ? tL * (1 + tS) : tL + tS - (tL * tS);
		p = 2 * tL - q;

		t[0] = tH + (1.0 / 3.0);
		t[1] = tH;
		t[2] = tH - (1.0 / 3.0);

		for (int i = 0; i < 3; i++)
		{
			t[i] = t[i] < 0 ? t[i] + 1.0 : t[i] > 1 ? t[i] - 1.0 : t[i];

			if (t[i] * 6.0 < 1.0)
				t[i] = p + ((q - p) * 6 * t[i]);
			else
				if (t[i] * 2.0 < 1.0)
					t[i] = q;
				else
					if (t[i] * 3.0 < 2.0)
						t[i] = p + ((q - p) * 6 * ((2.0 / 3.0) - t[i]));
					else
						t[i] = p;
		}
	}
	out.r = t[0];
	out.g = t[1];
	out.b = t[2];
	return out;
}

//
// http://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb
//
Color::rgb_t Color::hsv_to_rgb(hsv_t in)
{
	float hh, p, q, t, ff;
	long i;
	Color::rgb_t out;

	if(in.s <= 0.0) {
		// error - should never happen
		out.r = 0.0;
		out.g = 0.0;
		out.b = 0.0;
		return out;
	}
	hh = in.h;
	if(hh >= 360.0) hh = 0.0;
	hh /= 60.0;
	i = (long)hh;
	ff = hh - i;
	p = in.v * (1.0 - in.s);
	q = in.v * (1.0 - (in.s * ff));
	t = in.v * (1.0 - (in.s * (1.0 - ff)));

	switch(i) {
	case 0:
		out.r = in.v;
		out.g = t;
		out.b = p;
		break;
	case 1:
		out.r = q;
		out.g = in.v;
		out.b = p;
		break;
	case 2:
		out.r = p;
		out.g = in.v;
		out.b = t;
		break;

	case 3:
		out.r = p;
		out.g = q;
		out.b = in.v;
		break;
	case 4:
		out.r = t;
		out.g = p;
		out.b = in.v;
		break;
	case 5:
	default:
		out.r = in.v;
		out.g = p;
		out.b = q;
		break;
	}
	return out;
}

Color::hsv_t Color::rgb_to_hsv(rgb_t in)
{
	Color::hsv_t out;
	float min, max, delta;

	min = in.r < in.g ? in.r : in.g;
	min = min  < in.b ? min  : in.b;

	max = in.r > in.g ? in.r : in.g;
	max = max  > in.b ? max  : in.b;

	out.v = max;                                // v
	delta = max - min;
	if( max > 0.0 ) {
		out.s = (delta / max);                  // s
	} else {
		// r = g = b = 0                        // s = 0, v is undefined
		out.s = 0.0;
		return out;
	}
	if( in.r >= max )                           // > is bogus, just keeps compilor happy
		out.h = ( in.g - in.b ) / delta;        // between yellow & magenta
	else
		if( in.g >= max )
			out.h = 2.0 + ( in.b - in.r ) / delta;  // between cyan & yellow
		else
			out.h = 4.0 + ( in.r - in.g ) / delta;  // between magenta & cyan

	out.h *= 60.0;                              // degrees

	if( out.h < 0.0 )
		out.h += 360.0;

	return out;
}