#include <Pxf/Util/SceneManager.h>
#include <Pxf/Util/Scene.h>
#include <Pxf/Util/Application.h>
#include <Pxf/Util/Profiler.h>

using namespace Pxf;

SceneManager::SceneManager(Application* app)
	: app_(app)
	, locked_(false)
{
	prev_scene_ = 0;
	active_scene_ = 0;
}

SceneManager::~SceneManager()
{
}

void SceneManager::AddScene(Scene* _Scene)
{
	// Setup the first active scene
	if (prev_scene_ == 0)
	{
		active_scene_ = _Scene;
		active_scene_->Init();
		prev_scene_ = _Scene;
	}
	Scenes.push_back(std::shared_ptr<Scene>(_Scene));
}

void SceneManager::SetScene(Scene* _Scene)
{
	prev_scene_ = active_scene_;
	active_scene_ = _Scene;
	active_scene_->Init();
	locked_ = true;
}

void SceneManager::RestoreScene(Scene* _Scene)
{
	active_scene_ = prev_scene_;
	active_scene_->Init();
	locked_ = false;
}

bool SceneManager::Update()
{
	PXF_PROFILE("app.update.scene");
	if (locked_)
	{
		if (prev_scene_ != active_scene_)
		{
			prev_scene_ = active_scene_;
			active_scene_->Init();
			return true;
		}
		return false;
	}

	for(unsigned i = 0; i < Scenes.size(); i++)
	{
		Scene *c = Scenes[i].get();
		float starttime = c->scene_start_;
		float stoptime = c->scene_stop_;
		if (c->locked_ || (app_->running_time_ >= starttime && app_->running_time_ < stoptime))
		{
			bool ret = false;
			active_scene_ = c;
			if (prev_scene_ != active_scene_)
			{
				active_scene_->Init();
				prev_scene_ = active_scene_;
				ret = true;
			}
			return ret;
		}
	}
	return false;
}

Scene* SceneManager::GetPreviousScene()
{
	return prev_scene_;
}

Scene* SceneManager::GetCurrentScene()
{
	return active_scene_;
}

Scene* SceneManager::GetNextScene()
{
	Scene* r = NULL;
	for(unsigned i = 0; i < Scenes.size(); i++)
	{
		Scene *c = Scenes[i].get();
		if (c == active_scene_ && i > Scenes.size()-1)
			r = Scenes[i+1].get();
	}
	return r;
}

Scene* SceneManager::GetSceneAtIndex(int _Index)
{
	return Scenes[_Index].get();
}

