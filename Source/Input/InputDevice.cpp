#include <Pxf/Input/InputDevice.h>
#include <Pxf/Input/NullInputDevice.h>
#include <Pxf/Kernel.h>
#include <Pxf/Base/Utils.h>

#include <algorithm>

using namespace Pxf;
using namespace Pxf::Input;

InputController::InputController(Kernel* kernel, const char* _Identifier)
	: Pxf::System(kernel, Pxf::System::INPUT_CONTROLLER, _Identifier)
	, m_Kernel(kernel)
	, m_InputDevice(kernel->GetInputDevice())
	, m_Keyboard(nullptr)
	, m_Mouse(nullptr)
	, m_HasKeyboard(false)
	, m_HasMouse(false)
	, m_HasGamepad(false)
	, m_HasMidiDevice(false)
{

}

InputDevice::InputDevice(Kernel* _Kernel)
	: m_Kernel(_Kernel)
	, kbd_(0)
	, mouse_(0)
{
	logger_ = _Kernel->GetLogger("inpd");
	RegisterInputController(std::make_shared<NullInputController>(_Kernel));
	DiscoverDevices();
}

InputDevice::~InputDevice()
{
}

void InputDevice::RegisterInputController(const std::shared_ptr<InputController>& ctrl)
{
	m_InputControllers.push_back(ctrl);
}

void InputDevice::DiscoverDevices()
{
	for(int i = 0; i < m_InputControllers.size(); i++)
	{
		auto ctrl = m_InputControllers[i];
		ctrl->Discover();
		if (ctrl->HasKeyboard()
			&& &ctrl->GetKeyboard() != kbd_
			&& (kbd_ == 0 || kbd_->IsNull()))
		{
			kbd_ = &ctrl->GetKeyboard();
		}

		if (ctrl->HasMouse() 
			&& &ctrl->GetMouse() != mouse_
			&& (mouse_ == 0 || mouse_->IsNull()))
		{
			mouse_ = &ctrl->GetMouse();
		}

		if (ctrl->HasGamepad())
		{
			for(int i = 0; i < ctrl->GetGamepadCount(); i++)
			{
				Gamepad* pad = &ctrl->GetGamepad(i);
				if (std::find(gamepads_.begin(), gamepads_.end(), pad) == gamepads_.end())
					gamepads_.push_back(pad);
			}
		}

		if (ctrl->HasMidiDevice())
		{
			for(int i = 0; i < ctrl->GetMidiDeviceCount(); i++)
			{
				MidiDevice* dev = &ctrl->GetMidiDevice(i);
				if (std::find(mididevices_.begin(), mididevices_.end(), &ctrl->GetMidiDevice(i)) == mididevices_.end())
					mididevices_.push_back(dev);
			}
		}
	}
}

void InputDevice::Update()
{
	for(int i = 0;i < m_InputControllers.size(); i++)
		m_InputControllers[i]->Update();
}

Keyboard& InputDevice::GetKeyboard()
{
	if (!kbd_ || kbd_->IsNull())
		DiscoverDevices();
	return *kbd_;
}

Mouse& InputDevice::GetMouse()
{
	if (!mouse_ || mouse_->IsNull())
		DiscoverDevices();
	return *mouse_;
}

Gamepad& InputDevice::GetGamepad(const std::string& name)
{
	for(int i = 0; i < gamepads_.size(); i++)
	{
		Gamepad& dev = *gamepads_[i];
		if (dev.GetDescription() == name)
		{
			if (!dev.IsActivated())
				dev.Activate();
			return dev;
		}
	}
	return *gamepads_[0];
}

Gamepad& InputDevice::GetGamepad(int idx)
{
	idx += 1;
	// Unavailable or invalid -> return null device (idx=0)
	if (gamepads_.size() <= 1 || idx+1 > gamepads_.size())
		idx = 0;
	Gamepad& pad = *gamepads_[idx];
	if (!pad.IsActivated())
		pad.Activate();
	return pad;
}

MidiDevice& InputDevice::GetMidiDevice(const std::string& name)
{
	for(int i = 0; i < mididevices_.size(); i++)
	{
		MidiDevice& dev = *mididevices_[i];
		if (dev.GetDescription() == name)
		{
			if (!dev.IsActivated())
				dev.Activate();
			return dev;
		}
	}
	return *mididevices_[0];
}

MidiDevice& InputDevice::GetMidiDevice(int idx)
{
	idx += 1;
	// Unavailable or invalid -> return null device (idx=0)
	if (mididevices_.size() <= 1 || idx+1 > mididevices_.size())
		idx = 0;
	MidiDevice& dev = *mididevices_[idx];
	if (!dev.IsActivated())
		dev.Activate();
	return dev;
}


int InputDevice::GetDefaultMidiOutputPort(int idx)
{
	MidiDevice& dev = GetMidiDevice(idx);
	return dev.GetInputController().GetIntOption("default_output");
}

int InputDevice::GetDefaultMidiInputPort(int idx)
{
	MidiDevice& dev = GetMidiDevice(idx);
	return dev.GetInputController().GetIntOption("default_input");
}
