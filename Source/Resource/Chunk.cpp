#include <Pxf/Resource/Chunk.h>
#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Types.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/Path.h>

using namespace Pxf;

Resource::Chunk* Resource::LoadFile(const std::string& _FilePath, const char* openmode)
{
	std::string filepath = _FilePath;
	FileStream file;
	
	if (file.Open(filepath.c_str(), openmode))
	{
		uint size = file.GetSize();
		uint last_modified = file.GetLastModified();
		char* data = new char[size + 1];

		if (!data)
		{
			file.Close();
			return NULL;
		}

		Resource::Chunk* chunk = new Resource::Chunk();

		int read = 0;
		if((read = file.Read(data, size)) > 0)
			data[size] = 0; // protection when used for string loading
		chunk->data = data;
		chunk->size = size;
		chunk->source = filepath;
		chunk->last_modified = file.GetLastModified();
		chunk->is_static = false;
		file.Close();
		return chunk;
	}
	return NULL;
}

