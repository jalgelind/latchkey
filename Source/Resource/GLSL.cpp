#include <Pxf/Resource/GLSL.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>

#include <string>


using namespace Pxf;

bool Resource::GLSL::Build()
{
	std::string glsldata = (const char*)m_Chunk->data;
	std::vector<std::string> lines = split(glsldata, '\n');
	
	std::vector<std::string> vertexshader;
	std::vector<std::string> fragmentshader;

	enum State { ECommon, EVertex, EFragment };
	State state = ECommon;

	bool has_vsh = false;
	bool has_fsh = false;

	for(int i = 0; i < lines.size(); i++)
	{
		std::string line = lines.at(i);
		ltrim(line);

		if (IsPrefixI("#vertex", line))
		{
			state = EVertex;
			has_vsh = true;
			continue; 
		}
		else if (IsPrefixI("#fragment", line))
		{
			state = EFragment;
			has_fsh = true;
			continue;
		}

		switch(state)
		{
		case ECommon:
			vertexshader.push_back(line);
			fragmentshader.push_back(line);
			break;
		case EVertex:
			vertexshader.push_back(line);
			break;
		case EFragment:
			fragmentshader.push_back(line);
			break;
		}
	}

	m_VertexShader = "";
	for(int i = 0; i < vertexshader.size(); i++)
		m_VertexShader += vertexshader.at(i) + "\n";

	m_FragmentShader = "";
	for(int i = 0; i < fragmentshader.size(); i++)
		m_FragmentShader += fragmentshader.at(i) + "\n";

	bool valid = has_vsh && has_fsh;

	if (!valid)
		m_Logger.Error("Could not build GLSL shader. Vertex shader is '%s', fragment shader is '%s'."
					, has_vsh ? "invalid" : "valid"
					, has_fsh ? "invalid" : "valid" );

	return valid;
}
Resource::GLSL::~GLSL()
{
}

Resource::GLSLLoader::GLSLLoader(Pxf::Kernel* _Kernel)
	: ResourceLoader(_Kernel, "GLSL loader")
{
	m_Logger = m_Kernel->GetLogger("res");
}

Resource::GLSLLoader::~GLSLLoader()
{
}

Resource::GLSL* Resource::GLSLLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = LoadFile(_FilePath, "rb");
	if (!chunk)
		return NULL;
	return new Resource::GLSL(m_Kernel, chunk, this);
}

Resource::GLSL* Resource::GLSLLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	size_t size = _DataLen;
	char* data = new char[size+1];
	std::memcpy(data, _DataPtr, size);
	data[size] = 0;

	chunk->source = _Path ? _Path : "unknown";
	chunk->data = data;
	chunk->size = size+1;
	chunk->is_static = false;
	return new GLSL(m_Kernel, chunk, this);
}
