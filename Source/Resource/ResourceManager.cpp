#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/TaskPool.h>
#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceDepender.h>
#include <Pxf/Resource/BitmapFont.h>
#include <Pxf/Resource/Raw.h>
#include <Pxf/Resource/Json.h>
#include <Pxf/Resource/GLSL.h>

#include <algorithm>

using namespace Pxf;
using namespace Resource;

ResourceManager::ResourceManager(Kernel* _Kernel)
	: m_Kernel(_Kernel)
	, m_LoadedResources(nullptr)
	, m_ResourceLoaders(nullptr)
	, m_CachedFiles(nullptr)
	, m_TaskPool(0)
{
	m_Logger = m_Kernel->GetLogger("res");
	m_ResourceLoaders = new std::map<std::string, ResourceLoader*>();
	m_LoadedResources = new std::map<std::string, std::shared_ptr<ResourceBase>>();
	m_CachedFiles = new std::map<std::string, DataBlob*>();

	RegisterResourceLoader("txt", new Resource::RawLoader(m_Kernel));
	RegisterResourceLoader("pfnt", new Resource::BitmapFontLoader(m_Kernel));
	RegisterResourceLoader("json", new Resource::JsonLoader(m_Kernel));
	RegisterResourceLoader("glsl", new Resource::GLSLLoader(m_Kernel));
}

ResourceManager::~ResourceManager()
{
	m_Logger.Information("Destroying resource manager");

	
	if (m_ResourceLoaders->size() > 0)
	{
		std::map<std::string, ResourceLoader*>::iterator iter;
		for(iter = m_ResourceLoaders->begin(); iter != m_ResourceLoaders->end(); ++iter)
		{
			if (iter->second)
			{
				iter->second->Destroy(iter->second);
				iter->second = NULL;
			}
		}
	}

	delete m_ResourceLoaders;
	delete m_LoadedResources;
	delete m_CachedFiles;
}

void ResourceManager::Shutdown()
{

#ifdef CONF_DEBUG
	FileStream fs;
	if (!Path::Exist("debug")) Path::Mkdir("debug");
	fs.OpenWriteBinary("debug/_used_resources.log");
	for(auto& ent: m_DiskFileHistory)
	{
		std::string line = Pxf::Format("%s|%s\r\n", ent.path_on_disk.c_str(), ent.path_used.c_str());
		fs.Write(line.c_str(), line.size());
	}
	fs.Close();
#endif

	// clean up loaded resources
	if (m_LoadedResources->size() > 0)
	{
		m_Logger.Information("Cleaning up unreleased resources");
		for(auto iter = m_LoadedResources->begin(); iter != m_LoadedResources->end(); ++iter)
		{
			(iter->second).reset();
			iter->second = NULL;
		}
	}

}

void ResourceManager::Initialize()
{
	m_TaskPool = m_Kernel->GetTaskPool();
	m_FileLoadingChannelGroup = m_TaskPool->create_channel_group();
	m_TaskPool->create_channel(m_FileLoadingChannelGroup);
}

void ResourceManager::RegisterResourceLoader(const char* _Ext, Resource::ResourceLoader* _ResourceLoader)
{
	bool isreg = false;
	for(auto p: *m_ResourceLoaders)
	{
		if (p.second == _ResourceLoader)
		{
			isreg = true;
			break;
		}
	}
	if (!isreg)
		m_ResourceLoaders->insert(std::make_pair(_Ext, _ResourceLoader));
}


void ResourceManager::RegisterCachedFile(const char* _Path, const unsigned char* _DataPtr, unsigned _Size, int64 _LastModified)
{
	DataBlob* datablob = new DataBlob;
	datablob->data = _DataPtr;
	datablob->size = _Size;
	datablob->last_modified = _LastModified;
	m_CachedFiles->insert(std::make_pair(_Path, datablob));
}

bool ResourceManager::HasCachedFile(const std::string& _FilePath)
{
	std::map<std::string, DataBlob*>::iterator cached_iter = m_CachedFiles->find(_FilePath);
	return cached_iter != m_CachedFiles->end();
}

void ResourceManager::ExpandFileCache()
{
	std::map<std::string, DataBlob*>::iterator i = m_CachedFiles->begin();
	m_Logger.Information("Expanding cached files...");
	for(i; i != m_CachedFiles->end(); i++)
	{
		std::string path = Path::GetPath(i->first);
		Path::Mkdir(path);
		FileStream fs;
		fs.OpenWriteBinary(i->first.c_str());
		m_Logger.Information("    writing '%s'...", i->first.c_str());
		fs.Write(i->second->data, i->second->size);
		fs.Close();
	}

}

void ResourceManager::CheckForUpdatedResources(std::function<void(Resource::ResourceBase&)> cb)
{
	auto resit = m_LoadedResources->begin();
	for(resit; resit != m_LoadedResources->end(); ++resit)
	{
		std::string respath = resit->second->GetSource();

		const int max_tries = 10;
		bool exists = false;
		for(int i = 0; i < max_tries; i++)
		{
			if (exists = Path::Exist(respath))
				break;
			Platform::ThreadSleep(50);
		}

		if (exists && resit->second->IsUpdated())
			cb(*(resit->second));
	}
}

void Resource::ResourceManager::DumpResourceLoaders()
{
	m_Logger.Information("Enumerating resource loaders...");
	int i = 0;
	for(std::map<std::string, ResourceLoader*>::iterator it = m_ResourceLoaders->begin(); it != m_ResourceLoaders->end(); ++it)
	{
		m_Logger.Information("| %d. %s -> %s", i++, it->first.c_str(), it->second->GetIdentifier());
	}
}

void Resource::ResourceManager::DumpMemoryUsage()
{
	auto resit = m_LoadedResources->begin();
	m_Logger.Debug("Enumerating resources...");
	for(resit; resit != m_LoadedResources->end(); ++resit)
	{
		auto b = resit->second;
		Chunk* c = b->GetChunk();
		if (c)
		{
			if (c->is_static)
				m_Logger.Debug("%s -- ** static resource **", b->GetSource());
			else
				m_Logger.Debug("%s -- %d bytes", b->GetSource(), c->size, b->GetMemoryUsage());
		}
		else
			m_Logger.Debug("%s -- %d bytes -- ** source file not loaded **", b->GetSource(), b->GetMemoryUsage());
	}
}

unsigned Resource::ResourceManager::GetMemoryUsage()
{
	auto resit = m_LoadedResources->begin();
	unsigned tot = 0;
	for(resit; resit != m_LoadedResources->end(); ++resit)
	{
		tot += resit->second->GetMemoryUsage();
	}
	return tot;
}

std::shared_ptr<ResourceBase> Resource::ResourceManager::GetStaticResource(ResourceLoader* loader, const char* _FilePath)
{
	ResourceBase* resource = NULL;
	auto cached_iter = m_CachedFiles->find(_FilePath);
	if (cached_iter != m_CachedFiles->end())
	{
		// Create from cached data
		DataBlob* datablob = cached_iter->second;
		resource = loader->CreateFrom(datablob->data, datablob->size, _FilePath);
		resource->m_Chunk->is_static = true;
		resource->m_Chunk->source = _FilePath;
		resource->m_Source = _FilePath;
		std::string info = resource->ToString();
		m_Logger.Information("Loading cached version of '%s'", _FilePath);
		if (info != "")
			m_Logger.Information("  * %s", info.c_str());
	}
	else
	{
		m_Logger.Error("Failed to load resource '%s'", _FilePath);
		return NULL;
	}
	auto ret = std::shared_ptr<ResourceBase>(resource, [loader](ResourceBase* r) { /* do not touch static resource */ });
	return ret;
}

std::shared_ptr<ResourceBase> Resource::ResourceManager::GetLoadedResource(ResourceLoader* loader, const char* _FilePath)
{
	auto resit = m_LoadedResources->find(_FilePath);
	if (resit != m_LoadedResources->end())
		return resit->second;
	return nullptr;
}

std::shared_ptr<ResourceBase> Resource::ResourceManager::GetFileResource(ResourceLoader* loader, const char* _FilePath)
{
	ResourceBase* resource = loader->Load(_FilePath);
	if (!resource)
		return nullptr;
	std::string info = resource->ToString();
	m_Logger.Information("Loading '%s' from disk", _FilePath);
	if (info != "")
		m_Logger.Information("  * %s", info.c_str());
	auto* log = &m_Logger;
	auto fdel = [loader,log](ResourceBase* i){ if (!i) return; log->Debug("Destroying resource '%s' (0x%X)", i->GetSource(), i); loader->Destroy(i); };
	auto ret = std::shared_ptr<ResourceBase>(resource, fdel);
	return ret;
}

// Factory methods

std::shared_ptr<Image> ResourceManager::CreateImageFromFile(const char* path)
{
	auto ldr = FindResourceLoader<ImageLoader>("png");
	auto* log = &m_Logger;
	auto fdel = [ldr,log](Image* i){ log->Debug("Destroying image 0x%X", i); ldr->Destroy(i); };
	return std::shared_ptr<Image>(ldr->Load(path), fdel);
}

std::shared_ptr<Image> ResourceManager::CreateImageFromData(const void* data, unsigned len, const char* fakepath)
{
	auto ldr = FindResourceLoader<ImageLoader>("png");
	auto* log = &m_Logger;
	auto fdel = [ldr,log](Image* i){ log->Debug("Destroying image 0x%X", i); ldr->Destroy(i);};
	return std::shared_ptr<Image>(ldr->CreateFrom(data, len, fakepath), fdel);
}

std::shared_ptr<Image> ResourceManager::CreateImageFromRaw(int _Width, int _Height, int _Channels, unsigned char* _DataPtr, const char* _Path)
{
	auto ldr = FindResourceLoader<ImageLoader>("png");
	auto* log = &m_Logger;
	auto fdel = [ldr,log](Image* i){ log->Debug("Destroying image 0x%X", i); ldr->Destroy(i);};
	return std::shared_ptr<Image>(ldr->CreateFromRaw(_Width, _Height, _Channels, _DataPtr, _Path), fdel);
}

std::shared_ptr<GLSL> ResourceManager::CreateShaderFromData(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	auto ldr = FindResourceLoader<GLSLLoader>("glsl");
	auto* log = &m_Logger;
	auto fdel = [ldr,log](GLSL* i){ log->Debug("Destroying glsl shader 0x%X", i); ldr->Destroy(i); };
	return std::shared_ptr<GLSL>(ldr->CreateFrom(_DataPtr, _DataLen, _Path), fdel);
}

std::shared_ptr<Resource::Json> ResourceManager::CreateJsonDocument()
{
	auto ldr = FindResourceLoader<JsonLoader>("json");auto* log = &m_Logger;
	auto fdel = [ldr,log](Json* i){ log->Debug("Destroying json document 0x%X", i); ldr->Destroy(i); };
	return std::shared_ptr<Resource::Json>(ldr->CreateEmpty(), fdel);
}
