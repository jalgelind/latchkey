#include <Pxf/Resource/Raw.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>

using namespace Pxf;

bool Resource::Raw::Build()
{
	return true;
}
Resource::Raw::~Raw()
{
}

Resource::RawLoader::RawLoader(Pxf::Kernel* _Kernel)
	: ResourceLoader(_Kernel, "Raw loader")
{
	m_Logger = m_Kernel->GetLogger("res");
}

Resource::RawLoader::~RawLoader()
{
}

Resource::Raw* Resource::RawLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = Resource::LoadFile(_FilePath, "rb");
	if (!chunk)
		return NULL;
	return new Resource::Raw(m_Kernel, chunk, this);
}

Resource::Raw* Resource::RawLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	size_t size = _DataLen;
	char* data = new char[size+1];
	std::memcpy(data, _DataPtr, size);
	data[size] = 0;

	chunk->source = _Path ? _Path : "unknown";
	chunk->data = data;
	chunk->size = size;
	chunk->is_static = false;
	return new Raw(m_Kernel, chunk, this);
}

