#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Resource/Chunk.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/BitmapFont.h>

using namespace Pxf;
using namespace Resource;

BitmapFont::BitmapFont(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
	: Resource::Font(_Kernel, _Chunk, _Loader)
	, m_Image(0)
{
	m_Logger = Logger(_Kernel, "fnt");
}

BitmapFont::~BitmapFont()
{
	Cleanup();
}

void BitmapFont::Cleanup()
{
	SafeDelete(m_Chunk);
}

void BitmapFont::Reload()
{
	Cleanup();
	Build();
}

bool BitmapFont::Build()
{
	m_FontInfo.name = Pxf::Path::GetFileName(m_Chunk->source);

	// Set constant values
	const uint32 HEADER		   = ByteswapNtoL(0xBADC0FFE);
	const uint32 HEADERNEW     = ByteswapNtoL(0xBADC0FFA);
	const uint32 SECTION_INFO	 = ByteswapNtoL(0x53C70001);
	const uint32 SECTION_COMMON   = ByteswapNtoL(0x53C70002);
	const uint32 SECTION_CHARS	= ByteswapNtoL(0x53C70003);
	const uint32 SECTION_KERNINGS = ByteswapNtoL(0x53C70004);
	const uint32 SECTION_IMAGE	= ByteswapNtoL(0x53C70005);

	m_Logger.Information("Creating font from file '%s'", GetSource() ? GetSource() : "unknown");

	// Check chunk
	if (m_Chunk->size < 512)
	{
		m_Logger.Information("Chunk is to small, invalid or corrupted file?");
		return false;
	}

	// Setup memory stream
	MemoryStream data((char*)m_Chunk->data, m_Chunk->size);

	// Check header
	uint32 header = data.ReadLE<uint32>();
	if (header != HEADER && header != HEADERNEW)
	{
		m_Logger.Information("invalid header (%x != %x, %x)", header, HEADER, HEADERNEW);
		return false;
	}

	// Info header
	if (data.ReadLE<uint32>() != SECTION_INFO)
	{
		m_Logger.Information("expected info section");
		return false;
	}

	// Font size
	m_FontInfo.size_restriction = true;
	if (header == HEADERNEW)
	{
		m_FontInfo.size_px = (float)data.ReadLE<uint32>();
	}

	// Read font name (throw away, we'll use the file name)
	uint32 fontname_length = data.ReadLE<uint32>();
	data.Skip(fontname_length);

	// Common header
	if (data.ReadLE<uint32>() != SECTION_COMMON)
	{
		m_Logger.Information("expected common section");
		return false;
	}

	short width = data.ReadLE<int16>(); // image width, height.
	short height = data.ReadLE<int16>();
	m_FontInfo.line_height = data.ReadLE<int16>();

	// Chars header
	if (data.ReadLE<uint32>() != SECTION_CHARS)
	{
		m_Logger.Information("expected character section");
		return false;
	}

	
	int16 num_chars = data.ReadLE<int16>();
	m_CharInfo.clear();
	m_CharInfo.resize(256);
	std::memset(&m_CharInfo[0], 0, sizeof(m_CharInfo[0]) * 256);
	for(int i = 0; i < num_chars; i++)
	{
		int16 id = data.ReadLE<int16>();
		m_CharInfo[id].Width	= float(data.ReadLE<int16>());
		m_CharInfo[id].Height   = float(data.ReadLE<int16>());
		m_CharInfo[id].XOffset  = float(data.ReadLE<int16>());
		m_CharInfo[id].YOffset  = float(data.ReadLE<int16>());
		m_CharInfo[id].XAdvance = float(data.ReadLE<int16>());
		m_CharInfo[id].Tx1	  = data.ReadLE<float>();
		m_CharInfo[id].Ty1	  = data.ReadLE<float>();
		m_CharInfo[id].Tx2	  = data.ReadLE<float>();
		m_CharInfo[id].Ty2	  = data.ReadLE<float>();
	}

	if (data.ReadLE<uint32>() != SECTION_KERNINGS)
	{
		m_Logger.Information("missing kerning section");
		return false;
	}

	int num_kernings = data.ReadLE<int16>();
	m_KerningPairs.clear();
	if (num_kernings > 0)
	{
		m_KerningPairs.resize(num_kernings);
		std::memset(&m_KerningPairs[0], 0, sizeof(m_KerningPairs[0]) * m_KerningPairs.size());
		for(int i = 0; i < num_kernings; i++)
		{
			m_KerningPairs[i].first  = (char)data.ReadLE<int16>();
			m_KerningPairs[i].second = (char)data.ReadLE<int16>();
			m_KerningPairs[i].offset = data.ReadLE<int16>();
		}
	}


	if (data.ReadLE<uint32>() != SECTION_IMAGE)
	{
		m_Logger.Information("missing image section");
		return false;
	}

	int image_size = data.ReadLE<uint32>(); // not used for now though..

	if (image_size > data.GetRemainingBytes())
	{
		m_Logger.Information("not enough data left to contain font image, corrupt file?");
		return false;
	}

	// Create chunk and read image
	Resource::Chunk* chunk = new Chunk();

	if (!chunk)
	{
		m_Logger.Information("could not allocate chunk, out of memory");
		return false;
	}

	chunk->data = data.GetDataPointer();
	chunk->size = data.GetRemainingBytes();
	chunk->is_static = true;

	Resource::ResourceManager* mgr = m_Kernel->GetResourceManager();
	m_Image = mgr->CreateImageFromData(chunk->data, chunk->size);

	if (!m_Image || !m_Image->IsReady())
	{
		m_Logger.Information("invalid image");
		SafeDelete(chunk);
		return false;
	}
	
	m_Initialized = true;
	return true;
}

std::shared_ptr<Image> BitmapFont::CreateImage(int _FontSize)
{
	if (!m_Initialized)
		Build();
	return m_Image;
}

int64 BitmapFont::GetMemoryUsage()
{
	int64 sum = 0;
	if (m_Chunk && !m_Chunk->is_static)
		sum += m_Chunk->size;
	if (m_Image)
		sum += m_Image->GetChunk()->size;
	return sizeof(BitmapFont) + sizeof(Chunk) + sum;
}

BitmapFontLoader::BitmapFontLoader(Pxf::Kernel* _Kernel)
	: FontLoader(_Kernel, "Bitmap Font Loader")
{
	m_Logger = Logger(_Kernel, "fnt");
	Init();
}

BitmapFontLoader::~BitmapFontLoader()
{
}

bool BitmapFontLoader::Init()
{
	return true;
}

Resource::Font* BitmapFontLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = LoadFile(_FilePath);
	if (!chunk)
		return NULL;
	return new BitmapFont(m_Kernel, chunk, this);
}

Resource::Font* BitmapFontLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	chunk->source = _Path ? _Path : "unknown";
	chunk->data = (void*) _DataPtr;
	chunk->size = _DataLen;
	chunk->is_static = true;
	return new BitmapFont(m_Kernel, chunk, this);
}

