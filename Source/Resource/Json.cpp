#include <Pxf/Kernel.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Stream.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Resource/Json.h>

#include <string>
#include <json/writer.h>

using namespace Pxf;

Resource::Json::Json(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
	: ResourceBase(_Kernel, _Chunk, _Loader)
{
	m_Logger = Logger(_Kernel, "res");
	Build();
}

Resource::Json::~Json()
{
}

bool Resource::Json::Build()
{
	// empty document
	if (!m_Chunk)
		return true;

	bool success = m_Reader.parse((char*)m_Chunk->data, m_Root);

	if (!success)
	{
		m_Logger.Error("Failed to parse json file.");
		m_Logger.Error("%s", m_Reader.getFormatedErrorMessages().c_str());
	}

	return success;
}

bool Resource::Json::SaveToDisk(const char* _FilePath)
{
	::Json::StyledWriter writer;
	std::string json_text = writer.write(m_Root).c_str();
	FileStream fs;
	if (!fs.OpenWriteBinary(_FilePath))
		return false;
	fs.Write(json_text.c_str(), json_text.size());
	fs.Close();
	return true;
}

/*
	JsonCppLoader
*/

Resource::JsonLoader::JsonLoader(Pxf::Kernel* _Kernel)
	: Resource::ResourceLoader(_Kernel, "Json Loader")
{
	m_Logger = Logger(_Kernel, "res");
	Init();
}



bool Resource::JsonLoader::Init()
{
	return true;
}

Resource::Json* Resource::JsonLoader::Load(const char* _FilePath)
{
	Resource::Chunk* chunk = LoadFile(_FilePath);
	if (!chunk)
		return NULL;
	return new Json(m_Kernel, chunk, this);
}

Resource::Json* Resource::JsonLoader::CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path)
{
	Resource::Chunk* chunk = new Resource::Chunk();
	chunk->source = _Path ? _Path : "unknown";
	chunk->data = (void*) _DataPtr;
	chunk->size = _DataLen;
	chunk->is_static = true;
	return new Json(m_Kernel, chunk, this);
}

Resource::Json* Resource::JsonLoader::CreateEmpty()
{
	return new Json(m_Kernel, 0, this);
}

Resource::JsonLoader::~JsonLoader()
{
}

