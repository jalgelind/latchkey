#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Base/Path.h>

using namespace Pxf;
using namespace Pxf::Resource;

void ResourceBase::Load()
{
	if (m_Chunk)
		Unload();

	// When a file change is detected, it would possibly
	// take a while for the changes to be reflected in the actual file
	// (Case and point: NFS)
	const int max_tries = 10;
	for(int i = 0; i < max_tries; i++)
	{
		m_Chunk = LoadFile(GetSource());
		if (m_Chunk)
			break;
		Platform::ThreadSleep(50);
	}
	if (m_Chunk)
	{
		m_LastModified = m_Chunk->last_modified;
		m_Source = m_Chunk->source;
		Build();
		ReloadDependers();
	}
	else
	{
		m_Logger.Warning("Failed to reload resource '%s'.", GetSource());	
	}
}

void ResourceBase::Unload()
{
	if (m_Chunk && !m_Chunk->is_static)
	{
		delete m_Chunk;
		m_Chunk = 0;
	}
}

bool ResourceBase::IsUpdated()
{
	std::string respath = GetSource();
	int64 modtime = Path::GetModifiedTime(respath);
	int64 oldtime = GetLastModified();
	if (modtime > 0 && modtime != oldtime)
		return true;
	return false;
}

void ResourceBase::ReloadDependers()
{
	for(auto& d: m_Dependers)
	{
		d->Reload();
	}
}

void ResourceBase::AddDependency(const std::shared_ptr<ResourceDepender>& _dep)
{
	auto& lst = m_Dependers;
	if (std::find(lst.begin(), lst.end(), _dep) == lst.end())
		m_Dependers.push_back(_dep);
}

void ResourceBase::RemoveDependency(const std::shared_ptr<ResourceDepender>& _dep)
{
	auto& lst = m_Dependers;
	lst.erase(std::remove(lst.begin(), lst.end(), _dep), lst.end());
}