#include <Pxf/Base/String.h>
#include <Pxf/Resource/Image.h>

using namespace Pxf::Resource;

int64 Image::GetMemoryUsage()
{
	int64 sum = 0;
	if (m_Chunk && !m_Chunk->is_static)
		sum += m_Chunk->size;
	if (m_ImageData)
		sum += m_Height * m_Width * m_Channels;
	return sizeof(Image) + sizeof(Chunk) + sum;
}

std::string Image::ToString()
{
	return Pxf::Format("format: %dx%d@%d, obj: %d bytes", 
		m_Width, m_Height, m_Channels, GetMemoryUsage());
}
