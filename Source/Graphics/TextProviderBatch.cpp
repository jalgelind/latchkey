#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/PrimitiveBuffer.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Util/Profiler.h>

#include <cstring>
#include <vector>

using namespace Pxf;
using namespace Graphics;


TextProvider::Batch::Batch(Graphics::GraphicsDevice* _pDevice, Graphics::TextProvider* _TextProvider)
	: m_pDevice(_pDevice)
	, m_TextProvider(_TextProvider)
	, m_Valid(false)
	, m_QuadBuffer(0)
	, m_CurrentBufferPos(0)
{
	if (m_pDevice)
	{
		m_Valid = true;
		m_Logger = m_pDevice->GetKernel()->GetLogger("fntb");
		m_QuadBuffer = new QuadBuffer(_pDevice, 4096 * 8);
	}
}

TextProvider::Batch::~Batch()
{
}

std::shared_ptr<TextProvider::String> TextProvider::Batch::CreateString(int x, int y, int maxlength, const std::string& text)
{
	auto str = std::make_shared<String>(m_pDevice, m_TextProvider, this, x, y);
	str->Set(text);
	//str->SetMaxLength(maxlength);
	BatchString strb;
	strb.str = str;
	strb.length = Max(maxlength, text.size());
	strb.offset = m_CurrentBufferPos;
	strb.revision = -1;
	m_Strings.push_back(strb);
	m_CurrentBufferPos += strb.length;

	return str;
}

/*
void TextProvider::Batch::SetPosition(const std::shared_ptr<TextProvider::String>& str, int x, int y)
{
	// TODO: update quadbuffer etc
	str->SetPosition(x, y);
}

void TextProvider::Batch::SetMaxLength(const std::shared_ptr<TextProvider::String>& str, int length)
{
	// find
	// update

}
*/

void TextProvider::Batch::Draw()
{
	PXF_PROFILE("text.batchdraw");
	// if any string cannot fit in it's allocated storage
	// print warning and resize.
	PXF_ASSERT(m_Valid, "batch isn't valid");

	for(auto& s: m_Strings)
	{
		int rev = s.str->GetRevision();
		if (rev != s.revision)
		{
			m_QuadBuffer->Update(s.offset, s.str->GetQuads().quads);
		}
		s.revision = rev;
	}
	m_TextProvider->DrawTextBuffer(*m_QuadBuffer, 0, 0, 1.f);
}