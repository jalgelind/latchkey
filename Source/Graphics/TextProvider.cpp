#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/GraphicsDefs.h>
#include <Pxf/Graphics/Texture.h>
#include <Pxf/Graphics/Shader.h>
#include <Pxf/Graphics/PrimitiveBatch.h>
#include <Pxf/Graphics/PrimitiveBuffer.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Util/Profiler.h>
#include <Pxf/Util/DistanceField.h>

#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <cstring>
#include <algorithm>

using namespace Pxf;
using namespace Graphics;

Graphics::TextProvider::TextProvider(Graphics::GraphicsDevice* _pDevice, 
									 Resource::Font* _Font, 
									 int _FontSize, bool use_sdf)
	: m_Font(0)
	, m_FontSize(_FontSize)
	, m_Texture(0)
	, m_DFShader(0)
	, m_OffsetX(0.f)
	, m_OffsetY(0.f)
	, DeviceResource(_pDevice)
	, m_MinFilter(Graphics::TEX_FILTER_NEAREST)
	, m_MagFilter(Graphics::TEX_FILTER_NEAREST)
	, m_UseDistanceFields(use_sdf)
{
	if (!_Font)
		return;

	if (m_UseDistanceFields)
	{
		m_MinFilter = Graphics::TEX_FILTER_LINEAR_MIPMAP_NEAREST;
		m_MagFilter = Graphics::TEX_FILTER_LINEAR;
	}

	m_Window = m_pDevice->GetActiveWindow();
	int window_width = m_Window->GetWidth();
	int window_height = m_Window->GetHeight();
	m_Logger = m_pDevice->GetKernel()->GetLogger("txtp");
	m_Font = _Font;

	m_DefaultColor = "grey";
	m_ColorBank.add("black",   0,   0,   0);
	m_ColorBank.add("grey",  200, 200, 200);
	m_ColorBank.add("white", 255, 255, 255);
	m_ColorBank.add("red",   255,   0,   0);
	m_ColorBank.add("green",   0, 255,   0);
	m_ColorBank.add("blue",    0,   0, 255);
	Reload();
}

Graphics::TextProvider::~TextProvider()
{
}

void TextProvider::SetDefaultColor(std::string color)
{
	m_DefaultColor = color;
}

void TextProvider::SetDrawOffset(float dx, float dy)
{
	m_OffsetX = dx;
	m_OffsetY = dy;
}

bool TextProvider::_TranslateColor(std::string &name, unsigned char* r, unsigned char* g, unsigned char* b)
{
	bool ret;
	Color c = m_ColorBank.get(name, &ret);
	if (!ret) return false;
	*r = c.r;
	*g = c.g;
	*b = c.b;
	return true;
}

void Graphics::TextProvider::Reload()
{
	auto fontinfo = m_Font->GetInfo();
	if (fontinfo.size_restriction == true && fontinfo.size_px != m_FontSize)
	{
		m_Logger.Warning("Attempting to create a font of unsupported size, %d. Supported size: %d. Things will not look right.", m_FontSize, fontinfo.size_px);
	}

	auto fnt = m_Font->CreateImage(m_FontSize);
	if (!fnt)
	{
		m_Logger.Error("Could not create font texture for font provider.");
		return;
	}

	m_FontCharInfo = m_Font->GetCharInfo();
	m_KerningPairs = m_Font->GetKerningPairs();

	if (m_UseDistanceFields)
	{
		m_DFShader = m_pDevice->CreateShaderFromPath("font-shader", "data/shaders/df_font.glsl");
		if (m_DFShader)
			m_DFShader->SetUniformi("tex", 0); // df-texture is on texture unit 0
		DistanceField df;
		df.Build(fnt);
		auto dfimg = df.GetImage();
#ifdef CONF_DEBUG
		{
			std::string path = m_Font->GetSource();
			path += ".png";
			dfimg->SaveAs(path.c_str());
		}
#endif
		m_Texture = GetDevice()->CreateTexture(dfimg);
		SetTextureFilters(m_MinFilter, m_MagFilter);
	}
	else
	{
		m_Texture = GetDevice()->CreateTexture(fnt);
		SetTextureFilters(m_MinFilter, m_MagFilter);
	}
}

bool Graphics::TextProvider::IsReady() const
{
	return m_Texture && m_Font;
}

std::shared_ptr<TextProvider::String> TextProvider::CreateString(const char* text)
{
	String* prep = new String(m_pDevice, this);
	prep->Set(text);
	return std::shared_ptr<TextProvider::String>(prep);
}

std::shared_ptr<TextProvider::Batch> TextProvider::CreateBatch()
{
	return std::make_shared<TextProvider::Batch>(m_pDevice, this);
}

void Graphics::TextProvider::SetTextureFilters(TextureFilter _MinFilter, TextureFilter _MagFilter)
{
	m_MinFilter = _MinFilter;
	m_MagFilter = _MagFilter;
	if (m_Texture->IsValid())
	{
		m_Texture->SetMagFilter(_MagFilter);
		m_Texture->SetMinFilter(_MinFilter);
	}
}

short TextProvider::GetKerning(const unsigned char _First, const unsigned char _Second)
{
	for(int i = 0; i < m_KerningPairs.size(); i++)
	{
		if (m_KerningPairs[i].first == _First && m_KerningPairs[i].second == _Second)
			return m_KerningPairs[i].offset;
	}
	return 0;
}


TextProvider::QuadSet TextProvider::BuildQuads(const std::string& _Text, bool _UseKerning, int xoffset, int yoffset)
{
	PXF_PROFILE("text.buildquads");
	QuadSet qs;
	if (!(m_Font && m_Font->IsReady()))
		return qs;

	Resource::Font::CharInfo_t& info = m_FontCharInfo[0];
	const Resource::Font* font = GetFont();

	// round and scale
	float x = xoffset;
	float y = yoffset;

	auto fontinfo = m_Font->GetInfo();
	float height = fontinfo.line_height, width = 0, maxwidth = 0;

	unsigned char last_char = 0;
	int kerning = 0;
	bool visible = true;

	enum State { ENormal = 0, ENewLine, ESetColor, ESetAttrib, EMetaEnd };
	State state = ENormal;

	unsigned char r, g, b;
	_TranslateColor(m_DefaultColor, &r, &g, &b);

	Color color;
	color.set_range(0, 255);
	color.set_int8(r, g, b, 255);

	for (int i = 0; i < _Text.size(); i++)
	{
		unsigned char c = _Text[i];
		if (c == '\n')
			state = ENewLine;
		else if (c == SmbSetColor)
			state = ESetColor;
		else if (c == SmbSetAttrib)
			state = ESetAttrib;
		else if (c == SmbEnd)
		{
			state = EMetaEnd;
			break;
		}

		switch(state)
		{
		case ENewLine:
		{
			int offset = fontinfo.line_height + fontinfo.newline_offset;
			height += offset;
			y += offset;
			if (width > maxwidth)
				maxwidth = width;
			width = 0;
			state = ENormal;
			break;
		}
		case ESetAttrib:
			{
				char attrib = _Text[i+1];
				i += 1;
				state = ENormal;
				break;
			}
		case ESetColor:
			{
				unsigned char r, g, b;
				r = _Text[i+1];
				g = _Text[i+2];
				b = _Text[i+3];
				i += 3;
				color = Color(r, g, b, 255);
				state = ENormal;
				break;
			}
		case ENormal:
			info = m_FontCharInfo[c];
			kerning = 0;

			// handle whitespace as special cases, don't generate quads for them.
			float whitespacemod = 1.f;
			bool draw = true;
			if (c == ' ')
			{
				draw = false;
			}
			else if (c == '\t')
			{
				info = m_FontCharInfo[' '];
				draw = false;
				whitespacemod = 4.f;
			}

			if (_UseKerning)
				kerning = GetKerning(last_char, c);
			if (draw)
			{
				Quad q;
				float xoffs = x + width + info.XOffset + kerning + m_OffsetX;
				float yoffs = y + info.YOffset + m_OffsetY;// + m_Font->GetFontSize();
				Rect quadrect(xoffs, yoffs, info.Width, info.Height);
				glm::vec2 uv1(info.Tx1, info.Ty1);
				glm::vec2 uv2(info.Tx2, info.Ty2);
				q.SetTopLeft(quadrect, 0.f);
				q.SetTextureCoordinates(uv1, uv2);
				q.SetColor(color);
				qs.quads.push_back(q);
			}
			const int advance = (info.XAdvance + kerning) * whitespacemod;
			// TODO: do something with advance - floorf(advance)?
			width += floorf(advance);
			last_char = c;
			break;
		}
	}
	qs.rect.w = Max(maxwidth, width);
	qs.rect.h = height;
	return qs;
}

void TextProvider::DrawTextBuffer(QuadBuffer& qb, int _X, int _Y, float _Scale)
{
	PXF_PROFILE("text.drawbuffer");
	std::string popcolor = GetDefaultColor();
	SetDefaultColor(m_DefaultColor);

	std::shared_ptr<Texture> old_texture = 0;
	std::shared_ptr<Shader> shdr = 0;

	m_pDevice->PushViewportAttribute();
	m_pDevice->PushModelMatrix();

	m_pDevice->SetBlendNormal();
	bool old_depth_test_value = m_pDevice->SetDepthTest(false);
	old_texture = m_pDevice->BindTexture(GetTexture(), 0);
	if (m_DFShader)
		shdr = m_pDevice->BindShader(m_DFShader);
	m_pDevice->Translate(glm::vec3(_X, _Y, 0.f));
	m_pDevice->Scale(glm::vec3(_Scale, _Scale, 1.f));
	qb.Draw();
	m_pDevice->BindTexture(old_texture);
	if (m_DFShader)
		m_pDevice->BindShader(shdr);
	m_pDevice->SetDepthTest(old_depth_test_value);
	m_pDevice->PopModelMatrix();
	m_pDevice->PopAttribute(); // viewport
	SetDefaultColor(popcolor);
}