#include <Pxf/Graphics/Rect.h>

using namespace Pxf;
using namespace Pxf::Graphics;

void Rect::HorizontalSplitFromTop(Rect& top, Rect& bottom, float pos)
{
	top.x = this->x;
	top.y = this->y;
	top.w = this->w;
	top.h = pos;
	bottom.x = this->x;
	bottom.y = this->y + pos;
	bottom.w = this->w;
	bottom.h = this->h - pos;
}

void Rect::HorizontalSplitFromBottom(Rect& top, Rect& bottom, float pos)
{
	top.x = this->x;
	top.y = this->y;
	top.w = this->w;
	top.h = this->h-pos;
	bottom.x = this->x;
	bottom.y = this->y + this->h - pos;
	bottom.w = this->w;
	bottom.h = pos;
}

void Rect::HorizontalSplitFromMiddle(Rect& top, Rect& bottom)
{
	float half_height = this->h/2.f;
	HorizontalSplitFromTop(top, bottom, half_height);
}

void Rect::VerticalSplitFromLeft(Rect& left, Rect& right, float pos)
{
	left.x = this->x;
	left.y = this->y;
	left.w = pos;
	left.h = this->h;
	right.x = this->x + pos;
	right.y = this->y;
	right.w = this->w - pos;
	right.h = this->h;
}

void Rect::VerticalSplitFromRight(Rect& left, Rect& right, float pos)
{
	left.x = this->x;
	left.y = this->y;
	left.w = this->w-pos;
	left.h = this->h;
	right.x = this->x + this->w - pos;
	right.y = this->y;
	right.w = pos;
	right.h = this->h;
}

void Rect::VerticalSplitFromMiddle(Rect& left, Rect& right)
{
	float half_width = this->w/2.f;
	VerticalSplitFromLeft(left, right, half_width);
}

void Rect::HorizontalMargin(Rect& new_rect, float margin)
{
	new_rect.y = this->y-margin;
	new_rect.h = this->h+2*margin;
}

void Rect::VerticalMargin(Rect& new_rect, float margin)
{
	new_rect.x = this->x-margin;
	new_rect.w = this->w+2*margin;
}

void Rect::Margin(Rect& new_rect, float margin)
{
	HorizontalMargin(new_rect, margin);
	VerticalMargin(new_rect, margin);
}

void Rect::HorizontalCenter(Rect& rect1)
{
	float margin_h = this->h - rect1.h;
	rect1.y = this->y + margin_h/2;
}

void Rect::VerticalCenter(Rect& rect1)
{
	float margin_w = this->w - rect1.w;
	rect1.x = this->x + margin_w/2;
}

void Rect::Center(Rect& rect1)
{
	HorizontalCenter(rect1);
	VerticalCenter(rect1);
}

