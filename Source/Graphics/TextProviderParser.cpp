#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Util/Profiler.h>

#include <cstring>

using namespace Pxf;
using namespace Graphics;

TextProvider::Parser::Parser(TextProvider* _TextProvider)
	: m_TextProvider(_TextProvider)
	, m_Length(0)
{
	m_Logger = Kernel::GetInstance()->GetLogger("pars");
}

bool TextProvider::Parser::Parse(const std::string& source, std::string& dest)
{
	PXF_PROFILE("text.parse");
	if (!m_TextProvider)
		return false;

	m_Length = 0;

	enum State { ESkip, EScan, EOpen, EConsume, EClose };
	size_t textlen = source.size();
	State state = EScan, prev_state = EScan;
	std::string tag = "";
	dest = "";

	std::vector<std::string> ColorStack;
	ColorStack.clear();

	for(int i = 0; i < textlen; i++)
	{
		char c = source[i];
		char cn = 0;
		if (i < textlen-1)
			cn = source[i+1];

		if (state == ESkip)
			state = prev_state;
		else
		{
			switch(c)
			{
			case '<':
				if (cn != '[')
				state = EOpen;
				break;
			case '>':
				if (prev_state == EConsume || prev_state == EOpen)
					state = EClose;
			}
		}

		switch(state)
		{
		case EScan:
			dest += c;
			m_Length++;
			break;
		case EConsume:
			tag += c;
			break;
		case EOpen:
			if (cn == '<')
			{
				state = ESkip;
				continue;
			}
			else
				state = EConsume;
			break;
		case EClose:
			state = EScan;
			// emit meta codes
			if (tag.length() > 0)
			{
				unsigned char r = 0, g = 0, b = 0;
				State type = EOpen;
				if (tag[0] == '/')
				{
					type = EClose;
					tag = tag.substr(1, tag.size()-1);
				}

				/*if (tag == "blink")
				{
					m_PreparedData += SmbSetAttrib;
					if (type == EClose)
						m_PreparedData += SmbAttribNone;
					else
						m_PreparedData += SmbAttribBlink;
				} */
				if (!m_TextProvider->_TranslateColor(tag, &r, &g, &b))
				{
					// TODO: enable verbosity?
					/*
					m_Logger.Warning("Failed to lookup color = '%s'.", tag.c_str());
					m_PreparedData += '<';
					if (type == EClose)
						m_PreparedData += '/';
					m_PreparedData += tag;
					m_PreparedData += '>';
					*/
				}
				else
				{
					dest += TextProvider::SmbSetColor;
					if (type == EClose)
					{
						// Pop color
						r = 0, g = 0, b = 0;
						if (ColorStack.size() > 0)
						{
							if (ColorStack.back() == tag)
							{
								ColorStack.pop_back();
								std::string color = m_TextProvider->GetDefaultColor();
								if (ColorStack.size() > 0)
									color = ColorStack.back();
								m_TextProvider->_TranslateColor(color, &r, &g, &b);
							}
							else
								m_Logger.Warning("Expected end tag '%s' at position %d. Instead I found '%s'.", ColorStack.at(ColorStack.size()-1).c_str(), i, tag.c_str());
						} 
						else
							m_Logger.Warning("Why is there a closing tag at position %d? I found no open tags.", i);
					}
					else
						// Push color
						ColorStack.push_back(tag);
					dest += r;
					dest += g;
					dest += b;
				}
				tag = "";
				state = EScan;
			}
			break;
		}
		prev_state = state;
	}
	dest += TextProvider::SmbEnd;
	return true;
}