#include <Pxf/Graphics/Model.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Mesh.h>
#include <Pxf/Kernel.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Logger.h>

using namespace Pxf;
using namespace Pxf::Graphics;

Model::Model(Graphics::GraphicsDevice* _pDevice)
	: Graphics::DeviceResource(_pDevice)
	, m_TriangleCount(0)
{
	m_Logger = Logger(m_pDevice->GetKernel(), "gfx");
	if(!Init())
		m_Logger.Error("Unable to initialize model");
}

Model::~Model()
{
}

bool Model::Load(const char* _FilePath)
{
	Resource::ResourceManager* res = GetDevice()->GetKernel()->GetResourceManager();
	auto mesh =  res->Acquire<Resource::Mesh>(_FilePath);

	if(!mesh)
	{
		m_Logger.Error("Failed to load model: '%s'", _FilePath);
		return false;
	}

	return Load(mesh);
}

bool Model::Load(const std::shared_ptr<Resource::Mesh>& _Mesh)
{
	Resource::Mesh::mesh_descriptor md = (*_Mesh->GetData());

	m_VertexCount = md.vertex_count;
	m_TriangleCount = md.triangle_count;

	m_VertexBuffer = GetDevice()->CreateVertexBuffer(VB_LOCATION_GPU, VB_USAGE_STATIC_DRAW);
	m_VertexBuffer->CreateNewBuffer(md.triangle_count*3, sizeof(Model::Vertex));
	m_VertexBuffer->SetData(VB_VERTEX_DATA, 0 , 3); // SetData(Type, OffsetInBytes, NumComponents)

	if (md.has_normals)
	{
		m_VertexBuffer->SetData(VB_NORMAL_DATA, sizeof(glm::vec3), 3);
	}

	if (md.has_uvmap)
	{
		m_VertexBuffer->SetData(VB_TEXCOORD0_DATA, sizeof(glm::vec3)*2, 2);
	}

	m_VertexBuffer->SetPrimitive(VB_PRIMITIVE_TRIANGLES);

	Model::Vertex* ptr = (Model::Vertex*) m_VertexBuffer->MapData(VB_ACCESS_READ_WRITE);
	for(int i = 0; i < md.triangle_count*3; i++)
	{
		unsigned int idx = md.indices[i] * 3;
		unsigned int idy = md.indices[i] * 2;

		glm::vec3 vertex = glm::vec3(*(md.vertices+idx+0), *(md.vertices+idx+1), *(md.vertices+idx+2));
		glm::vec3 normal;
		glm::vec2 texcoord;
		if (md.has_normals)
			normal = glm::vec3(*(md.normals+idx+0), *(md.normals+idx+1), *(md.normals+idx+2));

		if (md.has_uvmap)
			texcoord = glm::vec2(*(md.texcoords+idy+0), *(md.texcoords+idy+1));

		// set position
		ptr[i].vertex = vertex;
		ptr[i].normal = normal;
		ptr[i].texcoord = texcoord;
	}

	m_VertexBuffer->UnmapData();

	return true;
}

bool Model::Unload()
{
	m_VertexCount = 0;
	return true;
}

void Model::Draw()
{
	GetDevice()->DrawBuffer(m_VertexBuffer, 0, m_TriangleCount*3);
}

