#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/Model.h>
#include <Pxf/Graphics/PrimitiveBatch.h>
#include <Pxf/Graphics/PrimitiveBuffer.h>
#include <Pxf/Graphics/Shader.h>
#include <Pxf/Graphics/FrameBuffer.h>
#include <Pxf/Resource/Mesh.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Base/Path.h>
#include <cstdio>
#include <cstdarg>
#include <algorithm>

using namespace Pxf;
using namespace Pxf::Graphics;

GraphicsDevice::GraphicsDevice(Pxf::Kernel* _Kernel, const char* _Identifier)
	: System(_Kernel, Pxf::System::GRAPHICS_DEVICE, _Identifier)
	, m_VBOMemoryBytes(0)
	, m_TextureMemoryBytes(0)
	, m_CurrentFrameBuffer(nullptr)
	, m_CurrentShader(nullptr)
{
	blend_mode_ = BLEND_NONE;
	m_ProjectionMatrix = glm::mat4(1.f);
	m_ModelViewMatrix = glm::mat4(1.f);
}

GraphicsDevice::~GraphicsDevice()
{
}

uint64 GraphicsDevice::GetTextureMemoryUsage()
{
	return m_TextureMemoryBytes;
}

uint64 GraphicsDevice::GetVBOMemoryUsage()
{
	return m_VBOMemoryBytes;
}

void GraphicsDevice::_UpdateTextureMemoryUsage(int64 delta)
{
	m_TextureMemoryBytes += delta;
}

void GraphicsDevice::_UpdateVBOMemoryUsage(int64 delta)
{
	m_VBOMemoryBytes += delta;
}

void GraphicsDevice::_AddTexture(Texture* _tex)
{
	if (std::find(begin(m_ActiveTextures), end(m_ActiveTextures), _tex) == end(m_ActiveTextures))
		m_ActiveTextures.push_back(_tex);
}

void GraphicsDevice::_RemoveTexture(Texture* _tex)
{
	m_ActiveTextures.erase(std::remove(begin(m_ActiveTextures), end(m_ActiveTextures), _tex), end(m_ActiveTextures));
}

std::shared_ptr<Model> GraphicsDevice::CreateModel(const char* _FilePath)
{
	Model* _NewModel = new Model(this);
	_NewModel->Load(_FilePath);
	return std::shared_ptr<Model>(_NewModel);
}

std::shared_ptr<Model> GraphicsDevice::CreateModel(const std::shared_ptr<Resource::Mesh>& _Mesh)
{
	_Mesh->_AddRef();
	Model* _NewModel = new Model(this);
	_NewModel->Load(_Mesh);
	return std::shared_ptr<Model>(_NewModel);
}

std::shared_ptr<QuadBuffer> GraphicsDevice::CreateQuadBuffer(unsigned size)
{
	return std::make_shared<QuadBuffer>(this, size);
}

std::shared_ptr<PrimitiveBatch> GraphicsDevice::CreatePrimitiveBatch()
{
	return std::make_shared<PrimitiveBatch>(this);
}

std::shared_ptr<TextProvider> GraphicsDevice::CreateTextProvider(const std::shared_ptr<Resource::Font>& _font, int _fontsize, bool use_distance_fields)
{
	return std::make_shared<TextProvider>(this, _font.get(), _fontsize, use_distance_fields);
}

void GraphicsDevice::DumpTexturesToDisk(const std::string& outdir)
{
	if (!Path::Exist(outdir))
		Path::Mkdir(outdir);
	// TODO: check if outdir is a directory
	std::vector<int> done; // TODO: m_ActiveTextures is buggy and stores the same texture id multiple times 
	                       // (something related to fontprovider and resource reloading)
	for(auto tex: m_ActiveTextures)
	{
		auto id = tex->GetTextureID();
		if (std::find(begin(done), end(done), id) == end(done))
		{
			// TODO: hmmm, how to make this work with shared_ptr-funcs...
			auto image = tex->ToImage();
			std::string outfile = Path::Join(outdir, Pxf::Format("%d.png", id));
			image->SaveAs(outfile.c_str());
			done.push_back(id);
		}
	}
}