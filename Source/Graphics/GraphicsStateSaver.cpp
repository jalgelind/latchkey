#include <Pxf/Graphics/GraphicsStateSaver.h>
#include <Pxf/Graphics/GraphicsDevice.h>

using namespace Pxf::Graphics;

BoundTextureStateSaver::BoundTextureStateSaver(GraphicsDevice& dev)
	: dev(dev)
	, old(dev.GetBoundTexture())
{}

BoundTextureStateSaver::~BoundTextureStateSaver()
{
	if (old != nullptr)
		dev.BindTexture(old);
}

BoundFrameBufferStateSaver::BoundFrameBufferStateSaver(GraphicsDevice& dev)
	: dev(dev)
	, old(dev.GetActiveFrameBuffer())
{}

BoundFrameBufferStateSaver::~BoundFrameBufferStateSaver()
{
	if (old != nullptr)
		dev.BindFrameBuffer(old);
}