#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/PrimitiveBuffer.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Util/Profiler.h>

#include <cstring>

using namespace Pxf;
using namespace Graphics;


TextProvider::String::String(Graphics::GraphicsDevice* _pDevice, Graphics::TextProvider* _TextProvider, Batch* _Batch, int _XOffset, int _YOffset)
	: m_pDevice(_pDevice)
	, m_TextProvider(_TextProvider)
	, m_KerningOn(true)
	, m_Valid(false)
	, m_QuadBuffer(0)
	, m_Revision(0)
	, m_MaxLength(0)
	, m_TextBatch(_Batch)
	, m_XOffset(_XOffset)
	, m_YOffset(_YOffset)
	, m_Parser(_TextProvider)
{
	if (m_pDevice)
	{
		m_Valid = true;
		m_Logger = m_pDevice->GetKernel()->GetLogger("fntc");
		if (_TextProvider)
			m_DefaultColor = _TextProvider->GetDefaultColor();

		// TODO: TextProvider::String uses it's own buffer.
		// TODO: Share a TextProvider-global buffer.
		if (!m_TextBatch)
			m_QuadBuffer = m_pDevice->CreateQuadBuffer(32);
	}
}

TextProvider::String::~String()
{
}

void TextProvider::String::SetKerning(bool _On)
{
	m_KerningOn = _On;
}

void TextProvider::String::SetDefaultColor(const std::string& color)
{
	m_DefaultColor = color;
}

void TextProvider::String::SetPosition(int _XOffset, int _YOffset)
{
	m_XOffset = _XOffset;
	m_YOffset = _YOffset;
}

void TextProvider::String::SetMaxLength(int _Length)
{
	m_MaxLength = _Length;
	if (m_TextBatch)
	{
		// TODO
		m_Logger.Error("TP:S:SetMaxLength - not implemented");
		//m_TextBatch->SetMaxLength(this, _Length);
	}
	else
	{
		m_QuadBuffer->Resize(_Length);
	}
}

void TextProvider::String::Set(std::string text)
{
	if (text == "")
	{
		m_PreparedData = "";
		return;
	}

	Build(text.c_str());

	// Update m_QuadSet, make old quads invisible.
	QuadSet other = m_TextProvider->BuildQuads(m_PreparedData, m_KerningOn, m_XOffset, m_YOffset);
	int news = other.quads.size();
	int olds = m_QuadSet.quads.size();
	if (news >= olds)
		m_QuadSet.quads = other.quads;
	else
	{
		for(int i = 0; i < news; i++)
			m_QuadSet.quads[i] = other.quads[i];
		for(int i = news; i < olds; i++)
		{
			Color color(0.f, 0.f, 0.f, 0.f);
			m_QuadSet.quads[i].SetColor(color);
		}
	}
	m_QuadSet.rect = other.rect;
	
	if (m_QuadBuffer)
		m_QuadBuffer->Update(0, m_QuadSet.quads);
	m_Revision++;
}

void TextProvider::String::SetF(std::string format, ...)
{
	va_list ap;
	va_start (ap, format);
	std::string buf = Pxf::Format(format.c_str(), ap);
	va_end (ap);
	Set(buf);
}

bool TextProvider::String::Build(const char* text)
{
	return m_Parser.Parse(text, m_PreparedData);
}

void TextProvider::String::Print(float _X, float _Y, float _Scale)
{
	m_TextProvider->DrawTextBuffer(*m_QuadBuffer, _X, _Y, _Scale);
}
