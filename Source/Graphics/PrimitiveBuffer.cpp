#include <Pxf/Graphics/PrimitiveBuffer.h>

#include <Pxf/Math/Math.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Texture.h>
#include <Pxf/Graphics/VertexBuffer.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Utils.h>

using namespace Pxf;
using namespace Pxf::Graphics;

static void RotateXY(float rotation, const glm::vec3& center, glm::vec3& point)
{
	glm::vec3 p = point - center;
	point.x = p.x * cosf(rotation) - p.y * sinf(rotation) + center.x;
	point.y = p.x * sinf(rotation) + p.y * cosf(rotation) + center.y;
}

//
// Primitives
//

void Quad::SetColor(Color color)
{
	m_Vertices[0].color = color.as_vec4();
	m_Vertices[1].color = color.as_vec4();
	m_Vertices[2].color = color.as_vec4();
	m_Vertices[3].color = color.as_vec4();
}

void Quad::SetColor(Color color_tl, Color color_tr, Color color_bl, Color color_br)
{
	m_Vertices[0].color = color_tl.as_vec4();
	m_Vertices[1].color = color_tr.as_vec4();
	m_Vertices[2].color = color_br.as_vec4();
	m_Vertices[3].color = color_bl.as_vec4();
}

void Quad::SetDepth(float z)
{
	m_Vertices[0].pos.z = z;
	m_Vertices[1].pos.z = z;
	m_Vertices[2].pos.z = z;
	m_Vertices[3].pos.z = z;
}

void Quad::SetTextureCoordinates(glm::vec2 tl, glm::vec2 br)
{
	m_Vertices[0].tex.x = tl.x;
	m_Vertices[0].tex.y = tl.y;

	m_Vertices[1].tex.x = br.x;
	m_Vertices[1].tex.y = tl.y;

	m_Vertices[2].tex.x = br.x;
	m_Vertices[2].tex.y = br.y;

	m_Vertices[3].tex.x = tl.x;
	m_Vertices[3].tex.y = br.y;
}

void Quad::SetTopLeft(Rect rect, float rotation)
{
	glm::vec3 center(rect.x + rect.w/2, rect.y + rect.h/2, 0.f);

	m_Vertices[0].pos.x = rect.x;
	m_Vertices[0].pos.y = rect.y;
	m_Vertices[0].pos.z = 0.f;
	RotateXY(rotation, center, m_Vertices[0].pos);

	m_Vertices[1].pos.x = rect.x+rect.w;
	m_Vertices[1].pos.y = rect.y;
	m_Vertices[1].pos.z = 0.f;
	RotateXY(rotation, center, m_Vertices[1].pos);

	m_Vertices[2].pos.x = rect.x+rect.w;
	m_Vertices[2].pos.y = rect.y+rect.h;
	m_Vertices[2].pos.z = 0.f;
	RotateXY(rotation, center, m_Vertices[2].pos);

	m_Vertices[3].pos.x = rect.x;
	m_Vertices[3].pos.y = rect.y+rect.h;
	m_Vertices[3].pos.z = 0.f;
	RotateXY(rotation, center, m_Vertices[3].pos);
}

void Quad::SetCenter(Rect rect, float rotation)
{
	Rect crect(rect.x-rect.w/2, rect.y-rect.h/2, rect.w, rect.h);
	SetTopLeft(crect, rotation);
}

//
// Line
//

void Line::SetColor(Color color)
{
	m_Vertices[0].color = color.as_vec4();
	m_Vertices[1].color = color.as_vec4();
}

void Line::SetColor(Color color_from, Color color_to)
{
	m_Vertices[0].color = color_from.as_vec4();
	m_Vertices[1].color = color_to.as_vec4();
}

void Line::SetDepth(float z)
{
	m_Vertices[0].pos.z = z;
	m_Vertices[1].pos.z = z;
}

void Line::Set(glm::vec2 from, glm::vec2 to)
{
	m_Vertices[0].pos.x = from.x;
	m_Vertices[0].pos.y = from.y;
	m_Vertices[1].pos.x = to.x;
	m_Vertices[1].pos.y = to.y;
}

//
// Point
//

void Point::SetColor(Color color)
{
	m_Vertices[0].color = color.as_vec4();
}

void Point::SetDepth(float z)
{
	m_Vertices[0].pos.z = z;
}

void Point::Set(glm::vec2 pos)
{
	m_Vertices[0].pos.x = pos.x;
	m_Vertices[0].pos.y = pos.y;
}