#include <Pxf/Pxf.h>
#include <Pxf/System.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Random.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/Json.h>
#include <Pxf/Util/Configuration.h>
#include <Pxf/Kernel.h>

#include <ctime>
#include <cstdio>
#include <string>
#include <vector>
#include <sstat.h>

#ifdef CONF_FAMILY_WINDOWS
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace Pxf;

void Pxf::Init( int argc, const char** argv )
{
	std::string binpath = Path::GetPath(argv[0]);
	Pxf::RandSetSeed(time(0));
	Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();

    // TODO: Properly parse command line parameters
    bool no_config = false;
	bool no_dev_paths = false;
	bool keep_cwd = false;
    for(int i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "--no-config")
            no_config = true;
        else if (std::string(argv[i]) == "--no-dev-paths")
            no_dev_paths = true;
        else if (std::string(argv[i]) == "--keep-cwd")
            keep_cwd = true;
    }

	// Create default system configuration
	const auto& conf = kernel->GetConfiguration();
	Json::Value modules;
	modules["snd"]["load"].append("AUDIO_DEVICE");
	modules["snd"]["load"].append("RESOURCE_LOADER");
	modules["snd"]["override"] = true;
	modules["img"]["load"].append("RESOURCE_LOADER");
	modules["img"]["override"] = true;
	modules["mesh"]["load"].append("RESOURCE_LOADER");
	modules["mesh"]["override"] = true;
	modules["ttf"]["load"].append("RESOURCE_LOADER");
	modules["ttf"]["override"] = true;
	modules["auxinput"]["load"].append("INPUT_CONTROLLER");
	modules["auxinput"]["override"] = false;
	modules["dgfx"]["load"].append("GRAPHICS_DEVICE");
	modules["dgfx"]["load"].append("INPUT_CONTROLLER");
	modules["dgfx"]["override"] = false;

	Json::Value paths;
	if(keep_cwd)
		paths.append(binpath);
	paths.append("data/");
#ifdef CONF_DEBUG
	if (!no_dev_paths)
	{
		std::vector<std::string> possible_paths;
		possible_paths.push_back("../Latchkey/Dist/data/");
		possible_paths.push_back("../Latchkey/Dist/");
		possible_paths.push_back("../pxf-base/Dist/data/");
		possible_paths.push_back("../pxf-base/Dist/");
		possible_paths.push_back("../../Dist/data/");
		possible_paths.push_back("../../Dist/");
		for(auto& p: possible_paths)
		{
			if (Path::Exist(p))
				paths.append(p);
		}
	}
#endif
	
	Json::Value loggers;
	loggers["file"]["enable"] = true;
	loggers["stdout"]["enable"] = true;
#ifdef CONF_FAMILY_WINDOWS
	loggers["stdout"]["linebreakhint"] = 69;
#else
	loggers["stdout"]["linebreakhint"] = 170;
#endif

	conf->Set("sys.paths", paths, "resource search paths");
	conf->Set("sys.modules", modules, "system modules to load dynamically");
	conf->Set("sys.loggers", loggers, "system loggers");
	conf->Set("sys.cwdroot", true, "set cwd to application root");
	conf->Set("sys.win32.hide_console", false, "hide console window");

	conf->MarkAsFallback();

	std::string configpath = "config.json";
	std::string altpath = Path::Join(binpath, "config.json");
	if (!no_config)
	{
		if(Path::Exist(altpath))
			configpath = altpath;
		conf->Load(configpath, true);
		if (!conf->Save(configpath))
			printf("Failed to system configuration to file.");
		conf->Reload();
	}
	
	#ifdef CONF_FAMILY_WINDOWS
	// HACK: should build executable with WinMain as entrypoint.
	if (conf->GetBool("sys.win32.hide_console"))
		FreeConsole();
	#endif

	std::vector<std::string> args;
	for(size_t i = 1; i < argc; i++)
		args.push_back(argv[i]);

	// Set working directory
	if (!keep_cwd)
	{
	#ifdef CONF_FAMILY_WINDOWS
		SetCurrentDirectoryA(binpath.c_str());
	#else
		chdir(binpath.c_str());
	#endif
	}

	for(auto& s: conf->Get("sys.paths"))
		kernel->AddResourcePath(s.asString());

	kernel->Initialize(argv[0], args);

	auto flog = kernel->GetFileLogger();
	auto stdlog = kernel->GetStdoutLogger();
	flog->Enable(conf->GetBool("sys.loggers.file.enable"));
	stdlog->Enable(conf->GetBool("sys.loggers.stdout.enable"));
	if (stdlog->IsEnabled())
		stdlog->SetLinebreakHint(conf->GetInt("sys.loggers.stdout.linebreakhint"));

	// Load/initialize modules based on configuration
	Json::Value& data = conf->Get("sys.modules");
	for(::Json::ValueIterator itr = data.begin(); itr != data.end(); ++itr)
	{
		std::string modname = itr.key().asString();
		bool ovr = (*itr)["override"].asBool();
		unsigned int flags = 0;
		Json::Value& components = (*itr)["load"];
		for(auto& v: components)
		{
			unsigned int flag = System::repr(v.asString());
			if (flag == System::UNDEFINED)
				printf("Undefined flag '%s'\n", v.asString().c_str());
			flags |= flag;
		}
		#ifdef CONF_MODULAR
		kernel->RegisterModule(modname.c_str(), flags, ovr);
		#else
		kernel->SetModuleFilter(modname.c_str(), flags);
		if (ovr)
			kernel->RegisterModule(modname.c_str(), flags, ovr);
		#endif
	}
}

void Pxf::InitModules()
{
	Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();
	kernel->InitializeModules();
	kernel->DumpAvailableModules();
	kernel->GetResourceManager()->DumpResourceLoaders();
	kernel->GetInputDevice()->DiscoverDevices();

}

void Pxf::Shutdown()
{
// FIXME, XXX: Causes FreeLibrary() to fail when releasing some library. Heap corruption?
//#ifndef CONF_MODULAR
	Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();
	delete kernel;
//#endif
}

