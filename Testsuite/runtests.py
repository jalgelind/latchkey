#!/usr/bin/env python

# runtests.py <test-executable>

import os, re, sys, subprocess, threading
import helpers

class TestRunner(threading.Thread):
    def __init__(self, cmd, testdir, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd 
        self.testdir = testdir
        self.timeout = timeout
        self.output = None
        self.died = False
        self.returncode = 1

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, cwd=self.testdir)
        self.output = self.p.communicate()[0].decode("utf-8")
        self.p.wait()

    def launch(self):
        self.start()
        self.join(self.timeout)
        if self.is_alive():
            self.p.terminate()
            #self.p.kill()
            self.died = True
            self.join()
        self.returncode = self.p.returncode

def run_test(bin, testdir, script, timeout):
    r = TestRunner([bin, '--no-config', '--no-dev-paths', '--keep-cwd', script], testdir, timeout)
    r.launch()
    return r.output, not r.died, r.returncode

### Fixme
logfp = None
def lopen(fn = "default.log"):
    global logfp
    logfp = open(fn, "w")

def lwrite(msg):
    print(msg)
    logfp.write(msg + "\n")

def lclose():
    logfp.close()
###

def main(prog, args):
    if not helpers.get_compare():
        print("Failed to setup a proper testing environment.")
        sys.exit(1)

    if len(args) == 0:
        print("usage: runtests.py <test-executable>")
        sys.exit(1)

    basedir = os.path.dirname(prog)
    testdir = os.path.realpath(os.path.join(basedir, "tests"))
    testbin = os.path.abspath(args[0])

    if not os.path.exists(testbin):
        print("Could not find executable '{}'.".format(testbin))
        sys.exit(1)

    os.chdir(testdir)

    lopen()
    tests = []
    for root, dirs, files in os.walk("."):
        for fn in [f for f in files if f.endswith(".lua")]:
            if os.path.realpath(root) == testdir:
                continue
            path = os.path.join(root, fn)
            test = {}
            test['script'] = path
            tests.append(test)

    print("Collected {} test(s).".format(len(tests)))

    for test in tests:
        script = test["script"]
        lwrite("Running test: {}".format(os.path.basename(script)))
        out, success, errorcode = run_test(testbin, testdir, script, 10)
        lwrite("success: {}, errorcode: {}".format(success, errorcode))
        def parseout(out):
            for line in out.split("\n"):
                rline = re.search("\[(\w+)\]\[(\w+)\]\s+(.*)", line.strip())
                if rline:
                    msgtype, what, msg = rline.groups()
                    if msgtype in ("E",): # only show errors
                        lwrite(line)
        parseout(out)

if __name__ == '__main__':
    main(sys.argv[0], sys.argv[1:])

