import os, glob, zipfile

def which(program):
    """ Find program in PATH
        as suggested here http://stackoverflow.com/a/377028
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep)+['.']:
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

def fetch_imagemagick():
    from ftplib import FTP
    ftp = FTP('ftp.imagemagick.org')
    ftp.login()
    ftp.cwd('pub/ImageMagick/binaries/')
    hack = dict(filename="")
    def cb(fn):
        if fn.strip().endswith('windows.zip'):
            hack["filename"] = fn.split()[-1]
    ftp.retrlines('LIST', cb)
    print("Downloading '%s'..." % hack["filename"])
    fp = open("tools/arch/" + hack["filename"], 'wb')
    ftp.retrbinary("RETR %s" % hack["filename"], fp.write)
    fp.close()
    ftp.quit()
    return hack["filename"]

def setup_compare():
    compare_path = "tools/compare.exe"
    imagemagick_zip = None
    for fn in glob.glob("tools/arch/*.zip"):
        if 'imagemagick' in fn.lower():
            imagemagick_zip = fn
    if not imagemagick_zip:
            imagemagick_zip = "tools/arch/" + fetch_imagemagick()
    print("Setting up imagemagick...")
    zf = zipfile.ZipFile(imagemagick_zip)
    for fn in zf.namelist():
        fnl = fn.lower()
        if fnl.endswith("compare.exe"):
            with open("tools/" + os.path.basename(fn), "wb") as f:
                f.write(zf.open(fn, "r").read())
                return compare_path

def get_compare():
    """ Get path to imagemagick compare. Downloads and sets it up on windows
        automatically if it's not found. """
    if os.name == 'posix':
        return which("compare")
    elif os.name == 'nt':
        try:
            os.mkdir("tools")
        except OSError: pass

        try:
            os.mkdir("tools/arch")
        except OSError: pass

        compare_path = which("compare.exe")
        if compare_path:
            return compare_path

        compare_path = "tools/compare.exe"
        if os.path.exists(compare_path):
            return compare_path
        return setup_compare()
    else:
        print("OS (={}) is probably not supported. Try installing imagemagick.".format(os.name))
        sys.exit(1)
    return None
