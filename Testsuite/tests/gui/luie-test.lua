require 'gui/luie'

-- avoid issue 15
function on_file_reload()
  return true
end

function on_init()
  build_gui()
end

function make_button(text, align, valign)
  local a = align or "left"
  local va = valign or "top"
  return { type="button", align=a, valign=va, text=text }
end

function make_label(text, align, valign)
  local a = align or "left"
  local va = valign or "top"
  return { type="label", align=a, valign=va, text=text}
end

function make_checkbox(value, align, valign)
  local a = align or "left"
  local va = valign or "top"
  return { type="checkbox", align=a, valign=va, value=value }
end

function make_hstack(...)
  return { type="stack", direction="horizontal", align="right", valign="top", body = { ... } }
end

function make_vstack(...)
  return { type="stack", align="right", valign="top", direction="vertical", body = { ... } }
end

function make_hsplit(...)
  return { type = 'split2', from = "left", at = "50%", body = { ... }}
end

function make_vsplit(...)
  return { type = 'split2', from = "top", at = "50%", body = { ... }}
end

function build_gui()
  -- TODO: min/max height/width
  btns1 = make_vstack(make_hstack(make_button("A", "left"), make_button("B", "right"))
                          ,make_hstack(make_button("C", "left"), make_button("D", "right")))
  btns2 = make_vstack(make_hstack(make_button("A", "center"), make_button("B", "center"))
                          ,make_hstack(make_button("C", "right"), make_button("D", "left")))
  btns3 = make_vstack(make_hstack(make_button("A", "right"), make_button("B", "right"))
                          ,make_hstack(make_button("C", "center"), make_button("D", "right")))
  chk1 = make_vstack(make_hstack(make_checkbox(0, "left"), make_checkbox(1, "right"))
                          ,make_hstack(make_checkbox(1, "left"), make_checkbox(0, "right")))
  chk2 = make_vstack(make_hstack(make_checkbox(0, "center"), make_checkbox(1, "center"))
                          ,make_hstack(make_checkbox(1, "right"), make_checkbox(0, "left")))
  chk3 = make_vstack(make_hstack(make_checkbox(0, "right"), make_checkbox(1, "right"))
                          ,make_hstack(make_checkbox(1, "center"), make_checkbox(0, "right")))
  main_layout = make_vstack(
                  make_label("Greetings, stranger..."),
                  make_hsplit(
                    make_vstack(btns1, btns2, btns3
                               ,chk1, chk2, chk3),
                    make_hsplit(
                        make_hstack(make_button("Quit"),
                                    make_button("Exit")),
                        make_hstack(make_vsplit(make_vstack(chk1),
                                                make_vstack(btns3)),
                                    make_vstack(make_checkbox(0),
                                                make_checkbox(1))
                                    ))))

  classic = make_vstack(
                    make_label("Hello world"),
                    make_hsplit(
                      make_vstack(make_button("Continue"),
                                  make_button("Ladala"),
                                  make_button("ilalala"),
                                  make_button("ijbelrr"),
                                  make_button("hrroal"),
                                  make_button("lolololol"),
                                  make_button("kneoeu������"),
                                  make_button("1re0oO0"),
                                  make_vstack(make_button("aoeu", "right"))),
                      make_vstack(
                        make_hstack(make_button("Quit"),
                                    make_button("Exit"),
                                    make_button("No")),
                        make_vstack(make_checkbox(0, "left"),
                                    make_checkbox("1", "center"),
                                    make_hstack(make_checkbox(1), make_label("check itaaoeeaoe  \n\tlols")),
                                    make_checkbox(1, "right"),
                                    make_checkbox(1, "center", "top")
                                    ),
                        make_vsplit(make_button("apa"),
                                    make_vsplit(make_button("baloaoeae"),
                                                make_button("baloaae"))
                                    ),
                        make_hstack(make_vstack(make_checkbox(1, "left"),
                                                make_checkbox(0, "left")),
                                    make_checkbox(0, "left"),
                                    make_checkbox(0, "center"),
                                    make_checkbox(1, "right"),
                                    make_checkbox(0, "right"),
                                    make_vstack(make_button("bae"), make_button("be")),
                                    make_button("baloaoeae"))
                        )))

  gui = luie.new()

  local window = luie:build(main_layout, {gfx.get_active_window():size()})
  luie:pushwindow(window)
end

function update()
  gui:update()
end

gfx.set_window_size(460, 340)
win = gfx.get_active_window()
function draw()
  win_w, win_h = win:size()
  gfx.set_viewport(0, 0, win_w, win_h)
  gfx.set_ortho_projection(math.ortho(0, win_w, win_h, 0, -10, 10))
  gfx.clear()
  gui:draw()
  --gfx.save_framebuffer_as("themed.png")
end
