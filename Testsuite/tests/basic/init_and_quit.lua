local ts = require 'testsuite'

function _on_init()
  print("on_init")
end

function _on_shutdown()
  print("on_shutdown")
end

function _update()
  lapp.app():quit()
end

function _draw()
end
