Experimenting with some basic testing. The idea is to test both logic and rendering.

The tests are written in Lua. If the Lua API works as intended, then the underlying
C++ API should work equally well.

Since not everything is exposed in the Lua API, there will be some things that can't
yet be tested. It's a start anyhow.

External tools will automatically be downloaded if they aren't installed on the system.

Required external tools:
	imagemagick for comparing images.