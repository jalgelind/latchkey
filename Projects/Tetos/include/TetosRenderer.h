#ifndef _TETOS_BOARDRENDERER_H_
#define _TETOS_BOARDRENDERER_H_

#include "TetosEngine.h"

namespace Pxf { namespace Graphics { class GraphicsDevice;
									 class Texture;
									 class PrimitiveBatch; }}

namespace Tetos
{
	class Game;
	class Sprite;
	class Piece;
	class Board;

	class TetosRenderer
	{
	public:
		Game& game_;
		Pxf::Graphics::GraphicsDevice& gfx_;
		Pxf::Graphics::PrimitiveBatch& pb_;
		Pxf::Graphics::Texture& tex_;

		TetosRenderer(Game& game);
		virtual ~TetosRenderer();

		void _SetBlockSprite(Block::Color color);

		void _SelectSprite(const Sprite& sprite);
		void _SetTexCoords(int x, int y, int w, int h, int tx, int ty);

		void _DrawBlock(Block& block, int x, int y, float opacity);
		void DrawPiece(Piece& p, int x, int y, float opacity = 1.f);
		void DrawBackground();
		void DrawBoard(Board& b, int x, int y);
	};
}

#endif // _TETOS_BOARDRENDERER_H_