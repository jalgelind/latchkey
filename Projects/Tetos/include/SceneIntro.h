#ifndef _TETOS_SCENEINTRO_H_
#define _TETOS_SCENEINTRO_H_

#include <Pxf/Base/Timer.h>
#include <Pxf/Util/Scene.h>
#include <Pxf/Util/Curve.h>

#include <Pxf/Util/Easing.h>

/*
	Game screen rendering and input handling
*/

namespace Pxf { class Logger;
				namespace Input    { class InputDevice;    }
				namespace Graphics { class GraphicsDevice;
									 class Texture;
									 class Prepared;
									 class PrimitiveBatch; }}

namespace Tetos
{
	class Game;
	class TetosLogic;

	class SceneIntro : public Pxf::Scene
	{
	public:
		// Game instance and system devices
		Game& game_;
		std::shared_ptr<Pxf::Graphics::PrimitiveBatch> pb_;
		std::shared_ptr<Pxf::Graphics::Texture> tex_;

		std::shared_ptr<Pxf::Graphics::TextProvider> textprovider_;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> textstring_;

		float fade_color_;
		float fade_pos_x_;
		float fade_pos_y_;
		float fade_curve_;

		Pxf::Curve curve_;

		std::vector<Pxf::Easing*> easings_;

		SceneIntro(Game& game);
		~SceneIntro();

		void Init();
		void Reset();

		void DoInput();
		void DoUpdate(unsigned int framenum);
		void DoRender(float _Interpolation);
	};
}

#endif // _TETOS_SCENEINTRO_H_
