#ifndef _TETOS_ROTATIONSYSTEM_H_
#define _TETOS_ROTATIONSYSTEM_H_

#include <Pxf/Math/Math.h>

#include "Game.h" // Actions
#include "TetosEngine.h"
#include "TetosInput.h"

namespace Tetos
{
	class RotationSystem
	{
	public:

		// TODO: Implement this as return for Rotate() function
		struct RotationResult
		{
			Piece piece;
			glm::ivec2 offset;
			bool valid;
			RotationResult() { valid = false; }
		};

		// Init by setting default orientation
		virtual void Init(Piece& piece) = 0;

		// Rotate piece CCW
		virtual void RotateLeft(Piece& piece) = 0;

		// Rotate piece CW
		virtual void RotateRight(Piece& piece) = 0;
		
		// Is piece allowed to wallkick at its current position?
		// (Useful for preventing I (when using 4 states) to wallkick
		// between its 2 vertical positions.)
		virtual bool CanWallKick(TetosLogic& board, Piece& piece) = 0;

		// Attempt rotation (incl. wallkick), return true on success.
		virtual RotationResult Rotate(TetosLogic& board, Piece& piece, InputTrigger dir, bool allow_kick) = 0;
	};

	// 2-state SZI
	// Simpler wallkick
	class SimpleRotationSystem : public RotationSystem
	{
	public:
		virtual void Init(Piece& piece);
		virtual void RotateLeft(Piece& piece);
		virtual void RotateRight(Piece& piece);
		virtual bool CanWallKick(TetosLogic& engine, Piece& piece);
		virtual RotationResult Rotate(TetosLogic& engine, Piece& piece, InputTrigger dir, bool allow_kick);
	};

	class SuperRotationSystem : public RotationSystem
	{
	public:
		virtual void Init(Piece& piece);
		virtual void RotateLeft(Piece& piece);
		virtual void RotateRight(Piece& piece);
		virtual bool CanWallKick(TetosLogic& engine, Piece& piece);
		virtual RotationResult Rotate(TetosLogic& engine, Piece& piece, InputTrigger dir, bool allow_kick);
	};

	// TODO: ARS?
}

#endif // _TETOS_ROTATIONSYSTEM_H_