#ifndef _TETOS_TETOSCOMPONENTS_H_
#define _TETOS_TETOSCOMPONENTS_H_

#include <string>

namespace Tetos
{

	struct Block
	{
		enum Color {
			None = 0,
			Red,
			Green,
			Blue,
			DarkGrey,
			LightGrey,
			Dark,
			Yellow,
			Purple,
			Orange,
			Cyan
		};

		Color color;
		float opacity;

		Block()
		{
			opacity = 0.f;
		}
	};


	class Piece
	{
	public:
		// Block for data storage
		Block data[4][4];
		int size; // size used by tetromino (not all tetrominos use 4x4)

		int max_rotations; // for tetromino. 4 = full rotate; 2 = flipflop forth and back (will need to adjust the kick algorithm).
		int rotations;     // on rotation: rotations %= max_rotations; 

		enum Type {
#define TETOS_PIECETYPE(type) type,
#include "Tetos.def"
		};

		std::string repr(Type p)
		{
#define TETOS_PIECETYPE(piece) { if (piece == p) return #piece; }
#include "Tetos.def"
			return "";
		}

		Type type;

		Piece();
		Piece(Type type);
		Piece (const Piece& p);
		void Set(Type type);
		void SetOpacity(float opacity);

		void _RotateRightImpl();
		void RotateRight();
		void RotateLeft();

		int Height();
		int Width();

		int GetRotationState() const
		{
			return 4-rotations;
		}
	};


	class Board
	{
	public:
		Block data_[22][10]; // board[rows][col]
		// 2 invisible rows for spawn

		void CopyFrom(Board& b);
		bool CanPutBlock(Block& b, int x, int y);
		bool CanPutPiece(Piece& p, int x, int y);
		void PutBlock(Block& b, int x, int y, bool dead);
		void PutPiece(Piece& p, int x, int y, bool dead);
		int FreeSpaceBelow(Piece& p, int x, int y);
		void Clear();
	};
}


#endif // _TETOS_TETOSCOMPONENTS_H_