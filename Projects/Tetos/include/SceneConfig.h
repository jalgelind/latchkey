#ifndef _TETOS_SCENE_CONFIG_H_
#define _TETOS_SCENE_CONFIG_H_

#include <Pxf/Base/Timer.h>
#include <Pxf/Util/Scene.h>
#include <Pxf/Util/Curve.h>
#include <Pxf/Util/Easing.h>
#include "TetosEngine.h"
#include "TetosInput.h"
#include <string>

/*
	Game screen rendering and input handling
*/

namespace Pxf { class Logger;
				namespace Input    { class InputDevice;    }
				namespace Graphics { class GraphicsDevice;
									 class Texture;
									 class Prepared;
									 class PrimitiveBatch; }}

namespace Tetos
{
	class Game;
	class TetosLogic;

	class SceneConfig : public Pxf::Scene
	{
	public:
		// Game instance and system devices
		Game& game_;
		Pxf::Graphics::PrimitiveBatch& pb_;
		Pxf::Graphics::Texture& tex_;

		Pxf::Graphics::TextProvider& font_;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> str_;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> str_desc_;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> str_configurable_;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> str_key_pressed_;

		InputProcessor::Keys keys_;
		InputProcessor::PadControls padctl_;

		enum ConfigState
		{
			NONE,
			PROMPT_KEYBOARD_CONFIGURATION,
			CONFIG_KEYBOARD,
			PROMPT_GAMEPAD_CONFIGURATION,
			CONFIG_GAMEPAD
		};

		ConfigState state_;

		enum ControlConfigState
		{
			CONFIGURE_MOVE_LEFT,
			CONFIGURE_MOVE_RIGHT,
			CONFIGURE_ROTATE_LEFT,
			CONFIGURE_ROTATE_RIGHT,
			CONFIGURE_SOFT_DROP,
			CONFIGURE_HARD_DROP,
			CONFIGURE_HOLD,
			CONFIGURE_DONE
		};

		ControlConfigState control_config_state_;

		std::string text_;
		std::string text_desc_;

		std::string configurable;
		std::string key_pressed;

		float text_color_;
		float text_pos_x_;
		Pxf::Curve text_path_;

		SceneConfig(Game& game);
		~SceneConfig();

		void Init();
		void Reset();

		void PrepareState(ConfigState state);

		void DoInput();
		void DoUpdate(unsigned int framenum);
		void DoRender(float _Interpolation);
	};
}

#endif // _TETOS_SCENE_CONFIG_H_