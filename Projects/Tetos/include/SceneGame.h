#ifndef _TETOS_SCENEGAME_H_
#define _TETOS_SCENEGAME_H_

#include <Pxf/Base/Timer.h>
#include <Pxf/Util/Scene.h>
#include <Pxf/Graphics/TextProvider.h>
#include "TetosRenderer.h"

/*
	Game screen rendering and input handling
*/

namespace Pxf { class Logger;
				namespace Input    { class InputDevice;    }
				namespace Graphics { class GraphicsDevice;
									 class Texture;
									 class PrimitiveBatch; }}

namespace Tetos
{
	class Game;
	class TetosLogic;

	class SceneGame : public Pxf::Scene
	{
	public:
		Game& game_;
		std::shared_ptr<Pxf::Graphics::Texture> game_texture_;
		std::shared_ptr<Pxf::Graphics::PrimitiveBatch> game_pb_;

		std::shared_ptr<Pxf::Graphics::TextProvider> font_;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> gfxstr_;

		// Board engine and board renderer
		TetosLogic tetos_logic_;
		TetosRenderer board_renderer_;

		bool display_eliminated_;

		SceneGame(Game& game);
		~SceneGame();

		void Init();
		void Reset();

		void DoInput();
		void DoUpdate(unsigned int framenum);
		void DoRender(float _Interpolation);

		void DrawBoard(float interpolation, int in_pos_x, int in_pos_y, float in_scale);
		void DrawDebug();
	};
}

#endif // _TETOS_SCENEGAME_H_