#ifndef _TETOS_TETOSLOGIC_H_
#define _TETOS_TETOSLOGIC_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Timer.h>
#include <Pxf/Input/Input.h>
#include <Pxf/Math/Math.h>

#include <TetosBlocks.h>
#include <TetosInput.h>
#include <TetosPlayer.h>

#include <string>
#include <vector>
#include <random>

namespace Pxf
{
	namespace Input
	{
		class Keyboard;
		class Gamepad;
	}
	class Configuration;
}

namespace Tetos
{
	class Game;
	class RotationSystem;

	static const char* CURRENT_SAVE_FORMAT_VERSION = "1.0-SNIGEL";

	class TetosLogic
	{
	public:
		// TODO: when Input::* each has a unique bit value, make this a bitfield
		// and add m_PreviousInputState;
		std::vector<bool> m_InputStates;

		struct ActionHistoryItem
		{
			unsigned int tick;
			InputEvent ev;
		};

		struct GameHistory
		{
			std::string identifier;
			std::string playername;
			std::string saveformat;
			std::vector<ActionHistoryItem> actionhistory;
			std::vector<Piece::Type> piecelog;
			GameHistory()
			{
				playername = "J.D.";
				identifier = "Unnamed session";
				saveformat = CURRENT_SAVE_FORMAT_VERSION;
				actionhistory.reserve(60 * 60 * 10); // reserve space for a long game (10 minutes)
				piecelog.reserve(60*60);
			}
		};


		GameHistory current_game_history_;

		//
		// Game states
		//

		enum State
		{
		#define TETOS_STATE(statename) statename,
		#include "Tetos.def"
		};

		std::string repr(State state)
		{
		#define TETOS_STATE(statename) { if (statename == state) return #statename; }
		#include "Tetos.def"
		return "";
		}

		State state_;

		//
		// Move actions
		//

		Board board_; // Board with garbage tiles
		RotationSystem* rotsys_;

		Pxf::Logger logger_;

		Player player_;
		
		bool game_forcelock_;

		// Score related stuffs
		unsigned game_current_level_; // Current level (managed in Board::Step right now)
		unsigned game_eliminated_rows_;

		Pxf::Timer timer_gametick_; // Timer for gametick
		unsigned int current_frame_;
		unsigned int current_gametick_;

		unsigned int rand_seed_;

		struct Counter
		{
			int soft_drop_counter;
			int hard_drop_counter;
			int are_counter;
			int line_clear_counter;
		};
		Counter counter_;

		Timing& timing_;

		int game_gravity_;
		int game_gravity_accumulator_;

		struct FrameState
		{
			unsigned int rows_eliminated;
			char rowstate[22];
			bool gameover;
			bool in_are;

			FrameState()
			{
				Reset();
			}

			void Reset()
			{
				rows_eliminated = 0;
				for(int i = 0; i < 22; i++)
					rowstate[i] = 0;
				gameover = false;
				in_are = false;
			}
		};
		FrameState frame_state_;

		bool run_replay_;

		TetosLogic(RotationSystem* rotsys, Timing& timing);
		virtual ~TetosLogic();

		void Init();

		// clear / reset
		void Clear();

		void ResetTimers();

		void GameOver();

		// Update game state
		void Update(unsigned int framenum, std::vector<InputEvent>& events);

		int EliminateRows(bool dry=false);

		Piece::Type RandomPiece(std::vector<Piece::Type>& next_pieces);

		// Get
		unsigned GetEliminatedRows()
		{
			return game_eliminated_rows_;
		}
		unsigned GetCurrentLevel()
		{
			return game_current_level_;
		}

		unsigned GetTargetLevel()
		{
			int multiplier = 1 + game_current_level_ / 100;
			return multiplier * 100;
		}

		Piece::Type GetNextPiece(int i)
		{
			//if (i == 0 && player_.pos_.y < 1)
			//	return player_.active_piece_.type;
			return player_.next_pieces_[i];
		}

		Piece& GetHoldPiece()
		{
			return player_.hold_piece_;
		}

		void RunReplay()
		{
			run_replay_ = true;
			state_ = STATE_INIT;
		}

		// int GetScore();
	};

}

#endif // _TETOS_TETOSLOGIC_H_

