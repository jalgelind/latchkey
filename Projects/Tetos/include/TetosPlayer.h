#ifndef _TETOS_TETOSPLAYER_H_
#define _TETOS_TETOSPLAYER_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>
#include <Pxf/Math/Math.h>
#include <TetosInput.h>
#include <TetosBlocks.h>

#include <vector>

namespace Tetos
{
	class RotationSystem;
	class TetosLogic;
	class Board;
	class Player
	{
	public:
		Player(TetosLogic& engine, Board& b, RotationSystem& rot, Timing& t);

		void ProcessInputEvent(InputEvent ev);
		void ProcessGameTick();

		void Rotate(InputTrigger trig, bool allow_kick=true);
		void HoldSwap();
		void OnInstantActionEvent(InputEvent ev);

		void OnNewGame();
		void OnNewRound();

		void OnMoveEvent(InputEvent ev);
		InputTrigger CurrentMovementState();

		bool CanMove(InputTrigger dir);
		void Move(InputTrigger dir, bool gravitystep = false);

		glm::ivec2 GetPosition()
		{
			return pos_;
		}

		void SetPosition(int x, int y)
		{
			pos_.x = x;
			pos_.y = y;
		}

		void NextPiece();

		Pxf::Kernel* kernel_;
		Pxf::Logger logger_;

		glm::ivec2 pos_;

		TetosLogic& engine_;
		Board& board_;
		Timing& timing_;
		RotationSystem& rotsys_;

		std::vector<InputTrigger> movement_stack_;

		InputTrigger curr_event_;
		InputTrigger prev_event_;

		int move_shift_initial_delay_;
		int move_shift_delay_;
		int move_shift_counter_;
		int lock_counter_;
		bool can_use_holdswap_;
		bool can_rotate_;
		bool can_force_lock_;

		Piece active_piece_;          // tetromino in play
		Piece hold_piece_;            // tetromino on hold
		std::vector<Piece::Type> next_pieces_; // upcoming tetrominos
	};
}

#endif // _TETOS_TETOSPLAYER_H_