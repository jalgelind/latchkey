#ifndef _TETOS_TETOSINPUT_H_
#define _TETOS_TETOSINPUT_H_

#include <Pxf/Util/Configuration.h>
#include <Pxf/Input/Keyboard.h>
#include <Pxf/Input/Gamepad.h>

namespace Tetos
{
	struct Timing
	{
		int hds_delay;   // hard drop shift delay
		int sds_delay;   // soft drop shift delay
		int asm_delay;   // auto shift move delay
		int das_delay;   // delayed auto shift delay
		int are_delay;   // piece appearence delay
		int piecelock_delay;   // delay before locking piece
		int lineclear_delay;   // line clear delay

	};

	enum InputTrigger
	{
		// TODO: value = unique bit
#define TETOS_INPUT(inputname, value) inputname, //=value,
#include "Tetos.def"
	};

	static std::string repr(InputTrigger input)
	{
#define TETOS_INPUT(inp, value) { if (inp == input) return #inp; }
#include "Tetos.def"
		return "";
	}

	enum InputState
	{
#define TETOS_INPUT_STATE(inputstate) inputstate,
#include "Tetos.def"
	};

	static std::string repr(InputState input)
	{
#define TETOS_INPUT_STATE(inp) { if (inp == input) return #inp; }
#include "Tetos.def"
		return "";
	}

	struct InputEvent
	{
		InputTrigger trigger;
		InputState state;
		InputEvent() { trigger = INPUT_INVALID; }
		InputEvent(InputTrigger t, InputState s) { trigger = t; state = s; }
		std::string AsString()
		{
			std::string ret = repr(trigger) + " - " + repr(state);
			return ret;
		}
	};

	struct InputProcessor
	{
		Pxf::Input::Keyboard& kbd_;
		Pxf::Input::Gamepad& pad_;
		InputProcessor(Pxf::Input::Keyboard& kbd, Pxf::Input::Gamepad& pad)
			: kbd_(kbd), pad_(pad)
		{}

		struct Keys
		{
			Pxf::Input::Keyboard::Event left;
			Pxf::Input::Keyboard::Event right;
			Pxf::Input::Keyboard::Event rotleft;
			Pxf::Input::Keyboard::Event rotright;
			Pxf::Input::Keyboard::Event soft_drop;
			Pxf::Input::Keyboard::Event hard_drop;
			Pxf::Input::Keyboard::Event hold;
		};

		struct PadControls
		{
			Pxf::Input::Gamepad::Event left;
			Pxf::Input::Gamepad::Event right;
			Pxf::Input::Gamepad::Event rotleft;
			Pxf::Input::Gamepad::Event rotright;
			Pxf::Input::Gamepad::Event soft_drop;
			Pxf::Input::Gamepad::Event hard_drop;
			Pxf::Input::Gamepad::Event hold;
		};

		Keys keys;
		void SetKeys(Keys ks) { keys = ks; }

		PadControls padctl;
		void SetPadControls(PadControls pc) { padctl = pc; }

		InputEvent Process(Pxf::Input::Keyboard::Event& kbd);
		InputEvent Process(Pxf::Input::Gamepad::Event& pad);

		Pxf::Input::Keyboard::Event ReadKeyboardValue(Pxf::Configuration& cfg, std::string root, std::string node);
		void WriteKeyboardValue(Pxf::Configuration& cfg, std::string root, std::string node, Pxf::Input::Keyboard::Event value);

		Pxf::Input::Gamepad::Event ReadGamepadValue(Pxf::Configuration& cfg, std::string root, std::string node);
		void WriteGamepadValue(Pxf::Configuration& cfg, std::string root, std::string node, Pxf::Input::Gamepad::Event value);

		void Load(Pxf::Configuration& cfg, std::string root);
		void Save(Pxf::Configuration& cfg, std::string root);
	};
}

#endif // _TETOS_TETOSINPUT_H_