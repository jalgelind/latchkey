#ifndef _TETOS_TETOSGAMEHELPERS_H_
#define _TETOS_TETOSGAMEHELPERS_H_

#include <string>
#include "TetosEngine.h"

namespace Tetos
{
	bool SaveGameAs(std::string path, TetosLogic::GameHistory& history);
	TetosLogic::GameHistory LoadGameFrom(std::string path);
	int GetGravityForLevel(int level);
}

#endif // _TETOS_TETOSGAMEHELPERS_H_