#ifndef _TETOS_TETOSDATA_H_
#define _TETOS_TETOSDATA_H_

/*
	Types and some type data
*/

#include <Pxf/Graphics/PrimitiveBatch.h>

namespace Tetos
{
	struct Sprite
	{
		const char* file;

		// Location of top left corner
		int x;
		int y;

		// Size
		int w;
		int h;

		// Texture size
		int tx;
		int ty;
	};

	/*
	template <int NumFrames = 10>
	class AnimatedSprite
	{
	public:
		Sprite frames[NumFrames];
		float delay; // delay in seconds
	};
	*/


	// Images
	static const char* DATA_FILE = "data/data.png";

	// Sounds
	static const char* SND_ROTATE_L_FILE = "data/snd2.ogg";
	static const char* SND_ROTATE_R_FILE = "data/snd2.ogg";
	static const char* SND_HARDDROP_FILE = "data/snd4.ogg";
	static const char* SND_LINEREMOVE_FILE = "data/snd4.ogg";

	//                                     file    x   y   w   h   tx   ty
	static const Sprite BLOCK_PURPLE = {DATA_FILE,  0,  0, 8, 8, 64, 64};
	static const Sprite BLOCK_YELLOW = {DATA_FILE,  8,  0, 8, 8, 64, 64};
	static const Sprite BLOCK_RED    = {DATA_FILE, 16,  0, 8, 8, 64, 64};
	static const Sprite BLOCK_DGREY  = {DATA_FILE, 24,  0, 8, 8, 64, 64};
	static const Sprite BLOCK_LGREY  = {DATA_FILE, 32,  0, 8, 8, 64, 64};
	static const Sprite BLOCK_DARK   = {DATA_FILE, 40,  0, 8, 8, 64, 64};
	static const Sprite BLOCK_ORANGE = {DATA_FILE,  0,  8, 8, 8, 64, 64};
	static const Sprite BLOCK_GREEN  = {DATA_FILE,  8,  8, 8, 8, 64, 64};
	static const Sprite BLOCK_BLUE   = {DATA_FILE, 16,  8, 8, 8, 64, 64};
	static const Sprite BLOCK_CYAN   = {DATA_FILE, 24,  8, 8, 8, 64, 64};

	static const Sprite BACKGROUND   = {DATA_FILE, 0, 16, 20, 20, 64, 64};
	static const Sprite MOUSE_POINTER= {DATA_FILE, 20, 16, 8, 12, 64, 64};
}
#endif // _TETOS_TETOSDATA_H_

