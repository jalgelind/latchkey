#ifndef _TETOS_GAME_H_
#define _TETOS_GAME_H_

#include <Pxf/Base/Timer.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Input/Input.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Util/Configuration.h>
#include <Pxf/Util/Application.h>
#include <Pxf/Resource/Resource.h>
#include <Pxf/Util/Scene.h>
#include <Pxf/Util/Profiler.h>
#include "SceneConfig.h"
#include "TetosInput.h"
#include "TetosEngine.h"

#include <list>

namespace Pxf
{
	namespace Graphics
	{
		class GraphicsDevice;
		class PrimitiveBatch;
		class Texture;
		class Window;
		class Prepared;
	}
	namespace Resource
	{
		class ResourceManager;
		class Font;
		class Json;
		class Sound;
	}

	class Lapp;
}

namespace Tetos
{
	struct GameConfig
	{
		bool smooth_movement;
		int refresh_rate;
		bool skip_intro;
	};

	struct GameAudioIDs
	{
		int rotate_left;
		int rotate_right;
		int harddrop;
		int lineremove;
		GameAudioIDs()
		{
			rotate_left = -1;
			rotate_right = -1;
			harddrop = -1;
			lineremove = -1;
		}
	};

	struct GameResources
	{
		float osc1;
		float osc2;
		std::shared_ptr<Pxf::Resource::Image> tex_image_;
		std::shared_ptr<Pxf::Resource::Font> gamefont_;
		std::shared_ptr<Pxf::Resource::Font> font_;
		std::shared_ptr<Pxf::Graphics::TextProvider> textp_;
		std::shared_ptr<Pxf::Graphics::TextProvider> textpd_;
		std::shared_ptr<Pxf::Graphics::Texture> tex_;
		std::shared_ptr<Pxf::Resource::Sound> sndrotL;
		std::shared_ptr<Pxf::Resource::Sound> sndrotR;
		std::shared_ptr<Pxf::Resource::Sound> sndHD;
		std::shared_ptr<Pxf::Resource::Sound> sndLR;
		GameResources()
		{
			gamefont_ = 0;
			textp_ = 0;
			tex_ = 0;
			tex_image_ = 0;
			sndrotL = 0; sndrotR = 0;
			sndHD = 0; sndLR = 0;
			font_ = 0;
			textpd_ = 0;
		}
	};

	class RotationSystem;
	class TetosRenderer;
	class Player;

	class Game : public Pxf::Application
	{
	public:
		// Inherited:
	    // kernel_, snd_, gfx_, inp_, res_, logger_, cfg_, scene_manager_
		std::string default_gamepad;
		std::string default_mididevice;
		Pxf::Input::Gamepad* gamepad_;
		Pxf::Input::MidiDevice* mididev_;

		// Configurable settings
		InputProcessor* inputprocessor_;
		Timing timing_;
		GameConfig gamecfg_;
		RotationSystem* rotsys_;

		// Scenes
		Pxf::Scene* scene_game_;
		Pxf::Scene* scene_intro_;
		Pxf::Scene* scene_outro_;
		Tetos::SceneConfig* scene_config_;

		// Game resources
		GameResources gameres_;
		std::shared_ptr<Pxf::Graphics::PrimitiveBatch> pb_;

		GameAudioIDs audioid;
		
		bool showdebug;

		Game(Pxf::Kernel* _Kernel);
		~Game();

		bool SetupConfiguration();
		bool SetupWindow();
		bool SetupTextures();
		bool SetupAudio();
		bool SetupInput();
		bool SetupUI();

		bool OnInit();
		void Reboot() {};

		void OnResize(int x, int y);
		void OnMove(int x, int y);
		void OnFocus(bool focus);
		void OnShutdown();

		void DoInput();
		void CheckForReloadedResources();
		void DoUpdate(unsigned int framenum);

		void DoRender(float _Interpolation);

		// Debug text batch
		std::shared_ptr<Pxf::Graphics::TextProvider::Batch> textbatch_;

		struct ProbeUserData
		{
			std::shared_ptr<Pxf::Graphics::TextProvider::String> name;
			std::shared_ptr<Pxf::Graphics::TextProvider::String> field_a;
			std::shared_ptr<Pxf::Graphics::TextProvider::String> field_b;
			std::shared_ptr<Pxf::Graphics::TextProvider::String> field_c;
			std::shared_ptr<Pxf::Graphics::TextProvider::String> field_d;
		};

		std::shared_ptr<Pxf::Graphics::TextProvider::String> debug_text_;
		int name_maxwidth_;
		int tmp_iter_;
		int tmp_x_;
		int tmp_y_;

		void RefreshProbes(Pxf::Profiler::ProbeNode& node, int indent = 0);
		void RefreshDebugInfo();
		void DoRenderDebug();

	};
}

#endif //_TETOS_GAME_H_

