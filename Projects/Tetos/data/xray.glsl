varying vec3 N;
varying vec3 I;
varying vec4 Cs;

#vertexshader

void main()
{
	vec4 P = gl_ModelViewMatrix * gl_Vertex;
	I  = P.xyz - vec3 (0);
	N  = gl_NormalMatrix * gl_Normal;
	Cs = gl_Color;
	gl_Position = ftransform(); //gl_ModelViewProjectionMatrix * gl_Vertex;	
}


#fragmentshader
uniform sampler2D tex;
// entry point
void main()
{
	float edgefalloff = 0.6;
    float opac = dot(normalize(-N), normalize(-I));
    vec4 aoeu;
    opac = abs(opac);
    opac = 1.0-pow(opac, edgefalloff);
    //opac = 1.0 - opac;
    
   // gl_FragColor =  opac * Cs;
   // gl_FragColor.a = opac;
   // vec4 d = texture2D(tex, gl_FragCoord.st);
    vec4 z = texture2D(tex, Cs.xy);
	aoeu = vec4(opac * Cs.a, opac * Cs.g, opac * Cs.b, opac);
	gl_FragData[0] = aoeu;
    gl_FragData[1] = z;//vec4(1) - aoeu;
}

