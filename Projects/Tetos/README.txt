Tetos - cross-platform tetromino game
---------------------------------------
Features
    * Hold (remembers rotation)
    * Supports initial hold state (IHS) and initial rotation state (IRS).
    * Uses the super rotation system (SRS).
    * Tweakable timing values.
    * Bugs.
    * TGM2+ like gravity.
    * Bugs.
    * Poor support for the Xbox360 controller.

Controls
--------
    default controls are for sv_dvorak users :)

    esc = quit

    a = move left
    e = move right
    , = hard drop
    o = soft drop
    � = hold
    arrow left = rotate left
    arrow right = rotate right


    F1 - Load replay
    F2 - Restart game
    F3 - Show profiler
    F4 - Dump screenshot
    F7 - Disable logger flushing
    F8 - Toggle file logger
    F9 - Toggle stdout logger
    F10 - Configure keyboard
    F11 - Configure gamepad
    F12 - Scan for newly connected devices (gamepads etc)