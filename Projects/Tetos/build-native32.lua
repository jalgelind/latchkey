-- BUILD_TARGET("Release DLL", "runbam release_dll", "{project_name}_sr.exe")
-- BUILD_TARGET("Debug DLL", "runbam debug_dll", "{project_name}_d.exe")
-- BUILD_TARGET("Static Release", "runbam release", "{project_name}_sr.exe")
-- BUILD_TARGET("Static Debug", "runbam debug", "{project_name}_sd.exe")
Import("../../Builder.lua")

project = NewProject("tetos")
project:RequireLibrary("jsoncpp")
--if platform == "linux" then
    --project:AddDefine("CONF_SKIP_GTK_DEPS")
--else
    project:RequireLibrary("sstat")
--end
project:RequireLibrary("lua")
project:RequireModule("dgfx")
project:RequireModule("img")
project:RequireModule("snd")
project:RequireModule("mesh")
project:RequireModule("auxinput")
project:AddIncludeDirectory("include/")
--project:AddIncludeDirectory("../Lapp/include")
--project:AddSourceDirectory("../Lapp/source/*.cpp")
project:AddSourceDirectory("source/*.cpp")
if family == "unix" then
	project.settings.cc.flags:Add("-m64")
end
project:Build()
