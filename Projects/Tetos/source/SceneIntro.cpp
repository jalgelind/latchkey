#include <Pxf/Base/Utils.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/PrimitiveBatch.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Resource/Font.h>

#include "TetosRotation.h"
#include "TetosRenderer.h"
#include "SceneIntro.h"
#include "TetosEngine.h"
#include "Game.h"

#include <stdio.h>

using namespace Pxf;
using namespace Graphics;
using namespace Tetos;

SceneIntro::SceneIntro(Game& game)
	: Scene(game, "intro")
	, game_(game)
	, pb_(game.pb_)
	, tex_(game.gameres_.tex_)
	, textprovider_(game.gameres_.textp_)
	, curve_()
{
	textstring_ = textprovider_->CreateString("hello");
	textprovider_->DefineColor("intro", 0, 0, 0);
	textprovider_->DefineColor("intro2", 0, 0, 0);
	textstring_->SetDefaultColor("intro");

	easings_.push_back(new EasingBack(0, 1, 5));
	easings_.push_back(new EasingBounce(0, 1, 5));
	easings_.push_back(new EasingCirc(0, 1, 5));
	easings_.push_back(new EasingCubic(0, 1, 5));
	easings_.push_back(new EasingElastic(0, 1, 5, 1, 1));
	easings_.push_back(new EasingExponential(0, 1, 5));
	easings_.push_back(new EasingLinear(0, 1, 5));
	easings_.push_back(new EasingQuad(0, 1, 5));
	easings_.push_back(new EasingQuart(0, 1, 5));
	easings_.push_back(new EasingQuint(0, 1, 5));
	easings_.push_back(new EasingSine(0, 1, 5));
};

SceneIntro::~SceneIntro()
{
}

void SceneIntro::Init()
{
	interpol_.Clear();
	interpol_.RegisterFadeIn<EasingLinear>(&fade_color_, 0.f, 1.f,  0.f, 0.85f);
	interpol_.RegisterFadeIn<EasingLinear>(&fade_color_, 1.f, 2.f,  0.85f, 0.9f);
	interpol_.RegisterFadeIn<EasingLinear>(&fade_color_, 2.f, 3.f,  0.9f, 1.f);
	interpol_.RegisterFadeOut<EasingLinear>(&fade_color_,3.f, 4.f, 1.f, 0.f);
	interpol_.RegisterFadeInOut<EasingExponential>(&fade_pos_x_, 2.f,  4.f, 0.f, 1.f);
	interpol_.RegisterFadeOut<EasingQuint>(&fade_pos_y_, 0.f, 3.5f,  0.f,  1.f);

	interpol_.RegisterFadeOut<EasingExponential>(&fade_curve_, 0.f, 4.f,  .3f,  1.f);
	interpol_.RegisterFadeOut<EasingQuint>(&fade_curve_, 5.5f, 7.f,  1.f,  0.f);

	curve_.points_.clear();
	curve_.Add(glm::vec3(1,     0, 0));
	curve_.Add(glm::vec3(1,     0, 0));
	curve_.Add(glm::vec3(0.57f,  0, 0));
	curve_.Add(glm::vec3(0.97f,  0, 0));
	curve_.Add(glm::vec3(0.82f,  0, 0));
	curve_.Add(glm::vec3(0.8f,  0, 0));
	curve_.Add(glm::vec3(0.85f, 0, 0));
	curve_.Add(glm::vec3(0.92f, 0, 0));
	curve_.Add(glm::vec3(0.7f, 0, 0));
	curve_.Add(glm::vec3(0.9f, 0, 0));
	curve_.Add(glm::vec3(1.f, 0, 0));
}

void SceneIntro::Reset()
{

}

void SceneIntro::DoInput()
{
	Input::Keyboard::KbdQ& charQ = inp_.GetKeyboard().GetCharQueue();
	Input::Keyboard::KbdQ& keyQ = inp_.GetKeyboard().GetKeyQueue();

	Input::Keyboard::Event ev = keyQ.Peek();
	if (ev.IsPressed())
	{
		if(ev.code == Input::ESC)
			game_.scene_manager_.SetScene(game_.scene_game_);
		else if (ev.code == Input::SPACE)
		{
			Init();
			scene_start_ = app_.running_time_;
			scene_stop_ = app_.running_time_ + scene_duration_;
		}
	}
	kbd_.ClearQueues();
}


void SceneIntro::DoUpdate(unsigned int framenum)
{
}

static Graphics::Rect centerstr(TextProvider::String& label, Rect& root)
{
	Rect cent = label.GetQuads().rect;
	root.Center(cent);
	return cent;
}

void SceneIntro::DoRender(float interpolation)
{
	float winh = gfx_.GetActiveWindow()->GetHeight();
	float winw = gfx_.GetActiveWindow()->GetWidth();

	int glidepos_y = 100.f * (1.f - fade_pos_y_);
	int glidepos_x = 20.f * (1.f - fade_pos_x_);

	int h = game_.gameres_.gamefont_->GetInfo().line_height + 3;
	int yoffset = - 38 - glidepos_y;
	int xoffset = -glidepos_x;
	
	float sync = app_.running_time_ - (int)app_.running_time_;
	float rts = app_.running_time_ - sync;

	Rect root = game_.root_;

	float osc = sin(app_.running_time_-scene_start_) * 0.5f + 0.5f;
	float osc7 = sin(app_.running_time_-scene_start_ * 7)  * 0.5f + 0.5f;

	std::string str;
	std::string introtxt = "<intro>PIXEL</intro> <intro2>FOLDERS</intro2>";
	Rect centered_rect = centerstr(*textstring_, root);

	// Test easing
	float ypos = centered_rect.y - 30 + 400 * (1-fade_pos_y_);
	float ydots = centered_rect.y - 30;
	float ydotsi = winh - ydots;

	float xdots = 100 * fade_color_;
	float xdotsi = winw - xdots;
	
	gfx_.SetPointSize(2);
	pb_->PointsBegin();
	for(int i = 0; i < easings_.size(); i++)
	{
		int y = ydots - 100 + (50 + (float(i) / easings_.size()) * i);

		float mod = TransformRange<float>(sin(app_.running_time_ + (i/easings_.size())*glm::pi<float>()), 0, 1, 0.5f, 0.8f); 
		glm::vec3 bzc = curve_.GetBezier(i/easings_.size());
		glm::vec3 cc = curve_.GetCatmullRom(i/easings_.size());

		// -->
		pb_->SetColor(osc7 * osc * float(i) / easings_.size(), mod*osc, 1.f/i, fade_color_ * fade_curve_);
		pb_->PointsDraw(xdots + 440*easings_[i]->EaseIn(app_.running_time_ - scene_start_), y + bzc.x*mod);
		pb_->PointsDraw(xdots + 440*easings_[i]->EaseOut(app_.running_time_ - scene_start_), y + 2 + bzc.x*mod);
		pb_->PointsDraw(xdots + 440*easings_[i]->EaseInOut(app_.running_time_ - scene_start_), y + 4 + bzc.x*mod);
		
		// <--
		pb_->SetColor(osc7 * mod* osc * float(i) / easings_.size(), osc, 1.f/i, fade_color_ * fade_curve_);
		pb_->PointsDraw(xdotsi - 440*easings_[easings_.size()-1-i]->EaseIn(app_.running_time_ - scene_start_), y + cc.x*mod);
		pb_->PointsDraw(xdotsi - 440*easings_[easings_.size()-1-i]->EaseOut(app_.running_time_ - scene_start_), y + 2 + cc.x*mod);
		pb_->PointsDraw(xdotsi - 440*easings_[easings_.size()-1-i]->EaseInOut(app_.running_time_ - scene_start_), y + 4 + cc.x*mod);
		

	}
	pb_->PointsEnd();


	// Test the curves

	float xoff = 100 * fade_color_;
	float yoff = 70;

	float r, g, b, a;

	app_.gfx_.SetPointSize(2.f);
	//app_.gfx_.SetLineWidth(2.f);
	pb_->LinesBegin();
	for(int i = 0; i < 640*2; i++)
	{
		float ii = 640*2.f;
		float pos = i / ii;
		float posn = (i+1) / ii;

		float mod = sin(-glm::pi<float>()/2 + (app_.running_time_-scene_start_)*2 + pos*glm::pi<float>() * 2);
		float mod2 = TransformRange<float>(mod, -1, 1, 0, 1); 
		float modi = cos(-glm::pi<float>()/2 - app_.running_time_-scene_start_ + pos*glm::pi<float>() * 2);
		float mod2i = TransformRange<float>(modi, -1, 1, 0, 1); 

		float osci = osc * -1;
		float osc2 = ((i*osc*mod)/(640.f*2))*fade_color_;

		float alphamod = sin(pos*glm::pi<float>()*2 + (app_.running_time_-scene_start_));
		float xpos = pos*(centered_rect.w+200);
		
		float bc = curve_.GetBezier(pos).x;
		float bn = curve_.GetBezier(posn).x;
		r = mod, g = mod*(1.f-osc), b = mod*osc2;
		a = alphamod * Clamp<float>(fade_curve_ * mod, 0.8, 1) * fade_curve_;

		// fade edges
		const float edge = 0.1;
		if (pos < edge)
		{
			float fade = (pos/edge);
			a *= fade; r *= fade; g *= fade; b *= fade;
		}
		else if (pos > (1.f - edge))
		{
			float fade = ((1.f - pos)/edge);
			a *= fade; r *= fade; g *= fade; b *= fade;
		}

		pb_->SetColor(r, g, b, a);
		pb_->LinesDraw( centered_rect.x - 100 + xpos,   ydots + 20 + bc * 30
			         , centered_rect.x - 100 + xpos+2, ydots + 20 + bn * 30);
	}
	pb_->LinesEnd();


	// Update text colors.

	r = (fade_color_ * osc);
	g = 1 - (fade_color_ * (1-osc));
	b = 1 - (1-osc)*(fade_pos_y_);
	r *= fade_color_; g*= fade_color_; b *= fade_color_;
	textprovider_->DefineColor("intro", r * 255, g * 255, b * 255);

	r = 1.f - (fade_color_ * osc);
	g = (fade_color_ * (1-osc));
	b = 1.f - osc*(fade_pos_y_);
	r *= fade_color_; g*= fade_color_; b*= fade_color_;
	textprovider_->DefineColor("intro2", r * 255, g * 255, b * 255);

	textstring_->Set(introtxt);
	textstring_->Print(centered_rect.x + xoffset, centered_rect.y + yoffset, 1.f + GetSceneProgress()*0.1);
}
