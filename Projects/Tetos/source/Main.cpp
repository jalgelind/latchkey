#include <Pxf/Pxf.h>
#include <Pxf/Base/Platform.h>
#include "Game.h"

// http://www.tetrisconcept.com/wiki/index.php?title=Random_Generator
// http://bsixcentdouze.free.fr/tc/tgm-en/tgm.html

// Board should have a hash method, for all blocks: sum += y*x*block_id
// A board in assigned to a player. The board is managed by the server.
// All player moves are performed at the client, but are also verified by the
// server.

// different render states for gameplay / fluffy row-remove / game over / etc

// Powerups?
// - Undo => Undo the placing of the last tetromino
// - Bomb => Blasts 4 blocks wide, and up (or down?)

#include <cstdio>

#include <Pxf/Base/Path.h>
#include <Pxf/Resource/ResourceManager.h>
//#include "../PreloadedResources.h"

using namespace Pxf;
using Tetos::Game;

int main(int argc, const char** argv)
{
	//try
	//{
		Pxf::Init(argc, argv);
		
		Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();
		Pxf::Logger logger = kernel->GetLogger("init");

		for(int i = 0; i < argc; i++)
		{
			if (std::string(argv[i]) == "-expand")
			{
				kernel->GetResourceManager()->ExpandFileCache();
				logger.Information("Bye bye.");
				return 0;
			}
		}
		
		Pxf::InitModules();

		logger.Information("Initializing Tetos.");
		{
			Game g(kernel);
			if (g.Init())
			{
				g.Run();
			}
			else
				logger.Error("Failed to initialize Tetos.");
		}
		logger.Information("Puss puss.");
	//}
	/*
	catch(...)
	{
		Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();
		Pxf::Logger logger = kernel->GetLogger("crash");

		std::vector<std::string> stacktrace = Platform::GetStacktrace();
		for(int i = 0; i < stacktrace.size(); i++)
			logger.Error("%s", stacktrace[i].c_str());
	}
	*/

	Pxf::Shutdown();
	return 0;
}

