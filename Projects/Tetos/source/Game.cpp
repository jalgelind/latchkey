#include "Game.h"

#include <Pxf/Base/Base.h>
#include <Pxf/Base/TaskPool.h>
#include <Pxf/Audio/Audio.h>
#include <Pxf/Graphics/Graphics.h>
#include <Pxf/Input/Input.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Resource/Resource.h>
#include <Pxf/Resource/GLSL.h>

#include <Pxf/Modules/dgfx/OpenGL.h>

#include <cstdio>

#include "TetosRenderer.h"
#include "TetosData.h"
#include "TetosRotation.h"
#include "SceneGame.h"
#include "SceneIntro.h"

using namespace Pxf;
using namespace Pxf::Input;
using namespace Pxf::Graphics;

using Pxf::Resource::Sound;
using Pxf::Resource::Image;
using Pxf::Resource::GLSL;

using namespace Tetos;

Game::Game(Pxf::Kernel* kernel)
		: Application(kernel, "game")
		, rotsys_(0)
		, pb_(0)
		, scene_intro_(0)
		, scene_game_(0)
		, scene_outro_(0)
		, scene_config_(0)
		, gamepad_(0)
		, inputprocessor_(0)
		, textbatch_(0)
		, name_maxwidth_(0)
{
	showdebug = false;
};

Game::~Game()
{
	if (rotsys_)
		delete rotsys_;
}

bool Game::SetupConfiguration()
{
	// Default settings
	cfg_.Set("audio.buffersize", 1024);
	cfg_.Set("audio.max_voices", 8);
	cfg_.Set("input.keys.move_left", "A");
	cfg_.Set("input.keys.move_right", "E");
	cfg_.Set("input.keys.rotate_left", "LEFT");
	cfg_.Set("input.keys.rotate_right", "RIGHT");
	cfg_.Set("input.keys.soft_drop", "O");
	cfg_.Set("input.keys.hard_drop", "COMMA");
	cfg_.Set("input.keys.hold", "APOSTROPHE");
	cfg_.Set("input.gamepad", "Controller (XBOX 360 For Windows)");
	cfg_.Set("input.mididevice", "Oxygen 25");
	cfg_.Set("win.width", 640);
	cfg_.Set("win.height", 480);
	cfg_.Set("win.vsync", false);
	cfg_.Set("ui.font", "data/calibri24.pfnt");
	cfg_.Set("ui.debugfont", "data/tempesta12compressed.pfnt");
	cfg_.Set("timing.auto_shift_speed", 2, "frames");
	cfg_.Set("timing.delayed_auto_shift", 14, "frames");
	cfg_.Set("timing.soft_drop_speed", 2, "frames");
	cfg_.Set("timing.hard_drop_speed", 60, "rename: time until re-dropping the next piece");
	cfg_.Set("timing.lock_delay", 30);
	cfg_.Set("timing.line_clear_delay", 40);
	cfg_.Set("timing.are", 25, "spawn delay");
	cfg_.Set("game.mode", "SRS", "SRS or Simple");
	cfg_.Set("game.refresh-rate", 60, "hz");
	cfg_.Set("game.smooth-movement", true);
	cfg_.Set("game.skip-intro", false);
	cfg_.Set("engine.threads", Platform::GetNumberOfProcessors(), "should be equal to #cores on your machine. on *nix this value might need to be adjusted as it can include virtual HT-cores.");

	cfg_.MarkAsFallback();

	// Overwrite defaults with config file
	if (!cfg_.Load("config.json", true))
		logger_.Information("Created a new configuration file with default settings.");

	cfg_.Save("config.json");
	return true;
}

bool Game::SetupWindow()
{
	Graphics::WindowDetails spec;
	spec.Width = window_width_;
	spec.Height = window_height_;
	spec.XPos = cfg_.GetInt("win.xpos");
	spec.YPos = cfg_.GetInt("win.ypos");
	spec.ColorBits = 24;
	spec.AlphaBits = 8;
	spec.DepthBits = 8;
	spec.StencilBits = 0;
	spec.FSAASamples = 0;
	spec.Fullscreen = false;
	spec.Resizeable = false;
	spec.VerticalSync = cfg_.GetBool("win.vsync");

	root_.x = 0;
	root_.y = 0;
	root_.h = window_height_;
	root_.w = window_width_;

	wnd_ = gfx_.OpenWindow(spec);

	if (!wnd_)
	{
		logger_.Error("Could not create OpenGL context. Is hardware rendering available?");
		return false;
	}

	wnd_->SetTitle("2012/pxf");
	prjmtx_ = glm::ortho(0.f, (float)window_width_, (float)window_height_, 0.f, -10.f, 10.f);
	gfx_.SetProjectionMatrix(prjmtx_);
	gfx_.SetViewport(0, 0, window_width_, window_height_);

	return true;
}

bool Game::SetupTextures()
{
	pb_ = gfx_.CreatePrimitiveBatch();
	if (gameres_.tex_image_)
	{
		gameres_.tex_ = gfx_.CreateTexture(gameres_.tex_image_);
		if (gameres_.tex_->IsValid())
		{
			gameres_.tex_->SetMagFilter(TEX_FILTER_NEAREST);
			gameres_.tex_->SetMinFilter(TEX_FILTER_NEAREST);
			gameres_.tex_image_->Unload();
		}
		else
		{
			logger_.Error("Hardware texture seems to be invalid. No game for you.");
			return false;
		}
	}
	else
	{
		logger_.Error("The game texture is either missing or damaged.");
		return false;
	}
	return true;
}

bool Game::SetupAudio()
{
	snd_.SetPreferredBufferSize(cfg_.GetInt("audio.buffersize"));
	snd_.SetMaxActiveVoices(cfg_.GetInt("audio.max_voices"));

	if (res_.CanLoad("ogg"))
	{
		if (gameres_.sndrotL && 
			gameres_.sndrotR && 
			gameres_.sndHD && 
			gameres_.sndLR)
		{
			audioid.rotate_left = snd_.RegisterSound(gameres_.sndrotL);
			audioid.rotate_right = snd_.RegisterSound(gameres_.sndrotR);
			audioid.harddrop = snd_.RegisterSound(gameres_.sndHD);
			audioid.lineremove = snd_.RegisterSound(gameres_.sndLR);
			return true;
		}
	}
	else
	{
		logger_.Error("No loader for ogg-files available.");
	}

	return false;
}

bool Game::SetupInput()
{
	inp_.DiscoverDevices();
	kbd_.SetSystemKeys(true);
	kbd_.SetKeyRepeat(false);

	if (inp_.GetGamepadCount() == 1)
	{
		std::string newdescr = inp_.GetGamepad(0).GetDescription();
		std::string olddescr = cfg_.GetString("input.gamepad", "No Controller");
		if (newdescr != olddescr)
		{
			cfg_.SetString("input.gamepad", newdescr.c_str());
			cfg_.Save("config.json");
		}
	}

	default_gamepad = cfg_.GetString("input.gamepad");
	default_mididevice = cfg_.GetString("input.mididevice");
	gamepad_ = &inp_.GetGamepad(default_gamepad);
	mididev_ = &inp_.GetMidiDevice(default_mididevice);

	inputprocessor_ = new InputProcessor(kbd_, *gamepad_);
	inputprocessor_->Load(cfg_, "input");

	return true;
}

bool Game::SetupUI()
{
	if (!gameres_.gamefont_ || !gameres_.font_)
		return false;
	
	gameres_.textp_ = gfx_.CreateTextProvider(gameres_.gamefont_, 16);
	gameres_.textp_->SetTextureFilters(TEX_FILTER_LINEAR_MIPMAP_LINEAR, TEX_FILTER_LINEAR);

	gameres_.textpd_ = gfx_.CreateTextProvider(gameres_.font_, 16);
	gameres_.textpd_->SetTextureFilters(TEX_FILTER_NEAREST, TEX_FILTER_NEAREST);
	gameres_.textpd_->SetDefaultColor("white");

	textbatch_ = gameres_.textpd_->CreateBatch();
	debug_text_ = textbatch_->CreateString(0, 0, 512, "test");

	gameres_.gamefont_->AddDependency(gameres_.textp_);
	gameres_.font_->AddDependency(gameres_.textpd_);
	return true;
}

bool Game::OnInit()
{
	logger_ = kernel_.GetLogger("game");

	if (!SetupConfiguration())
		return false;

	gameres_.gamefont_ = res_.Acquire<Resource::Font>(cfg_.GetString("ui.font").c_str());
	gameres_.font_ = res_.Acquire<Resource::Font>(cfg_.GetString("ui.debugfont").c_str());
	gameres_.tex_image_ = res_.Acquire<Image>("data/data.png");
	gameres_.sndrotL = res_.Acquire<Resource::Sound>(SND_ROTATE_L_FILE);
	gameres_.sndrotR = res_.Acquire<Resource::Sound>(SND_ROTATE_R_FILE);
	gameres_.sndHD = res_.Acquire<Resource::Sound>(SND_HARDDROP_FILE);
	gameres_.sndLR = res_.Acquire<Resource::Sound>(SND_LINEREMOVE_FILE);

	window_width_  = cfg_.GetInt("win.width");
	window_height_ = cfg_.GetInt("win.height");
	timing_.asm_delay = cfg_.GetInt("timing.auto_shift_speed");
	timing_.das_delay = cfg_.GetInt("timing.delayed_auto_shift");
	timing_.sds_delay = cfg_.GetInt("timing.soft_drop_speed");
	timing_.hds_delay = cfg_.GetInt("timing.hard_drop_speed");
	timing_.are_delay = cfg_.GetInt("timing.are");
	timing_.piecelock_delay = cfg_.GetInt("timing.lock_delay");
	timing_.lineclear_delay = cfg_.GetInt("timing.line_clear_delay");

	gamecfg_.refresh_rate = cfg_.GetInt("game.refresh-rate");
	refresh_rate_ = gamecfg_.refresh_rate; // todo: redundant
	gamecfg_.smooth_movement = cfg_.GetBool("game.smooth-movement");
	gamecfg_.skip_intro = cfg_.GetBool("game.skip-intro");

	if (IsEqualI(cfg_.GetString("game.mode"), "SRS"))
	{
		rotsys_ = new SuperRotationSystem();
		logger_.Information("Using super rotation system");
	}
	else
	{
		rotsys_ = new SimpleRotationSystem();
		logger_.Information("Using simple rotation system");
	}

	if (!SetupAudio())
	{
		logger_.Error("Audio failed to initialize properly.");
	}

	if (!SetupWindow())
		return false;
	
	if (!SetupInput())
		return false;

	if (!SetupTextures())
		return false;

	if (!SetupUI())
		return false;
	
	//GLSL* glslsrc = res_.Acquire<GLSL>("data/xray.glsl");
	//Shader* shdr = gfx_.CreateShader("xrayshdr", glslsrc);
	//gfx_.DestroyShader(shdr);

	scene_game_ = new SceneGame(*this);
	scene_intro_ = new SceneIntro(*this);
	scene_intro_->SetTimedScene(0.f, 7.f);

	// todo: should triggered when quitting.
	scene_outro_ = new SceneIntro(*this);
	scene_outro_->SetTimedScene(12.f, 15.f);

	// configuration scene
	scene_config_ = new SceneConfig(*this);


	scene_manager_.AddScene(scene_intro_);
	//scene_manager_.AddScene(scene_outro_);
	scene_manager_.AddScene(scene_game_);

	if (gamecfg_.skip_intro)
		scene_manager_.SetScene(scene_game_);

	gfx_.SetBlendNormal();
	gfx_.SetCullFace(false);
	gfx_.SetDepthTest(false);
	gfx_.Clear(0.f, 0.f, 0.f, 1.f);

	logger_.Information("misc bindings:");
	logger_.Information(" F1 - Load game");
	logger_.Information(" F2 - Restart game");
	logger_.Information(" F3 - Show profiler");
	logger_.Information(" F4 - Dump screenshot");

	logger_.Information(" F7 - Disable logger flushing");
	logger_.Information(" F8 - Toggle file logger");
	logger_.Information(" F9 - Toggle stdout logger");
	logger_.Information("F10 - Configure keyboard");
	logger_.Information("F11 - Configure gamepad");
	logger_.Information("F12 - Detect new devices");

	res_.DumpMemoryUsage();
	return true;
}

void Game::OnResize(int x, int y)
{
	(void)0;
}

void Game::OnMove(int x, int y)
{
	(void)0;
}

void Game::OnFocus(bool focus)
{
	if (focus)
		logger_.Debug("game is in focus");
	else
		logger_.Debug("game not in focus");
}

void Game::OnShutdown()
{

}

void Game::DoInput()
{
	Input::Keyboard::KbdQ& keyQ = inp_.GetKeyboard().GetKeyQueue();
	inp_.Update();

	// Exit. should set STATE = MENU
	Input::Keyboard::Event last_key = keyQ.Peek();
	if (last_key.IsPressed())
	{
		if (last_key.code == Input::F12)
		{
			SetupInput();
			keyQ.Pop();
		}
		else if (last_key.code == Input::F3)
		{
			showdebug = !showdebug;
			keyQ.Pop();
		}
		else if (last_key.code == Input::F4)
		{
			gfx_.GetActiveWindow()->Swap();
			auto tex = gfx_.CreateTextureFromFramebuffer();
			auto img = gfx_.CreateImageFromTexture(tex);
			img->SaveAs("screenshot.png");
			keyQ.Pop();
		}
		else if (last_key.code == Input::F10)
		{
			scene_config_->PrepareState(SceneConfig::CONFIG_KEYBOARD);
			scene_manager_.SetScene(scene_config_);
			keyQ.Pop();
		}
		else if (last_key.code == Input::F11)
		{
			scene_config_->PrepareState(SceneConfig::CONFIG_GAMEPAD);
			scene_manager_.SetScene(scene_config_);
			keyQ.Pop();
		}
	}
	scene_manager_.GetCurrentScene()->DoInput();
	//kbd_.ClearLastKey();
	//kbd_.ClearLastChar();
	mouse_.ClearLastButton();
}

void Game::DoUpdate(unsigned int framenum)
{
	DoInput();
	if (showdebug && framenum == 1)
	{
		RefreshDebugInfo();
	}

	PXF_PROFILE("app.update.game");
	Scene* scene = scene_manager_.GetCurrentScene();
	if (scene)
		scene->_DoUpdate(framenum);

	if (just_switched_scene_)
		logger_.Debug("scene switch.");
}

void Game::DoRender(float interpolation)
{
	PXF_PROFILE("app.render.game");

	// not needed for this.
	(void) interpolation;

	gfx_.Clear(0.f, 0.f, 0.f);
	gfx_.SetProjectionMatrix(prjmtx_);
	gfx_.SetViewport(0, 0, window_width_, window_height_);

	scene_manager_.GetCurrentScene()->_DoRender(interpolation);
	
	if (showdebug)
		DoRenderDebug();

}


//
// Debugstuff
//

void Game::RefreshProbes(Profiler::ProbeNode& node, int indent)
{
	int lh = gameres_.font_->GetInfo().line_height;
	if (node.name != "")
	{
		ProbeUserData* ud = (ProbeUserData*)node.userdata;
		// Create userdata for node
		if (!node.userdata)
		{
			// Position will be updated later when rendering?
			ud = new ProbeUserData; // TODO: delete this in destructor
			ud->name = textbatch_->CreateString(0, 0, 16, node.name);
			if (node.probe)
			{
				ud->field_a = textbatch_->CreateString(0, 0, 16, "");
				ud->field_b = textbatch_->CreateString(0, 0, 16, "");
				ud->field_c = textbatch_->CreateString(0, 0, 16, "");
				ud->field_d = textbatch_->CreateString(0, 0, 16, "");
			}
			node.userdata = (void*)ud;
		}

		ud->name->SetPosition(tmp_x_ + indent*10, tmp_y_ + tmp_iter_*lh);
		ud->name->Set(node.name);

		if (node.probe)
		{
			double usage = ((((node.probe->Average()*(node.probe->calls_per_sec/60))/1000.) / (1.f / gamecfg_.refresh_rate)) * 100.);
			ud->field_a->SetPosition(tmp_x_ + name_maxwidth_ + 90 * 1, tmp_y_ + tmp_iter_*lh);
			ud->field_a->SetF("%.2f ms", node.probe->Average());
			ud->field_b->SetPosition(tmp_x_ + name_maxwidth_ + 90 * 2, tmp_y_ + tmp_iter_*lh);
			ud->field_b->SetF("%02.f %%", usage);
			ud->field_c->SetPosition(tmp_x_ + name_maxwidth_ + 90 * 3, tmp_y_ + tmp_iter_*lh);
			ud->field_c->SetF("%02.f %% avg", node.probe->load);
			ud->field_d->SetPosition(tmp_x_ + name_maxwidth_ + 90 * 4, tmp_y_ + tmp_iter_*lh);
			ud->field_d->SetF("%.0f calls/s", node.probe->calls_per_sec);
		}
		tmp_iter_++;
	}
	if (node.name.size() + indent > name_maxwidth_)
		name_maxwidth_ = node.name.size() + indent;

	std::vector<Profiler::ProbeNode>::iterator itr = node.children.begin();
	for(itr; itr != node.children.end(); itr++)
	{
		Profiler::ProbeNode& n = *itr;
		RefreshProbes(n, indent+1);
	}
}

void Game::RefreshDebugInfo()
{
	PXF_PROFILE("app.update.refreshdebug");
	tmp_iter_ = 0;
	RefreshProbes(Profiler::GetInstance().root);

	float tot = gfx_.GetMemorySize() / (1024 * 1024.f);
	float usg = gfx_.GetMemoryUsage() / (1024 * 1024.f);
	float ded = gfx_.GetDedicatedMemory() / (1024 * 1024.f);
	float evict = gfx_.GetEvictedMemory() / (1024 * 1024.f);
	float tex = gfx_.GetTextureMemoryUsage() / (1024 * 1024.f);
	float vbo = gfx_.GetVBOMemoryUsage() / (1024 * 1024.f);
	float res = res_.GetMemoryUsage()  / (1024 * 1024.f);
	unsigned evictc = gfx_.GetEvictionCount();
	const char *a = gfx_.GetVendor(), *b = gfx_.GetVersion(), *c = gfx_.GetRenderer(), *d = gfx_.GetShaderLangVersion();

	std::string threadinfo = "\nThreads:\n";
	for (auto c: kernel_.GetTaskPool()->get_channels())
	{
		threadinfo += Pxf::Format("%s: %d\n", c->get_name().c_str(), c->get_processed());
	}

	std::string fmt = std::string("") +
		"%s - %s (%s/%s)\n"
		"SYSTEM GLOBAL GPU STATS -- %.1f MB / %.1f MB (%.1f MB ded) / EVICT %.1f MB (# %d)\n" +
		"\n" +
		"SYSTEM RAM:\n" + 
		" Res: %.2f MB \n" +
		"\n"+
		"GPU MEMORY:\n"
		"Tex: %.2f MB \n" + 
		"VBO: %.2f MB \n" +
		"%s";
	std::string str = Pxf::Format(fmt.c_str(), a, c, b, d, usg, tot, ded, evict, evictc, res, tex, vbo, threadinfo.c_str());
	debug_text_->SetPosition(10, 5);
	debug_text_->SetF(fmt, a, c, b, d, usg, tot, ded, evict, evictc, res, tex, vbo, threadinfo.c_str());

}

void Game::DoRenderDebug()
{
	PXF_PROFILE("app.render.debug");

	gfx_.BindTexture(0);
	pb_->QuadsBegin();
	pb_->SetColor(0, 0, 0, 1);
	pb_->QuadsDrawTopLeft(root_);

	int lh = gameres_.font_->GetInfo().line_height;
	Rect profrect(0, 0, name_maxwidth_*8 + 90*4, tmp_iter_*lh + 10);
	root_.Center(profrect);

	profrect.x += 10;
	profrect.y += 10;

	tmp_x_ = profrect.x + 10;
	tmp_y_ = profrect.y;

	pb_->SetColor(0.1, 0.1, 0.1, 1);
	pb_->QuadsDrawTopLeft(profrect);
	pb_->QuadsEnd();

	textbatch_->Draw();
}
