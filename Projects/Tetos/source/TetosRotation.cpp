#include "TetosRotation.h"
#include "TetosEngine.h"
#include <Pxf/Base/Utils.h>

using namespace Tetos;

// ----------------------------------------------------------------------------------------------------------
// -- Simple Rotation System
// ----------------------------------------------------------------------------------------------------------


void SimpleRotationSystem::Init(Piece& piece)
{
	// The default rotation is SRS
	piece.Set(piece.type);

	// Flip-flop I, S and Z when rotating (avoiding multiple vertical states).
	if (piece.type == Piece::I || piece.type == Piece::S || piece.type == Piece::Z)
		piece.max_rotations = 2;
}

void SimpleRotationSystem::RotateLeft(Piece& piece)
{
	piece.RotateLeft();
}

void SimpleRotationSystem::RotateRight(Piece& piece)
{
	piece.RotateRight();
}

bool SimpleRotationSystem::CanWallKick(TetosLogic& board, Piece& piece)
{
	return true;
}

// TODO: floor kicks?
RotationSystem::RotationResult SimpleRotationSystem::Rotate(TetosLogic& engine, Piece& piece, InputTrigger dir, bool allow_kick)
{
	// http://tetrisconcept.net/wiki/TGM_Rotation

	RotationResult result;
	result.piece = piece;

	if (dir == INPUT_ROTATE_LEFT)
		RotateLeft(result.piece);
	else
		RotateRight(result.piece);

	int px = engine.player_.pos_.x;
	int py = engine.player_.pos_.y;

	// Test in new position, move piece if valid.
	if (engine.board_.CanPutPiece(result.piece, px, py)) // Basic in-place rotation
	{
		result.valid = true;
		return result;
	}

	// Don't wallkick I
	bool can_I_kick_it = allow_kick && piece.type != Piece::I;

	if (can_I_kick_it && engine.board_.CanPutPiece(result.piece, px+1, py))  // 1 step right
	{
		result.valid = true;
		result.offset.x += 1;
	}
	else if (can_I_kick_it && engine.board_.CanPutPiece(result.piece, px-1, py))  // 1 step left
	{
		result.valid = true;
		result.offset.x -= 1;
	}

	return result;
}


// ----------------------------------------------------------------------------------------------------------
// -- SRS Rotation
// ----------------------------------------------------------------------------------------------------------

void SuperRotationSystem::Init(Piece& piece)
{
	piece.Set(piece.type);
	piece.max_rotations = 4;
}

void SuperRotationSystem::RotateLeft(Piece& piece)
{
	piece.RotateLeft();
}

void SuperRotationSystem::RotateRight(Piece& piece)
{
	piece.RotateRight();
}

bool SuperRotationSystem::CanWallKick(TetosLogic& engine, Piece& piece)
{
	int px = engine.player_.pos_.x;
	int py = engine.player_.pos_.y;
	int space = engine.board_.FreeSpaceBelow(piece, px, py);
	if (piece.type == Piece::I && space >= 2)
		return false;
	return engine.board_.CanPutPiece(piece, px,   py+1) == false || 
	       engine.board_.CanPutPiece(piece, px+1, py) == false ||
	       engine.board_.CanPutPiece(piece, px-1, py) == false;
}

RotationSystem::RotationResult SuperRotationSystem::Rotate(TetosLogic& engine, Piece& piece, InputTrigger dir, bool allow_kick)
{
	RotationResult result;
	result.piece = piece;

	if (dir == INPUT_ROTATE_LEFT)
		RotateLeft(result.piece);
	else
		RotateRight(result.piece);
/*
	Rotation state is used for lookups in the Wall Kick table.
	Source: http://tetrisconcept.net/wiki/SRS
	0 = spawn state 
	R = state resulting from a clockwise rotation ("right") from spawn 
	L = state resulting from a counter-clockwise ("left") rotation from spawn 
	2 = state resulting from 2 successive rotations in either direction from spawn.

		The tables:
	J, L, S, T, Z Tetromino Wall Kick Data
			Test 1  Test 2  Test 3  Test 4  Test 5
	0->R    ( 0, 0) (-1, 0) (-1,+1) ( 0,-2) (-1,-2)
	R->0    ( 0, 0) (+1, 0) (+1,-1) ( 0,+2) (+1,+2)
	R->2    ( 0, 0) (+1, 0) (+1,-1) ( 0,+2) (+1,+2)
	2->R    ( 0, 0) (-1, 0) (-1,+1) ( 0,-2) (-1,-2)
	2->L    ( 0, 0) (+1, 0) (+1,+1) ( 0,-2) (+1,-2)
	L->2    ( 0, 0) (-1, 0) (-1,-1) ( 0,+2) (-1,+2)
	L->0    ( 0, 0) (-1, 0) (-1,-1) ( 0,+2) (-1,+2)
	0->L    ( 0, 0) (+1, 0) (+1,+1) ( 0,-2) (+1,-2)

	I Tetromino Wall Kick Data (Arika)
			 Test 1  Test 2  Test 3  Test 4  Test 5
	0->R    ( 0, 0) (-2, 0) (+1, 0) (+1,+2) (-2,-1)
	R->0    ( 0, 0) (+2, 0) (-1, 0) (+2,+1) (-1,-2)
	R->2    ( 0, 0) (-1, 0) (+2, 0) (-1,+2) (+2,-1)
	2->R    ( 0, 0) (-2, 0) (+1, 0) (-2,+1) (+1,-1)
	2->L    ( 0, 0) (+2, 0) (-1, 0) (+2,+1) (-1,-1)
	L->2    ( 0, 0) (+1, 0) (-2, 0) (+1,+2) (-2,-1)
	L->0    ( 0, 0) (-2, 0) (+1, 0) (-2,+1) (+1,-2)
	0->L    ( 0, 0) (+2, 0) (-1, 0) (-1,+2) (+2,-1)

	Encoded states (in order):
	1, 65536, 65538, 131073, 131075, 196610, 196608, 3

	// states: 0 = 0, R = 1, L = 3, 2 = 2. (state = piece.num_rotations_performed % 4)
	uint32 encoder(uint16 state, uint16 next_state) { return (state << 16) | next_state; }
*/


	unsigned short curr_state = (unsigned short)piece.rotations;
	unsigned short next_state = (unsigned short)result.piece.rotations;//(curr_state + (action == TetosLogic::ROTATE_LEFT ? 3 : 1)) % 4;
	unsigned int state = Pxf::PackShort2(curr_state, next_state);

	int indexorder[8] = {1, 65536, 65538, 131073, 131075, 196610, 196608, 3};
	int wallkickindex = -1;
	for(int i = 0; i < 8; i++)
	{
		if (indexorder[i] == state)
		{
			wallkickindex = i;
			break;
		}
	}

	//J, L, S, T, Z Tetromino Wall Kick Data
	int wallkickdata_jlstz[8][5][2] = {
	//    Test 1   Test 2   Test 3   Test 4  Test 5
		{ {0, 0}, {-1, 0}, {-1,+1}, {0,-2}, {-1,-2} },
		{ {0, 0}, {+1, 0}, {+1,-1}, {0,+2}, {+1,+2} },
		{ {0, 0}, {+1, 0}, {+1,-1}, {0,+2}, {+1,+2} },
		{ {0, 0}, {-1, 0}, {-1,+1}, {0,-2}, {-1,-2} },
		{ {0, 0}, {+1, 0}, {+1,+1}, {0,-2}, {+1,-2} },
		{ {0, 0}, {-1, 0}, {-1,-1}, {0,+2}, {-1,+2} },
		{ {0, 0}, {-1, 0}, {-1,-1}, {0,+2}, {-1,+2} },
		{ {0, 0}, {+1, 0}, {+1,+1}, {0,-2}, {+1,-2} }};

	// I Tetromino Wall Kick Data (Arika)
	int wallkickdata_i[8][5][2] = {
		{ { 0, 0}, {-2, 0}, {+1, 0}, {+1,+2}, {-2,-1} },
		{ { 0, 0}, {+2, 0}, {-1, 0}, {+2,+1}, {-1,-2} },
		{ { 0, 0}, {-1, 0}, {+2, 0}, {-1,+2}, {+2,-1} },
		{ { 0, 0}, {-2, 0}, {+1, 0}, {-2,+1}, {+1,-1} },
		{ { 0, 0}, {+2, 0}, {-1, 0}, {+2,+1}, {-1,-1} },
		{ { 0, 0}, {+1, 0}, {-2, 0}, {+1,+2}, {-2,-1} },
		{ { 0, 0}, {-2, 0}, {+1, 0}, {-2,+1}, {+1,-2} },
		{ { 0, 0}, {+2, 0}, {-1, 0}, {-1,+2}, {+2,-1} }};


	if (piece.type == Piece::O /* does not rotate */ || wallkickindex == -1 /* invalid state */)
		return result;

	int px = engine.player_.pos_.x;
	int py = engine.player_.pos_.y;

	// In-place rotation
	if (engine.board_.CanPutPiece(result.piece, px, py))
	{
		result.valid = true;
		return result;
	}

	if (allow_kick && CanWallKick(engine, piece))
	{
		for(int i = 0; i < 5; i++)
		{
			// We flip Y because we have a reversed Y-axis.
			int offset_x, offset_y;
			if (piece.type == Piece::I)
			{
				offset_x = wallkickdata_i[wallkickindex][i][0];
				offset_y = -wallkickdata_i[wallkickindex][i][1];
			}
			else
			{
				offset_x = wallkickdata_jlstz[wallkickindex][i][0];
				offset_y = -wallkickdata_jlstz[wallkickindex][i][1];
			}

			if (engine.board_.CanPutPiece(result.piece, px+offset_x, py+offset_y))
			{
				result.offset.x += offset_x;
				result.offset.y += offset_y;
				result.valid = true;
				return result;
			}
		}
	}
	return result;
}
