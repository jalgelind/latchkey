#include "TetosRenderer.h"

#include <Pxf/Base/Config.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Utils.h>

#include "Game.h"
#include "TetosData.h"
#include "TetosEngine.h"

using namespace Tetos;
using namespace Pxf;

TetosRenderer::TetosRenderer(Game& game)
	: game_(game)
	,  gfx_(game.gfx_)
    ,  pb_(*game.pb_)
    ,  tex_(*game.gameres_.tex_)
{}

TetosRenderer::~TetosRenderer()
{}

void TetosRenderer::_SetBlockSprite(Block::Color color)
{
	PXF_ASSERT(color > Block::None && color <= Block::Cyan, "unknown color! :O");

	static const Sprite* arr[] = {
		0,
		&BLOCK_RED,
		&BLOCK_GREEN,
		&BLOCK_BLUE,
		&BLOCK_DGREY,
		&BLOCK_LGREY,
		&BLOCK_DARK,
		&BLOCK_YELLOW,
		&BLOCK_PURPLE,
		&BLOCK_ORANGE,
		&BLOCK_CYAN
	};

	_SelectSprite(*arr[color]);
}

void TetosRenderer::_SelectSprite(const Sprite& sprite)
{
	int x = sprite.x;
	int y = sprite.y;
	int w = sprite.w;
	int h = sprite.h;
	int tx = sprite.tx;
	int ty = sprite.ty;
	_SetTexCoords(x, y, w, h, tx, ty);
}

void TetosRenderer::_SetTexCoords(int x, int y, int w, int h, int tx, int ty)
{
	float x1 = x/(float)tx;
	float x2 = (x+w)/(float)tx;
	float y1 = y/(float)ty;
	float y2 = (y+h)/(float)ty;
	pb_.QuadsSetTextureSubset(x1, y1, x2, y2);
}

void TetosRenderer::_DrawBlock(Block& b, int x, int y, float opacity)
{
	_SetBlockSprite((Block::Color)b.color);
	pb_.SetColor(1,1,1,opacity);
	pb_.QuadsDrawTopLeft(x, y, 8, 8);
}

void TetosRenderer::DrawPiece(Piece& p, int x, int y, float opacity)
{
	for(int py = 0; py < 4; py++)
	{
		for (int px = 0; px < 4; px++)
		{
			Block b = p.data[py][px];
			if (b.color != Block::None)
				_DrawBlock(b, x + px*8, y + py*8, opacity);
		}
	}
}

void TetosRenderer::DrawBackground()
{
	_SelectSprite(BLOCK_DARK); // BLOCK_DARK works aswell..
	for (float y = 0; y < 480; y += 80)
		for (float x = 0; x < 640; x += 80)
			pb_.QuadsDrawTopLeft(x, y, 80, 80);
}

void TetosRenderer::DrawBoard(Board& b, int x, int y)
{
	for (int by = 0; by < 20; by++)
	{
		for (int bx = 0; bx < 10; bx++)
		{
			if (b.data_[by+2][bx].color != Block::None)
			{
				//_SetBlockSprite(game->pb_, Block::DarkGrey); // (Block::Color)m_Board[y][x]
				_SetBlockSprite((Block::Color)b.data_[by+2][bx].color);

				float o = 1.f - b.data_[by+2][bx].opacity;
				pb_.SetColor(o,o,o,o);

				float px = bx*8; // -1px, share borders
				float py = by*8; // same
				pb_.QuadsDrawTopLeft(x+px, y+py, 8, 8);
			}
		}
	}
}
