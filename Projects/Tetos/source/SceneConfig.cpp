#include <Pxf/Base/Utils.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/PrimitiveBatch.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Resource/Font.h>

#include "TetosRotation.h"
#include "TetosRenderer.h"
#include "SceneConfig.h"
#include "TetosEngine.h"
#include "Game.h"

#include <stdio.h>

using namespace Pxf;
using namespace Graphics;
using namespace Tetos;

SceneConfig::SceneConfig(Game& game)
	: Scene(game, "config")
	, game_(game)
	, pb_(*game.pb_)
	, tex_(*game.gameres_.tex_)
	, font_(*game.gameres_.textp_)
	, str_(0)
	, str_desc_(0)
	, str_configurable_(0)
	, str_key_pressed_(0)
{
	text_ = "";
	text_desc_ = "";
	str_ = font_.CreateString("");
	str_->SetDefaultColor("white");
	str_->Set(text_);

	str_desc_ = font_.CreateString("");
	str_desc_->SetDefaultColor("white");
	str_desc_->Set(text_);

	str_configurable_ = font_.CreateString("");
	str_configurable_->SetDefaultColor("white");
	str_configurable_->Set(text_);

	str_key_pressed_ = font_.CreateString("");
	str_key_pressed_->SetDefaultColor("white");
	str_key_pressed_->Set(text_);

	state_ = NONE;
};

SceneConfig::~SceneConfig()
{
}

void SceneConfig::Init()
{
	interpol_.Clear();
	interpol_.RegisterFadeIn<EasingLinear>(&text_color_, 0.f, 1.f,  0.f, 1.f);
	interpol_.RegisterFadeIn<EasingLinear>(&text_pos_x_, 0.f, 1.f,  0.f, 1.f);

	text_path_.points_.clear();
}

void SceneConfig::Reset()
{

}

void SceneConfig::PrepareState(ConfigState state)
{
	state_ = state;
	kbd_.ClearQueues();
	switch(state)
	{
	case SceneConfig::PROMPT_KEYBOARD_CONFIGURATION:
		text_ = "Would you like to configure your keyboard controls?";
		text_desc_ = "Press [ESC] to skip, or [ENTER] to confirm.";
		break;
	case SceneConfig::PROMPT_GAMEPAD_CONFIGURATION:
		text_ = "Would you like to configure your gamepad controls?";
		text_desc_ = "Press [ESC] to skip, or wiggle the controller to confirm.";
		break;
	case SceneConfig::CONFIG_KEYBOARD:
		text_ = "Keyboard configuration";
		text_desc_ = "Assign new game controls";
		control_config_state_ = SceneConfig::CONFIGURE_MOVE_LEFT;
		configurable = "Configure key for 'Move Left'";
		key_pressed = "LEFT";
		break;
	case SceneConfig::CONFIG_GAMEPAD:
		text_ = "Gamepad configuration";
		text_desc_ = "Assign new game controls";
		control_config_state_ = SceneConfig::CONFIGURE_MOVE_LEFT;
		configurable = "Configure key for 'Move Left'";
		key_pressed = "LEFT";
		break;
	}
	str_->Set(text_);
	str_desc_->Set(text_desc_);
}

void SceneConfig::DoInput()
{
	Input::Keyboard::KbdQ& charQ = inp_.GetKeyboard().GetCharQueue();
	Input::Keyboard::KbdQ& keyQ = inp_.GetKeyboard().GetKeyQueue();
	Input::Gamepad::EventQ& padQ = pad_.GetEventQueue();

	Input::Keyboard::Event ev = keyQ.Peek();
	Input::Gamepad::Event padev = padQ.Peek();

	if (!ev.IsValid() && !padev.IsValid())
		return;

	if (ev.IsPressed() || padev.IsPressed() || padev.IsAxisDown() || padev.IsAxisUp())
	{
		if(ev.code == Input::ESC)
			game_.scene_manager_.SetScene(game_.scene_game_);
			// TODO: scene_manager_.Pop()
	}
	else
	{
		// Do not process release-events.
		keyQ.Pop();
		padQ.Pop();
		return;
	}


	ev = keyQ.Pop();
	Input::Keyboard::Event evc = charQ.Pop();
	padev = padQ.Pop();
	if (ev.IsPressed() || padev.IsPressed())
	{
		if (state_ == CONFIG_KEYBOARD)
		{
			logger_.Debug("kbd-ev: %d - state: %d", ev.code, control_config_state_);
			key_pressed = kbd_.GetKeyName(ev.code);
			if (key_pressed == "???" && evc.IsValid())
			{
				key_pressed = "";
				key_pressed += (char)evc.code;
			}
			str_key_pressed_->Set(key_pressed);
		}
		else if (state_ == CONFIG_GAMEPAD)
		{
			logger_.Debug("pad-ev: %d - state: %d", padev.code, control_config_state_);
		}
	}

	//
	// This is horrible.
	//
	if (state_ == CONFIG_KEYBOARD || state_ == CONFIG_GAMEPAD)
	{
		switch(control_config_state_)
		{
		case CONFIGURE_MOVE_LEFT:
			configurable = "Configure key for 'Move Right'";
			control_config_state_ = SceneConfig::CONFIGURE_MOVE_RIGHT;
			if (state_ == CONFIG_KEYBOARD)
				keys_.left = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.left = padev;
			break;
		case CONFIGURE_MOVE_RIGHT:
			configurable = "Configure key for 'Rotate Left'";
			control_config_state_ = SceneConfig::CONFIGURE_ROTATE_LEFT;
			if (state_ == CONFIG_KEYBOARD)
				keys_.right = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.right = padev;
			break;
		case CONFIGURE_ROTATE_LEFT:
			configurable = "Configure key for 'Rotate Right'";
			control_config_state_ = SceneConfig::CONFIGURE_ROTATE_RIGHT;
			if (state_ == CONFIG_KEYBOARD)
				keys_.rotleft = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.rotleft = padev;
			break;
		case CONFIGURE_ROTATE_RIGHT:
			configurable = "Configure key for 'Soft Drop'";
			control_config_state_ = SceneConfig::CONFIGURE_SOFT_DROP;
			if (state_ == CONFIG_KEYBOARD)
				keys_.rotright = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.rotright = padev;
			break;
		case CONFIGURE_SOFT_DROP:
			configurable = "Configure key for 'Hard Drop'";
			control_config_state_ = SceneConfig::CONFIGURE_HARD_DROP;
			if (state_ == CONFIG_KEYBOARD)
				keys_.soft_drop = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.soft_drop = padev;
			break;
		case CONFIGURE_HARD_DROP:
			configurable = "Configure key for 'Hold'";
			control_config_state_ = SceneConfig::CONFIGURE_HOLD;
			if (state_ == CONFIG_KEYBOARD)
				keys_.hard_drop = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.hard_drop = padev;
			break;
		case CONFIGURE_HOLD:
			configurable = "Done?";
			control_config_state_ = SceneConfig::CONFIGURE_DONE;
			if (state_ == CONFIG_KEYBOARD)
				keys_.hold = ev;
			else if (state_ == CONFIG_GAMEPAD)
				padctl_.hold = padev;
			break;
		default:
			if (state_ == CONFIG_KEYBOARD)
				game_.inputprocessor_->SetKeys(keys_);
			else if (state_ == CONFIG_GAMEPAD)
				game_.inputprocessor_->SetPadControls(padctl_);
			
			game_.inputprocessor_->Save(game_.cfg_, "input");
			game_.scene_manager_.SetScene(game_.scene_game_);
			break;
		}
	}

	str_configurable_->Set(configurable);
}


void SceneConfig::DoUpdate(unsigned int framenum)
{
}

static Graphics::Rect centerstr(TextProvider::String& label, Rect& root)
{
	Rect cent = label.GetQuads().rect;
	root.Center(cent);
	return cent;
}

void SceneConfig::DoRender(float interpolation)
{
	float winh = gfx_.GetActiveWindow()->GetHeight();
	float winw = gfx_.GetActiveWindow()->GetWidth();

	Rect root = game_.root_;
	Rect centered_rect = centerstr(*str_, root);

	int xpos = centered_rect.x;
	int ypos = centered_rect.y / 8;
	int lineheight = font_.GetFont()->GetInfo().line_height;

	str_->Print(xpos, ypos, 1.f);
	str_desc_->Print(xpos + lineheight*2, ypos + lineheight + lineheight*0.2, .8f);

	str_configurable_->Print(xpos / 2, ypos * 4, 1.f);
	str_key_pressed_->Print(xpos + xpos / 2, ypos * 8, 1.f);
}
