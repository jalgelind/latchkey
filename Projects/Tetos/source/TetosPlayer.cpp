#include <TetosPlayer.h>
#include <TetosRotation.h>
#include <algorithm>

using namespace Tetos;
using glm::ivec2;

Player::Player(TetosLogic& engine, Board& b, RotationSystem& rot, Timing& t)
	: board_(b)
	, timing_(t)
	, rotsys_(rot)
	, engine_(engine)
	, move_shift_initial_delay_(0)
	, move_shift_delay_(0)
	, move_shift_counter_(0)
	, lock_counter_(0)
	, can_use_holdswap_(true)
{
	kernel_ = Pxf::Kernel::GetInstance();
	logger_ = kernel_->GetLogger("player");
}

void Player::ProcessInputEvent(InputEvent ev)
{
	if (ev.state == INPUT_STATE_ON)
		prev_event_ = curr_event_;

	switch (ev.trigger)
	{
		case INPUT_ROTATE_LEFT:
		case INPUT_ROTATE_RIGHT:
			if (ev.state == INPUT_STATE_ON)
				Rotate(ev.trigger);
			break;
		case INPUT_MOVE_LEFT:
		case INPUT_MOVE_RIGHT:
		case INPUT_SOFT_DROP:
			OnMoveEvent(ev);
			break;
		case INPUT_HARD_DROP:
		case INPUT_HOLD_PIECE:
			OnInstantActionEvent(ev);
			break;
		default:
			break;
	}
	if (ev.state == INPUT_STATE_ON)
		curr_event_ = ev.trigger;
}

void Player::ProcessGameTick()
{
	InputTrigger trig = CurrentMovementState();
	if (trig != INPUT_INVALID)
		Move(trig);
}

void Player::Rotate(InputTrigger trig, bool allow_kick)
{
	RotationSystem::RotationResult res =
		rotsys_.Rotate(engine_, active_piece_, trig, allow_kick);
	if (res.valid && can_rotate_)
	{
		active_piece_ = res.piece;
		pos_ += res.offset;

		// short pause before starting to move again
		move_shift_counter_ = move_shift_initial_delay_ / 2;

		// reset piece lock counter
		lock_counter_ = engine_.timing_.piecelock_delay;
	}
}

void Player::HoldSwap()
{
	if (can_use_holdswap_)
	{
		if (hold_piece_.type == Piece::None)
		{
			hold_piece_ = active_piece_;
			NextPiece();
		}
		else
		{
			Piece tmp = hold_piece_;
			hold_piece_ = active_piece_;
			active_piece_ = tmp;
			pos_ = glm::ivec2(3, 0);
		}
		can_use_holdswap_ = false;
	}
}

void Player::OnInstantActionEvent(InputEvent ev)
{
	if (ev.trigger == INPUT_HARD_DROP &&
		ev.state == INPUT_STATE_ON)
	{
		for (int y = 0; y < 20; y++)
		{
			if (CanMove(INPUT_SOFT_DROP))
			{
				Move(INPUT_SOFT_DROP);
			}
			else
			{
				// If we can't move further down
				// an additional move will lock the piece.
				// TODO: option.hard_drop_forces_lock
				Move(INPUT_SOFT_DROP);
				can_force_lock_ = true;
				break;
			}
		}
	}
	else if (ev.trigger == INPUT_HOLD_PIECE && 
			 ev.state == INPUT_STATE_ON)
	{
		HoldSwap();
	}
}


void Player::OnNewGame()
{
	hold_piece_.Set(Piece::None);
	next_pieces_.clear();
	for (int n = 0; n < 4; n++)
		next_pieces_.push_back(Piece::None);

	for (int n = 0; n < 4; n++)
		next_pieces_[n] = engine_.RandomPiece(next_pieces_);
	NextPiece();
}

void Player::OnNewRound()
{
	can_use_holdswap_ = true;
	can_rotate_ = true;
	can_force_lock_ = false;
	curr_event_ = INPUT_INVALID;
	prev_event_ = INPUT_INVALID;
	//movement_stack_.clear();
}

void Player::OnMoveEvent(InputEvent ev)
{
	if (ev.state == INPUT_STATE_ON)
	{
		movement_stack_.push_back(ev.trigger);
		if (ev.trigger == INPUT_MOVE_RIGHT || ev.trigger == INPUT_MOVE_LEFT)
		{
			move_shift_initial_delay_ = timing_.das_delay;
			move_shift_delay_ = timing_.asm_delay;
		}
		else if (ev.trigger == INPUT_SOFT_DROP)
		{
			move_shift_initial_delay_ = timing_.sds_delay;
			move_shift_delay_ = timing_.sds_delay;
		}
		move_shift_counter_ = -1;
	}
	else if (ev.state == INPUT_STATE_OFF)
	{
		auto& v = movement_stack_;
		v.erase(std::remove(v.begin(), v.end(), ev.trigger), v.end());
	}
}

InputTrigger Player::CurrentMovementState()
{
	InputTrigger ret = INPUT_INVALID;
	if (movement_stack_.size() > 0)
	{
		// Initial movement followed by long delay
		// XXX: this step could immedeately be done in UpdateMovement.
		if (move_shift_counter_ == -1)
		{
			move_shift_counter_ = move_shift_initial_delay_;
			ret = movement_stack_.back();
		}
		// movement after that are much shorter;
		else if (move_shift_counter_ == 0)
		{
			move_shift_counter_ = move_shift_delay_;
			ret = movement_stack_.back();
		}
		else
			move_shift_counter_--;
	}
	return ret;
}

static glm::ivec2 DirectionToOffset(InputTrigger dir)
{
	glm::ivec2 offset;
	switch(dir)
	{
	case INPUT_MOVE_LEFT:
		offset.x = -1;
		break;
	case INPUT_MOVE_RIGHT:
		offset.x = 1;
		break;
	case INPUT_SOFT_DROP:
		offset.y = 1;
		break;
	}
	return offset;
}

bool Player::CanMove(InputTrigger dir)
{
	glm::ivec2 offset = DirectionToOffset(dir);
	return board_.CanPutPiece(active_piece_, pos_.x+offset.x, pos_.y+offset.y);
}

void Player::Move(InputTrigger dir, bool gravitystep)
{
	glm::ivec2 offset = DirectionToOffset(dir);
	if (CanMove(INPUT_SOFT_DROP))
		can_force_lock_ = false;

	if (CanMove(dir))
	{
		pos_ += offset;
		lock_counter_ = engine_.timing_.piecelock_delay;
	}
	else if (!gravitystep && ((curr_event_ == INPUT_HARD_DROP && prev_event_ == INPUT_HARD_DROP) ||
			 (curr_event_ == INPUT_SOFT_DROP && prev_event_ == INPUT_HARD_DROP) ||
			 (curr_event_ == INPUT_SOFT_DROP && can_force_lock_) ||
			 (curr_event_ == INPUT_HARD_DROP && can_force_lock_)))
	{
		logger_.Debug("FORCE LOCKING PIECE.");
		engine_.game_forcelock_ = true;
	}
}


void Player::NextPiece()
{
	active_piece_.Set(next_pieces_[0]);
	rotsys_.Init(active_piece_);
	for (int n = 0; n < 3; n++)
		next_pieces_[n] = next_pieces_[n+1];
	next_pieces_[3] = engine_.RandomPiece(next_pieces_);
	pos_ = glm::ivec2(3, 0);
}