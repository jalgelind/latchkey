#include <Pxf/Kernel.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Json.h>

#include <TetosUtils.h>
#include "TetosInput.h"


#include <algorithm>

using namespace Pxf;
using namespace Tetos;

// Serialize current game state to a json file at `path`.
bool Tetos::SaveGameAs(std::string path, TetosLogic::GameHistory& history)
{
	Pxf::Kernel* kernel = Kernel::GetInstance();
	Pxf::Resource::ResourceManager* res = kernel->GetResourceManager();

	::Json::Value root;
	root["playername"] = history.playername;
	root["identifier"] = history.identifier;
	root["saveformat"] = history.saveformat;
	for(int i = 0; i < history.actionhistory.size(); i++)
	{
		unsigned int tick = history.actionhistory[i].tick;
		InputEvent ev = history.actionhistory[i].ev;

		::Json::Value jev;
		jev["tr"] = (unsigned int)ev.trigger;
		jev["st"] = (unsigned int)ev.state;
		root["input"][tick].append(jev);
	}

	for(int i = 0; i < history.piecelog.size(); i++)
	{
		Piece::Type type = history.piecelog[i];
		root["pieces"][i] = (int)type;
	}

	auto doc = res->CreateJsonDocument();
	doc->SetRoot(root);
	if(!doc->SaveToDisk(path.c_str()))
		return false;
	return true;
}

static bool itemsorter(TetosLogic::ActionHistoryItem const& lhs, TetosLogic::ActionHistoryItem const& rhs) {
	return lhs.tick < rhs.tick;
}

TetosLogic::GameHistory Tetos::LoadGameFrom(std::string path)
{
	TetosLogic::GameHistory history;
	Pxf::Kernel* kernel = Kernel::GetInstance();
	Pxf::Resource::ResourceManager* res = kernel->GetResourceManager();
	auto doc = res->Acquire<Resource::Json>(path.c_str());
	if (!doc)
	{
		//logger_.Error("Could not load saved game '%s'", path.c_str());
		return history;
	}
	::Json::Value root = doc->GetRoot();

	if (root["saveformat"].asString() != CURRENT_SAVE_FORMAT_VERSION)
	{
		//logger_.Error("Incompatible save file format.");
		return history;
	}

	std::vector<TetosLogic::ActionHistoryItem>& hist = history.actionhistory;
	hist.clear();
	::Json::Value data = root["input"];
	for(::Json::ValueIterator itr = data.begin(); itr != data.end(); ++itr)
	{
		int tick = itr.key().asInt();
		for(auto v: data[tick])
		{
			unsigned int ev_trigger = v["tr"].asUInt();
			unsigned int ev_status = v["st"].asUInt();
			TetosLogic::ActionHistoryItem item;
			item.tick = tick;
			item.ev.trigger = (InputTrigger)ev_trigger;
			item.ev.state = (InputState)ev_status;
			hist.push_back(item);
		}
	}

	::Json::Value pcs = root["pieces"];
	history.piecelog.clear();
	for(int i = 0; i < pcs.size(); i++)
	{
		history.piecelog.push_back((Piece::Type)pcs[i].asInt());
	}

	std::sort(hist.rbegin(), hist.rend(), &itemsorter);
	std::reverse(history.piecelog.begin(), history.piecelog.end());

	history.playername = root["playername"].asString();
	history.identifier = root["identifier"].asString();
	return history;
}

int Tetos::GetGravityForLevel(int _level)
{
	/* 
		Table taken from here:
			http://tetrisconcept.net/wiki/Tetris_The_Absolute_The_Grand_Master_2_PLUS#Speed_Timings
	*/
	static int gravity_lut[][2] = { // tgm2plus_gravity_lut
		{  0,   4},
		{  8,   5},
		{ 19,   6},
		{ 35,   8},
		{ 40,  10},
		{ 50,  12},
		{ 60,  16},
		{ 70,  32},
		{ 80,  48},
		{ 90,  64},
		{100,   4},
		{108,   5},
		{119,   6},
		{125,   8},
		{131,  12},
		{139,  32},
		{149,  48},
		{156,  80},
		{164, 112},
		{174, 128},
		{180, 144},
		{200,  16},
		{212,  48},
		{221,  80},
		{232, 112},
		{244, 144},
		{256, 176},
		{267, 192},
		{277, 208},
		{287, 224},
		{295, 240},
		{300, 5120}
	};
	
	int ret = 5120;

	for(int i = sizeof(gravity_lut)/sizeof(gravity_lut[0]); i--;)
	{
		int level = gravity_lut[i][0];
		int gravity = gravity_lut[i][1];

		if (_level >= level)
		{
			ret = gravity;
			break;
		}
	}
	printf("level: %d => gravity %d\n", _level, ret);
	return ret;
}
