#include <Pxf/Util/Configuration.h>
#include <TetosInput.h>

using namespace Pxf;
using namespace Tetos;

using Input::Gamepad;
using Input::Keyboard;

//
// Input processor
// ===============
// Implement new controllers here 
//
InputEvent InputProcessor::Process(Keyboard::Event& ev)
{
	InputEvent ret;
	ret.trigger = INPUT_INVALID;

	if (keys.hard_drop == ev.code)
		ret.trigger = INPUT_HARD_DROP;
	else if (keys.soft_drop == ev.code)
		ret.trigger = INPUT_SOFT_DROP;
	else if (keys.left == ev.code)
		ret.trigger = INPUT_MOVE_LEFT;
	else if (keys.right == ev.code)
		ret.trigger = INPUT_MOVE_RIGHT;
	else if (keys.rotleft == ev.code)
		ret.trigger = INPUT_ROTATE_LEFT;
	else if (keys.rotright == ev.code)
		ret.trigger =  INPUT_ROTATE_RIGHT;
	else if (keys.hold == ev.code)
		ret.trigger =  INPUT_HOLD_PIECE;
	ret.state = ev.IsPressed() ? INPUT_STATE_ON : INPUT_STATE_OFF;
	return ret;
}

InputEvent InputProcessor::Process(Gamepad::Event& ev)
{
	InputEvent ret;
	ret.trigger = INPUT_INVALID;

	if (!ev.IsValid())
		return ret;

	if (padctl.hard_drop == ev.code)
		ret.trigger = INPUT_HARD_DROP;
	else if (padctl.soft_drop == ev.code)
		ret.trigger = INPUT_SOFT_DROP;
	else if (padctl.left == ev.code)
		ret.trigger = INPUT_MOVE_LEFT;
	else if (padctl.right == ev.code)
		ret.trigger = INPUT_MOVE_RIGHT;
	else if (padctl.rotleft == ev.code)
		ret.trigger = INPUT_ROTATE_LEFT;
	else if (padctl.rotright == ev.code)
		ret.trigger = INPUT_ROTATE_RIGHT;
	else if (padctl.hold == ev.code)
		ret.trigger = INPUT_HOLD_PIECE;
	ret.state = ev.IsAxisUpRelease() || ev.IsAxisDownRelease() || ev.IsReleased() ? INPUT_STATE_OFF : INPUT_STATE_ON;
	return ret;
}

Keyboard::Event InputProcessor::ReadKeyboardValue(Pxf::Configuration& cfg, std::string root, std::string node)
{
	std::string path = root + "." + node;
	int code;
	cfg.SetTypechecking(false);
	if (cfg.IsInt(path.c_str()))
		code = cfg.GetInt(path.c_str(), 0);
	else
	{
		std::string str = cfg.GetString(path.c_str(), "a");
		code = kbd_.GetKeyCode(str);
	}
	cfg.SetTypechecking(true);
	return Keyboard::Event(code, Keyboard::Event::KEY_PRESS);
}

void InputProcessor::WriteKeyboardValue(Pxf::Configuration& cfg, std::string root, std::string node, Keyboard::Event value)
{
	std::string path = root + "." + node;
	std::string name = kbd_.GetKeyName(value.code);
	cfg.SetTypechecking(false);
	if (name == "???")
		cfg.Set(path, (int)value.code);
	else
	{
		std::string comment = Pxf::Format("keycode %d", value.code);
		cfg.Set(path, name, comment);
	}
	cfg.SetTypechecking(true);
}

Gamepad::Event InputProcessor::ReadGamepadValue(Pxf::Configuration& cfg, std::string root, std::string node)
{
	std::string path = root + "." + node;
	Gamepad::Event ev;
	ev.code = cfg.GetInt(path + ".c", 0);
	ev.type = (Gamepad::EventType)cfg.GetInt(path + ".t", 0);
	return ev;
}

void InputProcessor::WriteGamepadValue(Pxf::Configuration& cfg, std::string root, std::string node, Gamepad::Event value)
{
	std::string path = root + "." + node;
	std::string type = Pxf::Format("%s[%d]", value.IsAxisEvent()? "Axis" : "Button", value.code);
	cfg.Set(path + ".c", (int)value.code, type);
	cfg.Set(path + ".t", (int)value.type, Gamepad::repr((Gamepad::EventType)value.type));
}

void InputProcessor::Load(Pxf::Configuration& cfg, std::string root)
{
	keys.left = ReadKeyboardValue(cfg, root, "keys.move_left");
	keys.right = ReadKeyboardValue(cfg, root, "keys.move_right");
	keys.rotleft = ReadKeyboardValue(cfg, root, "keys.rotate_left");
	keys.rotright = ReadKeyboardValue(cfg, root, "keys.rotate_right");
	keys.soft_drop = ReadKeyboardValue(cfg, root, "keys.soft_drop");
	keys.hard_drop = ReadKeyboardValue(cfg, root, "keys.hard_drop");
	keys.hold = ReadKeyboardValue(cfg, root, "keys.hold");

	padctl.left = ReadGamepadValue(cfg, root, "pad.move_left");
	padctl.right = ReadGamepadValue(cfg, root, "pad.move_right");
	padctl.rotleft = ReadGamepadValue(cfg, root, "pad.rotate_left");
	padctl.rotright = ReadGamepadValue(cfg, root, "pad.rotate_right");
	padctl.soft_drop = ReadGamepadValue(cfg, root, "pad.soft_drop");
	padctl.hard_drop = ReadGamepadValue(cfg, root, "pad.hard_drop");
	padctl.hold = ReadGamepadValue(cfg, root, "pad.hold");
}

void InputProcessor::Save(Pxf::Configuration& cfg, std::string root)
{
	WriteKeyboardValue(cfg, root, "keys.move_left", keys.left);
	WriteKeyboardValue(cfg, root, "keys.move_right", keys.right);
	WriteKeyboardValue(cfg, root, "keys.rotate_left", keys.rotleft);
	WriteKeyboardValue(cfg, root, "keys.rotate_right", keys.rotright);
	WriteKeyboardValue(cfg, root, "keys.soft_drop", keys.soft_drop);
	WriteKeyboardValue(cfg, root, "keys.hard_drop", keys.hard_drop);
	WriteKeyboardValue(cfg, root, "keys.hold", keys.hold);

	WriteGamepadValue(cfg, root, "pad.move_left", padctl.left);
	WriteGamepadValue(cfg, root, "pad.move_right", padctl.right);
	WriteGamepadValue(cfg, root, "pad.rotate_left", padctl.rotleft);
	WriteGamepadValue(cfg, root, "pad.rotate_right", padctl.rotright);
	WriteGamepadValue(cfg, root, "pad.soft_drop", padctl.soft_drop);
	WriteGamepadValue(cfg, root, "pad.hard_drop", padctl.hard_drop);
	WriteGamepadValue(cfg, root, "pad.hold", padctl.hold);

	cfg.Save();
}