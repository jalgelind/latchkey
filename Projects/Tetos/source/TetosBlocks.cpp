#include <TetosBlocks.h>
#include <Pxf/Base/Utils.h>

using namespace Tetos;


// -------------------------------------------------------------
// - Piece Logic                                               -
// -------------------------------------------------------------

Piece::Piece()
{
	Set(None);
}

Piece::Piece(Type type)
{
	Set(type);
}

Piece::Piece(const Piece& other)
{
	type = other.type;
	max_rotations = other.max_rotations;
	size = other.size;
	rotations = other.rotations;

	for (int y = 0; y < 4; y++)
		for (int x = 0; x < 4; x++)
			data[y][x] = other.data[y][x];
}

void Piece::Set(Type type)
{
	//  Clear
	for (int y = 0; y < 4; y++)
		for (int x = 0; x < 4; x++)
			data[y][x].color = Block::None;

	// Reset data members
	this->type = type;
	size = 4;
	rotations = 0;
	max_rotations = 4;

	switch(type)
	{
	case I:
		data[1][0].color = Block::Red; // ----
		data[1][1].color = Block::Red; // xxxx
		data[1][2].color = Block::Red; // ----
		data[1][3].color = Block::Red; // ----
		size = 4;
		//max_rotations = 2; // simple vs compilant
		max_rotations = 4;
		break;
	case J:
		data[1][0].color = Block::Purple; // x---
		data[1][1].color = Block::Purple; // xxx-
		data[1][2].color = Block::Purple; // ----
		data[0][0].color = Block::Purple; // ----
		size = 3;
		max_rotations = 4;
		break;
	case L:
		data[1][0].color = Block::Yellow; // --x-
		data[1][1].color = Block::Yellow; // xxx-
		data[1][2].color = Block::Yellow; // ----
		data[0][2].color = Block::Yellow; // ----
		size = 3;
		max_rotations = 4;
		break;
	case O:
		data[0][1].color = Block::Orange; // -xx-
		data[0][2].color = Block::Orange; // -xx-
		data[1][1].color = Block::Orange; // ----
		data[1][2].color = Block::Orange; // ----
		size = 4;
		max_rotations = 1;
		break;
	case T:
		data[1][0].color = Block::Blue; // ----
		data[1][1].color = Block::Blue; // xxx-
		data[1][2].color = Block::Blue; // -x--
		data[0][1].color = Block::Blue; // ----
		size = 3;
		max_rotations = 4;
		break;
	case S:
		data[0][1].color = Block::Green; // -xx-
		data[0][2].color = Block::Green; // xx--
		data[1][0].color = Block::Green; // ----
		data[1][1].color = Block::Green; // ----
		size = 3;
		max_rotations = 4;
		//max_rotations = 2; // simple vs compilant
		break;
	case Z:
		data[0][0].color = Block::Cyan; // xx--
		data[0][1].color = Block::Cyan; // -xx-
		data[1][1].color = Block::Cyan; // ----
		data[1][2].color = Block::Cyan; // ----
		size = 3;
		max_rotations = 4;
		//max_rotations = 2; // simple vs compilant
		break;
	default: break;
	}
}

void Piece::SetOpacity(float opacity)
{
	for(int y = 0; y < 4; y++)
		for (int x = 0; x < 4; x++)
			data[x][y].opacity = opacity;
}

static void do_rotation(Block data[4][4], int s, int r)
{
	Block tmp[4][4];

	for(int y = 0; y < s; y++)
		for (int x = 0; x < s; x++)
			tmp[y][x] = data[r-x][y];

	for (int y = 0; y < s; y++)
		for (int x = 0; x < s; x++)
			data[y][x] = tmp[y][x];
}

void Piece::_RotateRightImpl()
{
	if (max_rotations <= 1)
		return;

	rotations++;

	if(max_rotations == 2 && rotations == 2)
	{
		do_rotation(data, size, size-1);
		do_rotation(data, size, size-1);
		do_rotation(data, size, size-1);
	}
	else
		do_rotation(data, size, size-1);
}

void Piece::RotateRight()
{
	_RotateRightImpl();
	rotations %= max_rotations;
}

void Piece::RotateLeft()
{
	_RotateRightImpl();
	_RotateRightImpl();
	_RotateRightImpl();
	rotations %= max_rotations;
}

int Piece::Height()
{
	int height = 0;
	for (int y = 0; y < 4; y++)
	{
		bool freerow = false;
		for (int x = 0; x < 4; x++)
		{
			if (data[y][x].color != Block::None)
				freerow = true;
		}
		if (freerow)
			++height;
	}
	return height;
}

int Piece::Width()
{
	int width = 0;
	for (int x = 0; x < 4; x++)
	{
		bool freecol = false;
		for (int y = 0; y < 4; y++)
		{
			if (data[y][x].color != Block::None)
				freecol = true;
		}
		if (freecol)
			++width;
	}
	return width;
}

// -------------------------------------------------------------
// - Board Logic                                               -
// -------------------------------------------------------------

void Board::CopyFrom(Board& other)
{
	for(int y = 0; y < 22; y++)
	{
		for (int x = 0; x < 10; x++)
		{
			if (other.data_[y][x].color != Block::None)
				data_[y][x].color = Block::DarkGrey;
			else
				data_[y][x].color = other.data_[y][x].color;
			data_[y][x].opacity = other.data_[y][x].opacity;
		}
	}
}

bool Board::CanPutBlock(Block& b, int bx, int by)
{
	Block::Color color = b.color;

	if (color != Block::None)
	{
		if (by >= 22 || data_[by][bx].color != Block::None)
			return false;

		if (bx > 9 || bx < 0 || data_[by][bx].color != Block::None)
			return false;
	}
	return true;
}


bool Board::CanPutPiece(Piece& p, int dx, int dy)
{
	for (int py = 0; py < 4; py++)
	{
		for (int px = 0; px < 4; px++)
		{
			if (!CanPutBlock(p.data[py][px], px+dx, py+dy))
				return false;
		}
	}
	return true;
}

void Board::PutBlock(Block& b, int dx, int dy, bool dead)
{
	int x = Pxf::Clamp(dx, 0, 9); // TODO: make sure this not happens, and then remove
	int y = Pxf::Clamp(dy, 0, 21);
	data_[y][x].color = b.color;
	data_[y][x].opacity = b.opacity;
	//if (dead)
	//	data_[y][x].opacity = 0.1f;
}


void Board::PutPiece(Piece& p, int dx, int dy, bool dead)
{
	for (int py = 0; py < 4; py++)
	{
		for (int px = 0; px < 4; px++)
		{
			if (p.data[py][px].color != Block::None)
				PutBlock(p.data[py][px], dx+px, dy+py, dead);
		}
	}
}

int Board::FreeSpaceBelow(Piece& p, int x, int y)
{
	int space = 0;
	for(int ny = y; ny < 22; ny++)
	{
		if (CanPutPiece(p, x, ny))
			++space;
		else
			break;
	}
	return space-1;
}

void Board::Clear()
{
	for (int y = 0; y < 22; y++)
		for (int x = 0; x < 10; x++)
		{
			data_[y][x].color = Block::None;
			data_[y][x].opacity = 0.f;
		}
}