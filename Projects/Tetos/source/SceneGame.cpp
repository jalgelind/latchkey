#include <Pxf/Base/Utils.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/PrimitiveBatch.h>
#include <Pxf/Graphics/TextProvider.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Resource/Font.h>

#include "TetosRotation.h"
#include "TetosRenderer.h"
#include "SceneGame.h"
#include "TetosEngine.h"
#include "TetosInput.h"
#include "TetosUtils.h"
#include "Game.h"

#ifndef CONF_SKIP_GTK_DEPS
#include <sstat.h>
#endif

#include <ctime>
#include <stdio.h>

using namespace Pxf;
using namespace Graphics;
using namespace Input;
using namespace Tetos;

SceneGame::SceneGame(Game& game)
	: Scene(game, "game")
	, game_(game)
	, game_texture_(game.gameres_.tex_)
	, game_pb_(game.pb_)
	, tetos_logic_( game.rotsys_
				  , game.timing_)
	, board_renderer_(game)
	, display_eliminated_(false)
	, font_(game.gameres_.textp_)
{
	gfxstr_ = font_->CreateString("hello");
	gfxstr_->SetDefaultColor("white");
};

SceneGame::~SceneGame()
{
}

void SceneGame::Init()
{
	locked_ = true;
	tetos_logic_.Init();
}

void SceneGame::Reset()
{
	locked_ = false;
}

void SceneGame::DoInput()
{
	Input::Keyboard::KbdQ& keyQ = inp_.GetKeyboard().GetKeyQueue();
	Input::Keyboard::Event ev = keyQ.Peek();
	if (ev.IsPressed())
	{
		if (ev.code == Input::ESC)
		{
			game_.is_running_ = false;
			keyQ.Pop();
			return;
		}

		if (ev.code == Input::F1)
		{
			char path[256];
#ifndef CONF_SKIP_GTK_DEPS
			sstat_getcwd(path, 255);
			sstat_openfiledialog(path);
#else
			system("xmessage -nearmouse Enter filename of replay in the active terminal.");
			printf("Input filename of replay: ");
			fflush(stdout);
			scanf("%s", path);
#endif
			TetosLogic::GameHistory history = LoadGameFrom(path);
			tetos_logic_.current_game_history_ = history;
			tetos_logic_.RunReplay();
			keyQ.Pop();
			return;
		}
		else if (ev.code == Input::F2)
		{
			tetos_logic_.GameOver();
			keyQ.Pop();
		}
	}
}


void SceneGame::DoUpdate(unsigned int framenum)
{
	PXF_PROFILE("app.update.scene.game");

	Input::Keyboard::KbdQ& charQ = inp_.GetKeyboard().GetCharQueue();
	Input::Keyboard::KbdQ& keyQ = inp_.GetKeyboard().GetKeyQueue();
	Input::Mouse::MouseQ& mouseQ = inp_.GetMouse().GetEventQueue();
	Input::Gamepad::EventQ& padQ = inp_.GetGamepad().GetEventQueue();

	std::vector<InputEvent> tickevents;
	do
	{
		Keyboard::Event ev = keyQ.Pop();
		if (ev.IsValid())
		{
			InputEvent tev = game_.inputprocessor_->Process(ev);
			if (tev.trigger != INPUT_INVALID)
				tickevents.push_back(tev);
		}

		/* I don't remember why this is done this way. Need to experiment more with the game pad. */
		Gamepad::Event pev = padQ.Pop();
		static Gamepad::Event prev_pad_event;
		if (pev.IsValid())
		{
			if (prev_pad_event.IsAxisUp() || prev_pad_event.IsAxisDown())
			{
				if (pev.IsAxisUp() || pev.IsAxisDown())
				{
					Gamepad::Event release(prev_pad_event.axis, prev_pad_event.IsAxisUp() ? Gamepad::EVENT_AXISUPRELEASE : Gamepad::EVENT_AXISDOWNRELEASE);
					InputEvent release_event = game_.inputprocessor_->Process(release);
					tickevents.push_back(release_event);
				}
			}
			prev_pad_event = pev;
			InputEvent tev = game_.inputprocessor_->Process(pev);
			if (tev.trigger != INPUT_INVALID)
				tickevents.push_back(tev);
		}
	} while (keyQ.Peek().IsValid() || padQ.Peek().IsValid());

	tetos_logic_.Update(framenum, tickevents);
	TetosLogic::FrameState& fs = tetos_logic_.frame_state_;

	if (fs.gameover)
	{
		unsigned time = std::time(NULL);
		std::string name = Format("data/save-%d.json", time);
		bool res = SaveGameAs(name, tetos_logic_.current_game_history_);
		if (!res)
			logger_.Error("Could not save game.");
	}
}

void SceneGame::DoRender(float interpolation)
{
	PXF_PROFILE("app.render.game.playfield");
	gfx_.BindTexture(game_texture_);
	game_pb_->QuadsBegin();
	board_renderer_.DrawBackground();
	game_pb_->QuadsEnd();

/*
	-----------------------------
	| header                    |
	-----------------------------
	| gamearea                  |
	-----------------------------
	| footer                    |
	-----------------------------
*/
	int header_height = 45; // px
	int footer_height = 45; // px

	Rect rootrect = game_.root_;
	Rect header, rest;
	Rect gamearea, footer;

	rootrect.HorizontalSplitFromTop(header, rest, header_height);
	rest.HorizontalSplitFromBottom(gamearea, footer, footer_height);

	Rect board;
	board.h = 160;
	board.w = 80;
	gamearea.HorizontalCenter(board);


	DrawBoard(interpolation, 80, 78, 2.f);
	DrawDebug();
	
	// Draw score
	int x = 80 + 10*8*2 + 16;
	int y = 50;
	int h = game_.gameres_.gamefont_->GetInfo().line_height + 3;
	

	int freespace = tetos_logic_.board_.FreeSpaceBelow(tetos_logic_.player_.active_piece_
													  ,tetos_logic_.player_.pos_.x
													  ,tetos_logic_.player_.pos_.y);

	std::string stats_str = std::string("")
						  + "<grey>Framecount: <white>%d</white>\n"
							"Drop length: " + (freespace < 5 ? "<blink><red>%d</red></blink>"
															 : "<white>%d</white>") + "\n" + 
							"Piece size: <white>%d x %d</white>\n"
							"Eliminated rows: <white>%d</white>";
	gfxstr_->SetF(stats_str
					 ,game_.frame_counter_, freespace
					 ,(int)tetos_logic_.player_.active_piece_.Height()
					 ,(int)tetos_logic_.player_.active_piece_.Width()
					 ,tetos_logic_.GetEliminatedRows());
	gfxstr_->Print(x, y+h*2, 1.f);
}


void SceneGame::DrawBoard(float interpolation, int _board_pos_x, int _board_pos_y, float in_scale)
{
	// Scale position and size
	_board_pos_x /= in_scale;
	_board_pos_y /= in_scale;
	gfx_.PushProjectionMatrix();
	gfx_.PushModelMatrix();
	glm::mat4 scale = glm::scale(glm::mat4(1.f), glm::vec3(in_scale, in_scale, 1.f));
	gfx_.SetModelViewMatrix(scale);

	// Draw frame
	gfx_.BindTexture(0);
	game_pb_->QuadsBegin();
	game_pb_->SetColor(1,1,1,1);
	game_pb_->QuadsDrawTopLeft(_board_pos_x-1, _board_pos_y-1, 80+2, 160+2);

	// Draw black background
	game_pb_->SetColor(0,0,0,1);
	game_pb_->QuadsDrawTopLeft(_board_pos_x, _board_pos_y, 80, 160);
	game_pb_->QuadsEnd();

	// Player info
	glm::ivec2& ppos = tetos_logic_.player_.pos_;
	Piece& player = tetos_logic_.player_.active_piece_;
	Board& board = tetos_logic_.board_;

	// Board to output
	Board FinalOut;
	FinalOut.CopyFrom(tetos_logic_.board_);

	// Render ghost tetromino on top of everything.
	int freespace = tetos_logic_.board_.FreeSpaceBelow(player, ppos.x, ppos.y);
	if (tetos_logic_.state_ == TetosLogic::STATE_GAMETICK && !tetos_logic_.frame_state_.in_are)
	{
		Piece ghost = player;
		ghost.SetOpacity(0.4f);
		FinalOut.PutPiece(ghost, ppos.x, ppos.y+freespace, false);
	}

	// Draw everything inside the game board
	gfx_.BindTexture(game_texture_);
	gfx_.SetClip((_board_pos_x)*in_scale, _board_pos_y*in_scale+1, 80*2+1, 160*2+2); // TODO: ugh.
	game_pb_->QuadsBegin();
	{
		if (!game_.gamecfg_.smooth_movement && !tetos_logic_.frame_state_.in_are)
		{
			FinalOut.PutPiece(player, ppos.x, ppos.y, false);
		}

		board_renderer_.DrawBoard(FinalOut, _board_pos_x, _board_pos_y);
		
		if ( game_.gamecfg_.smooth_movement
		  && tetos_logic_.state_ != TetosLogic::STATE_SPAWN_PIECE
		  && tetos_logic_.state_ != TetosLogic::STATE_LINE_CLEAR
		  && !tetos_logic_.frame_state_.in_are)
		{
			float gravity_interpolation = (tetos_logic_.game_gravity_accumulator_ / 256.f) * 8;
			float frame_interpolation = 0;
			float pos = (ppos.y-2)*8 + gravity_interpolation;
			float ypos = Clamp(pos, -16, (ppos.y+freespace-2/*-hiddenrows*/)*8);
			board_renderer_.DrawPiece(player, _board_pos_x + ppos.x * 8, _board_pos_y + ypos);
		}
	}
	game_pb_->QuadsEnd();
	gfx_.SetNoClip();

	// Draw next pieces, hold piece and some other stuff
	game_pb_->QuadsBegin();
	{
		Piece p;
		p.Set(tetos_logic_.GetNextPiece(0));
		board_renderer_.DrawPiece(p, _board_pos_x + 8*3, _board_pos_y - 8*3);

		p = tetos_logic_.GetHoldPiece();
		board_renderer_.DrawPiece(p, _board_pos_x - 8*3, _board_pos_y - 8*3);

		// Some indicatiors for lines eliminated
		Block b;
		b.color = Block::Red;
		b.opacity = 1.f;
		for(int y = 0; y < 22; y++)
		{
			if (tetos_logic_.frame_state_.rowstate[y] == 1 && display_eliminated_)
				board_renderer_._DrawBlock(b, _board_pos_x - 16, _board_pos_y + 8*y - 16, .3f);
		}
	}
	game_pb_->QuadsEnd();

	int fh = game_.gameres_.gamefont_->GetInfo().line_height;
	int fy = _board_pos_y + 8*22;

	fy += fh;

	gfxstr_->SetKerning(true);
	gfxstr_->SetF("%03d\n%03d"
		,tetos_logic_.GetCurrentLevel()
		,tetos_logic_.GetTargetLevel());
	gfxstr_->Print(_board_pos_x - 16, fy + fh*3, 1.f);

	// Scale down
	gfx_.PushProjectionMatrix();
	gfx_.PushModelMatrix();
	scale = glm::scale(glm::mat4(1.f), glm::vec3(in_scale / 2.f, in_scale / 2.f, 1.f));
	gfx_.SetModelViewMatrix(scale);

	game_pb_->QuadsBegin();
	{
		Piece next_piece;
		for (int n = 0; n < 3; n++)
		{
			next_piece.Set(tetos_logic_.GetNextPiece(n+1));
			board_renderer_.DrawPiece(next_piece, _board_pos_x*2 + 8*4*2 + 8*3*2 + 40 + (40*n), _board_pos_y);
		}
	}
	game_pb_->QuadsEnd();
	gfx_.PopModelMatrix();
	gfx_.PopProjectionMatrix();

	// Draw borders
	gfx_.BindTexture(0);
	game_pb_->QuadsBegin();
	{
		game_pb_->SetColor(1,1,1,1);
		for (int y = 0; y < 20; y++)
		{
			for (int x = 0; x < 10; x++)
			{
				if (tetos_logic_.board_.data_[y+2][x].color == Block::None)
					continue;

				float px = _board_pos_x + (x*8); // -1px, share borders
				float py = _board_pos_y + (y*8); // same

				// left
				int tmpy = Clamp(y+2, 0, 21);
				int tmpx = Clamp(x-1, 0, 9);
				if (tetos_logic_.board_.data_[tmpy][tmpx].color == Block::None)
					game_pb_->QuadsDrawTopLeft(px, py, 1, 8);

				// right
				tmpy = Clamp(y+2, 0, 21);
				tmpx = Clamp(x+1, 0, 9);
				if (tetos_logic_.board_.data_[tmpy][tmpx].color == Block::None)
					game_pb_->QuadsDrawTopLeft(px+7, py, 1, 8);

				// top
				tmpy = Clamp(y+2-1, 0, 21);
				tmpx = Clamp(x, 0, 9);
				if (tetos_logic_.board_.data_[tmpy][tmpx].color == Block::None)
					game_pb_->QuadsDrawTopLeft(px, py, 8, 1);

				// bottom
				tmpy = Clamp(y+2+1, 0, 21);
				tmpx = Clamp(x, 0, 9);
				if (tetos_logic_.board_.data_[tmpy][tmpx].color == Block::None)
					game_pb_->QuadsDrawTopLeft(px, py+7, 8, 1);
			}
		}
	}
	game_pb_->QuadsEnd();

	gfx_.PopModelMatrix();
	gfx_.PopProjectionMatrix();
}

void SceneGame::DrawDebug()
{
	const int offset_x = 480;
	const int offset_y = 170;
	const int space_x = 5;
	const int space_y = 5;

	gfx_.PushProjectionMatrix();

	gfx_.SetPointSize(4.f);
	game_pb_->PointsBegin();
	for(int i = 0; i < game_.gamepad_->ButtonCount(); i++)
	{
		if (game_.gamepad_->GetButtonStatus(i))
			game_pb_->PointsDraw(offset_x + 4*i, offset_y - 160);
	}

	for(int i = 0; i < game_.gamepad_->AxisCount(); i++)
	{
		float val = game_.gamepad_->GetAxisValue(i);
		float oldval = game_.gamepad_->GetPreviousAxisValue(i);
		if (game_.gamepad_->GetAxisStatus(2).IsAxisUp())
			game_pb_->PointsDraw(offset_x + 80 + 4*i, (offset_y - 100) + 50*val);
	}

	game_pb_->PointsEnd();

	gfx_.PopProjectionMatrix();
	
}
