#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/Base/Random.h>
#include <Pxf/Base/String.h>

#include "SceneGame.h"
#include "TetosEngine.h"
#include "TetosData.h"
#include "TetosUtils.h"
#include "TetosRotation.h"

#include <ctime>

using namespace Pxf;
using namespace Pxf::Input;
using namespace Tetos;

// -------------------------------------------------------------
// - Tetos Logic                                               -
// -------------------------------------------------------------

TetosLogic::TetosLogic(RotationSystem* rotsys, Timing& timing)
	: rotsys_(rotsys)
	, timing_(timing)
	, current_frame_(0)
	, current_gametick_(0)
	, run_replay_(false)
	, player_(*this, board_, *rotsys, timing)
{
	logger_ = Pxf::Kernel::GetInstance()->GetLogger("logic");
	ResetTimers();
	Clear();
}

TetosLogic::~TetosLogic()
{
}

void TetosLogic::Init()
{
	rand_seed_ = std::time(NULL);
	run_replay_ = false;
	state_ = STATE_INIT; // STATE_PAUSED
}

void TetosLogic::Clear()
{
	ResetTimers();
	game_forcelock_ = false;
	game_gravity_accumulator_ = 0;
	game_eliminated_rows_ = 0;
	game_current_level_ = 0;
	game_gravity_ = GetGravityForLevel(game_current_level_);
	current_frame_ = 0;
	m_InputStates.clear();
	for(int i = 0; i <= INPUT_INVALID; i++)
		m_InputStates.push_back(INPUT_STATE_OFF);
	Pxf::RandSetSeed(rand_seed_);
	player_.OnNewGame();
	board_.Clear();
}

void TetosLogic::ResetTimers()
{
	counter_.soft_drop_counter = timing_.sds_delay;
	counter_.hard_drop_counter = 0;
	counter_.are_counter = timing_.are_delay;
	counter_.line_clear_counter = timing_.lineclear_delay;
}

void TetosLogic::GameOver()
{
	game_current_level_ = 0;
	game_gravity_ = GetGravityForLevel(game_current_level_);
	game_eliminated_rows_ = 0;
	Init();
}

static bool trigger_sorter(InputEvent e1, InputEvent e2)
{
	return e1.trigger < e2.trigger || 
		   (e1.trigger == e2.trigger && e1.state < e2.state);
}

void TetosLogic::Update(unsigned int framenum, std::vector<InputEvent>& events)
{
	frame_state_.Reset();

	// Sort the input events so that they are processed in a preferred order.
	// 1. Rotation
	// 2. Horizontal movements
	// 3. Vertical movement
	// OFF events always follows after ON events.
	std::sort(begin(events), end(events), trigger_sorter);

	std::vector<ActionHistoryItem>& hist = current_game_history_.actionhistory;
	if (run_replay_ && hist.size() > 0)
	{
		events.clear();
		ActionHistoryItem item = hist.back();
		while (hist.size() > 0 && ((item = hist.back()).tick == current_frame_))
		{
			hist.pop_back();
			events.push_back(item.ev);
		}
	}

	for(auto ev: events)
	{
		m_InputStates[ev.trigger] = ev.state;
		if (!run_replay_)
		{
			ActionHistoryItem item;
			item.tick = current_frame_;
			item.ev.trigger = ev.trigger;
			item.ev.state = ev.state;
			current_game_history_.actionhistory.push_back(item);
		}
		// TODO: Should not process events during all game states?
		player_.ProcessInputEvent(ev);
	}

	switch(state_)
	{
	case STATE_PAUSED: /* TODO */ break;
	case STATE_SELECT:
		break;
	case STATE_GAMEOVER:
		GameOver();
		state_ = STATE_INIT;
		break;
	case STATE_INIT:
		if (!run_replay_)
		{
			current_game_history_.actionhistory.clear();
			current_game_history_.piecelog.clear();
		}
		Clear();
		state_ = STATE_SPAWN_PIECE;
		return;
	case STATE_LOCK_PIECE:
	{
		state_ = STATE_SPAWN_PIECE;
		board_.PutPiece(player_.active_piece_, player_.pos_.x, player_.pos_.y, true);

		if (EliminateRows(true))
			state_ = STATE_LINE_CLEAR;
		game_current_level_++; // +1 for each piece
		game_gravity_ = GetGravityForLevel(game_current_level_);
		game_forcelock_ = false;
		player_.NextPiece();
		player_.can_rotate_ = false;
		break;
	}
	case STATE_SPAWN_PIECE:
		state_ = STATE_GAMETICK;
		//
		// Entry delay
		//
		if (counter_.are_counter-- > 0)
		{
			state_ = STATE_SPAWN_PIECE;
			frame_state_.in_are = true;
			//logger_.Debug("are wait: %d", counter_.are_counter);
		}
		else
		{
			player_.OnNewRound();
			if (m_InputStates[INPUT_MOVE_LEFT] == INPUT_STATE_ON)
			{
				logger_.Debug("initial movement left");
				player_.move_shift_counter_ = 0;
			}
			else if (m_InputStates[INPUT_MOVE_RIGHT] == INPUT_STATE_ON)
			{
				logger_.Debug("initial movement right");
				player_.move_shift_counter_ = 0;
			}

			counter_.are_counter = timing_.are_delay;

			// IHS: Initial hold state.
			if (m_InputStates[INPUT_HOLD_PIECE] == INPUT_STATE_ON)
			{
				logger_.Debug("Initial hold state.");
				player_.HoldSwap();
				player_.can_use_holdswap_ = false;
			}

			// Initial rotation state IRS
			// No kick is allowed in IRS
			if (m_InputStates[INPUT_ROTATE_LEFT] == INPUT_STATE_ON)
			{
				logger_.Debug("Initial rotation state - left");
				player_.Rotate(INPUT_ROTATE_LEFT, false);
			}
			else if (m_InputStates[INPUT_ROTATE_RIGHT] == INPUT_STATE_ON)
			{
				logger_.Debug("Initial rotation state - right");
				player_.Rotate(INPUT_ROTATE_RIGHT, false);
			}
		}

		break;
	case STATE_LINE_CLEAR:
		if (counter_.line_clear_counter-- > 0)
		{
			state_ = STATE_LINE_CLEAR;
		}
		else
		{
			int num = EliminateRows();
			frame_state_.rows_eliminated = num;
			game_current_level_ += num; // +1 for each line removed
			game_gravity_ = GetGravityForLevel(game_current_level_);
			state_ = STATE_SPAWN_PIECE;
			counter_.line_clear_counter = timing_.lineclear_delay;
			ResetTimers();
		}
		break;
	case STATE_GAMETICK:
	{
		//
		// TODO: Player should not be movable in the first game tick in a new round.
		//
		player_.ProcessGameTick();

		if (frame_state_.in_are)
			player_.move_shift_counter_--;
		else
			game_gravity_accumulator_ += game_gravity_;

		if (!player_.CanMove(INPUT_SOFT_DROP))
		{
			if (player_.pos_.y == 0 && player_.lock_counter_ <= 0)
			{
				state_ = STATE_GAMEOVER;
				if (!run_replay_)
					frame_state_.gameover = true;
				break;
			}
			else if (game_forcelock_ || player_.lock_counter_ <= 0)
			{
				state_ = STATE_LOCK_PIECE;
				player_.lock_counter_ = timing_.piecelock_delay;
			}
			else
				player_.lock_counter_--;
		}
		else
		{
			player_.lock_counter_ = timing_.piecelock_delay;
			while (game_gravity_accumulator_ >= 256)
			{
				game_gravity_accumulator_ -= 256;
				player_.Move(INPUT_SOFT_DROP, true);
			}
		}
		break;
	}}

	if (state_ != STATE_PAUSED && state_ != STATE_SELECT)
		++current_gametick_;
	++current_frame_;
}

int TetosLogic::EliminateRows(bool dry)
{
	int num = 0;
	bool full;

	for (int y = 0; y < 22; y++)
	{
		full = true;
		// check if full
		for (int x = 0; x < 10; x++)
			if (board_.data_[y][x].color == Block::None)
				full = false;

		// if full, remove. should be animated etc.
		if (full)
		{
			if (dry)
				return 1;
			for(int n = y; n > 0; n--)
			{
				for (int x = 0; x < 10; x++)
				{
					board_.data_[n][x].color = board_.data_[n-1][x].color;
					board_.data_[n][x].opacity = board_.data_[n-1][x].opacity;
				}
			}
			num++;
		}
		if (!dry)
			frame_state_.rowstate[y] = full;
	}

	if (!dry)
		game_eliminated_rows_ += num;
	return num;
}

Piece::Type TetosLogic::RandomPiece(std::vector<Piece::Type>& next_pieces)
{
	int rand = Piece::None;
	if (!run_replay_)
	{
		int tries = 0;
		bool ok;
		do
		{
			rand = RandUI32() % 7;
			ok = true;
			for (int n = 0; n < 4; n++)
			{
				if (next_pieces[n] == rand)
				{
					ok = false;
					break;
				}
			}
		} while (++tries < 6 && !ok);
		current_game_history_.piecelog.push_back((Piece::Type)rand);
	}
	else
	{
		if (current_game_history_.piecelog.size() > 0)
		{
			rand = current_game_history_.piecelog.back();
			current_game_history_.piecelog.pop_back();
		}
		else
			logger_.Error("Replay history ran out of pieces.");
	}
	return (Piece::Type) rand;
}