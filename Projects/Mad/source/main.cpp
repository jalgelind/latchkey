#include <Pxf/Pxf.h>
#include <Pxf/Kernel.h>
#include <cstdio>
#include <shared/Kjell.h>

//#include <PreloadedResources.h>

#include <string>

using namespace Pxf;
using namespace Kjell;

int main(int argc, const char** argv)
{
	Pxf::Init(argc, argv);
	Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();
	Pxf::Logger logger = kernel->GetLogger("init");
	Pxf::InitModules();
	logger.Information("Initializing...");
	{
		KjellApp* app = KjellApp::Instance();
		if (app->Init())
		{
			//app->Run();
			app->RunVariableFPS();
		}
		else
			logger.Error("Failed to initialize game.");
		delete app;
	}
	logger.Information("Puss puss.");
	Pxf::Shutdown();
	return 0;
}