-- vsproj.py
-- BUILD_TARGET("Native Release", "build-native release", "{project_name}_sr.exe")
-- BUILD_TARGET("Native Debug"  , "build-native debug", "{project_name}_sd.exe")
-- BUILD_TARGET("Native Release DLL", "build-native release_dll", "{project_name}_r.exe")
-- BUILD_TARGET("Native Debug DLL"  , "build-native debug_dll", "{project_name}_d.exe")

-- FIXME: Hack so modules/libs are included
--[[
project:RequireLibrary("lua")
project:RequireLibrary("glfw3")
project:RequireLibrary("sstat")
project:RequireModule("dgfx")
project:RequireModule("ttf")
project:RequireModule("img")
project:RequireModule("snd")
]]

Import("../../Builder.lua")
Import("../Kjell/build-base.lua")

project = KjellProject("mad")
project:AddIncludeDirectory("include")
project:AddSourceDirectory("source/*.cpp")
project:Build()

