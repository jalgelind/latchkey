-- vsproj.py
-- BUILD_TARGET("Native Release", "build-native release", "{project_name}_sr.exe")
-- BUILD_TARGET("Native Debug"  , "build-native debug", "{project_name}_sd.exe")
-- BUILD_TARGET("Native Release DLL", "build-native release_dll", "{project_name}_r.exe")
-- BUILD_TARGET("Native Debug DLL"  , "build-native debug_dll", "{project_name}_d.exe")
-- BUILD_TARGET("VST32 Release" , "build-vst32 shared_release", "{project_name}_ssr.dll", "VST Plugin Analyser.exe")
-- BUILD_TARGET("VST32 Debug"   , "build-vst32 shared_debug", "{project_name}_ssd.dll", "VST Plugin Analyser.exe")

kjelldir = PathDir(ModuleFilename())

Import("../../Builder.lua")

function KjellProject(name)
    local project = NewProject(name)

    project.settings.optimize = 2

    --project.settings.dll.flags:Add("/NODEFAULTLIB:libcmt")
    --project.settings.dll.libs:Add("msvcrt")
    if file_exists("libluajit.a") or file_exists("luajit.lib") then
        project:AddSystemLibrary("luajit")
    end
    project:RequireLibrary("lua")
    project:RequireLibrary("love2d")
    project:RequireLibrary("box2d")
    project:RequireLibrary("glfw3")
    project:RequireLibrary("sstat")
    --project:RequireLibrary("stk")
    project:RequireModule("dgfx")
    project:RequireModule("ttf")
    project:RequireModule("img")
    project:RequireModule("snd")
    project:AddIncludeDirectory(PathJoin(kjelldir, "include/"))
    project:AddIncludeDirectory("../Lapp/include")
    project:AddIncludeDirectory("../Saint/include")
    project:AddSourceDirectory("../Lapp/source/*.cpp")
    project:AddSourceDirectory("../Saint/source/shared/*.cpp")
    project:AddSourceDirectory(PathJoin(kjelldir, "source/shared/*.cpp"))
    return project
end
