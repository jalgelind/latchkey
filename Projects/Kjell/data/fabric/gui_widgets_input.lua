-------------------------------------------------------------------------------------------------------------
-- Text input field
-------------------------------------------------------------------------------------------------------------
function gui:create_textinput(x,y,w,masked,stdvalue,changed) -- changed = function to be called once changed
  local wid = gui:create_basewidget(x,y,w,20)
  wid.changed = changed
  wid.masked = masked -- passwords etc
  wid.stdheight = 20
  wid.selectionheight = 16
  wid.stdpadding = 10
  wid.value = ""
  wid.old_value = nil
  if (stdvalue) then
    wid.value = stdvalue
  end
  wid.state = "normal"
  wid.selection = {start = 0, finish = nil, direction = 0}
  wid.viewstart = 0
  -- key states
  wid.backspace = false
  wid.deletekey = false
  wid.leftkey = false
  wid.rightekey = false
  wid.mouseselect = false
  -- find out max visible char number
  wid.maxvisible = math.floor((w-wid.stdpadding-8) / 8)
  function wid:sanitycheck_selection()
    if (self.selection.finish == self.selection.start) then
      self.selection.finish = nil
    end
    if self.selection.finish then
      if (self.selection.finish < self.selection.start) then
        local tstart = self.selection.start
        self.selection.start = self.selection.finish
        self.selection.finish = tstart
      end
      if (self.selection.finish < 0) then
        self.selection.finish = 0
      elseif (self.selection.finish > #self.value) then
        self.selection.finish = #self.value
      end
    end
    if (self.selection.start < 0) then
      self.selection.start = 0
    elseif (self.selection.start > #self.value) then
      self.selection.start = #self.value
    end
    if (self.selection.start - self.viewstart > self.maxvisible) then
      self.viewstart = self.selection.start - self.maxvisible
    end
    if (self.selection.start < self.viewstart) then
      self.viewstart = self.selection.start
    end
  end

  function wid:mousedrag(dx,dy,button)
    if (button == inp.MOUSE_LEFT) then
      if (self.mouseselect) then
        self:needsredraw()
        -- find selection finish
        local mx,my = inp.getmousepos()
        local dw = mx - self.drawbox.x - self.stdpadding / 2 + 4
        local i = math.floor((dw / (self.drawbox.w - self.stdpadding)) * self.maxvisible)
        local deltaselection = self.viewstart + i
        if (deltaselection <= self.selection.start) then
          if not (self.selection.finish) then
            self.selection.finish = self.selection.start
          end
          self.selection.start = deltaselection
        else
          self.selection.finish = deltaselection
        end
      end
    end
  end

  function wid:mouserelease(mx,my,button)
    self.mouseselect = false
  end
  function wid:mousepush(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      if not (self.mouseselect) then
        self:needsredraw()
        -- new mouse selection
        -- find selection start
        local dw = mx - self.drawbox.x - self.stdpadding / 2 + 4
        local i = math.floor((dw / (self.drawbox.w - self.stdpadding)) * self.maxvisible)
        self.selection.start = self.viewstart + i
        self.selection.finish = nil
        self:sanitycheck_selection()
        self.mouseselect = true
      else
        -- continued selection
      end
    end
  end

  function wid:lostfocus(wid)
    self.state = "normal"
    --if (self.changed) then
    if self.old_value and self.old_value ~= self.value then
      if (self.changed) then
        self:changed(self.value)
      end
    end
    self.old_value = self.value
    self:needsredraw()
  end

  function wid:gotfocus()
    self.state = "input"
    self.backspace = false
    self:needsredraw()
  end

  function wid:update()
    if (self.state == "input") then
      -- special keys
      if (inp.iskeydown(inp.BACKSPACE)) then
        if not (self.backspace) then
          -- delete part of value if we have a range of value selected
          if (self.selection.finish) then
            self.value = string.sub(self.value, 1, self.selection.start) .. string.sub(self.value, self.selection.finish+1)
          else
            self.selection.start = self.selection.start - 1
            if (self.selection.start >= 0) then
              self.value = string.sub(self.value, 1, self.selection.start) .. string.sub(self.value, self.selection.start+2)
            end
          end
          self.selection.finish = nil
        end
        self.backspace = true
        self:needsredraw()
    elseif (inp.iskeydown(inp.ENTER)) then
    if self.changed then
      self:changed(self.value)
    end
      elseif (inp.iskeydown(inp.DEL)) then
        if not (self.deletekey) then
          -- delete part of value if we have a range of value selected
          if (self.selection.finish) then
            self.value = string.sub(self.value, 1, self.selection.start) .. string.sub(self.value, self.selection.finish)
          else
            self.value = string.sub(self.value, 1, self.selection.start) .. string.sub(self.value, self.selection.start+2)
          end
          self.selection.finish = nil

        end
        self.deletekey = true
        self:needsredraw()
      elseif (inp.iskeydown(inp.LEFT)) then
        -- left arrow key
        if (inp.iskeydown(inp.LSHIFT)) then
          -- shift pushed = selection
          if (self.selection.finish == nil) then -- first selection
            self.selection.finish = self.selection.start
            self.selection.start = self.selection.start - 1
            self.selection.direction = -1
          else
            if (self.selection.direction == -1) then
              self.selection.start = self.selection.start - 1
            else
              self.selection.finish = self.selection.finish - 1
            end
          end
        else
          self.selection.finish = nil
          self.selection.start = self.selection.start - 1
        end
        self:needsredraw()
      elseif (inp.iskeydown(inp.RIGHT)) then
        -- right arrow key
        if (inp.iskeydown(inp.LSHIFT)) then
          -- shift pushed = selection
          if not (self.selection.finish) then -- first selection
            self.selection.finish = self.selection.start + 1
            self.selection.direction = 1
          else
            if (self.selection.direction == 1) then
              self.selection.finish = self.selection.finish + 1
            else
              self.selection.start = self.selection.start + 1
            end
          end
        else
          self.selection.finish = nil
          self.selection.start = self.selection.start + 1
        end
        self:needsredraw()
      else
        -- reset special keys
        self.backspace = false
        self.deletekey = false
        -- char inputs
        local c = inp.getlastchar()
        inp.clearlastchar()
        if not (c == 0) then
          -- delete part of value if we have a range of value selected
          if (self.selection.finish) then
            self.value = string.sub(self.value, 1, self.selection.start) .. string.char(c) .. string.sub(self.value, self.selection.finish+1)
          else
            self.value = string.sub(self.value, 1, self.selection.start) .. string.char(c) .. string.sub(self.value, self.selection.start+1)
            self.selection.start = self.selection.start + 1
          end
          self.selection.finish = nil
        end
        self:needsredraw()
      end
      self:sanitycheck_selection()
    end
  end

  function wid:draw(force)
    if (self.redraw_needed or force) then
      gfx.translate(self.drawbox.x, self.drawbox.y)
      -- bg
      --gfx.drawtopleft(0, 0, self.drawbox.w, self.drawbox.h,
      --                40,6,1,1)
      -- bg
      gfx.drawtopleft(1, 1, self.drawbox.w-2, self.drawbox.h-2, 2, 2, 1, 1)
      -- borders
      if self.state == "input" then
        gfx.drawtopleft(1,0,self.drawbox.w-2,1,13,1,1,1) -- top
        gfx.drawtopleft(self.drawbox.w-1,1,1,self.drawbox.h-2,13,1,1,1) -- right
        gfx.drawtopleft(1,self.drawbox.h-1,self.drawbox.w-2,1,13,1,1,1) -- bottom
        gfx.drawtopleft(0,1,1,self.drawbox.h-2,13,1,1,1) -- right
      else
        gfx.drawtopleft(1,0,self.drawbox.w-2,1,1,5,1,1) -- top
        gfx.drawtopleft(self.drawbox.w-1,1,1,self.drawbox.h-2,1,5,1,1) -- right
        gfx.drawtopleft(1,self.drawbox.h-1,self.drawbox.w-2,1,1,5,1,1) -- bottom
        gfx.drawtopleft(0,1,1,self.drawbox.h-2,1,5,1,1) -- right
      end
      local oldtex = gfx.bindtexture(0)
      local out_str = self.value
      -- find out what is visible
      out_str = string.sub(self.value, self.viewstart+1, self.viewstart+1+self.maxvisible)
      -- password?
      if (self.masked) then
        local new_outstr = ""
        for i=1,#out_str do
          new_outstr = new_outstr .. self.masked
        end
        out_str = new_outstr
      end
      if not (self.selection.finish == nil) then
        -- selected range
        local r,g,b = gfx.getcolor()
        gfx.setcolor(0.8,0.5,0.5)
        local sx1 = self.stdpadding / 2 + (self.selection.start - self.viewstart)*8
        local sx2 = self.stdpadding / 2 + (self.selection.finish - self.viewstart)*8
        if (sx1 < self.stdpadding / 2) then
          sx1 = self.stdpadding / 2
        elseif (sx1 > self.drawbox.w - self.stdpadding / 2) then
          sx1 = self.drawbox.w - self.stdpadding / 2
        end
        if (sx2 < self.stdpadding / 2) then
          sx2 = self.stdpadding / 2
        elseif (sx2 > self.drawbox.w - self.stdpadding / 2) then
          sx2 = self.drawbox.w - self.stdpadding / 2
        end
        gfx.drawtopleft(sx1, self.drawbox.h / 2 - self.selectionheight / 2, sx2-sx1, self.selectionheight,
                        17,6,1,1)
        gfx.setcolor(r,g,b)
      end
      -- render visible string
      gui:drawfont(out_str, self.stdpadding / 2+4, self.drawbox.h / 2)

      if (self.state == "input" and not self.selection.finish) then
        local r,g,b = gfx.getcolor()
        gfx.setcolor(0.8,0.5,0.5)
        gui:drawfont("-", self.stdpadding / 2 + 4 + (self.selection.start - self.viewstart)*8, self.drawbox.h / 2+4);
        gfx.setcolor(r,g,b)
      end
      gfx.bindtexture(oldtex)
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
end

