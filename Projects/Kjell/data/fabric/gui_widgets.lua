
----------------------------------------------
-- base classes for widgets

function gui:create_basewidget(x,y,w,h)
  local wid = {
    hitbox = {x = x, y = y, w = w, h = h},
    drawbox = {x = x, y = y, w = w, h = h},
    parent = nil,
    redraw_needed = false,
    visible = true,
    widget_type = "stdwidget" -- stdwidget, menu
  }

  function wid:destroy()
    self:needsredraw()

    -- destroy childs
    for k,v in pairs(self.childwidgets) do
      v:destroy()
    end

    -- destroy self from parent
    if self.parent then
      --self.parent:removewidget(self)
      local deletek = nil
      for k,v in pairs(self.parent.childwidgets) do
        if v == self then
          deletek = k
          break
        end
      end
      if deletek then
        self.parent.childwidgets[deletek] = nil
      end
    end
  end

  -- child widget control
  wid.childwidgets = {}
  function wid:addwidget(cwid,key)
    cwid.parent = self
    gui:set_focus(cwid)
    cwid:needsredraw()
    if key then
        self.childwidgets[key] = cwid
    else
        table.insert(self.childwidgets, cwid)
    end
end

function wid:removewidget(cwid)
  local find_k = nil
  if not cwid then
    return nil
  end

  for k,v in pairs(self.childwidgets) do
    if (v == cwid) then
        find_k = k
        break
    end
  end

  if find_k then
    self.childwidgets[find_k] = nil
  end
end

  -----------------------------------
  -- update functions
  --  (can be useful for animation or input)
  function wid:update()
    for k,v in pairs(self.childwidgets) do
          v:update(force)
        end
  end

  -----------------------------------
  -- store key shortcuts in a table
  wid.shortcuts = {} -- { { keys = {inp.LSHIFT, 'C'}, onpress = function () print("LOL SUP") end} }

  -----------------------------------
  -- redraw functions
  function wid:needsredraw(full)
    if (full) then
      gui:redraw()
    else
      local x,y = self:find_abspos(self)
      gui:redraw(x, y, self.drawbox.w, self.drawbox.h)
    end
    self.redraw_needed = true
    -- notify parent
    if not (self.parent == nil) then
      self.parent:childisredrawn()
    end
  end

  function wid:childisredrawn()
    -- do nothing as standard
  end

  function wid:resetredraw()
    self.redraw_needed = false
    for k,v in pairs(self.childwidgets) do
      v:resetredraw()
    end
  end

  function wid:find_abspos(sender)
    local x,y
    x = self.drawbox.x
    y = self.drawbox.y
    if not (self.parent == nil) then
      local tx,ty = self.parent:find_abspos(sender)
      x = x + tx
      y = y + ty
    end
    return x,y
  end

  function wid:move_relative(x,y)
    self.hitbox.x = self.hitbox.x + x
    self.hitbox.y = self.hitbox.y + y
    self.drawbox.x = self.drawbox.x + x
    self.drawbox.y = self.drawbox.y + y
  end

  function wid:move_abs(x,y)
    self.hitbox.x = x
    self.hitbox.y = y
    self.drawbox.x = x
    self.drawbox.y = y
  end

  function wid:resize_relative(w,h)
    self.hitbox.w = self.hitbox.w + w
    self.hitbox.h = self.hitbox.h + h
    self.drawbox.w = self.drawbox.w + w
    self.drawbox.h = self.drawbox.h + h
    -- notify parent
    if not (self.parent == nil) then
      self.parent:child_resized(self)
    end
  end

  function wid:resize_abs(w,h)
    self.hitbox.w = w
    self.hitbox.h = h
    self.drawbox.w = w
    self.drawbox.h = h
    -- notify parent
    if not (self.parent == nil) then
      self.parent:child_resized(self)
    end
  end
  function wid:child_resized(cwid)
    -- do nothing ?

  end

  function wid:resize_callback(w,h,edge)
    for k,v in pairs(self.childwidgets) do
        v:resize_callback(w,h)
    end
  end

  -- end of redraw functions
  ----------------------------------

  function wid:draw(force)
    if (self.redraw_needed or force) and self.visible then
        if (gui.draw_debug_rects) then
            gfx.drawtopleft(self.drawbox.x, self.drawbox.y, self.drawbox.w, 1, 5, 5, 1, 1) -- top
            gfx.drawtopleft(self.drawbox.x, self.drawbox.y, 1, self.drawbox.h, 5, 5, 1, 1) -- left
            gfx.drawtopleft(self.drawbox.x + self.drawbox.w, self.drawbox.y, 1,self.drawbox.h, 5, 5, 1, 1) -- right
            gfx.drawtopleft(self.drawbox.x, self.drawbox.y + self.drawbox.h, self.drawbox.w, 1, 5, 5, 1, 1) -- bottom
        end
        if (gui.draw_hitbox_rects) then
            gfx.drawtopleft(self.hitbox.x, self.hitbox.y, self.hitbox.w, 1, 17, 5, 1, 1) -- top
            gfx.drawtopleft(self.hitbox.x, self.hitbox.y, 1, self.hitbox.h, 17, 5, 1, 1) -- left
            gfx.drawtopleft(self.hitbox.x + self.hitbox.w, self.hitbox.y, 1,self.hitbox.h, 17, 5, 1, 1) -- right
            gfx.drawtopleft(self.hitbox.x, self.hitbox.y + self.hitbox.h, self.hitbox.w, 1, 17, 5, 1, 1) -- bottom
        end
        gfx.translate(self.drawbox.x, self.drawbox.y)
        for k,v in pairs(self.childwidgets) do
          v:draw(force)
        end
        gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end

  function wid:hittest(x0,y0,x1,y1)
    if not self.visible then
        return false
    end
    if (x1 < self.hitbox.x) then
      return false
    elseif (x0 > self.hitbox.x + self.hitbox.w) then
      return false
    elseif (y1 < self.hitbox.y) then
      return false
    elseif (y0 > self.hitbox.y + self.hitbox.h) then
      return false
    end
    return true
  end

  function wid:hittest_d(x0,y0,x1,y1) -- drawbox hittest
    if not self.visible then
        return false
    end
    if (x1 < self.drawbox.x) then
      return false
    elseif (x0 > self.drawbox.x + self.drawbox.w) then
      return false
    elseif (y1 < self.drawbox.y) then
      return false
    elseif (y0 > self.drawbox.y + self.drawbox.h) then
      return false
    end
    return true
  end

  function wid:find_mousehit(mx,my)
    if (self:hittest(mx,my,mx,my)) then
      local thit = nil
      for k,v in pairs(self.childwidgets) do
        if v then
          local htest = v:find_mousehit(mx - self.hitbox.x, my - self.hitbox.y)
          if htest then
            thit = htest
          end
        end
      end
      if not (thit == nil) then
        -- we hit a child widget, return this one instead
        return thit
      end
      return self
    end
    return nil
  end

  function wid:find_redrawhit(rx0,ry0,rx1,ry1)
    -- should we redraw?
    if (self:hittest_d(rx0,ry0,rx1,ry1)) then
      self.redraw_needed = true
    end
    -- should any of our childs redraw?    
    for k,v in pairs(self.childwidgets) do
      v:find_redrawhit(rx0 - self.drawbox.x, ry0 - self.drawbox.y, rx1 - self.drawbox.x, ry1 - self.drawbox.y)
    end
  end
  function wid:mouseleave(mx,my)
    --print(self.widget_type)
  end
  function wid:mouseover(mx,my)
  end
  return wid
end

function gui:create_root()
  local rootwid = self:create_basewidget(0, 0, app.width, app.height)
  -- child widget control
  rootwid.super_addwidget = rootwid.addwidget
  function rootwid:addwidget(cwid)
    self:super_addwidget(cwid)
    gui:set_focus(cwid)
  end
  function rootwid:draw(force)
    --[[local r,g,b = gfx.getcolor()
    local oldtex = gfx.bindtexture(0)
    gfx.setcolor(5/255,5/255,5/255)
    gfx.drawtopleft(0, 0, self.drawbox.w, self.drawbox.h)
    gfx.setcolor(r,g,b)
    gfx.bindtexture(oldtex)]]

    for k,v in pairs(self.childwidgets) do
      gfx.translate(self.drawbox.x, self.drawbox.y)
      v:draw(force)
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end

  end

  function rootwid:find_mousehit(mx,my)
    if (self:hittest(mx,my,mx,my)) then
      local thit = nil
      for k,v in pairs(self.childwidgets) do
        if v then
          local htest = v:find_mousehit(mx - self.hitbox.x, my - self.hitbox.y)
          if htest then
            thit = htest
          end
        end
      end
      if not (thit == nil) then
        -- we hit a child widget, return this one instead
        return thit
      end
      return nil -- always return nil for root object, since the root does not have any "input" (ie. click through it)
    end
    return nil
  end
  return rootwid
end


-- creates scrollable panel (scrollable in all directions)
--[[function gui:create_panel(x,y,w,h)
  local wid = gui:create_basewidget(x,y,w,h)
  function wid:mousedrag(dx,dy,button)
    if (button == inp.MOUSE_MIDDLE) then
      self:needsredraw()
      self.drawbox.x = self.drawbox.x + dx
      self.drawbox.y = self.drawbox.y + dy
      self.hitbox.x = self.hitbox.x + dx
      self.hitbox.y = self.hitbox.y + dy
    end
  end
  wid.superdraw = wid.draw
  function wid:draw()
    if (self.redraw_needed) then
      local r,g,b = gfx.getcolor()
      gfx.setcolor(46/256,46/256,46/256)
      gfx.drawtopleft(self.drawbox.x, self.drawbox.y, self.drawbox.w, self.drawbox.h,
                      18,2,1,1)
      gfx.setcolor(r,g,b)
    end
    self:superdraw()
  end
  return wid
end]]

-------------------------------------------------------------------------------------------------------------
-- Scrollable horizontal panel
-------------------------------------------------------------------------------------------------------------

function gui:create_horizontalpanel(x,y,w,h,max)
  local wid = gui:create_basewidget(x,y,w,h)
  wid.offset = 0
  wid.max = max

  function wid:mousedrag(dx,dy,button)
    if (button == inp.MOUSE_MIDDLE) then -- TODO: change this to MOUSE_MIDDLE
      --self.drawbox.x = self.drawbox.x + dx
      self.offset = self.offset + dx
      if (self.offset < -self.max) then
        self.offset = -self.max
      elseif (self.offset > 0) then
        self.offset = 0
      end
      self:needsredraw()
    end
  end

  function wid:needsredraw()
    local x,y = self:find_abspos(self)
    gui:redraw(x, y, self.drawbox.w, self.drawbox.h)
    self.redraw_needed = true
    -- notify parent
    if not (self.parent == nil) then
      self.parent:childisredrawn()
    end
  end

  function wid:find_abspos(sender)
    local x,y
    x = self.drawbox.x
    if not (sender == self) then
      x = x + self.offset
    end
    y = self.drawbox.y
    if not (self.parent == nil) then
      local tx,ty = self.parent:find_abspos(sender)
      x = x + tx
      y = y + ty
    end
    return x,y
  end

  -- wid.superdraw = wid.draw
  function wid:draw(force)
    if self.visible then
      if (self.redraw_needed or force) then
        local r,g,b = gfx.getcolor()
        gfx.setcolor(46/256,46/256,46/256)
        gfx.drawtopleft(self.drawbox.x, self.drawbox.y, self.drawbox.w, self.drawbox.h,
                        18,2,1,1)
        gfx.setcolor(r,g,b)
      end
      gfx.translate(self.drawbox.x + self.offset, self.drawbox.y)
      for k,v in pairs(self.childwidgets) do
        v:draw(force)
      end
      gfx.translate(-(self.drawbox.x + self.offset), -self.drawbox.y)
    end
  end

  function wid:find_mousehit(mx,my)
    if (self:hittest(mx,my,mx,my)) then
      local thit = nil
      for k,v in pairs(self.childwidgets) do
      --for i = #self.childwidgets, 1, -1 do
        --local v = self.childwidgets[i]
        local htest = v:find_mousehit(mx - (self.hitbox.x + self.offset), my - self.hitbox.y)
        if htest then
          thit = htest
        end
      end
      if not (thit == nil) then
        -- we hit a child widget, return this one instead
        return thit
      end
      return self
    end
    return nil
  end
  return wid
end

-------------------------------------------------------------------------------------------------------------
-- Creates a widget stack that displays all widgets as a "stack"
-------------------------------------------------------------------------------------------------------------
function gui:create_verticalstack(x,y,w,h)
  local wid = gui:create_basewidget(x,y,w,h)
  function wid:addwidget(cwid)
    cwid.parent = self
    local offsety = 0
    for k,v in pairs(self.childwidgets) do
      offsety = offsety + v.drawbox.h
    end
    cwid:move_abs(0, offsety)
    offsety = offsety + cwid.drawbox.h
    self:resize_abs(self.drawbox.w, offsety)
    table.insert(self.childwidgets, cwid)
  end
  function wid:child_resized(cwid)
    local offsety = 0
    for k,v in pairs(self.childwidgets) do
      v:move_abs(v.drawbox.x, offsety)
      offsety = offsety + v.drawbox.h
    end
    self:needsredraw()
    self:resize_abs(self.drawbox.w, offsety)
    self:needsredraw()
  end
  function wid:childisredrawn()
    if not self.redraw_needed then
      self:needsredraw()
    end
  end
  --[[function wid:find_mousehit(mx,my)
    if (self:hittest(mx,my,mx,my)) then
      local thit = nil
      local total_offset = 0
      for k,v in pairs(self.childwidgets) do
        thit = v:find_mousehit(mx - self.hitbox.x, my - (self.hitbox.y + total_offset))
        total_offset = total_offset + v.drawbox.h
        if not (thit == nil) then
          -- we hit a child widget, return this one instead
          return thit
        end
      end
      return self
    end
    return nil
  end]]
  return wid
end

-------------------------------------------------------------------------------------------------------------
-- creates a widget stack that displays all widgets as a "stack" but sideways
-------------------------------------------------------------------------------------------------------------
function gui:create_horizontalstack(x,y,w,h)
  local wid = gui:create_basewidget(x,y,w,h)
  function wid:addwidget(cwid,location)
    cwid.parent = self
    local offsetx = 0
    for k,v in pairs(self.childwidgets) do
      offsetx = offsetx + v.drawbox.w
    end
    cwid:move_abs(offsetx, 0)
    offsetx = offsetx + cwid.drawbox.w
    --self:resize_abs(offsetx, self.drawbox.h)
    table.insert(self.childwidgets, cwid)
  --print(cwid.widget_type .. ": " .. offsetx)
  end
   function wid:removewidget(cwid)
  local find_k = nil
  if not cwid then
    return nil
  end
  -- make sure we find
  for k,v in pairs(self.childwidgets) do
    if (v == cwid) then
      find_k = k
      break
    end
  end
  if find_k then
    --self.childwidgets[find_k] = nil
    table.remove(self.childwidgets, find_k)
    local offset_x = 0
    for k,v in pairs(self.childwidgets) do
    end
  end
  end

  function wid:child_resized(cwid)
    local offsetx = 0
    for k,v in pairs(self.childwidgets) do
      v:move_abs(v.drawbox.x, offsetx)
      offsetx = offsetx + v.drawbox.w
    end
    --self:needsredraw()
    --self:resize_abs(offsetx, self.drawbox.h)
    self:needsredraw()
  end
  function wid:childisredrawn()
    if not self.redraw_needed then
      self:needsredraw()
    end
  end
  return wid
end



-------------------------------------------------------------------------------------------------------------
-- Create status bar
-------------------------------------------------------------------------------------------------------------
function gui:create_statusbar(x,y,w,h,default_text)
  local wid = gui:create_basewidget(x,y,w,h)
  wid.default_text = default_text
  wid.text = default_text

  function wid:settext(text)
    if (text == nil) then
      self.text = self.default_text
    else
      self.text = text
    end
  end

  function wid:draw(force)
    if (self.redraw_needed or force) then
      gfx.translate(self.drawbox.x, self.drawbox.y)
      local r,g,b = gfx.getcolor()
      gfx.setcolor(0.6,0.6,0.6)
      gui:drawfont(self.text,24,12)
      gfx.setcolor(r,g,b)

      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
end



-------------------------------------------------------------------------------------------------------------
-- Simple console output
-------------------------------------------------------------------------------------------------------------
function gui:create_console(x,y,w,h,open_state)
  local wid = gui:create_basewidget(x,y,w,h)
  wid.open_state = open_state
  wid.stdheight = h
  wid.minheight = 5
  wid.consolelines = {}

  if open_state then
    wid.drawbox.h = wid.stdheight
    wid.hitbox.h = wid.stdheight
  else
    wid.drawbox.h = wid.minheight
    wid.hitbox.h = wid.minheight
  end

  function wid:addline(str)
    table.insert(self.consolelines, str)
  end

  function wid:draw(force)
    gfx.translate(self.drawbox.x, self.drawbox.y)
    if (self.redraw_needed or force) then
      --panic.text("wut", 16, 16)
      local r,g,b = gfx.getcolor()
      gfx.setcolor(26/256,26/256,26/256)
      gfx.drawtopleft(0, 0, self.drawbox.w, self.drawbox.h,18,2,1,1)
      gfx.setcolor(r,g,b)
      if (self.open_state) then
        for k,v in pairs(self.consolelines) do
          panic.text(v, 16, 16*k)
        end
      else
        -- TODO: Draw something special when hidden?
      end
    end
    gfx.translate(-self.drawbox.x, -self.drawbox.y)
  end

  function wid:mouserelease(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self.open_state = not self.open_state
      if (self.open_state) then
        --self.drawbox.h = self.stdheight
        --self.hitbox.h = self.stdheight
        self:resize_abs(self.drawbox.w, self.stdheight)
      else
        --self.drawbox.h = 10
        --self.hitbox.h = 10
        self:resize_abs(self.drawbox.w, self.minheight)
      end
      self:needsredraw()
    end
  end
  return wid
end


-------------------------------------------------------------------------------------------------------------
-- Create a centered multiline label
-------------------------------------------------------------------------------------------------------------
function gui:create_centeredmultiline_label(x,y,w,h,lines)
  local base_widget = gui:create_basewidget(x,y,w,h)
  base_widget.lines = lines
  base_widget.superdraw = base_widget.draw
  function base_widget:draw()
    gfx.translate(self.drawbox.x,self.drawbox.y)
    for k,v in pairs(self.lines) do
      gui:drawcenteredfont(v, self.drawbox.w / 2, 12+16*(k-1))
    end
    gfx.translate(-self.drawbox.x,-self.drawbox.y)
    base_widget:superdraw()
  end
  return base_widget
end


-------------------------------------------------------------------------------------------------------------
-- Create a multiline label
-------------------------------------------------------------------------------------------------------------
function gui:create_multiline_label(x,y,w,h,lines)
  local base_widget = gui:create_basewidget(x,y,w,h)
  base_widget.lines = lines
  base_widget.superdraw = base_widget.draw

  function base_widget:draw()
    gfx.translate(self.drawbox.x,self.drawbox.y)
    local max_len = math.floor(w / 8)
    for k,v in pairs(self.lines) do
      local len = gui:get_font_length(v)
      local out_str = v
      if (len > max_len) then
        out_str = string.sub(out_str, 1, max_len)
      end
      gui:drawfont(tostring(out_str), 12, 12+16*(k-1))
    end
    gfx.translate(-self.drawbox.x,-self.drawbox.y)
    base_widget:superdraw()
  end
  return base_widget
end


-------------------------------------------------------------------------------------------------------------
-- Create a centered label panel
-------------------------------------------------------------------------------------------------------------
function gui:create_centeredlabelpanel(x,y,w,h,text)
  local base_widget = gui:create_basewidget(x,y,w,h)
  base_widget.label_text = text
  base_widget.superdraw = base_widget.draw
  function base_widget:draw()
    gfx.translate(self.drawbox.x,self.drawbox.y)
    local maxlen = math.ceil(self.drawbox.w / 12) - 3
    local shortlabel = self.label_text--string.sub(self.label_text,-maxlen)
    if (maxlen < gui:get_font_length(self.label_text)) then
      shortlabel = "..." .. string.sub(shortlabel,-maxlen)
    end
    gui:drawcenteredfont(shortlabel, self.drawbox.w / 2, 12)
    gfx.translate(-self.drawbox.x,-self.drawbox.y)
    base_widget:superdraw()
  end
  return base_widget
end


-------------------------------------------------------------------------------------------------------------
-- Create a label panel
-------------------------------------------------------------------------------------------------------------
function gui:create_labelpanel(x,y,w,h,text)
  local base_widget = gui:create_basewidget(x,y,w,h)
  base_widget.label_text = text
  base_widget.superdraw = base_widget.draw
  function base_widget:draw()
    gfx.translate(self.drawbox.x,self.drawbox.y)
    gui:drawfont(self.label_text, 12, self.drawbox.h / 2)
    gfx.translate(-self.drawbox.x,-self.drawbox.y)
    base_widget:superdraw()
  end
  function base_widget:find_mousehit(mx,my)
    if (self:hittest(mx,my,mx,my)) then
      local thit = nil
      for k,v in pairs(self.childwidgets) do
      --for i = #self.childwidgets, 1, -1 do
        --local v = self.childwidgets[i]
        if not (v == nil) then
          local htest = v:find_mousehit(mx - self.hitbox.x, my - self.hitbox.y)
          if htest then
            thit = htest
          end
        end
      end
      if not (thit == nil) then
        -- we hit a child widget, return this one instead
        return thit
      end
      return nil
    end
    return nil
  end
  return base_widget
end


-------------------------------------------------------------------------------------------------------------
-- Create a static panel
-------------------------------------------------------------------------------------------------------------
function gui:create_staticpanel(x,y,w,h)
  local base_widget = gui:create_basewidget(x,y,w,h)
  base_widget.superdraw = base_widget.draw
  function base_widget:draw()
    gfx.translate(self.drawbox.x,self.drawbox.y)
    -- bg
    gfx.drawtopleft(2, 2, self.drawbox.w-4, self.drawbox.h-4,  512,1,1,254)
    -- topleft
    gfx.drawtopleft(0, 0, 5,                                   5,0,0,5,5)
    -- topright
    gfx.drawtopleft(self.drawbox.w-5, 0, 5,                    5,9,0,5,5)
    -- top
    gfx.drawtopleft(5, 0, self.drawbox.w-10,                   5,5,0,1,5)
    -- bottomleft
    gfx.drawtopleft(0, self.drawbox.h-5, 5,                    5,0,9,5,5)
    -- bottomright
    gfx.drawtopleft(self.drawbox.w-5, self.drawbox.h-5, 5,     5,9,9,5,5)
    -- top
    gfx.drawtopleft(5, self.drawbox.h-5, self.drawbox.w-10,    5,5,9,1,5)
    -- left
    gfx.drawtopleft(0, 5, 5,                                   self.drawbox.h-10,0,5,5,1)
    -- right
    gfx.drawtopleft(self.drawbox.w-5, 5, 5,                    self.drawbox.h-10,9,5,5,1)
    gfx.translate(-self.drawbox.x,-self.drawbox.y)
    self:superdraw()
  end
  return base_widget
end


-------------------------------------------------------------------------------------------------------------
-- Create a movable panel
-------------------------------------------------------------------------------------------------------------
function gui:create_movablepanel(x,y,w,h)
  local base_widget = gui:create_basewidget(x,y,w,h)

  function base_widget:mousedrag(dx,dy,button)
    if (button == inp.MOUSE_LEFT) then
      if (gui.snap_to_grid) then
        local x,y = inp.getmousepos()
        local cell_x = math.ceil(x / gui.grid_size)
        local cell_y = math.ceil(y / gui.grid_size)

        -- Note: add offset from point clicked within the component
        -- Right now, its just moving the box
        self:needsredraw()
        self.drawbox.x = cell_x * gui.grid_size
        self.hitbox.x = cell_x * gui.grid_size
        self.drawbox.y = cell_y * gui.grid_size
        self.hitbox.y = cell_y * gui.grid_size
        self:needsredraw()
      else
        self:needsredraw()
        self.drawbox.x = self.drawbox.x + dx
        self.drawbox.y = self.drawbox.y + dy
        self.hitbox.x = self.hitbox.x + dx
        self.hitbox.y = self.hitbox.y + dy
        self:needsredraw()
      end
    end
  end

  function base_widget:draw()
    gfx.translate(self.drawbox.x,self.drawbox.y)

    -- bg
    gfx.drawtopleft(2, 2, self.drawbox.w-4, self.drawbox.h-4, 512,1,1,254)
    -- topleft
    gfx.drawtopleft(0, 0, 5,                                  5,0,0,5,5)
    -- topright
    gfx.drawtopleft(self.drawbox.w-5, 0, 5,                   5,9,0,5,5)
    -- top
    gfx.drawtopleft(5, 0, self.drawbox.w-10,                  5,5,0,1,5)
    -- bottomleft
    gfx.drawtopleft(0, self.drawbox.h-5, 5,                   5,0,9,5,5)
    -- bottomright
    gfx.drawtopleft(self.drawbox.w-5, self.drawbox.h-5, 5,    5,9,9,5,5)
    -- top
    gfx.drawtopleft(5, self.drawbox.h-5, self.drawbox.w-10,   5,5,9,1,5)
    -- left
    gfx.drawtopleft(0, 5, 5,                                  self.drawbox.h-10,0,5,5,1)
    -- right
    gfx.drawtopleft(self.drawbox.w-5, 5, 5,                   self.drawbox.h-10,9,5,5,1)

    gfx.translate(-(self.drawbox.x),-(self.drawbox.y))
  end
  return base_widget
end


-------------------------------------------------------------------------------------------------------------
-- Create a labeled button
-------------------------------------------------------------------------------------------------------------
function gui:create_labelbutton(x,y,w,h,label,action)
  local wid = gui:create_basewidget(x,y,w,h)
  wid.action = action
  wid.label = label
  wid.state = 0 -- 0 = up, 1 = down

  function wid:mouserelease(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self:action(mx,my,button)
    end
    self.state = 0
    self:needsredraw()
  end

  function wid:mousepush(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self.state = 1
      self:needsredraw()
    end
  end

  function wid:draw(force)
    if (self.redraw_needed or force) then
      gfx.translate(self.drawbox.x, self.drawbox.y)
      -- bg
      gfx.drawtopleft(2,2,self.drawbox.w-4,self.drawbox.h-4, 507,0,0,128) -- upper left corner
      --[[if (self.state == 0) then
        gfx.drawtopleft(2, 2, self.drawbox.w-4, self.drawbox.h-4,
                        512,1,1,254)
      else
        gfx.drawtopleft(2, 2, self.drawbox.w-4, self.drawbox.h-4,
                        510,1,0,254)
      end]]
      -- draw borders
      gfx.drawtopleft(0, 0, 2, 2, 5,10,2,2) -- upper left corner
      gfx.drawtopleft(self.drawbox.w - 2, 0, 2, 2,10,10,2,2) -- upper right corner
      gfx.drawtopleft(0, self.drawbox.h-2, 2, 2,5,15,2,2) -- lower left corner
      gfx.drawtopleft(self.drawbox.w-2,self.drawbox.h-2,2,2,10,15,2,2) -- lower right corner
      gfx.drawtopleft(0,2,2,self.drawbox.h-4,5,12,2,1) -- left frame  
      gfx.drawtopleft(self.drawbox.w-2,2,2,self.drawbox.h-4,10,12,2,1) -- right frame 
      gfx.drawtopleft(2,0,self.drawbox.w-4,2,7,10,1,2) -- upper frame
      gfx.drawtopleft(2,self.drawbox.h - 2,self.drawbox.w-4,2,7,15,1,2) -- lower frame)
      -- topleft
      --[[gfx.drawtopleft(0, 0, 4, 4,
                      0,0,4,4)
      -- topright
      gfx.drawtopleft(self.drawbox.w-4, 0, 4, 4,
                      10,0,4,4)
      -- top
      gfx.drawtopleft(4, 0, self.drawbox.w-8, 4,
                      7,0,1,4)
      -- bottomleft
      gfx.drawtopleft(0, self.drawbox.h-4, 4, 4,
                      0,10,4,4)
      -- bottomright
      gfx.drawtopleft(self.drawbox.w-4, self.drawbox.h-4, 4, 4,
                      10,10,4,4)
      -- bottom
      gfx.drawtopleft(4, self.drawbox.h-4, self.drawbox.w-8, 4,
                      7,10,1,4)
      -- left
      gfx.drawtopleft(0, 4, 4, self.drawbox.h-8,
                      0,7,4,1)
      -- right
      gfx.drawtopleft(self.drawbox.w-4, 4, 4, self.drawbox.h-8,
                      10,7,4,1)]]
      -- label
      if (self.state == 0) then
        gui:drawcenteredfont(self.label, self.drawbox.w / 2 - 1, self.drawbox.h / 2)
      else
        gui:drawcenteredfont(self.label, self.drawbox.w / 2, self.drawbox.h / 2 + 1)
      end
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
end


-------------------------------------------------------------------------------------------------------------
-- Create an iconic button
-------------------------------------------------------------------------------------------------------------
function gui:create_iconbutton(x,y,w,h,icon_coords,action)
  local wid = gui:create_basewidget(x,y,w,h)
  wid.action = action
  wid.icon_coords = icon_coords
  wid.state = 0 -- 0 = up, 1 = down

  function wid:mouserelease(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self:action(mx,my,button)
    end
    self.state = 0
    self:needsredraw()
  end

  function wid:mousepush(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self.state = 1
      self:needsredraw()
    end
  end

  function wid:draw(force)
    if (self.redraw_needed or force) then
      gfx.translate(self.drawbox.x, self.drawbox.y)
      -- bg
      --[[if (self.state == 0) then
        gfx.drawtopleft(2, 2, self.drawbox.w-4, self.drawbox.h-4,
                        512,1,1,254)
      else
        gfx.drawtopleft(2, 2, self.drawbox.w-4, self.drawbox.h-4,
                        510,1,0,254)
      end]]
      --gfx.drawtopleft(0, 0, self.drawbox.w, self.drawbox.h, 2, 2, 1, 1)
      -- icon
      if (self.state == 0) then
        gfx.drawcentered(self.drawbox.w / 2, self.drawbox.h / 2, self.icon_coords[3], self.icon_coords[4]
                        , self.icon_coords[1], self.icon_coords[2], self.icon_coords[3], self.icon_coords[4]) -- texcoords
      else
        gfx.drawcentered(self.drawbox.w / 2 + 1, self.drawbox.h / 2 + 1, self.icon_coords[3], self.icon_coords[4]
                        , self.icon_coords[1], self.icon_coords[2], self.icon_coords[3], self.icon_coords[4]) -- texcoords
      end
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
end

-------------------------------------------------------------------------------------------------------------
-- Create a checkbox
-------------------------------------------------------------------------------------------------------------
function gui:create_checkbox(x,y,w,label,action)
  local label_size = 0
  if label then
    label_size = #label * 10
  end

  local wid = gui:create_basewidget(x,y,20 + label_size,20)
  wid.action = action
  wid.icon_coords = icon_coords
  wid.state = false -- false = unchecked, true = checked
  wid.icon_coords = {21,11,8,8}

  function wid:toggle(val)
    if (val == nil) then
      if (self.state) then
        self.state = false
      else
        self.state = true
      end
    else
      self.state = val
    end
  end

  function wid:mouserelease(mx,my,button)
    self:toggle()
    if (button == inp.MOUSE_LEFT) then
      self:action(self.state)
    end
    self:needsredraw()
  end

  function wid:mousepush(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self:needsredraw()
    end
  end

  function wid:draw(force)
    if (self.redraw_needed or force) then
      gfx.translate(self.drawbox.x, self.drawbox.y)
    --gfx.drawtopleft(self.drawbox.x,self.drawbox.y,self.drawbox.w,self.drawbox.h,507,0,0,128)
      -- bg
      gfx.drawtopleft(2,2,20-4,self.drawbox.h-4, 507,0,0,128) -- upper left corner
      -- draw borders
      gfx.drawtopleft(0, 0, 2, 2, 5,10,2,2) -- upper left corner
      gfx.drawtopleft(20 - 2, 0, 2, 2,10,10,2,2) -- upper right corner
      gfx.drawtopleft(0, self.drawbox.h-2, 2, 2,5,15,2,2) -- lower left corner
      gfx.drawtopleft(20-2,self.drawbox.h-2,2,2,10,15,2,2) -- lower right corner
      gfx.drawtopleft(0,2,2,self.drawbox.h-4,5,12,2,1) -- left frame
      gfx.drawtopleft(20-2,2,2,self.drawbox.h-4,10,12,2,1) -- right frame
      gfx.drawtopleft(2,0,20-4,2,7,10,1,2) -- upper frame
      gfx.drawtopleft(2,self.drawbox.h - 2,20-4,2,7,15,1,2) -- lower frame)
      -- icon
      if (self.state == true) then
        gfx.drawcentered(20 / 2, self.drawbox.h / 2, self.icon_coords[3], self.icon_coords[4]
                        , self.icon_coords[1], self.icon_coords[2], self.icon_coords[3], self.icon_coords[4]) -- texcoords
      end
    if label then
      gui:drawfont("^(1,1,1){".. label .. "}",30,10)
    end
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
end

-------------------------------------------------------------------------------------------------------------
-- Create progressbar
-------------------------------------------------------------------------------------------------------------
function gui:create_progressbar(x,y,w,h,progress)
  local wid = gui:create_basewidget(x,y,w,h)
  wid.progress = progress
  function wid:draw(force)
    if (self.redraw_needed or force) then
      gfx.translate(self.drawbox.x, self.drawbox.y)
      -- borders
      gfx.drawtopleft(1,0,self.drawbox.w-2,1,13,1,1,1) -- top
      gfx.drawtopleft(self.drawbox.w-1,1,1,self.drawbox.h-2,13,1,1,1) -- right
      gfx.drawtopleft(1,self.drawbox.h-1,self.drawbox.w-2,1,13,1,1,1) -- bottom
      gfx.drawtopleft(0,1,1,self.drawbox.h-2,13,1,1,1) -- right
      -- progress
      gfx.drawtopleft(1, 1, (self.drawbox.w-2)*self.progress, self.drawbox.h-2, 13,1,1,1)
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
end


