-------------------------------------------------------------------------------------------------------------
-- Menu handling
-------------------------------------------------------------------------------------------------------------

-- spawns a menu in the root of the widget tree
function gui:spawn_menu(x,y,menu)
  local wid = gui:create_menu(x,y,menu)
  gui.widgets:addwidget(wid)
end

function gui:spawn_submenu(root,parent,x,y,menu)
  local wid = gui:create_menu(x,y,menu)
  wid.menu_parent = parent
  wid.menu_root = root
  parent.menu_child = wid
  gui.widgets:addwidget(wid)
end

function gui:spawn_primenu(parent,x,y,menu)
  local wid = gui:create_menu(x,y,menu)
  wid.menu_parent = parent
  wid.menu_root = parent
  parent.menu_child = wid
  gui.widgets:addwidget(wid)
end

-- creates a menu
function gui:create_menu(x,y,menu)
  local stdwidth = 200
  local wid = gui:create_basewidget(x,y,stdwidth,10)
  wid.widget_type = "menu"
  wid.menu_parent = nil
  wid.menu_child = nil
  wid.menu_root = nil
  wid.stdwith = stdwidth
  wid.itemheight = 24
  wid.menu = menu
  wid.highlightid = 0
  -- overflow control
  if (x + wid.stdwith > app.width) then
    wid.drawbox.x = app.width - wid.stdwith
    wid.hitbox.x = wid.drawbox.x
  end
  -- set correct height of menu
  local new_h = 0
  for k,v in pairs(menu) do
    new_h = new_h + wid.itemheight
  end
  wid.drawbox.h = new_h
  wid.hitbox.h = new_h
  wid.superdestroy = wid.destroy
  function wid:destroy()
    -- call sub menus to also destroy
    if (self.menu_child) then
      self.menu_child:destroy()
    end
    self:superdestroy()
  if self.destroy_callback then
    self:destroy_callback()
  end
  end
  function wid:mouseover(mx,my)
    -- find correct menu item
    local dh = my - self.drawbox.y
    local i = math.ceil((dh / self.drawbox.h) * #self.menu)

    if not (i == self.highlightid) then
      self.highlightid = i
      self:needsredraw()
      if (self.menu[i]) then
        -- update statusbar
        if (self.menu[i][2].tooltip) then
          gui:statusbarinfo(self.menu[i][2].tooltip)
        end
        -- close all submenus
        if (self.menu_child) then
          self.menu_child:destroy()
        end
        -- if this is a submenu, open it!
        if not (self.menu[i][2].menu == nil) then
          local t_menu_root = self.menu_root
          if (t_menu_root == nil) then
            t_menu_root = self
          end
          gui:spawn_submenu(t_menu_root, -- "menu root"
                            self, -- "menu parent"
                            self.drawbox.x+self.drawbox.w, -- x position
                            self.drawbox.y+(i-1)*self.itemheight, -- y position
                            self.menu[i][2].menu) -- menu table
        end
      end
    end
  end
  function wid:lostfocus(newfocus)
    if (newfocus.widget_type == "menu") then
      return 0
    end
    if (self.menu_parent) then
      self.menu_parent:lostfocus(newfocus)
    end
    self:destroy()
  end
  function wid:mouserelease(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      -- if we have child menus, close them
      if (self.menu_child) then
        self.menu_child:destroy()
      end
      -- find correct menu item
      local dh = my - self.drawbox.y
      local i = math.ceil((dh / self.drawbox.h) * #self.menu)
      if (self.menu[i]) then
        if (self.menu[i][2].menu == nil) then
          -- clickable menu item
          self.menu[i][2]:onclick(self,mx, my, button)
          if ( not (self.menu[i][2].toggle == nil) ) then
            -- execute function
            self.menu[i][2].toggle = not self.menu[i][2].toggle

          -- close the whole tree!
          elseif (self.menu_root) then
            self.menu_root:destroy()
          else
            self:destroy() -- we are the root
          end
          self:needsredraw()
        else
          -- submenu
          local t_menu_root = self.menu_root
          if (t_menu_root == nil) then
            t_menu_root = self
          end
          gui:spawn_submenu(t_menu_root, -- "menu root"
                            self, -- "menu parent"
                            self.drawbox.x+self.drawbox.w, -- x position
              self.drawbox.y, -- y position
                            --self.drawbox.y+(i-1)*self.itemheight+self.itemheight/2, -- y position
                            self.menu[i][2].menu) -- menu table
        end
      end
    end
  end

  function wid:mousepush(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self:needsredraw()
    end
  end

  wid.superdraw = wid.draw
  function wid:draw(force)
    if (self.redraw_needed or force) then
      local old_a = gfx.getalpha()
      gfx.translate(self.drawbox.x, self.drawbox.y)
      gfx.setalpha(0.8)
      gfx.drawtopleft(1,1,self.drawbox.w-2,self.drawbox.h-2,510, 0, 1, 128)
      -- borders
      local r,g,b = gfx.getcolor()
      gfx.setcolor(0.278431373,0.215686275,0.0941176471)
      gfx.drawtopleft(1,0,self.drawbox.w-2,1,5,5,1,1) -- top
      gfx.drawtopleft(self.drawbox.w-1,1,1,self.drawbox.h-2,5,5,1,1) -- right
      gfx.drawtopleft(1,self.drawbox.h-1,self.drawbox.w-2,1,5,5,1,1) -- bottom
      gfx.drawtopleft(0,1,1,self.drawbox.h-2,5,5,1,1) -- right
      gfx.setalpha(old_a)
      gfx.setcolor(r,g,b)

      -- loop through all menu items
      local item_y = 0
      for k,v in pairs(self.menu) do
        if (k == self.highlightid) then
          r,g,b = gfx.getcolor()
          old_a = gfx.getalpha()
          gfx.setalpha(0.6)
          gfx.setcolor(1.0,1.0,1.0)
          gfx.drawtopleft(1, item_y+1, self.drawbox.w-1, self.itemheight-1, 21,5,1,1)
          gfx.setcolor(r,g,b)
          gfx.setalpha(old_a)
        end

        -- item
        gui:drawfont(v[1], 20, item_y + 12)

        -- short
        if not (v[2].shortcut == nil) then
          local r,g,b = gfx.getcolor()
          gfx.setcolor(0.5, 0.5, 0.5)
          gui:drawfont(v[2], self.stdwith-(#v[2]*8), item_y + 12)
          gfx.setcolor(r,g,b)
        elseif not (v[2].menu == nil) then
          gui:drawfont(">", self.stdwith-8, item_y + 12)
        elseif v[2].toggle then
          gui:drawfont("*",10,item_y+14)
        end
        item_y = item_y + self.itemheight
      end
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
    end
  return wid
end

-- Create menu bar
function gui:create_menubar(x,y,w,menus)
  local wid = gui:create_basewidget(x,y,w,30)
  --wid.widget_type = "menu"
  wid.stdheight = 30
  wid.stdpadding = 20
  wid.menuwidth = 0
  wid.menus = menus
  wid.menu_child = nil
  wid.highlightid = 0
  wid.state = "inactive" -- or "active"

  for k,v in pairs(menus) do
    wid.menuwidth = wid.menuwidth + (#v[1]*8) + wid.stdpadding
  end

  function wid:mouseover(mx,my)
    if (self.state == "active") then
      -- find correct menu item
      local offset_x,offset_y = self:find_abspos(self)
      local dw = mx - self.drawbox.x - offset_x
      local i = math.ceil((dw / self.menuwidth) * #self.menus)

      if not (i == self.highlightid) then
        self.highlightid = i
        self:needsredraw()
        if (self.menus[i]) then
          -- close all submenus
          if (self.menu_child) then
            self.menu_child:destroy()
          end
          -- get correct spawn position
          local offset = 0
          for k,v in pairs(self.menus) do
            if (self.menus[i] == v) then
              break
            end
            offset = offset + (#v[1]*8) + self.stdpadding
          end
          -- this is always a submenu
          gui:spawn_primenu(self,offset + offset_x,self.drawbox.y+self.drawbox.h+offset_y,self.menus[i][2])
        end
      end
    end
  end

  function wid:destroy()
    self.state = "inactive"
    self.highlightid = 0
    -- only destroy our child menu
    if (self.menu_child) then
      self.menu_child:destroy()
    end
    self:needsredraw()
  end

  function wid:lostfocus(newfocus)
    if (newfocus.widget_type == "menu") then
      return 0
    else
      -- menus lost focus
      self:destroy()
    end
  end

  function wid:mouserelease(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      -- find correct menu item
      local offset_x,offset_y = self:find_abspos(self)
      local dw = mx - self.drawbox.x - offset_x
      local i = math.ceil((dw / self.menuwidth) * #self.menus)
      if not (i == self.highlightid) then
        self.state = "active"
        self.highlightid = i
        self:needsredraw()
        if (self.menus[i]) then
          -- close all submenus
          if (self.menu_child) then
            self.menu_child:destroy()
          end
          -- get correct spawn position
          local offset = 0
          for k,v in pairs(self.menus) do
            if (self.menus[i] == v) then
              break
            end
            offset = offset + (#v[1]*8) + self.stdpadding
          end
          -- this is always a submenu
          gui:spawn_primenu(self,offset+offset_x,self.drawbox.y+self.drawbox.h+offset_y,self.menus[i][2])
        end
      end
    end
  end

  function wid:mousepush(mx,my,button)
    if (button == inp.MOUSE_LEFT) then
      self:needsredraw()
    end
  end

  wid.superdraw = wid.draw
  function wid:draw(force)
    if (self.redraw_needed or force) and self.visible then
      gfx.translate(self.drawbox.x, self.drawbox.y)
      local old_a = gfx.getalpha()
      -- loop through all menu items
      local item_x = 0
      for k,v in pairs(self.menus) do
        if (k == self.highlightid) then
          gfx.setalpha(0.8)
          local w = (#v[1]*8) + self.stdpadding
          gfx.drawtopleft(item_x,self.drawbox.y,w,self.drawbox.h,510, 0, 1, 128)
          -- borders
          local r,g,b = gfx.getcolor()
          gfx.setcolor(0.278431373,0.215686275,0.0941176471)
          gfx.drawtopleft(item_x+1,self.drawbox.y,w-1,1,5,5,1,1) -- top
          gfx.drawtopleft(item_x,self.drawbox.y+1,1,self.drawbox.h-1,5,5,1,1)
          gfx.drawtopleft(item_x+w,self.drawbox.y+1,1,self.drawbox.h-1,5,5,1,1)
          gfx.drawtopleft(item_x+1,self.drawbox.y + self.drawbox.h,w-1,1,5,5,1,1)
          gfx.setalpha(old_a)
          gfx.setcolor(r,g,b)
          gfx.setalpha(old_a)
          gui:drawcenteredfont(v[1], item_x + ((#v[1]*8) + self.stdpadding) / 2, 15)
        else
          -- item
          local r,g,b = gfx.getcolor()
          gfx.setcolor(0.278431373,0.215686275,0.0941176471)
          gui:drawcenteredfont(v[1], item_x + ((#v[1]*8) + self.stdpadding) / 2, 15)
          gfx.setcolor(r,g,b)
        end
        item_x = item_x + self.stdpadding + (#v[1]*8)
      end
      gfx.translate(-self.drawbox.x, -self.drawbox.y)
    end
  end
  return wid
  --gui.widgets:addwidget(wid)
end

