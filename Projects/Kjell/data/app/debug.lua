require 'app'
require 'gfx'
require 'inp'
require 'rect'

--
-- gui test

function gui_init()
  gui_build()
end

function gui_build()
end

local last_buttons = 0
function gui_update()
  local buttons = inp.get_button_states()
  if last_buttons ~= buttons then
    local btnright = bit.band(buttons, 1) == 1
    local btnmid = bit.band(buttons, 2) == 2
    local btnleft = bit.band(buttons, 4) == 4
  end
  last_buttons = buttons
end

files = app.listdir("data", 3)

function gui_draw()
end


function update()
  gui_update()
end

--

delta = 0

local eng = saint.engine()

local bank = eng:get_samplebank()
local tracks = eng:get_tracks()

eng:reset()

local mzk = bank:load("data/samples/DBD_175_A_MUSIC_LP1.ogg")
local kick = bank:load("data/samples/drums/808-BD1.wav")
local snare = bank:load("data/samples/drums/808-SD1.wav")
local clap = bank:load("data/samples/drums/909-CLP1.wav")
local hh = bank:load("data/samples/drums/909-HH2.wav")
local hho = bank:load("data/samples/drums/909-HHo1.wav")

for i, track in ipairs(tracks) do
  track:get_pattern():clear()
end

-- hard coded filters
--tracks[1]:get_effect(1):set_option(saint.filter.FILTER_MODE, saint.filter.LOWPASS)
--tracks[1]:get_effect(0):set_option(saint.filter.CUTOFF, 20000)
--tracks[1]:get_effect(1):set_option(saint.filter.FILTER_MODE, saint.filter.HIGHPASS)
--tracks[1]:get_effect(1):set_option(saint.filter.CUTOFF, 50)
--tracks[1]:get_effect(2):set_option("CUTOFF", 20000)

tracks[1]:get_sampler():set_sample(mzk)
tracks[1]:get_sampler():get_filter():set_option(saint.filter.CUTOFF, 20000)
tracks[1]:get_sampler():get_filter():set_option(saint.filter.GAIN, 1./math.sqrt(2.))
tracks[1]:get_sampler():update_volume_envelope(2, 3, 0.4, 1)
tracks[1]:get_pattern():set_pattern_tempo(0.5)
tracks[1]:get_pattern():set_step(0, true)

tracks[2]:get_sampler():set_sample(kick)
tracks[2]:get_sampler():update_volume_envelope(0, 0.7, 0.3, 1)
tracks[2]:get_pattern():set_step(0, true)
tracks[2]:get_pattern():set_step(3, true)
tracks[2]:get_pattern():set_step(4+2, true)
tracks[2]:get_pattern():set_step(4+4+4, true)

tracks[3]:get_sampler():set_sample(clap)
tracks[3]:get_pattern():set_pattern_tempo(1)
tracks[3]:get_pattern():set_step(6, true)
tracks[3]:get_pattern():set_step(10, true)
tracks[3]:get_pattern():set_step(15, true)

tracks[4]:get_sampler():set_sample(hh)
tracks[4]:get_pattern():set_step(2, true)
tracks[4]:get_pattern():set_step(6, true)
tracks[4]:get_pattern():set_step(7, true)
tracks[4]:get_pattern():set_step(9, true)
tracks[4]:get_pattern():set_step(14, true)


eng:set_tempo(175)
eng:set_state("play")

local tempo = eng:get_tempo()

eng:save_project("testproj.json")

local r = rect.new(0, 0, 400, 400)
local top, bot = rect.split_htop(r, 200)

function draw()
  gfx.clear()
  delta = delta + inp.get_mousewheel_delta()

 -- gfx.print(font, 15, 15, rect.print(top))
 -- gfx.print(font, 15, 27, rect.print(bot))

  -- Debug info
  local tempo = "tempo: <red>" .. tostring(tempo) .. "</red> "
  local wheel = " wheel: <green>" .. tostring(delta) .. "</green>"
  local buttons = " btn1: " .. (inp.is_button_down(1) and "DOWN" or "UP") ..
                  " btn2: " .. (inp.is_button_down(2) and "DOWN" or "UP") ..
                  " btn3: " .. (inp.is_button_down(3) and "DOWN" or "UP") ..
                  " st: " .. tostring(inp.get_button_states())
  local shift_down = inp.is_key_down("LEFT_SHIFT") or inp.is_key_down("RIGHT_SHIFT")
  local ctrl_down = inp.is_key_down("LEFT_CONTROL") or inp.is_key_down("RIGHT_CONTROL")
  local keys = " shift: " .. (shift_down and "DOWN" or "UP") ..
               " ctrl: " .. (ctrl_down and "DOWN" or "UP")
  gfx.print(font, 5, 0, tempo .. wheel .. buttons .. keys)

  for i, t in ipairs(tracks) do
    local step = t:get_pattern():get_current_step()
    local trig = t:get_pattern():get_step(step)
    
    local stepstr = ""
    stepstr = tostring(step)
    if trig then
        stepstr = "<green>" .. tostring(step)
    end

    local sampleid = t:get_sampler():get_sample()
    local envv = t:get_sampler():get_volume_envelope_value()
    local envs = t:get_sampler():get_volume_envelope_state()
    local env = string.format("%.1f %s", envv, envs)
    local sample = eng:get_samplebank():get_sample(sampleid)
    if sample then
      sample = tostring(sample.id) ..  " / " ..sample.filename
      gfx.print(font, 5 + 15*step, 40 + (15*i), stepstr)
    end
    gfx.print(font, 5 + 15*16, 40 + (15*i), "| " .. tostring(sample) .. " / " .. env)
  end

  function printdir(files, xoff, yoff)
    if files ~= nil then
      local off = 0
      for i, a in pairs(files) do
       -- gfx.print(font, xoff, yoff + off, tostring(i) .. ". ".. tostring(a))
        off = off + 12
        if (type(a) == 'table') then
         -- printdir(a, xoff, yoff)
        end
      end
    else
    --  gfx.print(font, 5, 20, "--- invalid directorylisting --")
    end
  end
  --printdir(files, 5, 20)
end
