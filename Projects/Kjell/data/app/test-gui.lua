require 'gui/luie'
require 'gui/rect'

--require("ext/mobdebug").start()

function on_file_reload(result, path)
  print(string.format("%s is reloaded (%s)", path, result))
  return true
end

function on_init()
  build_gui()
end

function on_shutdown()
  if app.is_debug() then
    gfx.dump_textures("debug")
  end
end

function make_button(text, onclick)
  return { type="button", align="right", text=text, onclick=onclick }
end

function make_label(text)
  return { type="label", text=text}
end

function make_checkbox(value)
  return { type="checkbox", align="center", value=value }
end

function make_hstack(...)
  return { type="stack", spacing="fit", direction="horizontal", body = { ... } }
end

function make_vstack(...)
  return { type="stack", spacing="fit", direction="vertical", body = { ... } }
end

function make_hsplit(...)
  return { type = 'split2', from = "left", at = "50%", body = { ... }}
end

function make_vsplit(...)
  return { type = 'split2', from = "top", at = "50%", body = { ... }}
end

function make_menu(entries)
  -- spacing = automatically fit
  -- menu can be vstack
  --return { type = 'menu', button = "left", on = "release", body = { ... }}
end 

function build_gui()
  gui_struct1 = make_hsplit(
        make_hstack(make_label("label 1"),make_button("button 2"),make_button("button 3")),
        make_hstack(make_hstack(make_button("button 4"), make_button("lol")),
                    make_hstack(make_vstack(make_checkbox(0), make_checkbox(1))
                               ,make_vstack(make_checkbox(1), make_checkbox(1))
                               ,make_vstack(make_checkbox(1), make_checkbox(0)))))

  local function callback()
      local window = luie:build(gui_struct4, nil, "fit")
      luie:pushwindow(window)
  end
  gui_struct2 = make_vstack(
                  make_hstack(make_label("A long text label maybe?")),
                  make_hsplit(
                        make_hstack(make_button("button 1", callback), make_label("LABEL"),make_button("btn3")),
                        make_hstack(make_vsplit(make_button("button 5"), make_button("button 6")),
                                    make_hstack(make_vstack(make_checkbox(0), make_checkbox(1))
                                               ,make_vstack(make_checkbox(1), make_checkbox(1))
                                               ,make_vstack(make_checkbox(1), make_checkbox(0))))))

  gui_struct3 = make_hsplit(
        make_hstack(make_button("button 1"),make_button("button 2"),make_button("button 3")),
        make_hstack(make_hsplit(make_button("button 5"), make_button("button 6")),
                    make_hstack(make_vstack(make_checkbox(0), make_checkbox(1))
                               ,make_vstack(make_checkbox(1), make_checkbox(1))
                               ,make_vstack(make_checkbox(1), make_checkbox(0)))))

  gui_struct4 = make_vstack(make_button("button 1"),
                  make_button("button 2"),
                  make_hstack(make_button("button 3"), make_button("button 4"), make_vstack(make_button("button 5"), 
                                                                                         make_button("button 6"))))


  gui = luie.new()

  -- push root window
  local window = luie:build(gui_struct2, gfx.get_active_window())
  luie:pushwindow(window)
end

function update()
  gui:update()
end

local tbatch = lapp.textbatch(font)
--print(tbatch)
tstr = tbatch:create_string(33, 33, 90, "<green>hello world?\n:)")
--tstr = tbatch:create_string(33, 33, 90, "<green>hello world\n:D\n:)")
--tstr:set("aoeuaoeu")

win = gfx.get_active_window()
win_w, win_h = win:size()
function draw()
  gfx.set_viewport(0, 0, win_w, win_h)
  gfx.set_ortho_projection(math.ortho(0, win_w, win_h, 0, -10, 10))
  gfx.clear()
  gui:draw()
  --gfx.print(font, 0, 12, "<white>debug text on top")
  tbatch:draw()
end
