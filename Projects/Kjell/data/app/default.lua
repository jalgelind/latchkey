-- Test Font (bitmap, TTF/SDF), QuadBuffer, Texture and FrameBuffer
--

require 'app'
require 'gfx'
require 'inp'

function on_shutdown()
  gfx.dump_textures("debug")
end

local input = lapp.input()

win = gfx.get_active_window()
win_w, win_h = win:size()

local fonten = gfx.font("data/fonts/DroidSerif-Regular.ttf", 96, true)
local fonten2 = gfx.font("data/fonts/DroidSerif-Regular.ttf", 48, false)

function update()
  -- clear unprocessed input events, we don't want them
end

-- QuadBuffer
local buffer = gfx.quadbuffer(15)
local quads = {}
for y = 0, 3 do
  for x = 0, 3 do
    local quad = lapp.quad()
    quad.set_rect(0 + x*10, 0 + y*10, 10, 10)
    quad.set_color(x / 4., y / 4., (x+1) / (y+1.), 1)
    quad.set_depth(-1)
    --quad = {
    --  color = {x / 4., y / 4., (x+1) / (y+1.), 1},
    --  rect = {0 + x*10, 0 + y*10, 10, 10},
   -- }
    table.insert(quads, quad)
  end
end
buffer:update(0, quads)

-- Framebuffer
local fbo = gfx.framebuffer()

local tex0 = gfx.texture(win_w, win_h, "rgba")
local dtex = gfx.texture(win_w, win_h, "depthstencil")
dtex:set("mipmaps", false)
dtex:set("min", "linear")
dtex:set("mag", "linear")
fbo:attach(tex0, "color0")
fbo:attach(dtex, "depthstencil")

-- shader
local shader1 = gfx.shader([[
  #vertexshader
  void main(void)
  {
    gl_Position = ftransform();
    gl_FrontColor = gl_Color;
  }

  #fragmentshader
  void main()
  { 
    gl_FragColor = gl_Color;
  }
]])

local shader2 = gfx.shader([[
  #vertexshader
  uniform sampler2D tex;
  void main(void)
  {
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_FrontColor = gl_Color;
  }

  #fragmentshader
  uniform sampler2D tex0;
  void main()
  {
    gl_FragColor = texture2D(tex0, gl_TexCoord[0].st);
  }
]])

shader2:set_uniform("tex0", 1);

-- full screen quad
local fbuff = gfx.quadbuffer(1)
local quad = {
  color = {1, 1, 1, 1},
  rect = {0, 0, win_w, win_h},
  tex = {0, 0, 1, 1}
}
fbuff:update(0, {quad})

function draw()
  gfx.set_viewport(0, 0, win_w, win_h)
  gfx.set_ortho_projection(math.ortho(0, win_w, win_h, 0, -10, 10))
  gfx.clear()
  gfx.set_blending("normal")
  gfx.set_depthtest("<=")

  gfx.bind(shader1)
  gfx.bind(fbo)
    gfx.clear()
    local qi = 6
    local q = quads[qi]
    q.rotation = app.get_uptime()
    q.set_color(1, 1, 1)
    q.depth = 0.2
    --q.set_size(15, 15)
    buffer:update(qi, {q})

    buffer:draw()
    gfx.print(fonten, 200, 30, 1/2, "<green>ttf on texture")
  gfx.unbind(fbo)
  gfx.unbind(shader1)

  gfx.set_ortho_projection(math.ortho(0, win_w, 0, win_h, -10, 10))
  gfx.clear()
 
  gfx.bind(tex0, 1)
  gfx.bind(shader2)
  fbuff:draw()
  gfx.unbind(shader2)
  gfx.unbind(tex0, 1)
  
  gfx.set_ortho_projection(math.ortho(0, win_w, win_h, 0, -10, 10))
  gfx.print(fonten2, 200, 68, 1/2, "<green>ttf on top")
  gfx.unbind_texture()
  gfx.print(font, 200, 120, "<white>debug text on top")
end
