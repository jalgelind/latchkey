require 'app'
require 'gfx'
require 'inp'

playground = {}

defaultcolor = gfx.color(26, 26, 26)

timer = app.timer()
timer:start()

playground.update = function()

    timer:start()

  --if inp.iskeydown(inp.ESC) then -- and not app.isdll
 --   app.quit()
--  end
 -- if inp.isbuttondown(0) then
  --end
end

x = 0

time = os.time()

rng = app.random_generator()
rng:seed(time)

r, g, b = rng:get(3)

tex = gfx.texture("data/fabric/logo.png")

fb = lapp.framebuffer()
fb:attach(tex)

shader = lapp.shader()
shader:set_uniform("hey", {2, 3, 4})

batch = gfx.batch()

gfx.setblending("normal")

app.easereg("blarg", 0, 30)

app.setfloat("win.width", 100)

color = gfx.color()

playground.draw = function()
  x = x + 1
  color:set_hsv(x % 360, 0.7, 0.8)
  font:set_color("test", color:rgb())
  font:set_default_color("test")

  gfx.clear(defaultcolor:rgb())

  tex:bind()

  batch:quads_begin()
  --batch:set_color(0, 0, 0, 1)
  batch:set_texture_subset(0, 0, 1, 1)
  batch:quads_set_rotation((x / 200) % 360)
  batch:quads_add_top_left(200, 0, 128 * 2, 64 * 2)
  batch:quads_end()

  tex:unbind()

  gfx.print(font, 0, 0, defaultcolor:hex() .. "\n" .. app.getfloat("win.width") .."\n\n" .. app.easeget("blarg") .. "\n" .. tostring(timer:interval()))
  --gfx.print(font, 0, 0, "hello" .. " = " .. color:hex() .. " / " .. tostring(r))
  --error = 3
 -- error.x = 3
end
