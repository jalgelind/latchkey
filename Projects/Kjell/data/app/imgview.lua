require 'app'
require 'gfx'
require 'inp'
require 'ext/pprint'
local serp = require 'ext/serpent'

-- for remote debugging, do:
--require("ext/mobdebug").start()

function on_shutdown()
  print "Bye-bye. /imgview"
end

function on_reboot()
  local save = {cnt=cnt}
  return serp.dump(save)
end

function on_restore(state)
  local fun, err = load(state)
  if err then error(err) end
  local save = fun()
  if save ~= nil and save.cnt ~= nil then
    cnt = save.cnt
    update_window()
  else
    cnt = 0
  end
end

function shuffled(tab)
  local n, order, res = #tab, {}, {}
  for i=1,n do order[i] = { rnd = math.random(), idx = i } end
  table.sort(order, function(a,b) return a.rnd < b.rnd end)
  for i=1,n do res[i] = tab[order[i].idx] end
  return res
end

function init()
  app.set_title("image viewer")
  cnt = 0
  timer = app.timer()
  files = shuffled(app.listdir("data/app/imgview/"))
  tex = gfx.texture()
  tex:set("force_pot", true)
  tex:set("mipmaps", false)
  ftb = gfx.quadbuffer(1)
  update_window()
end

function update_window()
  timer:start()
  local idx = cnt % #files
  fn = files[idx + 1]
  tex:load("data/app/imgview/" .. fn)
  if tex:valid() then
    tex:set("min", "linear")
    tex:set("mag", "linear")
    w, h = tex:size()
    function scaleimg(origw, origh, maxw, maxh)
        local ratio = 0
        local neww, newh = origw, origh
        if neww > maxw then
          ratio = maxw / neww
          newh = newh * ratio
          neww = neww * ratio
        end
        if newh > maxh then
          ratio = maxh / newh
          newh = newh * ratio
          neww = neww * ratio
        end
        return neww, newh
    end
    w, h = scaleimg(w, h, 1000, 600)
  else
    w, h = 300, 100
  end
  gfx.set_window_size(w, h)
  ftb:update(0, {{rect = {0, 0, w, h}
                 ,color = {1, 1, 1, 1}
                 ,tex = {0, 0, 1, 1}}})
  timer:stop()
  fn = fn .. string.format(" (time: %.2f s)", timer:interval() / 1000.)
end

function update()
  -- flip through images with left/right mouse button
  local events = inp.get_events()
  for i, ev in ipairs(events) do
    if ev["type"] == "button" then
      if ev.pressed == 1 then
        cnt = cnt + 1
        update_window()
      elseif ev.pressed == 2 then
        cnt = cnt - 1
        update_window()
      end
    elseif ev["type"] == "key" then
      if ev.pressed == "space" then
        cnt = cnt + 1
        update_window()
      elseif ev.pressed == "backspace" then
        cnt = cnt - 1
        update_window()
      elseif ev.pressed == "enter" then
        cnt = math.floor(app.rand() * #files)
        update_window()
      elseif ev.pressed == "esc" then
        app.quit()
      end
    end
  end
end

function draw()
  gfx.set_viewport(0, 0, w, h)
  gfx.set_ortho_projection(math.ortho(0, w, h, 0, -10, 10))
  gfx.clear()
  tex:bind()
  ftb:draw()
  gfx.print(font, 0, 0, fn)
end

-- init
init()