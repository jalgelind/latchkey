#include <shared/Kjell.h>
#include <shared/MusicEngine.h>

#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Audio/AudioCallback.h>
#include <Pxf/Graphics/Graphics.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Json.h>

#include <Lapp/Lapp.h>
#include <shared/lapp/Bindings.h>

using namespace Saint;
using namespace Kjell;
using namespace Pxf;
using namespace Pxf::Input;
using namespace Pxf::Graphics;

KjellApp* KjellApp::s_KjellApp = 0;

KjellApp::KjellApp(Pxf::Kernel* kernel)
	: Pxf::Application(kernel, "app")
	, music_engine_(nullptr)
	, audio_device_(0)
	, audio_callback_(0)
	, lapp_(nullptr)
	, is_initiated_(false)
{
	// Audio device
	audio_device_ = kernel->GetAudioDevice();
	cfg_.MarkAsFallback();

	if(kernel->GetExeType() == Kernel::EXE_NATIVE)
	{
		int audiorate = cfg_.GetInt("audio.samplerate");
		if (audiorate != 44100)
		{
			logger_.Debug("audio rate currently unsupported: %d", audiorate);
			audiorate = 44100;
		}
		audio_device_->SetSamplingRate(audiorate);
		audio_device_->SetPreferredBufferSize(cfg_.GetInt("audio.buffersize"));
	}

	// Overwrite defaults with config file
	// TODO: should respect --no-config
	if (!cfg_.Load("config.json", true))
	{
		cfg_.Save("config.json");
	}
	// Window options
	window_width_ = cfg_.GetInt("win.width");
	window_height_ = cfg_.GetInt("win.height");
	vsync_ = cfg_.GetBool("win.vsync");

	// Create engine
	music_engine_ = Pxf::make_unique<MusicEngine>(&kernel_, audio_device_);

	// Init music callback
	audio_device_->SetAudioCallback(Pxf::make_unique<SaintAudioCallback>(audio_device_, music_engine_.get()));
	audio_callback_ = audio_device_->GetAudioCallback();
	audio_device_->Initialize();

	logger_.Information("Using samplerate = %d", audio_device_->GetSamplingRate());
}

KjellApp::~KjellApp()
{
	audio_device_->Shutdown();
}

bool KjellApp::OnInit()
{
	if (kernel_.GetExeType() == Kernel::EXE_NATIVE && !InitWindow())
		return false;

	if (kernel_.GetExeType() == Kernel::EXE_VST)
		audio_callback_->SetBufferFormat(Audio::AudioCallback::EBuffersSeparate);

	lapp_ = std::make_shared<Pxf::Lapp>(this);
	AddListner(lapp_);

	if (!is_initiated_)
	{
		lapp_->InstallSetupHook([](lua_State* L)
		{
			Lunar<Lapp_MusicEngine>::Register(L);
			Lunar<Lapp_Track>::Register(L);
			Lunar<Lapp_Pattern>::Register(L);
			Lunar<Lapp_SampleBank>::Register(L);
			Lunar<Lapp_Sampler>::Register(L);
			Lunar<Lapp_Filter>::Register(L);
		});

		std::string path = cfg_.GetString("app.script");
		for(auto arg: kernel_.GetCommandLineArguments())
		{
			if (Path::GetExt(arg) == "lua")
				kernel_.GetResourcePath(arg, path);
		}
		lapp_->SetApplicationScript(path);
		lapp_->Boot();
		is_initiated_ = true;
	} 
	else 
		lapp_->Reboot();

	cfg_.SetBool("app.firstlaunch", false);
	return true;
}

bool KjellApp::InitWindow()
{
	if (wnd_)
		wnd_->Close();

	Graphics::WindowDetails spec;
	spec.Width = window_width_;
	spec.Height = window_height_;
	spec.XPos = cfg_.GetInt("win.xpos");
	spec.YPos = cfg_.GetInt("win.ypos");
	spec.ColorBits = 24;
	spec.AlphaBits = 8;
	spec.DepthBits = 8;
	spec.StencilBits = 0;
	spec.FSAASamples = 0;
	spec.Fullscreen = false;
	spec.Resizeable = false;
	spec.VerticalSync = vsync_;

	root_.x = 0;
	root_.y = 0;
	root_.h = window_height_;
	root_.w = window_width_;

	wnd_ = gfx_.OpenWindow(spec);

	if (!wnd_)
	{
		logger_.Error("Could not create OpenGL context. Is hardware rendering available?");
		return false;
	}

	wnd_->SetTitle("");
	prjmtx_ = glm::ortho(0.f, (float)window_width_, (float)window_height_, 0.f, -10.f, 10.f);
	gfx_.SetProjectionMatrix(prjmtx_);
	gfx_.SetViewport(0, 0, window_width_, window_height_);

	return true;
}

void KjellApp::Reboot()
{
	Init();
}

bool KjellApp::Boot()
{
	lapp_->Boot();
	return true;
}

bool KjellApp::LoadProject(const std::string& path)
{
	auto doc = res_.Acquire<Resource::Json>(path.c_str());
	if (!doc)
		return false;
	return music_engine_->FromJson(doc);
}

bool KjellApp::SaveProject(const std::string& path)
{
	auto doc = music_engine_->ToJson();
	if(doc && doc->SaveToDisk(path.c_str()))
		return true;
	logger_.Error("Failed to save '%s'.", path.c_str());
	return false;
}

void KjellApp::OnResize(int x, int y)
{
	logger_.Debug("on_resize: %d x %d", x, y);
}

void KjellApp::OnMove(int x, int y)
{
	logger_.Debug("on_move: %d x %d", x, y);
}

void KjellApp::OnFocus(bool focus)
{
	logger_.Debug("on_focus: %d", (int)focus);
}

void KjellApp::OnShutdown()
{
	lapp_->Shutdown();
}

void KjellApp::DoUpdate(unsigned int framenum)
{
	Input::Keyboard::KbdQ& keyQ = inp_.GetKeyboard().GetKeyQueue();
	Input::Mouse::MouseQ& mouseQ = inp_.GetMouse().GetEventQueue();
	
	inp_.Update();
	lapp_->Update();

	/*

	// Process mouse input
	do 
	{
		Mouse::Event ev = mouseQ.Pop();
		switch(ev.type)
		{
		case Mouse::Event::POSITION:
			logger_.Debug("Mousepos - x: %d, y: %d (dx: %d, dy: %d)", ev.x, ev.y, ev.dx, ev.dy);
			break;
		case Mouse::Event::BUTTON:
			logger_.Debug("Mousebtn - pressed: %s, released: %s",
				Mouse::repr((Pxf::Input::MouseButton)ev.mouse_pressed).c_str(),
				Mouse::repr((Pxf::Input::MouseButton)ev.mouse_released).c_str()); // TODO: casts should not be needed
			break;
		case Mouse::Event::SCROLL:
			logger_.Debug("Mousescr - x: %d, y: %d (dx: %d, dy: %d)", ev.scroll_x, ev.scroll_y, ev.scroll_dx, ev.scroll_dy);
			break;
		}
	} while (mouseQ.Peek().IsValid());


	// Process keyboard input
	do
	{
		Keyboard::Event ev = keyQ.Pop();
		if (ev.IsValid())
		{
			if (ev.code == Input::BACKSPACE)
				logger_.Debug("Backspace... :)");
		}
	} while (keyQ.Peek().IsValid());
	*/

	Keyboard::Event ev;
	while(keyQ.PopTo(ev))
	{
		if (ev.code == Input::BACKSPACE)
			logger_.Debug("Backspace... :)");
	}

	if (framenum == 0)
		CheckForReloadedResources();
}

void KjellApp::DoRender(float _Interpolation)
{
	lapp_->Draw();
}

void KjellApp::Run()
{
	RunConstantFPS();
}
