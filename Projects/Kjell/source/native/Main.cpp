#include <Pxf/Pxf.h>
#include <Pxf/Kernel.h>
#include <cstdio>
#include <shared/Kjell.h>

//#include <PreloadedResources.h>

#include <string>

using namespace Pxf;
using namespace Kjell;

int main(int argc, const char** argv)
{
    Pxf::Kernel* kernel = Pxf::Kernel::GetInstance();

    // TODO: Properly parse command line parameters
    bool show_version = false;
    bool show_version_full = false;
    for(int i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "--version")
        {
            show_version = true;
        }
        else if (std::string(argv[i]) == "--version-full")
        {
            show_version = true;
            show_version_full = true;
        }
    }

    if (show_version)
    {
        unsigned kv = kernel->GetKernelVersion();
        uint16 maj, min;
        Pxf::UnpackShort2(kv, &maj, &min);
        const char* versiontext = \
            "Latchkey v%d.%d\n"
            "\n"
            "%s"
            "\n\n";
        auto licenseinfo = kernel->GetLicenseInformation();
        printf(versiontext, maj, min, licenseinfo->license);
        if (licenseinfo->third_party)
        {
            printf("The following third party libraries were used in the creation of this program.\n\n"
                   "Run with parameter --version-full for full third party licensing information.\n"
                   "\n");
            for(int i = 0; i < licenseinfo->third_party_size; i++)
            {
                if (show_version_full)
                {
                    printf("* %s\n%s\n", licenseinfo->third_party[i].name
                        ,licenseinfo->third_party[i].license);
                }
                else
                {
                    printf("%s", licenseinfo->third_party[i].name);
                    if (i != licenseinfo->third_party_size-1)
                        printf(", ");
                }
            }
            puts("");
        }
        fflush(stdout);
        exit(0);
    }

    Pxf::Init(argc, argv);

	Pxf::Logger logger = kernel->GetLogger("init");
	Pxf::InitModules();
	logger.Information("Initializing...");
	{
		KjellApp* app = KjellApp::Instance();
		if (app->Init())
		{
			app->Run();
		}
		else
			logger.Error("Failed to initialize application.");
		delete app;
	}
	logger.Information("Puss puss.");
	Pxf::Shutdown();
	return 0;
}