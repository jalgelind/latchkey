//	Copyright (c) 2005-2006 Niall Moody
//	Modified for personal use, (c) 2012 Johannes Algelind
//	
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the "Software"),
//	to deal in the Software without restriction, including without limitation
//	the rights to use, copy, modify, merge, publish, distribute, sublicense,
//	and/or sell copies of the Software, and to permit persons to whom the
//	Software is furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//	DEALINGS IN THE SOFTWARE.
//	--------------------------------------------------------------------------

#include <Pxf/Modules/dgfx/OpenGL.h>
#include <vst/VstEditor.h>
#include <vst/VstPlugin.h>

#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Math/Math.h>
#include <shared/Camera.h>

#include <Lapp/Lapp.h>
#include <shared/Kjell.h>

#include <Pxf/Base/Timer.h>

using namespace Pxf;

//----------------------------------------------------------------------------
VstEditor::VstEditor(AudioEffect *effect, VstPlugin* plug, Pxf::Kernel* kernel)
	: VSTGLEditor(effect, Antialias4x)
	, Timer(30)
	, plugin_(plug)
	, kernel_(kernel)
	, callback_(0)
	, gfx_(0)
	, app_(0)
{
	kernel->SetExeType(Kernel::EXE_VST);
	log_.Debug("created editor...");
	gfx_ = kernel->GetGraphicsDevice();
	log_ = kernel->GetLogger("host");
	win_ = new Pxf::Graphics::WindowDeviceVSTGL(gfx_);
	inp_ = new Pxf::Input::VstGL_InputController(kernel_);
	kernel->RegisterInputController(inp_);
	kernel->GetAudioDevice()->SetSamplingRate(plug->getSampleRate());

	gfx_->SetActiveWindow(win_);

	// TODO: Should create application here and use config values
	win_->SetWindowSize(550, 300);
	setRect(0, 0, win_->GetWidth(), win_->GetHeight());
	log_.Debug("initialized editor...");
}

//----------------------------------------------------------------------------
VstEditor::~VstEditor()
{
	
}


//----------------------------------------------------------------------------
void VstEditor::onMouseMove(int x, int y)
{
	inp_->OnMouseMove(x, y);
};

void VstEditor::onMouseUp(int button, int x, int y)
{
	inp_->OnMouseUp(button, x, y);
};
void VstEditor::onMouseDown(int button, int x, int y) 
{
	inp_->OnMouseDown(button, x, y);
};
void VstEditor::onMouseWheel(int wheel, int x, int y) 
{
	inp_->OnMouseScroll(wheel, x, y);
};

void VstEditor::onGLKeyDown(const VstKeyCode& key) 
{
	inp_->Update(key, false);
};

void VstEditor::onGLKeyUp(const VstKeyCode& key)
{
	inp_->Update(key, true);
};

//----------------------------------------------------------------------------
void VstEditor::guiOpen()
{
//	PXFGLCHECK("VstEditor::guiOpen/Start");
	win_->Open();

	//Setup context
	log_.Debug("setting up editor context...");
	log_.Information("OpenGL Vendor  : %s", (const char*)glGetString(GL_VENDOR));
	log_.Information("OpenGL Renderer: %s", (const char*)glGetString(GL_RENDERER));
	log_.Information("OpenGL Version : %s", (const char*)glGetString(GL_VERSION));
	log_.Information("GLSL Version   : %s", (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

	gfx_->SetViewport(0, 0, win_->GetWidth(), win_->GetHeight());
	glm::mat4 prjmat = glm::mat4::Ortho(0, win_->GetWidth(), 0, win_->GetWidth(), -10, 10);
	gfx_->SetProjection(&prjmat);

	if (!app_)
	{
		kernel_->GetAudioDevice()->SetSamplingRate(plugin_->getSampleRate());
		log_.Debug("booting up gui...");
		app_ = Saint::KjellApp::Instance();
		app_->Init();
		callback_ = kernel_->GetAudioDevice()->GetAudioCallback();
		plugin_->audio_callback_ = callback_;
	}
	else
	{
		log_.Debug("rebooting gui...");
		app_->Reboot();
	}

	//Start timer to constantly update gui.
	start();
}

//----------------------------------------------------------------------------
void VstEditor::guiClose()
{
	//Stop the timer.

	// FIXME: With this uncommented, closing/reopening vst window is broken.
	//stop();
}

//----------------------------------------------------------------------------
void VstEditor::draw()
{
	//gfx_->Clear(0.23, 0.323, 1.);
	//lapp_->Draw();
	app_->DoRender(0.f);
	win_->Swap(); // compability: does not actually swap
}

void VstEditor::Initialize()
{
	// Do configuration stuff here.
}

//----------------------------------------------------------------------------
void VstEditor::timerCallback()
{
	refreshGraphics();
	static int f = 0;
	app_->_DoUpdate(f);
	if (f == 0)
	{
		app_->CheckForReloadedResources();
	}
	f = (f + 1) % 30;
}