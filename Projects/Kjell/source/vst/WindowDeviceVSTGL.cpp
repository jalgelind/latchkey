#include <Pxf/Kernel.h>
#include <Pxf/Base/Platform.h>
#include <Pxf/Modules/dgfx/OpenGL.h>
#include <Pxf/Base/Debug.h>

#include <string>

#ifdef CONF_PLATFORM_MACOSX
#include <Carbon/Carbon.h>
#endif
#include <Pxf/Modules/dgfx/OpenGL.h>
#include <vst/WindowDeviceVSTGL.h>

using namespace Pxf;
using namespace Pxf::Graphics;
using std::string;

int WindowDeviceVSTGL::GetWidth() { return m_width; }
int WindowDeviceVSTGL::GetHeight() {return m_height; }
float WindowDeviceVSTGL::GetAspectRatio() { return ((float)m_width / (float)m_height); }


WindowDeviceVSTGL::WindowDeviceVSTGL(GraphicsDevice* _Device)
	: Window(_Device)
{
	m_Logger = Logger(_Device->GetKernel(), "gfx");

	// FPS
	m_fps = 0;
	m_fps_count = 0;
	m_fps_laststamp = Platform::GetTime();
}

WindowDeviceVSTGL::~WindowDeviceVSTGL()
{

}

bool WindowDeviceVSTGL::Open()
{
/*
	Not sure if this is needed. Must test.

	#ifdef CONF_PLATFORM_MACOSX
	ProcessSerialNumber psn;
	GetCurrentProcess(&psn);
	TransformProcessType(&psn,kProcessTransformToForegroundApplication);
	SetFrontProcess(&psn);
	#endif
*/

	GLenum err = glewInit();
	glewExperimental = true;
	if (err != GLEW_OK)
	{
		m_Logger.Error("Could not initiate glew: %s", glewGetErrorString(err));
	}

	// Map gl-functionality
	Pxf::Graphics::GL::SetupExtensions();
	return true;
}

void WindowDeviceVSTGL::Activate()
{

}

bool WindowDeviceVSTGL::Close()
{
	return true;
}

void WindowDeviceVSTGL::Swap()
{
	if (IsOpen())
	{
		int64 t_current_time = Platform::GetTime();
		int64 diff = t_current_time - m_fps_laststamp;
		if (diff >= 1000)
		{
			m_fps = m_fps_count;
			m_fps_count = 0;
			m_fps_laststamp = t_current_time;
		}

		// SWAP FUNCTION HERE
		m_fps_count += 1;
	}
}

bool WindowDeviceVSTGL::IsOpen()
{
	return true;
}

bool WindowDeviceVSTGL::IsActive()
{
	return true;
}

bool WindowDeviceVSTGL::IsMinimized()
{
	return false;
}

void WindowDeviceVSTGL::SetWindowSize(int _Width, int _Height)
{
	m_width = _Width;
	m_height = _Height;
}

void WindowDeviceVSTGL::SetTitle(const char *_title)
{
}

int WindowDeviceVSTGL::GetFPS()
{
	return m_fps;
}

const char* WindowDeviceVSTGL::GetContextTypeName()
{
	return "OpenGL";
}

