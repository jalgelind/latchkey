
#include <vst/InputDeviceVSTGL.h>

using namespace Pxf;
using namespace Pxf::Input;

static unsigned char translate_key(unsigned char virt)
{
	return 0;
	/*
	unsigned keycode = 0;
	switch(virt)
	{
	case VKEY_BACK: keycode = Pxf::Input::BACKSPACE; break;
	case VKEY_TAB: keycode = Pxf::Input::TAB; break;
	case VKEY_CLEAR: keycode = Pxf::Input::BACKSPACE; break; // Not sure what this keycode is...
	case VKEY_RETURN: keycode = Pxf::Input::ENTER; break;
	case VKEY_PAUSE: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_ESCAPE: keycode = Pxf::Input::ESC; break;
	case VKEY_SPACE: keycode = Pxf::Input::SPACE; break;
	case VKEY_NEXT: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_END: keycode = Pxf::Input::END; break;
	case VKEY_HOME: keycode = Pxf::Input::HOME; break;
	case VKEY_LEFT: keycode = Pxf::Input::LEFT; break;
	case VKEY_UP: keycode = Pxf::Input::UP; break;
	case VKEY_RIGHT: keycode = Pxf::Input::RIGHT; break;
	case VKEY_DOWN: keycode = Pxf::Input::DOWN; break;
	case VKEY_PAGEUP: keycode = Pxf::Input::PAGEUP; break;
	case VKEY_PAGEDOWN: keycode = Pxf::Input::PAGEDOWN; break;
	case VKEY_SELECT: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_PRINT: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_ENTER: keycode = Pxf::Input::KP_ENTER; break;
	case VKEY_SNAPSHOT: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_INSERT: keycode = Pxf::Input::INSERT; break;
	case VKEY_DELETE: keycode = Pxf::Input::DEL; break;
	case VKEY_HELP: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_NUMPAD0: keycode = Pxf::Input::KP_0; break;
	case VKEY_NUMPAD1: keycode = Pxf::Input::KP_1; break;
	case VKEY_NUMPAD2: keycode = Pxf::Input::KP_2; break;
	case VKEY_NUMPAD3: keycode = Pxf::Input::KP_3; break;
	case VKEY_NUMPAD4: keycode = Pxf::Input::KP_4; break;
	case VKEY_NUMPAD5: keycode = Pxf::Input::KP_5; break;
	case VKEY_NUMPAD6: keycode = Pxf::Input::KP_6; break;
	case VKEY_NUMPAD7: keycode = Pxf::Input::KP_7; break;
	case VKEY_NUMPAD8: keycode = Pxf::Input::KP_8; break;
	case VKEY_NUMPAD9: keycode = Pxf::Input::KP_9; break;
	case VKEY_MULTIPLY: keycode = Pxf::Input::KP_MULTIPLY; break;
	case VKEY_ADD: keycode = Pxf::Input::KP_ADD; break;
	case VKEY_SEPARATOR: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_SUBTRACT: keycode = Pxf::Input::KP_SUBTRACT; break;
	case VKEY_DECIMAL: keycode = Pxf::Input::KP_DECIMAL; break;
	case VKEY_DIVIDE: keycode = Pxf::Input::KP_DIVIDE; break;
	case VKEY_F1: keycode = Pxf::Input::F1; break;
	case VKEY_F2: keycode = Pxf::Input::F2; break;
	case VKEY_F3: keycode = Pxf::Input::F3; break;
	case VKEY_F4: keycode = Pxf::Input::F4; break;
	case VKEY_F5: keycode = Pxf::Input::F5; break;
	case VKEY_F6: keycode = Pxf::Input::F6; break;
	case VKEY_F7: keycode = Pxf::Input::F7; break;
	case VKEY_F8: keycode = Pxf::Input::F8; break;
	case VKEY_F9: keycode = Pxf::Input::F9; break;
	case VKEY_F10: keycode = Pxf::Input::F10; break;
	case VKEY_F11: keycode = Pxf::Input::F11; break;
	case VKEY_F12: keycode = Pxf::Input::F12; break;
	case VKEY_NUMLOCK: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_SCROLL: keycode = Pxf::Input::UNKNOWN; break;
	case VKEY_SHIFT: keycode = Pxf::Input::LSHIFT; break;
	case VKEY_CONTROL: keycode = Pxf::Input::LCTRL; break;
	case VKEY_ALT: keycode = Pxf::Input::LALT; break;
	case VKEY_EQUALS: keycode = Pxf::Input::KP_EQUAL; break;
	}
	return keycode;
}

static unsigned int translate_name(const std::string& name)
{
	unsigned keycode = 0;
	if (name == "BACKSPACE") return VKEY_BACK;
	else if (name == "TAB") return VKEY_TAB;
	else if (name == "BACKSPACE") return VKEY_CLEAR; // Not sure what this keycode is...
	else if (name == "ENTER") return VKEY_RETURN;
	else if (name == "ESC") return VKEY_ESCAPE;
	else if (name == "SPACE") return VKEY_SPACE;
	else if (name == "END") return VKEY_END;
	else if (name == "HOME") return VKEY_HOME;
	else if (name == "LEFT") return VKEY_LEFT;
	else if (name == "UP") return VKEY_UP;
	else if (name == "RIGHT") return VKEY_RIGHT;
	else if (name == "DOWN") return VKEY_DOWN;
	else if (name == "PAGEUP") return VKEY_PAGEUP;
	else if (name == "PAGEDOWN") return VKEY_PAGEDOWN;
	else if (name == "KP_ENTER") return VKEY_ENTER;
	else if (name == "INSERT") return VKEY_INSERT;
	else if (name == "DEL") return VKEY_DELETE;
	else if (name == "KP_0") return VKEY_NUMPAD0;
	else if (name == "KP_1") return VKEY_NUMPAD1;
	else if (name == "KP_2") return VKEY_NUMPAD2;
	else if (name == "KP_3") return VKEY_NUMPAD3;
	else if (name == "KP_4") return VKEY_NUMPAD4;
	else if (name == "KP_5") return VKEY_NUMPAD5;
	else if (name == "KP_6") return VKEY_NUMPAD6;
	else if (name == "KP_7") return VKEY_NUMPAD7;
	else if (name == "KP_8") return VKEY_NUMPAD8;
	else if (name == "KP_9") return VKEY_NUMPAD9;
	else if (name == "KP_MULTIPLY") return VKEY_MULTIPLY;
	else if (name == "KP_ADD") return VKEY_ADD;
	else if (name == "KP_SUBTRACT") return VKEY_SUBTRACT;
	else if (name == "KP_DECIMAL") return VKEY_DECIMAL;
	else if (name == "KP_DIVIDE") return VKEY_DIVIDE;
	else if (name == "F1") return VKEY_F1;
	else if (name == "F2") return VKEY_F2;
	else if (name == "F3") return VKEY_F3;
	else if (name == "F4") return VKEY_F4;
	else if (name == "F5") return VKEY_F5;
	else if (name == "F6") return VKEY_F6;
	else if (name == "F7") return VKEY_F7;
	else if (name == "F8") return VKEY_F8;
	else if (name == "F9") return VKEY_F9;
	else if (name == "F10") return VKEY_F10;
	else if (name == "F11") return VKEY_F11;
	else if (name == "F12") return VKEY_F12;
	else if (name == "LSHIFT") return VKEY_SHIFT;
	else if (name == "LCTRL") return VKEY_CONTROL;
	else if (name == "LALT") return VKEY_ALT;
	else if (name == "KP_EQUAL") return VKEY_EQUALS;
	else
		return 0;
	*/
}

void VstGL_InputController::Update(const VstKeyCode& keycode, bool released)
{
	m_IntKeyboard->GetCharQueue().Put(Keyboard::Event(keycode.character, released ? Keyboard::Event::KEY_RELEASE : Keyboard::Event::KEY_PRESS));
	m_IntKeyboard->GetKeyQueue().Put(Keyboard::Event(translate_key(keycode.virt), released ? Keyboard::Event::KEY_RELEASE : Keyboard::Event::KEY_PRESS));
	m_IntKeyboard->keys[translate_key(keycode.virt)] = !released;
}

void VstGL_InputController::OnMouseMove(int x, int y)
{
	m_IntMouse->mouse_x = x;
	m_IntMouse->mouse_y = y;
}

void VstGL_InputController::OnMouseUp(int button, int x, int y)
{
	m_IntMouse->mouse_x = x;
	m_IntMouse->mouse_y = y;
	m_IntMouse->mouse_buttons &= ~(1 << (button-1));
}

void VstGL_InputController::OnMouseDown(int button, int x, int y)
{
	m_IntMouse->mouse_x = x;
	m_IntMouse->mouse_y = y;
	m_IntMouse->mouse_buttons |= (1 << (button-1));
}

void VstGL_InputController::OnMouseScroll(int wheel, int x, int y)
{
	m_IntMouse->mouse_x = x;
	m_IntMouse->mouse_y = y;
	m_IntMouse->mouse_scroll += wheel / 120;
	if (wheel > 0)
		m_IntMouse->mouse_wheel_delta++;
	else
		m_IntMouse->mouse_wheel_delta--;
}

const char* VstGL_Keyboard::GetKeyName(int _key)
{
	return "NULL";
}

int VstGL_Keyboard::GetKeyCode(const char* _name)
{
	return 0; // TODO
	//return translate_name(_name);
}

bool VstGL_Keyboard::IsKeyDown(int _key)
{
	if (_key >= 0 && _key < 512)
		return keys[translate_key(_key)];
	return false;
}