#ifndef _KJELL_KJELL_H_
#define _KJELL_KJELL_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Util/Application.h>
#include <Pxf/Base/Memory.h>

namespace Pxf
{
	namespace Audio
	{
		class AudioDevice;
		class AudioCallback;
	}
	class Kernel;
}

namespace Saint
{
	class MusicEngine;
}

namespace Kjell
{
	class KjellApp : public Pxf::Application
	{
	public:
		std::unique_ptr<Saint::MusicEngine> music_engine_;
		Pxf::Audio::AudioDevice* audio_device_;
		Pxf::Audio::AudioCallback* audio_callback_;
		static KjellApp* s_KjellApp;
		std::shared_ptr<Pxf::Lapp> lapp_;

		bool is_initiated_;

		KjellApp(Pxf::Kernel* _Kernel);
		~KjellApp();

		static KjellApp* Instance()
		{
			if (!s_KjellApp)
				s_KjellApp = new KjellApp(Pxf::Kernel::GetInstance());
			return s_KjellApp;
		}

		Saint::MusicEngine* GetMusicEngine() const 
		{
			return music_engine_.get();
		}

		bool OnInit();
		bool InitWindow();

		void Reboot();
		bool Boot();

		bool LoadProject(const std::string& path);
		bool SaveProject(const std::string& path);

		void OnResize(int x, int y);
		void OnMove(int x, int y);
		void OnFocus(bool focus);
		void OnShutdown();

		void DoUpdate(unsigned int framenum);
		void DoRender(float _Interpolation);
		void Run();
	};
}

#endif // _KJELL_KJELL_H_