#ifndef _KJELL_COMPAT_INPUTDEVICEVSTGL_H_
#define _KJELL_COMPAT_INPUTDEVICEVSTGL_H_

#include <Pxf/Input/InputDevice.h>
#include <Pxf/Base/Debug.h>

#include <aeffectx.h>


namespace Pxf
{
	namespace Input
	{
		class VstGL_Keyboard : public Keyboard
		{
		public:

			int last_char;   /* stores last char pressed */
			int last_char_released;   /* stores last char released */
			int last_key;	/* stores last key pressed */
			int last_key_released; /* stores last key released */
			bool keys[512];

			VstGL_Keyboard(Kernel* krn, InputController* inp)
				: Keyboard(krn, inp)
			{
				for(int i = 0; i < 512; i++)
					keys[i] = false;
				last_char = 0;
				last_key = 0;
			}

			virtual const char* GetKeyName(int _key);
			virtual int GetKeyCode(const char* _name);
			virtual bool IsKeyDown(int _key);
			virtual void SetKeyRepeat(bool _On)       {}
			virtual void SetSystemKeys(bool _On)      {}
		};

		//
		// Null Mouse
		//
		class VstGL_Mouse : public Mouse
		{
		public:
			int last_button; /* stores last mouse button pressed */
			int last_button_released; /* stores last mouse button released */

			Input::MouseMode mouse_mode;
			int mouse_x, mouse_y, mouse_scroll;
			int mouse_wheel_delta;
			unsigned int mouse_buttons; /* store status for mouse button, bit #0 = button 1. 1 = pressed */

			VstGL_Mouse(Kernel* krn, InputController* inp)
				: Mouse(krn, inp)
			{
				mouse_x = 0;
				mouse_y = 0;
				mouse_scroll = 0;
				mouse_buttons = 0;
				mouse_wheel_delta = 0;
				last_button = 0;
			}

			virtual bool IsButtonDown(int _button)    {return mouse_buttons & ( 1 << _button);}
			virtual unsigned GetButtonStates()        {return mouse_buttons;}
			virtual int GetLastButton()               {return last_button;}
			virtual int GetLastReleasedButton()       {return last_button_released;}
			virtual void ClearLastButton()            {	last_button = 0; last_button_released = 0;}
			virtual int GetMouseWheel()               {return mouse_scroll;}
			virtual int GetMouseWheelDelta()
			{
				int delta = mouse_wheel_delta;
				mouse_wheel_delta = 0;
				return delta;
			}
			virtual void GetMousePos(int *x, int *y)  {	*x = mouse_x; *y = mouse_y;}
			virtual void SetMousePos(int x, int y)    {}
			virtual MouseMode GetMouseMode()          {return MODE_RELATIVE;}
			virtual void SetMouseMode(MouseMode _Mode){}
			virtual void ShowCursor(bool _show)       {}
		};

		class VstGL_InputController : public Pxf::Input::InputController
		{
		private:
			virtual bool Init()
			{   return true;	}
		public:
			VstGL_Keyboard* m_IntKeyboard;
			VstGL_Mouse* m_IntMouse;

			VstGL_InputController(Kernel* _Kernel)
				: Pxf::Input::InputController(_Kernel, "Null Input Controller")
			{
				m_IntKeyboard = new VstGL_Keyboard(_Kernel, this);
				m_Keyboard = m_IntKeyboard;
				m_HasKeyboard = true;

				m_IntMouse = new VstGL_Mouse(_Kernel, this);
				m_Mouse = m_IntMouse;
				m_HasMouse = true;

				m_HasGamepad = false;
				m_HasMidiDevice = false;
			}

			virtual ~VstGL_InputController() {};

			virtual void Update(const VstKeyCode& keycode, bool released = false);
			void OnMouseMove(int x, int y);
			void OnMouseUp(int button, int x, int y);
			void OnMouseDown(int button, int x, int y);
			void OnMouseScroll(int wheel, int x, int y);


			virtual void Update() {}
			virtual void Discover() {};

			virtual int GetIntOption(const std::string& option) { return -1; }
			virtual float GetFloatOption(const std::string& option) { return -1; }
	};
	}
}

#endif // _KJELL_COMPAT_INPUTDEVICEVSTGL_H_
