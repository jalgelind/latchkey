//	Copyright (c) 2005-2006 Niall Moody
//	Modified for personal use, (c) 2012 Johannes Algelind
//
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the "Software"),
//	to deal in the Software without restriction, including without limitation
//	the rights to use, copy, modify, merge, publish, distribute, sublicense,
//	and/or sell copies of the Software, and to permit persons to whom the
//	Software is furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//	DEALINGS IN THE SOFTWARE.
//	--------------------------------------------------------------------------

#ifndef VSTPLUGIN_H_
#define VSTPLUGIN_H_

#include "audioeffectx.h"
#include <string>

#include <Pxf/Audio/AudioCallback.h>

struct PluginProgram;

//Trick to ensure inline functions get inlined properly.
#ifdef WIN32
#define strictinline __forceinline
#elif defined (__GNUC__)
#define strictinline inline __attribute__((always_inline))
#else
#define strictinline inline
#endif

//----------------------------------------------------------------------------
class VstPlugin : public AudioEffectX
{
public:
	VstPlugin(audioMasterCallback audioMaster);
	~VstPlugin();

	Pxf::Audio::AudioCallback* audio_callback_;

	void process(float **inputs, float **outputs, VstInt32 sampleFrames);
	void processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames);
	VstInt32 processEvents(VstEvents* events);
	void resume();
	void suspend();
	void setProgram(VstInt32 program);
	void setProgramName(char *name);
	void getProgramName(char *name);
	bool getProgramNameIndexed(VstInt32 category, VstInt32 index, char* text);
	bool copyProgram(VstInt32 destination);
	void setParameter(VstInt32 index, float value);
	float getParameter(VstInt32 index);
	void getParameterLabel(VstInt32 index, char *label);
	void getParameterDisplay(VstInt32 index, char *text);
	void getParameterName(VstInt32 index, char *text);
	VstInt32 canDo(char* text);
	float getVu();
	bool getEffectName(char* name);
	bool getVendorString(char* text);
	bool getProductString(char* text);
	VstInt32 getVendorVersion();
	VstPlugCategory getPlugCategory();
	bool getInputProperties(VstInt32 index, VstPinProperties* properties);
	bool getOutputProperties(VstInt32 index, VstPinProperties* properties);
	VstInt32 getGetTailSize();

	//------------------------------------------
	enum
	{
		param1,
		numParameters
	};
  private:
	strictinline void processMIDI(VstInt32 pos);
	strictinline void MIDI_NoteOn(int ch, int note, int val, int delta);
	strictinline void MIDI_NoteOff(int ch, int note, int val, int delta);
	strictinline void MIDI_PolyAftertouch(int ch, int note, int val, int delta);
	strictinline void MIDI_CC(int ch, int num, int val, int delta);
	strictinline void MIDI_ProgramChange(int ch, int val, int delta);
	strictinline void MIDI_ChannelAftertouch(int ch, int val, int delta);
	strictinline void MIDI_PitchBend(int ch, int x1, int x2, int delta);

	enum
	{
		numPrograms = 1,	///<Number of programs this plugin has.
		versionNumber = 100,///<The current version of the plugin.
		maxNumEvents = 250	///<The maximum number of events in our MIDI queue.
	};

    PluginProgram *programs;

	float samplerate;
	float tempo;
	float parameters[numParameters];

	//------------------------------------------
	//MIDI stuff
	VstEvents *tempEvents;
	VstMidiEvent *midiEvent[maxNumEvents];
	int numEvents;
	int eventNumArray[maxNumEvents];
	int numPendingEvents;
	VstInt32 frames;
	//-----------------------------------------

	std::string effectName;
    std::string vendorName;
};

//---------------------------------------------------------------------------
struct PluginProgram
{
  public:
	PluginProgram():
	name("(empty)")
	{
		for(long i=0;i<VstPlugin::numParameters;++i)
			parameters[i] = 0.0f;
	};
	float parameters[VstPlugin::numParameters];
	std::string name;
};

#endif
