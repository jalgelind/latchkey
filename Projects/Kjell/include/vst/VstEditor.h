//	Copyright (c) 2005-2006 Niall Moody
//	Modified for personal use, (c) 2012 Johannes Algelind
//	
//	Permission is hereby granted, free of charge, to any person obtaining a
//	copy of this software and associated documentation files (the "Software"),
//	to deal in the Software without restriction, including without limitation
//	the rights to use, copy, modify, merge, publish, distribute, sublicense,
//	and/or sell copies of the Software, and to permit persons to whom the
//	Software is furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//	DEALINGS IN THE SOFTWARE.
//	--------------------------------------------------------------------------

#ifndef EDITOR_H_
#define EDITOR_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <vst/InputDeviceVSTGL.h>
#include <vst/WindowDeviceVSTGL.h>

#include "VSTGLEditor.h"
#include "VSTGLTimer.h"

#include <iostream>

class VstKeyCode;

namespace Pxf
{
	class Lapp;
	class Application;
	namespace Audio
	{
		class AudioCallback;
	}
}

namespace Saint
{
	class Kjell;
}

class VstPlugin;

class VstEditor : public VSTGLEditor, public Timer
{
  public:
	VstEditor(AudioEffect *effect, VstPlugin* plug, Pxf::Kernel* kernel);
	virtual ~VstEditor();

	VstPlugin* plugin_;
	void onMouseMove(int x, int y);
	void onMouseUp(int button, int x, int y);
	void onMouseDown(int button, int x, int y) ;
	void onMouseWheel(int wheel, int x, int y);
	void onGLKeyDown(const VstKeyCode& key);
	void onGLKeyUp(const VstKeyCode& key);

	Pxf::Kernel* kernel_;
	Pxf::Audio::AudioCallback* callback_;
	Pxf::Logger log_;
	Pxf::Graphics::GraphicsDevice* gfx_;
	Pxf::Graphics::WindowDeviceVSTGL* win_;
	Pxf::Input::VstGL_InputController* inp_;

	Saint::KjellApp* app_;

	void guiOpen();
	void guiClose();

	void Initialize();

	void draw();
	void timerCallback();
};

#endif