#ifndef _KJELL_COMPAT_WINDOWDEVICEVSTGL_H_
#define _KJELL_COMPAT_WINDOWDEVICEVSTGL_H_

#include <Pxf/Base/Types.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Graphics/WindowDetails.h>

namespace Pxf{
	namespace Graphics {

		class WindowDeviceVSTGL : public Window
		{
		public:
			WindowDeviceVSTGL(GraphicsDevice* _Device);
			virtual ~WindowDeviceVSTGL();

			bool Open();
			void Activate();
			bool Close();
			void Swap();

			void SetTitle(const char *_title);
			void SetWindowSize(int _Width, int _Height);


			int GetFPS();
			int GetWidth();
			int GetHeight();
			float GetAspectRatio();
			const char* GetContextTypeName();

			bool IsOpen();
			bool IsActive();
			bool IsMinimized();
		private:
			void* m_parent;
			int m_width, m_height;

			// FPS
			int64 m_fps_laststamp;
			int m_fps, m_fps_count;
			Logger m_Logger;
		};

	} // Graphics
} // Pxf

#endif // _KJELL_COMPAT_WINDOWDEVICEVSTGL_H_

