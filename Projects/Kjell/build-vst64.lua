Import("build-base.lua")

project = KjellProject("Kjell")
project:RequireLibrary("vst2sdk")
project:RequireLibrary("vstgl")
project.settings.cc.defines:Add("_USRDLL")
project.settings.cc.defines:Add("_WINDLL")
project.settings.dll.flags:Add("/DEF:vstplug.def")
project.settings.dll.flags:Add("/DYNAMICBASE:NO")
project.settings.dll.flags:Add("/NXCOMPAT:NO")
project.settings.dll.flags:Add("/SUBSYSTEM:WINDOWS")

--project.settings.cc.flags:Add("/MT")
project.settings.dll.flags:Add("/MACHINE:X64")
project.settings.link.flags:Add("/MACHINE:X64")

project:Build()
