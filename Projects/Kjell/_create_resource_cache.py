import os

IGNORE_FILTER = ['.hg', '.git', '.svn',
                 '.nocache.', "config.json"]
#DATA_FOLDERS = ['data', '../Lapp/data']
DATA_FOLDERS = ['data/images']

def get_resource_list(root):
    resource_list = []
    if os.path.exists("debug/_used_resources.log"):
        print "using log..."
        with open("debug/_used_resources.log") as f:
            for line in f:
                path, tmp = line.split("|")
                fn = os.path.basename(path)
                dn = os.path.dirname(path)
                print path, dn
                resource_list.append((path, dn))
    else:
        srcfolder = root
        for root, dirs, files in os.walk(root):
            for file in files:
                path = os.path.join(root, file)
                if any([ignore in path for ignore in IGNORE_FILTER]):
                    continue
                resource_list.append((path, srcfolder))
    return resource_list

def mangle_filename(path):
    replace = [os.path.sep, '.', '-', '\\', '/']
    for r in replace:
        path = path.replace(r, "_")
    return path

def create_data_file(resources):
    data_file = "#ifndef _PRELOADEDRESOURCES_\n"\
                "#define _PRELOADEDRESOURCES_\n\n"

    data_file += "struct PreloadedFile\n"\
                 "{\n"\
                 "\tconst char * path;\n"\
                 "\tunsigned size;\n"\
                 "\tconst unsigned char* data;\n"\
                 "};\n\n"

    def hexify(name, data):
        chunk_size = len(data)
        hex_chunk = ["0x%x"%int(byte.encode("hex"), 16) + ("\n\t" if index % 20 == 0 else "") 
                     for index, byte in enumerate(data)]
        hex_data = "const unsigned char %s[] = {" % name
        hex_data += ",".join(hex_chunk)
        hex_data += "};\n"
        return hex_data, chunk_size

    file_size_map = {}
    for file, root in resources:
        print ("Processing '{0}'".format(file))
        mangled_name = mangle_filename(file)
        with open(file, "rb") as f:
            raw_data = f.read()
            hex_chunk, chunk_size = hexify(mangled_name, raw_data)
            file_size_map[file] = chunk_size
            data_file += hex_chunk

    data_file += "\n"
    data_file += "PreloadedFile preloaded_files[] = {"
    for file, root in resources:
        if not os.path.exists("debug/_used_resources.log"):
            sn = file.replace(os.path.sep, "/").replace(root, "data")
        sn = file
        data_file += '{"%s", %d, %s},\n' % (sn
                                           ,file_size_map[file]
                                           ,mangle_filename(file))
    data_file += "};\n"

    data_file += """

#include <Pxf/Kernel.h>
#include <Pxf/Resource/ResourceManager.h>
class preloaded_file_reg_
{
public:
    preloaded_file_reg_()
    {
        Pxf::Resource::ResourceManager* mgr = Pxf::Kernel::GetInstance()->GetResourceManager();
        for(int i = 0; i < sizeof(preloaded_files)/sizeof(preloaded_files[0]); i++)
        {
            PreloadedFile& f = preloaded_files[i];
            mgr->RegisterCachedFile(f.path, f.data, f.size, 0);
        }
    }
};

static preloaded_file_reg_ cachefiles_static_init_;


"""


    data_file += "#endif\n"

    return data_file

def main():
    resources = []
    for resourcepath in DATA_FOLDERS:
        resources += get_resource_list(resourcepath)

    data_file = create_data_file(resources)
    with open("PreloadedResources.h", "w") as f:
        print "Writing result to PreloadedResources.h"
        f.write(data_file)
    print("Done.")

if __name__ == '__main__':
    main()