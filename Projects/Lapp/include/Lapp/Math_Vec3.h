#ifndef _PXF_LAPP_MATH_VEC3_H_
#define _PXF_LAPP_MATH_VEC3_H_

#include "Lapp_Common.h"
#include "Math_VecT.h"

namespace Pxf
{
	class Math_Vec3 : public LVecT<Math_Vec3, glm::vec3> 
	{
	public:
	public:
		static const char className[];
		bool enable_gc;

		Math_Vec3(lua_State *L, void* data = 0)
			: enable_gc(true)
		{
			vec_ = glm::vec3(0.f, 0.f, 0.f);
		}
		~Math_Vec3()
		{}

		int __set_x(lua_State *L);
		int __get_x(lua_State *L);
		int __set_y(lua_State *L);
		int __get_y(lua_State *L);
		int __set_z(lua_State *L);
		int __get_z(lua_State *L);
		int cross(lua_State *L);
		int rotate(lua_State *L);
		int rotatex(lua_State *L);
		int rotatey(lua_State *L);
		int rotatez(lua_State *L);

		static const Luna<Math_Vec3>::FunctionType methods[];
		static const Luna<Math_Vec3>::PropertyType properties[];
	};
}

#endif // _PXF_LAPP_MATH_VEC3_H_
