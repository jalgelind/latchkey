#ifndef _PXF_LAPP_COLOR_H_
#define _PXF_LAPP_COLOR_H_

#include <Lapp/Lapp_Common.h>
#include <Pxf/Util/Color.h>

namespace Pxf
{

	class Lapp_Color {
	public:
		Pxf::Color color;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Color>::RegType methods[];

		Lapp_Color(lua_State *L, void* data);
		~Lapp_Color();

		int set_range(lua_State *L);
		int set_int8(lua_State *L);
		int set_float(lua_State *L);
		int set_hsv(lua_State *L);
		int set_hsl(lua_State *L);
		int set_hex(lua_State *L);
		int rgb(lua_State* L);
		int hex(lua_State *L);
	};
}

#endif // _PXF_LAPP_COLOR_H_
