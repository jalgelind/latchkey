#pragma once

#include <Lapp/Lapp_Common.h>
#include <Pxf/Graphics/TextProvider.h>

namespace Pxf
{
	class Lapp_TextBatch;
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
	}
	namespace Resource
	{
		class Font;
	}

	class Lapp_TextBatch {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		std::shared_ptr<Pxf::Graphics::TextProvider> textprovider;
		std::shared_ptr<Pxf::Graphics::TextProvider::Batch> batch;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_TextBatch>::RegType methods[];

		Lapp_TextBatch(lua_State *L, void* data);
		~Lapp_TextBatch();

		int create_string(lua_State* L);
		int draw(lua_State* L);
		int valid(lua_State *L);

		std::shared_ptr<Pxf::Graphics::TextProvider::Batch> get_batch()
		{
			return batch;
		}
	};
}