#ifndef _PXF_LAPP_QUAD_H_
#define _PXF_LAPP_QUAD_H_

#include "Lapp_Common.h"
#include <Pxf/Graphics/PrimitiveBuffer.h> // Quad
#include <Pxf/Graphics/Rect.h> // Rect
#include <Pxf/Util/Color.h> // Color
#include <Pxf/Math/Math.h> // Vec2

namespace Pxf
{
	class Lapp_Quad {
	public:
		Graphics::Rect rect;
		glm::vec2 texcoords[2];
		Color color;
		float rotation;
		float depth;
	public:
		static const char className[];
		bool enable_gc;

		Lapp_Quad(lua_State *L, void* data = 0)
			: enable_gc(true)
			, rect(0, 0, 10, 10)
			, color(1.f, 1.f, 1.f)
			, rotation(0.f)
			, depth(0.f)
		{
			texcoords[0] = glm::vec2(0, 0);
			texcoords[1] = glm::vec2(1, 1);
		}
		~Lapp_Quad()
		{
		}

		// properties
		int __set_x(lua_State *L);
		int __get_x(lua_State *L);
		int __set_y(lua_State *L);
		int __get_y(lua_State *L);
		int __set_w(lua_State *L);
		int __get_w(lua_State *L);
		int __set_h(lua_State *L);
		int __get_h(lua_State *L);
		int __set_rotation(lua_State *L);
		int __get_rotation(lua_State *L);
		int __set_depth(lua_State *L);
		int __get_depth(lua_State *L);
		

		// methods
		int set_pos(lua_State *L);
		int set_size(lua_State *L);
		int set_rect(lua_State *L);
		int set_color(lua_State *L);
		int set_texcoords(lua_State *L);
		int set_rotation(lua_State *L);
		int set_depth(lua_State *L);
		//int set(lua_State *L);

		// operators
		int __add(lua_State *L);
		int __mul(lua_State *L);
		int __div(lua_State *L);
		int __eq(lua_State *L);

		static const Luna<Lapp_Quad>::FunctionType methods[];
		static const Luna<Lapp_Quad>::PropertyType properties[];
	};
}

#endif // _PXF_LAPP_QUAD_H_
