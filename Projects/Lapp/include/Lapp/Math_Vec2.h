#ifndef _PXF_LAPP_MATH_VEC2_H_
#define _PXF_LAPP_MATH_VEC2_H_

#include "Lapp_Common.h"
#include "Math_VecT.h"

namespace Pxf
{
	class Math_Vec2 : public LVecT<Math_Vec2, glm::vec2> 
	{
	public:
	public:
		static const char className[];
		bool enable_gc;

		Math_Vec2(lua_State *L, void* data = 0)
			: enable_gc(true)
		{
			vec_ = glm::vec2(0.f, 0.f);
		}
		~Math_Vec2()
		{}

		int __set_x(lua_State *L);
		int __get_x(lua_State *L);
		int __set_y(lua_State *L);
		int __get_y(lua_State *L);

		int rotate(lua_State *L);

		static const Luna<Math_Vec2>::FunctionType methods[];
		static const Luna<Math_Vec2>::PropertyType properties[];
	};
}

#endif // _PXF_LAPP_MATH_VEC2_H_
