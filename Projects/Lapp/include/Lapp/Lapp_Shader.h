#ifndef _PXF_LAPP_SHADER_H_
#define _PXF_LAPP_SHADER_H_

#include <Pxf/Base/Path.h>
#include <Lapp/Lapp_Common.h>

namespace Pxf
{

	class Lapp_Shader {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		bool is_valid;
		void validate(lua_State* L, Graphics::Shader* shader);
	public:
		std::shared_ptr<Pxf::Graphics::Shader> shader;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Shader>::RegType methods[];

		Lapp_Shader(lua_State *L, void* data);
		~Lapp_Shader();

		int load_source(lua_State *L);
		int load_file(lua_State *L);
		int bind(lua_State *L);
		int unbind(lua_State *L);
		int set_uniformi(lua_State *L);
		int set_uniformf(lua_State *L);
		int set_uniformv(lua_State *L);
		int set_uniform(lua_State *L);

		/*
		virtual void BindAttributeLocation(Shader* _Shader, unsigned _Index, const char* _Name) = 0;
		virtual void SetUniformi(Shader* _Shader, const char* _name, int _value) = 0;
		virtual void SetUniformf(Shader* _Shader, const char* _name, float _value) = 0;
		virtual void SetUniformVec2(Shader* _Shader, const char* _name, const glm::vec2* _value) = 0;
		virtual void SetUniformVec3(Shader* _Shader, const char* _name, const glm::vec3* _value) = 0;
		virtual void SetUniformVec3v(Shader* _Shader, const char* _name, unsigned count, const glm::vec3* _value) = 0;
		virtual void SetUniformVec4(Shader* _Shader, const char* _name, const glm::vec4* _value) = 0;
		virtual void SetUniformVec4v(Shader* _Shader, const char* _name, unsigned count, const glm::vec4* _value) = 0;
		virtual void SetUniformMat4(Shader* _Shader, const char* _name, const glm::mat4* _value) = 0;
		*/

		int valid(lua_State *L);
	};
}
#endif // _PXF_LAPP_SHADER_H_
