#ifndef _PXF_LAPP_PRIMITIVEBUFFER_H_
#define _PXF_LAPP_PRIMITIVEBUFFER_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
		class QuadBuffer;
	}

	class Lapp_QuadBuffer {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		std::shared_ptr<Pxf::Graphics::QuadBuffer> buffer;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_QuadBuffer>::RegType methods[];

		Lapp_QuadBuffer(lua_State *L, void* data);
		~Lapp_QuadBuffer();

		int update(lua_State *L);
		int draw(lua_State *L);
		int valid(lua_State *L);
	};
}

#endif // _PXF_LAPP_PRIMITIVEBUFFER_H_
