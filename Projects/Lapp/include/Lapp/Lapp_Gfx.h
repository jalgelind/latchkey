#ifndef _PXF_LAPP_GFX_H_
#define _PXF_LAPP_GFX_H_

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include <Pxf/Kernel.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Graphics/Graphics.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Resource/Image.h>

#include "Lapp_Color.h"
#include "Lapp_Texture.h"
#include "Lapp_Batch.h"
#include "Lapp_Quad.h"
#include "Lapp_QuadBuffer.h"
#include "Lapp_Framebuffer.h"
#include "Lapp_Shader.h"
#include "Lapp_Font.h"
#include "Lapp_TextBatch.h"
#include "Lapp_TextString.h"
#include "Lapp_Window.h"
#include "Lapp_Graphics.h"

#endif
