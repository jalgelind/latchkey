#pragma once

#include <lua.h>

namespace Pxf
{
	void register_lapp_physics(lua_State* L);
}
