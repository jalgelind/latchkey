#ifndef _PXF_LAPP_INPUT_H_
#define _PXF_LAPP_INPUT_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics { class GraphicsDevice; }
	namespace Input { class InputDevice; 
					  class Keyboard; 
					  class Mouse;
	}

	class Lapp_Input {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		Pxf::Input::InputDevice* inp;
		Pxf::Input::Keyboard* kbd;
		Pxf::Input::Mouse* mouse;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Input>::RegType methods[];

		Lapp_Input(lua_State *L, void* data);
		~Lapp_Input();
		int get_mouse_pos(lua_State* L);
		int is_button_down(lua_State* L);
		int get_button_states(lua_State* L);
		int is_key_down(lua_State* L);
		int get_mousewheel_delta(lua_State* L);
		int get_events(lua_State* L); // all events in a table
		int valid(lua_State *L);
	};
}

#endif // _PXF_LAPP_INPUT_H_
