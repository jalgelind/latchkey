#ifndef _PXF_LAPP_RANDOM_H_
#define _PXF_LAPP_RANDOM_H_

#include <Lapp/Lapp_Common.h>
#include <random>

namespace Pxf
{

	class Lapp_Random {
		std::mt19937 rng;
		std::uniform_real_distribution<> dist_real;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Random>::RegType methods[];

		Lapp_Random(lua_State *L, void* data);
		~Lapp_Random();

		int seed(lua_State* L);
		int get(lua_State* L); // todo: return a table...
	};
}

#endif // _PXF_LAPP_RANDOM_H_
