#ifndef _PXF_LAPP_TIMER_H_
#define _PXF_LAPP_TIMER_H_

#include <Pxf/Base/Timer.h>
#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Lapp_Timer {
		Pxf::TimerF timer;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Timer>::RegType methods[];

		Lapp_Timer(lua_State *L, void* data);
		~Lapp_Timer();

		int start(lua_State* L);
		int stop(lua_State* L);
		int interval(lua_State* L);
	};
}

#endif // _PXF_LAPP_TIMER_H_
