#ifndef _PXF_LAPP_CONFIGURATION_H_
#define _PXF_LAPP_CONFIGURATION_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	class Configuration;
	class Lapp_Configuration {
		Kernel* kernel_;
		Configuration* conf_;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Configuration>::RegType methods[];

		Lapp_Configuration(lua_State *L, void* data);
		~Lapp_Configuration();

		void _set_conf(Configuration* conf)
		{ conf_ = conf; }

		int get_float(lua_State* L);
		int get_string(lua_State* L);
		int get_bool(lua_State* L);
		int set_float(lua_State* L);
		int set_string(lua_State* L);
		int set_bool(lua_State* L);

		int set(lua_State *L);
		int get(lua_State *L);
		
        int load(lua_State *L);
        int save_as(lua_State *L);

		int valid(lua_State *L);
	};
}

#endif // _PXF_LAPP_CONFIGURATION_H_
