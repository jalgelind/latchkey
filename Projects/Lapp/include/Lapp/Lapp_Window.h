#pragma once
#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
		class Window;
	}

	class Lapp_Window {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Window>::RegType methods[];

		Pxf::Graphics::Window* window;

		Lapp_Window(lua_State *L, void* data);
		~Lapp_Window();

		int size(lua_State *L);
		int id(lua_State *L);
		int valid(lua_State *L);
	};
}
