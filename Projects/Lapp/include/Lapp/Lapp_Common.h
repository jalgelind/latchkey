#ifndef __LAPP_COMMON_H__
#define __LAPP_COMMON_H__

#include <Pxf/Math/Math.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/String.h> // Pxf::Format

#include <memory>

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
//#include <lstate.h>
}

#include <luna5.h>
#include <lunar.h>
// FIXME: ASSERT should only trigger in debug builds.
#if defined(CONF_COMPILER_MSVC)
#define LAPP_ERROR(LCTX, msg, ...) Pxf::lapp_pusherror(LCTX, Pxf::Format(msg, __VA_ARGS__).c_str(), __FUNCTION__)
#define LAPP_ASSERT(LCTX, expr, msg, ...) if (!(expr)) { Pxf::lapp_pusherror(LCTX, Pxf::Format(msg, __VA_ARGS__).c_str(), __FUNCTION__); }
#elif defined(CONF_COMPILER_GCC)
#define LAPP_ERROR(LCTX, msg, ...) Pxf::lapp_pusherror(LCTX, Pxf::Format(msg, ## __VA_ARGS__).c_str(), __PRETTY_FUNCTION__)
#define LAPP_ASSERT(LCTX, expr, msg, ...) if (!(expr)) { Pxf::lapp_pusherror(LCTX, Pxf::Format(msg, ## __VA_ARGS__).c_str(), __PRETTY_FUNCTION__); }
#else
#error "No suitable compiler."
#endif

namespace Pxf
{
	/* helpers */
	int lapp_loadfile(lua_State* L, const char* filepath);
	bool lapp_check_error(lua_State* L, int status);
	bool lapp_call(lua_State* L, const char* func, int nargs = 0, int nres = 0);
	bool lapp_run_script(lua_State* L, const char* filepath);
	void lapp_pusherror(lua_State* L, const char* msg, const char* nativeinfo);
	void lapp_stackdump(lua_State* L);
}

#endif