#pragma once

#include <Lapp/Lapp_Common.h>
#include <Pxf/Graphics/TextProvider.h> // Need this for inner class TextProvider::String

namespace Pxf
{
	class Lapp_TextBatch;
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
	}
	namespace Resource
	{
		class Font;
	}

	class Lapp_TextString {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> bstring;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_TextString>::RegType methods[];

		Lapp_TextString(lua_State *L, void* data);
		~Lapp_TextString();

		int set_kerning(lua_State* L);
		int set_default_color(lua_State* L);
		int set_position(lua_State* L);
		int set_max_length(lua_State* L);
		int set(lua_State* L);
		int size(lua_State* L);
		int draw(lua_State* L);

		int valid(lua_State *L);
	};
}
