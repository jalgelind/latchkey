#ifndef _PXF_LAPP_INTERPOLATOR_H_
#define _PXF_LAPP_INTERPOLATOR_H_

#include <string>
#include <map>

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	class Application;

	class Lapp_Interpolator {
		Kernel* kernel;
		Application* app;

		std::map<std::string, float> variables;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Interpolator>::RegType methods[];

		Lapp_Interpolator(lua_State *L, void* data);
		~Lapp_Interpolator();

		int reg(lua_State* L);
		int get(lua_State* L);
	};
}

#endif // _PXF_LAPP_INTERPOLATOR_H_
