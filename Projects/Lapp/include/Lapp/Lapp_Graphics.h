#ifndef _PXF_LAPP_GRAPHICS_H_
#define _PXF_LAPP_GRAPHICS_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
	}

	class Lapp_Graphics {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Graphics>::RegType methods[];

		Lapp_Graphics(lua_State *L, void* data);
		~Lapp_Graphics();

		int getopt_string(lua_State* L);
		int getopt_float(lua_State* L);

		int get_active_window(lua_State* L);
		int get_window(lua_State* L);

		int set_window_size(lua_State* L); // todo: move to window class
		int set_viewport(lua_State *L);
		int set_ortho_projection(lua_State *L);
		int set_depthtest(lua_State* L);
		int set_cullface(lua_State* L);
		int set_clip(lua_State* L);

		int set_pointsize(lua_State* L);
		int set_linewidth(lua_State* L);

		int push_projection_matrix(lua_State* L);
		int pop_projection_matrix(lua_State* L);
		int push_model_matrix(lua_State* L);
		int pop_model_matrix(lua_State* L);

		int bind(lua_State* L);
		int unbind(lua_State* L);

		int bind_texture(lua_State* L);
		int unbind_texture(lua_State* L);
		int dump_textures(lua_State* L);

		int bind_shader(lua_State* L);
		int unbind_shader(lua_State* L);

		int bind_framebuffer(lua_State* L);
		int unbind_framebuffer(lua_State* L);
		int save_framebuffer_as(lua_State* L);

		int translate(lua_State* L);
		int scale(lua_State* L);

		int clear(lua_State *L);
		int set_blending(lua_State *L);
		int valid(lua_State *L);
	};


}

#endif // _PXF_LAPP_GRAPHICS_H_
