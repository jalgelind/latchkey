#ifndef _PXF_LAPP_LAPP_H_
#define _PXF_LAPP_LAPP_H_
//
// Adaption of Fabric to the Native pxf-base interface
//
// Fabric (c) Sven Andersson - http://www.svenandersson.se
// Lapp (c) Johannes Algelind
//

#include <Pxf/Kernel.h>
#include <Pxf/Base/Config.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Timer.h>
#include <Pxf/Base/String.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Util/ApplicationListner.h>

#include <string>
#include <map>
#include <vector>
#include <functional>
#include <memory>

#include <Lapp/Lapp_Common.h>


namespace Pxf
{
	namespace Resource
	{
		class Raw;
	}

	/* logger device */
	class LappLogger : public LoggingDevice
	{
	public:
		
		// Store full strings, we don't know if any pointers to loggers/strings
		// are still available/valid when we poll messages
		struct Msg
		{
			std::string target;
			std::string identifier;
			std::string message;
			char type;
		};
		std::vector<Msg> m_MessageQueue;

		std::vector<Msg>& GetMessages() { return m_MessageQueue; }
		
		LogMessage* m_Message;

		LappLogger() {}
		virtual ~LappLogger(){};
		virtual bool WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength);
		virtual bool WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength);
		virtual void PreWrite(LogMessage* _Message) { m_Message = _Message; };
	};

	int pxf_loader (lua_State *_L);
	void register_resource_loader(lua_State* _L);

	class Application;
	class Lapp : public ApplicationListner
	{
		enum AppState {
			LAPP_UNKNOWN,
			LAPP_RUNNING,
			LAPP_ERROR,
		};

		AppState m_AppState;

		// states

		lua_State* L;
		bool m_Initiated;
		bool m_Shutdown;
		bool m_Reboot;

		std::string m_BootScript;
		std::string m_ApplicationScript;
		std::string m_ApplicationRoot;

		Pxf::Logger host_logger_;
		std::shared_ptr<LappLogger> logger_;

		std::vector< std::function<void(lua_State*)> > m_Hooks;
		std::vector<std::shared_ptr<Resource::Raw>> m_LoadedFiles;

		// Timers
		Pxf::Timer m_TimerUpdate;
		Pxf::Timer m_TimerDraw;


		Pxf::Kernel* m_kernel;
		Pxf::Resource::ResourceManager* m_mgr;
		Pxf::Graphics::GraphicsDevice* m_gfx;
		Pxf::Input::InputDevice* m_inp;
		Pxf::Audio::AudioDevice* m_snd;
		Pxf::Network::NetworkDevice* m_net;
		Pxf::Resource::ResourceManager* m_res;
		std::shared_ptr<Pxf::Graphics::Window> m_win;
		glm::mat4 m_ProjectionMatrix;

		// Register lua libs and callbacks
		void _register_lua_libs_callbacks();
		void _register_own_callbacks();
		void _run_external_setup_hooks();

		// Callback methods
		static int Print(lua_State *_L);
		static int PrintDbg(lua_State *_L);
		static int Uptime(lua_State *_L);
		static int LoadFile(lua_State *_L);
		static int ImportFile(lua_State *_L);

	public:
		Application* app_;

		Lapp(Application* app, const std::string& appscript = "data/app/default.lua");
		~Lapp();

		static Lapp* GetInstance();

		const std::string& GetApplicationRoot()
		{
			return m_ApplicationRoot;
		}

		void SetApplicationScript(const std::string& script = "default.lua");

		void RemoveLogger();

		void InstallSetupHook(std::function<void(lua_State*)> hook)
		{
			m_Hooks.push_back(hook);
		}

		// When a file is loaded in Lua, our custom loader calls this method.
		void _AddLoadedFile(const std::shared_ptr<Resource::Raw>& text)
		{
			if (std::find(begin(m_LoadedFiles), end(m_LoadedFiles), text) == end(m_LoadedFiles))
				m_LoadedFiles.push_back(text);
		}

		virtual void OnFileRefresh(const std::vector<std::string>& paths);
		void OnShutdown() { Shutdown(); }

		void CleanUp();

		void _Init();

		void Init();
		bool Boot();
		void Reboot();
		void Shutdown();

		bool Update();
		void Draw();
	};
}

#endif // _PXF_LAPP_LAPPH_
