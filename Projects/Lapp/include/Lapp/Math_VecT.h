#ifndef _PXF_LAPP_MATH_VECT_H_
#define _PXF_LAPP_MATH_VECT_H_

namespace Pxf
{
	template <class Class, typename T>
	class LVecT {
	public:
		T vec_;
	public:
		LVecT()
		{
			vec_ = T();
		}
		~LVecT()
		{}

		// common functions

		int distance(lua_State *L)
		{
			Class* v = Luna<Class>::check(L, 1);
			lua_pushnumber(L, glm::distance(vec_, v->vec_));
			return 1;
		}

		int dot(lua_State *L)
		{
			Class* v = Luna<Class>::check(L, 1);
			lua_pushnumber(L, glm::dot(vec_, v->vec_));
			return 1;
		}

		int faceforward(lua_State *L)
		{
			Class* I = Luna<Class>::check(L, 1);
			Class* Nref = Luna<Class>::check(L, 2);
			Class* n = new Class(L);
			n->vec_ = glm::faceforward(vec_, I->vec_, Nref->vec_);
			Luna<Class>::push(L, n);
			return 1;
		}

		int length(lua_State *L)
		{
			lua_pushnumber(L, glm::length(vec_));
			return 1;
		}

		int normalize(lua_State *L)
		{
			Class* n = new Class(L);
			n->vec_ = glm::normalize(vec_);
			Luna<Class>::push(L, n);
			return 1;
		}

		int reflect(lua_State *L)
		{
			Class* N = Luna<Class>::check(L, 1);
			Class* n = new Class(L);
			n->vec_ = glm::reflect(vec_, N->vec_);
			Luna<Class>::push(L, n);
			return 1;
		}

		int refract(lua_State *L)
		{
			Class* N = Luna<Class>::check(L, 1);
			lua_Number eta = luaL_checknumber(L, 2);
			Class* n = new Class(L);
			n->vec_ = glm::refract(vec_, N->vec_, eta);
			Luna<Class>::push(L, n);
			return 1;
		}

		// common operators
		int __add(lua_State *L)
		{
			Class* v = new Class(L);
			if (lua_isnumber(L, 1))
				v->vec_ = vec_ + (float)lua_tonumber(L, 1);
			else
			{
				Class* rhs = Luna<Class>::check(L, 1);
				v->vec_ = vec_ + rhs->vec_;
			}
			Luna<Class>::push(L, v);
			return 1;
		}

		int __mul(lua_State *L)
		{
			Class* v = new Class(L);
			if (lua_isnumber(L, 1))
				v->vec_ = vec_ * (float)lua_tonumber(L, 1);
			else
			{
				Class* rhs = Luna<Class>::check(L, 1);
				v->vec_ = vec_ * rhs->vec_;
			}
			Luna<Class>::push(L, v);
			return 1;
		}

		int __div(lua_State *L)
		{
			Class* v = new Class(L);
			if (lua_isnumber(L, 1))
				v->vec_ = vec_ / (float)lua_tonumber(L, 1);
			else
			{
				Class* rhs = Luna<Class>::check(L, 1);
				v->vec_ = vec_ / rhs->vec_;
			}
			Luna<Class>::push(L, v);
			return 1;
		}

		int __eq(lua_State *L)
		{
			Class* rhs = Luna<Class>::check(L, 1);
			lua_pushboolean(L, vec_ == rhs->vec_);
			return 1;
		}

	};
}
#endif // _PXF_LAPP_MATH_VECT_H_
