#ifndef _PXF_LAPP_APP_H_
#define _PXF_LAPP_APP_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Application;
	class Kernel;
	class Lapp_App
	{
		Kernel* kernel;
		Application* app;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_App>::RegType methods[];

		Lapp_App(lua_State *L, void* data);
		~Lapp_App();

		int get_fps(lua_State* L);
		int get_uptime(lua_State *L);
		int get_exe_argstr(lua_State* L);
		int get_exe_args(lua_State* L);
		int get_exe_name(lua_State* L);
		int get_exe_path(lua_State* L);
		int get_exe_type(lua_State* L);
		int get_app_root(lua_State* L);
		int get_platform(lua_State* L);
		int get_platform_family(lua_State* L);
		int is_debug(lua_State* L);
		int get_configuration(lua_State* L);
		int get_cwd(lua_State* L);
		int set_cwd(lua_State* L);
		int set_title(lua_State* L);
		int listdir(lua_State* L);
		int quit(lua_State* L);
	};
}

#endif // _PXF_LAPP_APP_H_
