#ifndef _PXF_LAPP_TEXTURE_H_
#define _PXF_LAPP_TEXTURE_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
		class Texture;
	}
	namespace Resource
	{
		class Image;
	}

	class Lapp_Texture {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		std::shared_ptr<Pxf::Resource::Image> image;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Texture>::RegType methods[];

		std::shared_ptr<Pxf::Graphics::Texture> texture;

		Lapp_Texture(lua_State *L, void* data);
		~Lapp_Texture();

		int load(lua_State *L);
		int make(lua_State *L);
		int bind(lua_State* L);
		int unbind(lua_State* L);
		int destroy(lua_State* L);
		int save_as(lua_State* L);
		int set(lua_State* L);
		int size(lua_State *L);
		int id(lua_State *L);
		int valid(lua_State *L);
	};
}

#endif // _PXF_LAPP_TEXTURE_H_
