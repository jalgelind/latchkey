#ifndef _PXF_LAPP_FRAMEBUFFER_H_
#define _PXF_LAPP_FRAMEBUFFER_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
	}

	class Lapp_Framebuffer {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		bool is_valid;
	public:
		std::shared_ptr<Pxf::Graphics::FrameBuffer> fbo;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Framebuffer>::RegType methods[];

		Lapp_Framebuffer(lua_State *L, void* data);
		~Lapp_Framebuffer();

		int attach(lua_State *L);
		int detach(lua_State *L);
		int num_color_attachments(lua_State *L);
		int has_depth(lua_State *L);
		int has_stencil(lua_State *L);
		int bind(lua_State *L);
		int unbind(lua_State *L);
		int valid(lua_State *L);
	};
}

#endif // _PXF_LAPP_FRAMEBUFFER_H_
