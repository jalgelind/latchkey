#ifndef _PXF_LAPP_FONT_H_
#define _PXF_LAPP_FONT_H_

#include <Lapp/Lapp_Common.h>
#include <Pxf/Graphics/TextProvider.h> // Need this for inner class TextProvider::String

namespace Pxf
{
	class Lapp_Texture;
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
	}
	namespace Resource
	{
		class Font;
	}

	class Lapp_Font {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		std::shared_ptr<Pxf::Resource::Font> font;
		std::shared_ptr<Pxf::Graphics::TextProvider> fontlabel;
		std::shared_ptr<Pxf::Graphics::TextProvider::String> prepared;
		std::shared_ptr<Lapp_Texture> texture_;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Font>::RegType methods[];

		Lapp_Font(lua_State *L, void* data);
		~Lapp_Font();
		int load(lua_State *L);
		int get_texture(lua_State* L);

		int create_batch(lua_State* L);

		int print(lua_State *L);
		int set_color(lua_State* L);
		int set_default_color(lua_State* L);
		int set_draw_offset(lua_State* L);

		int valid(lua_State *L);

		std::shared_ptr<Pxf::Graphics::TextProvider> get_provider()
		{
			return fontlabel;
		}
	};
}

#endif // _PXF_LAPP_FONT_H_
