#ifndef _PXF_LAPP_MATH_MAT4_H_
#define _PXF_LAPP_MATH_MAT4_H_

#include "Lapp_Common.h"

namespace Pxf
{
	class Math_Mat4 {
	public:
		glm::mat4 mat_;
	public:
		static const char className[];
		bool enable_gc;

		Math_Mat4(lua_State *L, void* data = 0)
			: enable_gc(true)
		{
			mat_ = glm::mat4(1.f);
		}
		~Math_Mat4()
		{
		}

		// methods
		int determinant(lua_State *L);
		int inverse(lua_State *L);
		int transpose(lua_State *L);
		int translate(lua_State *L);
		int rotate(lua_State *L);
		int scale(lua_State *L);
		
		// operators
		int __add(lua_State *L);
		int __mul(lua_State *L);
		int __div(lua_State *L);
		int __eq(lua_State *L);

		static const Luna<Math_Mat4>::FunctionType methods[];
		static const Luna<Math_Mat4>::PropertyType properties[];
	};
}

#endif // _PXF_LAPP_MATH_MAT4_H_
