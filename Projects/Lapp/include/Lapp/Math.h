#ifndef _PXF_LAPP_MATH_H_
#define _PXF_LAPP_MATH_H_

#include <Pxf/Math/Math.h>

#include "Math_Vec2.h"
#include "Math_Vec3.h"
#include "Math_Mat4.h"

// Lua math: http://lua-users.org/wiki/MathLibraryTutorial
// TODO: multiply matrix with vector, multiply vector with matrix (?)

namespace Pxf
{
	// TODO: proper handling of multiple types?
	int math_length(lua_State* L)
	{
		float length = 0.f;
		Math_Vec2* v2;
		Math_Vec3* v3;
		if (v2 = Luna<Math_Vec2>::lightcheck(L, 1))
			lua_pushnumber(L, glm::length(v2->vec_));
		else if (v3 = Luna<Math_Vec3>::lightcheck(L, 1))
			lua_pushnumber(L, glm::length(v3->vec_));
		else
			lua_pushnil(L);
		return 1;
	}

	int mat4_ortho(lua_State* L)
	{
		glm::mat4 mtx;
		lua_Number left = luaL_checknumber(L, 1);
		lua_Number right = luaL_checknumber(L, 2);
		lua_Number bottom = luaL_checknumber(L, 3);
		lua_Number top = luaL_checknumber(L, 4);
		if (lua_gettop(L) == 6)
		{
			lua_Number zn = luaL_checknumber(L, 5);
			lua_Number zf = luaL_checknumber(L, 6);
			mtx = glm::ortho(left, right, bottom, top, zn, zf);
		}
		else
			mtx = glm::ortho(left, right, bottom, top);
		Math_Mat4* v = new Math_Mat4(L);
		v->mat_ = mtx;
		Luna<Math_Mat4>::push(L, v);
		return 1;
	}

	int mat4_perspective(lua_State* L)
	{
		glm::mat4 mtx;
		lua_Number fovy = luaL_checknumber(L, 1);
		lua_Number aspect = luaL_checknumber(L, 2);
		lua_Number zn = luaL_checknumber(L, 3);
		lua_Number zf = luaL_checknumber(L, 4);
		mtx = glm::perspective(fovy, aspect, zn, zf);
		Math_Mat4* v = new Math_Mat4(L);
		v->mat_ = mtx;
		Luna<Math_Mat4>::push(L, v);
		return 1;
	}

	int mat4_perspective_fov(lua_State* L)
	{
		glm::mat4 mtx;
		lua_Number fov = luaL_checknumber(L, 1);
		lua_Number width = luaL_checknumber(L, 2);
		lua_Number height = luaL_checknumber(L, 3);
		lua_Number zn = luaL_checknumber(L, 4);
		lua_Number zf = luaL_checknumber(L, 5);
		mtx = glm::perspectiveFov(fov, width, height, zn, zf);
		Math_Mat4* v = new Math_Mat4(L);
		v->mat_ = mtx;
		Luna<Math_Mat4>::push(L, v);
		return 1;
	}

	static const luaL_Reg mathext[] =
	{
	  {"length",   math_length},
	  {"ortho",   mat4_ortho},
	  {"perspective",   mat4_perspective},
	  {"perspective_fov",   mat4_perspective_fov},
	  {NULL, NULL}
	};

	void RegisterLappMath(lua_State* L)
	{
		Luna<Math_Vec2>::Register(L, "math");
		Luna<Math_Vec3>::Register(L, "math");
		Luna<Math_Mat4>::Register(L, "math");

		lua_getglobal(L, "math");
		luaL_setfuncs(L, mathext, 0);
		lua_pop(L, 1);
	}
}
#endif // _PXF_LAPP_MATH_H_
