#ifndef _PXF_LAPP_BATCH_H_
#define _PXF_LAPP_BATCH_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
	namespace Graphics
	{
		class GraphicsDevice;
		class PrimitiveBatch;
	}

	class Lapp_Batch {
		Pxf::Kernel* kernel;
		Pxf::Graphics::GraphicsDevice* device;
		std::shared_ptr<Pxf::Graphics::PrimitiveBatch> batch;
		bool is_valid;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Batch>::RegType methods[];

		Lapp_Batch(lua_State *L, void* data);
		~Lapp_Batch();

		int set_color(lua_State* L);
		int set_texture_subset(lua_State* L);
		int points_begin(lua_State* L);
		int points_add(lua_State* L);
		int points_end(lua_State* L);

		int lines_begin(lua_State* L);
		int lines_add(lua_State* L);
		int lines_end(lua_State* L);

		int quads_begin(lua_State* L);
		int quads_add_top_left(lua_State* L);
		int quads_add_centered(lua_State* L);
		int quads_set_rotation(lua_State* L);
		int quads_end(lua_State* L);
		int valid(lua_State *L);
	};
}
#endif // _PXF_LAPP_BATCH_H_
