#include <Lapp/Lapp_Timer.h>

using namespace Pxf;

const char Lapp_Timer::classNamespace[] = "lapp";
const char Lapp_Timer::className[] = "timer";

Lunar<Lapp_Timer>::RegType Lapp_Timer::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Timer, start),
    LUNAR_DECLARE_METHOD(Lapp_Timer, stop),
    LUNAR_DECLARE_METHOD(Lapp_Timer, interval),
    {0,0}
};

Lapp_Timer::Lapp_Timer(lua_State *L, void* data)
{
}

Lapp_Timer::~Lapp_Timer()
{}

int Lapp_Timer::start(lua_State* L)
{
    timer.Start();
    return 0;
}

int Lapp_Timer::stop(lua_State* L)
{
    timer.Stop();
    return 0;
}

int Lapp_Timer::interval(lua_State* L)
{
    lua_pushnumber(L, timer.Interval());
    return 1;
}