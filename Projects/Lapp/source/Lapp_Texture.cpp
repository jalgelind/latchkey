#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Texture.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Image.h>

#include <Lapp/Lapp_Texture.h>

using namespace Pxf;


const char Lapp_Texture::classNamespace[] = "lapp";
const char Lapp_Texture::className[] = "texture";

Lunar<Lapp_Texture>::RegType Lapp_Texture::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Texture, load),
    LUNAR_DECLARE_METHOD(Lapp_Texture, make),
    LUNAR_DECLARE_METHOD(Lapp_Texture, valid),
    LUNAR_DECLARE_METHOD(Lapp_Texture, bind),
    LUNAR_DECLARE_METHOD(Lapp_Texture, unbind),
    LUNAR_DECLARE_METHOD(Lapp_Texture, destroy),
    LUNAR_DECLARE_METHOD(Lapp_Texture, save_as),
    LUNAR_DECLARE_METHOD(Lapp_Texture, set),
    LUNAR_DECLARE_METHOD(Lapp_Texture, size),
    LUNAR_DECLARE_METHOD(Lapp_Texture, id),
    {0,0}
};



Lapp_Texture::Lapp_Texture(lua_State *L, void* data)
    : image(0)
    , texture(0)
    , is_valid(false)
{
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
	
	// if a texture is passed from e.g Lapp_Font...
	if (lua_gettop(L) > 0 && lua_islightuserdata(L, -1))
	{
		texture = *((const std::shared_ptr<Pxf::Graphics::Texture>*)lua_touserdata(L, -1));
		is_valid = true;
		lua_pop(L, 1);
	}
	else
	{
		if (lua_isnumber(L, 1) && lua_isnumber(L, 2))
		{
			make(L);
		}
		else if (lua_isstring(L, 1))
		{
			load(L);
		}

		else if (lua_gettop(L) == 0)
		{
			lua_pushinteger(L, 32);
			lua_pushinteger(L, 32);
			make(L);
			is_valid = false;
		}
		else
			LAPP_ERROR(L, "Invalid arguments. Expected nothing, string or width/height.");
	}
}
Lapp_Texture::~Lapp_Texture()
{
	if (texture)
		texture->Unload();
}

int Lapp_Texture::load(lua_State *L)
{
    is_valid = false;
    const char* str = luaL_checkstring(L, 1);
    auto mgr = kernel->GetResourceManager();

	if (image)
	{
		image->RemoveDependency(texture);
	}

    image = mgr->Acquire<Pxf::Resource::Image>(str);
    if (image && image->IsReady())
    {
        if (texture)
            texture->LoadData(image);
        else
            texture = device->CreateTexture(image);
		image->AddDependency(texture);
        is_valid = true;
    }
    return 0;
}

int Lapp_Texture::make(lua_State *L)
{
	is_valid = false;
	if (lua_gettop(L) > 3)
		LAPP_ERROR(L, "too many arguments; usage: make(w, h, [format=r|rgb|rgba|depth|depth+stencil])");
	int w = luaL_checkinteger(L, 1);
	int h = luaL_checkinteger(L, 2);
	Graphics::TextureFormat fmt = Graphics::TEX_FORMAT_RGBA;
	if (lua_gettop(L) == 3)
	{
		const std::string& sfmt = luaL_checkstring(L, 3);
		if (sfmt == "rgb")
			fmt = Graphics::TEX_FORMAT_RGB;
		else if (sfmt == "rgba")
			fmt = Graphics::TEX_FORMAT_RGBA;
		else if (sfmt == "depth")
			fmt = Graphics::TEX_FORMAT_DEPTH;
		else if (sfmt == "depthstencil" || sfmt == "depth+stencil")
			fmt = Graphics::TEX_FORMAT_DEPTH_STENCIL;
		else if (sfmt == "r")
			fmt = Graphics::TEX_FORMAT_R;
	}
	texture = device->CreateEmptyTexture(w, h, fmt);
	is_valid = true;
	return 0;
}

int Lapp_Texture::bind(lua_State* L)
{
    int unit = 0;
    if (lua_gettop(L) == 1)
        unit = luaL_checknumber(L, 1);
	if (is_valid)
		device->BindTexture(texture, unit);
	else
		device->BindTexture(device->GetDebugTexture(), unit);
    return 0;
}

int Lapp_Texture::unbind(lua_State* L)
{
    int unit = 0;
    if (lua_gettop(L) == 1)
        unit = luaL_checknumber(L, 1);
    device->BindTexture(0, unit);
    return 0;
}

int Lapp_Texture::destroy(lua_State* L)
{
    if (!is_valid) return 0;
    texture.reset();
    is_valid = false;
    return 0;
}

int Lapp_Texture::save_as(lua_State* L)
{
	if (!is_valid) return 0;
	const char* fn = luaL_checkstring(L, 1);
	auto image = texture->ToImage();
	image->SaveAs(fn);
	return 0;
}

int Lapp_Texture::set(lua_State* L)
{
	//) def: option : string, value : bool | number
	//) option: min, mag, clamp, force_pot, mipmaps
    const char* err_unk = "unknown option '%s' (available are 'min', 'mag', 'clamp', 'force_pot' and 'mipmaps'.";
    if (!is_valid) return 0;
    if (lua_gettop(L) != 2)
        LAPP_ERROR(L, "expected two arguments.");
    std::string opt = luaL_checkstring(L, 1);
    if (lua_isstring(L, 2))
    {
        std::string value = luaL_checkstring(L, 2);
        auto v = Graphics::TEX_FILTER_NEAREST;
        if (opt == "min")
        {
            if      (value == "nearest") v = Graphics::TEX_FILTER_NEAREST;
            else if (value == "linear")  v = Graphics::TEX_FILTER_LINEAR;
            else if (value == "linear_mipmap_linear")  v = Graphics::TEX_FILTER_LINEAR_MIPMAP_LINEAR;
            else if (value == "linear_mipmap_nearest")  v = Graphics::TEX_FILTER_LINEAR_MIPMAP_NEAREST;
            else if (value == "nearest_mipmap_linear")  v = Graphics::TEX_FILTER_NEAREST_MIPMAP_LINEAR;
            else if (value == "nearest_mipmap_neraest")  v = Graphics::TEX_FILTER_NEAREST_MIPMAP_NEAREST;
            else
                LAPP_ERROR(L, "unknown value for min-filter: '%s' "
                              "(available are 'nearest', 'linear', 'linear_mipmap_linear', 'linear_mipmap_nearest', "
                              "'nearest_mipmap_linear', 'nearest_mipmap_nearest')", value.c_str());
            texture->SetMinFilter(v);
        }
        else if (opt == "mag")
        {
            auto v = Graphics::TEX_FILTER_NEAREST;
            if      (value == "nearest") v = Graphics::TEX_FILTER_NEAREST;
            else if (value == "linear")  v = Graphics::TEX_FILTER_LINEAR;
            else
                LAPP_ERROR(L, "unknown value for mag-filter: '%s' (available are 'nearest', 'linear')", value.c_str());
            texture->SetMagFilter(v);
        }
        else if (opt == "clamp")
        {
            auto v = Graphics::TEX_CLAMP;
            if      (value == "clamp") v = Graphics::TEX_CLAMP;
            else if (value == "clamp_to_edge")  v = Graphics::TEX_CLAMP_TO_EDGE;
            else if (value == "repeat")  v = Graphics::TEX_REPEAT;
            else
                LAPP_ERROR(L, "unknown value for clamp: '%s' (available are 'clamp', 'clamp_to_edge', 'repeat')", value.c_str());
            texture->SetClampMethod(v);
        }
        else
            LAPP_ERROR(L, err_unk, opt.c_str());
    }
    else if (lua_isboolean(L, 2))
    {
        if (opt == "force_pot")
        {
			bool force_pot = lua_toboolean(L, 2);
            texture->SetPOTHint(force_pot);
        }
        else if (opt == "mipmaps")
        {
			bool mipmapping = lua_toboolean(L, 2);
            texture->SetMipmapHint(mipmapping);
        }
        else
            LAPP_ERROR(L, err_unk, opt.c_str());
    }
    else
        LAPP_ERROR(L, "unexpected value type.");
    return 0;
}

int Lapp_Texture::size(lua_State *L)
{
    if (!is_valid) return 0;
    lua_pushnumber(L, texture->GetWidth());
    lua_pushnumber(L, texture->GetHeight());
    return 2;
}

int Lapp_Texture::id(lua_State *L)
{
    if (!is_valid) return 0;
    lua_pushinteger(L, texture->GetTextureID());
    return 1;
}

int Lapp_Texture::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
