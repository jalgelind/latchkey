#include <Lapp/Lapp_Common.h>

#include <Pxf/Kernel.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Raw.h>

#include <Lapp/Lapp.h>

#include <string>

//) option: ignore

extern Pxf::Logger g_LappLogger;

int Pxf::lapp_loadfile(lua_State* L, const char* filepath)
{
	Pxf::Resource::ResourceManager* res = Kernel::GetInstance()->GetResourceManager();
	auto text = res->Acquire<Resource::Raw>(filepath, "txt");
	if (text)
	{
		int size = text->Length();
		std::string path = filepath;
		std::string str = std::string(text->Ptr(), size) + "\n";
		const char* data = text->Ptr();
		int s = luaL_loadbuffer(L, str.c_str(), str.size()-1, ("@" + path).c_str());
		Lapp::GetInstance()->_AddLoadedFile(text);
		return s;
	}
	return LUA_ERRRUN;
}

bool Pxf::lapp_check_error(lua_State* L, int status)
{
	bool ok = status == LUA_OK;
	if (!ok)
	{
		const char* errtype = "";
		switch(status)
		{
		case LUA_ERRRUN:   errtype = "Runtime error"; break;
		case LUA_ERRSYNTAX:errtype = "Syntax error"; break;
		case LUA_ERRMEM:   errtype = "Memory error"; break;
		case LUA_ERRERR:   errtype = "Error running the error handler"; break;
		}

		const char* errmsg = lua_tostring(L, -1);
		g_LappLogger.Error("%s: %s", errtype, errmsg);
		lua_pop(L, 1);

		lua_getglobal(L, "_on_error_signal");
		if (lua_isfunction(L, -1))
			lua_pcall(L, 0, 0, 0);
		else
			lua_pop(L, 1);

		lua_getglobal(L, "_on_log_message");
		if (lua_isfunction(L, -1))
		{
			lua_pushstring(L, errmsg);
			lua_pcall(L, 1, 0, 0);
		}
		else
			lua_pop(L, 1);
	}
	return ok;
}

bool Pxf::lapp_call(lua_State* L, const char* func, int nargs, int nres)
{
	if (lua_gettop(L) < nres)
		printf("Warning: possibly missing arguments to lapp_call(%s, %d, %d), lua_gettop(L) == %d\n", func, nargs, nres, lua_gettop(L));
	lua_getglobal(L, "debug");
	lua_getfield(L, -1, "traceback");
	lua_remove(L, -2);
	int ehpos = lua_gettop(L);
	lua_getglobal(L, func);
	int fun = lua_gettop(L);

	int num = lua_gettop(L);
	if (nargs > 0)
	{
		for(int i = num - nargs - 1; i < num - 1; i++)
		{
			lua_pushvalue(L, i);
		}
	}

	int s = LUA_ERRERR;
	bool ok = false;
	if (lua_isfunction(L, fun))
	{
		s = lua_pcall(L, nargs, nres, ehpos);
		ok = lapp_check_error(L, s);
	}
	else
		lua_pop(L, 1); // pop function
	lua_remove(L, -nres - 1); // remove traceback

	// remove duplicated values
	for (int i = 0; i < nargs; i++)
		lua_remove(L, -nres - 1);
	return ok;
}

bool Pxf::lapp_run_script(lua_State* L, const char* filepath)
{
	int s = lapp_loadfile(L, filepath);

	bool ok = lapp_check_error(L, s);

	if (!ok)
		return false; // failed to load file...
	int top = lua_gettop(L);
	if (lua_isfunction(L, -1))
	{
		s = lua_pcall(L, 0, LUA_MULTRET, 0);
	}
	ok = lapp_check_error(L, s);
	lua_pop(L, lua_gettop(L) - top);
	return ok;
}

void Pxf::lapp_pusherror(lua_State* L, const char* msg, const char* nativeinfo)
{
	luaL_traceback(L, L, msg, 0);
	lua_pushstring(L, "\n\nError assertion at: ");
	lua_pushstring(L, nativeinfo);
	lua_concat(L, 3);
	lua_error(L);
}

void Pxf::lapp_stackdump(lua_State *L) {
	int i;
	int top = lua_gettop(L);
	printf("STACKDUMP:\n");
	for (i = 1; i <= top; i++) {  /* repeat for each level */
		int t = lua_type(L, i);
		printf("%d: ", i);
		switch (t) {
		case LUA_TSTRING:  /* strings */
			printf("`%s'", lua_tostring(L, i));
			break;

		case LUA_TBOOLEAN:  /* booleans */
			printf(lua_toboolean(L, i) ? "true" : "false");
			break;

		case LUA_TNUMBER:  /* numbers */
			printf("%g", lua_tonumber(L, i));
			break;

		default:  /* other values */
			printf("%s", lua_typename(L, t));
			break;

		}
		printf("\n");  /* put a separator */
		fflush(stdout);
	}
	printf("\n");  /* end the listing */
}
