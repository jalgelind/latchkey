#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Image.h>

#include <Lapp/Lapp_Window.h>

using namespace Pxf;


const char Lapp_Window::classNamespace[] = "lapp";
const char Lapp_Window::className[] = "window";

Lunar<Lapp_Window>::RegType Lapp_Window::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Window, size),
    LUNAR_DECLARE_METHOD(Lapp_Window, id),
    {0,0}
};



Lapp_Window::Lapp_Window(lua_State *L, void* data)
    : window(0)
    , is_valid(false)
{
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();

	if (lua_gettop(L) > 0 && lua_islightuserdata(L, -1))
	{
		window = (Pxf::Graphics::Window*)lua_touserdata(L, -1);
		is_valid = true;
		lua_pop(L, 1);
	}
	else
	{
		LAPP_ERROR(L, "Don't create this object manually. Window instance expected.");
	}
}
Lapp_Window::~Lapp_Window()
{
}

int Lapp_Window::size(lua_State *L)
{
    if (!is_valid) return 0;
    lua_pushnumber(L, window->GetWidth());
    lua_pushnumber(L, window->GetHeight());
    return 2;
}

int Lapp_Window::id(lua_State *L)
{
    if (!is_valid) return 0;
    lua_pushinteger(L, window->GetID());
    return 1;
}

int Lapp_Window::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
