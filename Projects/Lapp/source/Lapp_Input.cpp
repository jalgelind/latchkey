#include <Lapp/Lapp_Input.h>
#include <Pxf/Kernel.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Input/InputDevice.h>
#include <Pxf/Input/Keyboard.h>
#include <Pxf/Input/Mouse.h>

using namespace Pxf;

//) option: singleton

//) Provides access to the input backend. Only keyboard and mouse is currently implemented.
//)
//) By incuding the wrapper 'inp', all methods can be accessed as regular functions, e.g. inp.get_events()


const char Lapp_Input::classNamespace[] = "lapp";
const char Lapp_Input::className[] = "input";

Lunar<Lapp_Input>::RegType Lapp_Input::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Input, valid),

	LUNAR_DECLARE_METHOD(Lapp_Input, get_mouse_pos),
	LUNAR_DECLARE_METHOD(Lapp_Input, is_button_down),
	LUNAR_DECLARE_METHOD(Lapp_Input, get_button_states),
	LUNAR_DECLARE_METHOD(Lapp_Input, get_mousewheel_delta),
	LUNAR_DECLARE_METHOD(Lapp_Input, is_key_down),
	LUNAR_DECLARE_METHOD(Lapp_Input, get_events),
	{0,0}
};


Lapp_Input::Lapp_Input(lua_State *L, void* data)
{
	is_valid = false;
	kernel = Pxf::Kernel::GetInstance();
	device = kernel->GetGraphicsDevice();
	inp = kernel->GetInputDevice();
	mouse = &inp->GetMouse();
	kbd = &inp->GetKeyboard();

	// TODO: register input constants, e.g lapp.input.MOUSE_1?
	/*
	lua_getglobal(L, classNamespace);
	lua_getfield(L, -1, className);
	int mt = lua_gettop(L);
	#define INPUT_KEYNAME(name, value) { lua_pushinteger(L, Pxf::Input::name); lua_setfield(L, mt, #name);}
	#define INPUT_MOUSE_BUTTON(name, value) {lua_pushinteger(L, Pxf::Input::name); lua_setfield(L, mt, #name);}
	#define MOUSE_EVENT_TYPE(name)  {lua_pushinteger(L, Pxf::Input::name); lua_setfield(L, mt, #name);}
	#define INPUT_MOUSE_MODE(name, value)  {lua_pushinteger(L, Pxf::Input::name); lua_setfield(L, mt, #name);}
	#include <Pxf/Input/Input.def>
	lua_pop(L, 2);
	*/
}
Lapp_Input::~Lapp_Input()
{
}

static std::string lower(const std::string& str)
{
	std::string s = str;
	StringToLower(s);
	return s;
}

static std::string upper(const std::string& str)
{
	std::string s = str;
	StringToUpper(s);
	return s;
}

int Lapp_Input::get_mouse_pos(lua_State* L)
{
	int w = device->GetActiveWindow()->GetWidth();
	int h = device->GetActiveWindow()->GetHeight();
	int x, y;
	mouse->GetMousePos(&x, &y);
	lua_pushinteger(L, Clamp<int>(x, 0, w));
	lua_pushinteger(L, Clamp<int>(y, 0, h));
	return 2;
}

int Lapp_Input::is_button_down(lua_State* L)
{
	int v = luaL_checkinteger(L, 1);
	if (v == 0)
		LAPP_ERROR(L, "invalid mouse button, left mouse button = 1, ...");
	v += Pxf::Input::MOUSE_1 - 1;
	lua_pushboolean(L, mouse->IsButtonDown(v));
	return 1;
}

int Lapp_Input::get_button_states(lua_State* L)
{
	unsigned int v = mouse->GetButtonStates();
	lua_pushinteger(L, v);
	return 1;
}

int Lapp_Input::is_key_down(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	int v = kbd->GetKeyCode(upper(str).c_str());
	lua_pushboolean(L, kbd->IsKeyDown(v));
	return 1;
}

int Lapp_Input::get_mousewheel_delta(lua_State* L)
{
	lua_pushinteger(L, mouse->GetMouseWheelDelta());
	return 1;
}

static void mk_event_table(lua_State* L, int itemtable, Input::Keyboard::Event& ev, const std::string& type)
{
	if (ev.window)
	{
		lua_pushinteger(L, ev.window->GetID());
		lua_setfield(L, itemtable, "window");
	}

	lua_pushstring(L, type.c_str());
	lua_setfield(L, itemtable, "type");
	
	if (type == "key")
	{
		std::string name = lower(Input::Keyboard::repr((Input::SpecialKey)ev.code));
		lua_pushstring(L, name.c_str());
		lua_setfield(L, itemtable, ev.event_action == Input::Keyboard::Event::KEY_PRESS ? "pressed" : "released");
		lua_pushinteger(L, ev.code);
		lua_setfield(L, itemtable, "code");
	}
	else // char
	{
		std::string name = lower(Input::Keyboard::repr((Input::SpecialKey)ev.code));
		char tmpname[2];
		tmpname[0] = (char)ev.code;
		tmpname[1] = 0;
		if (name == "") name = tmpname;
		lua_pushstring(L, name.c_str());
		lua_setfield(L, itemtable, "pressed"); // no release events for chars
		lua_pushinteger(L, ev.code);
		lua_setfield(L, itemtable, "code");
	}
}

static void mk_event_table(lua_State* L, int itemtable, Input::Mouse::Event& ev, const char* type)
{
	if (ev.window)
	{
		lua_pushinteger(L, ev.window->GetID());
		lua_setfield(L, itemtable, "window");
	}

	lua_pushstring(L, type);
	lua_setfield(L, itemtable, "type");
	switch(ev.type)
	{
	case ev.POSITION:
		lua_pushinteger(L, ev.x);
		lua_setfield(L, itemtable, "x");
		lua_pushinteger(L, ev.y);
		lua_setfield(L, itemtable, "y");
		lua_pushinteger(L, ev.dx);
		lua_setfield(L, itemtable, "dx");
		lua_pushinteger(L, ev.dy);
		lua_setfield(L, itemtable, "dy");
		break;
	case ev.BUTTON:
		if (ev.mouse_pressed != Input::MOUSE_NONE)
		{
			lua_pushinteger(L, ev.mouse_pressed - Input::MOUSE_1 + 1);
			lua_setfield(L, itemtable, "pressed");
		}
		if (ev.mouse_released != Input::MOUSE_NONE)
		{
			lua_pushinteger(L, ev.mouse_released - Input::MOUSE_1 + 1);
			lua_setfield(L, itemtable, "released");
		}
		// click position
		lua_pushinteger(L, ev.x);
		lua_setfield(L, itemtable, "x");
		lua_pushinteger(L, ev.y);
		lua_setfield(L, itemtable, "y");
		break;
	case ev.SCROLL:
		lua_pushinteger(L, ev.scroll_x);
		lua_setfield(L, itemtable, "scroll_x");
		lua_pushinteger(L, ev.scroll_y);
		lua_setfield(L, itemtable, "scroll_y");
		lua_pushinteger(L, ev.scroll_dx);
		lua_setfield(L, itemtable, "scroll_dx");
		lua_pushinteger(L, ev.scroll_dy);
		lua_setfield(L, itemtable, "scroll_dy");
		break;
	}
}

int Lapp_Input::get_events(lua_State* L)
{
	//) def: -> table
	lua_newtable(L);
	int tbl = lua_gettop(L);
	int idx = 1;

	Input::Keyboard::KbdQ& keyQ = kbd->GetKeyQueue();
	Input::Keyboard::KbdQ& charQ = kbd->GetCharQueue();
	Input::Mouse::MouseQ& mouseQ = mouse->GetEventQueue();
	
	// TODO: fix order {mouse1, mouse2, kbd1, kbd2, char1..}
	//              -> {mouse1, kbd1, char1, mouse2...}

	// Process mouse input
	do 
	{
		Input::Mouse::Event ev = mouseQ.Pop();
		if (ev.IsValid())
		{
			lua_newtable(L);
			int tid = lua_gettop(L);
			mk_event_table(L, tid, ev, lower(Input::Mouse::repr(ev.type)).c_str());
			lua_rawseti(L, tbl, idx++);
		}
	} while (mouseQ.Peek().IsValid());


	// Process keyboard input
	do
	{
		Input::Keyboard::Event ev = keyQ.Pop();
		if (ev.IsValid())
		{
			lua_newtable(L);
			int tid = lua_gettop(L);
			mk_event_table(L, tid, ev, "key");
			lua_rawseti(L, tbl, idx++);
		}
	} while (keyQ.Peek().IsValid());

	// Process keyboard character input
	do
	{
		Input::Keyboard::Event ev = charQ.Pop();
		if (ev.IsValid())
		{
			lua_newtable(L);
			int tid = lua_gettop(L);
			mk_event_table(L, tid, ev, "char");
			lua_rawseti(L, tbl, idx++);
		}
	} while (charQ.Peek().IsValid());

	return 1;
}

int Lapp_Input::valid(lua_State *L)
{
	lua_pushboolean(L, is_valid);
	return 1;
}
