#include <Lapp/Lapp.h>

#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Raw.h>
#include <Pxf/Util/Application.h>
#include <Pxf/Modules/dgfx/OpenGL.h>

#include <Lapp/Math.h>

#include <Lapp/Lapp_Gfx.h>
#include <Lapp/Lapp_Random.h>
#include <Lapp/Lapp_App.h>
#include <Lapp/Lapp_Interpolator.h>
#include <Lapp/Lapp_Timer.h>
#include <Lapp/Lapp_Configuration.h>
#include <Lapp/Lapp_Physics.h>

#include <Pxf/Input/Input.h>
#include <Pxf/Input/Keyboard.h>
#include <Pxf/Input/Mouse.h>
#include <Lapp/Lapp_Input.h>

#include <mutex>

//) option: ignore

extern "C" {
#include <luasocket.h>
}

using namespace Pxf;
using namespace Pxf::Graphics;

static Lapp* _appinstance;

Pxf::Logger g_LappLogger;
std::mutex g_LappLogMessageLock;

Pxf::Timer g_TimerRuntime;

const unsigned char default_shader[] = \
	"#vertexshader                   \n"
	"void main()                     \n"
	"{                               \n"
	"   gl_Position = ftransform();  \n"
	"   gl_FrontColor = gl_Color;  \n"
	"}                               \n"
	"#fragmentshader                 \n"
	"void main()                     \n"
	"{                               \n"
	"   vec4 color;                  \n"
	"   color = gl_Color;            \n"
	"   gl_FragColor = color;        \n"
	"}                               \n";


// Logger
bool LappLogger::WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength)
{
	// Put messages in a queue to be polled later by the lua thread
	// calling lua directly from here is not thread safe, as a logger can be invoked from any thread.
	g_LappLogMessageLock.lock();
	Msg msg;
	msg.target = m_Message->source->GetTarget();
	msg.identifier = m_Message->source->GetIdentifier();
	msg.type = m_Message->type;
	msg.message = m_Message->message;
	m_MessageQueue.push_back(msg);
	g_LappLogMessageLock.unlock();
	return true;
}

bool LappLogger::WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength)
{
	std::string s = _SrcBuffer;
	s += "\n";
	return WriteImpl(s.c_str(), s.size());
}

int Pxf::pxf_loader (lua_State *_L)
{
	std::string path = luaL_checkstring(_L, 1);

	if (!Pxf::IsSuffix(path.c_str(), ".lua"))
		path += ".lua";

	int s = lapp_loadfile(_L, path.c_str());
	bool ok = lapp_check_error(_L, s);
	
	if (ok)
	{
		if (lua_isfunction(_L, 1))
			lua_call(_L, 0, 1);
		return 1;
	}

	std::string err = "Error loading file: " + path;
	lua_pushstring(_L, err.c_str());
	lua_error(_L);
	return 0;
}

void Pxf::register_resource_loader(lua_State* _L)
{
	lua_getglobal(_L, "package");
#if LUA_VERSION_NUM > 501
	lua_getfield(_L, -1, "searchers");
#else
	lua_getfield(_L, -1, "loaders");
#endif
	lua_remove(_L, -2);
	int loader_count = 0;
	lua_pushnil(_L);
	while (lua_next(_L, -2) != 0)
	{
		lua_pop(_L, 1);
		loader_count++;
	}
	//lua_pushinteger(_L, loader_count + 1);
	lua_pushinteger(_L, 1); // always use this loader
	lua_pushcfunction(_L, pxf_loader);
	lua_rawset(_L, -3);
	lua_pop(_L, 1);
}

Lapp* Lapp::GetInstance()
{
	return _appinstance;
}

Lapp::Lapp(Application* app, const std::string& appscript)
	: m_snd(0)
	, m_net(0)
	, m_win(nullptr)
	, m_gfx(0)
	, logger_(0)
	, m_AppState(LAPP_UNKNOWN)
	, m_Initiated(false)
	, m_ApplicationScript(appscript)
	, app_(app)
{
	Kernel* kernel = Kernel::GetInstance();
	m_kernel = kernel;

	m_mgr = m_kernel->GetResourceManager();
	m_mgr->RegisterCachedFile("data/shaders/default-shader.glsl", default_shader, sizeof(default_shader)/sizeof(char), 0);
	
	SetApplicationScript(m_ApplicationScript);
	
	// add  custom search paths for lua
	auto paths = m_kernel->GetResourcePaths("*");
	for(auto& path: paths)
	{
		m_kernel->AddResourcePath(Path::Join(path, m_ApplicationRoot), "lua");
	}

	m_kernel->AddResourcePath(m_ApplicationRoot);

}

Lapp::~Lapp()
{
	if (!m_Shutdown)
		Shutdown();

	CleanUp();

	// Unload files loaded by Lua
	for(auto& text: m_LoadedFiles)
	{
		m_res->Release(text);
	}
}

void Lapp::SetApplicationScript(const std::string& script)
{
	m_ApplicationRoot = Path::GetPath(script);

	std::string maybe_boot = Path::Join(m_ApplicationRoot, "boot.lua");
	m_kernel->GetResourcePath(maybe_boot, maybe_boot);
	if (Path::Exist(maybe_boot))
		m_BootScript = maybe_boot;
	m_ApplicationScript = script;
	m_kernel->GetResourcePath(m_ApplicationScript, m_ApplicationScript);
}

void Lapp::_Init()
{
	if (!m_Initiated)
	{
		logger_ = std::make_shared<LappLogger>();
		m_kernel->RegisterLoggingDevice(logger_);

		m_gfx = m_kernel->GetGraphicsDevice();
		m_inp = m_kernel->GetInputDevice();
		m_snd = m_kernel->GetAudioDevice();
		m_net = m_kernel->GetNetworkDevice();
		m_win = m_gfx->GetActiveWindow();
		m_res = m_kernel->GetResourceManager();
		m_Shutdown = false;
		m_Reboot = false;
		m_ProjectionMatrix = glm::ortho(0.f, (float)m_win->GetWidth(), (float)m_win->GetHeight(), 0.f, 100.f, -100.f);
		host_logger_ = m_kernel->GetLogger("lapp.host");
		g_LappLogger = m_kernel->GetLogger("lapp.script");
		_appinstance = this;

		L = NULL;
		m_Initiated = true;
	}
}

void Lapp::Init()
{
	_Init();
	m_win = m_gfx->GetActiveWindow();
	m_AppState = LAPP_UNKNOWN;

	glClearColor(26.0f/255.0f,26.0f/255.0f,26.0f/255.0f,1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER,0.1f);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glClearStencil(0x0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Lapp::OnFileRefresh(const std::vector<std::string>& paths)
{
	for(auto& p: paths)
	{
		bool ok = false;
		// Reboot the application when the main application script or the boot script has been updated
		// Also reboot if the application state is faulty
		if (m_ApplicationScript == p || m_BootScript == p || m_AppState == LAPP_ERROR)
		{
			host_logger_.Information("Rebooting application script.");
			Reboot();
			return;
		}
		// Otherwise, just reload the updated file.
		else
		{
			int s = lapp_loadfile(L, p.c_str());
			lapp_check_error(L, s);
			if (s == LUA_OK)
			{
				s = lua_pcall(L, 0, 0, 0);
				ok = s == LUA_OK;
				lapp_check_error(L, s);
			}
		}

		lua_getglobal(L, "_on_file_reload");
		if (lua_isfunction(L, -1))
		{
			lua_pushboolean(L, ok);
			lua_pushstring(L, p.c_str());
			lua_pcall(L, 2, 1, 0);

			// reboot if function returns true
			if (lua_isboolean(L, -1) && lua_toboolean(L, -1))
				Reboot();

			lua_pop(L, 1); // pop return value
		}
		else
			lua_pop(L, 1); // pop function
	}	
}

void Lapp::CleanUp()
{
	// Close lua state
	if (L != NULL)
	{
		lua_gc(L, LUA_GCCOLLECT, 0);
		lua_close(L);
		L = NULL;
	}

	// reset states
	m_Reboot = false;
}

bool Lapp::Boot()
{
	// Init ourselves
	Init();

	g_TimerRuntime.Start();

	// Init lua state
	L = luaL_newstate();

	// Register lua libs
	_register_lua_libs_callbacks();

	// Register own callbacks
	_register_own_callbacks();

	_run_external_setup_hooks();
	
	host_logger_.Debug("executing boot script: %s", m_BootScript.c_str());
	bool ok = false;
	if (Path::Exist(m_BootScript))
		ok = lapp_run_script(L, m_BootScript.c_str());
	else
	{
		host_logger_.Warning("no boot script found, skipping...");
		ok = true;
	}
	if (ok)
	{
		host_logger_.Debug("executing application script: %s", m_ApplicationScript.c_str());
		ok = lapp_run_script(L, m_ApplicationScript.c_str());
		if (ok)
		{
			m_AppState = LAPP_RUNNING;
			lua_getglobal(L, "_on_init");
			if (lua_isfunction(L, -1))
				lapp_call(L, "_on_init");
			lua_pop(L, 1);
		}
		else
		{
			m_AppState = LAPP_ERROR;
			host_logger_.Error("Failed to run application script");
		}
	}
	else
	{
		m_AppState = LAPP_ERROR;
		host_logger_.Error("Failed to run boot script");
	}

	lua_gc(L, LUA_GCCOLLECT, 0);
	return m_AppState == LAPP_RUNNING;
}

void Lapp::Reboot()
{
	m_Reboot = true;
}

void Lapp::Shutdown()
{
	lua_getglobal(L, "_on_shutdown");
	if (lua_isfunction(L, -1))
	{
		lua_pcall(L, 0, 0, 0);
	}
	else lua_pop(L, 1);
	m_Shutdown = true;
}

bool Lapp::Update()
{
	if (m_Reboot)
	{
		std::string restore_state;
		lua_getglobal(L, "_on_reboot");
		if (lua_isfunction(L, -1))
		{
			lua_pcall(L, 0, 1, 0);
			const char* str = "";
			restore_state = luaL_checkstring(L, -1);
		}
		else lua_pop(L, 1);
		CleanUp();
		Boot();
		lua_getglobal(L, "_on_restore");
		if (lua_isfunction(L, -1))
		{
			lua_pushstring(L, restore_state.c_str());
			lua_pcall(L, 1, 0, 0);
		}
		else lua_pop(L, 1);
	}

	lua_gc(L, LUA_GCSTEP, 200);

	g_LappLogMessageLock.lock();
	auto& msgs = logger_->GetMessages();
	for(auto& msg: msgs)
	{
		lua_getglobal(L, "_on_log_message");
		if (lua_isfunction(L, -1))
		{
			lua_pushinteger(L, msg.type);
			lua_pushstring(L, msg.target.c_str());
			lua_pushstring(L, msg.identifier.c_str());
			lua_pushstring(L, msg.message.c_str() + msg.identifier.size() + 3); // strip identifier
			lua_pcall(L, 4, 0, 0);
		}
		else
			lua_pop(L, 1);
	}
	msgs.clear();
	g_LappLogMessageLock.unlock();

	m_TimerUpdate.Start();
	switch(m_AppState)
	{
	case LAPP_RUNNING:
		if (!lapp_call(L, "_update"))
			m_AppState = LAPP_ERROR;
		break;
	case LAPP_ERROR:
		break;
	default:
		// LAPP_UNKNOWN
		break;
	}
	m_TimerUpdate.Stop();
	return !m_Shutdown;
}

void Lapp::Draw()
{
	m_TimerDraw.Start();
	switch(m_AppState)
	{
	case LAPP_RUNNING:
		if (!lapp_call(L, "_draw"))
			m_AppState = LAPP_ERROR;
		break;
	case LAPP_ERROR:
 		if (!lapp_call(L, "_draw_error"))
		{
			// fatal error!
		}
		break;
	default:
		PXF_ASSERT(0, "Unknown draw state!");
		break;
	}
	m_TimerDraw.Stop();
}

void Lapp::_register_lua_libs_callbacks()
{
	luaL_openlibs(L);
	luaopen_socket_core(L);
	register_lapp_physics(L);

	Lunar<Lapp_App>::Register(L);
	Lunar<Lapp_Interpolator>::Register(L);
	Lunar<Lapp_Timer>::Register(L);
	Lunar<Lapp_Random>::Register(L);
	Lunar<Lapp_Font>::Register(L);
	Lunar<Lapp_TextBatch>::Register(L);
	Lunar<Lapp_TextString>::Register(L);
	Lunar<Lapp_Color>::Register(L);
	Lunar<Lapp_Graphics>::Register(L);
	Lunar<Lapp_Texture>::Register(L);
	Lunar<Lapp_Batch>::Register(L);
	Lunar<Lapp_QuadBuffer>::Register(L);
	Lunar<Lapp_Shader>::Register(L);
	Lunar<Lapp_Framebuffer>::Register(L);
	Lunar<Lapp_Input>::Register(L);
	Lunar<Lapp_Window>::Register(L);
	Lunar<Lapp_Configuration>::Register(L);
	Luna<Lapp_Quad>::Register(L, "lapp");

	RegisterLappMath(L);
}

void Lapp::_register_own_callbacks()
{
	register_resource_loader(L);

	// Register own callbacks
	lua_register(L, "print", Print);
	lua_register(L, "print_dbg", PrintDbg);
	lua_register(L, "uptime", Uptime);

	// Sockets
	//luaopen_socket_core(L);
}

void Lapp::_run_external_setup_hooks()
{
	for (auto& fun: m_Hooks)
	{
		fun(L);
	}
}


///////////////////////////////////////////////////////////////////
// Callback methods

int Lapp::Uptime(lua_State *_L)
{
	int n = lua_gettop(_L);
	if (n != 0)
	{
		lua_pushstring(_L, "Uptime function takes no arguments.");
		lua_error(_L);
	}
	else
	{
		double time = g_TimerRuntime.Interval() / 1000.f;
		lua_pushnumber(_L, time);
		return 1;
	}
	return 0;
}

int Lapp::Print(lua_State *_L)
{
	int n = lua_gettop(_L);
	int i;
	lua_getglobal(_L, "tostring");
	std::string fullstr;
	for (i=1; i<=n; i++) {
		const char *s;
		lua_pushvalue(_L, -1);  /* function to be called */
		lua_pushvalue(_L, i);   /* value to print */
		lua_pcall(_L, 1, 1, 0);
		s = lua_tostring(_L, -1);  /* get result */
		if (s == NULL)
			return luaL_error(_L, LUA_QL("tostring") " must return a string to "
								  LUA_QL("print"));
		fullstr += s;
		if (i>0||i<n-1)
			fullstr += " ";
		lua_pop(_L, 1);  /* pop result */
	}
	g_LappLogger.Debug("%s", fullstr.c_str());
	return 0;
}

int Lapp::PrintDbg(lua_State *_L)
{
	int n = lua_gettop(_L);
	int i;
	lua_getglobal(_L, "tostring");
	std::string fullstr;
	for (i=1; i<=n; i++) {
		const char *s;
		lua_pushvalue(_L, -1);  /* function to be called */
		lua_pushvalue(_L, i);   /* value to print */
		lua_pcall(_L, 1, 1, 0);
		s = lua_tostring(_L, -1);  /* get result */
		if (s == NULL)
			return luaL_error(_L, LUA_QL("tostring") " must return a string to "
								  LUA_QL("print"));
		fullstr += " ";
		fullstr += s;
		lua_pop(_L, 1);  /* pop result */
	}
	printf("DEBUG -%s\n", fullstr.c_str());
	return 0;
}
