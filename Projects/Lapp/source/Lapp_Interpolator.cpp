#include <Pxf/Kernel.h>
#include <Pxf/Util/Application.h>
#include <Pxf/Util/InterpolationManager.h>

#include <Lapp/Lapp_Interpolator.h>

using namespace Pxf;

const char Lapp_Interpolator::classNamespace[] = "lapp";
const char Lapp_Interpolator::className[] = "interpolator";

Lunar<Lapp_Interpolator>::RegType Lapp_Interpolator::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Interpolator, reg),
    LUNAR_DECLARE_METHOD(Lapp_Interpolator, get),
    {0,0}
};


Lapp_Interpolator::Lapp_Interpolator(lua_State *L, void* data)
{
    kernel = Kernel::GetInstance();
    app = Application::GetInstance();
    
}

Lapp_Interpolator::~Lapp_Interpolator()
{
    auto iter = std::begin(variables);
    for(iter; iter != std::end(variables); ++iter)
    {
        app->interpolmgr_->Remove(&iter->second);
    }
}

int Lapp_Interpolator::reg(lua_State* L)
{
    const char* str = luaL_checkstring(L, 1);
    float start = luaL_checknumber(L, 2);
    float duration = luaL_checknumber(L, 3);
    variables[str] = 0.f;
    app->interpolmgr_->RegisterFadeIn<Pxf::EasingExponential>(&variables[str], start, start+duration, 0.f, 1.f);
    return 0;
}

int Lapp_Interpolator::get(lua_State* L)
{
    const char* str = luaL_checkstring(L, 1);
    float v = 0;
    auto iter = variables.find(str);
    if (iter != variables.end())
    {
        v = variables[str];
        lua_pushnumber(L, v);
        return 1;
    }

    LAPP_ERROR(L, "argument error - key does not exist");
    return 0;
}