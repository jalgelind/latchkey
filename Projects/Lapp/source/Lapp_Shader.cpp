#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Shader.h>

#include <Lapp/Lapp_Shader.h>

#include <cmath>

using namespace Pxf;

const char Lapp_Shader::classNamespace[] = "lapp";
const char Lapp_Shader::className[] = "shader";

Lunar<Lapp_Shader>::RegType Lapp_Shader::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Shader, load_source),
    LUNAR_DECLARE_METHOD(Lapp_Shader, load_file),
    LUNAR_DECLARE_METHOD(Lapp_Shader, bind),
    LUNAR_DECLARE_METHOD(Lapp_Shader, unbind),
	LUNAR_DECLARE_METHOD(Lapp_Shader, set_uniformi),
	LUNAR_DECLARE_METHOD(Lapp_Shader, set_uniformf),
	LUNAR_DECLARE_METHOD(Lapp_Shader, set_uniformv),
    LUNAR_DECLARE_METHOD(Lapp_Shader, set_uniform),

    {0,0}
};



Lapp_Shader::Lapp_Shader(lua_State *L, void* data)
{
    is_valid = false;
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    shader = nullptr;
    if (lua_gettop(L) == 0)
    {
        // load default shader
        lua_pushstring(L, "data/shaders/default-shader.glsl");
        load_file(L);
    }
    else
    {
        const char* data = luaL_checkstring(L, 1);
        if (Pxf::Path::Exist(data))
        {
            load_file(L);
        }
        else
        {
            load_source(L);
        }
    }
}
Lapp_Shader::~Lapp_Shader()
{
}

void Lapp_Shader::validate(lua_State* L, Graphics::Shader* shader)
{
	if (!shader || !shader->IsValid())
	{
		is_valid = false;
		LAPP_ERROR(L, "Failed to compile shader.");
	}
	else
	{
		is_valid = true;
	}
}

int Lapp_Shader::load_source(lua_State *L)
{
    const char* source = luaL_checkstring(L, 1);
    const char* ident = "<unnamed-shader>";
    if (lua_gettop(L) == 2)
        ident = luaL_checkstring(L, 2);
    shader = device->CreateShader(ident, source);
    validate(L, shader.get());
    return 0;
}

int Lapp_Shader::load_file(lua_State *L)
{
    const char* path = luaL_checkstring(L, 1);
    const char* ident = "<unnamed-shader>";
    if (lua_gettop(L) == 2)
        ident = luaL_checkstring(L, 2);

    if (Pxf::Path::Exist(path))
    {
        shader = device->CreateShaderFromPath(ident, path);
        validate(L, shader.get());
    }
    return 0;
}

int Lapp_Shader::bind(lua_State *L)
{
    if (shader)
        device->BindShader(shader);
    return 0;
}

int Lapp_Shader::unbind(lua_State *L)
{
    device->BindShader(nullptr);
    return 0;
}

static bool _is_integer(lua_State *L, int narg)
{
    //int isnum;
    //lua_Integer d = lua_tointegerx(L, narg, &isnum);
    //if (!isnum)
    //    return false;
    //return true;
	if (lua_isnumber(L, narg))
	{
		lua_Number number = lua_tonumber(L, narg);
		if (std::fmod(number, 1) == 0)
			return true;
	}
	return false;
}

int Lapp_Shader::set_uniformi(lua_State *L)
{
	const char* name = luaL_checkstring(L, 1);
	shader->SetUniformi(name, luaL_checkinteger(L, 2));
	return 0;
}

int Lapp_Shader::set_uniformf(lua_State *L)
{
	const char* name = luaL_checkstring(L, 1);
	shader->SetUniformf(name, luaL_checknumber(L, 2));
	return 0;
}

int Lapp_Shader::set_uniformv(lua_State *L)
{
	//) def: x : Number, y : Number, z : Number = optional, w : Number = optional
	const char* name = luaL_checkstring(L, 1);
	int x = 0, y = 0, z = 0, w = 0;
    int len = lua_rawlen(L, 2);

    for(int i = 0; i <= len; i++)
        lua_rawgeti(L, 2, i);

    switch(len)
    {
    case 2:
        {
            x = luaL_checknumber(L, -2);
            y = luaL_checknumber(L, -1);
            glm::vec2 vec2 = glm::vec2(x, y);
            shader->SetUniformVec2(name, vec2);
        }
        break;
    case 3:
        {
            x = luaL_checknumber(L, -3);
            y = luaL_checknumber(L, -2);
            z = luaL_checknumber(L, -1);
            glm::vec3 vec3 = glm::vec3(x, y, z);
            shader->SetUniformVec3(name, vec3);
            break;
        }
    case 4:
        {
            x = luaL_checknumber(L, -4);
            y = luaL_checknumber(L, -3);
            z = luaL_checknumber(L, -2);
            w = luaL_checknumber(L, -1);
            glm::vec4 vec4 = glm::vec4(x, y, z, w);
            shader->SetUniformVec4(name, vec4);
        }
        break;
    default:
        LAPP_ERROR(L, "expected table to contain 2, 3 or 4 values");
    }
    lua_pop(L, len);
	return 0;
}

int Lapp_Shader::set_uniform(lua_State *L)
{
    //) def: name : string, value : number | table
    const char* name = luaL_checkstring(L, 1);

    // single value argument; integer, float or table
    if (lua_gettop(L) == 2)
    {
        if (_is_integer(L, 2))
        {
			set_uniformi(L);
        }
        else if (lua_isnumber(L, 2))
        {
			set_uniformf(L);
        }
        else if (lua_istable(L, 2))
        {
			set_uniformv(L);
        }
        else
        {
            LAPP_ERROR(L, "second argument expected to be integer, float or table.");
        }
    }

    // set_uniform(name, x, y, z, w) ?

    return 0;
}

int Lapp_Shader::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
