#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/PrimitiveBuffer.h>

#include <Lapp/Lapp_Quad.h>
#include <Lapp/Lapp_QuadBuffer.h>

using namespace Pxf;

const char Lapp_QuadBuffer::classNamespace[] = "lapp";
const char Lapp_QuadBuffer::className[] = "quadbuffer";

Lunar<Lapp_QuadBuffer>::RegType Lapp_QuadBuffer::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_QuadBuffer, update),
    LUNAR_DECLARE_METHOD(Lapp_QuadBuffer, draw),
    LUNAR_DECLARE_METHOD(Lapp_QuadBuffer, valid),
    {0,0}
};


Lapp_QuadBuffer::Lapp_QuadBuffer(lua_State *L, void* data)
    : buffer(nullptr)
{
    is_valid = false;
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    buffer = device->CreateQuadBuffer(luaL_checkinteger(L, 1));
}
Lapp_QuadBuffer::~Lapp_QuadBuffer()
{
}

int Lapp_QuadBuffer::update(lua_State *L)
{
    //) def: offset : integer, quads : table
    unsigned int offset = luaL_checkinteger(L, 1);

    // The default lua convention is 1 based arrays, let's stick with that.
    if (offset > 0)
        offset--;

    if (!lua_istable(L, 2))
    {
        LAPP_ERROR(L, "Expected second argument to be a table.");
        return 0;
    }

	std::vector<Pxf::Graphics::Quad> quads;
	Lapp_Quad* quad;

    lua_pushnil(L); // first key
    while(lua_next(L, -2))
    {
        Pxf::Graphics::Quad q;

        float rotation = 0.f;
        float depth = 0.f;

        // -2: index
        // -1: value
        int idx = lua_tointeger(L, -2);

		
		if (quad = Luna<Lapp_Quad>::lightcheck(L, -1))
		{
			q.SetTopLeft(quad->rect, quad->rotation);
			q.SetDepth(quad->depth);
			q.SetColor(quad->color);
			q.SetTextureCoordinates(quad->texcoords[0], quad->texcoords[1]);
		}
		else // set-by-table
		{
			if (!lua_istable(L, -1))
				LAPP_ERROR(L, "Expected index %d to be a table.", idx);

			{   // Get rotation
				lua_pushstring(L, "rot");
				lua_gettable(L, -2);
				if (!lua_isnil(L, -1))
				{
					int table = lua_gettop(L);

					if (!lua_isnumber(L, -1))
						LAPP_ERROR(L, "table idx %d: Expected rot to be a number (rotation)", idx);
					rotation = lua_tonumber(L, -1);
				}
				lua_pop(L, 1);
			}

			{   // Get depth
				lua_pushstring(L, "depth");
				lua_gettable(L, -2);
				if (!lua_isnil(L, -1))
				{
					int table = lua_gettop(L);

					if (!lua_isnumber(L, -1))
						LAPP_ERROR(L, "table idx %d: Expected depth to be a number (rotation)", idx);
					depth = lua_tonumber(L, -1);
				}
				lua_pop(L, 1);
			}

			{   // Get position
				lua_pushstring(L, "rect");
				lua_gettable(L, -2);
				int table = lua_gettop(L);

				if (!lua_istable(L, -1) || lua_rawlen(L, -1) != 4)
					LAPP_ERROR(L, "table idx %d: Expected rect to be a table of length 4 (x, y, w, h)", idx);

				Pxf::Graphics::Rect pos;
				lua_rawgeti(L, table, 1); pos.x = lua_tonumber(L, -1);
				lua_rawgeti(L, table, 2); pos.y = lua_tonumber(L, -1);
				lua_rawgeti(L, table, 3); pos.w = lua_tonumber(L, -1);
				lua_rawgeti(L, table, 4); pos.h = lua_tonumber(L, -1);
				q.SetTopLeft(pos, rotation);
				q.SetDepth(depth);
				lua_pop(L, 5);
			}

			{   // Get color
				lua_pushstring(L, "color");
				lua_gettable(L, -2);
				if (!lua_isnil(L, -1))
				{
					int table = lua_gettop(L);
					if (!lua_istable(L, -1) || lua_rawlen(L, -1) != 4)
						LAPP_ERROR(L, "table idx %d: Expected color to be a table of length 4 (rgba, values 0..1)", idx);
					Color color;
					lua_rawgeti(L, table, 1); color.r = lua_tonumber(L, -1);
					lua_rawgeti(L, table, 2); color.g = lua_tonumber(L, -1);
					lua_rawgeti(L, table, 3); color.b = lua_tonumber(L, -1);
					lua_rawgeti(L, table, 4); color.a = lua_tonumber(L, -1);
					q.SetColor(color);
					lua_pop(L, 4);
				}
				lua_pop(L, 1);
			}

			{   // Get texture coords
				lua_pushstring(L, "tex");
				lua_gettable(L, -2);
				if (!lua_isnil(L, -1))
				{
					int table = lua_gettop(L);
					if (!lua_istable(L, -1) || lua_rawlen(L, -1) != 4)
						LAPP_ERROR(L, "table idx %d: Expected tex to be a table of length 4 (top left uv, bottom right uv)", idx);
					glm::vec2 tl, br;
					lua_rawgeti(L, table, 1); tl.x = lua_tonumber(L, -1);
					lua_rawgeti(L, table, 2); tl.y = lua_tonumber(L, -1);
					lua_rawgeti(L, table, 3); br.x = lua_tonumber(L, -1);
					lua_rawgeti(L, table, 4); br.y = lua_tonumber(L, -1);
					q.SetTextureCoordinates(tl, br);
					lua_pop(L, 4);
				}
				lua_pop(L, 1);
			}
		}
        quads.push_back(q);
        lua_pop(L, 1); // pop value
    }
    buffer->Update(offset, quads);
    /*
    Pxf::Graphics::Quad* buff = buffer->MapData(Pxf::Graphics::VB_ACCESS_READ_WRITE);
    for(int i = 0; i < 4; i++)
    {
        Pxf::Graphics::Quad q = buff[i];

    }
    buffer->UnmapData();
    */
    return 0;
}

int Lapp_QuadBuffer::draw(lua_State *L)
{
	//) def: offset = 1 : integer, count = -1 : integer
    int offset = 0, count = -1;
    if (lua_gettop(L) == 1)
        offset = luaL_checkinteger(L, 1);
    else if (lua_gettop(L) == 2)
    {
        offset = luaL_checkinteger(L, 1);
        count = luaL_checkinteger(L, 2);
    }
    buffer->Draw(offset, count);
    return 0;
}

int Lapp_QuadBuffer::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
