#include <Lapp/Lapp_App.h>
#include <Lapp/Lapp.h>
#include <Lapp/Lapp_Configuration.h>

#include <Pxf/Kernel.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Util/Application.h>

#include <sstat.h>

using namespace Pxf;

//) option: singleton

//) Access to the running application.
//)
//) By incuding the wrapper 'app', all methods can be accessed as regular functions, e.g. app.get_fps()

const char Lapp_App::classNamespace[] = "lapp";
const char Lapp_App::className[] = "app";

Lunar<Lapp_App>::RegType Lapp_App::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_App, get_fps),
	LUNAR_DECLARE_METHOD(Lapp_App, get_uptime),
	LUNAR_DECLARE_METHOD(Lapp_App, get_exe_argstr),
	LUNAR_DECLARE_METHOD(Lapp_App, get_exe_args),
	LUNAR_DECLARE_METHOD(Lapp_App, get_exe_name),
	LUNAR_DECLARE_METHOD(Lapp_App, get_exe_path),
	LUNAR_DECLARE_METHOD(Lapp_App, get_exe_type),
	LUNAR_DECLARE_METHOD(Lapp_App, get_app_root),
	LUNAR_DECLARE_METHOD(Lapp_App, get_platform),
	LUNAR_DECLARE_METHOD(Lapp_App, get_platform_family),
	LUNAR_DECLARE_METHOD(Lapp_App, is_debug),
	LUNAR_DECLARE_METHOD(Lapp_App, get_configuration),
	LUNAR_DECLARE_METHOD(Lapp_App, get_cwd),
	LUNAR_DECLARE_METHOD(Lapp_App, set_cwd),
	LUNAR_DECLARE_METHOD(Lapp_App, set_title),
	LUNAR_DECLARE_METHOD(Lapp_App, listdir),
	LUNAR_DECLARE_METHOD(Lapp_App, quit),
	{0,0}
};

Lapp_App::Lapp_App(lua_State *L, void* data)
{
	kernel = Kernel::GetInstance();
	app = Application::GetInstance();

	app->cfg_.SetStrictMerge(false);
	app->cfg_.SetTypechecking(false);
}
Lapp_App::~Lapp_App()
{}

int Lapp_App::get_fps(lua_State* L)
{
	//) Returns FPS count
	lua_pushnumber(L, app->GetFPS());
	return 1;
}

int Lapp_App::get_uptime(lua_State *L)
{
	//) Returns time since application launch
	lua_pushnumber(L, app->running_time_);
	return 1;
}

int Lapp_App::get_exe_argstr(lua_State* L)
{
	//) Return command line
	lua_pushstring(L, kernel->GetCommandLineArgumentString().c_str());
	return 1;
}

int Lapp_App::get_exe_args(lua_State* L)
{
	//) Return command line arguments split by spaces
	int i = 1;
	lua_newtable(L);
	int tbl = lua_gettop(L);
	for(auto& p: kernel->GetCommandLineArguments())
	{
		lua_pushstring(L, p.c_str());
		lua_rawseti(L, tbl, i++);
	}
	return 1;
}

int Lapp_App::get_exe_name(lua_State* L)
{
	//) Get name of executable, e.g "Kjell_sr.exe"
	const std::string& path = kernel->GetPathOfExecutable();
	std::string name = Path::GetFileName(path);
	lua_pushstring(L, name.c_str());
	return 1;
}

int Lapp_App::get_exe_path(lua_State* L)
{
	//) Get path of executable, e.g "./" or "/home/user/bin"
	const std::string& path = kernel->GetPathOfExecutable();
	std::string filepath = Path::GetPath(path);
	if (filepath == "")
		filepath = "./";
	lua_pushstring(L, filepath.c_str());
	return 1;
}

int Lapp_App::get_exe_type(lua_State* L)
{
	//) Return application type. "native" for a standalone application, "vst" for a hosted plugin.
	lua_pushstring(L, kernel->GetExeType() == Kernel::EXE_NATIVE ? "native" : "vst");
	return 1;
}

int Lapp_App::get_app_root(lua_State* L)
{
	//) Get path to application script directory, e.g "data/app"
	lua_pushstring(L, Lapp::GetInstance()->GetApplicationRoot().c_str());
	return 1;
}

int Lapp_App::get_platform(lua_State* L)
{
	//) Get the platform targeted with the current binary, e.g. "win32"
	lua_pushstring(L, CONF_PLATFORM_STRING);
	return 1;
}

int Lapp_App::get_platform_family(lua_State* L)
{
	//) Get the platform family, e.g. "windows"
	lua_pushstring(L, CONF_FAMILY_STRING);
	return 1;
}

int Lapp_App::is_debug(lua_State* L)
{
	//) Check if this is a debug build
	bool debug = false;
#ifdef CONF_DEBUG	
	debug = true;
#endif
	lua_pushboolean(L, debug);
	return 1;
}

int Lapp_App::get_configuration(lua_State* L)
{
	//) Get application/system configuration.
	Lapp_Configuration* lcfg = new Lapp_Configuration(L, 0);
	lcfg->_set_conf(&app->cfg_);
	Lunar<Lapp_Configuration>::push(L, lcfg);
	return 1;
}


int Lapp_App::get_cwd(lua_State* L)
{
	//) Get the current working directory.
	char path[256] = {0};
	sstat_getcwd(path, 255);
	lua_pushstring(L, path);
	return 1;
}

int Lapp_App::set_cwd(lua_State* L)
{
	//) Set the application working directory.
	const char* path = luaL_checkstring(L, 1); //) New application working directory
	if (Path::Exist(path))
	{
		sstat_setcwd(path);
		lua_pushboolean(L, true);
	}
	else
		lua_pushboolean(L, false);
	return 1;
}

int Lapp_App::set_title(lua_State* L)
{
	//) Set title of the active application window.
	const char* title = luaL_checkstring(L, 1); //) New window title.
	app->gfx_.GetActiveWindow()->SetTitle(title);
	return 0;
}

static void buildthing(lua_State* L, std::vector<Path::FileEntry> entries, int tableid)
{
	int i = 1;
	for(auto& e: entries)
	{
		if (e.type == Path::FileEntry::FILE)
		{
			lua_pushstring(L, e.name.c_str());
			lua_rawseti(L, tableid, i++);
		}
		else if (e.type == Path::FileEntry::DIRECTORY)
		{
			if (e.name == "." || e.name == "..")
				continue;
			lua_createtable(L, e.children.size(), 0);
			int id = lua_gettop(L);
			buildthing(L, e.children, id);
			lua_setfield(L, tableid, e.name.c_str());
			//lua_rawseti(L, tableid, i++);

		}
	}
}

int Lapp_App::listdir(lua_State* L)
{
	//) def: path : string, levels = 1 : integer
	//) Get directory content of `path.` Scan `depth` number of sub directories.
	//) The default value of depth is one - only seach the top level directory.
	const char* path = "";
	int levels = 1;
	if (lua_gettop(L) == 1)
	{
		path = luaL_checkstring(L, 1);
	}
	else if (lua_gettop(L) == 2)
	{
		path = luaL_checkstring(L, 1);
		levels = luaL_checkinteger(L, 2);
	}
	else
	{
		LAPP_ERROR(L, "argument error (path [, levels = 1])");
	}
	std::string newp = path;
	if (!Path::Exist(newp))
		kernel->GetResourcePath(path, newp);
	std::vector<Path::FileEntry> entries = Path::ListDirectory(newp, levels);

	lua_createtable(L, entries.size() ,0);
	int table = lua_gettop(L);

	buildthing(L, entries, table);

	return 1;
}

int Lapp_App::quit(lua_State* L)
{
	//) Quit application
	app->Quit();
	return 0;
}
