#include <Lapp/Lapp_Color.h>

using namespace Pxf;

const char Lapp_Color::classNamespace[] = "lapp";
const char Lapp_Color::className[] = "color";

//) Provides color management, with support for custom color ranges e.g 0..1, or 0..255.
//) Can create RGB colors from HSV and HSL.

Lunar<Lapp_Color>::RegType Lapp_Color::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Color, hex),
	LUNAR_DECLARE_METHOD(Lapp_Color, set_range),
	LUNAR_DECLARE_METHOD(Lapp_Color, set_int8),
	LUNAR_DECLARE_METHOD(Lapp_Color, set_float),
	LUNAR_DECLARE_METHOD(Lapp_Color, set_hsv),
	LUNAR_DECLARE_METHOD(Lapp_Color, set_hsl),
	LUNAR_DECLARE_METHOD(Lapp_Color, set_hex),
	LUNAR_DECLARE_METHOD(Lapp_Color, rgb),
	{0,0}
};

Lapp_Color::Lapp_Color(lua_State *L, void* data)
{
	set_int8(L);
}

Lapp_Color::~Lapp_Color()
{
}

int Lapp_Color::set_range(lua_State *L)
{
	//) Set / transform color range.
	float min = luaL_checknumber(L, 1); //) Lowest value (almost always zero)
	float max = luaL_checknumber(L, 2); //) Highest value (almost always 1 or 255)
	color.set_range(min, max);
	return 0;
}

int Lapp_Color::set_int8(lua_State *L)
{
	//) Set RGB color. This method is used when constructing the object.
	//) def: r = 0 : integer, g = 0 : integer, b = 0 : integer
	//) r: Red (0..255)
	//) g: Green (0..255)
	//) b: Blue (0..255)
	if (lua_gettop(L) == 0)
	{
		color.set_int8(0, 0, 0);
	}
	else if (lua_gettop(L) == 3)
	{
		color.set_int8(luaL_checknumber(L, 1), 
				        luaL_checknumber(L, 2),
				        luaL_checknumber(L, 3));
	}
	else
	{
		LAPP_ERROR(L, "argument error");
	}
	return 0;
}

int Lapp_Color::set_float(lua_State *L)
{
	//) def: r = 0 : number, g = 0 : number, b = 0 : number
	//) r: Red (0..1)
	//) g: Green (0..1)
	//) b: Blue (0..1)
	if (lua_gettop(L) == 0)
	{
		color.set_float(0.f, 0.f, 0.f);
	}
	else if (lua_gettop(L) == 3)
	{
		color.set_float(luaL_checknumber(L, 1), 
				        luaL_checknumber(L, 2),
				        luaL_checknumber(L, 3));
	}
	else
	{
		LAPP_ERROR(L, "argument error");
	}
	return 0;
}

int Lapp_Color::set_hsv(lua_State *L)
{
	//) See %link http://en.wikipedia.org/wiki/HSL_and_HSV
	//) def: h = 0 : number, s = 0 : number, v = 0 : number
	//) h: Hue (0..1)
	//) s: Saturation (0..1)
	//) v: Value (0..1)

	if (lua_gettop(L) == 3)
	{
		color.set_hsv(luaL_checknumber(L, 1), 
				        luaL_checknumber(L, 2),
				        luaL_checknumber(L, 3));
	}
	else
	{
		LAPP_ERROR(L, "argument error");
	}
	return 0;
}

int Lapp_Color::set_hsl(lua_State *L)
{
	//) def: h = 0 : number, s = 0 : number, l = 0 : number
	//) h: Hue (0..1)
	//) s: Saturation (0..1)
	//) l: Lightness (0..1)

	//) See %link http://en.wikipedia.org/wiki/HSL_and_HSV

	if (lua_gettop(L) == 3)
	{
		color.set_hsl(luaL_checknumber(L, 1), 
				        luaL_checknumber(L, 2),
				        luaL_checknumber(L, 3));
	}
	else
	{
		LAPP_ERROR(L, "argument error");
	}
	return 0;
}

int Lapp_Color::set_hex(lua_State *L)
{
	const char* hexs = luaL_checkstring(L, 1); //) Hex representation, e.g #fff or #c0ffee
	color.set_hex(hexs);
	return 0;
}

int Lapp_Color::rgb(lua_State* L)
{
	// Return r, g and b.
	lua_pushnumber(L, color.r);
	lua_pushnumber(L, color.g);
	lua_pushnumber(L, color.b);
	return 3;
}

int Lapp_Color::hex(lua_State *L)
{
	//) Get hex representation
	lua_pushstring(L, color.as_hex().c_str());
	return 1;
}