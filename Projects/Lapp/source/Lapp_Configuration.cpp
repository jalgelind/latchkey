#include <Lapp/Lapp_Configuration.h>
#include <Pxf/Util/Configuration.h>
#include <Pxf/Kernel.h>

using namespace Pxf;

const char Lapp_Configuration::classNamespace[] = "lapp";
const char Lapp_Configuration::className[] = "configuration";

Lunar<Lapp_Configuration>::RegType Lapp_Configuration::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Configuration, get_float),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, get_string),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, get_bool),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, set_float),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, set_string),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, set_bool),
	//LUNAR_DECLARE_METHOD(Lapp_Configuration, set),
	//LUNAR_DECLARE_METHOD(Lapp_Configuration, get),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, load),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, save_as),
	LUNAR_DECLARE_METHOD(Lapp_Configuration, valid),
	{0,0}
};

Lapp_Configuration::Lapp_Configuration(lua_State *L, void* data)
	: kernel_(0)
	, conf_(0)
{
	kernel_ = Pxf::Kernel::GetInstance();
}

Lapp_Configuration::~Lapp_Configuration()
{
}

int Lapp_Configuration::get_float(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	lua_pushnumber(L, conf_->GetFloat(str, 0.f));
	return 1;
}

int Lapp_Configuration::get_string(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	lua_pushstring(L, conf_->GetString(str, "").c_str());
	return 1;
}

int Lapp_Configuration::get_bool(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	lua_pushboolean(L, conf_->GetBool(str, false));
	return 1;
}

int Lapp_Configuration::set_float(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	float value = luaL_checknumber(L, 2);
	conf_->SetFloat(str, value);
	return 0;
}

int Lapp_Configuration::set_string(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	const char* value = luaL_checkstring(L, 2);
	conf_->SetString(str, value);
	return 0;
}

int Lapp_Configuration::set_bool(lua_State* L)
{
	const char* str = luaL_checkstring(L, 1);
	bool v = false;
	if (lua_isboolean(L, 2))
		v = lua_toboolean(L, 2);
	conf_->SetBool(str, v);
	return 0;
}

int Lapp_Configuration::set(lua_State *L)
{
    switch(lua_type(L, 1))
    {
    case LUA_TNUMBER: set_float(L); break;
    case LUA_TBOOLEAN: set_bool(L); break;
    case LUA_TSTRING: set_string(L); break;
    default:
        /* LUA_TNIL, LUA_TTABLE, LUA_TFUNCTION, LUA_TUSERDATA, LUA_TTHREAD, LUA_TLIGHTUSERDATA */
        LAPP_ERROR(L, "Unsupported value-type.");
    }
	return 0;
}

int Lapp_Configuration::get(lua_State *L)
{
    const char* key = luaL_checkstring(L, 1);
    auto value = conf_->Get(key);
	switch(value.type())
    {
    case Json::nullValue: lua_pushnil(L); break;
    case Json::intValue: lua_pushinteger(L, value.asInt()); break;
    case Json::uintValue: lua_pushunsigned(L, value.asUInt()); break;
    case Json::realValue: lua_pushnumber(L, value.asDouble()); break;
    case Json::stringValue: lua_pushstring(L, value.asCString()); break;
    case Json::booleanValue: lua_pushboolean(L, value.asBool()); break;
    default:
        /* objectValue, arrayValue */
        LAPP_ERROR(L, "Unsupported value type."); break;
    }
    lua_pushnil(L);
	return 1;
}

int Lapp_Configuration::load(lua_State *L)
{
    if (!conf_) return 0;
    const char* filename = luaL_checkstring(L, 1);
    bool replace_existing = true;
    if (conf_->Load(filename, replace_existing))
        lua_pushboolean(L, true);
    else
        lua_pushboolean(L, false);
	return 1;
}

int Lapp_Configuration::save_as(lua_State *L)
{
	if (!conf_) return 0;
    const char* filename = luaL_checkstring(L, 1);
    conf_->Save(filename);
	return 0;
}

int Lapp_Configuration::valid(lua_State *L)
{
	lua_pushboolean(L, conf_ != 0);
	return 1;
}