#include <Lapp/Lapp_Quad.h>
#include <Lapp/Math_Vec2.h>
#include <Lapp/Lapp_Color.h>

using namespace Pxf;

const char Lapp_Quad::className[] = "quad";

const Luna<Lapp_Quad>::PropertyType Lapp_Quad::properties[] = {
	LUNA_DECLARE_PROPERTY(Lapp_Quad, x),
	LUNA_DECLARE_PROPERTY(Lapp_Quad, y),
	LUNA_DECLARE_PROPERTY(Lapp_Quad, w),
	LUNA_DECLARE_PROPERTY(Lapp_Quad, h),
	LUNA_DECLARE_PROPERTY(Lapp_Quad, depth),
	LUNA_DECLARE_PROPERTY(Lapp_Quad, rotation),
	{0,0}
};

const Luna<Lapp_Quad>::FunctionType Lapp_Quad::methods[] = {
	LUNA_DECLARE_METHOD(Lapp_Quad, set_pos),
	LUNA_DECLARE_METHOD(Lapp_Quad, set_size),
	LUNA_DECLARE_METHOD(Lapp_Quad, set_rect),
	LUNA_DECLARE_METHOD(Lapp_Quad, set_color),
	LUNA_DECLARE_METHOD(Lapp_Quad, set_texcoords),
	LUNA_DECLARE_METHOD(Lapp_Quad, set_rotation),
	LUNA_DECLARE_METHOD(Lapp_Quad, set_depth),
//	LUNA_DECLARE_METHOD(Lapp_Quad, set),
	LUNA_DECLARE_METHOD(Lapp_Quad, __add),
	LUNA_DECLARE_METHOD(Lapp_Quad, __mul),
	LUNA_DECLARE_METHOD(Lapp_Quad, __div),
	LUNA_DECLARE_METHOD(Lapp_Quad, __eq),
	{0,0}
};

// Properties
int Lapp_Quad::__set_x(lua_State *L)
{
	rect.x = luaL_checknumber(L, 1);
	return 0;
}

int Lapp_Quad::__get_x(lua_State *L)
{
	lua_pushnumber(L, rect.x);
	return 1;
}

int Lapp_Quad::__set_y(lua_State *L)
{
	rect.y = luaL_checknumber(L, 1);
	return 0;
}
int Lapp_Quad::__get_y(lua_State *L)
{
	lua_pushnumber(L, rect.y);
	return 1;
}
int Lapp_Quad::__set_w(lua_State *L)
{
	rect.w = luaL_checknumber(L, 1);
	return 0;
}
int Lapp_Quad::__get_w(lua_State *L)
{
	lua_pushnumber(L, rect.w);
	return 1;
}
int Lapp_Quad::__set_h(lua_State *L)
{
	rect.h = luaL_checknumber(L, 1);
	return 0;
}

int Lapp_Quad::__get_h(lua_State *L)
{
	lua_pushnumber(L, rect.h);
	return 1;
}
int Lapp_Quad::__set_rotation(lua_State *L)
{
	rotation = luaL_checknumber(L, 1);
	return 0;
}

int Lapp_Quad::__get_rotation(lua_State *L)
{
	lua_pushnumber(L, rotation);
	return 1;
}
int Lapp_Quad::__set_depth(lua_State *L)
{
	depth = luaL_checknumber(L, 1);
	return 0;
}

int Lapp_Quad::__get_depth(lua_State *L)
{
	lua_pushnumber(L, depth);
	return 1;
}

// methods
int Lapp_Quad::set_pos(lua_State *L)
{
	rect.x = luaL_checknumber(L, 1);
	rect.y = luaL_checknumber(L, 2);
	return 0;
}

int Lapp_Quad::set_size(lua_State *L)
{
	rect.w = luaL_checknumber(L, 1);
	rect.h = luaL_checknumber(L, 2);
	return 0;
}

int Lapp_Quad::set_rect(lua_State *L)
{
	rect.x = luaL_checknumber(L, 1);
	rect.y = luaL_checknumber(L, 2);
	rect.w = luaL_checknumber(L, 3);
	rect.h = luaL_checknumber(L, 4);
	return 0;
}

int Lapp_Quad::set_color(lua_State *L)
{
    //) def: color : lapp.color()
    //) def: r : number, g : number, b : number, a = 1 : number
	Lapp_Color* c;
	if (c = Luna<Lapp_Color>::lightcheck(L, 1))
	{
		float a = 1.f;
		if (lua_gettop(L) > 1)
			a = luaL_checknumber(L, 2);
		color.r = c->color.r;
		color.g = c->color.g;
		color.b = c->color.b;
		color.a = a;
	}
	else
	{
		float a = 1.f;
		color.r = luaL_checknumber(L, 1);
		color.g = luaL_checknumber(L, 2);
		color.b = luaL_checknumber(L, 3);
		if (lua_gettop(L) > 4)
			a = luaL_checknumber(L, 2);
		color.a = a;
	}
	return 0;
}

int Lapp_Quad::set_texcoords(lua_State *L)
{
	//) def: tl_x : number, tl_y : number, br_x : number, br_y : number
	//) Set texture coordinates, specify top left and bottom right coordinates.
	 // TODO: check vec2, vec2
	texcoords[0].x = luaL_checknumber(L, 1);
	texcoords[0].y = luaL_checknumber(L, 2);
	texcoords[1].x = luaL_checknumber(L, 3);
	texcoords[1].y = luaL_checknumber(L, 4);
	return 0;
}

int Lapp_Quad::set_rotation(lua_State *L)
{
	rotation = luaL_checknumber(L, 1);
	return 0;
}

int Lapp_Quad::set_depth(lua_State *L)
{
	depth = luaL_checknumber(L, 1);
	return 0;
}

/*
int Lapp_Quad::set(lua_State *L)
{
	// Lapp_Vertex
	// Lapp_Color
	// Lapp_Vec2 <- texture
	return 0;
}
*/
// operators
int Lapp_Quad::__add(lua_State *L)
{
	//) Resize rectangle. TODO: vec2
	Lapp_Quad* nq = new Lapp_Quad(L);
	nq->color = color;
	nq->depth = depth;
	nq->rotation = rotation;
	nq->rect = rect;
	float n = luaL_checknumber(L, 1);
	nq->rect.w += n;
	nq->rect.h += n;
	Luna<Lapp_Quad>::push(L, nq);
	return 1;
}

int Lapp_Quad::__mul(lua_State *L)
{
	//) Resize rectangle. TODO: vec2
	Lapp_Quad* nq = new Lapp_Quad(L);
	nq->color = color;
	nq->depth = depth;
	nq->rotation = rotation;
	nq->rect = rect;
	float n = luaL_checknumber(L, 1);
	nq->rect.w *= n;
	nq->rect.h *= n;
	Luna<Lapp_Quad>::push(L, nq);
	return 1;
}

int Lapp_Quad::__div(lua_State *L)
{
	//) Resize rectangle. TODO: vec2
	Lapp_Quad* nq = new Lapp_Quad(L);
	nq->color = color;
	nq->depth = depth;
	nq->rotation = rotation;
	nq->rect = rect;
	float n = luaL_checknumber(L, 1);
	nq->rect.w /= n;
	nq->rect.h /= n;
	Luna<Lapp_Quad>::push(L, nq);
	return 1;
}

int Lapp_Quad::__eq(lua_State *L)
{
	//) Check rectangle...
	Lapp_Quad* rhs = Luna<Lapp_Quad>::check(L, 1);
	lua_pushboolean(L, rect == rhs->rect);
	return 1;
}