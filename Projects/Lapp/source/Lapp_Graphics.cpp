#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/Window.h>
#include <Pxf/Graphics/FrameBuffer.h>
#include <Pxf/Graphics/Texture.h>
#include <Pxf/Graphics/Shader.h>
#include <Pxf/Resource/Image.h>

#include <Lapp/Lapp_Font.h>
#include <Lapp/Lapp_Texture.h>
#include <Lapp/Lapp_Shader.h>
#include <Lapp/Lapp_Framebuffer.h>
#include <Lapp/Lapp_Window.h>
#include <Lapp/Lapp_Graphics.h>
#include <Lapp/Math_Mat4.h>

#include <string>

using namespace Pxf;

//) option: singleton

//) Provides access to the graphics backend.
//)
//) By incuding the wrapper 'gfx', all methods can be accessed as regular functions, e.g. gfx.bind(texture)

const char Lapp_Graphics::classNamespace[] = "lapp";
const char Lapp_Graphics::className[] = "graphics";

Lunar<Lapp_Graphics>::RegType Lapp_Graphics::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Graphics, clear),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, getopt_float),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, getopt_string),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, pop_model_matrix),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, push_model_matrix),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, pop_projection_matrix),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, push_projection_matrix),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, bind),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, unbind),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, bind_texture),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, unbind_texture),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, dump_textures),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, bind_shader),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, unbind_shader),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, bind_framebuffer),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, unbind_framebuffer),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, save_framebuffer_as),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, translate),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, scale),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_blending),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_clip),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_cullface),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, get_active_window),
	LUNAR_DECLARE_METHOD(Lapp_Graphics, get_window),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_window_size),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_viewport),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_ortho_projection),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_depthtest),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_linewidth),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, set_pointsize),
    LUNAR_DECLARE_METHOD(Lapp_Graphics, valid),
    {0,0}
};


Lapp_Graphics::Lapp_Graphics(lua_State *L, void* data)
{
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    is_valid = !device->IsNull();
}
Lapp_Graphics::~Lapp_Graphics()
{

}

int Lapp_Graphics::getopt_string(lua_State* L)
{
	//) Get information about the gfx backend.
    std::string opt = luaL_checkstring(L, 1);
    if (opt == "vendor")
        lua_pushstring(L, device->GetVendor());
    else if (opt == "version")
        lua_pushstring(L, device->GetVersion());
    else if (opt == "renderer")
        lua_pushstring(L, device->GetRenderer());
    else if (opt == "shaderversion")
        lua_pushstring(L, device->GetShaderLangVersion());
    else
    {
        LAPP_ERROR(L, "argument error");
        return 0;
    }
    return 1;
}

int Lapp_Graphics::getopt_float(lua_State* L)
{
	//) Get information about the gfx backend.
    std::string opt = luaL_checkstring(L, 1);
    if (opt == "memsize")
        lua_pushnumber(L, device->GetMemorySize());
    else if (opt == "memusage")
        lua_pushnumber(L, device->GetMemoryUsage());
    else if (opt == "memevict")
        lua_pushnumber(L, device->GetEvictedMemory());
    else if (opt == "evictcount")
        lua_pushnumber(L, device->GetEvictionCount());
    else if (opt == "texmem")
        lua_pushnumber(L, device->GetTextureMemoryUsage());
    else if (opt == "vbomem")
        lua_pushnumber(L, device->GetVBOMemoryUsage());
    else
    {
        LAPP_ERROR(L, "argument error");
        return 0;
    }
    return 1;
}

int Lapp_Graphics::get_active_window(lua_State* L)
{
	//) Returns table containing width and height of active window.
	//) def: -> lapp.window()
    auto win = device->GetActiveWindow();
	if (win == nullptr)
		return 0;
	lua_pushlightuserdata(L, (void*)win.get());
    Lunar<Lapp_Window>::push(L, new Lapp_Window(L, 0));
	return 1;
}

int Lapp_Graphics::get_window(lua_State* L)
{
	//) Returns table containing width and height of active window.
	//) def: -> lapp.window()
	int id = luaL_checkinteger(L, 1);
    auto win = device->GetWindow(id);
	if (win == nullptr)
		return 0;

	lua_pushlightuserdata(L, (void*)win.get());
    Lunar<Lapp_Window>::push(L, new Lapp_Window(L, 0));
    return 1;
}

int Lapp_Graphics::set_window_size(lua_State* L)
{
	//) Set size of active window.
    float w = luaL_checknumber(L, 1);
    float h = luaL_checknumber(L, 2);
    auto win = device->GetActiveWindow();
    win->SetWindowSize(w, h);
    return 0;
}

int Lapp_Graphics::set_viewport(lua_State *L)
{
	//) Specify size of window viewport.
	//)
	//) x, y, w, h: Viewport dimensions
    float x = luaL_checknumber(L, 1);
    float y = luaL_checknumber(L, 2);
    float w = luaL_checknumber(L, 3);
    float h = luaL_checknumber(L, 4);
    Graphics::Rect viewport(x, y, w, h);
    device->SetViewport(viewport);
    return 0;
}

int Lapp_Graphics::set_ortho_projection(lua_State *L)
{
	//) Update the projection matrix with orthogonal projection. Use %r math.ortho() to create the matrix.
    //) def: matrix : lapp.mat4()
    //) def: left : number, right : number, bottom : number, znear : number, zfar : number
    glm::mat4 mtx;
    Math_Mat4* mtxw;
    if (mtxw = Luna<Math_Mat4>::lightcheck(L, 1))
    {
        mtx = mtxw->mat_;
    }
    else
    {
        float left = luaL_checknumber(L, 1);
        float right = luaL_checknumber(L, 2);
        float bottom = luaL_checknumber(L, 3);
        float top = luaL_checknumber(L, 4);
        float znear = luaL_checknumber(L, 5);
        float zfar = luaL_checknumber(L, 6);
        mtx = glm::ortho(left, right, bottom, top, znear, zfar);
    }
    device->SetProjectionMatrix(mtx);
    return 0;
}

int Lapp_Graphics::set_depthtest(lua_State* L)
{
	//) Enable or disable depth testing.

	//) opt: The binary operator specifying the operation used on the incoming and stored fragment depth.
	//) opt:
	//) opt: Valid values are "off", "never", "always", "<=", "<", "==", ">=", ">" and "!=".
	
	//)@details
	//)	It works like this:
	//)@code
	//)	if (in > stored) then
	//)		pass 
	//)	else 
	//)		discard
	//)@endcode

	std::string opt = luaL_checkstring(L, 1); 

    if (opt == "off")
        device->SetDepthTest(false);
    else
        device->SetDepthTest(true);

    // Passes if the incoming depth value is {opt} than the stored depth value.
    if (opt == "always")
        device->SetDepthFunction(Graphics::DF_ALWAYS);
    else if (opt == "never")
        device->SetDepthFunction(Graphics::DF_NEVER);
    else if (opt == "<")
        device->SetDepthFunction(Graphics::DF_LESS);
    else if (opt == "<=")
        device->SetDepthFunction(Graphics::DF_LEQUAL);
    else if (opt == "==" || opt == "=")
        device->SetDepthFunction(Graphics::DF_EQUAL);
    else if (opt == ">")
        device->SetDepthFunction(Graphics::DF_GREATER);
    else if (opt == ">=")
        device->SetDepthFunction(Graphics::DF_GEQUAL);
    else if (opt == "!=")
        device->SetDepthFunction(Graphics::DF_NOTEQUAL);
    else
    {
        LAPP_ERROR(L, "Argument error, expected 'off', always, never, <, <=, ==, > >=, !=");
        return 0;
    }
    return 0;
}

int Lapp_Graphics::set_cullface(lua_State* L)
{
	//) Set face culling
    if (lua_isboolean(L, 1))
    {
        bool v = lua_toboolean(L, 1); //) Whether face culling should be enabled or not.
        device->SetCullFace(v);
    }
    else
    {
        LAPP_ERROR(L, "argument error (expected boolean)");
        return 0;
    }
    return 0;
}

int Lapp_Graphics::set_clip(lua_State* L)
{
    //) def: str : string
    //) def: x : number, y : number, w : number, h : number
	//) x, y, w, h: Region where rendering is allowed.
    if (lua_gettop(L) == 1)
    {
        std::string str = luaL_checkstring(L, 1); //) For turning off clipping, specify "off"
        if (str == "off")
            device->SetNoClip();
        else
        {
            LAPP_ERROR(L, "single argument to be 'off'.");
        }
        return 0;
    }
    else if (lua_gettop(L) == 4)
    {
        float x = luaL_checknumber(L, 1);
        float y = luaL_checknumber(L, 2);
        float w = luaL_checknumber(L, 3);
        float h = luaL_checknumber(L, 4);
        device->SetClip(x, y, w, h);
    }
    else
    {
        LAPP_ERROR(L, "expected argument 'off' or (x,y,w,h).");
    }
    return 0;
}

int Lapp_Graphics::set_pointsize(lua_State* L)
{
	//) Specify the size of rendered points.
    float v = luaL_checknumber(L, 1); //) Pointsize in pixels
    device->SetPointSize(v);
    return 0;
}

int Lapp_Graphics::set_linewidth(lua_State* L)
{
	//) Specify the width of rendered lines.
    float v = luaL_checknumber(L, 1); //) Linewidth in pixels
    device->SetLineWidth(v);
    return 0;
}

int Lapp_Graphics::push_projection_matrix(lua_State* L)
{
	//) Push the active projection matrix to the top of a stack.
    device->PushProjectionMatrix();
    return 0;
}

int Lapp_Graphics::pop_projection_matrix(lua_State* L)
{
	//) Pop the top most projection matrix on the stack.
    device->PopProjectionMatrix();
    return 0;
}

int Lapp_Graphics::push_model_matrix(lua_State* L)
{
	//) Push the active model matrix on the top of a stack.
    device->PushModelMatrix();
    return 0;
}

int Lapp_Graphics::pop_model_matrix(lua_State* L)
{
	//) Pop the top most model matrix from the stack.
    device->PopModelMatrix();
    return 0;
}

int Lapp_Graphics::bind(lua_State* L)
{
	//) Bind a %r lapp.texture(), %r lapp.shader() or a %r lapp.framebuffer().
    //) def: object : lapp.texture() | lapp.shader() | lapp.framebuffer()
	//) object: Object to bind.
	Lapp_Framebuffer* fbo;
	Lapp_Shader* shader;
	Lapp_Texture* tex;
	if (fbo = Luna<Lapp_Framebuffer>::lightcheck(L, 1))
		bind_framebuffer(L);
	else if (shader = Luna<Lapp_Shader>::lightcheck(L, 1))
		bind_shader(L);
	else if (tex = Luna<Lapp_Texture>::lightcheck(L, 1))
		bind_texture(L);
	else
		LAPP_ERROR(L, "Unsupported type.");
	return 0;
}

int Lapp_Graphics::unbind(lua_State* L)
{
	//) Unbind object type.
    //) def: object : lapp.texture() | lapp.shader() | lapp.framebuffer()
	//) object: Object %bb type %bb to unbind.
	Lapp_Framebuffer* fbo;
	Lapp_Shader* shader;
	Lapp_Texture* tex;
	if (fbo = Luna<Lapp_Framebuffer>::lightcheck(L, 1))
		unbind_framebuffer(L);
	else if (shader = Luna<Lapp_Shader>::lightcheck(L, 1))
		unbind_shader(L);
	else if (tex = Luna<Lapp_Texture>::lightcheck(L, 1))
		unbind_texture(L);
	else
		LAPP_ERROR(L, "Unsupported type.");
	return 0;
}

int Lapp_Graphics::bind_texture(lua_State* L)
{
	//) Bind texture object to unit.
	//) def: tex : lapp.texture(), unit = 0 : integer
    Lapp_Texture* tex = Lunar<Lapp_Texture>::check(L, 1); //) Texture to bind.
	int unit = 0;
	if (lua_gettop(L) > 1)
		unit = luaL_checknumber(L, 2); //) Texture unit to bind texture to.
    device->BindTexture(tex->texture, unit);
    return 0;
}

int Lapp_Graphics::unbind_texture(lua_State* L)
{
	//) Unbind texture unit. Accepts a texture as first parameter, but it isn't used.
    //) def: texture = optional : lapp.texture(), unit = 0 : integer
	
	//) texture: Optionally specify a texture. This argument isn't currently used.
	//) texture: 
	//) texture: TODO: It might be used later for making sure the intended texture we want to unbind is currently bound.

	Lapp_Texture* tex;
	int unit = 0;
	int unit_idx = 1;
	if (tex = Luna<Lapp_Texture>::lightcheck(L, 1))
		unit_idx = 2;

	if (lua_gettop(L) > 1)
		unit = luaL_checknumber(L, unit_idx); //) Texture unit to unbind
	
	device->BindTexture(0, unit);
	return 0;
}

int Lapp_Graphics::dump_textures(lua_State* L)
{
	//) Dump all active texture to disk
	//) outdir: Directory where textures will be saved.
	const char* outdir = luaL_checkstring(L, 1);
	device->DumpTexturesToDisk(outdir);
	return 0;
}

int Lapp_Graphics::bind_shader(lua_State* L)
{
	//) Bind shader.
	Lapp_Shader* sh = Lunar<Lapp_Shader>::check(L, 1); //) Shader to bind
	device->BindShader(sh->shader);
	return 0;
}

int Lapp_Graphics::unbind_shader(lua_State* L)
{
	//) Unbind shader
	device->BindShader(0);
	return 0;
}

int Lapp_Graphics::bind_framebuffer(lua_State* L)
{
	//) Bind framebuffer
	Lapp_Framebuffer* fbo = Lunar<Lapp_Framebuffer>::check(L, 1); //) Framebuffer to bind
	device->BindFrameBuffer(fbo->fbo);
	return 0;
}

int Lapp_Graphics::unbind_framebuffer(lua_State* L)
{
	//) Unbind framebuffer
	device->BindFrameBuffer(nullptr);
	return 0;
}

int Lapp_Graphics::save_framebuffer_as(lua_State* L)
{
	//) Save framebuffer to disk
	//) filename: filename of the output image
	const char* filename = luaL_checkstring(L, 1);
	auto tex = device->CreateTextureFromFramebuffer();
	auto img = tex->ToImage();
	img->SaveAs(filename);
	return 0;
}


int Lapp_Graphics::translate(lua_State* L)
{
	//) Translate the active model view matrix.
	//) x, y, z: Relative coordinates
    glm::vec3 v;
    v.x = luaL_checknumber(L, 1);
    v.y = luaL_checknumber(L, 2);
    v.z = luaL_checknumber(L, 3);
    device->Translate(v);
    return 0;
}

int Lapp_Graphics::scale(lua_State* L)
{
	//) Scale the model view matrix.
	//) x, y, z: Relative scale
    glm::vec3 v;
    v.x = luaL_checknumber(L, 1);
    v.y = luaL_checknumber(L, 2);
    v.z = luaL_checknumber(L, 3);
    device->Scale(v);
    return 0;
}

int Lapp_Graphics::clear(lua_State *L)
{
	//) Clear the framebuffer with given color, or black as default.
    //) def: r = 0 : number, g = 0 : number, b = 0 : number, a = 1 : number
	//) r, g, b, a: Color in range 0..1
    if (!is_valid)
        return 0;
    if (lua_gettop(L) == 0)
    {
        device->Clear(0.f, 0.f, 0.f, 1.f);
    }
    else if (lua_gettop(L) == 3)
    {
        device->Clear(luaL_checknumber(L, 1),
                      luaL_checknumber(L, 2),
                      luaL_checknumber(L, 3));
    }
    else if (lua_gettop(L) == 4)
    {
        device->Clear(luaL_checknumber(L, 1),
                      luaL_checknumber(L, 2),
                      luaL_checknumber(L, 3),
                      luaL_checknumber(L, 4));
    }
    else
    {
        LAPP_ERROR(L, "expected 0, 3 or 4 arguments.");
    }
    return 0;
}

int Lapp_Graphics::set_blending(lua_State *L)
{
	//) Set blending
    if (lua_gettop(L) == 0)
    {
        device->SetBlendNormal();
        return 0;
    }
    std::string str = luaL_checkstring(L, 1); //) Blend type, valid options are  "off", "normal" and "additive".
    if (str == "none")
    {
        device->SetBlendNone();
    }
    else if (str ==  "normal")
    {
        device->SetBlendNormal();
    }
    else if (str ==  "add" || str == "additive")
    {
        device->SetBlendAdditive();
    }
    else
    {
        LAPP_ERROR(L, "argument error; expected 'none', 'normal' or 'additive'.");
    }
    return 0;
}

int Lapp_Graphics::valid(lua_State *L)
{
	//) Verify that the graphics backend is valid.
    lua_pushboolean(L, is_valid);
    return 1;
}
