#include <Lapp/Lapp_Physics.h>
#if defined(CONF_WITH_LIBRARY_LOVE2D)
#include <physics/box2d/wrap_Physics.h>
#endif

namespace Pxf
{
	void register_lapp_physics(lua_State* L)
	{
	#if defined(CONF_WITH_LIBRARY_LOVE2D)
	love::physics::box2d::luaopen_love_physics(L);
	#else
		// warn about missing physics library?
	#endif
	}
}