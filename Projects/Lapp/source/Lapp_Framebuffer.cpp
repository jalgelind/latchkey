#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/FrameBuffer.h>
#include <Pxf/Graphics/Texture.h>
#include <Lapp/Lapp_Framebuffer.h>
#include <Lapp/Lapp_Texture.h>

using namespace Pxf;

const char Lapp_Framebuffer::classNamespace[] = "lapp";
const char Lapp_Framebuffer::className[] = "framebuffer";

Lunar<Lapp_Framebuffer>::RegType Lapp_Framebuffer::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Framebuffer, valid),
    LUNAR_DECLARE_METHOD(Lapp_Framebuffer, attach),
    LUNAR_DECLARE_METHOD(Lapp_Framebuffer, detach),
    LUNAR_DECLARE_METHOD(Lapp_Framebuffer, num_color_attachments),
    LUNAR_DECLARE_METHOD(Lapp_Framebuffer, has_depth),
    LUNAR_DECLARE_METHOD(Lapp_Framebuffer, has_stencil),
	LUNAR_DECLARE_METHOD(Lapp_Framebuffer, bind),
	LUNAR_DECLARE_METHOD(Lapp_Framebuffer, unbind),
    {0,0}
};

Lapp_Framebuffer::Lapp_Framebuffer(lua_State *L, void* data)
{
    is_valid = false;
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    fbo = device->CreateFrameBuffer();
}

Lapp_Framebuffer::~Lapp_Framebuffer()
{

}

static Graphics::FrameBufferAttachmentType Translate(const std::string& str)
{
	Graphics::FrameBufferAttachmentType attachment = Graphics::FB_INVALID;
	// TODO: magic macro that generates this
	// 1st macro, create static const char* types[] { ... }
	// 2st macro, index array and offset strings with 3 (strip 'FB_').
	// BUT: defined names might change, so don't do this just yet.
	if (str == "depth")
	{
		attachment = Graphics::FB_DEPTH;
	}
	else if (str == "depthstencil")
	{
		attachment = Graphics::FB_DEPTH_STENCIL;
	}
	else if (str == "stencil")
	{
		attachment = Graphics::FB_STENCIL;
	}
	else if (str == "color0")
	{
		attachment = Graphics::FB_COLOR0;
	}
	else if (str == "color1")
	{
		attachment = Graphics::FB_COLOR1;
	}
	else if (str == "color2")
	{
		attachment = Graphics::FB_COLOR2;
	}
	else if (str == "color3")
	{
		attachment = Graphics::FB_COLOR3;
	}
	return attachment;
}

int Lapp_Framebuffer::attach(lua_State *L)
{
    // Texture* _Texture, const unsigned _Attachment, bool _GenMipmaps
    std::shared_ptr<Graphics::Texture> tex = nullptr;
    Graphics::FrameBufferAttachmentType attachment = Graphics::FB_INVALID;
    bool genmipmaps = true;
    if (lua_gettop(L) > 0)
    {
        auto texture = Lunar<Lapp_Texture>::check(L, 1);
        tex = texture->texture;
    }
    if (lua_gettop(L) > 1)
    {
        /* should be constants etc */
        std::string str = luaL_checkstring(L, 2);
        attachment = Translate(str);
        if (attachment == Graphics::FB_INVALID)
            LAPP_ERROR(L, "attachment type expected to be depth, depthstencil, stencil, color0..4");
    }
    if (lua_gettop(L) > 2)
    {
        genmipmaps = lua_toboolean(L, 3);
    }
    fbo->Attach(tex, attachment, genmipmaps);
    return 0;
}

int Lapp_Framebuffer::detach(lua_State *L)
{
	Graphics::FrameBufferAttachmentType attachment = Graphics::FB_INVALID;
	std::string str = luaL_checkstring(L, 1);
	attachment = Translate(str);
	if (attachment == Graphics::FB_INVALID)
		LAPP_ERROR(L, "attachment type expected to be depth, stencil, color0..4");
    fbo->Detach(attachment);
    return 0;
}

int Lapp_Framebuffer::num_color_attachments(lua_State *L)
{
    int v = fbo->GetNumColorAttachment();
    lua_pushinteger(L, v);
    return 1;
}

int Lapp_Framebuffer::has_depth(lua_State *L)
{
    bool v = fbo->GetUseDepthAttachment();
    lua_pushboolean(L, v);
    return 1;
}

int Lapp_Framebuffer::has_stencil(lua_State *L)
{
    bool v = fbo->GetUseStencilAttachment();
    lua_pushboolean(L, v);
    return 1;
}

int Lapp_Framebuffer::bind(lua_State *L)
{
	device->BindFrameBuffer(fbo);
	return 0;
}

int Lapp_Framebuffer::unbind(lua_State *L)
{
	device->BindFrameBuffer(nullptr);
	return 0;
}

int Lapp_Framebuffer::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
