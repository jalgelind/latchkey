#include <Lapp/Math_Vec3.h>

using namespace Pxf;

//) inherit: Math_VecT.h

const char Math_Vec3::className[] = "vec3";

const Luna<Math_Vec3>::PropertyType Math_Vec3::properties[] = {
	LUNA_DECLARE_PROPERTY(Math_Vec3, x),
	LUNA_DECLARE_PROPERTY(Math_Vec3, y),
	LUNA_DECLARE_PROPERTY(Math_Vec3, z),
	{0,0}
};

const Luna<Math_Vec3>::FunctionType Math_Vec3::methods[] = {
	LUNA_DECLARE_METHOD(Math_Vec3, rotate),
	LUNA_DECLARE_METHOD(Math_Vec3, rotatex),
	LUNA_DECLARE_METHOD(Math_Vec3, rotatey),
	LUNA_DECLARE_METHOD(Math_Vec3, rotatez),
	LUNA_DECLARE_METHOD(Math_Vec3, cross),
	LUNA_DECLARE_METHOD(Math_Vec3, distance),
	LUNA_DECLARE_METHOD(Math_Vec3, dot),
	LUNA_DECLARE_METHOD(Math_Vec3, faceforward),
	LUNA_DECLARE_METHOD(Math_Vec3, length),
	LUNA_DECLARE_METHOD(Math_Vec3, normalize),
	LUNA_DECLARE_METHOD(Math_Vec3, reflect),
	LUNA_DECLARE_METHOD(Math_Vec3, refract),
	LUNA_DECLARE_METHOD(Math_Vec3, __add),
	LUNA_DECLARE_METHOD(Math_Vec3, __mul),
	LUNA_DECLARE_METHOD(Math_Vec3, __div),
	LUNA_DECLARE_METHOD(Math_Vec3, __eq),
	{0,0}
};

int Math_Vec3::__set_x(lua_State *L)
{
	vec_.x = luaL_checknumber(L, 1);
	return 0;
}

int Math_Vec3::__get_x(lua_State *L)
{
	lua_pushnumber(L, vec_.x);
	return 1;
}

int Math_Vec3::__set_y(lua_State *L)
{
	vec_.y = luaL_checknumber(L, 1);
	return 0;
}

int Math_Vec3::__get_y(lua_State *L)
{
	lua_pushnumber(L, vec_.y);
	return 1;
}

int Math_Vec3::__set_z(lua_State *L)
{
	vec_.z = luaL_checknumber(L, 1);
	return 0;
}

int Math_Vec3::__get_z(lua_State *L)
{
	lua_pushnumber(L, vec_.z);
	return 1;
}

int Math_Vec3::cross(lua_State *L)
{
	Math_Vec3* o = Luna<Math_Vec3>::check(L, 1);
	Math_Vec3* n = new Math_Vec3(L);
	n->vec_ = glm::cross(vec_, o->vec_);
	Luna<Math_Vec3>::push(L, n);
	return 1;
}

int Math_Vec3::rotate(lua_State *L)
{
	float angle = luaL_checknumber(L, 1);
	Math_Vec3* axis = Luna<Math_Vec3>::check(L, 2);
	Math_Vec3* n = new Math_Vec3(L);
	n->vec_ = glm::rotate(vec_, angle, axis->vec_);
	Luna<Math_Vec3>::push(L, n);
	return 1;
}

int Math_Vec3::rotatex(lua_State *L)
{
	float angle = luaL_checknumber(L, 1);
	Math_Vec3* n = new Math_Vec3(L);
	n->vec_ = glm::rotateX(vec_, angle);
	Luna<Math_Vec3>::push(L, n);
	return 1;
}

int Math_Vec3::rotatey(lua_State *L)
{
	float angle = luaL_checknumber(L, 1);
	Math_Vec3* n = new Math_Vec3(L);
	n->vec_ = glm::rotateY(vec_, angle);
	Luna<Math_Vec3>::push(L, n);
	return 1;
}

int Math_Vec3::rotatez(lua_State *L)
{
	float angle = luaL_checknumber(L, 1);
	Math_Vec3* n = new Math_Vec3(L);
	n->vec_ = glm::rotateZ(vec_, angle);
	Luna<Math_Vec3>::push(L, n);
	return 1;
}