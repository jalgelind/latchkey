#include <Lapp/Math_Vec2.h>

using namespace Pxf;

//) inherit: Math_VecT.h

const char Math_Vec2::className[] = "vec2";

const Luna<Math_Vec2>::PropertyType Math_Vec2::properties[] = {
	LUNA_DECLARE_PROPERTY(Math_Vec2, x),
	LUNA_DECLARE_PROPERTY(Math_Vec2, y),
	{0,0}
};

const Luna<Math_Vec2>::FunctionType Math_Vec2::methods[] = {
	LUNA_DECLARE_METHOD(Math_Vec2, rotate),
	LUNA_DECLARE_METHOD(Math_Vec2, distance),
	LUNA_DECLARE_METHOD(Math_Vec2, dot),
	LUNA_DECLARE_METHOD(Math_Vec2, faceforward),
	LUNA_DECLARE_METHOD(Math_Vec2, length),
	LUNA_DECLARE_METHOD(Math_Vec2, normalize),
	LUNA_DECLARE_METHOD(Math_Vec2, reflect),
	LUNA_DECLARE_METHOD(Math_Vec2, refract),
	LUNA_DECLARE_METHOD(Math_Vec2, __add),
	LUNA_DECLARE_METHOD(Math_Vec2, __mul),
	LUNA_DECLARE_METHOD(Math_Vec2, __div),
	LUNA_DECLARE_METHOD(Math_Vec2, __eq),
	{0,0}
};


int Math_Vec2::__set_x(lua_State *L)
{
	vec_.x = luaL_checknumber(L, 1);
	return 0;
}

int Math_Vec2::__get_x(lua_State *L)
{
	lua_pushnumber(L, vec_.x);
	return 1;
}

int Math_Vec2::__set_y(lua_State *L)
{
	vec_.y = luaL_checknumber(L, 1);
	return 0;
}

int Math_Vec2::__get_y(lua_State *L)
{
	lua_pushnumber(L, vec_.y);
	return 1;
}

int Math_Vec2::rotate(lua_State *L)
{
	//) custom
	//) info
	//) here
	float angle = luaL_checknumber(L, 1);
	Math_Vec2* n = new Math_Vec2(L);
	n->vec_ = glm::rotate(vec_, angle);
	Luna<Math_Vec2>::push(L, n);
	return 1;
}