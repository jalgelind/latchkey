#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Font.h>

#include <Lapp/Lapp_Font.h>
#include <Lapp/Lapp_TextBatch.h>
#include <Lapp/Lapp_Texture.h>

using namespace Pxf;

const char Lapp_Font::classNamespace[] = "lapp";
const char Lapp_Font::className[] = "font";

Lunar<Lapp_Font>::RegType Lapp_Font::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Font, load),
    LUNAR_DECLARE_METHOD(Lapp_Font, get_texture),
    LUNAR_DECLARE_METHOD(Lapp_Font, print),
    LUNAR_DECLARE_METHOD(Lapp_Font, set_color),
    LUNAR_DECLARE_METHOD(Lapp_Font, set_default_color),
	LUNAR_DECLARE_METHOD(Lapp_Font, set_draw_offset),
    LUNAR_DECLARE_METHOD(Lapp_Font, valid),
    {0,0}
};


Lapp_Font::Lapp_Font(lua_State *L, void* data)
    : font(0)
    , fontlabel(nullptr)
    , prepared(nullptr)
	, texture_(0)
{
    is_valid = false;
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    load(L);
}
Lapp_Font:: ~Lapp_Font()
{
}

int Lapp_Font::load(lua_State *L)
{
    bool sdf = false;
    const char* str = luaL_checkstring(L, 1);
	texture_ = nullptr;
    font = kernel->GetResourceManager()->Acquire<Pxf::Resource::Font>(str);
    if (font)
    {
		float size = 12.f;
        if (lua_gettop(L) > 1)
        {
            size = luaL_checknumber(L, 2);
        }

        if (lua_gettop(L) > 2 && lua_isboolean(L, 3))
            sdf = lua_toboolean(L, 3);
        fontlabel = device->CreateTextProvider(font, size, sdf);
        prepared = fontlabel->CreateString("");
        auto tex = fontlabel->GetTexture();
		lua_pushlightuserdata(L, (void*)&tex);
        texture_ = std::make_shared<Lapp_Texture>(L, (void*)&tex);
        is_valid = true;
    }
    return 0;
}

int Lapp_Font::get_texture(lua_State* L)
{
    Lunar<Lapp_Texture>::push(L, texture_.get());
    return 1;
}

int Lapp_Font::create_batch(lua_State* L)
{
	Lunar<Lapp_Font>::push(L, this);
	Lunar<Lapp_TextBatch>::push(L, new Lapp_TextBatch(L, 0));
	return 1;
}

int Lapp_Font::print(lua_State *L)
{
    //) def: x : number, y : number, scale = 1 : number, text : string
    if (!is_valid)
        LAPP_ERROR(L, "Attempting to use a broken font.");
    float scale = 1.f;
    const char* str;
    float x, y;

    x = luaL_checknumber(L, 1);
    y = luaL_checknumber(L, 2);
    if (lua_gettop(L) == 4)
    {
        scale = luaL_checknumber(L, 3);
        str = luaL_checkstring(L, 4);
    }
    else
        str = luaL_checkstring(L, 3);
    prepared->Set(str);
    prepared->Print(x, y, scale);
    return 0;
}

int Lapp_Font::set_color(lua_State* L)
{
    if (!is_valid)
        LAPP_ERROR(L, "Attempting to use a broken font.");
    if (lua_gettop(L) == 4)
    {
        const char* name = luaL_checkstring(L, 1);
        char r = luaL_checknumber(L, 2) * 255.f;
        char g = luaL_checknumber(L, 3) * 255.f;
        char b = luaL_checknumber(L, 4) * 255.f;
        fontlabel->DefineColor(name, r, g, b);
    }
    else
    {
        LAPP_ERROR(L, "argument error");
    }
    return 0;
}

int Lapp_Font::set_default_color(lua_State* L)
{
    if (!is_valid)
        LAPP_ERROR(L, "Attempting to use a broken font.");
    if (lua_gettop(L) == 1)
    {
        const char* name = luaL_checkstring(L, 1);
        prepared->SetDefaultColor(name);
    }
    else
    {
        LAPP_ERROR(L, "argument error");
    }
    return 0;
}

int Lapp_Font::set_draw_offset(lua_State* L)
{
    if (!is_valid)
        LAPP_ERROR(L, "Attempting to use a broken font.");
    if (lua_gettop(L) == 2)
    {
		auto dx = luaL_checknumber(L, 1);
		auto dy = luaL_checknumber(L, 2);
        fontlabel->SetDrawOffset(dx, dy);
    }
    else
    {
        LAPP_ERROR(L, "experted two arguments: dx, dy");
    }
    return 0;
}

int Lapp_Font::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
