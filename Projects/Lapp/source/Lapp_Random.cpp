#include <Lapp/Lapp_Random.h>

using namespace Pxf;

const char Lapp_Random::classNamespace[] = "lapp";
const char Lapp_Random::className[] = "random";

Lunar<Lapp_Random>::RegType Lapp_Random::methods[] = {
    LUNAR_DECLARE_METHOD(Lapp_Random, seed),
    LUNAR_DECLARE_METHOD(Lapp_Random, get),
    {0,0}
};

Lapp_Random::Lapp_Random(lua_State *L, void* data)
    : dist_real(0.f, 1.f)
{
}

Lapp_Random::~Lapp_Random()
{}

int Lapp_Random::seed(lua_State* L)
{
    unsigned seed = luaL_checknumber(L, 1);
    rng.seed(seed);
    return 0;
}
// todo: return a table...
int Lapp_Random::get(lua_State* L)
{
    int num = 1;
    if (lua_gettop(L) == 1)
    {
        num = luaL_checknumber(L, 1);
        if (num > 20)
            LAPP_ERROR(L, "you can't request more than 20 random numbers at once.");
    }
    int ret = num;
    while(num-->0)
    {
        float f = dist_real(rng);
        lua_pushnumber(L, f);
    }
    return ret;
}