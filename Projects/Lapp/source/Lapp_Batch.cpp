#include <Lapp/Lapp_Batch.h>
#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/PrimitiveBatch.h>

using namespace Pxf;

//) Deprecated in favor of QuadBuffer.

const char Lapp_Batch::classNamespace[] = "lapp";
const char Lapp_Batch::className[] = "pbatch";

Lunar<Lapp_Batch>::RegType Lapp_Batch::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Batch, set_color),
	LUNAR_DECLARE_METHOD(Lapp_Batch, set_texture_subset),

	LUNAR_DECLARE_METHOD(Lapp_Batch, points_begin),
	LUNAR_DECLARE_METHOD(Lapp_Batch, points_add),
	LUNAR_DECLARE_METHOD(Lapp_Batch, points_end),

	LUNAR_DECLARE_METHOD(Lapp_Batch, lines_begin),
	LUNAR_DECLARE_METHOD(Lapp_Batch, lines_add),
	LUNAR_DECLARE_METHOD(Lapp_Batch, lines_end),

	LUNAR_DECLARE_METHOD(Lapp_Batch, quads_begin),
	LUNAR_DECLARE_METHOD(Lapp_Batch, quads_add_top_left),
	LUNAR_DECLARE_METHOD(Lapp_Batch, quads_add_centered),
	LUNAR_DECLARE_METHOD(Lapp_Batch, quads_set_rotation),
	LUNAR_DECLARE_METHOD(Lapp_Batch, quads_end),

	LUNAR_DECLARE_METHOD(Lapp_Batch, valid),

	{0,0}
};

Lapp_Batch::Lapp_Batch(lua_State *L, void* data)
	: batch(nullptr)
{
	is_valid = false;
	kernel = Pxf::Kernel::GetInstance();
	device = kernel->GetGraphicsDevice();
	batch = device->CreatePrimitiveBatch();
}
Lapp_Batch::~Lapp_Batch()
{
}

int Lapp_Batch::set_color(lua_State* L)
{
	float r = luaL_checknumber(L, 1);
	float g = luaL_checknumber(L, 2);
	float b = luaL_checknumber(L, 3);
	float a = luaL_checknumber(L, 4);
	batch->SetColor(r, g, b, a);
	return 0;
}

int Lapp_Batch::set_texture_subset(lua_State* L)
{
	float tl_x = luaL_checknumber(L, 1);
	float tl_y = luaL_checknumber(L, 2);
	float br_x= luaL_checknumber(L, 3);
	float br_y = luaL_checknumber(L, 4);
	batch->QuadsSetTextureSubset(tl_x, tl_y, br_x, br_y);
	return 0;
}

int Lapp_Batch::points_begin(lua_State* L)
{
	batch->PointsBegin();
	return 0;
}

int Lapp_Batch::points_add(lua_State* L)
{
	float x = luaL_checknumber(L, 1);
	float y = luaL_checknumber(L, 2);
	batch->PointsDraw(x, y);
	return 0;
}

int Lapp_Batch::points_end(lua_State* L)
{
	batch->PointsEnd();
	return 0;
}

int Lapp_Batch::lines_begin(lua_State* L)
{
	batch->LinesBegin();
	return 0;
}

int Lapp_Batch::lines_add(lua_State* L)
{
	float x1 = luaL_checknumber(L, 1);
	float y1 = luaL_checknumber(L, 2);
	float x2 = luaL_checknumber(L, 3);
	float y2 = luaL_checknumber(L, 4);
	batch->LinesDraw(x1, y1, x2, y2);
	return 0;
}

int Lapp_Batch::lines_end(lua_State* L)
{
	batch->LinesEnd();
	return 0;
}

int Lapp_Batch::quads_begin(lua_State* L)
{
	batch->QuadsBegin();
	return 0;
}

int Lapp_Batch::quads_add_top_left(lua_State* L)
{
	float x = luaL_checknumber(L, 1);
	float y = luaL_checknumber(L, 2);
	float w = luaL_checknumber(L, 3);
	float h = luaL_checknumber(L, 4);
	batch->QuadsDrawTopLeft(x, y, w, h);
	return 0;
}

int Lapp_Batch::quads_add_centered(lua_State* L)
{
	float x = luaL_checknumber(L, 1);
	float y = luaL_checknumber(L, 2);
	float w = luaL_checknumber(L, 3);
	float h = luaL_checknumber(L, 4);
	batch->QuadsDrawCentered(x, y, w, h);
	return 0;
}

int Lapp_Batch::quads_set_rotation(lua_State* L)
{
	float rotation = luaL_checknumber(L, 1); //) Rotation angle, in degrees.
	batch->QuadsSetRotation(rotation);
	return 0;
}

int Lapp_Batch::quads_end(lua_State* L)
{
	batch->QuadsEnd();
	return 0;
}

int Lapp_Batch::valid(lua_State *L)
{
	//) Check validity.
	lua_pushboolean(L, is_valid);
	return 1;
}
