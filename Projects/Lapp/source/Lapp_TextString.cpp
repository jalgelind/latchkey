#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Font.h>

#include <Lapp/Lapp_TextString.h>
#include <Lapp/Lapp_TextBatch.h>
#include <cmath>

using namespace Pxf;

const char Lapp_TextString::classNamespace[] = "lapp";
const char Lapp_TextString::className[] = "textstring";

Lunar<Lapp_TextString>::RegType Lapp_TextString::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_TextString, set_kerning),
	LUNAR_DECLARE_METHOD(Lapp_TextString, set_default_color),
	LUNAR_DECLARE_METHOD(Lapp_TextString, set_position),
	LUNAR_DECLARE_METHOD(Lapp_TextString, set_max_length),
	LUNAR_DECLARE_METHOD(Lapp_TextString, set),
	LUNAR_DECLARE_METHOD(Lapp_TextString, size),
	LUNAR_DECLARE_METHOD(Lapp_TextString, draw),
    LUNAR_DECLARE_METHOD(Lapp_TextString, valid),
    {0,0}
};


Lapp_TextString::Lapp_TextString(lua_State *L, void* data)
{
    is_valid = false;
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    bstring = nullptr;

	auto batch = Lunar<Lapp_TextBatch>::check(L, -1);
	auto rbatch = batch->get_batch();
	lua_pop(L, 1);

	LAPP_ASSERT(L, rbatch != nullptr, "Invalid text batch.");

	//int x, int y, int maxlength, const std::string& text
	int x = luaL_checkinteger(L, 1);
	int y = luaL_checkinteger(L, 2);
	int maxl = luaL_checkinteger(L, 3);
	const char* str = luaL_checkstring(L, 4);
	if (rbatch)
		bstring = rbatch->CreateString(x, y, maxl, str);
	LAPP_ASSERT(L, bstring != nullptr, "Invalid batched text string.");
	is_valid = bstring != nullptr;
}

Lapp_TextString:: ~Lapp_TextString()
{
}

int Lapp_TextString::set_kerning(lua_State* L)
{
	if (!is_valid) return 0;
	bool enable = false;
	if(lua_isboolean(L, 1))
	{
		enable = lua_toboolean(L, 1);
	}
	bstring->SetKerning(enable);
	return 0;
}

int Lapp_TextString::set_default_color(lua_State* L)
{
	if (!is_valid) return 0;
	const char* default_color = luaL_checkstring(L, 1);
	bstring->SetDefaultColor(default_color);
	return 0;
}

int Lapp_TextString::set_position(lua_State* L)
{
	int x = luaL_checkinteger(L, 1);
	int y = luaL_checkinteger(L, 2);
	bstring->SetPosition(x, y);
	return 0;
}
int Lapp_TextString::set_max_length(lua_State* L)
{
	if (!is_valid) return 0;
	int max_length = luaL_checkinteger(L, 1);
	bstring->SetMaxLength(max_length);
	return 0;
}

int Lapp_TextString::set(lua_State* L)
{
	if (!is_valid) return 0;
	const char* str = luaL_checkstring(L, 1);
	bstring->Set(str);
	return 0;
}

int Lapp_TextString::size(lua_State* L)
{
	if (!is_valid) return 0;
	auto& qs = bstring->GetQuads();
	lua_pushnumber(L, std::ceil(qs.rect.w));
	lua_pushnumber(L, std::ceil(qs.rect.h));
	return 2;
}

int Lapp_TextString::draw(lua_State* L)
{
	if (!is_valid) return 0;
	int x = luaL_checkinteger(L, 1);
	int y = luaL_checkinteger(L, 2);
	int scale = luaL_checkinteger(L, 3);

	bstring->Print(x, y, scale);
	return 0;
}

int Lapp_TextString::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
