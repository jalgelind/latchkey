#include <Pxf/Kernel.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <Pxf/Resource/Font.h>

#include <Lapp/Lapp_TextBatch.h>
#include <Lapp/Lapp_TextString.h>
#include <Lapp/Lapp_Font.h>

using namespace Pxf;

const char Lapp_TextBatch::classNamespace[] = "lapp";
const char Lapp_TextBatch::className[] = "textbatch";

Lunar<Lapp_TextBatch>::RegType Lapp_TextBatch::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_TextBatch, create_string),
	LUNAR_DECLARE_METHOD(Lapp_TextBatch, draw),
    LUNAR_DECLARE_METHOD(Lapp_TextBatch, valid),
    {0,0}
};


Lapp_TextBatch::Lapp_TextBatch(lua_State *L, void* data)
{
    is_valid = false;
    kernel = Pxf::Kernel::GetInstance();
    device = kernel->GetGraphicsDevice();
    batch = nullptr;

	// TODO: need to check that the font is provided. invalid index assertion might occurr.
	auto font = Luna<Lapp_Font>::check(L, 1);
	textprovider = font->get_provider();
	LAPP_ASSERT(L, textprovider != nullptr, "Invalid textprovider.");
	batch = textprovider->CreateBatch();
	is_valid = batch != nullptr;
}

Lapp_TextBatch:: ~Lapp_TextBatch()
{
}

int Lapp_TextBatch::create_string(lua_State* L)
{
	//) x : integer, y : integer, max_length : integer, text : string
	if (!is_valid) return 0;
	Lunar<Lapp_TextBatch>::push(L, this);
	auto str = new Lapp_TextString(L, 0);
	Lunar<Lapp_TextString>::push(L, str);
	return 1;
}

int Lapp_TextBatch::draw(lua_State* L)
{
	if (!is_valid) return 0;
	batch->Draw();
	return 0;
}

int Lapp_TextBatch::valid(lua_State *L)
{
    lua_pushboolean(L, is_valid);
    return 1;
}
