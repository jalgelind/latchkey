#include <Lapp/Math_Mat4.h>
#include <Lapp/Math_Vec3.h>

using namespace Pxf;

const char Math_Mat4::className[] = "mat4";

const Luna<Math_Mat4>::PropertyType Math_Mat4::properties[] = {
	{0,0}
};

const Luna<Math_Mat4>::FunctionType Math_Mat4::methods[] = {
	LUNA_DECLARE_METHOD(Math_Mat4, determinant),
	LUNA_DECLARE_METHOD(Math_Mat4, inverse),
	LUNA_DECLARE_METHOD(Math_Mat4, transpose),
	LUNA_DECLARE_METHOD(Math_Mat4, translate),
	LUNA_DECLARE_METHOD(Math_Mat4, rotate),
	LUNA_DECLARE_METHOD(Math_Mat4, scale),
	LUNA_DECLARE_METHOD(Math_Mat4, __add),
	LUNA_DECLARE_METHOD(Math_Mat4, __mul),
	LUNA_DECLARE_METHOD(Math_Mat4, __div),
	LUNA_DECLARE_METHOD(Math_Mat4, __eq),
	{0,0}
};

// methods
int Math_Mat4::determinant(lua_State *L)
{
	Math_Mat4* o = Luna<Math_Mat4>::check(L, 1);
	Math_Mat4* n = new Math_Mat4(L);
	lua_pushnumber(L, glm::determinant(mat_));
	return 1;
}

int Math_Mat4::inverse(lua_State *L)
{
	Math_Mat4* v = new Math_Mat4(L);
	v->mat_ = glm::inverse(mat_);
	Luna<Math_Mat4>::push(L, v);
	return 1;
}

int Math_Mat4::transpose(lua_State *L)
{
	Math_Mat4* v = new Math_Mat4(L);
	v->mat_ = glm::transpose(mat_);
	Luna<Math_Mat4>::push(L, v);
	return 1;
}

int Math_Mat4::translate(lua_State *L)
{
	Math_Vec3* vec = Luna<Math_Vec3>::check(L, 1);
	Math_Mat4* v = new Math_Mat4(L);
	v->mat_ = glm::translate(mat_, vec->vec_);
	Luna<Math_Mat4>::push(L, v);
	return 1;
}
		
int Math_Mat4::rotate(lua_State *L)
{
	float angle = luaL_checknumber(L, 1);
	Math_Vec3* axis = Luna<Math_Vec3>::check(L, 2);
	Math_Mat4* v = new Math_Mat4(L);
	v->mat_ = glm::rotate(mat_, angle, axis->vec_);
	Luna<Math_Mat4>::push(L, v);
	return 1;
}

int Math_Mat4::scale(lua_State *L)
{
	Math_Vec3* vec = Luna<Math_Vec3>::check(L, 1);
	Math_Mat4* v = new Math_Mat4(L);
	v->mat_ = glm::scale(mat_, vec->vec_);
	Luna<Math_Mat4>::push(L, v);
	return 1;
}
		
// operators
int Math_Mat4::__add(lua_State *L)
{
	Math_Mat4* v = new Math_Mat4(L);
	if (lua_isnumber(L, 1))
		v->mat_ = mat_ + (float)lua_tonumber(L, 1);
	else
	{
		Math_Mat4* rhs = Luna<Math_Mat4>::check(L, 1);
		v->mat_ = mat_ + rhs->mat_;
	}
	Luna<Math_Mat4>::push(L, v);
	return 1;
}

// TODO: support multiplication with vector.
int Math_Mat4::__mul(lua_State *L)
{
	Math_Mat4* v = new Math_Mat4(L);
	if (lua_isnumber(L, 1))
		v->mat_ = mat_ * (float)lua_tonumber(L, 1);
	else
	{
		Math_Mat4* rhs = Luna<Math_Mat4>::check(L, 1);
		v->mat_ = mat_ * rhs->mat_;
	}
	Luna<Math_Mat4>::push(L, v);
	return 1;
}

int Math_Mat4::__div(lua_State *L)
{
	Math_Mat4* v = new Math_Mat4(L);
	if (lua_isnumber(L, 1))
		v->mat_ = mat_ / (float)lua_tonumber(L, 1);
	else
	{
		Math_Mat4* rhs = Luna<Math_Mat4>::check(L, 1);
		v->mat_ = mat_ / rhs->mat_;
	}
	Luna<Math_Mat4>::push(L, v);
	return 1;
}

int Math_Mat4::__eq(lua_State *L)
{
	Math_Mat4* rhs = Luna<Math_Mat4>::check(L, 1);
	lua_pushboolean(L, mat_ == rhs->mat_);
	return 1;
}