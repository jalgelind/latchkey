#include <shared/Pattern.h>
#include <shared/MusicEngine.h>

using namespace Saint;

Pattern::Pattern(MusicEngine* engine, unsigned steps)
	: m_Engine(engine)
	, m_StepCount(steps)
	, m_PatternTempo(.5f)
{
	for(int i = 0; i < m_StepCount; i++)
	{
		m_Steps.push_back(StepInfo());
	}

	phase = 0;
	omega = 0;

	step = 0;
	last_step = m_StepCount-1;

	SetTempo(engine->GetTempo());
}

Pattern::~Pattern()
{

}

bool Pattern::FromJson(::Json::Value value)
{
	bool ret = true;
	int i = 0;
	for(auto v: value["steps"])
	{
		if (!v.isBool())
			ret = false;
		m_Steps[i].on = v.asBool();
		i++;
	}
	return ret;
}

::Json::Value Pattern::ToJson()
{
	::Json::Value value;
	for(int i = 0; i < m_Steps.size(); i++)
	{
		value["steps"][i] = m_Steps[i].on;
	}
	return value;
}

bool Pattern::SetStep(int step, bool on)
{
	if (step < 0 || step >= m_Steps.size())
		return false;
	m_Steps[step].on = on;
	return true;
}

bool Pattern::GetStep(int step)
{
	if (step < 0 || step >= m_Steps.size())
		return false;
	return m_Steps[step].on;
}

void Pattern::Clear()
{
	for(int i = 0; i < m_StepCount; i++)
	{
		m_Steps[i].on = false;
	}
}

void Pattern::Reset()
{
	Clear();
	phase = 0;
}

void Pattern::SetTempo(float tempo)
{
	double tsec = tempo / (60.0 * m_StepCount * m_PatternTempo);
	double sr = m_Engine->GetSampleRate();
	omega = tsec / sr;
}

void Pattern::SetPatternTempo(float scale)
{
	m_PatternTempo = 1.f / scale;
	SetTempo(m_Engine->GetTempo());
}

Pattern::StepInfo* Pattern::Process()
{
	StepInfo* ret = 0;

	if (phase > 1)
		phase -= floor(phase);
	ph2 = phase * m_StepCount;
	int tmpstep = step;
	step = (int)floor(ph2);

	if (step != last_step && m_Steps[step].on)
		ret = &m_Steps[step];

	last_step = tmpstep;
	phase += omega;

	return ret;
}