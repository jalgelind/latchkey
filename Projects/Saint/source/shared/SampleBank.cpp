#include <shared/SampleBank.h>
#include <Pxf/Resource/ResourceManager.h>
#include <shared/MusicEngine.h>

using namespace Pxf;
using namespace Saint;

SampleBank::SampleBank(Pxf::Kernel* kernel, MusicEngine* engine)
	: m_Kernel(kernel)
	, m_Engine(engine)
	, m_Res(kernel->GetResourceManager())
	, m_SampleCount(0)
{
	m_Logger = kernel->GetLogger("SampleBank");
}

SampleBank::~SampleBank()
{
	Clear();
}

bool SampleBank::FromJson(::Json::Value value)
{
	Clear();
	bool ret = true;
	for(auto t: value["slots"])
	{
		if (Load(t.asString()) == InvalidID)
			ret = false;
	}
	return ret;
}

::Json::Value SampleBank::ToJson()
{
	::Json::Value value;
	for(int i = 0; i < m_Samples.size(); i++)
	{
		value["slots"][i] = m_Samples[i].sound->GetSource();
	}
	return value;
}

SampleBank::SoundID SampleBank::Load(const std::string& path)
{
	for(int i = 0; i < m_Samples.size(); i++)
	{
		if (m_Samples[i].sound->GetSource() == path)
			return i;
	}

	auto snd = m_Res->Acquire<Resource::Sound>(path.c_str());
	
	if (!snd)
		return InvalidID;
	
	SampleSlot s;
	s.sound = snd;
	
	s.length = snd->DataLen();
	s.channels = snd->Channels();
	s.sr = snd->SampleRate();
	s.id = m_Samples.size();

	if (s.sr != m_Engine->GetSampleRate())
	{
		m_Logger.Warning("Sample '%s' has sample rate (=%d), incompatible with host sample rate (=%d).", path.c_str(), s.sr, m_Engine->GetSampleRate());
	}

	if (s.length == 0 || s.channels == 0 || s.sr == 0)
		return InvalidID;

	s.ptr = new float[s.length];

	float fscale = 1.f;
	if (snd->Format() == Resource::Sound::FMT_PCM16)
	{
		fscale = (1.f / 32768.f);
		for(int i = 0; i < s.length; i++)
		{
			short* data = (short*)snd->DataPtr();
			s.ptr[i] = data[i] * fscale;
		}
	}
	else if (snd->Format() == Resource::Sound::FMT_PCM24)
	{
		fscale = (1.f / (1 << 23));
		for(int i = 0; i < s.length; i++)
		{
			int* data = (int*)snd->DataPtr();
			s.ptr[i] = data[i] * fscale;
		}
	}
	else
	{
		for(int i = 0; i < s.length; i++)
		{
			float* data = (float*)snd->DataPtr();
			s.ptr[i] = data[i];
		}
	}

	snd->Unload();
	
	m_Samples.push_back(s);
	m_SampleCount = m_Samples.size();
	return s.id;
}

void SampleBank::Clear()
{
	for(auto& s: m_Samples)
		delete [] s.ptr;
	m_Samples.clear();
}
