#include <Pxf/Audio/AudioDevice.h>
#include <Pxf/Resource/ResourceManager.h>
#include <shared/MusicEngine.h>
#include <shared/SampleBank.h>

#include <Pxf/Base/Memory.h>

using namespace Pxf;
using namespace Saint;

SaintAudioCallback::SaintAudioCallback(Pxf::Audio::AudioDevice* _device, MusicEngine* _engine)
	: Pxf::Audio::AudioCallback(_device)
	, engine_(_engine)
	, mixbuff_size(2 * 1024)
	, mixbuff_(0)
{
	// TODO: std::array?
	mixbuff_ = new float[mixbuff_size];
};

SaintAudioCallback::~SaintAudioCallback()
{
	delete [] mixbuff_;
}

void SaintAudioCallback::Mix(float* _in, float* _out, unsigned _channels, unsigned _size)
{
	float** fin = (float**)_in;
	float** fout = (float**)_out;

	// Reconfigure buffer size if needed
	if (_channels * _size != mixbuff_size)
	{
		mixbuff_size = _channels * _size;
		delete [] mixbuff_;
		mixbuff_ = new float[mixbuff_size];
	}

	engine_->Render(mixbuff_, _channels, _size);

	for(unsigned int i = 0, o = 0; i < _size*2; i += 2, o += 1)
	{
		switch(m_BufferFormat)
		{
		case AudioCallback::EBuffersInterleaved:
			_out[i+0] = mixbuff_[i+0];
			_out[i+1] = mixbuff_[i+1];
			break;
		case AudioCallback::EBuffersSeparate:
			fout[0][o] = mixbuff_[i+0];
			fout[1][o] = mixbuff_[i+1];
			break;
		}
	}
}

MusicEngine::MusicEngine(Pxf::Kernel* kernel, Pxf::Audio::AudioDevice* audiodevice)
	: m_Kernel(kernel)
	, m_Res(kernel->GetResourceManager())
	, m_Tempo(175)
	, m_AudioDevice(audiodevice)
	, m_SampleBank(nullptr)
	, m_State(STOPPED)
	, m_SamplingRate(audiodevice->GetSamplingRate())
	, m_TrackCount(8)
{
	m_Logger = kernel->GetLogger("MusicEngine");
	
	Init();
}

void MusicEngine::Init()
{
	m_SampleBank = Pxf::make_unique<SampleBank>(m_Kernel, this);
	m_Tracks.clear();
	for(int i = 0; i < m_TrackCount; i++)
	{
		m_Tracks.push_back(Track(this, i));
	}
}

void MusicEngine::Reset()
{
	for(auto& t: m_Tracks)
		t.Reset();
}

MusicEngine::~MusicEngine()
{
	m_State = STOPPED;
}

void MusicEngine::Render(float* buffer, int channels, int length)
{
	PXF_ASSERT(channels == 2, "only stereo is supported");
	std::memset(buffer, 0, channels * length * sizeof(float));
	switch(m_State)
	{
	case STOPPED:
		break;
	case PLAYING:
		{
			const float ggain = 1.f/sqrt(2);

			for(int i = 0; i < length*channels; i+=channels)
			{
				for(int ti = 0; ti < m_TrackCount; ti++)
				{
					float buff[2] = {0, 0};
					m_Tracks[ti].Tick(buff);
					buffer[i + 0] += buff[0];
					buffer[i + 1] += buff[1];
				}

				for(int c = 0; c < channels; c++)
					buffer[i+c] *= ggain;
			}
		}
		break;
	default:
		break;
	}
}

void MusicEngine::SetTempo(float tempo)
{
	m_Tempo = tempo;
	for(auto& t: m_Tracks)
		t.SetTempo(tempo);
}

Track* MusicEngine::GetTrack(unsigned int i)
{
	// todo: safe
	return &m_Tracks[i];
}

bool MusicEngine::FromJson(const std::shared_ptr<Pxf::Resource::Json>& json)
{
	if (json)
	{
		::Json::Value& root = json->GetRoot();
		if (root["tempo"].isDouble())
			SetTempo(root["tempo"].asFloat());
		else
			m_Logger.Error("Invalid tempo.");
		
		for(auto t: root["tracks"])
		{
			if (t["id"].isIntegral())
			{
				int id = t["id"].asInt();
				if (!m_Tracks[id].FromJson(t))
					m_Logger.Error("Failed to load track '%d'", id);
			}
			else
			{
				// TODO: test this
				m_Logger.Error("Invalid track id (='%s').", t["id"].asString().c_str());
			}
		}

		if(!m_SampleBank->FromJson(root["samplebank"]))
			m_Logger.Error("Failed to properly load samplebank.");

		// At least partial success...
		return true;
	}
	return false;
}

std::shared_ptr<Pxf::Resource::Json> MusicEngine::ToJson()
{
	::Json::Value root;
	root["tempo"] = GetTempo();
	for(int i = 0; i < m_Tracks.size(); i++)
		root["tracks"][i] = m_Tracks[i].ToJson();
	root["samplebank"] = m_SampleBank->ToJson();
	auto doc = m_Res->CreateJsonDocument();
	doc->SetRoot(root);
	return doc;
}
