#include <shared/meta/Envelope.h>
#include <shared/MusicEngine.h>
#include <Pxf/Base/Utils.h>

using namespace Saint;
using namespace Pxf;

Envelope::Envelope(MusicEngine* engine)
	: value_(0.f)
	, state_(NONE)
{
	m_Engine = engine;
	SetAttack(0.02f);
	SetDecay(0.5f);
	SetSustain(0.5f);
	SetRelease(2.f);
}

Envelope::~Envelope()
{}

bool Envelope::FromJson(::Json::Value value)
{
	if (value["a"].isNumeric() && value["d"].isNumeric() && 
		value["s"].isNumeric() && value["r"].isNumeric())
	{
		SetAttack(value["a"].asFloat());
		SetDecay(value["d"].asFloat());
		SetSustain(value["s"].asFloat());
		SetRelease(value["r"].asFloat());
		return true;
	}
	return false;
}

::Json::Value Envelope::ToJson()
{
	::Json::Value value;
	value["a"] = GetAttack();
	value["d"] = GetDecay();
	value["s"] = GetSustain();
	value["r"] = GetRelease();
	return value;
}

void Envelope::TriggerAttack()
{
	value_ = 0.f;
	state_ = ATTACK;
}

void Envelope::TriggerRelease()
{
	if (state_ != NONE)
		state_ = RELEASE;
}

void Envelope::SetAttack(float secs)
{
	if (secs <= 0.f)
		secs = 1.f / 1000000;
	attack_time_ = secs;
	attack_ = 1.f/(m_Engine->GetSampleRate() * attack_time_);
}

void Envelope::SetDecay(float secs)
{
	if (secs < 0.f)
		secs = 1.f / 1000000;
	decay_time_ = secs;
	decay_ = 1.f/(m_Engine->GetSampleRate() * decay_time_);
}

void Envelope::SetSustain(float level)
{
	sustain_ = Clamp(level, 0.f, 1.f);
}

void Envelope::SetRelease(float secs)
{
	if (secs < 0.f)
		secs = 1.f / 1000000;
	release_time_ = secs;
	release_ = 1.f/(m_Engine->GetSampleRate() * release_time_);
}