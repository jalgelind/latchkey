#include <shared/generators/Sampler.h>
#include <shared/MusicEngine.h>
#include <shared/SampleBank.h>

using namespace Saint;

Sampler::Sampler(MusicEngine* engine, unsigned int ID)
	: m_Engine(engine)
	, m_SampleBank(engine->GetSampleBank())
	, m_ID(ID)
	, m_VolumeEnvelope(engine)
{
	//m_Filter.setFrequency(300);
	//m_Filter.setGain(1.8);
	Reset();
}

Sampler::~Sampler()
{
		
}

bool Sampler::FromJson(::Json::Value value)
{
	m_VolumeEnvelope.FromJson(value["envelope"]);
	m_Filter.FromJson(value["filter"]);
	return true;
}

::Json::Value Sampler::ToJson()
{
	::Json::Value value;
	value["envelope"] = m_VolumeEnvelope.ToJson();
	value["filter"] = m_Filter.ToJson();
	return value;
}

void Sampler::SetOption(int id, float value)
{
	switch(id)
	{
	case SAMPLE_ID:
		{
			SampleBank::SoundID id = (SampleBank::SoundID)value;
			SetSample(id);
		}
		break;
	default:
		// TODO: error
		break;
	}
}

void Sampler::OnKeyPress(int key)
{
	m_Sample.playing = true;
	m_Sample.offset = 0;
	m_VolumeEnvelope.TriggerAttack();
}

void Sampler::OnKeyRelease(int key)
{
	m_VolumeEnvelope.TriggerRelease();
}

void Sampler::Reset()
{
	m_Sample.slot = NULL;
	m_Sample.playing = false;
	m_Sample.offset = 0;
}

void Sampler::SetSample(SampleBank::SoundID sample)
{
	if (sample == SampleBank::InvalidID)
		m_Sample.slot = NULL;
	else
	{
		m_Sample.id = sample;
		m_Sample.slot = m_SampleBank->GetSampleSlot(sample);
	}
	
	m_Sample.offset = 0;
	m_Sample.playing = false;
}

SampleBank::SoundID Sampler::GetSample()
{
	return m_Sample.id;
}

Envelope* Sampler::GetVolumeEnvelope()
{
	return &m_VolumeEnvelope;
}

Effect* Sampler::GetFilter()
{
	return &m_Filter;
}

void Sampler::Tick(float* buffer)
{
	m_VolumeEnvelope.Process();
	if (m_Sample.slot && m_Sample.playing)
	{
		SampleBank::SampleSlot* s = m_Sample.slot;

		float samples[2];
		switch(s->channels) {
		case 1:
			samples[0] = s->ptr[m_Sample.offset];
			samples[1] = s->ptr[m_Sample.offset];
			break;
		case 2:
			samples[0] = s->ptr[m_Sample.offset + 0];
			samples[1] = s->ptr[m_Sample.offset + 1];
			break;
		}

		m_Filter.Tick(samples);

		buffer[0] = samples[0] * m_VolumeEnvelope.GetValue();
		buffer[1] = samples[1] * m_VolumeEnvelope.GetValue();
		
		m_Sample.offset += s->channels;
		if (m_Sample.offset >= s->length)
		{
			m_Sample.playing = false;
			m_Sample.offset = 0;
		}
		//if loop mode is enabled:
		//m_Sample.offset %= s->length;
	}
}