#include <shared/Track.h>
#include <shared/effects/Filter.h>

using namespace Saint;

Track::Track(MusicEngine* engine, unsigned int ID)
	: m_Engine(engine)
	, m_Sampler(engine, ID)
	, m_Pattern(engine, 16)
	, m_ID(ID)
{
	m_Effects.push_back(new Filter());
	m_Effects.push_back(new Filter());
}

Track::~Track()
{

}

bool Track::FromJson(::Json::Value value)
{
	bool ret = true;
	m_ID = value["id"].asInt();
	if (!m_Pattern.FromJson(value["pattern"]))
		ret = false;
	if (!m_Sampler.FromJson(value["sampler"]))
		ret = false;
	m_Effects.clear();
	for(auto& t: value["effects"])
	{
		unsigned int id = t["id"].asUInt();
		if (id == 'FLT1') // TODO: would be nice with create[Lapp]EffectFromID(uint id)
		{
			Filter* flt = new Filter();
			flt->FromJson(t);
			m_Effects.push_back(flt);
		}
	}
	return ret;
}

::Json::Value Track::ToJson()
{
	::Json::Value value;
	value["id"] = m_ID;
	value["pattern"] = m_Pattern.ToJson();
	value["sampler"] = m_Sampler.ToJson();
	for(int i = 0; i < m_Effects.size(); i++)
		value["effects"].append(m_Effects[i]->ToJson());
	return value;
}

void Track::SetTempo(float tempo)
{
	m_Pattern.SetTempo(tempo);
}

void Track::Reset()
{
	m_Pattern.Reset();
	m_Sampler.Reset();
}

void Track::Tick(float* buffer)
{
	Pattern::StepInfo* step = m_Pattern.Process();
	if (step && step->on)
	{
		if (step->sample != SampleBank::InvalidID)
			m_Sampler.SetSample(step->sample);
		int key = 12 * 3; // key pressed (TODO)
		m_Sampler.OnKeyPress(key);
	}
	// if (m_Pattern->shouldRelease())
	else
	{
		//m_Sampler.OnKeyRelease(false);
	}
	m_Sampler.Tick(buffer);

	for(int fxi = 0; fxi < m_Effects.size(); fxi++)
		m_Effects[fxi]->Tick(buffer);
}