#include <Pxf/Base/Utils.h>
#include <shared/effects/Filter.h>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace Saint;
using namespace Pxf;

// Construction/Destruction:

Filter::Filter()
	: Effect('FLT1')
{
	for(auto& f: channelflt_)
	{
		// set sample rate and default configuration options
		//f.setSampleRate(...)
	}
}

bool Filter::FromJson(::Json::Value value)
{
	for(auto& f: channelflt_)
	{
		f.FromJson(value);
	}
	return true;
}

::Json::Value Filter::ToJson()
{
	// Both filters must currently share configuration.
	::Json::Value value;
	value["id"] = GetIdentifier();
	value["filter"] = channelflt_[0].ToJson();
	return value;
}

void Filter::SetOption(int id, float value)
{
	for(auto& f: channelflt_)
		{
		switch(id)
		{
		case SAMPLERATE:
			f.setSampleRate(value);
			break;
		case FILTER_MODE:
			{
				int ivalue = (int)value;
				f.setMode(ivalue);
			}
		case CUTOFF: 
			f.setFrequency(value);
			break;
		case GAIN: 
			f.setGain(value);
			break;
		case BANDWIDTH: 
			f.setBandwidth(value);
			break;
		case MORPH: 
			f.setMorph(value);
			break;
		default:
			// todo: not supported error
			break;
		}
	}
}