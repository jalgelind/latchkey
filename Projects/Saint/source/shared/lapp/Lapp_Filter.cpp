#include <shared/lapp/Lapp_Filter.h>

#include <Pxf/Kernel.h>
#include <shared/Kjell.h>
#include <shared/MusicEngine.h>
#include <shared/effects/Filter.h>

using namespace Saint;

const char Lapp_Filter::classNamespace[] = "saint";
const char Lapp_Filter::className[] = "filter";

Lunar<Lapp_Filter>::RegType Lapp_Filter::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Filter, get_id),
	LUNAR_DECLARE_METHOD(Lapp_Filter, get_option),
	LUNAR_DECLARE_METHOD(Lapp_Filter, set_option),
	{0,0}
};


Lapp_Filter::Lapp_Filter(lua_State *L, void* data)
	: m_Engine(0)
	, m_App(0)
	, Lapp_Effect((Effect*)data)
{
	kernel = Pxf::Kernel::GetInstance();
	m_App = Kjell::KjellApp::Instance();
	m_Engine = m_App->GetMusicEngine();

	// Set effect option constants
	lua_getglobal(L, classNamespace);
	lua_getfield(L, -1, className);
	int mt = lua_gettop(L);
	// TODO: should not be created here. saint.filter must be initialized before constants can be used.
	#define ROBINZDFSFV_OPTION(opt) { lua_pushinteger(L, Filter::opt); lua_setfield(L, mt, #opt);}
	#define ROBINZDFSFV_FILTER_MODE(mode) { lua_pushinteger(L, Filter::mode); lua_setfield(L, mt, #mode);}
	#include "shared/dsp/RobinZDF.def"
	lua_pop(L, 2);
}

Lapp_Filter::~Lapp_Filter()
{
}

int Lapp_Filter::get_id(lua_State* L)
{
	lua_pushinteger(L, m_Effect->GetIdentifier());
	return 1;
}

int Lapp_Filter::get_option(lua_State* L)
{
	//) TODO
	return 0;
}

int Lapp_Filter::set_option(lua_State* L)
{
	//) def: opt : integer, value : number
	//) opt: parameter to change, see table
	//) TODO: add table...
	if (lua_gettop(L) == 2)
	{
		int option = -1;
		if (lua_isnumber(L, 1))
		{
			option = lua_tointeger(L, 1);
			if (option == Filter::FILTER_MODE)
			{
				int mode = luaL_checkinteger(L, 2);
				m_Effect->SetOption(option, mode);
			}
			else
			{
				float value = luaL_checknumber(L, 2);
				m_Effect->SetOption(option, value);
			}
		}
		else
		{
			LAPP_ERROR(L, "option strings not supported, use integer values.");
		}
	}
	else
	{
		LAPP_ERROR(L, "expected two arguments, option and value");
	}
	return 0;
}