#include <shared/lapp/Lapp_MusicEngine.h>
#include <Pxf/Kernel.h>
#include <shared/Kjell.h>
#include <shared/MusicEngine.h>
#include <shared/SampleBank.h>

using namespace Saint;

const char Lapp_MusicEngine::classNamespace[] = "saint";
const char Lapp_MusicEngine::className[] = "engine";

Lunar<Lapp_MusicEngine>::RegType Lapp_MusicEngine::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, get_samplebank),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, get_tracks),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, load_project),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, save_project),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, reset),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, set_state),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, get_state),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, set_tempo),
	LUNAR_DECLARE_METHOD(Lapp_MusicEngine, get_tempo),

	{0,0}
};

Lapp_MusicEngine::Lapp_MusicEngine(lua_State *L, void* data)
	: m_Engine(0)
	, m_App(0)
	, m_LappSampleBank(L, 0)
{
	kernel = Pxf::Kernel::GetInstance();
	m_App = Kjell::KjellApp::Instance();
	m_Engine = m_App->GetMusicEngine();
	for(int i = 0; i < m_Engine->GetTrackCount(); i++)
	{
		//lua_pushinteger(L, i);
		m_Tracks.push_back(new Lapp_Track(L, (void*)i));
		//lua_pop(L, 1);
	}
}

Lapp_MusicEngine::~Lapp_MusicEngine()
{
	for (auto t: m_Tracks)
		delete t;
	m_Tracks.clear();
}

int Lapp_MusicEngine::get_samplebank(lua_State* L)
{
	Lunar<Lapp_SampleBank>::push(L, &m_LappSampleBank, false);
	return 1;
}


int Lapp_MusicEngine::get_tracks(lua_State* L)
{
	lua_newtable(L);
	int table = lua_gettop(L);
	for(int i = 0; i < m_Engine->GetTrackCount(); i++)
	{
		Lunar<Lapp_Track>::push(L, m_Tracks[i], false);
		lua_rawseti(L, table, i + 1);
	}
	return 1;
}

int Lapp_MusicEngine::load_project(lua_State* L)
{
	const char* path = luaL_checkstring(L, 1); //) Path to project file
	lua_pushboolean(L, m_App->LoadProject(path));
	return 1;
}

int Lapp_MusicEngine::save_project(lua_State* L)
{
	const char* path = luaL_checkstring(L, 1); //) Where the project should be saved
	lua_pushboolean(L, m_App->SaveProject(path));
	return 1;
}

int Lapp_MusicEngine::reset(lua_State* L)
{
	m_Engine->Reset();
	return 0;
}

int Lapp_MusicEngine::get_state(lua_State *L)
{
	switch(m_Engine->GetState())
	{
	case MusicEngine::STOPPED:
		lua_pushstring(L, "stopped");
		break;
	case MusicEngine::PLAYING:
		lua_pushstring(L, "playing");
		break;
	default:
		lua_pushstring(L, "unknown");
		break;
	}
	return 1;
}

int Lapp_MusicEngine::set_state(lua_State* L)
{
	std::string state = luaL_checkstring(L, 1);
	if (state == "playing" || state == "play")
		m_Engine->SetState(MusicEngine::PLAYING);
	else if (state == "stopped" || state == "stop")
		m_Engine->SetState(MusicEngine::STOPPED);
	else
	{
		LAPP_ERROR(L, "argument error (expected 'playing'/'stopped')");
	}
	return 0;
}

int Lapp_MusicEngine::set_tempo(lua_State *L)
{
	if (lua_gettop(L) == 1 && lua_isnumber(L, 1))
	{
		float tempo = luaL_checknumber(L, 1);
		m_Engine->SetTempo(tempo);
	}
	else
	{
		LAPP_ERROR(L, "argument error (expected number)");
	}
	return 0;
}

int Lapp_MusicEngine::get_tempo(lua_State *L)
{
	lua_pushnumber(L, m_Engine->GetTempo());
	return 1;
}