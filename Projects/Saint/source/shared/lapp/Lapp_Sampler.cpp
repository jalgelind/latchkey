#include <shared/lapp/Lapp_Sampler.h>
#include <Pxf/Kernel.h>
#include <shared/Kjell.h>
#include <shared/MusicEngine.h>
#include <shared/generators/Sampler.h>

using namespace Saint;


const char Lapp_Sampler::classNamespace[] = "saint";
const char Lapp_Sampler::className[] = "sampler";

Lunar<Lapp_Sampler>::RegType Lapp_Sampler::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Sampler, set_sample),
	LUNAR_DECLARE_METHOD(Lapp_Sampler, get_sample),

	LUNAR_DECLARE_METHOD(Lapp_Sampler, get_volume_envelope_state),
	LUNAR_DECLARE_METHOD(Lapp_Sampler, get_volume_envelope_value),
	LUNAR_DECLARE_METHOD(Lapp_Sampler, update_volume_envelope),

	LUNAR_DECLARE_METHOD(Lapp_Sampler, get_filter),

	LUNAR_DECLARE_METHOD(Lapp_Sampler, get_id),
	LUNAR_DECLARE_METHOD(Lapp_Sampler, get_option),
	LUNAR_DECLARE_METHOD(Lapp_Sampler, set_option),

	{0,0}
};


Lapp_Sampler::Lapp_Sampler(lua_State *L, void* data)
	: m_Engine(0)
	, m_App(0)
	, m_Sampler(0)
	, m_LappFilter(L, 0)
{
	kernel = Pxf::Kernel::GetInstance();
	m_App = Kjell::KjellApp::Instance();
	m_Engine = m_App->GetMusicEngine();
	if (data)
	{
		m_Sampler = (Sampler*)data;
		m_LappFilter = Lapp_Filter(L, m_Sampler->GetFilter());
	}

	// Set effect option constants
	lua_getglobal(L, classNamespace);
	lua_getfield(L, -1, className);
	int mt = lua_gettop(L);
	#define SAMPLER_OPTION(opt) { lua_pushinteger(L, Sampler::opt); lua_setfield(L, mt, #opt);}
	#include "shared/generators/Sampler.def"
	lua_pop(L, 2);
}

Lapp_Sampler::~Lapp_Sampler()
{
}

int Lapp_Sampler::set_sample(lua_State* L)
{
	if (lua_isnil(L, 1))
		kernel->GetLogger("sampler").Error("Cannot set a nil sample.");
	else
	{
		int id = luaL_checkinteger(L, 1);
		m_Sampler->SetSample(id);
	}
	return 0;
}

int Lapp_Sampler::get_sample(lua_State* L)
{

	lua_pushinteger(L, m_Sampler->GetSample());
	return 1;
}

int Lapp_Sampler::get_volume_envelope_value(lua_State* L)
{

	lua_pushnumber(L, m_Sampler->GetVolumeEnvelope()->GetValue());
	return 1;
}

int Lapp_Sampler::get_volume_envelope_state(lua_State* L)
{
	switch(m_Sampler->GetVolumeEnvelope()->GetState())
	{
	case Envelope::NONE:
		lua_pushstring(L, "None");
		break;
	case Envelope::ATTACK:
		lua_pushstring(L, "Attack");
		break;
	case Envelope::DECAY:
		lua_pushstring(L, "Decay");
		break;
	case Envelope::SUSTAIN:
		lua_pushstring(L, "Sustain");
		break;
	case Envelope::RELEASE:
		lua_pushstring(L, "Release");
		break;
	default:
		lua_pushstring(L, "Unknown");
	}
	return 1;
}

int Lapp_Sampler::update_volume_envelope(lua_State* L)
{
	if (lua_gettop(L) != 4)
	{
		LAPP_ERROR(L, "expected 4 arguments.");
		lua_error(L);
	}

	if (lua_isnumber(L, 1))
		m_Sampler->GetVolumeEnvelope()->SetAttack(lua_tonumber(L, 1));
	if (lua_isnumber(L, 2))
		m_Sampler->GetVolumeEnvelope()->SetDecay(lua_tonumber(L, 2));
	if (lua_isnumber(L, 3))
		m_Sampler->GetVolumeEnvelope()->SetSustain(lua_tonumber(L, 3));
	if (lua_isnumber(L, 4))
		m_Sampler->GetVolumeEnvelope()->SetRelease(lua_tonumber(L, 4));
	
	return 0;
}

int Lapp_Sampler::get_filter(lua_State* L)
{
	Lunar<Lapp_Filter>::push(L, &m_LappFilter);
	return 1;
}

int Lapp_Sampler::get_id(lua_State* L)
{
	// TODO: get_id
	return 0;
}

int Lapp_Sampler::get_option(lua_State* L)
{
	// TODO: get_option
	return 0;
}

int Lapp_Sampler::set_option(lua_State* L)
{
	//) def: opt : integer, value : number
	//) opt: parameter to change, see table
	//) TODO: add table...
	if (lua_gettop(L) == 2)
	{
		int option = -1;
		if (lua_isnumber(L, 1))
		{
			option = lua_tointeger(L, 1);
			if (option == Sampler::SAMPLE_ID)
			{
				int value = luaL_checkinteger(L, 2);
				m_Sampler->SetOption(option, value);
			}
			else
			{
				float value = luaL_checknumber(L, 2);
				m_Sampler->SetOption(option, value);	
			}
		}
		else
		{
			LAPP_ERROR(L, "option strings not supported, use integer values.");
		}
	}
	else
	{
		LAPP_ERROR(L, "expected two argument, option and value");
	}
	return 0;
}