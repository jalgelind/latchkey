#include <shared/lapp/Lapp_SampleBank.h>
#include <Pxf/Kernel.h>
#include <shared/Kjell.h>
#include <shared/MusicEngine.h>

using namespace Saint;

const char Lapp_SampleBank::classNamespace[] = "saint";
const char Lapp_SampleBank::className[] = "samplebank";

Lunar<Lapp_SampleBank>::RegType Lapp_SampleBank::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_SampleBank, load),
	//LUNAR_DECLARE_METHOD(Lapp_SampleBank, clear),
	LUNAR_DECLARE_METHOD(Lapp_SampleBank, get_sample),
	LUNAR_DECLARE_METHOD(Lapp_SampleBank, get_samples),
	LUNAR_DECLARE_METHOD(Lapp_SampleBank, get_sample_count),

	{0,0}
};


Lapp_SampleBank::Lapp_SampleBank(lua_State *L, void* data)
	: m_Engine(0)
	, m_App(0)
	, m_SampleBank(0)
{
	kernel = Pxf::Kernel::GetInstance();
	m_App = Kjell::KjellApp::Instance();
	m_Engine = m_App->GetMusicEngine();
	m_SampleBank = m_Engine->GetSampleBank();
}

Lapp_SampleBank::~Lapp_SampleBank()
{
}

int Lapp_SampleBank::load(lua_State* L)
{
	const char* path = luaL_checkstring(L, 1);
	int id = m_SampleBank->Load(path);
	if (id == SampleBank::InvalidID)
		lua_pushnil(L);
	else
		lua_pushinteger(L, id);
	return 1;
}

void Lapp_SampleBank::push_sample_info_table(lua_State* L, SampleBank::SampleSlot* s)
{
	lua_newtable(L);
	int itemtable = lua_gettop(L);
	lua_pushstring(L, s->sound->GetSource());
	lua_setfield(L, itemtable, "filename");
	lua_pushinteger(L, s->id);
	lua_setfield(L, itemtable, "id");
}

int Lapp_SampleBank::get_sample(lua_State* L)
{
	int id = luaL_checkinteger(L, 1);
	if (id >= 0 && id < m_SampleBank->GetCount())
	{
		SampleBank::SampleSlot* s = m_SampleBank->GetSampleSlot(id);
		push_sample_info_table(L, s);
	}
	else
	{
		lua_pushnil(L);
	}
	return 1;
}

int Lapp_SampleBank::get_samples(lua_State* L)
{
	// [{filename: string, id: integer}, ...]
	lua_newtable(L);
	int toptable = lua_gettop(L);
	for(int i = 0; i < m_SampleBank->GetCount(); i++)
	{
		SampleBank::SampleSlot* s = m_SampleBank->GetSampleSlot(i);
		push_sample_info_table(L, s);
		lua_rawseti(L, toptable, i + 1);
	}
	return 1;
}

int Lapp_SampleBank::get_sample_count(lua_State* L)
{
	lua_pushinteger(L, m_SampleBank->GetCount());
	return 1;
}