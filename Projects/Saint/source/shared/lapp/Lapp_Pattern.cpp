#include <shared/lapp/Lapp_Pattern.h>
#include <Pxf/Kernel.h>
#include <shared/Kjell.h>
#include <shared/MusicEngine.h>
#include <shared/Pattern.h>

using namespace Saint;


const char Lapp_Pattern::classNamespace[] = "saint";
const char Lapp_Pattern::className[] = "pattern";

Lunar<Lapp_Pattern>::RegType Lapp_Pattern::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Pattern, get_step),
	LUNAR_DECLARE_METHOD(Lapp_Pattern, set_step),
	LUNAR_DECLARE_METHOD(Lapp_Pattern, set_pattern_tempo),
	LUNAR_DECLARE_METHOD(Lapp_Pattern, clear),
	LUNAR_DECLARE_METHOD(Lapp_Pattern, get_step_count),
	LUNAR_DECLARE_METHOD(Lapp_Pattern, get_last_step),
	LUNAR_DECLARE_METHOD(Lapp_Pattern, get_current_step),

	{0,0}
};

Lapp_Pattern::Lapp_Pattern(lua_State *L, void* data)
	: m_Engine(0)
	, m_App(0)
	, m_Pattern(0)
{
	kernel = Pxf::Kernel::GetInstance();
	m_App = Kjell::KjellApp::Instance();
	m_Engine = m_App->GetMusicEngine();
	if (data)
		m_Pattern = (Pattern*)data;
}

Lapp_Pattern::~Lapp_Pattern()
{
}

int Lapp_Pattern::get_step(lua_State* L)
{
	int step = luaL_checkinteger(L, 1);
	lua_pushboolean(L, m_Pattern->GetStep(step));
	return 1;
}

int Lapp_Pattern::set_step(lua_State* L)
{
	int step = luaL_checkinteger(L, 1);
	bool state = false;
	if (lua_isboolean(L, 2))
		state = lua_toboolean(L, 2);
	else
	{
		LAPP_ERROR(L, "expected second argument to be boolean");
	}
	m_Pattern->SetStep(step, state);
	return 0;
}

int Lapp_Pattern::set_pattern_tempo(lua_State* L)
{
	float tempo = luaL_checknumber(L, 1);
	m_Pattern->SetPatternTempo(tempo);
	return 0;
}

int Lapp_Pattern::clear(lua_State* L)
{
	m_Pattern->Clear();
	return 0;
}

int Lapp_Pattern::get_step_count(lua_State* L)
{
	lua_pushinteger(L, m_Pattern->GetStepCount());
	return 1;
}

int Lapp_Pattern::get_last_step(lua_State* L)
{
	lua_pushinteger(L, m_Pattern->GetLastStep());
	return 1;
}

int Lapp_Pattern::get_current_step(lua_State* L)
{
	lua_pushinteger(L, m_Pattern->GetCurrentStep());
	return 1;
}