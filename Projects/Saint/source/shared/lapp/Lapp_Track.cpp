#include <shared/lapp/Lapp_Track.h>

#include <Pxf/Kernel.h>
#include <shared/Kjell.h>
#include <shared/MusicEngine.h>
#include <shared/Track.h>
#include <shared/lapp/Lapp_Effect.h>

using namespace Saint;

const char Lapp_Track::classNamespace[] = "saint";
const char Lapp_Track::className[] = "track";

Lunar<Lapp_Track>::RegType Lapp_Track::methods[] = {
	LUNAR_DECLARE_METHOD(Lapp_Track, get_sampler),
	LUNAR_DECLARE_METHOD(Lapp_Track, get_generator),
	LUNAR_DECLARE_METHOD(Lapp_Track, get_effect),
	LUNAR_DECLARE_METHOD(Lapp_Track, get_effect_count),
	LUNAR_DECLARE_METHOD(Lapp_Track, get_pattern),
	LUNAR_DECLARE_METHOD(Lapp_Track, get_track_id),

	{0,0}
};


Lapp_Track::Lapp_Track(lua_State *L, void* data)
	: m_Engine(0)
	, m_App(0)
	, m_LappPattern(L, 0)
	, m_Lapp_Sampler(L, 0)
{
	kernel = Pxf::Kernel::GetInstance();
	m_App = Kjell::KjellApp::Instance();
	m_Engine = m_App->GetMusicEngine();
	m_ID = (uintptr_t)data;
	m_Track = m_Engine->GetTrack(m_ID);
	m_LappPattern = Lapp_Pattern(L, &m_Track->GetPattern());
	m_Lapp_Sampler = Lapp_Sampler(L, &m_Track->GetSampler());
	
	// TODO: How do we handle addition of new effects after creation?
	for(int i = 0; i < m_Track->GetEffectCount(); i++)
	{
		Effect* effect = m_Track->GetEffect(i);
		m_LappEffects.push_back(new Lapp_Filter(L, effect));
	}
}

Lapp_Track::~Lapp_Track()
{
}

int Lapp_Track::get_sampler(lua_State* L)
{
	Lunar<Lapp_Sampler>::push(L, &m_Lapp_Sampler, false);
	return 1;
}

int Lapp_Track::get_generator(lua_State* L)
{
	return 0;
}

int Lapp_Track::get_effect(lua_State* L)
{
	int idx = luaL_checkinteger(L, 1); // effect index
	if (idx < 0 || idx >= m_LappEffects.size())
	{
		LAPP_ERROR(L, "invalid effect offset");
		return 0;
	}

	Lapp_Effect* lfx = m_LappEffects[idx];
	if (lfx->GetEffect()->GetIdentifier() == 'FLT1')
	{
		Lunar<Lapp_Filter>::push(L, (Lapp_Filter*)lfx);
	}
	else
		return 0;
	return 1;
}

int Lapp_Track::get_effect_count(lua_State* L)
{
	lua_pushinteger(L, m_LappEffects.size());
	return 1;
}

int Lapp_Track::get_pattern(lua_State* L)
{
	Lunar<Lapp_Pattern>::push(L, &m_LappPattern, false);
	return 1;
}

int Lapp_Track::get_track_id(lua_State* L)
{
	lua_pushinteger(L, m_ID);
	return 1;
}