#include <shared/lapp/Lapp_Effect.h>

//) option: ignore

using namespace Saint;


Lapp_Effect::Lapp_Effect(Effect* eff)
	: m_Effect(eff)
{
}

Effect* Lapp_Effect::GetEffect()
{
	return m_Effect;
}