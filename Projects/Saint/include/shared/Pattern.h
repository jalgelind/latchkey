#ifndef _SAINT_PATTERN_H_
#define _SAINT_PATTERN_H_

#include <Pxf/Resource/Json.h>
#include <vector>
#include <shared/SampleBank.h>

namespace Saint
{
	class MusicEngine;
	class Pattern
	{
	public:
	struct StepInfo
	{
		StepInfo() { on = false; sample=SampleBank::InvalidID; }
		bool on;
		SampleBank::SoundID sample;
	};

	protected:
	MusicEngine* m_Engine;
	unsigned m_StepCount;
	float m_PatternTempo;

	std::vector<StepInfo> m_Steps;

	double omega, phase, ph2;
	int last_step, step;

	public:
		Pattern(MusicEngine* engine, unsigned steps);
		~Pattern();

		bool FromJson(::Json::Value value);
		::Json::Value ToJson();

		std::vector<StepInfo>& GetSteps()
		{
			return m_Steps;
		}

		bool SetStep(int step, bool on);
		
		bool GetStep(int step);

		void Clear();

		void Reset();

		void SetTempo(float tempo);

		void SetPatternTempo(float scale);

		StepInfo* Process();

		unsigned GetStepCount() const
		{
			return m_StepCount;
		}

		unsigned GetLastStep() const
		{
			return last_step;
		}

		unsigned GetCurrentStep() const
		{
			return step;
		}
	};
}

#endif // _SAINT_PATTERN_H_
