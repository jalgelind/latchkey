#ifndef _SAINT_META_ENVELOPE_H_
#define _SAINT_META_ENVELOPE_H_

#include <Pxf/Resource/Json.h>
#include <shared/SampleBank.h>
#include <Pxf/Math/Math.h>

namespace Saint
{
	//class MusicEngine;
	class Envelope
	{
	public:
		enum State { NONE, ATTACK, DECAY, SUSTAIN, RELEASE };
	protected:
		MusicEngine* m_Engine;
		
		float value_;
		float attack_;  // rate
		float decay_;   // rate
		float sustain_; // level
		float release_; // rate

		float attack_time_; // seconds
		float decay_time_;  // seconds
		float release_time_; // seconds
		State state_;

	public:
		Envelope(MusicEngine* engine);
		~Envelope();

		bool FromJson(::Json::Value value);
		::Json::Value ToJson();

		void TriggerAttack();
		void TriggerRelease();

		inline void Process()
		{
			switch(state_)
			{
			case ATTACK:
				value_ += attack_;
				if (value_ > 1.0f)
				{
					value_ = 1.f;
					state_ = DECAY;
				}
				break;
			case DECAY:
				if (value_ > sustain_)
				{
					value_ -= decay_;
					if (value_ <= sustain_)
					{
						value_ = sustain_;
						state_ = SUSTAIN;
					}
				}
				else
				{
					value_ += decay_;
					if (value_ >= sustain_)
					{
						value_ = sustain_;
						state_ = SUSTAIN;
					}
				}
				break;
			case SUSTAIN:
				/* do nothing - sustain value */
				break;
			case RELEASE:
				value_ -= release_;
				if (value_ <= 0.f)
				{
					value_ = 0.f;
					state_ = NONE;
				}
				break;
			}
		}

		const float GetValue() const { return value_; }
		const State GetState() const { return state_; }

		void SetAttack(float secs);
		void SetDecay(float secs);
		void SetSustain(float level);
		void SetRelease(float secs);

		const float GetAttack() const { return attack_time_; }
		const float GetDecay() const { return decay_time_; }
		const float GetSustain() const { return sustain_; }
		const float GetRelease() const { return release_time_; }
	};
}

#endif // _SAINT_META_ENVELOPE_H_
