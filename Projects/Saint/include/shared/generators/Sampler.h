#ifndef _SAINT_GENERATOR_SAMPLER_H_
#define _SAINT_GENERATOR_SAMPLER_H_

#include <shared/generators/Generator.h>
#include <shared/SampleBank.h>
#include <shared/meta/Envelope.h>
#include <shared/effects/Filter.h>

#include <Pxf/Resource/Json.h>

namespace Saint
{
	class MusicEngine;
	class Sampler : public Generator
	{
	public:
		struct Sample
		{
			SampleBank::SoundID id;
			SampleBank::SampleSlot* slot;
			bool playing;
			unsigned offset;
			Sample()
			{
				id = SampleBank::InvalidID;
				slot = NULL;
				playing = false;
				offset = 0;
			}
		};
	protected:
		unsigned m_ID;
		MusicEngine* m_Engine;
		SampleBank* m_SampleBank;
		Sample m_Sample;
		Envelope m_VolumeEnvelope;
		Filter m_Filter;

	public:
		Sampler(MusicEngine* engine, unsigned int ID);
		~Sampler();

		bool FromJson(::Json::Value value);
		::Json::Value ToJson();

		enum Option
		{
			#define SAMPLER_OPTION(option) option,
			#include "Sampler.def"
		};

		void SetOption(int id, float value);

		void OnKeyPress(int key);
		void OnKeyRelease(int key);

		void Reset();

		void SetSample(SampleBank::SoundID sample);
		SampleBank::SoundID GetSample();
		
		Envelope* GetVolumeEnvelope();
		Effect* GetFilter();

		void Tick(float* buffer);
	};
}

#endif // _SAINT_GENERATOR_SAMPLER_H_
