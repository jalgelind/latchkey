#ifndef _SAINT_GENERATOR_H_
#define _SAINT_GENERATOR_H_

#include <Pxf/Resource/Json.h>

namespace Saint
{
	class Generator
	{
	public:
		virtual bool FromJson(::Json::Value value) = 0;
		virtual ::Json::Value ToJson() = 0;

		// TODO: store generator id string

		virtual void SetOption(int id, float value) = 0;

		//virtual float GetOption(int id);

		virtual void OnKeyPress(int key) = 0;
		virtual void OnKeyRelease(int key) = 0;

		virtual void Reset() = 0;
		virtual void Tick(float* buffer) = 0;
	};
}

#endif // _SAINT_GENERATOR_H_
