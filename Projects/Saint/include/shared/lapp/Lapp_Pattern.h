#ifndef _PXF_SAINT_LAPP_PATTERN_H_
#define _PXF_SAINT_LAPP_PATTERN_H_

#include <Lapp/Lapp_Common.h>

namespace Pxf
{
	class Kernel;
}

namespace Kjell
{
	class KjellApp;
}

namespace Saint
{
	class MusicEngine;
	class Pattern;

	class Lapp_Pattern {
		Pxf::Kernel* kernel;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Pattern>::RegType methods[];

		Kjell::KjellApp* m_App;
		MusicEngine* m_Engine;
		Pattern* m_Pattern;

		Lapp_Pattern(lua_State *L, void* data);
		~Lapp_Pattern();

		int get_step(lua_State* L);
		int set_step(lua_State* L);

		int set_pattern_tempo(lua_State* L);
		int clear(lua_State* L);
		int get_step_count(lua_State* L);
		int get_last_step(lua_State* L);
		int get_current_step(lua_State* L);
	};
}

#endif // _PXF_SAINT_LAPP_PATTERN_H_
