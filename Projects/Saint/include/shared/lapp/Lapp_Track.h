#ifndef _PXF_SAINT_LAPP_TRACK_H_
#define _PXF_SAINT_LAPP_TRACK_H_

#include <Lapp/Lapp_Common.h>
#include <shared/lapp/Lapp_Pattern.h>
#include <shared/lapp/Lapp_Sampler.h>

namespace Pxf
{
	class Kernel;
}

namespace Kjell
{
	class KjellApp;
}

namespace Saint
{
	class MusicEngine;
	class Track;
	class Lapp_Track {
		Pxf::Kernel* kernel;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Track>::RegType methods[];

		Kjell::KjellApp* m_App;
		MusicEngine* m_Engine;
		unsigned m_ID;

		Track* m_Track;
		Lapp_Pattern m_LappPattern;
		Lapp_Sampler m_Lapp_Sampler;

		std::vector<Lapp_Effect*> m_LappEffects;

		Lapp_Track(lua_State *L, void* data);
		~Lapp_Track();

		int get_sampler(lua_State* L);
		int get_generator(lua_State* L);
		int get_effect(lua_State* L);
		int get_effect_count(lua_State* L);
		int get_pattern(lua_State* L);
		int get_track_id(lua_State* L);
	};
}

#endif // _PXF_SAINT_LAPP_TRACK_H_
