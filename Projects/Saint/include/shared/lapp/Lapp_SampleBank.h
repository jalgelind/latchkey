#ifndef _PXF_SAINT_LAPP_SAMPLEBANK_H_
#define _PXF_SAINT_LAPP_SAMPLEBANK_H_

#include <Lapp/Lapp_Common.h>
#include <shared/SampleBank.h> // SampleBank::SampleSlot

namespace Pxf
{
	class Kernel;
}

namespace Kjell
{
	class KjellApp;
}

namespace Saint
{
	class MusicEngine;
	class SampleBank;
	class Lapp_SampleBank {
		Pxf::Kernel* kernel;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_SampleBank>::RegType methods[];

		Kjell::KjellApp* m_App;
		MusicEngine* m_Engine;
		SampleBank* m_SampleBank;

		Lapp_SampleBank(lua_State *L, void* data);
		~Lapp_SampleBank();

		int load(lua_State* L);
		void push_sample_info_table(lua_State* L, SampleBank::SampleSlot* s);
		int get_sample(lua_State* L);
		int get_samples(lua_State* L);
		int get_sample_count(lua_State* L);
	};
}

#endif // _PXF_SAINT_LAPP_SAMPLEBANK_H_
