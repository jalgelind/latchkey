#ifndef _PXF_SAINT_LAPP_EFFECT_H_
#define _PXF_SAINT_LAPP_EFFECT_H_

namespace Saint
{
	class Effect;
	class Lapp_Effect
	{
	protected:
		Effect* m_Effect;
	public:
		Lapp_Effect(Effect* eff);
		Effect* GetEffect();
	};
}

#endif // _PXF_SAINT_LAPP_EFFECT_H_