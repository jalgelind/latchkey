#ifndef _PXF_SAINT_LAPP_MUSICENGINE_H_
#define _PXF_SAINT_LAPP_MUSICENGINE_H_

#include <Lapp/Lapp_Common.h>
#include <shared/lapp/Lapp_SampleBank.h>
#include <shared/lapp/Lapp_Track.h>

namespace Pxf
{
	class Kernel;
}

namespace Kjell
{
	class KjellApp;
}

namespace Saint
{
	class MusicEngine;

	class Lapp_MusicEngine {
		Pxf::Kernel* kernel;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_MusicEngine>::RegType methods[];

		Kjell::KjellApp* m_App;
		MusicEngine* m_Engine;
		Lapp_SampleBank m_LappSampleBank;

		std::vector<Lapp_Track*> m_Tracks;

		Lapp_MusicEngine(lua_State *L, void* data);
		~Lapp_MusicEngine();

		int get_samplebank(lua_State* L);
		int get_tracks(lua_State* L);
		int load_project(lua_State* L);
		int save_project(lua_State* L);
		int reset(lua_State* L);

		int get_state(lua_State *L);
		int set_state(lua_State* L);
		int set_tempo(lua_State *L);
		int get_tempo(lua_State *L);
	};
}

#endif // _PXF_SAINT_LAPP_MUSICENGINE_H_
