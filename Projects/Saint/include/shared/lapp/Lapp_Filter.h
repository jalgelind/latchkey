#ifndef _PXF_SAINT_LAPP_FILTER_H_
#define _PXF_SAINT_LAPP_FILTER_H_

#include <Lapp/Lapp_Common.h>
#include <shared/lapp/Lapp_Effect.h>

namespace Pxf
{
	class Kernel;
}

namespace Kjell
{
	class KjellApp;
}

namespace Saint
{
	class MusicEngine;

	class Lapp_Filter : public Lapp_Effect {
		Pxf::Kernel* kernel;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Filter>::RegType methods[];

		Kjell::KjellApp* m_App;
		MusicEngine* m_Engine;

		Lapp_Filter(lua_State *L, void* data);
		~Lapp_Filter();
		
		int get_id(lua_State* L);
		int get_option(lua_State* L);
		int set_option(lua_State* L);
	};
}

#endif // _PXF_SAINT_LAPP_FILTER_H_
