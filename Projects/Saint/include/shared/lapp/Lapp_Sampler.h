#ifndef _PXF_SAINT_LAPP_SAMPLER_H_
#define _PXF_SAINT_LAPP_SAMPLER_H_

#include <Lapp/Lapp_Common.h>
#include <shared/lapp/Lapp_Filter.h>

namespace Pxf
{
	class Kernel;
}

namespace Kjell
{
	class KjellApp;
}

namespace Saint
{
	class MusicEngine;
	class Sampler;

	class Lapp_Sampler {
		Pxf::Kernel* kernel;
	public:
		static const char classNamespace[];
		static const char className[];
		static Lunar<Lapp_Sampler>::RegType methods[];

		Kjell::KjellApp* m_App;
		MusicEngine* m_Engine;
		Sampler* m_Sampler;
		Lapp_Filter m_LappFilter;

		Lapp_Sampler(lua_State *L, void* data);
		~Lapp_Sampler();

		int set_sample(lua_State* L);
		int get_sample(lua_State* L);

		int get_volume_envelope_value(lua_State* L);
		int get_volume_envelope_state(lua_State* L);
		int update_volume_envelope(lua_State* L);

		int get_filter(lua_State* L);
		int get_id(lua_State* L);
		int get_option(lua_State* L);
		int set_option(lua_State* L);
	};
}

#endif // _PXF_SAINT_LAPP_SAMPLER_H_
