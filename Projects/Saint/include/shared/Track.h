#ifndef _SAINT_TRACK_H_
#define _SAINT_TRACK_H_
#include <Pxf/Resource/Json.h>
#include <shared/Pattern.h>
#include <shared/effects/Effect.h>
#include <shared/generators/Sampler.h>

namespace Saint
{
	class MusicEngine;
	class Track
	{
	public:
	struct TrackEvent
	{
		unsigned offset;
		Pattern::StepInfo& step;
	};

	private:
	unsigned int m_ID;
	MusicEngine* m_Engine;
	Sampler m_Sampler;
	Pattern m_Pattern;
	std::vector<Effect*> m_Effects;

	public:
		Track(MusicEngine* engine, unsigned int ID);
		~Track();

		Pattern& GetPattern()
		{
			return m_Pattern;
		}

		Sampler& GetSampler()
		{
			return m_Sampler;
		}

		Effect* GetEffect(int idx)
		{
			if (idx < 0 || idx >= m_Effects.size())
				return NULL;

			return m_Effects[idx];
		}
		int GetEffectCount()
		{
			return m_Effects.size();
		}

		bool FromJson(::Json::Value value);
		::Json::Value ToJson();

		void SetTempo(float tempo);

		void Reset();

		void Tick(float* buffer);
	};
}

#endif // _SAINT_TRACK_H_
