#ifndef _SAINT_MUSICENGINE_H_
#define _SAINT_MUSICENGINE_H_

#include <Pxf/Audio/AudioCallback.h>
#include <Pxf/Base/Logger.h>

#include <Pxf/Resource/Json.h>

#include <shared/Track.h>
#include <atomic>
#include <memory>

namespace Pxf
{
	class Kernel;
	namespace Audio
	{
		class AudioDevice;
	}

	namespace Resource
	{
		class ResourceManager;
	}
}

namespace Saint
{
	class MusicEngine;
	class SampleBank;

	class SaintAudioCallback : public Pxf::Audio::AudioCallback
	{
	public:
		MusicEngine* engine_;
		float* mixbuff_;
		int mixbuff_size;
		SaintAudioCallback(Pxf::Audio::AudioDevice* _device, MusicEngine* _engine);
		virtual ~SaintAudioCallback();
		void Mix(float* _in, float* _out, unsigned _channels, unsigned _size);
	};

	class MusicEngine
	{
	public:
		enum State {PLAYING, STOPPED};
	protected:
		std::unique_ptr<SampleBank> m_SampleBank;

		Pxf::Kernel* m_Kernel;
		Pxf::Resource::ResourceManager* m_Res;
		Pxf::Logger m_Logger;
		Pxf::Audio::AudioDevice* m_AudioDevice;

		std::vector<Track> m_Tracks;
		unsigned int m_TrackCount;
		std::atomic<State> m_State;
		std::atomic<float> m_Tempo;
		std::atomic<unsigned> m_SamplingRate;


	public:
		MusicEngine(Pxf::Kernel* kernel, Pxf::Audio::AudioDevice* audiodevice);
		~MusicEngine();

		void Init();

		void Reset();

		void Render(float* buffer, int channels, int length);

		Track* GetTrack(unsigned int i);

		bool FromJson(const std::shared_ptr<Pxf::Resource::Json>& v);
		std::shared_ptr<Pxf::Resource::Json> ToJson();

		unsigned GetTrackCount() const
		{
			return m_TrackCount;
		}

		SampleBank* GetSampleBank() const
		{
			return m_SampleBank.get();
		}

		void SetState(State state)
		{
			m_State = state;
		}

		State GetState() const
		{
			return m_State;
		}

		int GetSampleRate() const
		{
			return m_SamplingRate;
		}

		void SetTempo(float tempo);

		float GetTempo() const { return m_Tempo; }
	};
}

#endif