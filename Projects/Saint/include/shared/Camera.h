#ifndef _SAINT_CAMERA_H_
#define _SAINT_CAMERA_H_

#include <Pxf/Input/InputDevice.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

/*
class InputDevice;
class Camera;
struct mouse_state;

void MoveCamera(Camera* cam,Pxf::Input::Keyboard* kbd);

class Camera
{
protected:
	glm::mat4 m_Model;
	glm::mat4 m_Projection;

	
	//static const glm::vec3 WORLD_XAXIS;
	//static const glm::vec3 WORLD_YAXIS;
	//static const glm::vec3 WORLD_ZAXIS;
public:
	Camera()
	{
		m_Model = glm::mat4(1.f);
		m_Projection = glm::mat4(1.f);
	}
	virtual ~Camera() {}

	glm::mat4 GetModelView() { return m_Model; }
	glm::mat4 GetProjectionView() { return m_Projection; }

	void SetProjectionView(glm::mat4 _Mtx) { m_Projection = _Mtx; }
};

class SimpleCamera : public Camera
{
private:
	Pxf::Math::Quaternion m_Orientation;
	glm::vec3 m_Eye;
	glm::vec3 m_ViewDir;

	glm::vec3 m_XAxis;
	glm::vec3 m_YAxis;
	glm::vec3 m_ZAxis;

	float m_PitchDegrees;
	float m_FovX;
	float m_AspectRatio;
	float m_ZNear;
	float m_ZFar;

public:
	SimpleCamera()
		: Camera()
		, m_Eye(0.0f,0.0f,0.0f)
		, m_ViewDir(0.f, 0.f, -1.f)
		, m_XAxis(1.0f,0.0f,0.0f)
		, m_YAxis(0.0f,1.0f,0.0f)
		, m_ZAxis(0.0f,0.0f,1.0f)
		, m_PitchDegrees(0.0f)
	{ }

	void UpdateModelViewMtx();

	const Pxf::Math::Quaternion* GetOrientation() const { return &m_Orientation; }
	const glm::vec3* GetPos() const { return &m_Eye; }
	const glm::vec3* GetDir() const { return &m_ViewDir; }

	glm::vec3 GetDir() { return m_ViewDir; }
	glm::vec3 GetPos() { return m_Eye; }

	void SetPerspective(float _fovx, float _aspect, float _znear, float _zfar);
	void SetRotation(float yaw, float pitch, float roll);
	void SetLookAt(float x,float y,float z);
	void SetPosition(float x,float y,float z);
	void SetPosition(const glm::vec3 &_Pos);
	void SetOrientation(Pxf::Math::Quaternion *_Orientation);
	void Translate(float x,float y,float z);
	void Translate(glm::vec3 v);

	glm::vec3 GetPosition();
};
*/

#endif // _SAINT_CAMERA_H_
