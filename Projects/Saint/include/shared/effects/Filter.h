#ifndef _SAINT_Filter_H_
#define _SAINT_Filter_H_

#include <shared/effects/Effect.h>
#include <shared/dsp/RobinZDF.h>

namespace Saint
{
  class Filter : public Effect
  {
  protected:
	RobinZDF channelflt_[2];

  public:
    Filter();   

	// Serialization
	bool FromJson(::Json::Value value);
	::Json::Value ToJson();

	enum Option
	{
		#define ROBINZDFSFV_OPTION(option) option,
		#include "shared/dsp/RobinZDF.def"
	};

	enum FilterMode
	{
		#define ROBINZDFSFV_FILTER_MODE(mode) mode,
		#include "shared/dsp/RobinZDF.def"
		NUM_MODES
	};

	void SetOption(int id, float value);
	void Reset();
	void Tick(float* buffer);
    float TickChannel(float input, int c);
  };

  inline void Filter::Reset()
  {
	  for(auto& f: channelflt_)
		  f.reset();
  }

  inline void Filter::Tick(float* buffer)
  {
	  for (int c=0; c < 2; c++)
		  buffer[c] = channelflt_[c].getSample(buffer[c]);
  }
}

#endif 