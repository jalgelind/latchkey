#ifndef _SAINT_EFFECT_H_
#define _SAINT_EFFECT_H_

#include <Pxf/Resource/Json.h>

namespace Saint
{
	class Effect
	{
	protected:
		const unsigned int m_Identifier;
	public:
		Effect(const unsigned int ident)
			: m_Identifier(ident)
		{}

		virtual bool FromJson(::Json::Value value) = 0;
		virtual ::Json::Value ToJson() = 0;

		struct OptionInfo
		{
			int id;             // Option ID
			int str_id;         // Option ID as a string
			const char* info;   // Option information

			float value_min;    // Minimum supported option value
			float value_max;    // Maximum supported option value

			float step;         // Recommended step size for option value
			float smart_step;   // Smart step for option value (e.g. increasing delay times by 1/4)
			                    // TODO: non-linear smart stepping e.g 1/128 -> 1/64 -> 1/32 -> 1/16 -> 1/8 -> 1/4 -> 2/4 -> 3/4 -> 4/4
			                    // std::vector<float> smart_step?
			//perhaps
			//float smart_step(float from);
		};

		//virtual OptionInfo GetOptionInfo(int id) = 0;
		//virtual float GetOption(int id) = 0;
		virtual void SetOption(int id, float value) = 0;

		virtual void Reset() = 0;
		virtual void Tick(float* buffer) = 0;

		struct EffectInfo
		{
			const char* info;    // Short description of effect
			const char* credits; // Credit DSP author
		};

		//virtual EffectInfo GetInfo() = 0;

		const unsigned int GetIdentifier() const
		{
			return m_Identifier;
		}
	};
}

#endif // _SAINT_EFFECT_H_
