#ifndef _SAINT_SAMPLEBANK_H_
#define _SAINT_SAMPLEBANK_H_

#include <string>
#include <vector>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/Sound.h>
#include <Pxf/Resource/Json.h>

namespace Pxf
{
	class Kernel;
	namespace Resource
	{
		class ResourceManager;
	}
}

namespace Saint
{
	class MusicEngine;
	class SampleBank
	{
	public:
		typedef int SoundID;
		enum {InvalidID = -1};
		
		// TODO: Aligned memory pool

		struct SampleSlot
		{
			std::shared_ptr<Pxf::Resource::Sound> sound;
			
			// TODO: Memory pool entry
			float* ptr;
			SoundID id;
			int length;
			int channels;
			int sr;

			SampleSlot()
			{
				id = InvalidID;
				sound = NULL;
				ptr = NULL;
				length = channels = sr = 0;
			}
		};
		SampleSlot m_InvalidSlot;
		Pxf::Logger m_Logger;
		Pxf::Kernel* m_Kernel;
		MusicEngine* m_Engine;
		Pxf::Resource::ResourceManager* m_Res;
		std::vector<SampleSlot> m_Samples;
		int m_SampleCount;

		SampleBank(Pxf::Kernel* kernel, MusicEngine* engine);
		~SampleBank();

		bool FromJson(::Json::Value value);
		::Json::Value ToJson();

		// returns sound id
		SoundID Load(const std::string& path);

		void Clear();

		void Render(SoundID i, float* buffer, int channels);

		SampleSlot* GetSampleSlot(SoundID id)
		{
			if (id == InvalidID || id >= GetCount())
				return 0;
			return &m_Samples[id];
		}

		int GetCount() const
		{
			return m_SampleCount;
		}
		//bool IsSampleLoaded();
		//bool IsSamplePlaying();

	};
}

#endif // _SAINT_SAMPLEBANK_H_