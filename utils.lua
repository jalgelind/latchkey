path_prefix = path_prefix or "."

function file_read(fn)
    fp = io.open(fn, "r")
    contents = ""
    for line in fp:lines() do
        contents = contents .. line .. "\n"
    end
    fp:close()
    return contents
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

function file_write(target, content)
    local fp = io.open(target, "wb")
    fp:write(content)
    fp:close()
end

function load_library(name)
    local libname = path_prefix .. "/Libraries/" .. name .. "/" .. name .. ".lua"
    loadfile(libname)()
    return library
end

function load_library_license(lib)
    local names = {"LICENSE.txt", "license.txt"
                  ,"LICENSE", "license"
                  ,"COPYRIGHT", "copyright"}
    for i, name in ipairs(names) do
        local license = path_prefix .. "/Libraries/" .. lib .. "/" .. name
        if file_exists(license) then
            return file_read(license)
        end
    end
    return nil
end

function string.startswith(self, substr)
  return string.sub(self, 1, string.len(substr)) == substr
end

function string.to_byte_array(self)
    local out = {}
    for i = 1 , #self do
        out[i] = self:byte(i)
    end
    return out
end

function string.to_formatted_hexblob(self)
    local bytes = self:to_byte_array()
    local out = ""
    for i, byte in ipairs(bytes) do
        out = out .. string.format("0x%X", byte)
        if i ~= #bytes then out = out .. ", " end
        if i%40 == 39 then out = out .. "\n    " end
    end
    return out
end

function string.to_cpp_multiline(self)
    local lines = self:split("\n")
    local out = ""
    for i, line in ipairs(lines) do
        out = out .. string.format('"%s\\n"\n', line:rtrim():gsub('"', '\\"'))
    end
    return out
end

function string.trim(s)
  return s:gsub("^%s*(.-)%s*$", "%1")
end

function string.ltrim(s)
  return s:gsub("^%s*", "")
end

function string.rtrim(s)
  local n = #s
  while n > 0 and s:find("^%s", n) do n = n - 1 end
  return s:sub(1, n)
end

function string.split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
     table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function table.slice(values,i1,i2)
    local res = {}
    local n = #values
    -- default values for range
    i1 = i1 or 1
    i2 = i2 or n
    if i2 < 0 then
        i2 = n + i2 + 1
    elseif i2 > n then
        i2 = n
    end
    if i1 < 1 or i1 > n then
        return {}
    end
    local k = 1
    for i = i1,i2 do
        res[k] = values[i]
        k = k + 1
    end
    return res
end

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end