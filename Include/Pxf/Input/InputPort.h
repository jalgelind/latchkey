#ifndef _PXF_INPUT_INPUTPORT_H_
#define _PXF_INPUT_INPUTPORT_H_

#include <Pxf/Util/Queue.h>

namespace Pxf
{
	class Kernel;
	namespace Input
	{
		class InputController;

		class InputPort
		{
		protected:
			Kernel* m_Kernel;
			InputController* m_InputController;
			std::string m_Description;
			bool m_IsNull;
		public:

			InputPort(Kernel* krn, InputController* inp)
				: m_Kernel(krn)
				, m_InputController(inp)
				, m_Description("Unknown Input Port")
				, m_IsNull(false)
			{}

			InputController& GetInputController()
			{
				return *m_InputController;
			}

			const std::string& GetDescription()
			{
				return m_Description;
			}

			void SetDescription(const std::string& descr)
			{
				m_Description = descr;
			}

			bool IsNull() const { return m_IsNull; }
		};
	}
}

#endif // _PXF_INPUT_INPUTPORT_H_