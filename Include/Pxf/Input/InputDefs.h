#ifndef _PXF_INPUT_INPUTDEFS_H_
#define _PXF_INPUT_INPUTDEFS_H_

namespace Pxf
{
	namespace Input
	{
		enum SpecialKey
		{
		#define INPUT_KEYNAME(name, value) name = value,
		#include "Input.def"
		};

		enum MouseMode
		{
		#define INPUT_MOUSE_MODE(name, value) name = value,
		#include "Input.def"
		};

		enum MouseButton
		{
		#define INPUT_MOUSE_BUTTON(name, value) name = value,
		#include "Input.def"
		};
	}
}

#endif

