#ifndef _PXF_INPUT_INPUTDEVICE_H_
#define _PXF_INPUT_INPUTDEVICE_H_

#include <Pxf/Base/Types.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Memory.h>
#include <Pxf/System.h>
#include <Pxf/Input/InputDefs.h>
#include <Pxf/Base/Logger.h>

#include <Pxf/Input/Keyboard.h>
#include <Pxf/Input/Mouse.h>
#include <Pxf/Input/Gamepad.h>
#include <Pxf/Input/MidiDevice.h>


#include <string>
#include <vector>
#include <memory>

namespace Pxf {
namespace Input
{
	class InputDevice;
	class NullInputController;

	class InputController : public Pxf::System
	{
	protected:
		Kernel* m_Kernel;
		InputDevice* m_InputDevice;

		std::unique_ptr<Keyboard> m_Keyboard;
		std::unique_ptr<Mouse> m_Mouse;
		std::vector<std::unique_ptr<Gamepad>> m_Gamepads;
		std::vector<std::unique_ptr<MidiDevice>> m_MidiDevices;

		bool m_HasKeyboard;
		bool m_HasMouse;
		bool m_HasGamepad;
		bool m_HasMidiDevice;
	public:
		InputController(Kernel* kernel, const char* _Identifier);
		virtual void Update() = 0;

		virtual void Discover() = 0;

		InputDevice& GetInputDevice()
		{
			return *m_InputDevice;
		}

		Keyboard& GetKeyboard()
		{
			return *m_Keyboard.get();
		}

		Mouse& GetMouse()
		{
			return *m_Mouse.get();
		}

		Gamepad& GetGamepad(int id)
		{
			if (id > m_Gamepads.size())
				id = 0;
			return *m_Gamepads[id].get();
		}

		int GetGamepadCount()
		{
			return m_Gamepads.size();
		}

		MidiDevice& GetMidiDevice(int id)
		{
			if (id > m_MidiDevices.size())
				id = 0;
			return *m_MidiDevices[id].get();
		}

		int GetMidiDeviceCount()
		{
			return m_MidiDevices.size();
		}

		bool HasKeyboard()
		{ return m_HasKeyboard; }

		bool HasMouse()
		{ return m_HasMouse; }
		
		bool HasGamepad()
		{ return m_HasGamepad; }

		bool HasMidiDevice()
		{ return m_HasMidiDevice; }

		virtual int GetIntOption(const std::string& option) = 0;
		virtual float GetFloatOption(const std::string& option) = 0;
	};

	//
	// Input device
	//

	class InputDevice
	{
	protected:
		Kernel* m_Kernel;
		Logger logger_;
		Keyboard* kbd_;
		Mouse* mouse_;

		std::vector<Gamepad*> gamepads_;
		std::vector<MidiDevice*> mididevices_; 

		std::vector<std::shared_ptr<InputController>> m_InputControllers;

	public:
	InputDevice(Kernel* _Kernel);
	virtual ~InputDevice();

	void RegisterInputController(const std::shared_ptr<InputController>& inputcontroller);
	void DiscoverDevices();
	void Update();

	Keyboard& GetKeyboard();
	Mouse& GetMouse();
	
	Gamepad& GetGamepad(const std::string& name);
	Gamepad& GetGamepad(int idx = 0);
	int GetGamepadCount()
	{
		return gamepads_.size() - 1;
	}

	MidiDevice& GetMidiDevice(const std::string& name);
	MidiDevice& GetMidiDevice(int idx = 0);
	int GetMidiDeviceCount()
	{
		return mididevices_.size() - 1;
	}

	int GetDefaultMidiOutputPort(int idx = 0);
	int GetDefaultMidiInputPort(int idx = 0);
	};
}
}

#endif // _PXF_INPUT_INPUTDEVICE_H_

