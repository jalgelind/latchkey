#ifndef _PXF_INPUT_MOUSE_H_
#define _PXF_INPUT_MOUSE_H_

#include <Pxf/Input/InputPort.h>
namespace Pxf
{
	namespace Input
	{
		class Mouse : public InputPort
		{
		public:
			Mouse(Kernel* krn, InputController* inp)
				: InputPort(krn, inp)
			{}

			struct Event
			{
				enum Type
				{
					#define MOUSE_EVENT_TYPE(ev) ev,
					#include "Input.def"
				};

				Type type;
				int x, y;   // position
				int dx, dy; // position delta since last event
				int scroll_x, scroll_y; // scroll
				int scroll_dx, scroll_dy; // relative scroll change
				MouseButton mouse_pressed;  // mouse button pressed
				MouseButton mouse_released; // mouse button released
				Graphics::Window* window;

				void Reset()
				{
					type = INVALID;
					x = y = dx = dy = 0;
					scroll_x = scroll_y = 0;
					scroll_dx = scroll_dy = 0;
					mouse_pressed = MOUSE_NONE;
					mouse_released = MOUSE_NONE;
					window = 0;
				}

				Event() { Reset(); }

				bool IsValid() { return type != INVALID; }
				bool IsReleased(int mouse_btn) const { return mouse_released == mouse_btn; }
				bool IsPressed(int mouse_btn) const { return mouse_pressed == mouse_btn; }
				
				//Event& operator=(const Event& other)
				//{ code = other.code; type = other.type; window=other.window; return *this; }
			};

			class MouseQ : public Queue<Event, 7>
			{};
			MouseQ m_EventQueue;
			Event m_LastEvent;
			MouseQ& GetEventQueue() { return m_EventQueue; }
			void ClearEventQueue() { m_EventQueue.Clear(); }


			virtual bool IsButtonDown(int _button) = 0;
			virtual unsigned GetButtonStates() = 0;

			virtual int GetLastButton() = 0;
			virtual int GetLastReleasedButton() = 0;
			virtual void ClearLastButton() = 0;

			virtual int GetMouseWheelDelta() = 0;
			virtual void GetMousePos(int *x, int *y) = 0;
			virtual void SetMousePos(int x, int y) = 0;

			virtual MouseMode GetMouseMode() = 0;
			virtual void SetMouseMode(MouseMode _Mode) = 0;

			virtual void ShowCursor(bool _show) = 0;

			static std::string repr(MouseMode mode)
			{
				#define INPUT_MOUSE_MODE(name, value) { if (name == mode) return #name; }
				#include "Input.def"
				return "";
			}

			static std::string repr(MouseButton btn)
			{
				#define INPUT_MOUSE_BUTTON(name, value) { if (name == btn) return #name; }
				#include "Input.def"
				return "";
			}

			static std::string repr(Mouse::Event::Type ev)
			{
				#define MOUSE_EVENT_TYPE(name) { if (Mouse::Event::name == ev) return #name; }
				#include "Input.def"
				return "";
			}
		};
	}
}

#endif // _PXF_INPUT_MOUSE_H_