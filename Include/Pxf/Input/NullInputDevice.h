#ifndef _PXF_INPUT_NULLINPUTDEVICE_H_
#define _PXF_INPUT_NULLINPUTDEVICE_H_

#include <Pxf/Input/InputDevice.h>
#include <Pxf/Base/Debug.h>

namespace Pxf
{
	namespace Input
	{
	class NullKeyboard : public Keyboard
	{
	public:
		NullKeyboard(Kernel* krn, InputController* inp)
			: Keyboard(krn, inp)
		{
			m_IsNull = true;
			m_Description = "Null Keyboard Device";	
		}
		virtual const char* GetKeyName(int _key)  {return "NULL"; }
		virtual int GetKeyCode(const char* _name) {return 0;}
		virtual bool IsKeyDown(int _key)          {return false;}
		virtual void SetKeyRepeat(bool _On)       {}
		virtual void SetSystemKeys(bool _On)      {}
	};

	//
	// Null Mouse
	//
	class NullMouse : public Mouse
	{
	public:
		NullMouse(Kernel* krn, InputController* inp)
			: Mouse(krn, inp)
		{
			m_IsNull = true;
			m_Description = "Null Mouse Device";	
		}

		virtual bool IsButtonDown(int _button)    {return false;}
		virtual unsigned GetButtonStates()        {return 0;}
		virtual int GetLastButton()               {return 0;}
		virtual int GetLastReleasedButton()       {return 0;}
		virtual void ClearLastButton()            {}
		virtual int GetMouseWheelDelta()          {return 0;}
		virtual void GetMousePos(int *x, int *y)  {}
		virtual void SetMousePos(int x, int y)    {}
		virtual MouseMode GetMouseMode()          {return MODE_RELATIVE;}
		virtual void SetMouseMode(MouseMode _Mode){}
		virtual void ShowCursor(bool _show)       {}
	};

	//
	// Null Gamepad
	//

	class NullGamepad : public Gamepad
	{
	public:
		NullGamepad(Kernel* krn, InputController* inp)
			: Gamepad(krn, inp)
		{
			m_IsNull = true;
			m_Description = "Null Gamepad Device";	
		}

		virtual int ButtonCount()            { return 0;  }
		virtual int AxisCount()              { return 0;  }
		virtual int GetButtonStatus(int idx) { return 0;  }
		virtual float GetAxisValue(int idx)  { return 0.f;}
		virtual float GetPreviousAxisValue(int idx) { return 0.f; }
		virtual AxisStatus GetAxisStatus(int idx) { return Gamepad::AxisStatus(); }

		virtual void Activate() {}
		virtual void Deactivate() {};

	};

	//
	// Null Midi device
	//

	class NullMidiDevice : public MidiDevice
	{
	public:
		NullMidiDevice(Kernel* krn, InputController* inp)
			: MidiDevice(krn, inp)
		{
			m_IsNull = true;
			m_Description = "Null Midi Device";	
		}

		virtual void Activate() {}
		virtual void Deactivate() {};
	};

	//
	// Null input device
	//

	class NullInputController : public Pxf::Input::InputController
	{
	private:
		virtual bool Init()
		{   return true;	}
	public:
		NullInputController(Kernel* _Kernel)
			: Pxf::Input::InputController(_Kernel, "Null Input Controller")
		{
			m_IsNull = true;
			m_Keyboard = Pxf::make_unique<NullKeyboard>(_Kernel, this);
			m_HasKeyboard = true;
			
			m_Mouse = Pxf::make_unique<NullMouse>(_Kernel, this);
			m_HasMouse = true;

			m_Gamepads.push_back(Pxf::make_unique<NullGamepad>(_Kernel, this));
			m_HasGamepad = true;

			m_MidiDevices.push_back(Pxf::make_unique<NullMidiDevice>(_Kernel, this));
			m_HasMidiDevice = true;
		}

		virtual ~NullInputController() {};
		virtual void Update() {}
		virtual void Discover() {};

		virtual int GetIntOption(const std::string& option) { return -1; }
		virtual float GetFloatOption(const std::string& option) { return -1; }
	};
	}
}

#endif // _PXF_INPUT_NULLINPUTDEVICE_H_

