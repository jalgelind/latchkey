#ifndef _PXF_INPUT_KEYBOARD_H_
#define _PXF_INPUT_KEYBOARD_H_

#include <Pxf/Input/InputPort.h>
#include <Pxf/Input/InputDefs.h>

namespace Pxf
{
	namespace Graphics
	{
		class Window;
	}

	namespace Input
	{
		class Keyboard : public InputPort
		{
		public:
			struct Event
			{
				enum Type
				{
					#define KEYBOARD_EVENT(ev) ev,
					#include "Input.def"
				};
				int code;
				Type event_action;
				Graphics::Window* window;
				Event(){code = 0; event_action = KEY_INVALID; window = 0;}
				Event(int c, Type ev) {code = c; event_action = ev; window = 0;}
				Event(int c, Type ev, Graphics::Window* ud) {code = c; event_action = ev; window = ud;}

				bool IsReleased() const { return event_action == KEY_RELEASE; }
				bool IsPressed() const { return event_action == KEY_PRESS; }
				bool IsValid() const { return event_action != KEY_INVALID; }
				bool operator==(const int code)
				{ return code == this->code; }
				bool operator==(const Event& other)
				{
					return code == other.code && 
					       event_action == other.event_action;
				}
				Event& operator=(const Event& other)
				{ code = other.code; event_action = other.event_action; window=other.window; return *this; }
			};

			class KbdQ : public Queue<Event, 7>
			{};

			KbdQ m_KeyQueue;
			KbdQ m_CharQueue;

			Keyboard(Kernel* krn, InputController* inp)
				: InputPort(krn, inp)
			{}

			KbdQ& GetKeyQueue()
			{ return m_KeyQueue;}

			KbdQ& GetCharQueue()
			{ return m_CharQueue;}

			void ClearQueues()
			{
				m_CharQueue.Clear();
				m_KeyQueue.Clear();
			}

			virtual bool IsKeyDown(int _key) = 0;
			virtual void SetKeyRepeat(bool _On) = 0;
			virtual void SetSystemKeys(bool _On) = 0;

			const char* GetKeyName(int _key)
			{
				#define INPUT_KEYNAME(name, value) { if (value == _key) return #name; }
				#include "Input.def"
				return "UNKNOWN";
			}

			virtual int GetKeyCode(const std::string& _name)
			{
				#define INPUT_KEYNAME(name, value) { if (#name == _name) return value; }
				#include "Input.def"
				return -1;
			}

			static std::string repr(Keyboard::Event::Type ev)
			{
				#define KEYBOARD_EVENT(name) { if (Keyboard::Event::name == ev) return #name; }
				#include "Input.def"
				return "";
			}

			static std::string repr(SpecialKey key)
			{
				#define INPUT_KEYNAME(name, value) { if (name == key) return #name; }
				#include "Input.def"
				return "";
			}
		};
	}
}

#endif // _PXF_INPUT_KEYBOARD_H_