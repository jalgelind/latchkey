#ifndef _PXF_INPUT_GAMEPAD_H_
#define _PXF_INPUT_GAMEPAD_H_

#include <Pxf/Input/InputPort.h>
#include <Pxf/Base/String.h>

namespace Pxf
{
	namespace Input
	{
		class Gamepad : public InputPort
		{
		public:
			bool is_activated_;
			float axis_activation_threshold;
			unsigned int product_id;
			unsigned int vendor_id;

			enum EventType
			{
				#define GAMEPAD_EVENT(ev) ev,
				#include "Input.def"
			};

			static std::string repr(Gamepad::EventType ev)
			{
				#define GAMEPAD_EVENT(name) { if (Gamepad::name == ev) return #name; }
				#include "Input.def"
				return "";
			}

			struct Event
			{
				union {int code; int axis; };
				EventType type;
				Event() { code = 0; type = EVENT_INVALID; }
				Event(int v, EventType ev) { code = v; type = ev; }

				bool IsButtonEvent()
				{
					return type == EVENT_RELEASE || type == EVENT_PRESS;
				}

				bool IsAxisEvent()
				{
					return !IsButtonEvent() && IsValid();
				}

				// axis..es?
				bool IsAxisUp() const { return type == EVENT_AXISUP; }
				bool IsAxisDown() const { return type == EVENT_AXISDOWN; }
				bool IsAxisUpRelease() const { return type == EVENT_AXISUPRELEASE; }
				bool IsAxisDownRelease() const { return type == EVENT_AXISDOWNRELEASE; }
				// buttons
				bool IsReleased() const { return type == EVENT_RELEASE; }
				bool IsPressed() const { return type == EVENT_PRESS; }
				bool IsValid() const { return type != EVENT_INVALID; }

				std::string AsString()
				{
					std::string stype;
					stype += repr(type);
					std::string buff;
					if (IsButtonEvent())
						buff += "Button[";
					else
						buff += "Axis[";
					buff = Pxf::Format("%s%d] = %s", buff.c_str(), code, stype.c_str());
					return buff;
				}
				bool operator==(const int code)
				{ return code == this->code;  }
				bool operator==(const Event& other)
				{ return code == other.code && type == other.type;}
				Event& operator=(const Event& other)
				{ code = other.code; type = other.type; return *this; }
			};

			class EventQ : public Queue<Event, 7>
			{};
			EventQ m_EventQ;

			typedef Event AxisStatus;

			Gamepad(Kernel* krn, InputController* inp)
				: InputPort(krn, inp)
				, axis_activation_threshold(0.5f)
				, product_id(0)
				, vendor_id(0)
				, is_activated_(false)
			{
				m_Description = "Unknown Gamepad Device";
			}

			virtual void Activate() = 0;
			virtual void Deactivate() = 0;

			bool IsActivated()
			{
				return is_activated_;
			}

			EventQ& GetEventQueue()
			{
				return m_EventQ;
			}

			void ClearEventQueue()
			{
				m_EventQ.Clear();
			}

			float GetAxisActivationThreshold()
			{
				return axis_activation_threshold;
			}

			void SetAxisActivationThreshold(float threshold)
			{
				axis_activation_threshold = threshold;
			}

			virtual int ButtonCount() = 0;
			virtual int AxisCount() = 0;
			virtual int GetButtonStatus(int idx) = 0;
			virtual float GetAxisValue(int idx) = 0;
			virtual float GetPreviousAxisValue(int idx) = 0;
			virtual AxisStatus GetAxisStatus(int idx) = 0;
		};
}
}

#endif // _PXF_INPUT_MIDIDEVICE_H_