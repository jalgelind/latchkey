#ifndef _PXF_INPUT_MIDIDEVICE_H_
#define _PXF_INPUT_MIDIDEVICE_H_

#include <Pxf/Input/InputPort.h>

namespace Pxf
{
	namespace Input
			{
		class MidiDevice : public InputPort
		{
		public:
			bool is_activated_;

			int num_inputs;
			int num_outputs;

			MidiDevice(Kernel* krn, InputController* inp)
				: InputPort(krn, inp)
				, is_activated_(false)
				, num_inputs(0)
				, num_outputs(0)
			{
				m_Description = "Unknown Midi Device";	
			}

			virtual void Activate() = 0;
			virtual void Deactivate() = 0;
			bool IsActivated()
			{
				return is_activated_;
			}

			enum MidiCode
			{	
				#define MIDI_MESSAGE(name, value) name = value,
				#include "Input.def"
			};

			static std::string repr(MidiDevice::MidiCode code)
			{
			#define MIDI_MESSAGE(name, value) { if (MidiDevice::name == code) return #name; }
			#include "Input.def"
				return "";
			}

			// TODO: Store state for messages, keep track of pitch bends etc.

			struct MidiMessage
			{
				unsigned int time;
				unsigned char status;
				unsigned char data1;
				unsigned char data2;
				MidiMessage()      { time = 0; status = 0; data1 = 0; data2 = 0; }
				MidiMessage(int t, unsigned char s, unsigned char d1, unsigned char d2)
				{ time = t; status = s; data1 = d1; data2 = d2; }
				std::string AsString()
				{
					
					std::string sname = repr((MidiDevice::MidiCode)status);
					if (sname == "")
						sname = Pxf::Format("%d", status);

					std::string buff = Pxf::Format("MidiMessage[%s] with (%d, %d) at %d", sname.c_str(), data1, data2, time);
					return buff;
				}
			};

			class MidiQ : public Queue<MidiMessage, 10>
			{};

			MidiQ msgs;
			MidiQ& GetEvents()
			{
				return msgs;
			}
		};
	}
}
#endif // _PXF_INPUT_MIDIDEVICE_H_