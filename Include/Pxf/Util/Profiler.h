#ifndef _PXF_UTIL_PROFILER_H_
#define _PXF_UTIL_PROFILER_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Platform.h>
#include <Pxf/Util/Queue.h>

#include <algorithm>
#include <string>
#include <map>

#define PXF_PROFILE(name) Pxf::Profiler::AutoProfile pxf_prof_(name)

namespace Pxf
{
	class Profiler
	{
	private:
			static Profiler* s_Profiler;
			Kernel* m_Kernel;
			Logger m_Logger;
			Profiler()
			{
				s_Profiler = this;
				m_Kernel = Kernel::GetInstance();
				m_Logger = m_Kernel->GetLogger("prof");
			}

	public:
		static const int BufferMemory = 5;
		class Probe
		{
		public:
			CircularQueue<double, BufferMemory> buff;
			unsigned hitcount;
			double calls_per_sec;
			double load;
			double clock;
			Probe()
			{
				init();
			}

			void init()
			{
				load = 0;
				hitcount = 0;
				calls_per_sec = 0.;
				clock = Platform::GetTimeF();
			}

			double Average()
			{
				double count = double(BufferMemory);
				double c = 0;
				for(int i = 0; i < BufferMemory+1; i++)
				{
					if (i != buff.start - 1)
						c += buff.buff[i];
				}
				return c / count;
			}
		};

		struct ProbeNode
		{
			std::vector<ProbeNode> children;
			std::string name;
			Probe* probe;
			void* userdata;

			bool operator == (const ProbeNode& other)
			{
				return name == other.name;
			}

			ProbeNode(std::string& name)
			{
				this->name = name;
				probe = NULL;
				userdata = NULL;
			}
			ProbeNode()
			{
				this->name = "";
				probe = NULL;
				userdata = NULL;
			}
		};

		typedef std::map<std::string, Probe> probemap;
		probemap m_Probes;
		ProbeNode root;

		static Profiler& GetInstance()
		{
			if (!s_Profiler)
				s_Profiler = new Profiler();
			return *s_Profiler;
		}

		void SetNode(std::vector<std::string>& path, Probe& buff, ProbeNode& node)
		{
			if (path.size() == 0)
			{
				node.probe = &buff;
				return;
			}
			else
			{
				std::string s = path.back();
				path.pop_back();
					
				std::vector<ProbeNode>::iterator itr = node.children.begin();
				bool found = false;
				for(itr; itr != node.children.end(); itr++)
				{
					if (itr->name == s)
						found = true;
				}

				if (!found)
				{
					node.children.push_back(ProbeNode(s));
				}
				SetNode(path, buff, node.children.back());
			}
		}

		void Rebuild()
		{
			root = ProbeNode();
			probemap::iterator i = m_Probes.begin();
			for(i; i != m_Probes.end(); i++)
			{
				std::vector<std::string> s = split(i->first, '.');
				std::reverse(s.begin(), s.end());
				SetNode(s, i->second, root);
			}
		}

		void Update(const std::string& name, double delta)
		{
			double start = Platform::GetTimeF();
			bool rebuild_tree = false;
			if (m_Probes.find(name) == m_Probes.end())
				rebuild_tree = true;

			Probe& n = m_Probes[name];
			double diff = start - n.clock;
			if (diff >= 1000)
			{
				n.calls_per_sec = (n.hitcount * diff) / diff;
				n.load = n.calls_per_sec * n.Average() * 0.1;
				n.clock = start;
				n.hitcount = 0;
			}

			n.buff.Put(delta);
			n.hitcount++;

			if (rebuild_tree)
				Rebuild();


			Probe& m = m_Probes["profiler.insert"];

			diff = start - m.clock ;
			if (diff >= 1000)
			{
				m.calls_per_sec = (m.hitcount * diff) / diff;
				m.load = m.calls_per_sec * m.Average() * 0.1;
				m.clock = start;
				m.hitcount = 0;
			}
			double end = Platform::GetTimeF();
			m.buff.Put(end - start);
			m.hitcount++;
		}

		std::string reprstr(ProbeNode& node, int indent = 0)
		{
			std::string ret = "";
			for(int i = 0; i < indent; i++)
				ret += "    ";
			ret += node.name;

			if (node.probe)
			{
				ret += Pxf::Format("  --  %.3f ms -- calls/s: %.1f", node.probe->Average(), node.probe->calls_per_sec);
			}
			ret += "\n";

			std::vector<ProbeNode>::iterator itr = node.children.begin();
			for(itr; itr != node.children.end(); itr++)
			{
				ProbeNode& n = *itr;
				ret += reprstr(n, indent+1);
			}
			return ret;
		}

		std::string DumpString()
		{
			PXF_PROFILE("profiler.dumpstring");
			std::string ret = "";
			ret = reprstr(root);
			return ret;
		}

		class AutoProfile
		{
		const char* name_;
		double starttime_;
		public:
			AutoProfile(const char* name)
				: name_(name)
			{
				starttime_ = Platform::GetTimeF();
			}
			~AutoProfile()
			{
				double end = Platform::GetTimeF();
				double delta = end - starttime_;
				Profiler& p = Profiler::GetInstance();
				p.Update(name_, delta);
			}
		};
	};
}

#endif // _PXF_UTIL_PROFILER_H_