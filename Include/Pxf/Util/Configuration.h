#ifndef _PXF_UTIL_CONFIGURATION_H_
#define _PXF_UTIL_CONFIGURATION_H_

#include <Pxf/Base/Config.h>

#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/Json.h>
#include <Pxf/Kernel.h>

#include <string>

#include <json/json.h>

/*
	This is a smallish configuration wrapper for jsoncpp to avoid some boilerplate
	and verbose syntax (when nesting settings in a tree).

	Example:

	Settings set;

	// Set default settings
	set.Set("audio.buffersize", 1024);
	set.Set("audio.max_voices", 8);

	// Mark the default values as fallback values used when
	//  e.g a configuration file defines a key with incompatible type.
	set.MarkAsFallback();
	
	// Overwrite defaults with config file
	if (!m_Settings.Load("data/config.json", true, "// optional comment"))
	{
		// If that failed, save the default configuration as a new file.
		m_Settings.Save("data/config.json");
		logger.Information("Created a new configuration file with default settings.");
	}

	buffersize = Set.GetInt("video.width");
*/

namespace Pxf {
	class Kernel;
	class Configuration
	{
	public:
		std::shared_ptr<Pxf::Resource::Json> m_SettingsDocument;
		Json::Value m_Settings;
		std::string m_Path;
		Json::Value m_FallbackSettings;
		bool m_StrictMerge;
		bool m_Typechecking;
		Kernel* m_Kernel;
		Logger m_Logger;

		Configuration(Kernel* _Kernel)
			: m_Kernel(_Kernel)
			, m_SettingsDocument(0)
			, m_Settings(Json::Value::null)
			, m_StrictMerge(false)
			, m_Typechecking(true)
		{
			m_Logger = m_Kernel->GetLogger("cfg");
		}

		void _Merge(Json::Value& dst, Json::Value& src);

		void Reload()
		{
			m_SettingsDocument->Unload();
			m_SettingsDocument->Load();
		}
		bool Load(const std::string& _path, bool _replace_existing);
		bool Save(const std::string& _path);
		bool Save()
		{
			return Save(m_Path);
		}

		void MarkAsFallback();

		void SetStrictMerge(bool v) { m_StrictMerge = v; }
		void SetTypechecking(bool v) { m_Typechecking = v; }

		Json::Value& GetRoot();
		Json::Value& Get(const std::string& k);
		Json::Value& Get(Json::Value& config, const std::string& k);
		void Set(const std::string& k, Json::Value v, const std::string& description = "");

		void SetString(const std::string& k, const std::string& v);
		void SetFloat(const std::string& k, const float v);
		void SetInt(const std::string& k, const int v);
		void SetBool(const std::string& k, const bool v);

		const std::string GetString(const std::string& k, const std::string& _fallback = "");
		float GetFloat(const std::string& k, float _fallback = 0.0f);
		int GetInt(const std::string& k, int _fallback = 0);
		bool GetBool(const std::string& k, bool _fallback = false);

		bool IsString(const std::string& k);
		bool IsFloat(const std::string& k);
		bool IsInt(const std::string& k);
		bool IsBool(const std::string& k);
	};
} // Pxf
#endif //_PXF_UTIL_CONFIGURATION_H_

