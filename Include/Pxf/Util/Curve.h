#ifndef _PXF_UTIL_BEZIERCURVE_H_
#define _PXF_UTIL_BEZIERCURVE_H_

#include <Pxf/Base/Utils.h>
#include <Pxf/Math/Math.h>

#include <vector>

namespace Pxf {
	class Curve
	{
	public:
		std::vector<glm::vec3> points_;
		Curve()
		{}

		~Curve()
		{}

		void Add(glm::vec3 v)
		{
			points_.push_back(v);
		}

		glm::vec3 GetCatmullRom(float pos)
		{
			float value = Clamp(pos, 0.f, 1.f);
			unsigned cPoints = points_.size();
			unsigned iPoint = Pxf::Clamp<int>((int)floorf(value * cPoints), 0, cPoints);

			glm::vec3 P0 = points_[Clamp<int>(iPoint - 1, 0, cPoints-1)];
			glm::vec3 P1 = points_[Clamp<int>(iPoint + 0, 0, cPoints-1)];
			glm::vec3 P2 = points_[Clamp<int>(iPoint + 1, 0, cPoints-1)];
			glm::vec3 P3 = points_[Clamp<int>(iPoint + 2, 0, cPoints-1)];

			float t = Pxf::Min<float>(value * cPoints, cPoints) - iPoint;
			float t2 = t*t;
			float t3 = t2*t;

			return ((P1 * 2.f) + (-P0 + P2) * t  + (P0*2.f - P1*5.f + P2*4.f - P3) * t2 +
				    (-P0 + P1*3.f- P2*3.f + P3) * t3) * 0.5f;
		}

		// t = [0, 1]
		glm::vec3 GetBezier(float pos)
		{
			float value = Clamp(pos, 0.f, 1.f);
			float v = (pos * (points_.size()+1)) - 1 ;
			const int index = (int)floorf(v);
			float t = v - index;

			glm::vec3 p0 = points_[Pxf::Clamp<int>(index-1, 0, points_.size()-1)];
			glm::vec3 p1 = points_[Pxf::Clamp<int>(index-0, 0, points_.size()-1)];
			glm::vec3 p2 = points_[Pxf::Clamp<int>(index+1, 0, points_.size()-1)];
			glm::vec3 p3 = points_[Pxf::Clamp<int>(index+2, 0, points_.size()-1)];

			float t2 = t*t;
			float t3 = t2*t;

			float b0 = pow(1-t, 3);
			float b1 = 4 - 6*t2 + 3*t3;
			float b2 = 1 + 3*t + 3*t2 - 3*t3;
			float b3 = t3;

			return (p0*b0 + p1*b1 + p2*b2 + p3*b3) * (1/6.f);
		}
	};
}

#endif // _PXF_UTIL_BEZIERCURVE_H_

