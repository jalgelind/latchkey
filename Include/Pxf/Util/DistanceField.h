#ifndef _PXF_UTIL_DISTANCEFIELD_H_
#define _PXF_UTIL_DISTANCEFIELD_H_

/*
	Reference: http://www.codersnotes.com/notes/signed-distance-fields
*/

#include <memory>

namespace Pxf
{
	namespace Resource {
		class Image;
	}
	class DistanceField
	{
	protected:
		struct Point
		{
			Point() { dx = 0; dy = 0; }
			Point(int x, int y) { dx = x; dy = y; }
			int dx, dy;
			int dist_sqr() const { return dx*dx + dy*dy; }
		};

		static Point inside;
		static Point empty;

		struct Grid
		{
			int width;
			int height;
			Point* grid;
			Grid(int w, int h)
			{
				width = w; height = h;
				grid = new Point[w * h];
			}

			Point Get(int x, int y)
			{
				int pos = y*width + x;
				if ( x >= 0 && y >= 0 && x < width && y < height )
					return grid[pos];
				else
					return empty;
			}

			void Put(int x, int y, const Point& p)
			{
				int pos = y*width + x;
				grid[pos] = p;
			}

			void Compare(Point &p, int x, int y, int offsetx, int offsety)
			{
				Point other = Get(x+offsetx, y+offsety );
				other.dx += offsetx;
				other.dy += offsety;

				if (other.dist_sqr() < p.dist_sqr())
					p = other;
			}
		};
		std::shared_ptr<Resource::Image> m_Result;

	public:
		DistanceField()
			: m_Result(0)
		{

		}
		//~DistanceField();

		void GenerateSDF(Grid& grid);
		bool Build(const std::shared_ptr<Resource::Image>& img);
		std::shared_ptr<Resource::Image> GetImage();
	};
}

#endif // _PXF_UTIL_DISTANCEFIELD_H_