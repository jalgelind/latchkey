#ifndef _PXF_UTIL_COLORBANK_H_
#define _PXF_UTIL_COLORBANK_H_

#include <Pxf/Util/Color.h>
#include <string>
#include <map>

namespace Pxf
{
	class ColorBank
	{
	protected:
		std::map<std::string, Color> colors_;
		Color default_;
	public:
		void add(const std::string& name, int r, int g, int b)
		{
			Color c; c.set_range(0, 255);
			c.set_int8(r, g, b);
			colors_[name] = c;
		}

		void add(const std::string& name, float r, float g, float b)
		{
			Color c; c.set_range(0, 255);
			c.set_float(r, g, b);
			colors_[name] = c;
		}

		Color get(const std::string& name, bool* result = 0)
		{
			auto iter = begin(colors_);
			if ((iter = colors_.find(name)) != end(colors_))
			{
				if (result) *result = true;
				return iter->second;
			}
			if (result) *result = false;
			return default_;
		}
	};
}

#endif // _PXF_UTIL_COLORBANK_H_