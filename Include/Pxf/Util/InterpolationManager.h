#ifndef _PXF_UTIL_INTERPOLATIONMANAGER_H_
#define _PXF_UTIL_INTERPOLATIONMANAGER_H_

#include <vector>
#include <Pxf/Util/Easing.h>
#include <Pxf/Base/Memory.h>

namespace Pxf
{
	class Application;

	class InterpolationManager
	{
	private:
		enum InterpolationType {FADE_IN, FADE_OUT, FADE_INOUT};

		struct Interpolation
		{
			float* ptr;
			float start;
			bool inv;
			InterpolationType type;
			std::shared_ptr<Easing> interpolator;
			// unique_ptr causes strange problems, unable to std::move interpolator, try again sometime.

			Interpolation()
				: inv(false)
			{}
		};

		std::vector<Interpolation> m_Array;
		Application* app_;
	public:
		InterpolationManager(Application* app_);
		~InterpolationManager();

		template <typename T>
		void RegisterFadeIn(float* _Var, float _Start, float _Stop, float _From, float _To)
		{
			Interpolation container;
			container.interpolator = std::make_shared<T>();
			container.type = FADE_IN;
			Register(container, _Var, _Start, _Stop, _From, _To);
		}

		template <typename T>
		void RegisterFadeOut(float* _Var, float _Start, float _Stop, float _From, float _To)
		{
			Interpolation container;
			container.interpolator = std::make_shared<T>();
			container.type = FADE_OUT;
			Register(container, _Var, _Start, _Stop, _From, _To);
		}

		template <typename T>
		void RegisterFadeInOut(float* _Var, float _Start, float _Stop, float _From, float _To)
		{
			Interpolation container;
			container.interpolator = std::make_shared<T>();
			container.type = FADE_INOUT;
			Register(container, _Var, _Start, _Stop, _From, _To);
		}

		void Remove(float* _Var)
		{
			auto iter = m_Array.begin();
			for(iter; iter != m_Array.end(); ++iter)
			{
				if ((*iter).ptr == _Var)
				{
					m_Array.erase(iter);
					break;
				}
			}
		}
		void Register(Interpolation container, float* _Var, float _Start, float _Stop, float _From, float _To);

		void UpdateVar(float* _Var, float _NewValue, float _Start, float _Stop, float _From, float _To);
		void Update();
		void Clear()
		{
			m_Array.clear();
		}
	};
}

#endif // _PXF_UTIL_INTERPOLATIONMANAGER_H_