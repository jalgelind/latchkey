#ifndef _PXF_UTIL_QUEUE_H_
#define _PXF_UTIL_QUEUE_H_
namespace Pxf
{
	template <typename T, int BufferCount>
	class QueueBase
	{
	public:
		int num;
		T buff[BufferCount];
		T defaultitem;
		QueueBase()
			: defaultitem()
			, num(0)
		{
			for(int i = 0; i < BufferCount; i++)
				buff[i] = defaultitem;
		}
		virtual void Put(const T& item) = 0;
		virtual T& Peek() = 0;
		virtual T& Pop() = 0;

		bool PopTo(T& t)
		{
			if (num == 0)
			{
				t = defaultitem;
				return false;
			}
			t = Pop();
			return true;
		}

		virtual int Count() = 0;
		virtual void Clear() = 0;

	};

	template <typename T, int BufferCount>
	class Queue : public QueueBase<T, BufferCount>
	{
	public:
		int start;

		Queue()
			: start(0)
		{}

		void Put(const T& item)
		{
			if (this->num < BufferCount)
			{
				int idx = (start + this->num) % BufferCount;
				this->buff[idx] = item;
				this->num++;
			}
		}

		T& Peek()
		{
			if (this->num == 0)
				return this->defaultitem;
			return this->buff[start];
		}

		T& Pop()
		{
			if (this->num == 0)
				return this->defaultitem;
			T& ret = this->buff[start];
			start = (start + 1) % BufferCount;
			this->num--;
			return ret;
		}

		int Count()
		{
			return this->num;
		}

		void Clear()
		{
			this->num = start = 0;
			this->buff[0] = this->defaultitem;
		}

	};

	//
	// CircularQueue (or CircularBuffer...)
	//

	template <typename T, int BufferCount>
	class CircularQueue : public QueueBase<T, BufferCount + 1>
	{
	public:
		static const int size = BufferCount + 1;
		int start;
		int end;

		CircularQueue()
			: start(0), end(0)
		{}

		void Put(const T& item)
		{
			this->buff[end] = item;
			end = (end + 1) % size;
			if (start == end)
				start = (start + 1) % size;
			if (this->num < size)
				this->num++;
		}

		T& Peek()
		{
			return this->buff[start];
		}

		T& Pop()
		{
			if (this->num > 1)
			{
				T& ret = this->buff[start];
				start = (start + 1) % size;
				this->num--;
				return ret;
			}
			return this->defaultitem;
		}

		int Count()
		{
			return this->num;
		}

		void Clear()
		{
			this->num = start = end = 0;
			this->buff[0] = this->defaultitem;
		}

	};
}
#endif // _PXF_UTIL_QUEUE_H_
