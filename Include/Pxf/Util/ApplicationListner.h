#pragma once

namespace Pxf
{
	class ApplicationListner
	{
	public:
		virtual bool OnInit() { return true; }
		virtual void OnReboot() {}
		virtual void OnShutdown() {}
		virtual void OnResize(int x, int y) {}
		virtual void OnMove(int x, int y) {}
		virtual void OnFocus(bool released) {}
		virtual void OnUpdate(unsigned int framenum) {}
		virtual void OnRender(float intperpolation) {}
		virtual void OnFileRefresh(const std::vector<std::string>& paths) {};
	};
}