#ifndef _PXF_UTIL_APPLICATION_H_
#define _PXF_UTIL_APPLICATION_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Kernel.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Util/Configuration.h>
#include <Pxf/Util/InterpolationManager.h>
#include <Pxf/Util/SceneManager.h>
#include <Pxf/Base/Timer.h>
#include <Pxf/Util/Profiler.h>
#include <Pxf/Util/ApplicationListner.h>
#include <memory>

namespace Pxf
{
	namespace Audio
	{
		class AudioDevice;
	}
	namespace Graphics
	{
		class GraphicsDevice;
		class Window;
	}
	namespace Input
	{
		class Keyboard;
		class Mouse;
		class Gamepad;
		class MidiDevice;
		class InputDevice;
	}
	namespace Resource
	{
		class ResourceManager;
	}

	class Lapp;

	class Application
	{
	public:
		// Systems
		Profiler& profiler_;
		Kernel& kernel_;
		Audio::AudioDevice& snd_;
		Graphics::GraphicsDevice& gfx_;
		Input::InputDevice& inp_;
		Input::Keyboard& kbd_;
		Input::Gamepad& pad_;
		Input::Mouse& mouse_;
		Resource::ResourceManager& res_;
		std::unique_ptr<InterpolationManager> interpolmgr_;
		Logger logger_;
		Timer start_;

		std::vector<std::shared_ptr<ApplicationListner>> listners_;

		bool vsync_;
	
		// Configuration
		Configuration cfg_;
		unsigned int refresh_rate_;

		// Scene management
		SceneManager scene_manager_;
		bool just_switched_scene_;

		// Window, window properties and projection matrix
		std::shared_ptr<Graphics::Window> wnd_;
		glm::mat4 prjmtx_;
		unsigned int window_width_;
		unsigned int window_height_;
		Graphics::Rect root_;

		// Window state
		bool  is_running_;
		float fps_;
		float running_time_;
		unsigned int frame_counter_;

		Application(Kernel* _Kernel, const char* loggertag);
		virtual ~Application();

		// snigelton
		static Application* GetInstance();

		float GetFPS() const { return fps_; }

		virtual bool Init();
		virtual void Reboot() = 0;
		virtual void Quit() { is_running_ = false; };

		void AddListner(std::shared_ptr<ApplicationListner> listner);
		void RemoveListner(std::shared_ptr<ApplicationListner> listner);

		virtual bool OnInit() = 0;
		virtual void OnResize(int x, int y) = 0;
		virtual void OnMove(int x, int y) = 0;
		virtual void OnFocus(bool released) = 0;
		virtual void OnShutdown() = 0;

		void CheckForReloadedResources();

		void _DoUpdate(unsigned int framenum);
		void _DoRender(float _Interpolation);

		virtual void DoUpdate(unsigned int framenum) = 0;
		virtual void DoRender(float _Interpolation) = 0;

	protected:
		void ProcessWindowEvents();

	public:
		void RunConstantFPS();
		void RunVariableFPS();
		virtual void Run();
	};
}

#endif //_PXF_UTIL_APPLICATION_H_

