#ifndef _PXF_UTIL_SCENEMANAGER_H_
#define _PXF_UTIL_SCENEMANAGER_H_

#include <vector>
#include <memory>

namespace Pxf {
	class Application;
	class Scene;

	// globally lockable scene by setting SceneManager.locked_.

	class SceneManager
	{
	public:
		Application* app_;
		std::vector<std::shared_ptr<Scene>> Scenes;
		SceneManager(Application* app);
		~SceneManager();

		void AddScene(Scene* _Scene);

		// Set permanent scene
		void SetScene(Scene* _Scene);

		// Restore previous scene
		void RestoreScene(Scene* _Scene);

		bool locked_; // locked to scene, update will not change anything.

		Scene* prev_scene_;
		Scene* active_scene_;

		// should be run every frame to detect timed scene changes
		// returns true if scene was changed
		bool Update();

		Scene* GetPreviousScene();
		Scene* GetCurrentScene();
		Scene* GetNextScene();
		Scene* GetSceneAtIndex(int _Index);

		int GetSceneCount()
		{
			return Scenes.size();
		}
	};
}

#endif // _PXF_UTIL_SCENEMANAGER_H_