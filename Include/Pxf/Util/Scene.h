#ifndef _PXF_UTIL_SCENE_H_
#define _PXF_UTIL_SCENE_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Util/Application.h>
#include <Pxf/Util/InterpolationManager.h>

namespace Pxf {
	namespace Audio    { class AudioDevice;    }
	namespace Input    { class InputDevice;    }
	namespace Graphics { class GraphicsDevice; }

	class Scene
	{
	protected:
		bool developer_mode_;
		bool locked_;
		float scene_start_;
		float scene_stop_;
		float scene_duration_;
		friend class InterpolationManager;
		friend class SceneManager;
	public:
		Application& app_;
		Logger logger_;
		Audio::AudioDevice& snd_;
		Input::InputDevice& inp_;
		Input::Keyboard& kbd_;
		Input::Gamepad& pad_;
		Input::Mouse& mouse_;
		Graphics::GraphicsDevice& gfx_;

		InterpolationManager interpol_;

		Scene(Application& app, const char* loggertag)
			: app_(app)
			, snd_(app.snd_)
			, gfx_(app.gfx_)
			, inp_(app.inp_)
			, kbd_(app.kbd_)
			, pad_(app.pad_)
			, mouse_(app.mouse_)
			, developer_mode_(false)
			, locked_(true)
			, scene_start_(0.f)
			, scene_stop_(999999999999999.f)
			, scene_duration_(0.f)
			, interpol_(&app)
		{
			logger_ = app_.kernel_.GetLogger(loggertag);
		}

		virtual ~Scene()
		{}

		void SetTimedScene(float start, float end)
		{
			locked_ = false;
			scene_start_ = start;
			scene_stop_ = end;
			scene_duration_ = end-start;
		}

		void SetLock(bool v)
		{
			locked_ = v;
		}

		// 0..1
		float GetSceneProgress()
		{
			float current_pos = app_.running_time_ - scene_start_;
			return current_pos/scene_duration_;
		}

		virtual void Init() = 0;
		virtual void Reset() = 0;

		void _DoUpdate(unsigned int framenum)
		{
			DoUpdate(framenum);
		}

		void _DoRender(float _Interpolation)
		{
			interpol_.Update();
			DoRender(_Interpolation);
		}

		virtual void DoInput() = 0;
		virtual void DoUpdate(unsigned int framenum) = 0;
		virtual void DoRender(float _Interpolation) = 0;
	};
}

#endif // _PXF_UTIL_SCENE_H_