/*
Robert Penners easing equations ported from Actionscript to C++.
Website: http://www.robertpenner.com/easing/

Copyright � 2001 Robert Penner
Ported to C++ by Johannes Algelind
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the author nor the names of contributors may be used to endorse or promote products derived from this 
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef _PXF_UTIL_EASING_H_
#define _PXF_UTIL_EASING_H_

#include <Pxf/Base/Utils.h>
#include <Pxf/Math/Math.h> // PI

/*
	"from" is the origin value
	"from" moves towards "from+offset" under duration "dur".
	
*/

namespace Pxf {
	class Easing
	{
	public:
		float from, offset, duration;

		// For bouncing easing
		float back; // 1.70158f = overshoot by 10%

		// For elastic easing
		float amplitude, period;

		Easing()
			: from(0.f)
			, offset(0.f)
			, duration(0.f)
			, back(1.70158f)
			, amplitude(0.5f)
			, period(1.5f)
		{}

		Easing(float from, float offset, float duration)
			: from(from)
			, offset(offset)
			, duration(duration)
			, back(1.70158f)
			, amplitude(0.5f)
			, period(1.5f)
		{}

		Easing(float from, float offset, float duration, float back)
			: from(from)
			, offset(offset)
			, duration(duration)
			, back(back)
			, amplitude(0.5f)
			, period(1.5f)
		{}

		Easing(float from, float offset, float duration, float amplitude, float period)
			: from(from)
			, offset(offset)
			, duration(duration)
			, back(1.70158f)
			, amplitude(amplitude)
			, period(period)
		{}

		void Set(float _from, float _offset, float _dur)
		{
			from = _from; offset = _offset; duration = _dur;
		}

		void SetBounce(float s)
		{
			this->back = s;
		}

		void SetAmplitude(float amplitude)
		{
			this->amplitude = amplitude;
		}

		void SetPeriod(float period)
		{
			this->period = period;
		}

		// 0 < t < dur
		virtual float EaseIn(float t) = 0;
		virtual float EaseOut(float t) = 0;
		virtual float EaseInOut(float t) = 0;
	};

	class EasingLinear : public Easing
	{
	public:
		EasingLinear()
			: Easing()
		{}

		EasingLinear(float from, float offset, float dur)
			: Easing(from, offset, dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			return offset*(t/duration) + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			return offset*(t/duration) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			return offset*(t/duration) + from;
		}
	};

	class EasingBack : public Easing
	{
	public:
		EasingBack()
			: Easing()
		{}

		EasingBack(float from, float offset, float dur)
			: Easing(from, offset, dur)
		{}

		EasingBack(float from, float offset, float dur, float back)
			: Easing(from, offset, dur, back)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return offset*t*t*((back+1)*t - back) + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t = t/duration-1;
			return (offset*t*t*((back+1)*t + back) + 1) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration/2;			
			if (t < 1.f)
			{
				float ss = back * 1.525f;
				return (offset/2.f*(t*t*(ss+1.f)*t - ss)) + from;
			}
			t -= 2;
			float ss = back * 1.525f;
			return offset/2*(t*t*((ss+1)*t + ss) + 2) + from;
		}
	};

	class EasingElastic : public Easing
	{
	public:
		EasingElastic()
			: Easing()
		{}

		EasingElastic(float from, float offset, float duration)
			: Easing(from, offset, duration)
		{}

		EasingElastic(float from, float offset, float duration, float amplitude, float period)
			: Easing(from, offset, duration, amplitude, period)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			float s = 0.0f;
			if (t == 0)
				return from; 

			t /= duration;
			if (t == 1.f)
				return from+offset;  

			if (period == 0.0f)
				period=duration*.3f;

			if (amplitude == 0.f || amplitude < fabs(offset))
			{
				amplitude=offset;
				s = period/4.f;
			}
			else
				s = period/(2.f*glm::pi<float>()) * asinf(offset/amplitude);

			t -= 1.f;
			return -(amplitude*powf(2.f,10.f*t) * sinf((t*duration-s)*(2.f*glm::pi<float>())/period)) + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);

			float s;
			if (t == 0.f)
				return from;

			t /= duration;
			if (t == 1.f)
				return from+offset;

			if (period == 0.f)
				period=duration*.3f;

			if (!amplitude || amplitude < fabs(offset))
			{ 
				amplitude=offset;
				s=period/4;
			}
			else
				s = period/(2*glm::pi<float>()) * asinf (offset/amplitude);
			return (amplitude*powf(2,-10*t) * sinf( (t*duration-s)*(2*glm::pi<float>())/period ) + offset + from);

		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			
			float s;
			if (t == 0)
				return from;

			t/=duration/2.f;
			if (t == 2.f)
				return from+offset;

			if (period == 0.f)
				period=duration*(.3f*1.5f);

			if (!amplitude || amplitude < fabs(offset))
			{ 
				amplitude=offset;
				s=period/4;
			}
			else
				s = period/(2.f*glm::pi<float>()) * asinf(offset/amplitude);

			if (t < 1)
			{
				t -= 1;
				return -.5f*(amplitude*powf(2,10.f*t) * sinf((t*duration-s)*(2.f*glm::pi<float>())/period)) + from;
			}
			t -= 1;
			return amplitude*powf(2.f,-10.f*t) * sinf((t*duration-s)*(2.f*glm::pi<float>())/period)*.5f + offset + from;
		}
	};

	class EasingBounce : public Easing
	{
	public:
		EasingBounce()
			: Easing()
		{}

		EasingBounce(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			return offset - EaseOut(duration-t) + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			float r = 0.0f;
			t /= duration;
			if (t < 1.f/2.75f)
			{
				r = offset*(7.5625f*t*t)+from;
			}
			else if (t < (2.f/2.75f))
			{
				t -= 1.5f/2.75f;
				r = offset*(7.5625f*t*t+.75f)+from;
			}
			else if (t < (2.5/2.75f))
			{
				t -= 2.25f/2.75f;
				r = offset*(7.5625f*t*t+.9375f)+from;
			}
			else 
			{
				t -= 2.625f/2.75f;
				r = offset*(7.5625f*t*t+.984375f)+from;
			}
			return r;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			if (t < duration/2.f)
				return EaseIn(t*2) * .5f + from;
			else
				return (EaseOut(t*2-duration) * .5f + offset*.5f + from);
		}
	};

	class EasingCirc : public Easing
	{
	public:
		EasingCirc()
			: Easing()
		{}

		EasingCirc(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return -offset * (sqrtf(1.f - t*t) - 1.f) + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t = t/duration-1.f;
			return offset * sqrtf(1.f - t*t) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration/2.f;
			if (t < 1.f)
				return -offset/2.f * (sqrtf(1.f - t*t) - 1) + from;
			t -= 2.f;
			return offset/2 * (sqrtf(1.f - t*t) + 1.f) + from;
		}
	};

	class EasingCubic: public Easing
	{
	public:
		EasingCubic()
			: Easing()
		{}

		EasingCubic(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return offset*t*t*t + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t = t/duration-1.f;
			return offset*(t*t*t + 1) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration/2.f;
			if (t < 1.f)
				return offset/2*t*t*t + from;
			t -= 2.f;
			return offset/2.f*(t*t*t + 2) + from;
		}
	};

	class EasingExponential : public Easing
	{
	public:
		EasingExponential()
			: Easing()
		{}

		EasingExponential(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			if (t == 0.f)
				return from;
			else
				return offset * powf(2, 10 * (t/duration - 1)) + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			if (t >= duration)
				return from+offset;
			else
				return offset * (-powf(2, -10 * t/duration) + 1) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			if (t == 0.f)
				return from;

			if (t >= duration)
				return from+offset;

			t /= duration/2;
			if (t < 1)
				return offset/2 * powf(2, 10 * (t - 1)) + from;

			return offset/2 * (-powf(2, -10 * (t-1)) + 2) + from;
		}
	};

	class EasingQuad : public Easing
	{
	public:
		EasingQuad()
			: Easing()
		{}

		EasingQuad(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return offset*t*t+from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return -offset*t*(t-2)+from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration/2.f;
			if (t < 1)
				return offset/2*t*t + from;
			return -offset/2 * ((--t)*(t-2) - 1) + from;
		}
	};

	class EasingQuart : public Easing
	{
	public:
		EasingQuart()
			: Easing()
		{}

		EasingQuart(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return offset*t*t*t*t + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t = t/duration-1;
			return -offset * (t*t*t*t - 1) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration/2;
			if (t < 1)
				return offset/2*t*t*t*t + from;
			t -= 2;
			return -offset/2 * (t*t*t*t - 2) + from;
		}
	};

	class EasingQuint : public Easing
	{
	public:
		EasingQuint()
			: Easing()
		{}

		EasingQuint(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration;
			return offset*t*t*t*t*t+from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t = t/duration-1;
			return offset*(t*t*t*t*t+1)+from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			t /= duration/2;
			if (t < 1)
				return offset/2*t*t*t*t*t+from;
			t -= 2;
			return offset/2*(t*t*t*t*t+2)+from;
		}
	};

	class EasingSine : public Easing
	{
	public:
		EasingSine()
			: Easing()
		{}

		EasingSine(float from, float offset, float dur)
			: Easing(from, offset,dur)
		{}

		float EaseIn(float t)
		{
			t = Clamp(t, 0.f, duration);
			return -offset * cos(t/duration * (glm::pi<float>()/2)) + offset + from;
		}

		float EaseOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			return offset * sinf(t/duration * (glm::pi<float>()/2)) + from;
		}

		float EaseInOut(float t)
		{
			t = Clamp(t, 0.f, duration);
			return -offset/2 * (cos(glm::pi<float>()*t/duration) - 1) + from;
		}
	};
}

#endif

