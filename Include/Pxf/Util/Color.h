#ifndef _PXF_UTIL_COLOR_H_
#define _PXF_UTIL_COLOR_H_

#include <string>
#include <Pxf/Base/String.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Math/Math.h>

namespace Pxf
{
	class Color
	{
	protected:
		float range_min;
		float range_max;
		void update(float r, float g, float b, float a = 1.f);
	public:
		float r, g, b, a;

		struct rgb_t
		{
			float r, g, b;
		};

		struct hsl_t
		{
			float h, s, l;
		};

		struct hsv_t
		{
			float h, s, v;
		};

		Color()
			: range_min(0.f)
			, range_max(1.f)
			, r(0), g(0), b(0), a(1)
		{
			set_float(0.f, 0.f, 0.f);
		}

		Color(float g)
			: range_min(0.f)
			, range_max(1.f)
			, r(0), g(0), b(0), a(1)
		{
			set_float(g, g, g);
		}

		Color(char g)
			: range_min(0.f)
			, range_max(1.f)
			, r(0), g(0), b(0), a(1)
		{
			set_int8(g, g, g);
		}

		Color(float r, float g, float b, float a = 1.f)
			: range_min(0.f)
			, range_max(1.f)
			, r(0), g(0), b(0), a(1)
		{
			set_float(r, g, b, a);
		}

		Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255)
			: range_min(0.f)
			, range_max(1.f)
			, r(0), g(0), b(0), a(1)
		{
			set_int8(r, g, b, a);
		}

		~Color()
		{}

		void set_range(float min, float max);

		void set_float(float r, float g, float b, float a = 1.f);
		void set_int8(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);
		bool set_hex(const std::string& hex);

		// hue, saturation, value
		// h = [0,360], s = [0,1], v = [0,1]
		void set_hsv(float h, float s, float v);

		// hue, saturation, luminance
		// h = [0,360], s = [0,1], l = [0,1]
		void set_hsl(float h, float s, float l);

		static rgb_t hsl_to_rgb(hsl_t in);
		static rgb_t hsv_to_rgb(hsv_t in);
		static hsv_t rgb_to_hsv(rgb_t in);

		std::string as_hex()
		{
			unsigned char cr = TransformRange(r, range_min, range_max, 0.f, 255.f);
			unsigned char cg = TransformRange(g, range_min, range_max, 0.f, 255.f);
			unsigned char cb = TransformRange(b, range_min, range_max, 0.f, 255.f);
			return Pxf::Format("#%02X%02X%02X", cr, cg, cb);
		}

		glm::vec4 as_vec4()
		{
			return glm::vec4(r, g, b, a);
		}
	};
}

#endif // _PXF_UTIL_COLOR_H_