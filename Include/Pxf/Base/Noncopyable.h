#ifndef _PXF_BASE_NONCOPYABLE_H_
#define _PXF_BASE_NONCOPYABLE_H_

namespace Pxf {

// protect from unintended ADL
namespace Noncopyable_
{
	class Noncopyable
	{
	protected:
		Noncopyable(){};
		~Noncopyable(){};
	private:
		Noncopyable(const Noncopyable&);
		const Noncopyable& operator=(const Noncopyable&);
	};
}
typedef Noncopyable_::Noncopyable Noncopyable;

} // Pxf

#endif //_PXF_BASE_NONCOPYABLE_H_


