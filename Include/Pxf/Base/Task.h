#ifndef _PXF_BASE_TASK_H_
#define _PXF_BASE_TASK_H_

#include <Pxf/Base/Types.h>
#include <functional>
#include <atomic>

#include <mutex>
#include <condition_variable>

namespace Pxf
{
	class Channel;
	class Task
	{
	public:
		typedef std::function<void(Task& task)> fun_t;
		typedef std::function<void(Task& task, float progress)> progress_fun_t;
	private:
		fun_t on_complete_;
		fun_t on_start_;
		progress_fun_t on_progress_;
		std::atomic<float> progress_;

		std::mutex m_;
		std::condition_variable cond_;
		bool completed_;

		std::shared_ptr<Channel> channel_;

	public:
		Task() 
			: on_complete_(nullptr)
			, on_start_(nullptr)
			, on_progress_(nullptr)
			, progress_(0)
			, completed_(false)
			, channel_(nullptr)
		{
		}

		Task(const Task& t){};
		virtual ~Task() {}

		void join()
		{
			std::unique_lock<std::mutex> lk(m_);
			while(!completed_)
				cond_.wait(lk);
		}

		void run()
		{
			std::unique_lock<std::mutex> lk(m_);
			_run();
			cond_.notify_all();
		}

		virtual void _run() = 0;

		float get_progress() const { return progress_; }
		const std::shared_ptr<Channel>& get_channel() const { return channel_; }

		void set_channel(std::shared_ptr<Channel> channel)
		{
			channel_ = channel;
		}

		void set_progress_callback(progress_fun_t on_progress)
		{
			on_progress_ = on_progress;
		}

		void set_complete_callback(fun_t on_complete)
		{
			on_complete_ = on_complete;
		}

		void set_start_callback(fun_t on_start)
		{
			on_start_ = on_start;
		}

		void on_progress(float progress)
		{
			progress_ = progress;
			if (on_progress_ != nullptr)
				on_progress_(*this, progress);
		}

		void on_start()
		{
			if (on_start_ != nullptr)
				on_start_(*this);
		}

		void on_complete()
		{
			completed_ = true;
			if (on_complete_ != nullptr)
				on_complete_(*this);
		}
	};

	class FunTask : public Task
	{;
	protected:
		fun_t fun_;
	public:
		FunTask(fun_t in_fun)
			: fun_(in_fun)
		{}

		FunTask(const FunTask& t) : fun_(t.fun_)
		{ };

		~FunTask() {}

		virtual void _run()
		{
			if (fun_)
				fun_(*this);
		}
	};

	/*

	// FunTask example

	m_TaskPool = m_Kernel->GetTaskPool();
	unsigned FileLoadingGroup = m_TaskPool->create_channel_group();
	m_TaskPool->create_channel(FileLoadingGroup);
	m_TaskPool->create_channel(FileLoadingGroup);
	m_TaskPool->create_channel(FileLoadingGroup);

	Logger& l = m_Logger;	
	Task::fun_t fs = [&](Task& task)
	{ l.Debug("I'm about to do something 4 you!!"); };

	Task::fun_t f = [&](Task& task)
	{
	l.Debug("I'm SO BUSY RIGHT NOW!!");
	for( int i = 0; i < 800; i++)
	{
	Platform::ThreadSleep(10);
	task.on_progress(i / 799.f);
	}
	};

	Task::progress_fun_t fp = [&](Task& task, float progress)
	{
	l.Debug("I'm working on it!!! (%s, %d %%)", task.get_channel()->get_name().c_str(), int(progress * 100));
	};

	Task::fun_t fc = [&](Task& task)
	{ l.Debug("I did that for you!!"); };

	auto task = std::make_shared<FunTask>(f);
	task->set_start_callback(fs);
	task->set_complete_callback(fc);
	task->set_progress_callback(fp);

	auto task2 = std::make_shared<FunTask>(f);
	task2->set_start_callback(fs);
	task2->set_complete_callback(fc);
	task2->set_progress_callback(fp);

	auto task3 = std::make_shared<FunTask>(f);
	task3->set_start_callback(fs);
	task3->set_complete_callback(fc);
	task3->set_progress_callback(fp);

	m_TaskPool->add(FileLoadingGroup, task);
	m_TaskPool->add(FileLoadingGroup, task2);
	m_TaskPool->add(FileLoadingGroup, task3);
	task->join();
	task2->join();
	//m_TaskPool->join(FileLoadingGroup);
	*/
}

#endif // _PXF_BASE_TASK_H_