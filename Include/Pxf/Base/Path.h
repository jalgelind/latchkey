#ifndef _PXF_BASE_PATH_H_
#define _PXF_BASE_PATH_H_

/* TODO: More path operations */

#include <Pxf/Base/Types.h>
#include <vector>
#include <string>

struct stat;

namespace Pxf
{
	class Path
	{
	protected:
		bool m_Valid;
		std::string m_Path;
		static bool _dostat(const char* _path, struct stat* _stat);
		static bool _mkdir(const std::string& _path);
	public:
		Path();
		Path(const std::string& _path);

		void Set(const std::string& _path);

		std::string GetExt();
		std::string GetFileName();
		std::string GetPath();
		std::string Join(const std::string& path2) const;

		void Mkdir();
		std::string GetFullPath();

		int64 GetAccessTime();
		int64 GetModifiedTime();
		int64 GetCreationTime();

		bool IsDirectory();
		bool IsFile();
		bool IsValid();
		bool Exists();

		/* Static methods */

		/* Directory listing */
		struct FileEntry
		{
			enum Type { NONE, DIRECTORY, FILE };

			FileEntry() { type = NONE; }
			FileEntry(Type t, const std::string& _name) { type = t; name = _name; }

			Type type;
			std::string name;
			std::vector<FileEntry> children;
		};
		static std::vector<FileEntry> ListDirectory(const std::string& path, int levels = 1);


		static std::string GetExt(const std::string& _path);
		static std::string GetFileName(const std::string& _path);
		static std::string GetPath(const std::string& _path);
		static std::string Join(const std::string& path1, const std::string& path2);
		static void Mkdir(const std::string& _path);
		static int64 GetAccessTime(const std::string& _path);
		static int64 GetModifiedTime(const std::string& _path);
		static int64 GetCreationTime(const std::string& _path);

		static bool IsDirectory(const std::string& _path);
		static bool IsFile(const std::string& _path);
		static bool Exist(const std::string& _path);
	};
}

#endif

