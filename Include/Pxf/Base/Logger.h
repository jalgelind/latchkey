#ifndef _PXF_BASE_LOGGER_H_
#define _PXF_BASE_LOGGER_H_

#include <vector>
#include <string>
#include <map>
#include <memory>

namespace Pxf
{
	class Kernel;
	class FileStream;

	/*
		Logger(k, "gfx") -> target: default, identifier: gfx
		Logger(k, "sys.load") -> target: sys, identifier: load
		Logger(k, "sys.load.this") -> assertion
	*/
	class Logger
	{
	public:
		Kernel* m_Kernel;
		std::string m_Target;
		std::string m_Identifier;
		Logger();
		Logger(Kernel* _Kernel, const char* _Destination);
		~Logger() {}
		const char* GetTarget() const { return m_Target.c_str(); }
		const char* GetIdentifier() const { return m_Identifier.c_str(); }
		void Warning(const char* _Format, ...);
		void Error(const char* _Format, ...);
		void Information(const char* _Format, ...);
		void Debug(const char* _Format, ...);
	};

	struct LogMessage
	{
		Logger* source;
		std::string message;
		char type; // 'W', 'E', 'I', 'D'
	};

	class LoggingDevice
	{
	protected:
		bool enabled;
		bool force_flush;
		int linebreakhint;
	public:
		LoggingDevice();
		virtual ~LoggingDevice(){};
		
		void EnableForceFlush(bool enable)
		{ force_flush = enable; }
		
		void SetLinebreakHint(int length)
		{ linebreakhint = length;}

		void Enable(bool enable)
		{ enabled = enable; }

		bool IsEnabled() const { return enabled; }

		virtual void FormatPrefix(LogMessage* logmsg, std::string& prefix);

		virtual bool WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength) = 0;
		virtual bool WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength) = 0; // With line feed
		virtual void PreWrite(LogMessage* _Message) {};
		bool Write(LogMessage* _Message);
	};

	class StdLogger : public LoggingDevice
	{
	public:
		StdLogger() {}
		virtual ~StdLogger(){};
		virtual bool WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength);
		virtual bool WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength);
	};

	class FileLogger : public LoggingDevice
	{
	protected:
		// Not very efficient, should be transformed to an array
		// where all targets (currently std::string) also has a unique id.
		std::map<std::string, std::unique_ptr<FileStream>> _fss;
	public:
		FileStream* _fs;
		const char* _filename;
		FileLogger(const char* filename="output.log");
		virtual ~FileLogger();
		virtual void PreWrite(LogMessage* m);
		virtual bool WriteImpl(const char* _SrcBuffer, unsigned int _SrcLength);
		virtual bool WriteLImpl(const char* _SrcBuffer, unsigned int _SrcLength);
	};

}

#endif // _PXF_BASE_LOGGER_H_

