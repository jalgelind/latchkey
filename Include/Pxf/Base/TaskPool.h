#ifndef _PXF_BASE_TASKPOOL_H_
#define _PXF_BASE_TASKPOOL_H_

#include <Pxf/Base/Types.h>
#include <Pxf/Base/BlockingQueue.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Task.h>

#include <functional>
#include <string>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace Pxf
{
	typedef BlockingQueue<std::shared_ptr<Task>> TaskQueue;

	typedef int TaskChannelID;
    typedef int TaskGroupID;

	class Channel
	{
		std::unique_ptr<std::thread> thread_;
		std::shared_ptr<TaskQueue> queue_;
		std::string name_;

		std::mutex m_;
		std::condition_variable cv_;

		int id_;
		bool running_;
		bool processing_;
		int processed_;
		friend class TaskPool;
	public:
		std::string& get_name() { return name_; }
		int get_processed() { return processed_; }
		bool completed() { return processed_; }

		void join()
		{
			std::unique_lock<std::mutex> lk(m_);
			while(processing_)
				cv_.wait(lk);
		}

		void on_complete()
		{
			processed_++;
			cv_.notify_all();
		}

		Channel() : thread_(nullptr), running_(true), processed_(0), queue_(nullptr)
		{}

		~Channel()
		{
		}
	};

	class TaskPool
	{
	private:
		struct channel_group_t
		{
			unsigned id;
			TaskQueue* q;
		};

		std::vector<std::shared_ptr<Channel>> channels_;
		std::vector<std::shared_ptr<TaskQueue>> channel_groups_;
		std::mutex mutex_;
		Logger logger_;

		TaskPool(const TaskPool& other) {} // don't copy this...
	public:
		TaskPool();
		~TaskPool();

		static void channel_processor(TaskPool* pool, const std::shared_ptr<TaskQueue>& queue, const std::shared_ptr<Channel>& channel);

		TaskChannelID create_channel_group(); // use id for event queueing and destruction
		TaskChannelID create_channel(TaskGroupID group); // use id for destruction

		// destructing channels is tricky, since the channel might be busy processing something
		// we can cancel queues and wait for the channel to finish processing tasks, hmm.

		void join(TaskGroupID gid);

		// Add task to channel group
		void add(TaskGroupID gid, const std::shared_ptr<Task>& task);

		std::vector<std::shared_ptr<Channel>>& get_channels()
		{
			return channels_;
		}
	};
}

#endif // _PXF_BASE_TASKPOOL_H_