#ifndef _PXF_BASE_BLOCKINGQUEUE_H_
#define _PXF_BASE_BLOCKINGQUEUE_H_

#include <Pxf/Base/Debug.h>
#include <memory>
#include <deque>
#include <map>
#include <exception>

#include <mutex>
#include <condition_variable>
namespace Pxf
{
	template <typename T>
	class BlockingQueue
	{
	private:
		std::condition_variable can_provide_item_;
		std::mutex items_lock_;
		std::deque<T> items_;
		
		bool m_Canceled;

		int count()
		{
			return items_.size();
		}

		BlockingQueue(const BlockingQueue &other){} // no-copy

	public:

		struct CancellationException : public std::exception
		{
			CancellationException() {}
			const char* what() const throw() { return "I'm done waiting."; }
		};

		BlockingQueue()
			: m_Canceled(false)
		{}

		~BlockingQueue()
		{}

		void push(const T _Task)
		{
			std::unique_lock<std::mutex> g(items_lock_);
			items_.push_back(_Task);
			can_provide_item_.notify_one();
		}

		T pop()
		{
			std::unique_lock<std::mutex> g(items_lock_);
			while(count() == 0 && !m_Canceled)
				can_provide_item_.wait(g);

			if (m_Canceled)
				throw CancellationException();

			T ret = items_.front();
			items_.pop_front();
			return ret;
		}

		void acquire()
		{
			items_lock_.lock();
		}

		void release()
		{
			items_lock_.unlock();
		}

		std::deque<T>& get_array()
		{
			return items_;
		}

		void cancel()
		{
			m_Canceled = true;
			can_provide_item_.notify_all();
		}

	};
}
#endif // _BLOCKINGARRAY_H_