#ifndef _PXF_BASE_STRING_H_
#define _PXF_BASE_STRING_H_

#include <vector>
#include <string>
#include <cstdarg>

namespace Pxf
{
	std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
	std::vector<std::string> split(const std::string &s, char delim);
	std::string& ltrim(std::string &s);
	std::string& rtrim(std::string &s);
	std::string& trim(std::string &s);

	// String formatting
	std::string Format(const char *format, va_list ap);
	std::string Format(const char *format, ...);


	// String modification
	void StringToLower(std::string& str);
	void StringToUpper(std::string& str);

	// String find functions
	std::string::size_type StringFind2(const std::string& target, const char find, const char alt);
	std::string::size_type StringFindRev2(const std::string& target, const char find, const char alt);

	// String compare functions
	bool IsEqualI(const std::string& str1, const std::string& str2);
	bool IsPrefix(const std::string& str, const std::string& prefix);
	bool IsPrefixI(const std::string& str, const std::string& prefix);
	bool IsSuffix(const std::string& str, const std::string& suffix);
	bool IsSuffixI(const std::string& str, const std::string& suffix);

	// Char type identification
	bool IsWhitespace(const char c);
	bool IsAlpha(const char c);
	bool IsNumeric(const char c);
	bool IsAlphanumeric(const char c);

	// String convertion functions
	int StringToInteger(const std::string& number);
	double StringToDouble(const std::string& number);
}
#endif // _PXF_BASE_STRING_H_

