#ifndef _PXF_SYSTEM_H_
#define _PXF_SYSTEM_H_

#include <string> // TODO: forward declare

namespace Pxf
{
	// Forward declarations
	class Kernel;

	// TODO: automatically create a logger for each system?

	//! Abstract system
	class System
	{
	protected:
		Kernel* m_Kernel;
		const char* m_Identifier;
		unsigned m_SystemType;
		bool m_IsNull;
		virtual bool Init() = 0;
	public:

		enum SystemType
		{
			#define PXF_SYSTEM_TYPE(t, v) t = v,
			#include "System.def"
		};

		static SystemType repr(const std::string& str)
		{
			#define PXF_SYSTEM_TYPE(t, v) { if (str == #t) return (SystemType)v; }
			#include "System.def"
			return UNDEFINED;
		}

		System(Kernel* _Kernel, unsigned _SystemType, const char* _Identifier)
		{
			m_IsNull = false;
			m_Kernel = _Kernel;
			m_SystemType = _SystemType;
			m_Identifier = _Identifier;
		}

		virtual ~System(){};

		Kernel* GetKernel() const
		{
			return m_Kernel;
		}

		unsigned GetSystemType() const
		{
			return m_SystemType;
		}

		const char* GetIdentifier() const
		{
			return m_Identifier;
		}

		bool IsNull() const
		{
			return m_IsNull;
		}
	};
}

#endif // _PXF_SYSTEM_H_

