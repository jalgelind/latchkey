#ifndef _PXF_GRAPHICS_WINDOW_H_
#define _PXF_GRAPHICS_WINDOW_H_

#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Util/Queue.h>

#include <string>

namespace Pxf{
	namespace Graphics
	{
		class WindowDetails;
		class Window : public DeviceResource
		{
		public:
			struct Event
			{
				enum Type
				{
					#define WINDOW_EVENT_TYPE(ev) ev,
					#include "Window.def"
				};

				Type type; 
				int x, y;
				int width, height;
				bool focus;
				
				void Reset()
				{
					type = INVALID;
					x = 0, y = 0;
					width = 0, height = 0;
					focus = false;
				}

				Event() { Reset(); }
			};

			class EventQueue : public Queue<Event, 16>
			{};

		protected:
			EventQueue m_EventQueue;
			int m_Id;

		public:
			Window(GraphicsDevice* _Device)
				: DeviceResource(_Device, false /* _NeedsGLContext = false */)
				, m_Id(-1)
			{ }

			EventQueue& GetQueue()
			{
				return m_EventQueue;
			}

			virtual ~Window(){};
			virtual bool Open() = 0;
			virtual void Activate() = 0;
			virtual bool Close() = 0;
			virtual void Swap() = 0;

			void SetID(int id) { m_Id = id; }
			int GetID() { return m_Id; }

			virtual void SetTitle(const char *_title) = 0;
			virtual void SetWindowSize(int _Width, int _Height) = 0;

			virtual int GetFPS() = 0;
			virtual int GetWidth() = 0;
			virtual int GetHeight() = 0;
			virtual float GetAspectRatio() = 0;
			virtual const char* GetContextTypeName() = 0;

			virtual bool IsOpen() = 0;
			virtual bool IsMinimized() = 0;
			virtual bool IsActive() = 0;

			static std::string repr(Window::Event::Type ev)
			{
				#define WINDOW_EVENT_TYPE(name) { if (Window::Event::name == ev) return #name; }
				#include "Window.def"
				return "";
			}
		};

	} // Graphics
} // Pxf

#endif //_PXF_GRAPHICS_WINDOW_H_

