#ifndef _PXF_GRAPHICS_RECT_H_
#define _PXF_GRAPHICS_RECT_H_

namespace Pxf {
namespace Graphics {
		class Rect
		{
		public:
			float x, y, w, h;
			Rect()
			{}

			Rect(float x, float y, float w, float h)
				: x(x), y(y), w(w), h(h)
			{}

			bool operator == (const Rect &v) const { return ((v.x == x) && (v.y == y) && (v.w == w) && (v.h == h)); }
			bool operator != (const Rect &v) const { return !(*this == v); }

			// Splitting
			void HorizontalSplitFromTop(Rect& top, Rect& bottom, float pos);
			void HorizontalSplitFromBottom(Rect& top, Rect& bottom, float pos);
			void HorizontalSplitFromMiddle(Rect& top, Rect& bottom);
			void VerticalSplitFromLeft(Rect& left, Rect& right, float pos);
			void VerticalSplitFromRight(Rect& left, Rect& right, float pos);
			void VerticalSplitFromMiddle(Rect& left, Rect& right);

			// Margin
			void HorizontalMargin(Rect& new_rect, float margin);
			void VerticalMargin(Rect& new_rect, float margin);
			void Margin(Rect& new_rect, float margin);

			// Center inner_rect inside this rectangle
			void HorizontalCenter(Rect& inner_rect);
			void VerticalCenter(Rect& inner_rect);
			void Center (Rect& inner_rect);
		};
}}

#endif // _PXF_GRAPHICS_RECT_H_

