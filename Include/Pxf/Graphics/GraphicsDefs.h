#ifndef _PXF_GRAPHICS_GRAPHICSDEFS_H_
#define _PXF_GRAPHICS_GRAPHICSDEFS_H_

namespace Pxf
{
	namespace Graphics
	{
		enum TextureClampMethod
		{
			#define GRAPHICS_TEXTURE_CLAMP_METHOD(method) method,
			#include "GraphicsDefs.def"
		};

		enum TextureFilter
		{
			#define GRAPHICS_TEXTURE_FILTER(filter) filter,
			#include "GraphicsDefs.def"
		};

		enum TextureFormat
		{
			#define GRAPHICS_TEXTURE_FORMAT(fmt) fmt,
			#include "GraphicsDefs.def"
		};

		enum DeviceDataType
		{
			#define GRAPHICS_DATA_TYPE(type) type,
			#include "GraphicsDefs.def"
		};

		enum DeviceType
		{
			#define GRAPHICS_BACKEND(backend) backend,
			#include "GraphicsDefs.def"
		};

		/**
		 * VertexBuffer location types
		 * VB_LOCATION_SYS is stored in system memory
		 * VB_LOCATION_GPU is stored in GPU memory
		 */
		enum VertexBufferLocation
		{
			#define GRAPHICS_VERTEX_BUFFER_LOCATION(location) location,
			#include "GraphicsDefs.def"
		};

		/**
		 * VertexBuffer attributes
		 */
		enum VertexBufferAttribute
		{
			#define GRAPHICS_VERTEX_BUFFER_ATTRIBUTE(attrib, value) attrib = value,
			#include "GraphicsDefs.def"
		};

		/**
		 * Vertex buffer usage flags (used for GPU located memory)
		 */

		enum VertexBufferUsageFlag
		{
			#define GRAPHICS_VERTEX_BUFFER_USAGE_FLAG(flag) flag,
			#include "GraphicsDefs.def"
		};

		/**
		 * Vertex buffer access flags, specifies what type of access when
		 * mapping gpu memory data.
		 */
		enum VertexBufferAccessFlag
		{
			#define GRAPHICS_VERTEX_BUFFER_ACCESS_FLAG(flag) flag,
			#include "GraphicsDefs.def"
		};

		/**
		 * Vertex buffer primitive types.
		 */
		enum VertexBufferPrimitiveType {
			#define GRAPHICS_VERTEX_BUFFER_PRIMITIVE_TYPE(type) type,
			#include "GraphicsDefs.def"
		};

		/**
		 *	Framebuffer stuff
		 */

		enum FrameBufferAttachmentType {
			#define GRAPHICS_FBO_ATTACHMENT_TYPE(type) type,
			#include "GraphicsDefs.def"
		};

		/*	Depth comparison function types, names match those found in the OpenGL api.
			There should exist similar functions in directx, albeit with different syntax.
			http://www.toymaker.info/Games/html/render_states.html	*/

		enum DepthFuncType {
			DF_NEVER,		// never pass
			DF_ALWAYS,		// always pass
			DF_LESS,		// Passes if the incoming depth value is less than the stored depth value.
			DF_EQUAL,		// Passes if the incoming depth value is equal to the stored depth value.
			DF_LEQUAL,		// Passes if the incoming depth value is less than or equal to the stored depth value.
			DF_GREATER,		// Passes if the incoming depth value is greater than the stored depth value.
			DF_NOTEQUAL,	// Passes if the incoming depth value is not equal to the stored depth value.
			DF_GEQUAL		// Passes if the incoming depth value is greater than or equal to the stored depth value.
		};
	}
}

#endif

