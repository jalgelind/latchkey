#pragma once

#include <memory>

// TODO: Implement a generic state saver that covers the most common
// graphics states.

namespace Pxf {
namespace Graphics {

class GraphicsDevice;
class Texture;
class FrameBuffer;
class VertexBuffer;
class RenderBuffer;

class BoundTextureStateSaver
{
protected:
	GraphicsDevice& dev;
	const std::shared_ptr<Texture>& old;
public:
	BoundTextureStateSaver(GraphicsDevice& device);
	~BoundTextureStateSaver();
};

class BoundFrameBufferStateSaver
{
protected:
	GraphicsDevice& dev;
	const std::shared_ptr<FrameBuffer>& old;
public:
	BoundFrameBufferStateSaver(GraphicsDevice& device);
	~BoundFrameBufferStateSaver();
};

}}