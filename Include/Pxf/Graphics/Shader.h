#ifndef _PXF_GRAPHICS_SHADER_H_
#define _PXF_GRAPHICS_SHADER_H_

#include <Pxf/Base/Debug.h>
#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Math/Math.h>

namespace Pxf {
	namespace Graphics {

		class Shader : public Graphics::DeviceResource
		{
		protected:
			bool m_Valid;
		public:
			Shader(GraphicsDevice* _pDevice)
				: Graphics::DeviceResource(_pDevice)
				, m_Valid(false)
			{}

			virtual ~Shader(){};

			virtual void Bind() = 0;

			virtual void BindAttributeLocation(unsigned _Index, const char* _Name) = 0;
			virtual void SetUniformi(const char* _name, int _value) = 0;
			virtual void SetUniformf(const char* _name, float _value) = 0;
			virtual void SetUniformVec2(const char* _name, const glm::vec2& _value) = 0;
			virtual void SetUniformVec3(const char* _name, const glm::vec3& _value) = 0;
			virtual void SetUniformVec3v(const char* _name, unsigned count, const glm::vec3& _value) = 0;
			virtual void SetUniformVec4(const char* _name, const glm::vec4& _value) = 0;
			virtual void SetUniformVec4v(const char* _name, unsigned count, const glm::vec4& _value) = 0;
			virtual void SetUniformMat4(const char* _name, const glm::mat4& _value) = 0;

			bool IsValid() const { return m_Valid; }
		};
	}
}


#endif // _PXF_GRAPHICS_SHADER_H_

