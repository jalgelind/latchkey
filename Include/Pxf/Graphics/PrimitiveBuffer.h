#ifndef _PXF_GRAPHICS_PRIMITIVEBUFFER_H_
#define _PXF_GRAPHICS_PRIMITIVEBUFFER_H_

#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/VertexBuffer.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Base/Utils.h>
#include <Pxf/Util/Color.h>

/*
	Persistant alternative to PrimitiveBatch.

	This is currently only for 2D primitives.

	Implemented primitives: Quads, Lines, Points

	TODO: Triangles
*/

namespace Pxf {
	namespace Graphics {
		template <typename T>
		class PrimitiveBuffer : public DeviceResource
		{
		public:
			struct Vertex
			{
				glm::vec3 pos;
				glm::vec2 tex;
				glm::vec4 color;
			};
		protected:
			int m_VertexBufferPos;
			std::shared_ptr<VertexBuffer> m_VertexBuffer;
			unsigned int m_VertexBufferSize;
			VertexBufferPrimitiveType m_PrimitiveType;
			VertexBufferUsageFlag m_UsageFlag;
			unsigned int m_PrimitiveVertices;

			std::shared_ptr<VertexBuffer> Create(unsigned size, VertexBufferUsageFlag usage)
			{
				auto buff = m_pDevice->CreateVertexBuffer(VB_LOCATION_GPU, usage);
				buff->CreateNewBuffer(size, sizeof(Vertex));
				buff->SetData(VB_VERTEX_DATA, 0, 3);
				buff->SetData(VB_TEXCOORD0_DATA, sizeof(glm::vec3), 2);
				buff->SetData(VB_COLOR_DATA, sizeof(glm::vec2)+sizeof(glm::vec3), 4);
				return buff;
			}

			void _Resize(unsigned size)
			{
				auto newvb = Create(size, m_UsageFlag);
				Vertex* olddata = (Vertex*)m_VertexBuffer->MapData(VB_ACCESS_READ_ONLY);
				Vertex* newdata = (Vertex*)newvb->MapData(VB_ACCESS_WRITE_ONLY);
				for(int i = 0; i < m_VertexBufferSize; i++)
					newdata[i] = olddata[i];
				newvb->UnmapData();
				m_VertexBuffer->UnmapData();
				m_VertexBuffer = newvb;
				m_VertexBufferSize = size;
			}
			PrimitiveBuffer(){};
		public:
			PrimitiveBuffer(GraphicsDevice* _pDevice, unsigned size, VertexBufferUsageFlag usage = VB_USAGE_STATIC_DRAW)
				: DeviceResource(_pDevice)
				, m_VertexBuffer(nullptr)
				, m_VertexBufferPos(0)
				, m_PrimitiveVertices(0)
				, m_PrimitiveType(VB_PRIMITIVE_NONE)
				, m_UsageFlag(usage)
			{
				m_PrimitiveVertices = sizeof(T) / sizeof(Vertex);
				m_VertexBufferSize = size * m_PrimitiveVertices;
				switch (m_PrimitiveVertices)
				{
				case 4: m_PrimitiveType = VB_PRIMITIVE_QUADS; break;
				case 3: m_PrimitiveType = VB_PRIMITIVE_TRIANGLES; break;
				case 2: m_PrimitiveType = VB_PRIMITIVE_LINES; break;
				case 1: m_PrimitiveType = VB_PRIMITIVE_POINTS; break;
				}

				m_VertexBuffer = Create(m_VertexBufferSize, m_UsageFlag);
			}

			virtual ~PrimitiveBuffer()
			{
			}

			void Resize(unsigned size)
			{
				_Resize(size * m_PrimitiveVertices);
			}

			unsigned int Size() const
			{
				return m_VertexBufferSize;
			}

			unsigned int Used() const
			{
				return m_VertexBufferPos;
			}

			void Clear()
			{
				Vertex* vs = (Vertex*)m_VertexBuffer->MapData(VB_ACCESS_WRITE_ONLY);
				for(int i = 0; i < m_VertexBufferSize; i++)
					vs[i].color.a = 0.f; // invisible
				m_VertexBuffer->UnmapData();
				m_VertexBufferPos = 0;
			}

			void Update(unsigned int pos, const std::vector<T>& data)
			{
				unsigned int T_size = sizeof(T);
				unsigned int vertices = data.size() * m_PrimitiveVertices;

				// Dosn't fit? Resize.
				if ((pos + vertices) > m_VertexBufferSize)
				{
					_Resize(m_VertexBufferSize * 2);
					Update(pos, data);
					return;
				}

				if (!data.empty())
					m_VertexBuffer->UpdateData((void*)&data[0], data.size() * T_size, pos * T_size);
				pos *= m_PrimitiveVertices;
				if (pos + vertices > m_VertexBufferPos)
					m_VertexBufferPos = pos + vertices;
			}

			T* MapData(VertexBufferAccessFlag access)
			{
				return (T*)m_VertexBuffer->MapData(access);
			}

			void UnmapData()
			{
				m_VertexBuffer->UnmapData();
			}

			void Draw(int offset = 0, int count = -1)
			{
				offset *= m_PrimitiveVertices;
				if (count == -1)
					count = m_VertexBufferPos;
				else
					count = Min(Max(0, m_VertexBufferPos - offset), count * m_PrimitiveVertices);
				m_VertexBuffer->SetPrimitive(m_PrimitiveType);
				m_pDevice->Translate(glm::vec3(0.375, 0.375, 0.));
				if (count > 0)
					m_pDevice->DrawBuffer(m_VertexBuffer, offset, count);
				m_pDevice->Translate(glm::vec3(-0.375, -0.375, 0.));
			}

		}; // PrimitiveBuffer


		//
		// Primitives
		//

		struct Quad
		{
			// DO NOT ADD MORE DATA FIELDS: This must remain a POD type containing 4 vertices.
			PrimitiveBuffer<Quad>::Vertex m_Vertices[4];
			void SetColor(Color color);
			void SetColor(Color color_tl, Color color_tr, Color color_bl, Color color_br);
			void SetDepth(float z);
			void SetTextureCoordinates(glm::vec2 tl, glm::vec2 br);
			void SetTopLeft(Rect rect, float rotation);
			void SetCenter(Rect rect, float rotation);
		};

		struct Line
		{
			// DO NOT ADD MORE DATA FIELDS
			PrimitiveBuffer<Line>::Vertex m_Vertices[2];
			void SetColor(Color color);
			void SetColor(Color color_from, Color color_to);
			void SetDepth(float z);
			void Set(glm::vec2 from, glm::vec2 to);
		};

		struct Point
		{
			// DO NOT ADD MORE DATA FIELDS
			PrimitiveBuffer<Point>::Vertex m_Vertices[1];
			void SetColor(Color color);
			void SetDepth(float z);
			void Set(glm::vec2 pos);
		};

		class QuadBuffer : public PrimitiveBuffer<Quad>
		{
		public:
			QuadBuffer(GraphicsDevice* _pDevice, unsigned size)
				: PrimitiveBuffer(_pDevice, size) {}
			~QuadBuffer(){};
		};

		//
		// TODO: these need to be tested
		//
		class LineBuffer : public PrimitiveBuffer<Line>
		{
		public:
			LineBuffer(GraphicsDevice* _pDevice, unsigned size)
				: PrimitiveBuffer(_pDevice, size) {}
		};

		class PointBuffer : public PrimitiveBuffer<Point>
		{
		public:
			PointBuffer(GraphicsDevice* _pDevice, unsigned size)
				: PrimitiveBuffer(_pDevice, size) {}
		};

	} // Graphics
} // Pxf

#endif // _PXF_GRAPHICS_PRIMITIVEBUFFER_H_

