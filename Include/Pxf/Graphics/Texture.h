#ifndef _PXF_GRAPHICS_TEXTURE_H_
#define _PXF_GRAPHICS_TEXTURE_H_

#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Graphics/GraphicsDefs.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Math/Math.h>

namespace Pxf
{
	namespace Resource
	{
		class Image;
	}
	namespace Graphics
	{
		//! Abstract texture class
		class Texture : public DeviceResource
		{
		public:
			Texture(GraphicsDevice* _pDevice)
				: DeviceResource(_pDevice)
			{
				m_pDevice->_AddTexture(this);
			}

			virtual ~Texture()
			{
				m_pDevice->_RemoveTexture(this);
			};

			virtual bool IsValid() = 0;

			virtual void SetMipmapHint(bool on) = 0;
			virtual void SetPOTHint(bool on) = 0;

			virtual void CreateEmpty(int _width, int _height, TextureFormat _format) = 0;
			virtual void LoadData(const std::shared_ptr<Resource::Image>& _img) = 0;
			virtual void Unload() = 0;
			virtual void Reload() = 0;

			virtual void Bind(unsigned texture_unit = 0) = 0;

			//virtual const unsigned char* GetData();

			virtual int GetWidth() = 0;
			virtual int GetHeight() = 0;
			virtual int GetChannels() = 0;

			virtual void SetMagFilter(TextureFilter _Filter) = 0;
			virtual void SetMinFilter(TextureFilter _Filter) = 0;

			virtual void SetClampMethod(TextureClampMethod _Method) = 0;

			virtual unsigned int GetTextureID() = 0;

			virtual bool HasMipmaps() = 0;

			virtual std::shared_ptr<Resource::Image> ToImage() = 0;

			virtual glm::vec4 CreateTextureSubset(float _x1, float _y1, float _x2, float _y2) = 0;
		};
	} // Graphics
} // Pxf

#endif // _PXF_GRAPHICS_TEXTURE_H_

