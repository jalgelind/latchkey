#ifndef _PXF_GRAPHICS_WINDOWSPECIFICATIONS_H_
#define _PXF_GRAPHICS_WINDOWSPECIFICATIONS_H_

namespace Pxf
{
	namespace Graphics
	{
		struct WindowDetails
		{
			int Width;
			int Height;
			int XPos;
			int YPos;
			int ColorBits;
			int AlphaBits;
			int DepthBits;
			int StencilBits;
			int FSAASamples;
			bool Fullscreen;
			bool Resizeable;
			bool VerticalSync;
			void* ParentWindow; /* When we're not running stand-alone. e.g in a VST-plugin. */
			WindowDetails()
			{
				Width = 640;
				Height = 480;
				XPos = 100;
				YPos = 100;
				ColorBits = 8;
				AlphaBits = 8;
				DepthBits = 8;
				StencilBits = 0;
				FSAASamples = 0;
				Fullscreen = false;
				Resizeable = false;
				VerticalSync = true;
				ParentWindow = 0;
			}
		};
	} // Graphics
} // Pxf

#endif // _PXF_GRAPHICS_WINDOWSPECIFICATIONS_H_

