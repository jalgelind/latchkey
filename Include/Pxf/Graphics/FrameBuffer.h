#ifndef _PXF_GRAPHICS_FRAMEBUFFEROBJECT_H_
#define _PXF_GRAPHICS_FRAMEBUFFEROBJECT_H_

#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <vector>

namespace Pxf
{
	namespace Graphics
	{
		class Texture;
		class RenderBuffer;
		class FrameBuffer : public DeviceResource
		{
		public:
	
			FrameBuffer(GraphicsDevice* _Device)
				: DeviceResource(_Device)
				, m_NumColorAttachment(0)
				, m_MaxColorAttachments(0)
				, m_UseStencilAttachment(false)
				, m_UseDepthAttachment(false)
				, m_AttachmentMask(0)
			{ }

			virtual ~FrameBuffer(){};

			virtual void Attach(const std::shared_ptr<Texture>& _Texture, const FrameBufferAttachmentType _Attachment, bool _GenMipmaps) = 0;
			virtual void Attach(const std::shared_ptr<RenderBuffer>& _Buffer, const FrameBufferAttachmentType _Attachment) = 0;
			virtual void Detach(const FrameBufferAttachmentType _Attachment) = 0;

			virtual void Bind() = 0;

			int GetNumColorAttachment() { return m_NumColorAttachment; }
			bool GetUseDepthAttachment() { return m_UseDepthAttachment; }
			bool GetUseStencilAttachment() { return m_UseStencilAttachment; }

			bool IsComplete() { return m_Complete; }
		protected:
			int m_MaxColorAttachments;	// upper bound on attachments, vendor specific
			int m_NumColorAttachment;

			short unsigned m_AttachmentMask;

			bool m_UseDepthAttachment;
			bool m_UseStencilAttachment;
			bool m_Complete;
		};
	} // Graphics
} // Pxf

#endif // _PXF_GRAPHICS_FRAMEBUFFEROBJECT_H_

