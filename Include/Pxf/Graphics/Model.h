#ifndef _PXF_GRAPHICS_MODEL_H_
#define _PXF_GRAPHICS_MODEL_H_

#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Graphics/VertexBuffer.h>

namespace Pxf {
	namespace Resource
	{
		class Mesh;
	}
	namespace Graphics {

		class Model : public Graphics::DeviceResource
		{
		public:
			struct Vertex
			{
				glm::vec3 vertex;
				glm::vec3 normal;
				glm::vec2 texcoord;
			};
		protected:
			std::shared_ptr<VertexBuffer> m_VertexBuffer;
			unsigned m_VertexCount;
			unsigned m_TriangleCount;
			Logger m_Logger;
			virtual bool Init() { return true; }
		public:
			Model(GraphicsDevice* _pDevice);
			virtual ~Model();

			virtual bool Load(const char* _FilePath);
			virtual bool Load(const std::shared_ptr<Resource::Mesh>& _Data);

			virtual bool Unload();
			virtual void Draw();

			VertexBuffer* GetVertexBuffer() { return m_VertexBuffer.get(); }
		};
	}
}


#endif // _PXF_GRAPHICS_MODEL_H_

