#ifndef _PXF_GRAPHICS_GRAPHICSDEVICE_H_
#define _PXF_GRAPHICS_GRAPHICSDEVICE_H_

#include <Pxf/Base/Types.h>
#include <Pxf/System.h>
#include <Pxf/Math/Math.h>
#include <Pxf/Graphics/GraphicsDefs.h>
#include <Pxf/Graphics/Rect.h>
#include <vector>
#include <memory>

namespace Pxf
{
	class Kernel;

	namespace Resource
	{
		class Image;
		class Mesh;
		class GLSL;
		class Font;
		class ResourceManager;
	}

	namespace Graphics
	{
		class Window;
		class WindowDetails;
		class VertexBuffer;
		class Texture;
		class RenderBuffer;
		class FrameBuffer;
		class PrimitiveBatch;
		class QuadBuffer;
		class Shader;
		class Model;
		class TextProvider;
		class Prepared;

		class GraphicsDevice : public System
		{
		protected:
			uint64 m_TextureMemoryBytes, m_VBOMemoryBytes;

			// TODO: move to GraphicsDefs.def
			enum BlendMode
			{
				BLEND_NONE,
				BLEND_ADDITIVE,
				BLEND_NORMAL
			};
			BlendMode blend_mode_;

			Rect viewport_;
			glm::mat4 m_ModelViewMatrix;
			glm::mat4 m_ProjectionMatrix;
			std::shared_ptr<Graphics::Window> m_Window;
			std::shared_ptr<Texture> m_DebugTexture;
			std::vector<Texture*> m_ActiveTextures;
			std::shared_ptr<FrameBuffer> m_CurrentFrameBuffer;
			std::shared_ptr<Shader> m_CurrentShader;
			std::shared_ptr<Texture> m_BindHistory[16];

			Resource::ResourceManager* m_Res;

		public:
			GraphicsDevice(Pxf::Kernel* _Kernel, const char* _Identifier);
			virtual ~GraphicsDevice();

			// Device information

			uint64 GetTextureMemoryUsage();
			uint64 GetVBOMemoryUsage();

			void _UpdateTextureMemoryUsage(int64 delta);
			void _UpdateVBOMemoryUsage(int64 delta);

			// Keep track of all loaded textures
			void _AddTexture(Texture* _tex);
			void _RemoveTexture(Texture* _tex);

			virtual uint64 GetMemorySize() = 0;
			virtual uint64 GetMemoryUsage() = 0;
			virtual uint64 GetDedicatedMemory() = 0;
			virtual uint64 GetEvictedMemory() = 0;
			virtual unsigned GetEvictionCount() = 0;

			virtual const char* GetVendor() = 0;
			virtual const char* GetRenderer() = 0;
			virtual const char* GetVersion() = 0;
			virtual const char* GetShaderLangVersion() = 0;

			// Windowing
			virtual std::shared_ptr<Window> OpenWindow(WindowDetails& _pWindowSpecs) = 0; // CreateWindow is a windows define
			virtual const std::shared_ptr<Window>& GetActiveWindow() = 0;
			virtual const std::shared_ptr<Graphics::Window> GetWindow(int id) = 0;
			virtual void SetActiveWindow(const std::shared_ptr<Window>& _Window) = 0;

			// Device
			virtual DeviceType GetDeviceType() = 0;

			// Graphics
			virtual void SetProjectionMatrix(glm::mat4& _matrix) = 0;
			virtual glm::mat4 GetProjectionMatrix() = 0;
			virtual void PushProjectionMatrix() = 0;
			virtual void PopProjectionMatrix() = 0;

			virtual void SetModelViewMatrix(glm::mat4& _matrix) = 0;
			virtual glm::mat4 GetModelViewMatrix() = 0;
			virtual void PushModelMatrix() = 0;
			virtual void PopModelMatrix() = 0;

			virtual void SetViewport(int _x, int _y, int _w, int _h) = 0;
			virtual void SetViewport(Graphics::Rect rect) = 0;
			virtual Graphics::Rect GetViewport() = 0;

			virtual void PushViewportAttribute() = 0;
			virtual void PopAttribute() = 0;

			virtual void Translate(glm::vec3 _translate) = 0;
			virtual void Scale(glm::vec3 _scale) = 0;
			virtual void SwapBuffers() = 0;
			virtual void SetDepthFunction(DepthFuncType _DepthType) = 0;
			virtual bool SetDepthTest(bool _On) = 0;
			virtual void SetCullFace(bool _On) = 0;

			virtual void Clear(float r, float g, float b, float a) = 0;
			virtual void Clear(float r, float g, float b) { Clear(r, g, b, 0.f); }
			virtual void SetBlendNone() = 0;
			virtual void SetBlendAdditive() = 0;
			virtual void SetBlendNormal() = 0;

			// Clipping
			virtual void SetClip(int x, int y, int w, int h) = 0;
			void SetClip(Rect& rect) { SetClip(rect.x, rect.y, rect.w, rect.h); }
			virtual void SetNoClip() = 0;

			// Font
			std::shared_ptr<TextProvider> CreateTextProvider(const std::shared_ptr<Resource::Font>& _font, int _fontsize, bool use_distance_fields = false);

			// Texture
			const std::shared_ptr<Graphics::Texture>& GetDebugTexture() { return m_DebugTexture; }
			const std::shared_ptr<Graphics::Texture>& GetBoundTexture(unsigned int _texture_unit = 0) { return m_BindHistory[_texture_unit]; }
			void DumpTexturesToDisk(const std::string& outdir = "debug");
			virtual std::shared_ptr<Graphics::Texture> CreateEmptyTexture(int _Width,int _Height, TextureFormat _Format = TEX_FORMAT_RGBA) = 0;
			virtual std::shared_ptr<Graphics::Texture> CreateTexture(const char* _filepath, bool force_pot = false) = 0;
			virtual std::shared_ptr<Graphics::Texture> CreateTexture(const std::shared_ptr<Resource::Image>& _Image, bool force_pot = false) = 0;
			virtual std::shared_ptr<Graphics::Texture> CreateTextureFromData(unsigned char* _datachunk, int _width, int _height, int _channels, bool force_pot = false) = 0;
			virtual std::shared_ptr<Graphics::Texture> BindTexture(const std::shared_ptr<Graphics::Texture>& _texture, unsigned int _texture_unit = 0) = 0; // Multi-texturing

			virtual std::shared_ptr<Graphics::Texture> CreateTextureFromFramebuffer() = 0;

			virtual std::shared_ptr<Resource::Image> CreateImageFromTexture(const std::shared_ptr<Texture>& _texture) = 0;

			virtual std::shared_ptr<VertexBuffer> CreateVertexBuffer(VertexBufferLocation _VertexBufferLocation, VertexBufferUsageFlag _VertexBufferUsageFlag) = 0;
			virtual void DrawBuffer(const std::shared_ptr<VertexBuffer>& _pVertexBuffer, unsigned _VertexOffset, unsigned _VertexCount) = 0;

			// Model
			virtual std::shared_ptr<Model> CreateModel(const char* _FilePath);
			virtual std::shared_ptr<Model> CreateModel(const std::shared_ptr<Resource::Mesh>& _Mesh);

			// Buffers
			virtual std::shared_ptr<RenderBuffer> CreateRenderBuffer(unsigned _Format, unsigned _Width, unsigned _Height) = 0;
			virtual void BindRenderBuffer(const std::shared_ptr<RenderBuffer>& _pRenderBuffer) = 0;

			const std::shared_ptr<FrameBuffer>& GetActiveFrameBuffer() { return m_CurrentFrameBuffer; }
			virtual std::shared_ptr<FrameBuffer> CreateFrameBuffer() = 0;
			virtual std::shared_ptr<Graphics::FrameBuffer> BindFrameBuffer(const std::shared_ptr<FrameBuffer>& _pFrameBuffer) = 0;

			std::shared_ptr<QuadBuffer> CreateQuadBuffer(unsigned size);
			std::shared_ptr<PrimitiveBatch> CreatePrimitiveBatch();

			virtual float SetLineWidth(float v) = 0;
			virtual float SetPointSize(float v) = 0;

			// Shaders
			const std::shared_ptr<Shader>& GetActiveShader() { return m_CurrentShader; }
			virtual std::shared_ptr<Shader> CreateShader(const char* _Ident, const std::shared_ptr<Resource::GLSL>& _Shader) = 0;
			virtual std::shared_ptr<Shader> CreateShader(const char* _Ident, const char* _Shader) = 0;
			virtual std::shared_ptr<Shader> CreateShaderFromPath(const char* _Ident, const char* _ShaderPath) = 0;
			virtual std::shared_ptr<Shader> BindShader(const std::shared_ptr<Shader>& _Shader) = 0;
		};
	} // Graphics
} // Pxf

#endif // _PXF_GRAPHICS_GRAPHICSDEVICE_H_

