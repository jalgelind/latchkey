#ifndef _PXF_GRAPHICS_TEXTPROVIDER_H_
#define _PXF_GRAPHICS_TEXTPROVIDER_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Timer.h>
#include <Pxf/Graphics/GraphicsDevice.h>
#include <Pxf/Graphics/DeviceResource.h>
#include <Pxf/Graphics/PrimitiveBuffer.h>
#include <Pxf/Graphics/Texture.h>
#include <Pxf/Graphics/Rect.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Util/ColorBank.h>

#include <string>
#include <map>
#include <vector>


namespace Pxf {
namespace Graphics {
	class Texture;
	class Shader;

	class TextProvider : public DeviceResource
	{
	private:
		Resource::Font* m_Font;
		std::vector<Resource::Font::CharInfo_t> m_FontCharInfo;
		std::vector<Resource::Font::KerningPair_t> m_KerningPairs;
		int m_FontSize;
		std::shared_ptr<Graphics::Texture> m_Texture;
		std::shared_ptr<Graphics::Shader> m_DFShader; 
		Graphics::TextureFilter m_MinFilter;
		Graphics::TextureFilter m_MagFilter;
		bool m_UseDistanceFields;

		float m_OffsetX;
		float m_OffsetY;

		std::shared_ptr<Window> m_Window;
		Logger m_Logger;
		ColorBank m_ColorBank;
		std::string m_DefaultColor;
		friend class String;
		friend class Batch;
	public:

		class String;
		class Batch;

		enum Symbols
		{
			SmbSetColor = 1,
			SmbSetAttrib,
			//SmbAttribBlink,
			SmbAttribNone,
			SmbEnd,
		};

		struct QuadSet
		{
			std::vector<Quad> quads;
			Rect rect;
		};

		TextProvider(GraphicsDevice* _pDevice, Resource::Font* _Font, int _FontSize, bool use_sdf = false);
		~TextProvider();

		void SetDefaultColor(std::string color);
		void SetDrawOffset(float dx, float dy);
		void DefineColor(const std::string& color, unsigned char r, unsigned char g, unsigned char b) { m_ColorBank.add(color, r, g, b); }
		bool _TranslateColor(std::string& color, unsigned char* r, unsigned char* g, unsigned char* b);
		void Reload();
	
		short GetKerning(const unsigned char _First, const unsigned char _Second);

		// Formatted text -> QuadSet
		QuadSet BuildQuads(const std::string& _Text, bool _UseKerning, int xoffset = 0, int yoffset = 0);

		void DrawTextBuffer(QuadBuffer& qb, int _X, int _Y, float _Scale);
		
		std::shared_ptr<String> CreateString(const char* text);
		std::shared_ptr<Batch> CreateBatch();
		
		void SetTextureFilters(TextureFilter _MinFilter, TextureFilter _MagFilter);

		std::string& GetDefaultColor()
		{
			return m_DefaultColor;
		}

		const std::shared_ptr<Texture>& GetTexture()
		{
			return m_Texture;
		}

		const Resource::Font* GetFont() const
		{
			return m_Font;
		}

		bool IsReady() const;

		/*
			TextProvider::Parser
		*/

		class Parser
		{
		protected:
			TextProvider* m_TextProvider;
			int m_Length;
			Logger m_Logger;
		public:
			Parser(TextProvider* _TextProvider);
			bool Parse(const std::string& source, std::string& dest);
			int GetLength() { return m_Length; }
		};

		/*
			TextProvider::String
		*/

		class String
		{
		public:
			GraphicsDevice* m_pDevice;
			TextProvider* m_TextProvider;
			Logger m_Logger;
			bool m_KerningOn;
			bool m_Valid;
			int m_Revision;
			int m_MaxLength;
			std::string m_PreparedData;
			std::string m_DefaultColor;
			TextProvider::QuadSet m_QuadSet;
			std::shared_ptr<QuadBuffer> m_QuadBuffer;
			TextProvider::Batch* m_TextBatch;
			int m_XOffset;
			int m_YOffset;
			Parser m_Parser;


			String(GraphicsDevice* _pDevice, TextProvider* _TextProvider, TextProvider::Batch* _Batch = nullptr, int xoffset = 0, int yoffset = 0);
			virtual ~String();

			void SetKerning(bool _On);
			void SetDefaultColor(const std::string& color);
			void SetPosition(int _XOffset, int _YOffset);
			void SetMaxLength(int _Length);
			void Set(std::string text);
			void SetF(std::string fmt, ...);
			bool Build(const char* text);

			TextProvider::QuadSet& GetQuads() { return m_QuadSet; }
			int GetRevision() { return m_Revision; }

			void Print(float _X, float _Y, float _Scale);
		};

		/*
			TextProvider::Batch
		*/

		class Batch
		{
		protected:
			GraphicsDevice* m_pDevice;
			TextProvider* m_TextProvider;
			Logger m_Logger;
			bool m_Valid;
			QuadBuffer* m_QuadBuffer;
			int m_CurrentBufferPos;
			struct BatchString
			{
				std::shared_ptr<TextProvider::String> str; // const&?
				int revision; // string revision stored in quadbuffer
				int offset;   // string offset into quadbuffer
				int length;   // string length allocated in quadbuffer
			};
			std::vector<BatchString> m_Strings;
		public:
			Batch(GraphicsDevice* _pDevice, TextProvider* _Font);
			virtual ~Batch();
			std::shared_ptr<TextProvider::String> CreateString(int x, int y, int maxlength, const std::string& text);
			/*
			void SetPosition(const std::shared_ptr<TextProvider::String>& str, int x, int y);
			void SetMaxLength(const std::shared_ptr<TextProvider::String>& str, int length);
			*/

			void Draw();
		};


	}; // TextProvider	

} // Graphics
} // Pxf

#endif // _PXF_GRAPHICS_TEXTPROVIDER_H_