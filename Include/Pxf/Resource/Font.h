#ifndef _PXF_RESOURCE_FONT_H_
#define _PXF_RESOURCE_FONT_H_

#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <string>
#include <vector>

/*
	TODO: Distance fields for supersmooth font rendering. :)
*/

namespace Pxf {
namespace Resource {
	class Image;
	class Font : public ResourceBase
	{
	public:
		struct CharInfo_t
		{
			float Tx1, Ty1, Tx2, Ty2;
			float Width, Height;
			float XOffset, YOffset;
			float XAdvance;
		};

		struct KerningPair_t
		{
			char first;
			char second;
			short offset;
		};

		struct FontInfo
		{
			std::string name;
			bool size_restriction;
			int size_px;
			int line_height;
			int newline_offset;
			FontInfo()
			{
				name = "unknown";
				line_height = 12;
				size_restriction = true;
				size_px = 12;
				newline_offset = 0;
			}
		};
	protected:
		Image* m_Image;
		FontInfo m_FontInfo;
		std::vector<CharInfo_t> m_CharInfo;
		std::vector<KerningPair_t> m_KerningPairs;
		bool m_Initialized;
	public:
		Font(Kernel* _Kernel, Chunk* _Chunk, ResourceLoader* _Loader)
			: ResourceBase(_Kernel, _Chunk, _Loader)
			, m_CharInfo(0)
			, m_Image(0)
			, m_Initialized(false)
		{}

		virtual ~Font()
		{}

		virtual bool Build() = 0;

		inline const std::vector<KerningPair_t> GetKerningPairs() const
		{
			return m_KerningPairs;
		}

		inline const std::vector<CharInfo_t> GetCharInfo() const
		{
			return m_CharInfo;
		}

		inline const FontInfo& GetInfo() const
		{
			return m_FontInfo;
		}

		// Create font map image
		virtual std::shared_ptr<Resource::Image> CreateImage(int _FontSize) = 0;

		virtual bool IsReady() const
		{
			return m_Initialized && m_CharInfo.size() > 0;
		}
	};

	class FontLoader : public Resource::ResourceLoader
	{
	private:
		bool Init(){ return true; }
	public:
		FontLoader(Pxf::Kernel* _Kernel, const char* _Identifier)
			: ResourceLoader(_Kernel, _Identifier)
		{}
		virtual ~FontLoader() {};
		virtual Resource::Font* Load(const char* _FilePath) = 0;
		virtual Resource::Font* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0) = 0;
		virtual void Destroy(void* _Resource)
		{
			if (_Resource)
				delete (Resource::Font*)_Resource;
		}
	};

} // Resource
} // Pxf

#endif //_PXF_RESOURCE_FONT_H_

