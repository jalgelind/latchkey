#ifndef _PXF_RESOURCE_GLSL_H_
#define _PXF_RESOURCE_GLSL_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>

#include <string>

/*
	Simplified glsl support. Both the vertex and the fragment shader is included in the same file.

	Format:
	------
	varying common_variables;

	#vertexshader

	void main() { vertex shader }

	#fragmentshader

	void main() { fragment shader }

*/

namespace Pxf {
	namespace Resource
	{
		class GLSL : public Resource::ResourceBase
		{
		protected:
			std::string m_VertexShader;
			std::string m_FragmentShader;
			virtual bool Build();
		public:
			GLSL(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
				: Resource::ResourceBase(_Kernel, _Chunk, _Loader)
			{
				Build();
			}

			virtual ~GLSL();

			const char* GetVertexShader() const
			{
				return m_VertexShader.c_str();
			}

			const char* GetFragmentShader() const
			{
				return m_FragmentShader.c_str();
			}

			bool IsReady() const
			{
				return m_Chunk != 0;
			}

			int64 GetMemoryUsage()
			{
				int64 sum = 0;
				if (m_Chunk && !m_Chunk->is_static)
					sum += m_Chunk->size;
				return sizeof(GLSL) + sizeof(Chunk) + sum + m_VertexShader.capacity() + m_FragmentShader.capacity();
			}
		};


		class GLSLLoader : public Resource::ResourceLoader
		{
		private:
			Logger m_Logger;
			bool Init(){ return true; }
		public:
			GLSLLoader(Pxf::Kernel* _Kernel);
			~GLSLLoader();
			virtual Resource::GLSL* Load(const char* _FilePath);
			virtual Resource::GLSL* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
			virtual void Destroy(void* _Resource)
			{
				if (_Resource)
					delete (Resource::GLSL*)_Resource;
			}
		};
	}
}

#endif // _PXF_RESOURCE_GLSL_H_

