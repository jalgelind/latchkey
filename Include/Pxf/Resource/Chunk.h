#ifndef _PXF_RESOURCE_CHUNK_H_
#define _PXF_RESOURCE_CHUNK_H_

#include <Pxf/Base/Types.h>
#include <Pxf/Base/Noncopyable.h>
#include <string>

namespace Pxf {
namespace Resource {

	class Chunk : public Noncopyable
	{
	public:
		void* data;
		unsigned size;
		int64 last_modified;
		std::string source;
		bool is_static;

		Chunk()
			: data(0)
			, size(0)
			, last_modified(0)
			, source("")
			, is_static(false)
		{}

		~Chunk()
		{
			if (!is_static)
			{
				if (data)
					delete [] (char*)data;
				data = 0;
				size = 0;
			}
		};
	};

	Chunk* LoadFile(const std::string& _FilePath, const char* openmode="rb");

} // Resource
} // Pxf

#endif //_PXF_RESOURCE_CHUNK_H_


