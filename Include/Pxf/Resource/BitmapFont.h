#ifndef _PXF_RESOURCE_BITMAPFONT_H_
#define _PXF_RESOURCE_BITMAPFONT_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/Font.h>

namespace Pxf {
namespace Resource {

	class BitmapFont : public Resource::Font
	{
	protected:
		Logger m_Logger;
		virtual void Cleanup();
		std::shared_ptr<Pxf::Resource::Image> m_Image;
	public:
		BitmapFont(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader);
		virtual bool Build();
		virtual std::shared_ptr<Image> CreateImage(int _FontSize);
		virtual void Reload();
		virtual ~BitmapFont();
		
		int64 GetMemoryUsage();
	};

	class BitmapFontLoader : public Resource::FontLoader
	{
	private:
		Logger m_Logger;
		bool Init();
	public:
		BitmapFontLoader(Pxf::Kernel* _Kernel);
		virtual ~BitmapFontLoader();
		virtual Resource::Font* Load(const char* _FilePath);
		virtual Resource::Font* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
	};

} // Resource
} // Pxf

#endif //_PXF_RESOURCE_BITMAPFONT_H_

