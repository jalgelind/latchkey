#ifndef _PXF_RESOURCE_JSON_H_
#define _PXF_RESOURCE_JSON_H_

#include <json/json.h>

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>

#include <vector>
#include <map>
#include <string>


namespace Pxf
{
	namespace Resource
	{
		class Chunk;

		class Json : public Resource::ResourceBase
		{
		protected:
			Logger m_Logger;
			::Json::Reader m_Reader;
			::Json::Value m_Root;
			virtual bool Build();
		public:
			Json(Kernel* _Kernel, Chunk* _Chunk, ResourceLoader* _Loader);
			~Json();
			virtual ::Json::Value& GetRoot()
			{
				return m_Root;
			}

			virtual void SetRoot(::Json::Value& _Value)
			{
				m_Root = _Value;
			}

			virtual bool SaveToDisk(const char* _FilePath);

			virtual bool IsReady() const
			{
				return true;
			}

			int64 GetMemoryUsage()
			{
				int64 sum = 0;
				if (m_Chunk && !m_Chunk->is_static)
					sum += m_Chunk->size;
				return sizeof(Json) + sizeof(Chunk) + sum;
			}
		};

		class JsonLoader : public Resource::ResourceLoader
		{
		private:
			Logger m_Logger;
			bool Init();
		public:
			JsonLoader(Pxf::Kernel* _Kernel);
			virtual ~JsonLoader();
			virtual Resource::Json* Load(const char* _FilePath);
			virtual Resource::Json* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
			virtual Resource::Json* CreateEmpty();
			virtual void Destroy(void* _Resource)
			{
				if (_Resource)
					delete (Resource::Json*)_Resource;
			}
		};
	} // Resource
} // Pxf

#endif //_PXF_RESOURCE_JSONLOADER_H_

