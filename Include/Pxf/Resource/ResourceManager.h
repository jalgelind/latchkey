#ifndef _PXF_RESOURCE_RESOURCEMANAGER_H_
#define _PXF_RESOURCE_RESOURCEMANAGER_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Base/Debug.h>
#include <Pxf/Base/Path.h>
#include <Pxf/Base/Platform.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/Chunk.h>
#include <Pxf/Base/String.h>

#include <Pxf/Resource/Chunk.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/Json.h>
#include <Pxf/Resource/GLSL.h>
#include <Pxf/Resource/Mesh.h>
#include <Pxf/Resource/Sound.h>
#include <Pxf/Resource/Raw.h>

#include <string>
#include <map>
#include <list>
#include <vector>
#include <functional>

// TODO: Too much implementation details in this file.
//		 Try to de-template a few functions.

namespace Pxf {
	class TaskPool;
namespace Resource
{
	class ResourceBase;
	class ResourceDepender;
	class ResourceManager
	{
	private:
		struct DataBlob
		{
			const unsigned char* data;
			unsigned size;
			int64 last_modified;
		};
		Kernel* m_Kernel;
		Logger m_Logger;
		std::map<std::string, ResourceLoader*>* m_ResourceLoaders;
		std::map<std::string, std::shared_ptr<ResourceBase>>* m_LoadedResources;
		std::map<std::string, DataBlob*>* m_CachedFiles;

#ifdef CONF_DEBUG
		struct HistoryEntry
		{
			std::string path_on_disk;
			std::string path_used;
			bool operator==(const HistoryEntry& other)
			{ return path_on_disk == other.path_on_disk; }
		};
		std::vector<HistoryEntry> m_DiskFileHistory; // all files loaded from disk during execution (in debug mode)
#endif
		TaskPool* m_TaskPool;
		unsigned int m_FileLoadingChannelGroup;
		friend class ResourceBase;

		template <typename T>
		T* FindResourceLoader(const char* _Ext)
		{
			auto loaderit = m_ResourceLoaders->find(_Ext);
			if (loaderit != m_ResourceLoaders->end())
				return (T*)loaderit->second;
			else
				return NULL;
		}

	public:
		ResourceManager(Kernel* _Kernel);
		~ResourceManager();

		void Initialize();
		void Shutdown();

		bool CanLoad(const std::string& fext)
		{
			std::string ext = fext;
			if (ext.size() > 0 && ext[0] == '.') ext = ext.c_str() + 1;
			auto it = m_ResourceLoaders->find(ext);
			return it != m_ResourceLoaders->end();
		}

		void RegisterResourceLoader(const char* _Ext, Resource::ResourceLoader* _ResourceLoader);

		void RegisterCachedFile(const char* _Path, const unsigned char* _DataPtr, unsigned _Size, int64 _LastModified);
		bool HasCachedFile(const std::string& _FilePath);
		void ExpandFileCache();

		template <typename ResourceType>
		ResourceType* GetResource(const char* _Path)
		{
			if(!_Path) return NULL;

			ResourceBase* resource = NULL;
			auto resit = m_LoadedResources->find(_Path);
			if(resit == m_LoadedResources->end())
			{
				m_Logger.Warning("Resource '%s' does not exist", _Path);
				return NULL;
			}

			resource = (ResourceBase*) resit->second;

			return (ResourceType*) resource;
		}

		void CheckForUpdatedResources(std::function<void(ResourceBase&)> cb);

		void DumpResourceLoaders();
		void DumpMemoryUsage();
		unsigned GetMemoryUsage();


		std::shared_ptr<ResourceBase> GetStaticResource(ResourceLoader* loader, const char* _FilePath);
		std::shared_ptr<ResourceBase> GetLoadedResource(ResourceLoader* loader, const char* _FilePath);
		std::shared_ptr<ResourceBase> GetFileResource(ResourceLoader* loader, const char* _FilePath);

		template <typename ResourceType>
		std::shared_ptr<ResourceType> Acquire(const char* _FilePath, const char* _ForcedExt = 0)
		{
			const char* _orig_file_path = _FilePath;
			std::shared_ptr<ResourceBase> resource = nullptr;
			ResourceLoader* loader = NULL;

			// Find loader based on file extension.
			std::string fileext;
			if (_ForcedExt == 0)
				fileext = Path::GetExt(_FilePath);
			else
				fileext = _ForcedExt;

			auto loaderit = m_ResourceLoaders->find(fileext);
			if (loaderit != m_ResourceLoaders->end())
				loader = loaderit->second;
			else
			{
				m_Logger.Warning("No supported loader available for type '%s'", fileext.c_str());
				return NULL;
			}

			// Look for resource...
			std::string filepath = _FilePath;
			bool res = m_Kernel->GetResourcePath(_FilePath, filepath);
			_FilePath = filepath.c_str();

			// Find resource
			resource = GetLoadedResource(loader, _FilePath);
			if (!resource)
			{
				resource = GetFileResource(loader, _FilePath);
#ifdef CONF_DEBUG
				// record to history
				if (resource)
				{
					HistoryEntry ent;
					ent.path_on_disk = _FilePath;
					ent.path_used = _orig_file_path;
					if (std::find(begin(m_DiskFileHistory), end(m_DiskFileHistory), ent) == end(m_DiskFileHistory))
						m_DiskFileHistory.push_back(ent);
				}
#endif
				if (!resource)
					resource = GetStaticResource(loader, _FilePath);
				if (resource)
					m_LoadedResources->insert(std::make_pair(_FilePath, resource));
			}
			else
			{
				if (!resource->GetChunk())
					resource->Load();
			}

			return std::dynamic_pointer_cast<ResourceType>(resource);
		}

		template <typename ResourceType>
		void Release(const std::shared_ptr<ResourceType>& _Resource, bool _Purge = false)
		{
			if (_Resource)
			{
				std::string source = _Resource->GetSource();
				auto iter = m_LoadedResources->find(source);
				if (iter != m_LoadedResources->end())
				{
					// Hm, _Purge cannot work when using shared_ptrs.
					if (_Purge)
						(iter->second).reset();
					m_LoadedResources->erase(iter);
				}
			}
		}

		// --
		std::shared_ptr<Image> CreateImageFromFile(const char* path);
		std::shared_ptr<Image> CreateImageFromData(const void* data, unsigned len, const char* fakepath = 0);
		std::shared_ptr<Image> CreateImageFromRaw(int _Width, int _Height, int _Channels, unsigned char* _DataPtr, const char* _Path = 0);
		std::shared_ptr<GLSL> CreateShaderFromData(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
		std::shared_ptr<Json> CreateJsonDocument();
		/* TODO: More factory methods */
		//--
	};

} // Resource
} // Pxf

#endif // _PXF_RESOURCE_RESOURCEMANAGER_H_

