#ifndef _PXF_RESOURCE_TEXT_H_
#define _PXF_RESOURCE_TEXT_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>

namespace Pxf {
namespace Resource
{
	class Raw : public Resource::ResourceBase
	{
	protected:
		virtual bool Build();
	public:
		Raw(Kernel* _Kernel, Resource::Chunk* _Chunk, Resource::ResourceLoader* _Loader)
			: Resource::ResourceBase(_Kernel, _Chunk, _Loader)
		{
			Build();
		}

		virtual ~Raw();

		char* Ptr() const
		{
			return (char*)m_Chunk->data;
		}

		unsigned Length() const
		{
			return m_Chunk->size;
		}

		bool IsReady() const
		{
			return m_Chunk != 0;
		}

		int64 GetMemoryUsage()
		{
			int64 sum = 0;
			if (m_Chunk && !m_Chunk->is_static)
				sum += m_Chunk->size;
			return sizeof(Raw) + sizeof(Chunk) + sum;
		}
	};


	class RawLoader : public Resource::ResourceLoader
	{
	private:
		Logger m_Logger;
		bool Init(){ return true; }
	public:
		RawLoader(Pxf::Kernel* _Kernel);
		~RawLoader();
		virtual Resource::Raw* Load(const char* _FilePath);
		virtual Resource::Raw* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0);
		virtual void Destroy(void* _Resource)
		{
			if (_Resource)
				delete (Resource::Raw*)_Resource;
		}
	};
}
}

#endif // _PXF_RESOURCE_TEXT_H_

