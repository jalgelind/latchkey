#ifndef _PXF_RESOURCE_SOUND_H_
#define _PXF_RESOURCE_SOUND_H_

#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>

namespace Pxf {
namespace Resource {
	class Sound : public ResourceBase
	{
	public:
		enum Format {FMT_PCM16, FMT_PCM24, FMT_FLOAT};
	protected:
		float m_Length;
		int m_Channels;
		int m_SampleRate;
		Format m_Format;

		char* m_SoundData;
		unsigned m_SoundDataLen;

	public:
		Sound(Kernel* _Kernel, Chunk* _Chunk, ResourceLoader* _Loader)
			: ResourceBase(_Kernel, _Chunk, _Loader)
			, m_Length(0.f)
			, m_Channels(0)
			, m_SampleRate(0)
			, m_SoundData(NULL)
			, m_SoundDataLen(0)
		{}
		virtual ~Sound()
		{}

		const float Length() const
		{
			return m_Length;
		}

		const unsigned int Channels() const
		{
			return m_Channels;
		}

		const int SampleRate() const
		{
			return m_SampleRate;
		}

		const Format Format() const
		{
			return m_Format;
		}

		char* DataPtr() const
		{
			return m_SoundData;
		}

		unsigned DataLen() const
		{
			return m_SoundDataLen;
		}

		virtual bool IsReady() const
		{
			return m_SoundData != NULL;
		}

		int64 GetMemoryUsage()
		{
			int64 sum = 0;
			if (m_Chunk && !m_Chunk->is_static)
				sum += m_Chunk->size;
			if (m_SoundData)
				sum += m_SoundDataLen * sizeof(short) * m_Channels;
			return sizeof(Sound) + sizeof(Chunk) + sum;
		}
	};

	class SoundLoader : public Resource::ResourceLoader
	{
	private:
		bool Init(){ return true; }
	public:
		SoundLoader(Pxf::Kernel* _Kernel, const char* _Identifier)
			: ResourceLoader(_Kernel, _Identifier)
		{}
		virtual ~SoundLoader() {};
		virtual Resource::Sound* Load(const char* _FilePath) = 0;
		virtual Resource::Sound* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0) = 0;
		virtual void Destroy(void* _Resource)
		{
			if (_Resource)
				delete (Resource::Sound*)_Resource;
		}
	};

} // Resource
} // Pxf

#endif //_PXF_RESOURCE_SOUND_H_

