#ifndef _PXF_RESOURCE_RESOURCEBASE_H_
#define _PXF_RESOURCE_RESOURCEBASE_H_

#include <Pxf/Kernel.h>
#include <Pxf/Base/Types.h>
#include <Pxf/Base/Platform.h>
#include <Pxf/Base/Logger.h>
#include <Pxf/Resource/Chunk.h>
#include <Pxf/Resource/ResourceDepender.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <algorithm>
#include <vector>
#include <string>
#include <list>

namespace Pxf {
namespace Resource {
	class ResourceBase : public Noncopyable
	{
	protected:
		Kernel* m_Kernel;
		Chunk* m_Chunk;
		Logger m_Logger;
		std::string m_Source;
		int m_References;
		int64 m_LastModified;
		ResourceLoader* m_Loader;
		std::list<std::shared_ptr<ResourceDepender>> m_Dependers;
		virtual bool Build() pure;
		friend class ResourceManager;
		friend class ResourceDepender;
	public:
		ResourceBase(Kernel* _Kernel, Chunk* _Chunk, ResourceLoader* _Loader)
			: m_Kernel(_Kernel)
			, m_Chunk(_Chunk)
			, m_Logger(_Kernel->GetLogger("res"))
			, m_Loader(_Loader)
			, m_References(0)
		{
			if (m_Chunk)
			{
				m_LastModified = _Chunk->last_modified;
				m_Source = _Chunk->source;
			}
			else
				m_LastModified = 0;
		}
		virtual ~ResourceBase()
		{
			if (m_Chunk)
				delete m_Chunk;
		};

		void Load();
		void Unload();
		virtual bool IsUpdated();

		void ReloadDependers();
		void AddDependency(const std::shared_ptr<ResourceDepender>& _dep);
		void RemoveDependency(const std::shared_ptr<ResourceDepender>& _dep);

		virtual bool IsReady() const pure;

		const char* GetSource() const
		{
			return m_Source.c_str();
		}

		Chunk* GetChunk() const
		{
			return m_Chunk;
		}

		int64 GetLastModified() const
		{
			return m_LastModified;
		}

		void _AddRef()
		{
			m_References++;
		}

		void _DeRef()
		{
			m_References--;
		}

		virtual int64 GetMemoryUsage() = 0;

		virtual std::string ToString()
		{
			return "";
		}
	};

} // Resource
} // Pxf

#endif //_PXF_RESOURCE_RESOURCEBASE_H_


