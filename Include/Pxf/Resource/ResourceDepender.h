#ifndef _PXF_RESOURCE_RESOURCEDEPENDER_H_
#define _PXF_RESOURCE_RESOURCEDEPENDER_H_

#include <vector>

namespace Pxf {
	class Kernel;
	namespace Resource
	{
		class ResourceBase;

		class ResourceDepender
		{
		public:
			ResourceDepender()
			{}

			virtual ~ResourceDepender()
			{};

			virtual void Reload() {};
		};
	} // Resource
} // Pxf

#endif //_PXF_RESOURCE_RESOURCEDEPENDER_H_


