#ifndef _PXF_RESOURCE_RESOURCE_H_
#define _PXF_RESOURCE_RESOURCE_H_

#include <Pxf/Resource/Chunk.h>
#include <Pxf/Resource/Font.h>
#include <Pxf/Resource/Image.h>
#include <Pxf/Resource/Json.h>
#include <Pxf/Resource/GLSL.h>
#include <Pxf/Resource/Mesh.h>
#include <Pxf/Resource/Sound.h>
#include <Pxf/Resource/Raw.h>

#include <Pxf/Resource/ResourceBase.h>
#include <Pxf/Resource/ResourceLoader.h>
#include <Pxf/Resource/ResourceManager.h>


#endif // #ifndef _PXF_RESOURCE_RESOURCE_H
