#ifndef _PXF_RESOURCE_RESOURCELOADER_H_
#define _PXF_RESOURCE_RESOURCELOADER_H_

#include <Pxf/System.h>
#include <Pxf/Resource/Chunk.h>

namespace Pxf {
namespace Resource
{
	class Chunk;
	class ResourceDepender;
	class ResourceBase;

	class ResourceLoader : public Pxf::System
	{
	public:
		ResourceLoader(Pxf::Kernel* _Kernel, const char* _Identifier)
			: Pxf::System(_Kernel, Pxf::System::RESOURCE_LOADER, _Identifier)
		{}

		virtual ~ResourceLoader(){};

		virtual ResourceBase* Load(const char* _FilePath) = 0;
		virtual ResourceBase* CreateFrom(const void* _DataPtr, unsigned _DataLen, const char* _Path = 0) = 0;
		virtual void Destroy(void* _Resource) = 0;
	};
} // Resource
} // Pxf

#endif // _PXF_RESOURCE_RESOURCELOADER_H_

