#ifndef _PXF_PXF_H_
#define _PXF_PXF_H_

namespace Pxf
{
	void Init(int argc, const char** argv);
	void InitModules();
	void Shutdown();
}

#endif
