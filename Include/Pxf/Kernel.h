#ifndef _PXF_KERNEL_H_
#define _PXF_KERNEL_H_

#include <Pxf/Base/Logger.h>
#include <Pxf/Base/SharedLibrary.h>
#include <Pxf/System.h>
#include <Pxf/Module.h>

#include <memory>
#include <map>
#include <vector>
#include <string>

namespace Pxf {
	class TaskPool;
	namespace Audio { class AudioDevice; }
	namespace Input { class InputDevice;
	                  class InputController; }
	namespace Graphics { class GraphicsDevice; }
	namespace Resource { class ResourceManager;
						 class ResourceLoader; }
	namespace Network { class NetworkDevice; }

	class Configuration;

	// Licence information structures
	struct third_party_libraries_t
	{
	    const char* name;
	    const char* license;
	};

	struct license_information_t {
	    const char* license;
	    third_party_libraries_t* third_party;
	    int third_party_size;
	};

	class Kernel
	{
	public:
		enum ExeType { EXE_NATIVE, EXE_VST};
	private:
		static Kernel* s_Kernel;

		typedef void(*DestroyModuleInstance_fun)(Pxf::Module*);
		struct ModuleEntry_t
		{
			std::shared_ptr<Pxf::SharedLibrary> dynlib;
			std::shared_ptr<Pxf::Module> module;
			DestroyModuleInstance_fun destroy;
			unsigned int filter;
			ModuleEntry_t(unsigned int _Filter, const std::shared_ptr<Pxf::Module>& _Module, 
			              DestroyModuleInstance_fun _Fun)
			{
				filter = _Filter;
				module = _Module;
				destroy = _Fun;
				dynlib = 0;
			}
			ModuleEntry_t(unsigned int _Filter, const std::shared_ptr<Pxf::SharedLibrary>& _Library, 
			              const std::shared_ptr<Pxf::Module>& _Module, DestroyModuleInstance_fun _Fun)
			{
				filter = _Filter;
				module = _Module;
				destroy = _Fun;
				dynlib = _Library;
			}
		};
		std::vector<std::shared_ptr<ModuleEntry_t>> m_AvailableModules;
		std::vector<std::shared_ptr<LoggingDevice>> m_LoggingDevices;

		Logger m_KernelLogger;
		std::shared_ptr<StdLogger> m_StdLogger;
		std::shared_ptr<FileLogger> m_FileLogger;

		std::string m_PathOfExecutable;
		std::string m_CommandLine;
		std::vector<std::string> m_CommandLineArguments;
		ExeType m_ExecutableType;

		std::unique_ptr<Audio::AudioDevice> m_AudioDevice;
		std::unique_ptr<Input::InputDevice> m_InputDevice;
		std::unique_ptr<Graphics::GraphicsDevice> m_GraphicsDevice;
		std::unique_ptr<Resource::ResourceManager> m_ResourceManager;
		std::unique_ptr<Network::NetworkDevice> m_NetworkDevice;
		std::unique_ptr<TaskPool> m_TaskPool;
		std::map<std::string, std::vector<std::string> > m_ResourcePaths;
		std::unique_ptr<Configuration> m_Configuration;

		Kernel();

	public:
		static const unsigned KERNEL_VERSION;

		~Kernel();

		static Kernel* GetInstance()
		{
			if (!s_Kernel)
				s_Kernel = new Kernel();
			return s_Kernel;
		}

		const std::string& GetPathOfExecutable();
		const std::string& GetCommandLineArgumentString();
		const std::vector<std::string>& GetCommandLineArguments();
		const std::unique_ptr<Configuration>& GetConfiguration() const;

		void AddResourcePath(const std::string& path, const std::string& ext = "*");
		std::vector<std::string> GetResourcePaths(const std::string& ext = "*");
		bool GetResourcePath(const std::string& filename, std::string& result);

		void RegisterAudioDevice(std::unique_ptr<Audio::AudioDevice> _Device);
		Audio::AudioDevice* GetAudioDevice();

		void RegisterInputController(std::unique_ptr<Input::InputController> _Controller);
		Input::InputDevice* GetInputDevice();

		void RegisterGraphicsDevice(std::unique_ptr<Graphics::GraphicsDevice> _Device);
		Graphics::GraphicsDevice* GetGraphicsDevice();

		void RegisterNetworkDevice(std::unique_ptr<Network::NetworkDevice> _Device);
		Network::NetworkDevice* GetNetworkDevice();

		void RegisterResourceLoader(const char* _Ext, Resource::ResourceLoader* _ResourceLoader);
		Resource::ResourceManager* GetResourceManager();

		TaskPool* GetTaskPool() { return m_TaskPool.get(); }

		Logger GetLogger(const char* _Identifier);
		void RegisterLoggingDevice(const std::shared_ptr<LoggingDevice>& _logger);
		void UnregisterLoggingDevice(const std::shared_ptr<LoggingDevice>& _logger);
		void LoggingDeviceBroadcast(LogMessage* _Message);

		LoggingDevice* GetFileLogger() { return m_FileLogger.get(); }
		LoggingDevice* GetStdoutLogger() { return m_StdLogger.get(); }
		
		static unsigned int GetKernelVersion()
		{
			return KERNEL_VERSION;
		}

        static const license_information_t* GetLicenseInformation();

		ExeType GetExeType() const
		{
			return m_ExecutableType;
		}

		void SetExeType(ExeType t)
		{
			m_ExecutableType = t;
		}

		bool RegisterModule(const char* _FilePath, unsigned _Filter, bool _OverrideBuiltin = false);
		bool RegisterModule(Pxf::Module* _Module);
		bool SetModuleFilter(const char* _ModuleName, unsigned _Filter);
		void InitializeModules();
		void Initialize(std::string exepath, std::vector<std::string> args);
		void DumpAvailableModules();
	}; // class Kernel

} // namespace Pxf

#endif // _PXF_KERNEL_H_

