#ifndef _PXF_AUDIO_AUDIOCALLBACK_H_
#define _PXF_AUDIO_AUDIOCALLBACK_H_

namespace Pxf {
namespace Audio {
	class AudioDevice;

	class AudioCallback
	{
	protected:
		AudioDevice* m_AudioDevice;
	public:
		//
		// TODO: Should use std::enable_if or something to add the right
		//       channel-write functions at compile time.
		// TODO: Should support double processing and maybe others.
		enum BufferFormat { EBuffersSeparate, EBuffersInterleaved };
		BufferFormat m_BufferFormat;

		AudioCallback(AudioDevice* _device)
			: m_AudioDevice(_device)
			, m_BufferFormat(EBuffersInterleaved)
		{}

		virtual ~AudioCallback()
		{}

		// _in: input buffer for chained effect processing (intended for audio plugins, it can be NULL)
		// _out: output buffer with _channels with _size samples
		virtual void Mix(float* _in, float* _out, unsigned _channels, unsigned _size) = 0;

		void SetBufferFormat(BufferFormat f)
		{
			m_BufferFormat = f;
		}

		BufferFormat GetBufferFormat() const
		{
			return m_BufferFormat;
		}
	};
}}

#endif // _PXF_AUDIO_AUDIOCALLBACK_H_
