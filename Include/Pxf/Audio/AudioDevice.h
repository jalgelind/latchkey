#ifndef _PXF_AUDIODEVICE_AUDIODEVICE_H_
#define _PXF_AUDIODEVICE_AUDIODEVICE_H_

#include <Pxf/Base/Types.h>
#include <Pxf/System.h>
#include <Pxf/Audio/AudioCallback.h>
#include <memory>

namespace Pxf
{
	class Kernel;
	namespace Resource
	{
		class Sound;
	}

	namespace Audio
	{
		class AudioDevice : public Pxf::System
		{
		protected:
		unsigned int m_PreferredBufferSize;
		unsigned int m_SamplingRate;
		unsigned int m_MaxActiveVoices;
		std::unique_ptr<AudioCallback> m_AudioCallback;

		public:
		AudioDevice(Kernel* _Kernel, const char* _Identifier)
			: Pxf::System(_Kernel, Pxf::System::AUDIO_DEVICE, _Identifier)
			, m_PreferredBufferSize(1024)
			, m_SamplingRate(44100)
			, m_MaxActiveVoices(8)
		{}
		virtual ~AudioDevice()
		{}

		virtual bool Initialize() = 0;
		virtual void Shutdown() = 0;
	
		virtual void SetAudioCallback(std::unique_ptr<AudioCallback> _callback)
		{
			m_AudioCallback = std::move(_callback);
		}

		AudioCallback* GetAudioCallback() const
		{
			return m_AudioCallback.get();
		}
		
		virtual void SetPreferredBufferSize(unsigned _PreferredBufferSize)
		{
			m_PreferredBufferSize = _PreferredBufferSize;
		}

		virtual void SetSamplingRate(unsigned _SamplingRate)
		{
			m_SamplingRate = _SamplingRate;
		}

		virtual unsigned GetSamplingRate() const
		{
			return m_SamplingRate;
		}

		// For the default one-shot engine
		virtual void SetMaxActiveVoices(unsigned _MaxActiveVoices)
		{
			m_MaxActiveVoices = _MaxActiveVoices;
		}

		/* Primitive play-back engine, for one-shot effects.
		   Used with the default mixer callback. */
		virtual int RegisterSound(const char* _Filename) = 0;
		virtual int RegisterSound(const std::shared_ptr<Resource::Sound>& _Sound) = 0;
		virtual int GetSoundID(const std::shared_ptr<Resource::Sound>& _Sound) = 0;
		virtual void UnregisterSound(int _Id) = 0;
		inline void UnregisterSound(const std::shared_ptr<Resource::Sound>& _Sound)
		{UnregisterSound(GetSoundID(_Sound));}
		virtual void Play(int _SoundID, bool _Loop = false) = 0;
		virtual void Stop(int _SoundID) = 0;
		virtual void StopAll() = 0;
		virtual void Pause(int _SoundID) = 0;
		virtual void PauseAll() = 0;
		virtual void DumpInfo() = 0;
		};
	} // Audio
} // Pxf

#endif // _PXF_AUDIODEVICE_AUDIODEVICE_H_

