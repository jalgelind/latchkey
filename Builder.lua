
-- global config
NOLIBREBUILD = false -- never rebuild third party librares


-- bam build script
path_prefix = PathDir(ModuleFilename())
package.path = package.path .. string.format(';%s/?.lua', path_prefix)

require 'utils'

function load_library(name)
    Import(path_prefix .. "/Libraries/" .. name .. "/" .. name .. ".lua")
    return library;
end

function load_module(name)
    Import(path_prefix .. "/Modules/" .. name .. "/" .. name .. ".lua")
    return module
end

function join_paths(...)
    local res = Path("")
    for i, p in ipairs({...}) do
        res = PathJoin(res, p)
    end
    return res
end

function join_root(...)
    return join_paths(path_prefix, ...)
end

function transform_output(settings, input)
    return join_root(join_paths("Build", settings.config_name), PathBase(string.gsub(input, "%.%./", "")))
end

function merge_settings(target, source)
    target = target:Copy()
    function merge(target, source)
        for i, v in ipairs(source) do
            if not table.contains(target, v) then
                target:Add(v)
            end
        end
    end
    merge(target.cc.includes, source.cc.includes)
    merge(target.cc.defines, source.cc.defines)
    merge(target.cc.flags, source.cc.flags)
    merge(target.dll.libs, source.dll.libs)
    merge(target.dll.frameworks, source.dll.frameworks)
    merge(target.link.libs, source.link.libs)
    merge(target.link.frameworks, source.link.frameworks)
    return target
end

function format_define(def, value)
    if type(value) == "string" then
        value = "\\\"" .. value .. "\\\""
    end
    if value ~= nil then
        def = def .. "=" .. "\"" .. value .. "\""
    end
    return def
end

function NewLibrary(name)
    local library = {}
    library.name = name
    library.settings = NewSettings()
    library.source_files = {}

    -- Libraries/Frameworks used when linking
    library.AddSystemLibrary = function(self, ...)
        for i, lib in ipairs({...}) do
            self.settings.link.libs:Add(lib)
            self.settings.dll.libs:Add(lib)
        end
    end

    library.AddSystemFramework = function(self, ...)
        for i, fw in ipairs({...}) do
            self.settings.link.frameworks:Add(fw)
            self.settings.dll.libs:Add(fw)
        end
    end

    library.AddCompilerFlag = function(self, ...)
        for i, flag in ipairs({...}) do
            self.settings.cc.flags:Add(flag)
        end
    end

    library.AddDefine = function(self, def, value)
        def = format_define(def, value)
        self.settings.cc.defines:Add(def)
    end

    library.AddIncludeDirectory = function(self, ...)
        for i, dir in ipairs({...}) do
            path = join_root("Libraries/"..self.name, dir)
            self.settings.cc.includes:Add(path)
        end
    end

    library.AddSourceDirectory = function(self, ...)
        for i, dir in ipairs({...}) do
            path = join_root("Libraries/"..self.name, dir)
            for j, p in ipairs(Collect(path)) do
                table.insert(self.source_files, p)
            end
        end
    end

	library.AddSourceDirectoryR = function(self, ...)
        for i, dir in ipairs({...}) do
            path = join_root("Libraries/"..self.name, dir)
            for j, p in ipairs(CollectRecursive(path)) do
                table.insert(self.source_files, p)
            end
        end
    end

    library.Build = function(self, settings)
        local settings = merge_settings(settings, self.settings)
        local objs = Compile(settings, self.source_files)
        return StaticLibrary(settings, self.name, objs)
    end
    return library
end

function NewModule(name)
    local module = {}
    module.name = name
    module.settings = NewSettings()
    module.required_libraries = {}
    module.required_modules = {}
    module.source_files = {}

    module.RequireLibrary = function(self, ...)
        for i, library in ipairs({...}) do
            if self.required_libraries[library] == nil then
                table.insert(self.required_libraries, library)
            end
        end
    end

    module.AddIncludeDirectory = function(self, ...)
        for i, dir in ipairs({...}) do
            path = join_root("Modules/"..self.name, dir)
            self.settings.cc.includes:Add(path)
        end
    end

    module.AddSourceDirectory = function(self, ...)
        for i, dir in ipairs({...}) do
            path = join_root("Modules/"..self.name, dir)
            for j, p in ipairs(Collect(path)) do
                table.insert(self.source_files, p)
            end
        end
    end

    module.Build = function(self, project, settings, frameworkobjs, baked_exe)
        settings = merge_settings(settings, self.settings)
        -- Set a define so that we can check if a library or a module is available to use.
        add_libdef_to_module = function(library, settings)
            local defname = "CONF_WITH_LIBRARY_"..string.upper(library.name)
            settings.cc.defines:Add(defname)
            project:AddDefine(defname)
        end

        -- Build module as a dynamic library
        if not baked_exe then
            local libs = {}
            for i, l in ipairs(self.required_libraries) do
                local lib = project.BuildLibrary(project, settings, l)
                if lib == "system" then
				 	settings.link.libs:Add(lib)
				 	settings.dll.libs:Add(lib)
				else
					table.insert(libs, lib)
				end
                local library = load_library(l)
                add_libdef_to_module(library, settings)
                settings = merge_settings(settings, self.settings)
            end
            local objs = Compile(settings, self.source_files)
            -- http://www.gentoo.org/proj/en/base/amd64/howtos/index.xml?part=1&chap=3
            -- http://www.cmake.org/pipermail/cmake/2007-May/014350.html
            -- http://lists.freebsd.org/pipermail/freebsd-amd64/2005-April/004650.html
            -- settings.cc.flags:Add("-fPIC")
            -- settings.dll.flags:Add("-fPIC")
            return SharedLibrary(settings, self.name, objs, frameworkobjs, libs)

        -- Just compile the module for linking a static binary
        else
            for i, l in ipairs(self.required_libraries) do
                local library = load_library(l)
                add_libdef_to_module(library, settings)
            end
            return Compile(settings, self.source_files);
        end
    end

    return module
end

function NewProject(name)
    local project = {}
    project.name = name
    project.settings = NewSettings()
    project.required_modules = {}
    project.required_libraries = {}
    project.source_directories = {}

    project.AddSystemLibrary = function(self, ...)
        for i, lib in ipairs({...}) do
            self.settings.link.libs:Add(lib)
        end
    end

    project.AddDefine = function(self, def, value)
        def = format_define(def, value)
        self.settings.cc.defines:Add(def)
    end

    project.RequireLibrary = function(self, ...)
        for i, library in ipairs({...}) do
            if self.required_libraries[library] == nil then
                table.insert(self.required_libraries, library)
            end
        end
    end

    project.RequireModule = function(self, ...)
        for i, module in ipairs({...}) do
            table.insert(self.required_modules, module)
        end
    end

    project.AddIncludeDirectory = function(self, ...)
        for i, dir in ipairs({...}) do
            self.settings.cc.includes:Add(dir)
        end
    end

    project.AddSourceDirectory = function(self, ...)
        for i, dir in ipairs({...}) do
            table.insert(self.source_directories, dir)
        end
    end

    project.BuildLibrary = function(self, settings, l)
        if not self.built_list[l] then
            local library = load_library(l)
			local fn = "./" .. settings.lib.prefix .. library.name .. settings.config_ext .. library.settings.lib.extension
			local sfn = library.name .. settings.config_ext
			local fnexists = file_exists(fn)
			local lib = library.name
			if fnexists and NOLIBREBUILD then
				settings.link.libs:Add(sfn)
				settings.dll.libs:Add(sfn)
				return "system"
			end
            lib = library:Build(settings)
            table.insert(self.built_libs, lib)
            self.built_list[l] = lib
            return lib
        else
            return self.built_list[l]
        end
    end

    project.Build = function(self)
        local settings = self.settings
        local source_files = {}

        settings.cc.Output = transform_output

        for i,m in ipairs(self.source_directories) do
            table.insert(source_files, CollectRecursive(m))
        end

        -- Compile Project
        local DoBuild = function(settings, source_files, baked_exe)
            local dep_modules = {}
            local req_libraries = {}

            self.built_libs = {}
            self.built_mods = {}
            self.built_dlls = {}
            self.built_list = {}

            -- Framework depends on these to be included
            self:RequireLibrary("pstdint")
            self:RequireLibrary("jsoncpp")
            self:RequireLibrary("dirent")
            self:RequireLibrary("glm")

            -- Collect those libraries that are required for the project
            --    and add their headerfiles
            for i,l in ipairs(self.required_libraries) do
                if not table.contains(req_libraries, l) then
                    table.insert(req_libraries, l)
                end
                local lib = load_library(l)
                settings = merge_settings(settings, lib.settings)
            end

            -- Collect required modules, and add their headers too.
            for i,m in ipairs(self.required_modules) do
                local mod = load_module(m)
                dep_modules[mod.name] = mod
                settings = merge_settings(settings, mod.settings)

                -- Add library headers required for the module
                for j, lib in ipairs(mod.required_libraries) do
                    local library = load_library(lib)
                    settings = merge_settings(settings, library.settings)
                end
            end

            -- Setup exports (for modules)
            if baked_exe == false then
                if family == "windows" then
                    settings.cc.defines:Add("PXFEXPORT=\"extern \\\"C\\\" __declspec(dllexport)\"")
                else
                    settings.cc.defines:Add("PXFEXPORT=\"extern \\\"C\"\\\"")
                end
            else
                settings.cc.defines:Add("PXFEXPORT=\"\"")
            end

            -- Build framework
            framework_settings = settings:Copy()
            -- Pxf source files
            pxf_source_files = Collect(join_root("Source/*.cpp")
                                      ,join_root("Source/Base/*.cpp")
                                      ,join_root("Source/Input/*.cpp")
                                      ,join_root("Source/Graphics/*.cpp")
                                      ,join_root("Source/Resource/*.cpp")
                                      ,join_root("Source/Util/*.cpp"))
            local pxf_objs = Compile(framework_settings, pxf_source_files)

            -- Build modules
            for i, m in ipairs(self.required_modules) do
                settings.cc.defines:Add("CONF_WITH_MODULE_"..string.upper(dep_modules[m].name))
                dep_modules[m]:RequireLibrary("jsoncpp")
                local module = dep_modules[m]:Build(self, settings, pxf_objs, baked_exe)
                -- Build with embedded modules
                if baked_exe == true then
                    table.insert(self.built_mods, module)
                    -- Add required libraries if they aren't already wanted
                    for j, l in ipairs(dep_modules[m].required_libraries) do
                        if not table.contains(req_libraries, l) then
                            table.insert(req_libraries, l)
                        end
                    end
                -- Build with dynamic modules
                else
                    table.insert(self.built_dlls, module)
                end
            end

            -- Build required libraries
            for i, l in ipairs(req_libraries) do
                local library = load_library(l)
                self:BuildLibrary(settings, l)
                settings.cc.defines:Add("CONF_WITH_LIBRARY_"..string.upper(library.name))
                settings = merge_settings(settings, library.settings)
            end

            settings = merge_settings(settings, self.settings)

			
            -- Then build the project
            project = Compile(settings, source_files)
            if table.contains(settings.cc.defines, "CONF_SHAREDLIBRARY") then
                project_exe = SharedLibrary(settings, self.name, project, pxf_objs, self.built_mods, self.built_libs)
            else
                project_exe = Link(settings, self.name, project, pxf_objs, self.built_mods, self.built_libs)
            end
            project_target = PseudoTarget(self.name.."_"..settings.config_name, project_exe)

            -- Embedd proper licensing information
            if settings.debug == 0 then
                local mods = ""
                for i, mod in ipairs(req_libraries) do
                    mods = mods .. mod .. " "
                end
                local license_fn = "include/Pxf/License.gen_"..settings.config_name
                AddJob(license_fn, "Generate license information", "bam -e " .. join_root("build-tools.lua") .." -- -r=".. path_prefix .." gen-license " .. mods)
                AddDependency(project_exe, license_fn)
            end

            PseudoTarget(settings.config_name, project_target, self.built_dlls)

            return project_exe
        end

        --

        if family == "windows" then
            settings.link.libs:Add("dbghelp")
            settings.dll.libs:Add("dbghelp")
            settings.cc.flags:Add("/EHsc")
            settings.cc.defines:Add("NOMINMAX")
        end

        if family == "unix" then
            settings.cc.flags_cxx:Add("-std=c++11")
            settings.link.flags:Add("-L./")
			settings.link.libs:Add("dl")
            settings.link.libs:Add("pthread") -- Threading in base/platform (posix)
        end

        if platform == "linux" then
            settings.link.libs:Add("rt")
            settings.cc.flags:Add("-fPIC")
            settings.cc.flags:Add("-Wno-multichar")
            settings.cc.flags:Add("-Wfatal-errors") -- stop compilation on first error
        end

        -- Add framework include dir
        settings.cc.includes:Add(join_root("Include"))

        debug_settings = settings:Copy()
        debug_settings.cc.defines:Add("CONF_DEBUG")
        debug_settings.config_name = "debug"
        debug_settings.config_ext = "_sd"
        debug_settings.debug = 1
        debug_settings.optimize = 0
        debug_settings_dll = debug_settings:Copy()
        debug_settings_dll.config_name = "debug_dll"
        debug_settings_dll.config_ext = "_d"
        debug_settings_dll.cc.defines:Add("CONF_MODULAR")

        sharedlib_debug_settings = debug_settings:Copy()
        sharedlib_debug_settings.config_name = "shared_debug"
        sharedlib_debug_settings.config_ext = "_ssd"
        sharedlib_debug_settings.cc.defines:Add("CONF_SHAREDLIBRARY")

        release_settings = settings:Copy()
        release_settings.cc.defines:Add("CONF_RELEASE")
        release_settings.config_name = "release"
        release_settings.config_ext = "_sr"
        release_settings.debug = 0
        release_settings.optimize = 1
        release_settings_dll = release_settings:Copy()
        release_settings_dll.config_name = "release_dll"
        release_settings_dll.config_ext = "_r"
        release_settings_dll.cc.defines:Add("CONF_MODULAR")

        sharedlib_release_settings = release_settings:Copy()
        sharedlib_release_settings.config_name = "shared_release"
        sharedlib_release_settings.config_ext = "_ssr"
        sharedlib_release_settings.cc.defines:Add("CONF_SHAREDLIBRARY")

        -- Create build targets
        DefaultTarget(DoBuild(debug_settings, source_files, true))
        DoBuild(release_settings, source_files, true)
        DoBuild(sharedlib_release_settings, source_files, true)
        DoBuild(sharedlib_debug_settings, source_files, true)

        DoBuild(debug_settings_dll, source_files, false)
        DoBuild(release_settings_dll, source_files, false)
    end

    return project
end
